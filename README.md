Period04 source repository
--------------------------------------------------------------------------
- /period04 ... contains latest version of Period04 source code
- /sgt      ... contains a modified version of Scientific Graphics Toolkit

See
- /period04/src/README.txt ... for rudimentary and slightly outdated compilation instructions
- /period04/src/LICENSE.txt ... for the copyright notice
