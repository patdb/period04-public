/*----------------------------------------------------------------------*
 *  XDialogEdit
 *  a dialog to edit a substrings data
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.*;                  // Property change stuff
import javax.swing.*;

public class XDialogEdit extends XDialog 
    implements ActionListener
{
    private static final long serialVersionUID = -1892355854354001830L;
    private String typedName     = null;
    private String typedWeight   = null;
    private String selectedColor = null;

    private JOptionPane optionPane;
    private final String s_OK = "OK";
    private final String s_Cancel = "Cancel";
    private final JComboBox b_Color;
    private boolean cancel = false;

    public XDialogEdit(JFrame mainframe, 
		       String s_name, String s_weight, String s_color) {
	super(mainframe, true);  // necessary to keep dialog in front

        Object [] options = {s_OK, s_Cancel};
	String [] s_Colors = new String[JColor.getNumberOfColors()];
	for (int i=0; i<JColor.getNumberOfColors(); i++) {
	    s_Colors[i] = JColor.getColorString(i);
	}

	JPanel inputpanel = new JPanel();
	inputpanel.setLayout(null);
	final JLabel l_Name       = createBoldLabel( "Name:"  );
	final JLabel l_Weight     = createBoldLabel( "Weight:");
	final JLabel l_Color      = createBoldLabel( "Color:" );
        final JTextField t_Name   = createTextField( s_name   );
        final JTextField t_Weight = createTextField( s_weight );
	b_Color = createComboBox(s_Colors);
	// set the appropriate color selected
	for (int i=0; i<s_Colors.length; i++) { 
	    if (s_color.compareTo(s_Colors[i])==0) {
		b_Color.setSelectedItem(s_Colors[i]);
	    }
	}
	l_Name.setBounds( 10, 10, 60, 20 );
	t_Name.setBounds( 80, 10, 160, 20 );
	l_Weight.setBounds( 10, 40, 60, 20 );
	t_Weight.setBounds( 80, 40, 160, 20 ); 
	l_Color.setBounds( 10, 70, 60, 20 );
	b_Color.setBounds( 80, 70, 160, 20);
	inputpanel.add( l_Name, null );
	inputpanel.add( t_Name, null );
	inputpanel.add( l_Weight, null );
	inputpanel.add( t_Weight, null );
	inputpanel.add( l_Color, null );
	inputpanel.add( b_Color, null );
        Object array = inputpanel;

        optionPane = new JOptionPane(array, 
                                    JOptionPane.PLAIN_MESSAGE,
                                    JOptionPane.YES_NO_OPTION,
                                    null, options, options[0]);
        setContentPane(optionPane);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent we) {
                /*
                 * Instead of directly closing the window,
                 * we're going to change the JOptionPane's
                 * value property.
                 */
                    optionPane.setValue(
			new Integer(JOptionPane.CLOSED_OPTION));
            }
        });

	optionPane.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
                String prop = e.getPropertyName();

                if (isVisible() 
                 && (e.getSource() == optionPane)
                 && (prop.equals(JOptionPane.VALUE_PROPERTY) ||
                     prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
                    Object value = optionPane.getValue();

                    if (value == JOptionPane.UNINITIALIZED_VALUE) {
                        //ignore reset
                        return;
                    }

                    // Reset the JOptionPane's value.
                    // If you don't do this, then if the user
                    // presses the same button next time, no
                    // property change event will be fired.
                    optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

                    if (value.equals(s_OK)) {
			typedName = t_Name.getText();
			typedWeight = t_Weight.getText();
			selectedColor = (String)b_Color.getSelectedItem();

			// check wether someone put letters into the weight field 
			NumberFormatException nan = null;
			try { Double.valueOf(typedWeight); } 
			catch (NumberFormatException exception) { nan=exception; }			   

			if (typedName.equals("")) {
			    // text was invalid
			    t_Name.selectAll();
			    JOptionPane.showMessageDialog(
				XDialogEdit.this,
				"Sorry, \"" + typedName + "\" "
				+ "isn't a valid name.\n",
				"Invalid input",
				JOptionPane.ERROR_MESSAGE);
			    typedName = null;
			} 
			else if ( typedWeight.equals("") || (nan!=null) ) {
			    t_Weight.selectAll();
			    JOptionPane.showMessageDialog(
				XDialogEdit.this,
				"Sorry, \"" + typedWeight + "\" "
				+ "isn't a valid weight.\n",
				"Invalid input",
				JOptionPane.ERROR_MESSAGE);
			    typedWeight = null;
			}
			else {
			    // we're done; dismiss the dialog
			    setVisible(false);
			}
		    } 
		    else { // user closed dialog or clicked cancel
			cancel = true;
			setVisible(false);
		    }
                }
            }
        });

	this.setTitle("Edit item properties:");
	this.setSize(290,190);
	this.setResizable(false);
	this.setLocationRelativeTo(mainframe);
	this.setVisible(true);
    }

    public String getName() {
        return typedName;
    }
    
    public String getWeight() {
	return typedWeight;
    }

    public String getColor() {
	return selectedColor;
    }

    public boolean isCanceled() { return cancel; }

    public void actionPerformed(ActionEvent evt) {
	if ( evt.getSource()!=b_Color ) 
	    optionPane.setValue(s_OK);
    }
}
