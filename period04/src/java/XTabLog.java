/*---------------------------------------------------------------------*
 *  XTabLog.java
 *  creates the Log tab
 *---------------------------------------------------------------------*/

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.awt.*;
import java.awt.event.*;
import java.text.*;                        // dateformat
import java.util.*;                        // calendar, locale
import javax.swing.*;
import javax.swing.border.*;               // etched border
import javax.swing.event.*;
import javax.swing.filechooser.*;
import javax.swing.JFileChooser.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.StyledEditorKit;
import javax.swing.text.StyleConstants;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.AttributeSet;
import javax.swing.text.Element;
import javax.swing.text.ElementIterator;
import javax.swing.text.StyledDocument;

public class XTabLog extends XTab
{
    private JTextPane  t_Log;                       // displays the text
    protected DefaultStyledDocument doc;
    protected StyledEditorKit kit;
    private JScrollPane logScrollPane;
    private JButton b_PrintLog, b_ExportLog;
    private JFileChooser fc;
    private int correct;
    private XProjectFrame mainframe;

    public XTabLog(XProjectFrame m_frame) {
	mainframe = m_frame;
	correct   = mainframe.getSizeDiff();

	// create main panel
	mainPanel = new JPanel();
	mainPanel.setLayout(null);
	createMainPanel();

	writeProtocol("Period04 Log - "+getDate()+"\n", false);
    }

    private void createMainPanel() {

	b_PrintLog = createButton("Print Log");
	b_PrintLog.setBounds( 5, 10, 100, 20 );
	b_ExportLog = createButton("Export Log");
	b_ExportLog.setBounds( 105, 10, 100, 20 );

	t_Log = new JTextPane();
 	doc   = new DefaultStyledDocument();
	kit   = new StyledEditorKit();
	t_Log.setEditorKit(kit);
	t_Log.setDocument(doc);
	
	// make tLog scrollable
	logScrollPane = new JScrollPane(t_Log, 
			    JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, 
			    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	logScrollPane.setBorder(BorderFactory.createEtchedBorder(
				    EtchedBorder.RAISED));
	if (Period04.isMacOS)
	    logScrollPane.setBounds( 5, 30, 500, 580-correct );
	else
	    logScrollPane.setBounds( 5, 30, 480, 560-correct );
       
	// create main panel
	mainPanel = new JPanel();
	mainPanel.setLayout(null);

	// add subpanels
	mainPanel.add(logScrollPane, null);
	mainPanel.add(b_PrintLog, null);
 	mainPanel.add(b_ExportLog, null);
    }

  public void initializeFileChooser ( )
  {
    // create FileChooser
    fc = new JFileChooser( Period04.getCurrentDirectory( ));

    fc.setFileFilter( new FileFilter ( )
      {
        public boolean accept ( File f)
        {
          return f.isDirectory( )
              || f.getName( ).toLowerCase( ).endsWith( ".txt")
          ;
        }

        public String getDescription ( )
        {
          return "Log files(*.txt)";
        }
      }
    );
  }

    public void actionPerformed(ActionEvent evt) { 
	Object s=evt.getSource();

	// calculation mode
	if (s==b_PrintLog) {
	    printLog();
	} else if (s==b_ExportLog) {
	    exportLog();
	}
    }
	
    public void exportLog() {
	initializeFileChooser();
	int returnVal = fc.showSaveDialog(mainPanel);
	if (returnVal == JFileChooser.APPROVE_OPTION) {
	    File file = fc.getSelectedFile();
	    //--- add an extension if not given
	    String name = file.getName();
	    if (name.indexOf(".")==-1) { 
		file = new File(file.getPath()+".log");
	    }
	    // check if file exists
	    if (file.exists()) {
		// ask if user wants to replace the existing file
		int value = JOptionPane.showConfirmDialog(mainframe.getFrame(),
		    "File does already exist!\n"+
		    "Do you want to replace the file?",
		    "File does exist", JOptionPane.YES_NO_OPTION);
		if (value == JOptionPane.NO_OPTION) {
		    return;
		}
	    }
	    // check wether we can write to the selected file
	    Period04.setCurrentDirectory(file);
	    mainframe.setSaving(true); // create a progress bar
	    // create a task for output
	    FileTask task = new FileTask(
		mainframe, FileTask.LOG_OUT, file, getProtocolText());
	    task.go();
	} 
    }

    public void printLog() {
	printText(getProtocolText());
    }

    //---
    //--- Token 
    //---
    public class Token {
	public String  text;
	public Color   color;
	public boolean bold;

	//--- constructor
	public Token(String t, Color c, boolean b) { text=t; color=c; bold=b; }
    }

    public void writeProtocol(String text, boolean withDate) {
	if (withDate) {
	    text = getDate()+":   "+text;
	}
	//---
	//--- make it a stylish text
	//---
	Vector content = new Vector();
	boolean bold = false;
	StringTokenizer st = new StringTokenizer(text, "*", false);
	while(st.hasMoreTokens()) {
	    String tmp = st.nextToken();
	    content.addElement(new Token(tmp+" ", Color.black, bold));
	    bold = !bold;
	}
 
	MutableAttributeSet attr = new SimpleAttributeSet();
	int i;

	//--- define the font size
	int fsize = 10;
	if (System.getProperty("os.name").toUpperCase().startsWith("WIN")) {
	    fsize = 11;
	}

	for(i = 0; i<content.size(); i++) {
	    StyleConstants.setBold(attr, ((Token) content.elementAt(i)).bold);
	    StyleConstants.setForeground(attr, ((Token) content.elementAt(i)).color);
	    StyleConstants.setFontFamily(attr, "Monospaced");
	    StyleConstants.setFontSize(attr, fsize);
	    try {
		doc.insertString(doc.getLength(), 
				 ((Token) content.elementAt(i)).text, attr);
	    } catch(BadLocationException ble) {}
	}
	try{
	    doc.insertString(doc.getLength(), "\n", new SimpleAttributeSet());}
	catch(BadLocationException ble) {}

	Period04.projectSetChanged();   // project has changed, obviously
	updateScrollViewPort();         // show the last entry
    }

    private String styledDocToPlain(StyledDocument doc) {
        ElementIterator eIter = new ElementIterator(doc);
        StringBuffer    strb  = new StringBuffer();
        Element holder = eIter.next();
        while (holder!=null){
            if (holder.isLeaf()){
                try {
		    String str = doc.getText(
			holder.getStartOffset(),
			holder.getEndOffset()-holder.getStartOffset());
		    if (str.contentEquals(new StringBuffer("\n"))) { 
			strb.append("\n"); 
			holder = eIter.next(); 
			continue;
		    }
		    boolean bold=StyleConstants.isBold(holder.getAttributes());
		    if (bold) { 
			strb.deleteCharAt(strb.length()-1);
			strb.append("*"); 
		    }
		    strb.append(str);
		    if (bold) { 
			strb.deleteCharAt(strb.length()-1);
			strb.append("*"); 
		    }
		} catch (BadLocationException e) {
                    e.printStackTrace();
		}
            }
            holder = eIter.next();
        }
        return strb.toString();
    }

    public void cleanProtocol(boolean printHeader) {
 	doc   = new DefaultStyledDocument();
	t_Log.setDocument(doc);
	if (printHeader) {
	    writeProtocol("Period04 Log - "+getDate()+"\n", false);
	}
    }

    public static String getDate() {
	SimpleDateFormat date = new SimpleDateFormat(
	    "E MMM dd HH:mm:ss yyyy", Locale.ENGLISH );
	Calendar cal;
	cal = new GregorianCalendar();
	return date.format(cal.getTime());
    }

    public void updateDisplay() {
	updateScrollViewPort();
    }

    //---
    //--- updateScrollViewPort
    //--- put the viewport at the end of the scrollpane
    //---
    private void updateScrollViewPort() {
	logScrollPane.getVerticalScrollBar().setValue(
	    logScrollPane.getVerticalScrollBar().getMaximum());
    }

    public String getProtocolText() {
	return styledDocToPlain(doc);
    }
    
    public void fitSize() {
	correct = mainframe.getSizeDiff(); 
	if (Period04.isMacOS) 
	    logScrollPane.setBounds( 5, 30, 500, 560-correct );
	else 
	    logScrollPane.setBounds( 5, 30, 480, 560-correct );
    }

}


