/*----------------------------------------------------------------------*
 *  XDialogShowTimes
 *  a dialog to subdivide the timestring
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.*;                  // Property change stuff
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.event.*;
import javax.swing.text.BadLocationException;

class XDialogShowTimes extends XDialog 
    implements ItemListener, ComponentListener
{
    private static final long serialVersionUID = 4167738982382189242L;
    private JOptionPane  optionPane;
    private final String s_OK="Close", s_Export="Export", s_Print="Print";
    
    private JCheckBox   checkIsDays;
    private JComboBox   cb_attribute;   
    private JTextArea   textarea;
    private JScrollPane textScrollPane;   

    private XProjectFrame     mainframe;
    private XTabTimeString tabTS;

    private final int WIDTH  = 650;      // default frame width
    private final int HEIGHT = 400;      // default frame height
    private int sizeDiff = 0;     // correction factor for height

    // a variable to prevent double eventhandling
    private boolean itemselected = false;
    
    public XDialogShowTimes(XProjectFrame frame, XTabTimeString tab) {
	super(frame.getFrame(), false);
	mainframe = frame;
	tabTS = tab;
	Object[] options = {s_Export, s_Print, s_OK}; // set button names
	    
	JPanel inputpanel = new JPanel();
	inputpanel.setLayout(null);
	
	JLabel l_show = createLabel(
	    "Show time structuring of the current time string");
	JLabel l_using = createLabel("using attribute: ");
	String [] s_attributes = new String[4];
	for (int i=0; i<4; i++) {
	    s_attributes[i] = Period04.projectGetNameSet(i);
	}	    
	cb_attribute = new JComboBox(s_attributes);
	cb_attribute.addItemListener(this);

	checkIsDays = new JCheckBox("Time unit is in days");
	checkIsDays.setSelected(true);
	checkIsDays.addItemListener(this);

	// create a textarea
	textarea = new JTextArea();
        textarea.setFont(new Font("Monospaced", Font.PLAIN, 11));
	updateList(0);
	// make the list scrollable
	textScrollPane = new JScrollPane(
	    textarea, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
	    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	textScrollPane.setBorder(
	    BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
	
	// set position
	l_show         .setBounds( 10,  0, 400,  20);
	l_using        .setBounds( 10, 22, 140,  20);
	cb_attribute   .setBounds(150, 22, 250,  20);
	checkIsDays    .setBounds( 10, 50, 200,  20);
	textScrollPane .setBounds(  7, 80, 615, 220);
	
	// add to the panel
	inputpanel.add(l_show);
	inputpanel.add(l_using);
	inputpanel.add(cb_attribute);
	inputpanel.add(checkIsDays);
	inputpanel.add(textScrollPane);
	
	//---
	//--- create the optionpane and add the inputpanel and the buttons
	//---
	optionPane = new JOptionPane((Object)inputpanel, 
				     JOptionPane.PLAIN_MESSAGE,
				     JOptionPane.YES_NO_OPTION,
				     null, options, options[2]);
	setContentPane(optionPane);
	setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent we) {
		    /*
		     * Instead of directly closing the window,
		     * we're going to change the JOptionPane's
		     * value property.
		     */
		    optionPane.setValue(
			new Integer(JOptionPane.CLOSED_OPTION));
		}
	    });
	// listen for frame size changes
	addComponentListener(this); 
	
	optionPane.addPropertyChangeListener(new PropertyChangeListener() {
		public void propertyChange(PropertyChangeEvent e) {
		    String prop = e.getPropertyName();
		    
		    if (isVisible() 
			&& (e.getSource() == optionPane)
			&& (prop.equals(JOptionPane.VALUE_PROPERTY) ||
			    prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
			Object value = optionPane.getValue();
			
			if (value == JOptionPane.UNINITIALIZED_VALUE) {
			    //ignore reset
			    return;
			}
		
			if (value=="Print") {
				print();
			}
			else if (value=="Export")
			    export();
			else 
			    setVisible(false);
		    }
			// reset 
			optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);
		}
	    });
	    
	this.setTitle("Show time structuring:");
	this.setSize(WIDTH,HEIGHT);
	this.setLocationRelativeTo(mainframe.getFrame());
	this.setVisible(true);
    }

    public void actionPerformed(ActionEvent evt) {
	optionPane.setValue(s_OK);
    }
 
    public void itemStateChanged(ItemEvent evt) {
	Object s=evt.getSource();
	if (s==checkIsDays) {
	    updateList(cb_attribute.getSelectedIndex());
	}
	if (s==cb_attribute) {
	    if (itemselected==true) { // a real event
	        updateList(cb_attribute.getSelectedIndex());
		itemselected = false;
	    } else { // just a secondary event - ignore
		itemselected = true;
	    }
	}
    }
    
    public void updateList(int attribute) {
	textarea.setText(Period04.projectGetTimeStatistics(
			     attribute, checkIsDays.isSelected()));
	try { textarea.setCaretPosition(textarea.getLineStartOffset(0)); }
	catch (BadLocationException e) {}
    }

    public void export() {
	// create FileChooser
	JFileChooser fc = new JFileChooser(Period04.getCurrentDirectory());
	int returnVal = fc.showSaveDialog(mainframe.getFrame());
	if (returnVal == JFileChooser.APPROVE_OPTION) {
	    File file = fc.getSelectedFile();
	    // check wether user wants to overwrite an existing file
	    if (file.exists()) {
		int v = JOptionPane.showConfirmDialog(
		    mainframe.getFrame(), "File does already exist!\n"+
		    "Do you really want to delete the old file?", 
			"Warning", JOptionPane.YES_NO_OPTION);
		if (v == JOptionPane.NO_OPTION) {
		    return;
		} 
	    }
	    // now go and save it
	    Period04.setCurrentDirectory(file);
	    try {
		FileWriter fw = new FileWriter(file);
		fw.write(textarea.getText());
		if ( fw!=null ) fw.close();
	    } catch (IOException e) {
		System.err.println(e);
	    }
	}
    }

    public void print() {
	printResult(textarea.getText());
    }


    //---
    //--- methods to listen to ComponentEvents 
    //--- (we are listening to ComponentEvents that are caused by resizing 
    //---  the mainframe)
    //---
    public void componentResized(ComponentEvent e) {
	//--- the frame has been resized. 
	//--- set the framewidth back to the default
	//--- if the frameheight is to low set it back to the default value
	//--- if the frameheight is ok, then determine the difference to the 
	//--- default value and call fitSize()
	//--- this will size the GUI elements to fit to the new frameheight
	Component c = e.getComponent();
	Dimension frameSize = c.getSize();
	if (frameSize.height <= HEIGHT) {
	    frameSize.height = HEIGHT;
	    sizeDiff = 0;
	} else {
	    sizeDiff = HEIGHT-frameSize.height;
    	}
	frameSize.width = WIDTH;
	setSize(frameSize);
	fitSize();
    }
    public void componentHidden(ComponentEvent e) {}
    public void componentShown(ComponentEvent e)  {}
    public void componentMoved(ComponentEvent e)  {}

    private void fitSize() {
	textScrollPane.setBounds(  7, 80, 615, 220-sizeDiff);
    }
}

