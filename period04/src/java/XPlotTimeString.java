/*---------------------------------------------------------------------*
 *  XPlotTimeString.java
 *  plots the time series data and the aktive fit
 *---------------------------------------------------------------------*/

import java.awt.Container;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import javax.help.CSH;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.MouseInputAdapter;

import gov.noaa.pmel.sgt.AbstractPane;
import gov.noaa.pmel.sgt.Axis;
//import gov.noaa.pmel.sgt.PlainAxis;
import gov.noaa.pmel.sgt.AxisNotFoundException;
import gov.noaa.pmel.sgt.Attribute;
import gov.noaa.pmel.sgt.PointAttribute;
import gov.noaa.pmel.sgt.LineAttribute;
import gov.noaa.pmel.sgt.CartesianGraph;
import gov.noaa.pmel.sgt.PlotMark;//use modified PlotMark class
import gov.noaa.pmel.sgt.Graph;
import gov.noaa.pmel.sgt.SGLabel;
import gov.noaa.pmel.sgt.dm.SGTData;
import gov.noaa.pmel.sgt.dm.SGTLine;
import gov.noaa.pmel.sgt.dm.SimplePoint;
import gov.noaa.pmel.sgt.dm.SimpleLine;
import gov.noaa.pmel.sgt.dm.SGTMetaData;
import gov.noaa.pmel.sgt.dm.Collection;
import gov.noaa.pmel.sgt.dm.PointCollection;

import gov.noaa.pmel.sgt.swing.XPlotLayout;

import gov.noaa.pmel.util.Domain;
import gov.noaa.pmel.util.Range2D;
import gov.noaa.pmel.util.Point2D;
import gov.noaa.pmel.util.SoTPoint;


public class XPlotTimeString extends XPlot 
    implements PropertyChangeListener, ComponentListener
{
    private JMenuBar    menuBar = new JMenuBar();
    private JMenu       menuFile, menuColor, menuData, menuZoom;
    private JMenu       menuDisplay, menuHelp;
    private JMenuItem   i_Print, i_ExportJPG, i_ExportEPS, i_Close;
    private JMenuItem   i_DisplayAll, i_SelectViewport, i_KeepViewport, i_Back;
    private JMenuItem   i_PlotHelp;
    private JCheckBoxMenuItem i_DisplayHeader;
    private JCheckBoxMenuItem [] i_Color = new JCheckBoxMenuItem[4];
    private JCheckBoxMenuItem [] i_Data  = new JCheckBoxMenuItem[4];

    private double  [] xArray;
    private double  [] yArray;
    private SGTData [] data;

    private double [] domain;
    private int     colorAttribute;
    private int     useData;
    private boolean isPlotFit = false;

    public  XProjectFrame mainframe;
    private boolean blockPropertyChanges = false;

    public XPlotTimeString(XProjectFrame mainframe) {
  this.mainframe = mainframe;

  layout = new XPlotLayout();
  layout.setUseOffset(true);  // prevents from points lying on the axes

  mainpanel = new JPanel();
  makeMenuBar();
  statusBar=new JLabel(stdtext);
  statusBar.setFont(new Font("Dialog", Font.PLAIN, 11));
  statusBar.setBorder(
      BorderFactory.createEtchedBorder(EtchedBorder.RAISED));

  JPanel plotpanel = new JPanel();
  plotpanel.setLayout(new BorderLayout());
  plotpanel.add(layout, BorderLayout.CENTER);

  Container content = getContentPane();
  content.setLayout(new BorderLayout());
  content.add(plotpanel, BorderLayout.CENTER);
  content.add(statusBar, BorderLayout.SOUTH);

  /*
   * Listen for window closing events and release resources
   */
  addWindowListener(new WindowAdapter() {
    public void windowClosing(WindowEvent event) {
        JFrame fr = (JFrame)event.getSource();
        fr.setVisible(false);
        fr.dispose();
        XPlotTimeString.this.mainframe.dereferenceTSPlot();
    }
    public void windowActivated(WindowEvent we) {
        XPlotTimeString.this.mainframe.updateCurrentProject();
    }
      });
  // listen for frame size changes
  addComponentListener(this);
  // listen for mouse events
  MyMouseInputAdapter m = new MyMouseInputAdapter();
  layout.addMouseListener(m);
  layout.addMouseMotionListener(m);
  // listen for key events
  addKeyListener(new KeyListener() {
	  @Override
	  public void keyPressed(KeyEvent e) {
	      //System.out.println("Key pressed code=" + e.getKeyCode() + ", char=" + e.getKeyChar());
	      if (e.getKeyChar()=='+')
              {
		PlotMark.increaseMarkSize();
		layout.revalidate();
	      }
	      if (e.getKeyChar()=='-')
              {
		PlotMark.decreaseMarkSize();
		layout.revalidate();
	      }
	  }

	  @Override
	  public void keyTyped(KeyEvent e) { }

	  @Override
	  public void keyReleased(KeyEvent e) { }
  });
   
  if (Period04.projectGetSelectedPoints()!=0) {
      layout.setBatch(true);  // batch changes to layout.
      readData();             // read the data and add to layout
      layout.setBatch(false); // execute changes
  }

  setTitle("Time string plot");        // configure frame
  setSize(500,400);
  setLocationRelativeTo(mainframe.getFrame());
  setVisible(true);                    // show it

  // does it make sense to draw the fit?
  // this has to be done AFTER the plot has been drawed and sized!
  if (Period04.projectGetActiveFrequencies()!=0) { 
      layout.setUseOffset(false);
      plotFit(); 
      layout.setUseOffset(true);
  }
  
  // any time a zooming action occurs the program should check
  // wether it makes sense to draw the fit or not.
  // we add a PropertyChangeListener to layout in order to listen
  // to PropertyChangeEvents that are fired by a zooming action  
  layout.addPropertyChangeListener(this);
    }

    public void makeMenuBar() {
  // create main-menu
  menuFile    = XMenuHelper.addMenuBarItem( menuBar, "_Graph" );
  menuColor   = XMenuHelper.addMenuBarItem( menuBar, "_Colors" );
  menuData    = XMenuHelper.addMenuBarItem( menuBar, "D_ata" );
  menuDisplay = XMenuHelper.addMenuBarItem( menuBar, "_Display" );
  menuZoom    = XMenuHelper.addMenuBarItem( menuBar, "_Zoom" );
  menuHelp    = XMenuHelper.addMenuBarItem( menuBar, "_Help" );

  // create action listener for menu items
  ActionListener action = new ActionListener() {
    public void actionPerformed( ActionEvent evt ) {
        Object s=evt.getSource();
        // graph menu
        if (s==i_Print)          { print(); }
        else if (s==i_ExportEPS) { exportImage(XPlot.EPS); }
        else if (s==i_ExportJPG) { exportImage(XPlot.JPG); }
        else if (s==i_Close)     { 
      dispose();
      XPlotTimeString.this.mainframe.dereferenceTSPlot();
        }
        // color menu
        for (int i=0; i<4; i++) {
      if (s==i_Color[i]) {
          if (i_Color[i].isSelected()) { 
        setUseAttribute(i); 
          }
      }
        }
        // data menu
        for (int i=0; i<4; i++) {
      if (s==i_Data[i]) {
          if (i_Data[i].isSelected()) { setUseData(i); }
      }
        }
        // zoom menu
        if (s==i_DisplayAll)          { resetZoom(); }
        else if (s==i_SelectViewport) { setZoom("Time", "Magnitude"); }
        else if (s==i_Back)           { undoZoom(); }
        else if (s==i_KeepViewport)      {
      setMaintainScale(i_KeepViewport.isSelected()); 
        }
        // options menu
        if (s==i_DisplayHeader) { 
      displayHeader(i_DisplayHeader.isSelected()); 
        }
    }
      };

  //---
  //--- file menu 
  //---
  i_Print = XMenuHelper.addMenuItem(menuFile, "Print Graph", action);
  JMenu export = new JMenu("Export Graph As ");
  menuFile.add( export );
  i_ExportEPS = XMenuHelper.addMenuItem(export, "eps", action);
  i_ExportJPG = XMenuHelper.addMenuItem(export, "jpg", action);
  // ...
  i_Close = XMenuHelper.addMenuItem(menuFile, "Close", action);
  //---
  //--- color menu
  //---
  ButtonGroup bg1 = new ButtonGroup(); 
  for (int i=0; i<4; i++) {
      i_Color[i] = XMenuHelper.addCheckBoxMenuItem(
    menuColor, Period04.projectGetNameSet(i), action);
      bg1.add(i_Color[i]);
  }
  i_Color[0].setSelected(true);
  colorAttribute=0;       
  //---
  //--- data menu
  //---
  useData = mainframe.getFitTab().getLastCalculationID();
  ButtonGroup bg2 = new ButtonGroup();
  String [] entries = {"Observed", "Adjusted", 
           "Residuals (Observed)", "Residuals (Adjusted)"};
  for (int i=0; i<4; i++) {
      i_Data[i] = XMenuHelper.addCheckBoxMenuItem(
    menuData, entries[i], action);
      bg2.add(i_Data[i]);
  }
  i_Data[useData].setSelected(true);
  //---
  //--- zoom menu
  //---
  i_DisplayAll = XMenuHelper.addMenuItem(
      menuZoom, "_Display all", 'D', action);
  i_SelectViewport = XMenuHelper.addMenuItem(
      menuZoom, "Select _viewport", 'V', action);
  i_KeepViewport = XMenuHelper.addCheckBoxMenuItem(
      menuZoom, "Freeze viewport", action);
  i_Back = XMenuHelper.addMenuItem(
      menuZoom, "_Back", 'B', action);
  //---
  //--- options menu
  //---
  i_DisplayHeader = XMenuHelper.addCheckBoxMenuItem(
      menuDisplay, "Display header", action);
  //---
  //--- help menu
  //---
  i_PlotHelp = XMenuHelper.addMenuItem(
      menuHelp, "Time string plots",
      new CSH.DisplayHelpFromSource(XProjectFrame.getHelpBroker()));
  CSH.setHelpIDString(i_PlotHelp,"gui.timestringplots"); // set link

  // add it to the panel
  this.setJMenuBar( menuBar );
    }

  //
  //
  //

  void readData ( )
  {
    if (Period04.projectGetSelectedPoints()<=0) { return; }
    final int names     = Period04.projectNumberOfNames(colorAttribute);
    final int selpoints = Period04.projectGetSelectedPoints();
    PointCollection [] data = new PointCollection[names];
    int idName=0;
    int [] idColor      = new int[names];
    int [] idConversion = new int[names];
    int i;

    // As far I could analyse this.
    // Here the IDs of color names, which could be non continious,
    // mapped into a sequential array index.

    // Anyway some entries of the array will still stay empty
    // since most times not for all colors there will be points selected.

    // What the hell means that?
    // // use the appropriate index !!
    for ( i = 0; i < names; i++)
    {
      idConversion[i] = Period04.projectGetIndexNameID( colorAttribute, i);

//      System.err.println
//        ( "idConversion "
//        + Integer.toString( idConversion[i])
//        + " -> "
//        + Integer.toString( i)
//        )
//      ;
    }
    
    // Cause the points come in groups most of the time the color is not changing.
    int last_idName = -1;
    int last_idConversion = -1;

    for ( i = 0; i < selpoints; i++)
    {
      idName = Period04.projectTimePointGetIDName( colorAttribute, i);

      if (idName < 0)
      { 
        // if the list entry was "unknown" then idName = -1 at this
        // point which would cause an ArrayIndexOutOfBoundsException,
        // in this case we have to set it to 0 again!
        idName = 0; 
      }

      if (last_idName == idName)
      {
        idName = last_idConversion;
      }
      else
      {
        boolean found = false;

        last_idName = idName;

        for ( int j = 0; j < names; j++)
        {
          if (idName == idConversion[j])
          {
            idName = j;
            found=true;
            break;
          }
        }

        last_idConversion = idName;

//         System.err.println
//           ( "point "
//           + Integer.toString( i)
//           + ": "
//           + Integer.toString( last_idName)
//           + " -> "
//           + Integer.toString( last_idConversion)
//           )
//         ;

        if (! found)
        {
          System.err.println( "Error in readData: Cannot find index!");
        }

        if (data[idName] == null)
          data[idName] = new PointCollection( );
      }

      double [] timepoint = Period04.projectTimePointGet( i, useData);

      data[idName].add( new SimplePoint
//        ( Period04.projectTimePointGetTime( i)
//        , Period04.projectTimePointGetAmplitude( i, useData)
        ( timepoint[0]
        , timepoint[1]
        , "timestring"
        )
      );
    }

//     System.err.println
//       ( "point "
//       + Integer.toString( i)
//       )
//     ;



    for (i=0; i<names; i++) {
      int id = mainframe.getTimeStringTab().retrieveListEntryID(colorAttribute, i);
      idColor[i] = Period04.projectGetIDNameColor(id, colorAttribute);
      if (data[i]!=null) {
        SGTMetaData meta = new SGTMetaData("Time","", false, false);
        data[i].setXMetaData(meta);
        boolean reverse = Period04.projectGetReverseScale();
        switch (useData) {
        case 1:
          meta = new SGTMetaData("Adjusted","", reverse, false);
          break;
        case 2:
          meta = new SGTMetaData(
                       "Residuals (Obs)","", reverse, false);
          break;
        case 3:
          meta = new SGTMetaData(
                       "Residuals (Adj)","", reverse, false);
          break;
        default:
          meta = new SGTMetaData("Observed","", reverse, false);
          break;
        }
        data[i].setYMetaData(meta);
        data[i].setId("Data "+i);
      }
    }


    // define point symbol properties
    int counter=0;
    for (i=0; i<names; i++) {
        if (data[i]!=null) {
      PointAttribute pa = new PointAttribute(51, JColor.getColor(idColor[i]));
      pa.setMarkHeightP( 0.2 );
      layout.addData(data[i],pa,"", XPlotLayout.POINTS);
      counter=i;
      break;
        }
    }

    if(counter+1<names) {
        for (i=counter+1; i<names; i++) {
      if (data[i]!=null) {
          PointAttribute pa = new PointAttribute(51, JColor.getColor(idColor[i]));
          pa.setMarkHeightP( 0.2 );
          layout.addData(data[i], pa, "", XPlotLayout.POINTS);
      }
        }
    }
    this.data = data; // save data
    }

    //
    //
    //

    private boolean checkShowFit() {
  // only plot the fit if we are showing the appropriate data
  switch (useData) {
      case 0: if (!(mainframe.getFitTab().getLastCalculationID()==0)) { return false; } break;
      case 1: if (!(mainframe.getFitTab().getLastCalculationID()==1)) { return false; } break;
      default: return false;
  }
  CartesianGraph g = 
      (CartesianGraph) layout.getFirstLayer().getGraph();
  Axis a;
  try {
      a = g.getXAxis("Bottom Axis");    
      Range2D xr = a.getRangeP();   // get physical range of x-axis
      a = g.getYAxis("Left Axis");
      Range2D yr = a.getRangeP();   // get physical range of y-axis
    
      double x0P = xr.start;        // physical coordinates 
      double x1P = xr.end;          // of the domain
      double y0P = yr.start;
      double y1P = yr.end;
      
      // device coordinates of the domain
      int x0D = g.getXUtoD(g.getXPtoU( x0P )); 
      int x1D = g.getXUtoD(g.getXPtoU( x1P ));
      int y0D = g.getYUtoD(g.getYPtoU( y0P ));
      int y1D = g.getYUtoD(g.getYPtoU( y1P ));
      
      double sampling=(g.getXPtoU(x1P)-g.getXPtoU(x0P))/(x1D-x0D);
      
      // find out maximum frequency
      double maxfreq=0.0;
      if (mainframe.getFitTab().getFitMode()==XTabFit.BINARY_MODE) {
    final int totfreqs = Period04.projectGetBMTotalFrequencies();
    for (int i=0; i<totfreqs; i++) {
        if ( Period04.projectGetBMActive(i) )     {
      double tmp = Period04.projectGetBMFrequencyValue(i);
      if (tmp>maxfreq) { maxfreq=tmp; }
        }
    }
      } else { // NORMAL_MODE
    final int totfreqs = Period04.projectGetTotalFrequencies();
    for (int i=0;i<totfreqs;i++) {
        if ( Period04.projectGetActive(i) )     {
      double tmp = Period04.projectGetFrequencyValue(i);
      if (tmp>maxfreq) { maxfreq=tmp; }
        }
    }
      }
      if (maxfreq!=0.0) { maxfreq=1/maxfreq; }
      return ((useData<2) && (8*sampling<maxfreq));
  } catch (Exception e) {
      return false;
  }
    }

    //---
    //--- check wether plotting the active fit makes sense
    //--- and add the fit to the plot
    //---
    private void plotFit() {
  // find out if the predicted signal should be plotted
  isPlotFit = false;
  // only plot the fit if we are showing the appropriate data
  switch (useData) {
      case 0: if (!(mainframe.getFitTab().getLastCalculationID()==0)) { return; } break;
      case 1: if (!(mainframe.getFitTab().getLastCalculationID()==1)) { return; } break;
      default: return;
  }
  CartesianGraph g = 
      (CartesianGraph) layout.getFirstLayer().getGraph();
  Axis a;
  try {
      a = g.getXAxis("Bottom Axis");    
      Range2D xr = a.getRangeP();   // get physical range of x-axis
      a = g.getYAxis("Left Axis");
      Range2D yr = a.getRangeP();   // get physical range of y-axis
    
      double x0P = xr.start;        // physical coordinates 
      double x1P = xr.end;          // of the domain
      double y0P = yr.start;
      double y1P = yr.end;
      
      // device coordinates of the domain
      int x0D = g.getXUtoD(g.getXPtoU( x0P )); 
      int x1D = g.getXUtoD(g.getXPtoU( x1P ));
      int y0D = g.getYUtoD(g.getYPtoU( y0P ));
      int y1D = g.getYUtoD(g.getYPtoU( y1P ));
      
      double sampling=(g.getXPtoU(x1P)-g.getXPtoU(x0P))/(x1D-x0D);
      
      // find out maximum frequency
      double maxfreq=0.0;
      if (mainframe.getFitTab().getFitMode()==XTabFit.BINARY_MODE) {
    final int totfreqs = Period04.projectGetBMTotalFrequencies();
    for (int i=0; i<totfreqs; i++) {
        if ( Period04.projectGetBMActive(i) )     {
      double tmp = Period04.projectGetBMFrequencyValue(i);
      if (tmp>maxfreq) { maxfreq=tmp; }
        }
    }
      } else { // NORMAL_MODE
    final int totfreqs = Period04.projectGetTotalFrequencies();
    for (int i=0;i<totfreqs;i++) {
        if ( Period04.projectGetActive(i) )     {
      double tmp = Period04.projectGetFrequencyValue(i);
      if (tmp>maxfreq) { maxfreq=tmp; }
        }
    }
      }
      if (maxfreq!=0.0) { maxfreq=1/maxfreq; }
      if ((useData<2) && (8*sampling<maxfreq)) {
    double [] xArray = new double[x1D-x0D+1];
    double [] yArray = new double[x1D-x0D+1];
    if (mainframe.getFitTab().getFitMode()==XTabFit.BINARY_MODE) {
        for (int i=x0D; i<=x1D; i++) {
      Point2D.Double tmp = transformDtoU(i, y0D);
      double time = tmp.x; 
      xArray[i-x0D] = time;
      yArray[i-x0D] = Period04.projectPredictBMAmplitude(time, -1);
        }
    } else {
        for (int i=x0D; i<=x1D; i++) {
      Point2D.Double tmp = transformDtoU(i, y0D);
      double time = tmp.x; 
      xArray[i-x0D] = time;
      yArray[i-x0D] = Period04.projectPredictAmplitude(time, -1);
        }
    }
    SimpleLine data = new SimpleLine(xArray, yArray, "");
    data.setId("Fit");
    SGTMetaData meta = new SGTMetaData();
    data.setXMetaData(meta);
    boolean reverse = Period04.projectGetReverseScale();
    meta = new SGTMetaData("","", reverse, false);
    data.setYMetaData(meta);
    SGTData sgtdata = data;
    layout.addData(sgtdata, 
             new LineAttribute(LineAttribute.SOLID),
             "Fit", XPlotLayout.LINE);
    isPlotFit = true;
      }     
  } catch(Exception e) {
      System.out.println(e);
  }
    }

    public void replotFit() {
  //System.out.println("replotFit");/////////////////
  /*if (isPlotFit) {
      layout.clear("Fit");
      isPlotFit = false;
      }*/
  //--- shall we plot the fit?
  //--- if yes, then we have to save the viewport as
  //--- the ranges are changed by plotFit in order to
  //--- maintain the viewport
  layout.saveCertainViewport();
  layout.blockSaveViewport(true);
  plotFit();
  try { // set the old viewport again
      Domain dm = layout.getCertainViewport();
      dm.setYReversed(Period04.projectGetReverseScale());
      layout.setRange(dm); 
  } catch (Exception e) {}
  layout.blockSaveViewport(false);
    }

    public void replot(boolean rescaleY, boolean checkPlotFit, boolean resetZoom) {
    layout.setBatch(true);
    //if (resetZoom) {
        layout.clear();
        //isPlotFit=false; //////////
        layout.reset();               // reset the zoom rectangle
        //}
    readData();                   // read the data and add to layout
    if (checkPlotFit)
        plotFit(); 
    if (i_KeepViewport.isSelected()) { rescaleY=false; } // an exception
    layout.setViewport(rescaleY); // set another viewport?
    layout.setBatch(false);
    }

    public void displayHeader(boolean show) {
  if (show) {
      StringBuffer text = new StringBuffer();
      text.append("Fit: ");
      switch (useData) {
    case 0: text.append("Observed"); break;
    case 1: text.append("Adjusted"); break;
    case 2: text.append("Residuals (Obs)"); break;
    case 3: text.append("Residuals (Adj)"); break;
      }
      text.append(",  Weights: "+Period04.projectGetWeightString());
      if (mainframe.getFitTab().getFitMode()==XTabFit.NORMAL_MODE) {
    text.append(
        ",  #Freqs: "+Period04.projectGetActiveFrequencies()+
        ",  Residuals: "+Period04.projectGetResiduals());
      } else {
    text.append(
        ",  #Freqs: "+Period04.projectGetBMActiveFrequencies()+
        ",  Residuals: "+Period04.projectGetBMResiduals());
      }
      layout.setPlotTitle(text.toString());
  } else {
      layout.setPlotTitle("");
  }
    }

    //---
    //--- replot showing the selected attribute
    //---
    private void setUseAttribute(int i) {
  colorAttribute = i;
  blockPropertyChanges = true;
  resetZoom();
  boolean oldvalue = layout.setMaintainViewport(true);
  replot(true, false, true);
  layout.setMaintainViewport(oldvalue);//false);
  blockPropertyChanges = false;
  replotFit();
    }

    //---
    //--- replot using the appropriate data
    //---
    private void setUseData(int i) {
  useData = i;
  blockPropertyChanges = true;
  resetZoom();
  boolean oldvalue = layout.setMaintainViewport(true);
  replot(true, false, true);
  layout.setMaintainViewport(oldvalue);//false);
  blockPropertyChanges = false;
  replotFit();
   }

    //---
    //--- reset the zoom and turn off clipping.
    //---
    protected void resetZoom() {
  layout.setBatch(true);
  layout.resetZoom();
  layout.setClipping(false);
  layout.setBatch(false);
    }

    //---
    //--- undo zooming action
    //---
    private void undoZoom() {
  layout.undoZoom();
  replotFit();
    }

    public void setMaintainScale(boolean value) {
  layout.setMaintainViewport(value);
  layout.saveViewport();
    }

    //--- 
    //--- this method is called everytime when a zooming action occurs
    //--- it removes the fit and checks wether it makes sense to replot the fit
    //---
    public void propertyChange(PropertyChangeEvent evt) {
  if (blockPropertyChanges) { return; }
  if (isPlotFit) {
      layout.clear("Fit");
      isPlotFit = false;
  }
  plotFit();
  //--- if the ranges should be kept, save the new viewport
  if (i_KeepViewport.isSelected()) { layout.saveViewport(); }
    }

    public XPlotLayout getPlotLayout() {
  return layout;
    }


    //---
    //--- methods to listen to ComponentEvents 
    //--- (we are listening to ComponentEvents that are caused by resizing 
    //---  the plotframe, this has become necessary to remove a SGT bug)
    //---
    public void componentResized(ComponentEvent e) {
    Range2D xrng = getDomainXRangeD();
    PlotMark.setDeviceXRange((int)xrng.start,(int)xrng.end);
    super.componentResized(e);
    }
    public void componentHidden(ComponentEvent e) {}
    public void componentShown(ComponentEvent e)  {}
    public void componentMoved(ComponentEvent e)  {}


    //---
    //--- MyMouseInputAdapter
    //---
    class MyMouseInputAdapter extends MouseInputAdapter 
  implements ComponentListener
    {

  private double offset      = 0;
  private double maxDistance = 100;

  private Rectangle zoom_rect_ = new Rectangle(0, 0, 0, 0);
  private Point zoom_start_ = new Point(0, 0);
  private boolean in_zoom_ = false;
  private boolean dragged_ = false;

  public void mouseClicked (MouseEvent evt) {
      // check wether the right mouse button has been pressed
      if (!SwingUtilities.isRightMouseButton(evt)) { return; } 
      in_zoom_ = false;

      int xD = evt.getX();      // of the point where the 
      int yD = evt.getY();      // MouseEvent has been detected
      
      // transform into user coord.
      Point2D.Double ptU = transformDtoU(xD, yD);
      
      CartesianGraph g = 
    (CartesianGraph) layout.getFirstLayer().getGraph();
      double x_start=0, x_end=0;
      try {
    Axis a = g.getXAxis("Bottom Axis");    
    Range2D xr = a.getRangeP();   // get physical range of x-axis
    
    double x0P = xr.start;        // physical coordinates 
    double x1P = xr.end;          // of the domain
    
    x_start = g.getXPtoU(x0P);
    x_end   = g.getXPtoU(x1P);
    offset = x_end-x_start/100.0;
      } catch (AxisNotFoundException e) {
    System.err.println(e); return;
      }     
      //---
      //--- find a point
      //---
      int pointFound = -1;
      final int selpoints = Period04.projectGetSelectedPoints();
      for (int i=0; i<selpoints; i++) {
    // get coordinates
    double amplitude, time = Period04.projectTimePointGetTime(i);
    // only  plot if data is within range
    if (time>x_end+offset) { break;}  // time is higher?
    if (time>x_start-offset) {           // time is on screen 
        amplitude=Period04.projectTimePointGetAmplitude(i,useData);
        // get device coordinates [pixel]
        double x = g.getXUtoD(time);
        double y = g.getYUtoD(amplitude);
        // calculate difference
        x = x-xD;
        y = y-yD;
        // calculate distance
        double distance = x*x+y*y;
        // are we close enough to a point
        if (distance<maxDistance) {
      // is there already another point within our range?
      if (pointFound!=-1) { return; }   // yes, so stop
      pointFound = i;
        }
    }
      }
      if (pointFound==-1) { return; }  // no point found
      // start the appropriate dialog
      
      if (evt.isControlDown()) {
    layout.saveViewport();
    Toolkit.getDefaultToolkit().beep();
    String protocol = Period04.projectDeletePoint(pointFound);
    blockPropertyChanges=true;
    boolean oldvalue = layout.setMaintainViewport(true);
    replot(false, false, true);
    layout.setMaintainViewport(oldvalue);//false);
    blockPropertyChanges=false;
    replotFit(); // check wether to plot the fit or not
    mainframe.updateTSDisplay();
    if (!protocol.equals("")) { 
        mainframe.getLog().writeProtocol(protocol, true);
    }
      } else {
    layout.saveViewport();
    XDialogRelabelPoint dialog = new XDialogRelabelPoint(
        XPlotTimeString.this, useData, pointFound);
    if (dialog.isCanceled()) { return; }
    mainframe.updateTSDisplay();
    blockPropertyChanges=true;
    boolean oldvalue = layout.setMaintainViewport(true);
    replot(false, false, true);
    layout.setMaintainViewport(oldvalue);//false);
    blockPropertyChanges=false;
    replotFit(); // check wether to plot the fit or not
    // protocol has already been written of the dialog
      }
  }

  public void mousePressed (MouseEvent evt) {
      // only for the right mouse button
      if (!SwingUtilities.isRightMouseButton(evt)) { return; } 
      dragged_ = false;
      in_zoom_ = true;
      zoom_start_.x = evt.getX();
      zoom_start_.y = evt.getY();
      zoom_rect_.x  = evt.getX();
      zoom_rect_.y  = evt.getY();
      zoom_rect_.width  = 1;
      zoom_rect_.height = 1;
      Graphics g = layout.getGraphics();
      g.setColor(Color.red);//layout.getForeground());
      g.setXORMode(layout.getBackground());
      g.drawRect(zoom_rect_.x, zoom_rect_.y,
           zoom_rect_.width, zoom_rect_.height);
      g.setPaintMode();
  }
  
  public void mouseReleased ( MouseEvent evt)
  {
    // only for the right mouse button
    if (! SwingUtilities.isRightMouseButton( evt)) { return; } 

    if (evt.isControlDown( )) { return; }
    
    if (in_zoom_)
    {
      // finish zoom
      in_zoom_ = false;
      Graphics g = layout.getGraphics( );
      g.setColor(Color.red);
        g.setXORMode(Color.red);//layout.getBackground());
      g.drawRect(zoom_rect_.x, zoom_rect_.y,
          zoom_rect_.width, zoom_rect_.height);
    }

    if (! dragged_) { return; }

    //---
    //--- do we really want to delete these points
    //---
    String [] options = { "Yes", "No"};

    JOptionPane jop = new JOptionPane
      ( "Do you really want to delete the points within the selected box?"
      , JOptionPane.QUESTION_MESSAGE
      , JOptionPane.YES_NO_OPTION
      , null
      , options
      , options[1]
      )
    ;

    jop.setInitialSelectionValue(new Integer(JOptionPane.NO_OPTION));
    JDialog dlg = jop.createDialog(XPlotTimeString.this, 
            "Delete points?");
    dlg.addComponentListener(this);
    dlg.show();
    Object value = jop.getValue();

    if (value == null || value == "No")
    {
      layout.saveViewport();
      blockPropertyChanges=true;
      boolean oldvalue = layout.setMaintainViewport(true);
      replot(false, false, true);
      blockPropertyChanges=false;
      layout.setMaintainViewport(oldvalue);
      replotFit();
      return;
    }

    if (value=="Yes") // ok, delete the points within the rectangle
    {
      // translate to User coordinates
      Point2D.Double pt0 = transformDtoU(zoom_start_.x, zoom_start_.y);   Point2D.Double pt1 = transformDtoU(evt.getX(), evt.getY());

      // put the coordinates in right order
      if (pt0.x > pt1.x)
      { 
          double tmp = pt0.x; pt0.x = pt1.x; pt1.x = tmp; 
      } 

      if (pt0.y > pt1.y)
      { 
          double tmp = pt0.y; pt0.y = pt1.y; pt1.y = tmp; 
      }

      String protocol = Period04.projectDeletePointsWithinBox( pt0.x, pt0.y, pt1.x, pt1.y, useData);
  
      if (protocol.equals("")) { return; } // no point has been found
      Toolkit.getDefaultToolkit().beep(); 
      mainframe.getLog().writeProtocol(protocol, true);

      layout.saveViewport();
      blockPropertyChanges=true;
      boolean oldvalue = layout.setMaintainViewport(true);
      replot(false, false, true);
      layout.setMaintainViewport(oldvalue);//false);
      blockPropertyChanges=false;
      replotFit();
      mainframe.updateTSDisplay();
    }
  }

  public void mouseEntered(MouseEvent evt) {}
  public void mouseExited (MouseEvent evt) {}

  public void mouseDragged(MouseEvent evt) {
      if(in_zoom_) {
    Graphics g = layout.getGraphics();
    g.setColor(Color.red);//layout.getForeground());
    g.setXORMode(layout.getBackground());
    g.drawRect(zoom_rect_.x, zoom_rect_.y,
         zoom_rect_.width, zoom_rect_.height);
    zoom_rect_.width = evt.getX() - zoom_start_.x;
    if(zoom_rect_.width < 0) {
        zoom_rect_.x = evt.getX();
        zoom_rect_.width = Math.abs(zoom_rect_.width);
    } else {
        zoom_rect_.x = zoom_start_.x;
    }
    zoom_rect_.height = evt.getY() - zoom_start_.y;
    if(zoom_rect_.height < 0) {
    zoom_rect_.y = evt.getY();
    zoom_rect_.height = Math.abs(zoom_rect_.height);
    } else {
        zoom_rect_.y = zoom_start_.y;
    }
    g.drawRect(zoom_rect_.x, zoom_rect_.y,
         zoom_rect_.width, zoom_rect_.height);
    g.setPaintMode();
      } 
      dragged_ = true;
  }

  public void mouseMoved(MouseEvent evt) {
      //--- if we reached maximum zoom show a message!
      if (layout.isMaximumZoom()) { 
    statusBar.setText(maxZoomText);
    return;
      }

      int xD = evt.getX();      // position of the point where the 
      int yD = evt.getY();      // MouseEvent has been detected

      // transform into user coord.
      Point2D.Double ptU = transformDtoU(xD, yD);

      // display message
      if (isWithinDomain()) {
    statusBar.setText(
        " (x,y) = (" + format(ptU.x) + ", " + format(ptU.y) + ")");
      } else {
    statusBar.setText(stdtext);
      }
  }

  //---
  //--- methods to listen to ComponentEvents caused by moving the 
  //--- delete-points-dialog
  //--- it is neccessary to redraw the red delete-points-rectangle
  //--- as dialog movements make it disappear 
  //---
  public void componentResized(ComponentEvent e) {}
  public void componentHidden(ComponentEvent e) {}
  public void componentShown(ComponentEvent e)  {}
  public void componentMoved(ComponentEvent e)  {
      Graphics g = layout.getGraphics();
      g.setColor(Color.red);
      g.drawRect(zoom_rect_.x, zoom_rect_.y,
           zoom_rect_.width, zoom_rect_.height);
  }


    }

}
