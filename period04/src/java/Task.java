/*--------------------------------------------------------------------*
 *  Task
 *  a superclass for calculation tasks
 *--------------------------------------------------------------------*/

public class Task {

    protected int         current  = 0;
    protected boolean     done     = false;
    protected boolean     canceled = false;
    protected SwingWorker worker;

    public void    stop()       { canceled = true; } // stop task
    public boolean isCanceled() { return canceled; } // canceled?
    public boolean isDone()     { return done;     } // ready?

}
