/*----------------------------------------------------------------------*
 *  XDialogRelabelPoint
 *  a dialog to relabel a timestring point
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.*;                  // Property change stuff
import javax.swing.*;
import javax.swing.border.*;

public class XDialogRelabelPoint extends XDialog 
    implements ActionListener
{
    private static final long serialVersionUID = -5506952027087696401L;
    private JFrame        parentframe;
    private XProjectFrame mainframe;
    private JOptionPane   optionPane;
    private final String  s_OK = "OK";
    private final String  s_Cancel = "Cancel";
    private boolean canceled = false;

    public XDialogRelabelPoint(JFrame mframe, 
			       int useData, final int pointIndex) {

	super(mframe, true);  // keep dialog in front
	this.parentframe = (JFrame)mframe;
	if (mframe instanceof XPlotTimeString) {
	    this.mainframe = ((XPlotTimeString)mframe).mainframe;
	} else {
	    this.mainframe = ((XTSTable)mframe).mainframe;
	}
        Object[] options = {s_OK, s_Cancel};

	JPanel inputpanel = new JPanel();
	inputpanel.setLayout(null);
	JLabel l_time       = createLabel("Time:");
	JLabel l_time_r     = createLabel(Double.toString(
	    Period04.projectTimePointGetTime(pointIndex)));
	JLabel l_amplitude  = createLabel("Amplitude:");
	JLabel l_amplitude_r= createLabel(Double.toString(
	    Period04.projectTimePointGetAmplitude(pointIndex, useData)));

	JLabel [] l_attribute = new JLabel[4];
	final JTextField [] t_attribute = new JTextField[4];
	for (int i=0; i<4; i++) {
	    l_attribute[i] = createLabel(Period04.projectGetNameSet(i));
	    t_attribute[i] = createTextField(
		Period04.projectGetIDNameStr(
		    Period04.projectTimeStringGetIDName(pointIndex, i),i));
	}

	l_time       .setBounds( 10,  5, 60, 20 );
	l_time_r     .setBounds(130,  5,150, 20 );
	l_amplitude  .setBounds( 10, 25, 60, 20 );
	l_amplitude_r.setBounds(130, 25,150, 20 );
	for (int i=0; i<4; i++) {
	    l_attribute[i].setBounds(  10, 60+i*20, 150, 20 );
	    t_attribute[i].setBounds( 130, 60+i*20, 150, 20 );
	    inputpanel.add( l_attribute[i], null );
	    inputpanel.add( t_attribute[i], null );
	}

	inputpanel.add( l_time, null );
	inputpanel.add( l_time_r, null );
	inputpanel.add( l_amplitude, null );
	inputpanel.add( l_amplitude_r, null );
	
	Object array = inputpanel;

        optionPane = new JOptionPane(array, 
                                    JOptionPane.PLAIN_MESSAGE,
                                    JOptionPane.YES_NO_OPTION,
                                    null, options, options[0]);
        setContentPane(optionPane);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent we) {
                /*
                 * Instead of directly closing the window,
                 * we're going to change the JOptionPane's
                 * value property.
                 */
                    optionPane.setValue(
			new Integer(JOptionPane.CLOSED_OPTION));
            }
        });

	optionPane.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
                String prop = e.getPropertyName();

                if (isVisible() 
                 && (e.getSource() == optionPane)
                 && (prop.equals(JOptionPane.VALUE_PROPERTY) ||
                     prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
                    Object value = optionPane.getValue();

                    if (value == JOptionPane.UNINITIALIZED_VALUE) {
                        //ignore reset
                        return;
                    }

                    // Reset the JOptionPane's value.
                    // If you don't do this, then if the user
                    // presses the same button next time, no
                    // property change event will be fired.
                    optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

                    if (value.equals(s_OK)) {
			for (int i=0; i<4; i++) {
			    if (t_attribute[i].getText().equals("")) {
				JOptionPane.showMessageDialog(
				    XDialogRelabelPoint.this,
				    "Please put a valid name into field \""+
				    Period04.projectGetNameSet(i)+"\"!",
				    "Invalid input",
				    JOptionPane.ERROR_MESSAGE);
				return;
			    }
			}
			String protocol = Period04.projectRelabelPoint(
			    pointIndex, t_attribute[0].getText(),
			    t_attribute[1].getText(), t_attribute[2].getText(),
			    t_attribute[3].getText());		
			mainframe.getLog().writeProtocol(
			    protocol, true);
			
			setVisible(false);
		    } 
		    else { // user closed dialog or clicked cancel
			canceled = true;
			setVisible(false);
		    }
                }
            }
        });

	this.setTitle("Relabel data point:");
	this.setSize(320,250);
	this.setResizable(false);
	this.setLocationRelativeTo(parentframe);
	this.setVisible(true);
    }

    public boolean isCanceled() {
	return canceled;
    }

    public void actionPerformed(ActionEvent evt) {
	optionPane.setValue(s_OK);
    }
}
