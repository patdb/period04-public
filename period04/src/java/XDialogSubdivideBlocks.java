/*----------------------------------------------------------------------*
 *  XDialogSubdivideBlocks
 *  a dialog to subdivide the timestring
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.*;                  // Property change stuff
import javax.swing.*;
import javax.swing.event.*;

class XDialogSubdivideBlocks extends XDialog 
{
    private static final long serialVersionUID = -3661048864160064912L;
    private JOptionPane     optionPane;
    private final String    s_OK = "OK", s_Cancel = "Cancel";
    
    private JTextField t_start, t_block, t_prefix;
    private JCheckBox  c_useCounter;
    private JComboBox  cb_attribute, cb_digits;   
    
    private XProjectFrame mainframe;
    private XTabTimeString tabTS;
    
    public XDialogSubdivideBlocks(XProjectFrame frame, XTabTimeString tab) {
	    super(frame.getFrame(), true); // necessary to keep dialog in front
	    mainframe = frame;
	    tabTS = tab;
	    Object[] options = {s_OK, s_Cancel}; // set button names
	    
	    JPanel inputpanel = new JPanel();
	    inputpanel.setLayout(null);

	    JLabel l_attribute = createLabel("<html>Choose the attribute<br>you want to subdivide:</html>");
	    JLabel l_start = createLabel("Start time:");
	    JLabel l_block = createLabel("Time interval:");
	    JLabel l_prefix = createLabel("Label prefix:");
	    JLabel l_digits = createLabel("Decimal places to use from time:");

	    t_start = createTextFieldNoAction(Period04.projectGetStartTime());
	    t_block = createTextFieldNoAction("10");
	    t_prefix = createTextFieldNoAction("JD");
	    c_useCounter = new JCheckBox("Use running counter");
	    String [] s_attributes = new String[4];
	    for (int i=0; i<4; i++) {
		s_attributes[i] = Period04.projectGetNameSet(i);
	    }	    cb_attribute = new JComboBox(s_attributes);
	    String [] s_digits = new String[11];
	    for (int i=0; i<11; i++) {
		s_digits[i] = Integer.toString(i);
	    }
	    cb_digits = new JComboBox(s_digits);
	    cb_digits.setSelectedIndex(2);

	    l_attribute .setBounds(  10,   0, 160, 40);
	    cb_attribute.setBounds( 170,  10, 150, 20);
	    l_start     .setBounds(  10,  40, 160, 20);
	    t_start     .setBounds( 170,  40, 150, 20);
	    l_block     .setBounds(  10,  70, 160, 20);
	    t_block     .setBounds( 170,  70, 150, 20);
	    l_prefix    .setBounds(  10, 105, 160, 20);
	    t_prefix    .setBounds( 170, 105, 150, 20);
	    c_useCounter.setBounds(  10, 130, 200, 20);
	    l_digits    .setBounds(  10, 155, 230, 20);
	    cb_digits   .setBounds( 240, 155,  80, 20);
		
	    inputpanel.add(l_start);
	    inputpanel.add(t_start);
	    inputpanel.add(l_block);
	    inputpanel.add(t_block);
	    inputpanel.add(l_attribute);
	    inputpanel.add(cb_attribute);
	    inputpanel.add(l_prefix);
	    inputpanel.add(t_prefix);
	    inputpanel.add(c_useCounter);
	    inputpanel.add(l_digits);
	    inputpanel.add(cb_digits);

	    optionPane = new JOptionPane((Object)inputpanel, 
					JOptionPane.PLAIN_MESSAGE,  
					JOptionPane.YES_NO_OPTION,
					 null, options, options[0]);
	    setContentPane(optionPane);
	    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	    addWindowListener(new WindowAdapter() {
		    public void windowClosing(WindowEvent we) {
			/*
			 * Instead of directly closing the window,
			 * we're going to change the JOptionPane's
			 * value property.
			 */
			optionPane.setValue(
			    new Integer(JOptionPane.CLOSED_OPTION));
		    }
		});
	    
	    optionPane.addPropertyChangeListener(new PropertyChangeListener() {
		    public void propertyChange(PropertyChangeEvent e) {
			String prop = e.getPropertyName();
			
			if (isVisible() 
			    && (e.getSource() == optionPane)
			    && (prop.equals(JOptionPane.VALUE_PROPERTY) ||
				prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
			    Object value = optionPane.getValue();
			    
			    if (value == JOptionPane.UNINITIALIZED_VALUE) {
				//ignore reset
				return;
			    }
			    

			    // Reset the JOptionPane's value.
			    // If you don't do this, then if the user
			    // presses the same button next time, no
			    // property change event will be fired.
			    optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

			    if (value.equals(s_OK)) {
				String s_start = t_start.getText();
				String s_block = t_block.getText();
				
				// check if it is a correct number
				NumberFormatException nan = null;
				try { 
				    Double.parseDouble(s_start); 
				} catch (NumberFormatException exception) { 
				    nan=exception; 
				} 	   
				if (s_start.equals("") || (nan!=null)) {
				    // text was invalid
				    t_start.selectAll();
				    JOptionPane.showMessageDialog(
					XDialogSubdivideBlocks.this,
					"Sorry, \"" + s_start + "\" "
					+ "isn't a valid number.\n",
					"Try again",
					JOptionPane.ERROR_MESSAGE);
				    s_start = null;
				} else {
				    if (Period04.projectGetSelectedPoints()==0) {
					JOptionPane.showMessageDialog(
					    mainframe.getFrame(),
					    "There are no selected points!",
					    "Error",JOptionPane.ERROR_MESSAGE);
					return;
				    }

				    double start=Double.parseDouble(s_start);
					double interval = Double.parseDouble(s_block);
				    int attribute=cb_attribute.getSelectedIndex();
				    boolean usecount=c_useCounter.isSelected();
				    String prefix=t_prefix.getText();
				    int digits=cb_digits.getSelectedIndex();
				    // now go and do it
				    Period04.projectSubdivide(
					start, interval, attribute, usecount, 
					prefix, digits);
				    // hide dialog
				    setVisible(false);
				    // write to protocol
				    StringBuffer text = new StringBuffer();
				    text.append(
					"Subdivided the current selection\n"+
					"In column:                "+attribute+
					"\nBy time intervals of:     "+s_block+
					"\nusing the prefix:         "+prefix+
					"\nand displaying "+digits+
					" decimal places\n");
				    if (usecount) {
					text.append("with a running counter.\n");
				    } else {
					text.append("with time.\n");
				    }
				    mainframe.getLog().writeProtocol(text.toString(), true);
				    // now update the list, 
				    // it's necessary to prevent the selection
				    // listener from trying to set the selection
				    // (the list items are already selected)
				    // otherwise a nullpointerexception occurs!
				    tabTS.blockSelectionEvent=true;
				    tabTS.updateList(attribute);
				    tabTS.blockSelectionEvent=false;
				}
			    } else {
				setVisible(false);
			    }
			}
		    }
		});
	    
	    this.setTitle("Subdivide timestring:");
	    this.setSize(360,270);
	    this.setResizable(false);
	    this.setLocationRelativeTo(mainframe.getFrame());
	    this.setVisible(true);
	}

	public void actionPerformed(ActionEvent evt) {
	    optionPane.setValue(s_OK);
	}

    }

