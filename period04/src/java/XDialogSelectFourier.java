/*----------------------------------------------------------------------*
 *  XDialogSelectFourier
 *  a dialog 
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.*;      //Property change stuff 
import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.event.*;


class XDialogSelectFourier extends JDialog 
    implements ActionListener
{
    private static final long serialVersionUID = -4236336114593931785L;
    private JOptionPane      optionPane;
    private boolean          canceled = false;
    private JPanel           listPanel;
    private JList            fourierList;
    private DefaultListModel fourierListModel;
    private JScrollPane      fourierListPane;

    public XDialogSelectFourier(JFrame mainframe) {    
        super(mainframe, true);

	listPanel = new JPanel();	 // create the panel
	
	//--- create the fourier list
	fourierListModel = new DefaultListModel();
	fourierList = new JList(fourierListModel);
	fourierList.setFont(new Font("Dialog", Font.PLAIN, 11));
	MouseInputListener mil = new DragSelectionListener();
	fourierList.addMouseMotionListener(mil);
	fourierList.addMouseListener(mil);
	fourierListPane = new JScrollPane(fourierList,
	    JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
	    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	fourierListPane.setBorder(BorderFactory.createEtchedBorder(
				  EtchedBorder.RAISED));
	fourierListPane.setBounds( 10, 10, 350, 250);
	listPanel.add(fourierListPane, null);
	updateList();

	createDialog( "Please choose the spectra you want to export:",
		      listPanel );

	this.setTitle("Please enter file format");
	this.pack();
	this.setResizable(false);
	this.setLocationRelativeTo(mainframe);
	this.setVisible(true);
    }

    public void updateList() {
	fourierListModel.clear();
	int entries = Period04.projectGetFourierEntries();
	if (entries==0) {
	    return;
	}
	for (int i=0; i<entries; i++) {
	    String [] peak = Period04.fourierGetPeak(i);
	    fourierListModel.add(
		i, Period04.projectGetFourierTitle(i)+
		" ( F="+peak[0]+", A="+peak[1]+" )");
	}
    }

    private void createDialog (Object message, Object content){
	// add message and listPanel to array
        Object[] array = {message, content};
	Object[] options = {"OK", "Cancel"};

        optionPane = new JOptionPane(array, JOptionPane.PLAIN_MESSAGE,
                                            JOptionPane.YES_NO_OPTION,
                                            null, options, options[0]);
	optionPane.setFont(new Font("Dialog", Font.PLAIN, 11));
        setContentPane(optionPane);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent we) {
                /*
                 * Instead of directly closing the window,
                 * we're going to change the JOptionPane's
                 * value property.
                 */
                    optionPane.setValue(
			new Integer(JOptionPane.CLOSED_OPTION));
            }
        });

	optionPane.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
                String prop = e.getPropertyName();

                if (isVisible() 
                 && (e.getSource() == optionPane)
                 && (prop.equals(JOptionPane.VALUE_PROPERTY) ||
                     prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
                    Object answer = optionPane.getValue();

                    if (answer == JOptionPane.UNINITIALIZED_VALUE) {
                        //ignore reset
                        return;
                    }

                    // Reset the JOptionPane's value.
                    // If you don't do this, then if the user
                    // presses the same button next time, no
                    // property change event will be fired.
                    optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

                    if (answer.equals("OK")) {
			setVisible(false);
		    } else {   
			// user closed dialog or clicked cancel
			canceled = true;
			setVisible(false);
		    }
                }
            }
        });
    }

    public int [] getSelectedIndices() {
	return fourierList.getSelectedIndices();
    }

    public boolean isCanceled() { return canceled; }

    public void actionPerformed(ActionEvent evt) {
	Object s = evt.getSource();

	// in case of OK or CANCEL:
	optionPane.setValue("OK");
    }
}
