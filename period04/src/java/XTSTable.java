/*---------------------------------------------------------------------*
 *  XTSTable.java
 *  displays a table of timestring data
 *---------------------------------------------------------------------*/

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import java.util.StringTokenizer;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.filechooser.*;
import javax.swing.table.*;

import java.awt.print.PrinterJob;
import java.awt.print.PrinterException;


class XTSTable extends JFrame
{
    public  XProjectFrame mainframe;
    private JPanel listPanel;
    private JMenuBar  menuBar = new JMenuBar();
    private JMenu     menuFile, menuHelp;
    private JMenuItem i_Print, i_Export, i_Close;
    private JFileChooser fc;
    private JPrintableTable table;
    private MyTableModel model;

    // popup menu
    private JPopupMenu popup;
    private MouseEvent popupEvent;
    private JMenuItem  iPop_Delete, iPop_Relabel;

    public XTSTable(XProjectFrame mframe) {
	mainframe = mframe;

	// create main-menu
	menuFile = XMenuHelper.addMenuBarItem( menuBar, "Table" );
	menuHelp = XMenuHelper.addMenuBarItem( menuBar, "Help" );

	// create action listener for menu items
	ActionListener actionProcCmd = new ActionListener() {
		public void actionPerformed( ActionEvent evt ) {
		    Object s=evt.getSource();
		    if      (s==i_Print)    { printTSTable();  return; }
		    else if (s==i_Export)   { exportTSTable(); return; }
		    else if (s==i_Close)    { dispose();       return; }
		    //--- popup menu
		    if (s==iPop_Relabel) {
			Object popupsource = popupEvent.getSource();
			int index = table.rowAtPoint(popupEvent.getPoint());
			if (index!=-1) {
			    XDialogRelabelPoint dialog=new XDialogRelabelPoint(
				XTSTable.this, 0/*Observed*/, index);
			    if (dialog.isCanceled()) { return; }
			    Toolkit.getDefaultToolkit().beep();	
			    model.updateData(); // update table
			    mainframe.updateTSDisplay();
			    table.repaint();
			    // protocol has already been written of the dialog
			}
		    }
		    if (s==iPop_Delete) {
			Object popupsource = popupEvent.getSource();
			int index = table.rowAtPoint(popupEvent.getPoint());
			if (index!=-1) {
			    String protocol=Period04.projectDeletePoint(index);
			    Toolkit.getDefaultToolkit().beep();	
			    model.updateData(); // update table
			    mainframe.updateTSDisplay();
			    mainframe.getLog().writeProtocol(protocol, true);
			    table.repaint();
			}
		    }
		}
	    };

	// file menu	
	i_Print = XMenuHelper.addMenuItem( 
	    menuFile, "Print Table", actionProcCmd );
	i_Export = XMenuHelper.addMenuItem( 
	    menuFile, "Export Table", actionProcCmd );
	i_Close = XMenuHelper.addMenuItem( 
	    menuFile, "Close", actionProcCmd );
	this.setJMenuBar( menuBar );

	// help menu

        model = new MyTableModel();
        table = new JPrintableTable(model);

	JTableHeader header = table.getTableHeader();
	header.setReorderingAllowed(false);
	header.setBackground(new Color(230,225,217));
	header.setFont(new Font("Dialog", Font.BOLD, 11));

	// set column width
	table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	TableColumnModel colModel = table.getColumnModel();
	for (int i=0; i<table.getColumnCount(); i++) {
	    colModel.getColumn(i).setPreferredWidth(120);
	}

 	table.setShowHorizontalLines(true);
	table.setFont(new Font("Dialog", Font.PLAIN, 11));
        table.setPreferredScrollableViewportSize(new Dimension(120*4,410) );
	table.setBounds( 0, 18, 120*table.getColumnCount(), 
			 table.getRowHeight()*table.getRowCount()+30 );

        //Create the scroll pane and add the table to it. 
        JScrollPane scrollPane = 
	    new JScrollPane(table,
			    JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, 
			    JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

	// create a popupmenu for the table
	createPopupMenu();
	iPop_Relabel.addActionListener(actionProcCmd);
	iPop_Delete.addActionListener(actionProcCmd);

        //Add the scroll pane to this window.
        this.getContentPane().add(scrollPane, null);

	this.setTitle("Time String Table");
	this.setSize(500,410);
	this.setLocationRelativeTo(mainframe.getFrame());
        this.setVisible(true);
    }

    private void exportTSTable() {
	// start export dialog
	XDialogTSFileFormat dialog = new XDialogTSFileFormat(
	    null,Period04.timestringGetOutputFormat(),
	    "",false,false,false);
	// assign new output-format
	String outputformat = dialog.getFileFormat(); 
	boolean votable = dialog.getUseVOTableFormat();
	if (outputformat==null) { return; } // if canceled
	initializeFileChooser();
	int returnVal = fc.showDialog(null, "Export");
	if (returnVal == JFileChooser.APPROVE_OPTION) {
	    File file = fc.getSelectedFile();
	    Period04.setCurrentDirectory(file);
	    // check if the extension is missing
	    String path = file.getPath();
	    if (!path.endsWith(".dat")) { // add extension
		path += ".dat";
	    }
	    // check wether user wants to overwrite an existing file
	    if (file.exists()) {
		int v = JOptionPane.showConfirmDialog(
		    null, "File does already exist!\n"+
		    "Do you really want to delete the old file?", 
		    "Warning", JOptionPane.YES_NO_OPTION);
		if (v == JOptionPane.NO_OPTION) {
		    return;
		} else {
		    JOptionPane.showMessageDialog(
			null, "The project has been saved in file\n"+
			path, "", JOptionPane.INFORMATION_MESSAGE);
		    // go on and save it
		}
	    }
	    // now go and save it
	    Period04.projectSaveTimeString(path, outputformat,votable);
	} 
    }
    
    private void printTSTable() {
	PrinterJob printJob = PrinterJob.getPrinterJob();
	printJob.setPrintable(table);
	if ( printJob.printDialog() ) {
	    try { printJob.print(); }
	    catch ( PrinterException pe ) { }
	}	
    }
   
   
  private void initializeFileChooser ( )
  {
    // create FileChooser
    fc = new JFileChooser( Period04.getCurrentDirectory( ));
    fc.setFileFilter( new TimestringFileFilter( ));
//    fc.addChoosableFileFilter( new TimestringFileFilter( ));
  }

    public void createPopupMenu() {
	popup = new JPopupMenu();
	iPop_Relabel = new JMenuItem("Relabel point");
	iPop_Delete = new JMenuItem("Delete point");
        popup.add(iPop_Relabel);
        popup.add(iPop_Delete);
	MouseListener popupListener = new PopupListener(popup);
	for (int i=0; i<4; i++) {
	    table.addMouseListener(popupListener);
	}
    }

    class MyTableModel extends AbstractTableModel {

        final Object[]   columnNames = Period04.tstableGetHeader();
 	final int        columns = columnNames.length;
	int        rows = Period04.projectGetSelectedPoints();
	Object[][] data = new Object[rows][columns];

	public MyTableModel() { 
	    readData(); 
	}

        public int getColumnCount() { return columns; }
	public int getRowCount()    { return rows; }

        public String getColumnName(int col) {
            return columnNames[col].toString();
        }

        public Object getValueAt(int row, int col) {
            return data[row][col];
        }
	public void setValueAt(int row, int col, Object value) {
	    data[row][col] = value;
	}

        public boolean isCellEditable(int row, int col) {
	    return false;
        }

	public void updateData() {
	    rows = Period04.projectGetSelectedPoints();
	    data = new Object[rows][columns];
	    readData();
	}

	public void readData() {
	    // get the data
	    for (int j=0; j<columns;j++) {	    
		Object [] datacolumn = Period04.projectGetData(j);	
		for (int i=0; i<rows; i++) {
		    this.data[i][j] = datacolumn[i];
		}
	    }	
	}
    }

    //---
    //--- for the attributelist-popup-menu:
    //---
    class PopupListener extends MouseAdapter {
        JPopupMenu popup;

        PopupListener(JPopupMenu popupMenu)       { popup = popupMenu; }
        public  void mousePressed(MouseEvent e)   { 
	    int index = table.rowAtPoint(e.getPoint());
	    if (index>0) {
		table.setRowSelectionInterval(index,index);
	    }
	    maybeShowPopup(e); 
	}
        public  void mouseReleased(MouseEvent e)  { maybeShowPopup(e); }
        private void maybeShowPopup(MouseEvent e) {
            if (e.isPopupTrigger()) {
                popup.show(e.getComponent(), e.getX(), e.getY());
		popupEvent = e;
            }
        }
    }  
}


