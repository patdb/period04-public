/*----------------------------------------------------------------------*
 *  XDialogSubdivideData
 *  a dialog to subdivide the timestring
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.*;                  // Property change stuff
import javax.swing.*;
import javax.swing.event.*;

class XDialogSubdivideGaps extends XDialog 
{
    private static final long serialVersionUID = 786290505925615162L;
    private JOptionPane     optionPane;
    private final String    s_OK = "OK", s_Cancel = "Cancel";
    
    private JTextField t_gapsize, t_prefix;
    private JCheckBox  c_useCounter;
    private JComboBox  cb_attribute, cb_digits;   
    
    private XProjectFrame mainframe;
    private XTabTimeString tabTS;
    
    public XDialogSubdivideGaps(XProjectFrame frame, XTabTimeString tab) {
	    super(frame.getFrame(), true); // necessary to keep dialog in front
	    mainframe = frame;
	    tabTS = tab;
	    Object[] options = {s_OK, s_Cancel}; // set button names
	    
	    JPanel inputpanel = new JPanel();
	    inputpanel.setLayout(null);

	    JLabel l_gapsize = createLabel("Size of the gap:");
	    JLabel l_attribute = createLabel(
		"<html>Choose the attribute<br>you want to subdivide:</html>");
	    JLabel l_prefix = createLabel("Label prefix:");
	    JLabel l_digits = createLabel("Decimal places to use from time:");

	    t_gapsize = createTextFieldNoAction("0.5");
	    t_prefix = createTextFieldNoAction("JD");
	    c_useCounter = new JCheckBox("Use running counter");
	    String [] s_attributes = new String[4];
	    for (int i=0; i<4; i++) {
		s_attributes[i] = Period04.projectGetNameSet(i);
	    }	    cb_attribute = new JComboBox(s_attributes);
	    String [] s_digits = new String[11];
	    for (int i=0; i<11; i++) {
		s_digits[i] = Integer.toString(i);
	    }
	    cb_digits = new JComboBox(s_digits);
	    cb_digits.setSelectedIndex(2);

	    l_attribute .setBounds(  10,   0, 160, 40);
	    cb_attribute.setBounds( 170,  10, 150, 20);
	    l_gapsize   .setBounds(  10,  45, 160, 20 );
	    t_gapsize   .setBounds( 170,  45, 150, 20 );
	    l_prefix    .setBounds(  10,  75, 160, 20);
	    t_prefix    .setBounds( 170,  75, 150, 20);
	    c_useCounter.setBounds(  10, 100, 200, 20);
	    l_digits    .setBounds(  10, 125, 230, 20);
	    cb_digits   .setBounds( 240, 125,  80, 20);

	    inputpanel.add(l_gapsize);
	    inputpanel.add(t_gapsize);
	    inputpanel.add(l_attribute);
	    inputpanel.add(cb_attribute);
	    inputpanel.add(l_prefix);
	    inputpanel.add(t_prefix);
	    inputpanel.add(c_useCounter);
	    inputpanel.add(l_digits);
	    inputpanel.add(cb_digits);

	    optionPane = new JOptionPane((Object)inputpanel, 
					 JOptionPane.PLAIN_MESSAGE,
					 JOptionPane.YES_NO_OPTION,
					 null, options, options[0]);
	    setContentPane(optionPane);
	    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	    addWindowListener(new WindowAdapter() {
		    public void windowClosing(WindowEvent we) {
			/*
			 * Instead of directly closing the window,
			 * we're going to change the JOptionPane's
			 * value property.
			 */
			optionPane.setValue(
			    new Integer(JOptionPane.CLOSED_OPTION));
		    }
		});
	    
	    optionPane.addPropertyChangeListener(new PropertyChangeListener() {
		    public void propertyChange(PropertyChangeEvent e) {
			String prop = e.getPropertyName();
			
			if (isVisible() 
			    && (e.getSource() == optionPane)
			    && (prop.equals(JOptionPane.VALUE_PROPERTY) ||
				prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
			    Object value = optionPane.getValue();
			    
			    if (value == JOptionPane.UNINITIALIZED_VALUE) {
				//ignore reset
				return;
			    }
			    

			    // Reset the JOptionPane's value.
			    // If you don't do this, then if the user
			    // presses the same button next time, no
			    // property change event will be fired.
			    optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

			    if (value.equals(s_OK)) {
				String s_gap = t_gapsize.getText();
				
				// check if it is a correct number
				NumberFormatException nan = null;
				try { 
				    Double.parseDouble(s_gap); 
				} catch (NumberFormatException exception) { 
				    nan=exception; 
				} 	   
				if (s_gap.equals("") || (nan!=null)) {
				    // text was invalid
				    t_gapsize.selectAll();
				    JOptionPane.showMessageDialog(
					XDialogSubdivideGaps.this,
					"Sorry, \"" + s_gap + "\" "
					+ "isn't a valid number.\n",
					"Try again",
					JOptionPane.ERROR_MESSAGE);
				    s_gap = null;
				} else {
				    if (Period04.projectGetSelectedPoints()==0) {
					JOptionPane.showMessageDialog(
					    mainframe.getFrame(),
					    "There are no selected points!",
					    "Error",JOptionPane.ERROR_MESSAGE);
					return;
				    }

				    double gap=Double.parseDouble(s_gap);
				    int attribute=cb_attribute.getSelectedIndex();
				    boolean usecount=c_useCounter.isSelected();
				    String prefix=t_prefix.getText();
				    int digits=cb_digits.getSelectedIndex();
				    // now go and do it
				    Period04.projectSubdivide(
					gap, attribute, usecount, 
					prefix, digits);
				    // hide dialog
				    setVisible(false);
				    // write to protocol
				    StringBuffer text = new StringBuffer();
				    text.append(
					"Subdivided the current selection\n"+
					"In column:                "+attribute+
					"\nBy gaps of:             "+s_gap+
					"\nusing the prefix:       "+prefix+
					"\nand displaying "+digits+
					" decimal places ");
				    if (usecount) {
					text.append("with a running counter.\n");
				    } else {
					text.append("with time.\n");
				    }
				    mainframe.getLog().writeProtocol(text.toString(), true);
				    // now update the list, 
				    // it's necessary to prevent the selection
				    // listener from trying to set the selection
				    // (the list items are already selected)
				    // otherwise a nullpointerexception occurs!
				    tabTS.blockSelectionEvent=true;
				    tabTS.updateList(attribute);
				    tabTS.blockSelectionEvent=false;
				}
			    } else {
				setVisible(false);
			    }
			}
		    }
		});
	    
	    this.setTitle("Subdivide timestring:");
	    this.setSize(360,240);
	    this.setResizable(false);
	    this.setLocationRelativeTo(mainframe.getFrame());
	    this.setVisible(true);
	}

	public void actionPerformed(ActionEvent evt) {
	    optionPane.setValue(s_OK);
	}

    }

