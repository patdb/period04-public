/*----------------------------------------------------------------------*
 *  XDialogImproveSpecial
 *  a dialog to select the free parameters of a least-squares fit
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.*;                  // Property change stuff
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.border.*;          // TitledBorder stuff
import javax.swing.event.*;           // mouseinputlistener

class XDialogImproveSpecial extends XDialog
    implements ComponentListener
{
    private static final long serialVersionUID = 1410788474213673672L;
    private JOptionPane         optionPane;
    private final JList []      listBox = new JList[3];
    private DefaultListModel [] listBoxModel   = new DefaultListModel[3];
    private JScrollPane      [] listScrollPane = new JScrollPane[3];
    private JPanel              inputpanel;

    private JButton             b_Calc, b_Cancel;
    private int          fitmode;
    private ArrayList [] array = new ArrayList[3];
    private boolean canceled = false;

    private final int WIDTH  = 510;      // default frame height
    private final int HEIGHT = 350;      // default frame width
    private int sizeDiff = 0;     // correction factor for height

    public XDialogImproveSpecial(JFrame mainframe, int fitmode, 
				 String s_OKButton) {
	super(mainframe, true);  // necessary to keep dialog in front
	this.fitmode = fitmode;

	inputpanel = new JPanel();
	inputpanel.setLayout(null);

	String text = "<html>Please select what you want to improve:</html>";
	JLabel l_text = createLabel(text);
	l_text.setFont(new Font("Dialog", Font.BOLD, 12));

	JLabel l_freq = createLabel("Frequency");
	JLabel l_amp = createLabel("Amplitude");
	JLabel l_pha = createLabel("Phase");

	l_text.setBounds( 10, 10, 300, 20);
	l_freq.setBounds( 10, 40, 150, 20 );
	l_amp.setBounds( 170, 40, 150, 20 );
	l_pha.setBounds( 330, 40, 150, 20 );

	inputpanel.add(l_text, null);
	inputpanel.add(l_freq, null);
	inputpanel.add(l_amp, null);
	inputpanel.add(l_pha, null);
	
	for (int i=0; i<3; i++) {
	    listBoxModel[i] = new DefaultListModel();
	    listBox[i] = new JList(listBoxModel[i]);
	    listBox[i].setSelectionMode(
		ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	    listBox[i].setFont(new Font("Dialog", Font.PLAIN, 11));

	    // add the selectionlistener
	    MouseInputListener mil = new DragSelectionListener();
	    listBox[i].addMouseMotionListener(mil);
	    listBox[i].addMouseListener(mil);

	    // make the list scrollable
	    listScrollPane[i] = new JScrollPane(
		listBox[i], JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, 
		JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	    listScrollPane[i].setBorder(
		BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
	    listScrollPane[i].setBounds( 10+i*160, 60, 150, 200 );
	    inputpanel.add(listScrollPane[i], null);
	}

	getData(); // fill the lists

	// create buttons
	b_Calc    = createButton(s_OKButton);
	b_Cancel  = createButton("Cancel");
	// add action listeners
	b_Calc   .addActionListener(this);
	b_Cancel .addActionListener(this);
	    
	Object[] options = {b_Calc, b_Cancel};

	Object array = inputpanel;
        optionPane = new JOptionPane(array, 
				     JOptionPane.PLAIN_MESSAGE,
				     JOptionPane.YES_NO_OPTION,
				     null, options, options[0]);
	setContentPane(optionPane);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent we) {
                /*
                 * Instead of directly closing the window,
                 * we're going to change the JOptionPane's
                 * value property.
                 */
                    optionPane.setValue(new Integer(JOptionPane.CLOSED_OPTION));
		}
	    });
	addComponentListener(this);

	optionPane.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
                String prop = e.getPropertyName();

                if (isVisible() 
                 && (e.getSource() == optionPane)
                 && (prop.equals(JOptionPane.VALUE_PROPERTY) ||
                     prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
                    Object value = optionPane.getValue();

                    if (value == JOptionPane.UNINITIALIZED_VALUE) {
                        //ignore reset
                        return;
                    }

                    // Reset the JOptionPane's value.
                    // If you don't do this, then if the user
                    // presses the same button next time, no
                    // property change event will be fired.
                    optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

                    if (value.equals("OK")) {
			if (XDialogImproveSpecial.this.fitmode!=1) { // normal mode
			    // retrieve the selection setting
			    // but first deselect everything...
			    for (int i=0; i<Period04.projectGetTotalFrequencies(); i++) {
				if (Period04.projectGetActive(i)) {
				    Period04.periodUnsetSelection(i, 0/*Fre*/);
				    Period04.periodUnsetSelection(i, 1/*Amp*/);
				    Period04.periodUnsetSelection(i, 2/*Pha*/);
				}
			    }
			    // now set the selection
			    int [] selectedindices;
			    for (int j=0; j<3; j++) {
				selectedindices = listBox[j].getSelectedIndices();
				for (int i=0; i<selectedindices.length; i++) {
				    Period04.periodSetSelection(
					getListEntryID(j,selectedindices[i]),j);				}
			    }
			} else { // binary mode
			    // retrieve the selection setting
			    // but first deselect everything...
			    for (int i=-1 /*-1=bm-frequency*/; 
				 i<Period04.projectGetBMTotalFrequencies(); i++) {
				Period04.periodUnsetBMSelection(i, 0/*Fre*/);
				Period04.periodUnsetBMSelection(i, 1/*Amp*/);
				Period04.periodUnsetBMSelection(i, 2/*Pha*/);
			    }
			    // now set the selection
			    int [] selectedindices;
			    for (int j=0; j<3; j++) {
				selectedindices = listBox[j].getSelectedIndices();
				for (int i=0; i<selectedindices.length; i++) {
				    Period04.periodSetBMSelection(
					getListEntryID(j,selectedindices[i]),j);

				}
			    }
			}
			canceled = false;
			setVisible(false);
		    } else { // user closed dialog or clicked cancel
			canceled = true;
			setVisible(false);
		    }
                }
            }
        });

	this.setTitle("Special improvement:");
	this.setSize(WIDTH,HEIGHT);
        // this.setResizable(false);
	this.setLocationRelativeTo(mainframe);
	this.setVisible(true);
    }

    public void getData() {
	String txt;	
	int f_count=0, a_count=0, p_count=0;
	// create 3 array lists to hold the list entries
	for (int i=0; i<3; i++) {
	    array[i] = new ArrayList(); 
	}
	// now get the data
	if (fitmode!=1) { // this is for normal mode
	    for (int i=0; i<Period04.projectGetTotalFrequencies(); i++) {
		if (Period04.projectGetActive(i)) {
		    String freqnumber = Period04.projectGetNumber(i);
		    if (!Period04.projectIsComposition(i) || 
				Period04.projectGetCompoUseFreqValue(i)) {
			txt = freqnumber.toString()+Period04.projectGetFrequency(i);
			array[0].add(new ListEntry(txt,i));
			listBoxModel[0].addElement(txt);
			if (Period04.periodIsSelected(i, 0 /*=Fre*/)) {
			    listBox[0].addSelectionInterval(f_count,f_count);
			}
			f_count++;
		    }
		    txt=freqnumber.toString()+Period04.projectGetAmplitude(i);
		    array[1].add(new ListEntry(txt,i));
		    listBoxModel[1].addElement(txt);
		    if (Period04.periodIsSelected(i, 1 /*=Amp*/)) {
			listBox[1].addSelectionInterval(a_count,a_count);
		    }
		    a_count++;
		    txt = freqnumber.toString()+Period04.projectGetPhase(i);
		    array[2].add(new ListEntry(txt,i));
		    listBoxModel[2].addElement(txt);
		    if (Period04.periodIsSelected(i, 2 /*=Pha*/)) {
			listBox[2].addSelectionInterval(p_count,p_count);
		    }
		    p_count++;
		}
	    }
	} else {
	    //---
	    //--- first add the periodic time shift frequency
	    //---
	    txt = "PTSF "+Period04.projectGetBMFrequency(-1);
	    array[0].add(new ListEntry(txt,-1));
	    listBoxModel[0].add(0,txt);
	    txt = "PTSF "+Period04.projectGetBMAmplitude(-1);
	    array[1].add(new ListEntry(txt,-1));
	    listBoxModel[1].add(0,txt);
	    txt = "PTSF "+Period04.projectGetBMPhase(-1);
	    array[2].add(new ListEntry(txt,-1));
	    listBoxModel[2].add(0,txt);
	    //--- select items if necessary
	    if (Period04.periodIsBMSelected(-1,0 /*=Fre*/)) 
		listBox[0].addSelectionInterval(f_count,f_count);
	    if (Period04.periodIsBMSelected(-1,1 /*=Amp*/)) 
		listBox[1].addSelectionInterval(a_count,a_count);
	    if (Period04.periodIsBMSelected(-1,2 /*=Pha*/)) 
		listBox[2].addSelectionInterval(p_count,p_count);
	    f_count++; a_count++; p_count++;
	    //---
	    //--- now add the other frequencies
	    //---
	    for (int i=0; i<Period04.projectGetBMTotalFrequencies(); i++) {
		if (Period04.projectGetBMActive(i)) {
		    String freqnumber = Period04.projectGetBMNumber(i);
		    if (!Period04.projectIsBMComposition(i)) {
			txt = freqnumber.toString() +
			    Period04.projectGetBMFrequency(i);
			array[0].add(new ListEntry(txt,i));
			listBoxModel[0].addElement(txt);
			if (Period04.periodIsBMSelected(i, 0 /*=Fre*/)) {
			    listBox[0].addSelectionInterval(f_count,f_count);
			}
			f_count++;
		    }
		    txt = freqnumber.toString()+
			Period04.projectGetBMAmplitude(i);
		    array[1].add(new ListEntry(txt,i));
		    listBoxModel[1].addElement(txt);
		    if (Period04.periodIsBMSelected(i, 1 /*=Amp*/)) {
			listBox[1].addSelectionInterval(a_count,a_count);
		    }
		    a_count++;
		    txt = freqnumber.toString()+Period04.projectGetBMPhase(i);
		    array[2].add(new ListEntry(txt,i));
		    listBoxModel[2].addElement(txt);
		    if (Period04.periodIsBMSelected(i, 2 /*=Pha*/)) {
			listBox[2].addSelectionInterval(p_count,p_count);
		    }
		    p_count++;
		}
	    }
	}
    }

    public void actionPerformed(ActionEvent evt) {
	Object s = evt.getSource();
	if (s==b_Cancel) { optionPane.setValue("cancel"); }
	else if (s==b_Calc) {
	    optionPane.setValue("OK");
	}
    }

    public boolean isCanceled() {
	return canceled;
    }

    class ListEntry {
	public int id;
	public String string;
	public ListEntry(String string, int id) {
	    this.string = string;
	    this.id = id;
	}
    }
    
    public int getListEntryID(int boxID, int index) {
	// retrieve string from list
	String s_entry = (String)listBoxModel[boxID].getElementAt(index);
	// now search the appropriate listentry object and return its ID
	for (int i=0; i<array[boxID].size(); i++) {
	    ListEntry tmp = (ListEntry)array[boxID].get(i);
	    if (tmp.string==s_entry) {
		return tmp.id;
	    }
	}
	return -1;
    }

    //---
    //--- methods to listen to ComponentEvents 
    //--- (we are listening to ComponentEvents that are caused by resizing 
    //---  the mainframe)
    //---
    public void componentResized(ComponentEvent e) {
	//--- the frame has been resized. 
	//--- set the framewidth back to the default
	//--- if the frameheight is to low set it back to the default value
	//--- if the frameheight is ok, then determine the difference to the 
	//--- default value and call fitSize()
	//--- this will size the GUI elements to fit to the new frameheight
	Component c = e.getComponent();
	Dimension frameSize = c.getSize();
	if (frameSize.height <= HEIGHT) {
	    frameSize.height = HEIGHT;
	    sizeDiff = 0;
	} else {
	    sizeDiff = HEIGHT-frameSize.height;
    	}
	frameSize.width = WIDTH;
	setSize(frameSize);
	fitSize();
    }
    public void componentHidden(ComponentEvent e) {}
    public void componentShown(ComponentEvent e)  {}
    public void componentMoved(ComponentEvent e)  {}

    private void fitSize() {
	for (int i=0; i<3; i++) {
	    listScrollPane[i].setBounds( 10+i*160, 60, 150, 200-sizeDiff );
	}
	inputpanel.revalidate();
    }
}
