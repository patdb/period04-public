/*--------------------------------------------------------------------*
 *  XTabFit
 *  creates the Fit panel
 *
 * todo: rename calcmode to datamode!!
 *--------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import javax.help.CSH;
import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.*;
import javax.swing.filechooser.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

public class XTabFit extends XTab {

    //---
    //--- general
    //---
    private XProjectFrame mainframe;
    private JTabbedPane   tabbedPane = new JTabbedPane();
    private JFileChooser  fc;
    private XDialogAmplitudeVariation ampvarDialog;
    private int           interval; 
    private FitTask       task;
    private XPlotPhase    plot = null;

    private int correct;
    private int MAXFREQ = Period04.projectGetMaxFreq();  
    private int INITFREQS = 24; //initial number of frequency fields

    public static final int AMP_PHA     = 0;
    public static final int FRE_AMP_PHA = 1;
    public static final int SPECIAL     = 2;
    public static final int AMP_VAR     = 3;
    public static final int BM_START    = 4;
    public static final int BM_VAR      = 5;
    private int selectionmode = -1;

    public static final int NORMAL_MODE = 0;
    public static final int BINARY_MODE = 1;
    private int fitmode = NORMAL_MODE;

    public static final int OBSERVED  = 0;
    public static final int ADJUSTED  = 1;
    public static final int SIMULATED = 7; // see CTimeString.h
    public int lastCalculation = OBSERVED;

    private boolean batchMode = false;

    private ToggleIcon toggleRight   = new ToggleIcon(true);
    private ToggleIcon toggleLeft    = new ToggleIcon(false);
    //private ToggleIcon toggleBMRight = new ToggleIcon(true);
    //private ToggleIcon toggleBMLeft  = new ToggleIcon(false);
   
    //---
    //--- frequency panel
    //---
    private JButton       b_OpenFourierPanel, b_OpenTimeStringPanel;
    private JButton       b_ImportFreq, b_ExportFreq, b_PrintFreq, b_Weights;
    private JButton       b_Calculate, b_ImproveAll, b_ImproveSpecial;
    private JButton       b_CalcVariation, b_PhasePlot;
    private JLabel        l_Selected, l_Zero, l_Residuals;
    private JLabel        l_FittingFormula;
    private JRadioButton [] checkCalcMode = new JRadioButton[2];
    private JTextField    t_Weights;
    private JScrollPane   frequencyScrollPane;
    private JPanel        frequencyListPanel, header;
    private Collection<JFrequencyListEntry> freqlist;
    private JLabel        l_FreqListLabel;

    //---
    //--- error panel
    //---
    private JPanel        errorPanel;//, errorListPanel;
    private JButton       b_ErrorCalc, b_MonteCarlo;
    private JButton       b_ExportErrorList, b_PrintErrorList, b_ErrorInfo;
    private JScrollPane   errorScrollPane;
    private JTextArea     t_errors;

    //---
    //--- periodic time shift panel (bmpanel)
    //---
    private JPanel        bmPanel;
    private JButton       b_BMCalcStartValues, b_BMImprovePTS;
    private JTextField    t_BMFreq, t_BMAmp, t_BMPha;
    private boolean blockChangeEvents =false;
    private XDialogMCInput mcDialog;

    //---
    //--- store amp-var settings
    //---
    public int    store_varmode         = 0;
    public int    store_attribute       = 0;
    public int [] store_selectedindices = null;

    private Object mutex = new Object();

    //---
    //--- XTabFit - Constructor
    //--- configures the tabbed frame
    //---
    public XTabFit(XProjectFrame m_frame) {
	mainframe = m_frame;
	correct   = mainframe.getSizeDiff();
	//---
	//--- configure tabbed frame
	//---
	tabbedPane.setTabPlacement(JTabbedPane.TOP);
	tabbedPane.setFont(new java.awt.Font("Dialog", Font.PLAIN, 11));
	//---
	//--- create main panel
	//---
	mainPanel = new JPanel();
	mainPanel.setLayout(null);
	createMainPanel();
	//---
	//--- create error panel
	//---
	errorPanel = new JPanel();
	errorPanel.setLayout(null);
	createErrorPanel();
	//---
 	//--- create bm panel
	//---
	bmPanel = new JPanel();
	bmPanel.setLayout(null);
	createBMPanel();
	//---
	//--- add panels to the tabbed pane
	//---
	tabbedPane.addTab("   Main   "       , null, mainPanel );
	tabbedPane.addTab("  Goodness of Fit  ", null, errorPanel);
    }

    public JTabbedPane getTabPanel() { return tabbedPane; }

  private void initializeFileChooser ( )
  {
    // create FileChooser
    fc = new JFileChooser( Period04.getCurrentDirectory( ));

    fc.setFileFilter( new FileFilter ( )
      {
        public boolean accept ( File f)
        {
          return f.isDirectory( )
              || f.getName( ).toLowerCase( ).endsWith( ".per")
          ;
        }

        public String getDescription ( )
        {
          return "Period files(*.per)";
        }
      }
    );
  }

    public void actionPerformed(ActionEvent evt) {
	if (mainframe.isWorking()) { return; }
	Object s=evt.getSource();
	// main panel
	if (s==b_Weights) { // show dialog and update weight string
	    new XDialogWeight(mainframe); 
	    updateWeights();
 	}
	else if (s==b_ImportFreq)       { importFrequencies(); }
	else if (s==b_ExportFreq)       { exportFrequencies(false); }
	else if (s==b_PrintFreq)        { printFrequencies(); }
	else if (s==b_OpenTimeStringPanel) { expandTimeStringPanel(); }
	else if (s==b_OpenFourierPanel) { expandFourierPanel(); }
	else if (s==b_Calculate)        { calculate(); }
	else if (s==b_ImproveAll)       { improveAll(); }
	else if (s==b_ImproveSpecial)   { improveSpecial(); }
	else if (s==b_CalcVariation)    { calculateAmpVar(); }
	else if (s==b_PhasePlot)        { displayPhasePlot(); }
	else if (s==b_ErrorCalc)       { calculateErrors(); }
	else if (s==b_MonteCarlo)      { calculateMonteCarloErrors(); }
	else if (s==b_ExportErrorList) { exportErrorList(); }
	else if (s==b_PrintErrorList)  { printErrorList();  }
	else if (s==b_BMCalcStartValues) { searchPTSStartValues(); }
	else if (s==b_BMImprovePTS)      { improvePTS(); }
    }


    //-m- ------------------------------------------------ ---//
    //---                                                  ---//
    //---        methods for the main fit panel            ---//
    //---                                                  ---//
    //--- ------------------------------------------------ ---//
 
    //-m-
    //--- create the panel for the main tab
    //---
    private void createMainPanel() {
	//---
	//--- create import/export buttons
	//---
	b_ImportFreq = createButton("Import");
	b_ExportFreq = createButton("Export");
	b_PrintFreq  = createButton("Print frequencies");
	b_ImportFreq.setFont(new Font("Dialog", Font.PLAIN, 11));
	b_ExportFreq.setFont(new Font("Dialog", Font.PLAIN, 11));
	b_PrintFreq .setFont(new Font("Dialog", Font.PLAIN, 11));
	b_ImportFreq.setMargin(new Insets(0,0,0,0));
	b_ExportFreq.setMargin(new Insets(0,0,0,0));
	b_PrintFreq .setMargin(new Insets(0,0,0,0));
	if (Period04.isMacOS) {
		b_ImportFreq.setBounds( 30, 8,  80, 20 );
		b_ExportFreq.setBounds(110, 8,  80, 20 );
		b_PrintFreq .setBounds( 30, 32, 160, 20 );
	} else {
		b_ImportFreq.setBounds( 30, 8,  70, 20 );
		b_ExportFreq.setBounds(100, 8,  70, 20 );
		b_PrintFreq .setBounds( 30, 32, 140, 20 );
	}
	//---
	//--- create some informative labels
	//---
	JLabel l_SelectedFreq = createLabel("Selected Frequencies:");
	JLabel l_ZeroPoint = createLabel("Zero point:");
	JLabel l_Res = createLabel("Residuals:");
	l_Selected  = createLabel("0");
	l_Zero      = createLabel("0");
	l_Residuals = createLabel("0");
	if (Period04.isMacOS) {
		l_SelectedFreq.setBounds( 220, 8, 140, 15 );
		l_Selected    .setBounds( 370, 8, 100, 15 );
		l_ZeroPoint   .setBounds( 220, 23, 140, 15 );
		l_Zero        .setBounds( 370, 23, 100, 15 );
		l_Res         .setBounds( 220, 38, 140, 15 );
		l_Residuals   .setBounds( 370, 38, 100, 15 );
	} else {
		l_SelectedFreq.setBounds( 210, 8, 140, 15 );
		l_Selected    .setBounds( 360, 8, 100, 15 );
		l_ZeroPoint   .setBounds( 210, 23, 140, 15 );
		l_Zero        .setBounds( 360, 23, 100, 15 );
		l_Res         .setBounds( 210, 38, 140, 15 );
		l_Residuals   .setBounds( 360, 38, 100, 15 );
	}
	//---
	//--- create flipflop buttons
	//---
	b_OpenTimeStringPanel = new JButton(toggleLeft);
	b_OpenFourierPanel    = new JButton(toggleRight);
//   	b_OpenTimeStringPanel.setBackground(new Color(230,225,217));
//   	b_OpenFourierPanel   .setBackground(new Color(230,225,217));
	b_OpenTimeStringPanel.setToolTipText("Detach the time string tab");
	b_OpenFourierPanel   .setToolTipText("Detach the Fourier tab");
	b_OpenTimeStringPanel.addActionListener(this);
	b_OpenFourierPanel   .addActionListener(this);
	b_OpenTimeStringPanel.setBounds(   5, 5, 10, 50 );
	b_OpenFourierPanel   .setBounds( 470, 5, 10, 50 );
	//---
	//--- create an opaque panel for the calculation settings
	//---
	JPanel calcPanel = new JPanel();
	calcPanel.setLayout(null);
	calcPanel.setOpaque(false);
	calcPanel.setBorder(BorderFactory.createCompoundBorder(
		BorderFactory.createTitledBorder(
		    BorderFactory.createEtchedBorder(EtchedBorder.RAISED), 
		    " Settings for the Least-Squares Fit Calculation ",
		    TitledBorder.LEADING, TitledBorder.TOP, 
		    new Font("Dialog", Font.BOLD, 10), 
		    new Color(20,30,150) ),
		BorderFactory.createEmptyBorder(5,5,5,5)));
	calcPanel.setBounds( 0, 60, 489, 100 );
	//---
	//--- settings panel: first line - the fitting formula
	//---
	JLabel l_Formula = createLabel("Fitting formula:");
	l_Formula.setBounds( 20, 80, 100, 20 ); 
	// display the fitting formula that is beeing used here
	l_FittingFormula = createLabel(
	    "<html>Z + \u2211 \u0391<sub>i</sub> "+
	    "sin( 2\u03C0 (\u03A9<sub>i</sub> t + \u03A6<sub>i</sub>) )</html>");
	l_FittingFormula.setBounds( 185, 77, 300, 28 );
	//---
	//--- settings panel: second line - the calculation mode
	//---
	JLabel l_Calc = createLabel("Calculations based on:");
	checkCalcMode[0] = createRadioButton("Original data");
	checkCalcMode[1] = createRadioButton("Adjusted data");
	ButtonGroup bg = new ButtonGroup();
	bg.add(checkCalcMode[0]);
	bg.add(checkCalcMode[1]);
	l_Calc          .setBounds(  20, 110, 140, 15 );
	if (Period04.isMacOS) {
		checkCalcMode[0].setBounds( 180, 110, 120, 15 );
		checkCalcMode[1].setBounds( 310, 110, 120, 15 );
	} else {
		checkCalcMode[0].setBounds( 180, 110, 110, 15 );
		checkCalcMode[1].setBounds( 300, 110, 110, 15 );
	}
	checkCalcMode[Period04.projectGetUseData()].setSelected(true);
	lastCalculation = Period04.projectGetUseData();
	//---
	//---
	//---
	JLabel l_UseWeights = createLabel("Use weights:");
	t_Weights = createTextField(Period04.projectGetWeightString());
	t_Weights.setEditable(false);
	t_Weights.setHorizontalAlignment(JTextField.CENTER);
	t_Weights.setToolTipText(
	    "<html>Weight string:<br>"+
	    "If no weights are used \"none\" will be displayed."+
	    "<table><tr><td width=\"50\" align=\"center\">1</td><td>"+
	    "use the weights assigned to attribute 1</td></tr>"+
	    "<tr><td align=\"center\">2</td><td>use the weights assigned to "+
	    "attribute 2</td></tr><tr><td align=\"center\">3</td><td>use the "+
	    "weights assigned to attribute 3</td></tr><tr>"+
	    "<td align=\"center\">4</td><td>use the weights assigned to "+
	    "attribute 4</td></tr><tr><td align=\"center\">p</td><td>use "+
	    "point weight</td></tr><tr><td align=\"center\">d</td><td>use "+
	    "deviation weight</td></tr></table></html>");
	b_Weights = createButton("Edit weight settings");
	b_Weights.setFont(new Font("Dialog", Font.PLAIN, 11));
	b_Weights.setMargin(new Insets(0,0,0,0));
	l_UseWeights.setBounds(  20, 130, 160, 20 );
	t_Weights   .setBounds( 110, 130,  70, 20 );
	b_Weights.setBounds( 180, 130, 160, 20 );

	createFreqListPanel();
	frequencyScrollPane = new JScrollPane(
	    frequencyListPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, 
	    JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	frequencyScrollPane.setBounds( 10, 165, 470, 355-correct );

	b_Calculate = createCalcButton("Calculate");
	b_Calculate.setFont(new Font("Dialog", Font.BOLD, 11));
	b_Calculate.setMnemonic(KeyEvent.VK_C); // shortcut
	b_Calculate.setToolTipText(
	    "<html>Keeps frequencies fixed,<br>"+
	    "calculates amplitudes and phases</html>");
	b_ImproveAll = createCalcButton("Improve all");
	b_ImproveAll.setMnemonic(KeyEvent.VK_I); // shortcut
	b_ImproveAll.setToolTipText(
	    "<html>Fits frequencies as well as<br>"+
	    "amplitudes and phases</html>");
	b_ImproveSpecial = createCalcButton("Improve special");
	b_ImproveSpecial.setToolTipText(
	    "<html>Opens a dialog for setting<br>"+
	    "a user-defined selection<br>"+
	    "of parameters to improve.</html>");
	b_CalcVariation = createCalcButton(
	    "Calculate amplitude/phase variations");
	b_CalcVariation.setToolTipText(
	    "<html>Opens a dialog for the calculation<br>"+
	    "of amplitude and/or phase variations</html>");
	b_PhasePlot = createButton("Phase diagram");
	b_ImproveAll.setFont(new Font("Dialog", Font.BOLD, 11));

	b_Calculate     .setBounds(  10, 525-correct, 150, 20 );
	b_ImproveAll    .setBounds( 170, 525-correct, 150, 20 );
	b_ImproveSpecial.setBounds( 330, 525-correct, 150, 20 );
	b_CalcVariation .setBounds(  10, 550-correct, 260, 20 );
	b_PhasePlot     .setBounds( 330, 550-correct, 150, 20 );

	// add to main panel
	mainPanel.add( b_ImportFreq, null);
	mainPanel.add( b_ExportFreq, null);
	mainPanel.add( b_PrintFreq, null);
	mainPanel.add( b_OpenTimeStringPanel, null);
	mainPanel.add( b_OpenFourierPanel, null);
	mainPanel.add( l_SelectedFreq, null);
	mainPanel.add( l_Selected, null);
	mainPanel.add( l_ZeroPoint, null);
	mainPanel.add( l_Zero, null);
	mainPanel.add( l_Res, null);
	mainPanel.add( l_Residuals, null);
	mainPanel.add( calcPanel, null);
	mainPanel.add( l_Formula, null);
	mainPanel.add( l_FittingFormula, null);
	mainPanel.add( l_Calc, null);
	mainPanel.add( checkCalcMode[0], null);
	mainPanel.add( checkCalcMode[1], null);
	mainPanel.add( l_UseWeights, null);
	mainPanel.add( t_Weights, null);
	mainPanel.add( b_Weights, null);
	mainPanel.add( frequencyScrollPane, null);
	mainPanel.add( b_Calculate, null);
	mainPanel.add( b_ImproveAll, null);
	mainPanel.add( b_ImproveSpecial, null);
	mainPanel.add( b_CalcVariation, null);
	mainPanel.add( b_PhasePlot, null);
    }

    private JPanel createFreqListHeader() {
	header = new JPanel();
	header.setLayout(null);

	// create labels
	JLabel l_useFreq = createLabel("Use Freq#");
	JLabel l_Freq    = createLabel("Frequency");
	JLabel l_Amp     = createLabel("Amplitude");
	JLabel l_Phase   = createLabel("Phase");
	l_useFreq.setBounds(  5, 5,  60, 15 );
	l_Freq   .setBounds( 80, 5, 130, 15 );
	l_Amp    .setBounds(230, 5, 110, 15 );
	l_Phase  .setBounds(360, 5,  90, 15 );
	header.add( l_useFreq, null);
	header.add( l_Freq, null);
	header.add( l_Amp, null);
	header.add( l_Phase, null);
	return header;
    }

    private void createFreqListPanel() { 
	frequencyListPanel = new JPanel();
	frequencyListPanel.setLayout(null);
	
	JPanel header=createFreqListHeader();
	header.setBounds(0,0,485,20);
	frequencyListPanel.add( header, null);

	// create list
	Period04.projectSetTotalFrequencies(INITFREQS);  
	freqlist  = new ArrayList();
	for (int i=0; i<INITFREQS; i++) {
	    JFrequencyListEntry entry = new JFrequencyListEntry(this,i);
	    entry.setBounds(0,20+i*22,485,22);
	    freqlist.add(entry);
	    frequencyListPanel.add(entry);
	}
	
	l_FreqListLabel = createLabel("More frequency fields will be shown after you fill the given ones.");
	l_FreqListLabel.setBounds(10,42+INITFREQS*22,450,22);
	frequencyListPanel.add(l_FreqListLabel);

	// frequencyListPanel.setBackground(new Color(230,225,217));
	Dimension s=new Dimension(490,64+INITFREQS*22);
	frequencyListPanel.setPreferredSize(s);
	frequencyListPanel.revalidate();
    }

    public void addEmptyFrequencyEntry() {
	int i=freqlist.size();
	if (i>=MAXFREQ) { return; }
	if (i>=Period04.projectGetTotalFrequencies()) {
	    Period04.projectSetTotalFrequencies(i+1);  
	}
	l_FreqListLabel.setBounds(0,20+(i+1)*22,400,22);
	JFrequencyListEntry entry = new JFrequencyListEntry(this,i);
	entry.setBounds(0,20+i*22,485,22);
	freqlist.add(entry);
	// now add it to the GUI
	Dimension s=frequencyListPanel.getPreferredSize();
	s.height+=22;
	frequencyListPanel.add(entry);
	frequencyListPanel.setPreferredSize(s);
	frequencyListPanel.revalidate();	// to update scroll bar
    }

    public void sortFrequencyList(int mode) {
	Comparator comp = new FreqIDComparator(); // this is the default
	switch (mode) {
	case 1: 
	    comp = new FrequencyComparator();
	    break;
	case 2:
	    comp = new AmplitudeComparator();
	    break;
	}
	Collections.sort((ArrayList)freqlist, comp);

	// now make change in GUI
	int i=0;
	for ( JFrequencyListEntry e : freqlist ) {
	    e.setBounds(0,20+i*22,485,22);
	    i++;
	}
	frequencyListPanel.revalidate();
    }


    //-e- ------------------------------------------------ ---//
    //---                                                  ---//
    //---        methods for the fit error panel           ---//
    //---                                                  ---//
    //--- ------------------------------------------------ ---//
 
    //-e-
    //--- create the panel for the error tab
    //---
    private void createErrorPanel() {
	//-- create the buttons
	b_ErrorInfo = new JButton("Info");
	b_ErrorCalc        = createCalcButton("Calculate LS uncertainties");
	b_MonteCarlo       = createCalcButton("Monte Carlo Simulation");
	b_ErrorCalc.setToolTipText(
	    "<html>Calculate uncertainties from a<br>"+
	    "least-squares fit to your data set<br>"+
	    "shifted in time in order to uncouple<br>"+
	    "frequency and phase uncertainties.</html>");
	b_MonteCarlo.setToolTipText(
	    "<html>Calculate uncertainties by means of<br>"+
	    "a Monte Carlo simulation</html>");
	//--- create the text area
	t_errors = new JTextArea();
	t_errors.setEditable(false);
        t_errors.setFont(new Font("Monospaced", Font.PLAIN, 11));
	errorScrollPane = new JScrollPane(
	    t_errors, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, 
	    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	//--- create the bottom button line
 	b_PrintErrorList  = createButton("Print list");
	b_ExportErrorList = createButton("Export list");

	if (Period04.isMacOS) {
		b_ErrorInfo       .setBounds( 10, 10,  70, 20);
		b_ErrorCalc       .setBounds( 85, 10, 195, 20);
	} else {
		b_ErrorInfo       .setBounds( 10, 10,  60, 20);
		b_ErrorCalc       .setBounds( 80, 10, 195, 20);
	}
	b_MonteCarlo      .setBounds(285, 10, 195, 20);
	errorScrollPane   .setBounds( 10, 40, 470, 500-correct );
	b_PrintErrorList  .setBounds( 10, 550-correct, 230, 20 );
	b_ExportErrorList .setBounds(250, 550-correct, 230, 20 );

	errorPanel.add( b_ErrorInfo,        null );
	errorPanel.add( b_ErrorCalc,        null );
	errorPanel.add( b_MonteCarlo,       null );
	errorPanel.add( errorScrollPane,    null );
	errorPanel.add( b_PrintErrorList,   null );
	errorPanel.add( b_ExportErrorList,  null );

	//--- now fill with text
	t_errors.setText(Period04.projectGetFitError());
    }
    
    public void addHelp() {
	b_ErrorInfo.addActionListener(
	    new CSH.DisplayHelpFromSource(XProjectFrame.getHelpBroker()));
	CSH.setHelpIDString(b_ErrorInfo,"howto.uncertainties"); // set link
    }

    private void calculateErrors() {
	if (task!=null) { return; }   // last task is not yet finished, return
	//---
	//--- prepare for calculation
	//---
	setFrequencyListData();	        // update data
	//--- have there been relevant changes in the timestring list?
	mainframe.getTimeStringTab().writeSelectionChanges();
	if (!isCalculationPossible(true)) { // are there enough points/freqs?
	    return;
	} 
	int mode = getCalcMode();     // get calculation mode (orig./adj.)
	cleanErrorList();             // clean the errors
	//---
	//--- show dialog if necessary
	//---
	if (getFitMode()!=BINARY_MODE && getSelectionMode()!=0) {
	    String [] options = {"Yes","No","Cancel"};
	    int value = JOptionPane.showOptionDialog(mainframe.getFrame(),
		"If frequency and phase are fitted simultaneously then the\n"+
                "uncertainties of frequencies and phases are correlated.\n"+
		"In the current mode, Period04 provides the possibility\n"+
		"to calculate uncorrelated uncertainties\n"+
		"(Montgomery & Odonoghue, 1999, DSSN, 13, 28).\n"+
		"Do you want to calculate uncorrelated uncertainties?",
		"Please note:", 
		JOptionPane.YES_NO_CANCEL_OPTION, 
		JOptionPane.INFORMATION_MESSAGE,
		null, options, options[0]);
	    if (value==JOptionPane.YES_OPTION) {
		Period04.projectSetShiftTimes(true);
	    } else if (value==JOptionPane.NO_OPTION) {
		Period04.projectSetShiftTimes(false);
	    } else { return; }
	} else { // frequency and phase are not fitted simultaneously
	    Period04.projectSetShiftTimes(false);
	}
	Period04.projectSetCalcErrors(true);
	//--- use the old selection mode
	int selMode = getSelectionMode(); 
	if (selMode==-1) // no calculation yet
	    setSelectionMode(FRE_AMP_PHA);
	//---
	//--- show the calculation panel while we're calculating
	//--- and set the time interval [ms] to update the progressbar
	//---
	mainframe.setCalculating(
	    true, getCalcSettingsPanel(false),
	    "Calculating Least-Squares Fit ...", /*interval=*/300, false);
	//---
	//--- initialize the calculation task
	//---
	task = new FitTask(mainframe, this, mode, selectionmode,
			   store_varmode, store_attribute, 
			   store_selectedindices);	    
	task.go();                  // start calculation
	mainframe.startTimer(task); // start the timer for the progressbar
    }

    private void calculateMonteCarloErrors() {
	if (task!=null) { return; }   // last task is not yet finished, return
	//---
	//--- prepare for calculation
	//---
	setFrequencyListData();	        // update data
	//--- have there been relevant changes in the timestring list?
	mainframe.getTimeStringTab().writeSelectionChanges();
	if (!isCalculationPossible(true)) { // are there enough points/freqs?
	    return;
	} 
	cleanErrorList();             // clean the errors
	//---
	//--- ask user to enter the number of monte carlo iterations
	//---
	XDialogMonteCarlo dialog = new XDialogMonteCarlo(mainframe);
	if (dialog.isCanceled()) { return; }
	int     processes  = dialog.getValidatedNumber();
 	boolean shiftTimes = dialog.isShiftTimes();
	boolean sysTime   = dialog.isInitRand();
	//---
	//--- get/set settings
	//---
	int selMode = getSelectionMode(); // use the old selection mode
	if (selMode==-1) // no calculation yet
	    setSelectionMode(AMP_PHA);
	if (shiftTimes)  // shift times by time average?
	    Period04.projectSetShiftTimes(true);
	// initialize rand using system time if wanted
	Period04.projectInitRand(sysTime);
	Period04.projectSetCalcErrors(true);
	Period04.projectSetMoCaSimulations(processes);// set # of iterations
	Period04.projectSetInitialCalcMode(           // set the calcmode for
	    getCalcMode());                           // the initial LS calc.

	//---
	//--- show the calculation panel while we're calculating
	//--- and set the time interval [ms] to update the progressbar
	//---
	mainframe.setCalculating(
	    true, getCalcSettingsPanel(true),
	    "Monte Carlo Simulation ...", /*interval=*/300, false);
	//---
	//--- initialize the calculation task
	//---
	task = new FitTask(mainframe, this, SIMULATED, selectionmode,
			   store_varmode, store_attribute, 
			   store_selectedindices);
	task.go();                  // start calculation
	mainframe.startTimer(task); // start the timer for the progressbar
    }

    public void calculateMonteCarloErrorsBatch(int processes, boolean shiftTimes, boolean sysTime) {
	if (task!=null) { return; }   // last task is not yet finished, return
	//---
	//--- prepare for calculation
	//---
	batchMode = true;
	//setFrequencyListData();	        // update data => not required because no user-set values are expected
	//--- have there been relevant changes in the timestring list?
//	mainframe.getTimeStringTab().writeSelectionChanges();
	if (!isCalculationPossible(true)) { // are there enough points/freqs?
	    return;
	} 
	cleanErrorList();             // clean the errors
	//---
	//--- get/set settings
	//---
	int selMode = getSelectionMode(); // use the old selection mode
	if (selMode==-1) // no calculation yet
	    setSelectionMode(AMP_PHA);
	if (shiftTimes)  // shift times by time average?
	    Period04.projectSetShiftTimes(true);
	// initialize rand using system time if wanted
	Period04.projectInitRand(sysTime);
	Period04.projectSetCalcErrors(true);
	Period04.projectSetMoCaSimulations(processes);// set # of iterations
	Period04.projectSetInitialCalcMode(           // set the calcmode for
	    getCalcMode());                           // the initial LS calc.

	//---
	//--- show the calculation panel while we're calculating
	//--- and set the time interval [ms] to update the progressbar
	//---
	mainframe.setCalculating(
	    true, getCalcSettingsPanel(true),
	    "Monte Carlo Simulation ...", /*interval=*/300, false);
	//---
	//--- initialize the calculation task
	//---
	task = new FitTask(mainframe, this, SIMULATED, selectionmode,
			   store_varmode, store_attribute, 
			   store_selectedindices);
	task.go();                  // start calculation
	mainframe.startTimer(task); // start the timer for the progressbar
	task.get(); // wait for task to finish!
    }

    public void resetErrorCalculation() {
	Period04.projectSetCalcErrors(false);
	Period04.projectSetShiftTimes(false);
	tabbedPane.setSelectedComponent(errorPanel);
    }
    
    //-b- ------------------------------------------------ ---//
    //---                                                  ---//
    //---    methods for the periodic time shifts panel    ---//
    //---         (double star mode, binary mode)          ---//
    //---                                                  ---//
    //--- ------------------------------------------------ ---//
 
    //-b-
    //--- create an additional panel for periodic time shift calculations
    //---
    private void createBMPanel() {
	//---
	//--- create labels for the binary frequency
	//--- 
	JLabel l_FreqBM = createLabel("Frequency");
	JLabel l_AmpBM = createLabel("Amplitude");
	JLabel l_PhaseBM = createLabel("Phase");
	l_FreqBM   .setBounds( 80, 5, 110, 15 );
	l_AmpBM    .setBounds(210, 5, 110, 15 );
	l_PhaseBM  .setBounds(340, 5, 110, 15 );
	bmPanel.add( l_FreqBM, null);
	bmPanel.add( l_AmpBM, null);
	bmPanel.add( l_PhaseBM, null);
	//---
	//--- create a disabled selected checkbox for the binary frequency
	//---
	JCheckBox c_BMFrequency = createCheckBox("PTSF");
	c_BMFrequency.setMargin(new Insets(0,0,0,0));
//	c_BMFrequency.setBackground(new Color(230,225,217));
	c_BMFrequency.setSelected(true);
	c_BMFrequency.setEnabled(false);
	c_BMFrequency.setBounds(   2, 20, 50, 15 );
	bmPanel.add( c_BMFrequency, null );
	//---
	//--- create textboxes for the binary frequency
	//---
	t_BMFreq = createTextField(Period04.projectGetBMFrequency(-1));
	t_BMAmp  = createTextField(Period04.projectGetBMAmplitude(-1));
	t_BMPha  = createTextField(Period04.projectGetBMPhase(-1));
	// add listeners for text changes
	TextChangeListener textlistener = new TextChangeListener();
	t_BMFreq.getDocument().addDocumentListener(textlistener);
	t_BMFreq.getDocument().putProperty("name", t_BMFreq);
	t_BMFreq.getDocument().putProperty("type", "Frequency");
	t_BMAmp.getDocument().addDocumentListener(textlistener);
	t_BMAmp.getDocument().putProperty("name", t_BMAmp);
	t_BMAmp.getDocument().putProperty("type", "Amplitude");
	t_BMPha.getDocument().addDocumentListener(textlistener);
	t_BMPha.getDocument().putProperty("name", t_BMPha);
	t_BMPha.getDocument().putProperty("type", "Phase");
	t_BMFreq.setBounds(  80, 20, 110, 17 );
	t_BMAmp .setBounds( 210, 20, 110, 17 );
	t_BMPha .setBounds( 340, 20, 110, 17 );
	bmPanel.add( t_BMFreq, null);
	bmPanel.add( t_BMAmp,  null);
	bmPanel.add( t_BMPha,  null);
	//---
	//--- pts calculation buttons
	//---
	b_BMCalcStartValues = createCalcButton("Search PTS start values");
	b_BMCalcStartValues.setToolTipText(
	    "<html>Searches start values for the<br>"+
	    "periodic time shift parameters<br>"+
	    "by means of Monte Carlo shots<br>"+
	    "within a user-defined range.</html>");
	b_BMImprovePTS = createCalcButton("Improve PTS");
	b_BMImprovePTS.setToolTipText(
	    "<html>Improve the periodic time shift parameters</html>");
	b_BMCalcStartValues.setBounds(  80, 42, 200, 20 );
	b_BMImprovePTS     .setBounds( 300, 42, 150, 20 );
	bmPanel.add( b_BMCalcStartValues, null );
	bmPanel.add( b_BMImprovePTS, null );

	bmPanel.setBounds(0,0,485,70);
   }

    private void updateBMData() {
	blockChangeEvents=true;
	// update the periodic time shift frequency (index=-1)
	t_BMFreq.setText(Period04.projectGetBMFrequency(-1));
	t_BMAmp .setText(Period04.projectGetBMAmplitude(-1));
	t_BMPha .setText(Period04.projectGetBMPhase(-1));
	blockChangeEvents=false;
    }
    
    private void setBMData(JTextField tf, String type) {
	if (!blockChangeEvents) {
	    if (type=="Frequency") {
		Period04.projectSetBMFrequency(-1, tf.getText());
	    } else if (type=="Amplitude") {
		Period04.projectSetBMAmplitude(-1, tf.getText());
	    } else if (type=="Phase") {
		Period04.projectSetBMPhase(-1, tf.getText());
	    }
	    return;
	}
    }
 
    public void displayPhasePlot() {
	setFrequencyListData();	// get data
	if (plot!=null) { // there is already an active plot!
	    plot.toFront(); return;
	}
	plot = new XPlotPhase(mainframe); // create a phase plot
    }

    public void updatePhasePlot() {
	if (plot!=null) {
	    plot.update();
	}
    }
    
    public void dereferencePhasePlot() {
	plot = null;
    }

    //---
    //--- a method to check if it is possible to start a calculation
    //---
    public boolean isCalculationPossible(boolean checkFrequencies) {
	if (Period04.projectGetSelectedPoints()==0) {
	    JOptionPane.showMessageDialog(
		mainframe.getFrame(), "The number of selected points\n"+
		     "in the timestring is to low...", "", 
		JOptionPane.ERROR_MESSAGE);
	    return false;
	}
	// are there enough frequencies?
	if (checkFrequencies) {
	    int freqs;
	    if (fitmode==BINARY_MODE) {
		freqs = Period04.projectGetBMActiveFrequencies();
	    } else { // NORMAL_MODE
		freqs = Period04.projectGetActiveFrequencies();
	    }
	    if (freqs==0) {
		JOptionPane.showMessageDialog(
		    mainframe.getFrame(), "There are no active frequencies", "Error",
		    JOptionPane.ERROR_MESSAGE);
		return false;
	    }
	}
	if (fitmode==BINARY_MODE) {
	    double limit  = 1.0/Period04.projectGetTotalTimeLength();
	    double bmfreq = Double.parseDouble(t_BMFreq.getText());
	    //--- check whether the pts frequency is to low.
	    //--- if the frequency is zero then the pts is not going 
	    //--- to be fitted, therefore this dialog is not shown
	    if (bmfreq!=0.0 && bmfreq<0.95*limit) {
		int v = JOptionPane.showConfirmDialog(
		    mainframe.getFrame(), "Using a periodic time shift frequency of "+
		    format(bmfreq)+"\nmay procuce unreliable results!\n"+
		    "The current time string has a total time base\nof "+
		    format(Period04.projectGetTotalTimeLength())+". "+
		    "It is recommended not to use\nfrequency values below"+
		    " the limit of "+format(limit)+
		    "\nfor a periodic time shift to obtain proper results.\n"+
		    "Do you really want to proceed?", 
		    "Warning", JOptionPane.YES_NO_OPTION);
		if (v==JOptionPane.NO_OPTION) {
		    return false;
		}
	    }
	}
    	return true;
    }
    
    //---
    //--- getCalcSettingsPanel
    //--- creates the panel that shows the calculation settings
    //--- while the program is calculating
    //---
    public JPanel getCalcSettingsPanel(boolean isMonteCarlo) {
	JPanel panel = new JPanel();
	panel.setLayout(null);
	panel.setOpaque(false);
	panel.setBorder(BorderFactory.createCompoundBorder(
		BorderFactory.createTitledBorder(
		    BorderFactory.createEtchedBorder(),
		    " Calculation settings: ",
		    TitledBorder.LEADING, TitledBorder.TOP,
		    new Font("Dialog", Font.BOLD, 12),
		    new Color(20,30,150) ),
		BorderFactory.createEmptyBorder(5,5,5,5))
	    );

	JLabel l_weights       = new JLabel("Use weights: ");
	JLabel l_weights_f     = new JLabel(Period04.projectGetWeightString());
	JLabel l_calcmode      = new JLabel("Calculation based on: ");
	JLabel l_calcmode_f;
	if (isMonteCarlo) { // Monte Carlo simulation
	    l_calcmode_f = new JLabel("Simulated Data");
	} else {
	    l_calcmode_f = new JLabel(getCalcModeString());
	}

	int x0 = 15;
	int x1 = x0+140;
	l_weights      .setBounds( x0, 100, 150, 20);
	l_weights_f    .setBounds( x1, 100, 150, 20);
	l_calcmode     .setBounds( x0, 120, 150, 20);
	l_calcmode_f   .setBounds( x1, 120, 150, 20);

	switch (getSelectionMode()) {
	    case 0: {		
		JLabel l_mode          = new JLabel("Improving: ");
		JLabel l_mode_f1     = new JLabel("all Amplitudes");
		JLabel l_mode_f2     = new JLabel("all Phases");
		l_mode   .setBounds( x0, 30, 150, 20);
		l_mode_f1.setBounds( x1, 30, 150, 20);
		l_mode_f2.setBounds( x1, 50, 150, 20);
		panel.add(l_mode);
		panel.add(l_mode_f1);
		panel.add(l_mode_f2);
		break;
	    }
	    case 1: {		
		JLabel l_mode          = new JLabel("Improving: ");
		JLabel l_mode_f0     = new JLabel("all Frequencies");
		JLabel l_mode_f1     = new JLabel("all Amplitudes");
		JLabel l_mode_f2     = new JLabel("all Phases");
		l_mode   .setBounds( x0, 20, 150, 20);
		l_mode_f0.setBounds( x1, 20, 150, 20);
		l_mode_f1.setBounds( x1, 40, 150, 20);
		l_mode_f2.setBounds( x1, 60, 150, 20);
		panel.add(l_mode);
		panel.add(l_mode_f0);
		panel.add(l_mode_f1);
		panel.add(l_mode_f2);
		break;
	    }
	    case 2: {
		JLabel l_mode    = new JLabel("Improving: ");
		JLabel l_mode_f0 = new JLabel("a custom selection");
		JLabel l_mode_f1 = new JLabel("of parameters");
		l_mode   .setBounds( x0, 20, 150, 20);
		l_mode_f0.setBounds( x1, 20, 150, 20);
		l_mode_f1.setBounds( x1, 40, 150, 20);
		panel.add(l_mode);
		panel.add(l_mode_f0);
		panel.add(l_mode_f1);
		break;
	    }
	    case 3: {
		JLabel l_mode    = new JLabel("Improving: ");
		JLabel l_mode_f0 = new JLabel("amplitude/phase");
		JLabel l_mode_f1 = new JLabel("variations");
		l_mode   .setBounds( x0, 20, 150, 20);
		l_mode_f0.setBounds( x1, 20, 150, 20);
		l_mode_f1.setBounds( x1, 40, 150, 20);
		panel.add(l_mode);
		panel.add(l_mode_f0);
		panel.add(l_mode_f1);
		break;
	    }
	    case 4: {
		JLabel l_freq    = new JLabel("Frequency range:");
		JLabel l_freq_r  = new JLabel("[ "+mcDialog.getLowFreq()+
					      " , "+mcDialog.getUpFreq()+" ]");
		JLabel l_amp     = new JLabel("Amplitude range:");
		JLabel l_amp_r   = new JLabel("[ "+mcDialog.getLowAmp()+
					      " , "+mcDialog.getUpAmp()+" ]");
		JLabel l_shots   = new JLabel("Shots:");
		JLabel l_shots_r = new JLabel((new Integer(mcDialog.getShots())).toString());
		l_freq   .setBounds( x0, 20, 150, 20);
		l_freq_r .setBounds( x1, 20, 150, 20);
		l_amp    .setBounds( x0, 40, 150, 20);
		l_amp_r  .setBounds( x1, 40, 150, 20);
		l_shots  .setBounds( x0, 60, 150, 20);
		l_shots_r.setBounds( x1, 60, 150, 20);
		panel.add(l_freq);
		panel.add(l_freq_r);
		panel.add(l_amp);
		panel.add(l_amp_r);
		panel.add(l_shots);
		panel.add(l_shots_r);
		break;
	    }
	    case 5: {
		JLabel l_mode    = new JLabel("Improving: ");
		JLabel l_mode_f0 = new JLabel("all periodic-time-shift");
		JLabel l_mode_f1 = new JLabel("parameters");
		l_mode   .setBounds( x0, 20, 150, 20);
		l_mode_f0.setBounds( x1, 20, 150, 20);
		l_mode_f1.setBounds( x1, 40, 150, 20);
		panel.add(l_mode);
		panel.add(l_mode_f0);
		panel.add(l_mode_f1);
		break;
	    }
	}
	
	panel.add(l_weights);
	panel.add(l_weights_f);
	panel.add(l_calcmode);
	panel.add(l_calcmode_f);

	return panel;
    }

    //-c- ------------------------------------------------ ---//
    //---                                                  ---//
    //---  methods for the initialization of calculations  ---//
    //---         (for all modes of calculations)          ---//
    //---                                                  ---//
    //--- ------------------------------------------------ ---//
 
    public void calculateFitBatch(String swhat, String smode) {
	int mode=-1;
	switch(swhat.charAt(0)){
	case 'o': mode=0; break;
	case 'a': mode=1; break;
	}
	if (mode==-1)
	    return;
	switch(smode.charAt(0)){
	case '1': setSelectionMode(AMP_PHA); break;
	case '2': setSelectionMode(FRE_AMP_PHA); break;
	}
	batchMode = true;
        setFrequencyListData();       
	
	//--- have there been relevant changes in the timestring list?
	mainframe.getTimeStringTab().writeSelectionChanges();
	if (!isCalculationPossible(false)) { // are there enough points/freqs?
	    return;
	} 
	cleanErrorList();             // clean the errors
	//---
	//--- show the calculation panel while we're calculating
	//--- and set the time interval [ms] to update the progressbar
	//---
	mainframe.setCalculating(
	    true, getCalcSettingsPanel(false),
	    "Calculating Least-Squares Fit ...", /*interval=*/300, false);
	//---
	//--- initialize the calculation task
	//---
	task = new FitTask(mainframe, this, mode, selectionmode);
	task.go();                  // start calculation
	mainframe.startTimer(task); // start the timer for the progressbar
	task.get(); // wait for task to finish!
    }

    public void calculate() { 
	if (task!=null) { return; }   // last task is not yet finished, return
	setSelectionMode(AMP_PHA);    // set the selection mode
	int mode = getCalcMode();     // get calculation mode (orig./adj.)
	setFrequencyListData();       // update data 
	//--- have there been relevant changes in the timestring list?
	mainframe.getTimeStringTab().writeSelectionChanges();
	if (!isCalculationPossible(false)) { // are there enough points/freqs?
	    return;
	} 
	cleanErrorList();             // clean the errors
	//---
	//--- show the calculation panel while we're calculating
	//--- and set the time interval [ms] to update the progressbar
	//---
	mainframe.setCalculating(
	    true, getCalcSettingsPanel(false),
	    "Calculating Least-Squares Fit ...", /*interval=*/300, false);
	//---
	//--- initialize the calculation task
	//---
	task = new FitTask(mainframe, this, mode, selectionmode);
	task.go();                  // start calculation
	mainframe.startTimer(task); // start the timer for the progressbar
    }

    public void searchPTSStartValues() {
	if (task!=null) { return; }   // last task is not yet finished, return
	setSelectionMode(BM_START);   // set the selection mode
	int mode = getCalcMode();     // get calculation mode (orig./adj.)
	t_BMFreq.setText("0");        // set pts frequency to zero to pass over 
	                              // a warning dialog in isCalulationPossible()
	setFrequencyListData();       // update data 
	//--- have there been relevant changes in the timestring list?
	mainframe.getTimeStringTab().writeSelectionChanges();
	if (!isCalculationPossible(true)) { // are there enough points/freqs?
	    return;
	}
	//---
	//--- ask user for appropriate ranges for our search
	//---
	mcDialog = new XDialogMCInput(mainframe);
	if (mcDialog.isCanceled()) { return; }
	Period04.projectSetMCRange(mcDialog.getLowFreq(), mcDialog.getUpFreq(),
				   mcDialog.getLowAmp(), mcDialog.getUpAmp());
	//---
	//--- show the calculation panel while we're calculating
	//--- and set the time interval [ms] to update the progressbar
	//---
	mainframe.setCalculating(
	    true, getCalcSettingsPanel(false),
	    "Searching for start values ...", /*interval=*/300, true);
	//---
	//--- initialize the calculation task
	//---
	task = new FitTask(mainframe, this, mode, selectionmode, mcDialog.getShots());
	task.go();                  // start calculation
	mainframe.startTimer(task); // start the timer for the progressbar
    }

    public void improvePTS() {
	if (task!=null) { return; }   // last task is not yet finished, return
	setSelectionMode(BM_VAR);     // set the selection mode
	int mode = getCalcMode();     // get calculation mode (orig./adj.)
	setFrequencyListData();	      // update data
	//--- have there been relevant changes in the timestring list?
	mainframe.getTimeStringTab().writeSelectionChanges();
	if (!isCalculationPossible(true)) { // are there enough points/freqs?
	    return;
	} 
	cleanErrorList();             // clean the errors
	//---
	//--- show the calculation panel while we're calculating
	//--- and set the time interval [ms] to update the progressbar
	//---
	mainframe.setCalculating(
	    true, getCalcSettingsPanel(false),
	    "Calculating Least-Squares Fit ...", /*interval=*/300, false);
	//---
	//--- initialize the calculation task
	//---
	task = new FitTask(mainframe, this, mode, selectionmode);
	task.go();                  // start calculation
	mainframe.startTimer(task); // start the timer for the progressbar
    }

    public void improveAll() {
	if (task!=null) { return; }   // last task is not yet finished, return
	setSelectionMode(FRE_AMP_PHA);// set the selection mode
	int mode = getCalcMode();     // get calculation mode (orig./adj.)
	setFrequencyListData();	      // update data
	//--- have there been relevant changes in the timestring list?
	mainframe.getTimeStringTab().writeSelectionChanges();
	if (!isCalculationPossible(false)) { // are there enough points/freqs?
	    return;
	} 
	cleanErrorList();             // clean the errors
	//---
	//--- show the calculation panel while we're calculating
	//--- and set the time interval [ms] to update the progressbar
	//---
	mainframe.setCalculating(
	    true, getCalcSettingsPanel(false),
	    "Calculating Least-Squares Fit ...", /*interval=*/300, false);
	//---
	//--- initialize the calculation task
	//---
	task = new FitTask(mainframe, this, mode, selectionmode);
	task.go();                  // start calculation
	mainframe.startTimer(task); // start the timer for the progressbar
    }

    public void improveSpecial() {
	if (task!=null) { return; }   // last task is not yet finished, return
	setSelectionMode(SPECIAL);    // set the selection mode
	int mode = getCalcMode();
	setFrequencyListData();	      // update data
	//--- have there been relevant changes in the timestring list?
	mainframe.getTimeStringTab().writeSelectionChanges();
	if (!isCalculationPossible(true)) { // are there enough points/freqs?
	    return;
	} 
	cleanErrorList();             // clean the errors
	//---
	//--- ask the user to make a selection
	//---
	XDialogImproveSpecial dialog = new XDialogImproveSpecial(
	    mainframe.getFrame(), fitmode, "Calculate");
	if (dialog.isCanceled()) { return; } // cancel calculation
	//---
	//--- show the calculation panel while we're calculating
	//--- and set the time interval [ms] to update the progressbar
	//---
	mainframe.setCalculating(
	    true, getCalcSettingsPanel(false),
	    "Calculating Least-Squares Fit ...", /*interval=*/300, false);
	//---
	//--- initialize the calculation task
	//---
	task = new FitTask(mainframe, this, mode, selectionmode);
	task.go();                  // start calculation
	mainframe.startTimer(task); // start the timer for the progressbar
	return;
    }
 
    public void calculateAmpVar() {
	if (task!=null) { return; }   // last task is not yet finished, return
	if (ampvarDialog!=null) { return; } // the dialog is already visible
	setSelectionMode(AMP_VAR);    // set the selection mode
	int mode = getCalcMode();
	setFrequencyListData();	      // update data
	//--- have there been relevant changes in the timestring list?
	mainframe.getTimeStringTab().writeSelectionChanges();
	if (!isCalculationPossible(true)) { // are there enough points/freqs?
	    return;
	} 
	cleanErrorList();             // clean the errors
	ampvarDialog = new XDialogAmplitudeVariation(mainframe, this, mode);
    }

    public void calculateEpoch() {
	setFrequencyListData(); // update data
	//--- have there been relevant changes in the timestring list?
	mainframe.getTimeStringTab().writeSelectionChanges();
	new XDialogEpoch(mainframe);
    }

    public void recalcResiduals() {
	setFrequencyListData();	// update data
	//--- have there been relevant changes in the timestring list?
	mainframe.getTimeStringTab().writeSelectionChanges();
	String zero = Period04.projectGetZeropoint();
	XDialogInput dialog = new XDialogInput(
	    mainframe.getFrame(), "Refit data:", "Zeropoint for refit:", zero,
	    1 /*numbercheck*/);
	Double d_zero = dialog.getValidatedNumber();
	if (d_zero == null) { return; }  // cancel has been pressed
	Period04.projectRefit(d_zero.doubleValue());
	updateDisplay(false);

	// auch ts display updaten ?????????????

	StringBuffer text = new StringBuffer();
	text.append("Recalculate Residuals:\n"+
		    "Recalculating the residuals for the "+
		    "currently selected data points\n"+
		    "according to the following fit:");
	mainframe.getLog().writeProtocol(text.toString(),true);
	writeFrequenciesToProtocol(false);
	text = new StringBuffer();
	if (fitmode==XTabFit.NORMAL_MODE) {
	    text.append("Zeropoint:  "+Period04.projectGetZeropoint()+"\n");
	    text.append("Residuals:  "+Period04.projectGetResiduals()+"\n");
	} else if (fitmode==XTabFit.BINARY_MODE) {
	    text.append("Zeropoint:  "+Period04.projectGetBMZeropoint()+"\n");
	    text.append("Residuals:  "+Period04.projectGetBMResiduals()+"\n");
	}
	mainframe.getLog().writeProtocol(text.toString(),false);
    }

    public void predictSignal() {
	int points = Period04.projectGetSelectedPoints();
	if (points==0) {
	    JOptionPane.showMessageDialog(mainframe.getFrame(), "No points selected ...",
					  "", JOptionPane.ERROR_MESSAGE);
	    return;
	}
	new XDialogPredictSignal(mainframe, this);
    }

    public void createArtificialData() {
	new XDialogCreateData(mainframe, this);
    }

    public void createArtificialDataBatch(boolean useFile, String infile,
					  String s_starttime, String s_endtime, String s_step, String s_leading,
					  String outfile, String sappend) {
	batchMode = true;
	boolean append = false;
	if (sappend=="a")
	{
	    append=true;
	}

	boolean isRangeInFile = false;
	    
      // create a task for output
      FileTask task;
				if (useFile) {
				    task = new FileTask(
					mainframe, 
					FileTask.ARTIFICIAL_DATA_FILE,
					outfile, infile,
					isRangeInFile,
					Double.parseDouble(s_step), 
					Double.parseDouble(s_leading), 
					append);
				} else {
				    task = new FileTask(
					mainframe, FileTask.ARTIFICIAL_DATA,
					outfile, 
					Double.parseDouble(s_starttime), 
					Double.parseDouble(s_endtime),
					Double.parseDouble(s_step), 
					Double.parseDouble(s_leading),
					append);
				}
      task.go();
      mainframe.startTimer(task); // start the timer for the progressbar
      task.get(); // wait for task to finish!
    }	
    
    public void showAnalyticalErrors() {
	if (!isCalculationPossible(true)) { return; }
	if (getFitMode()==0) { new XDialogAnalyticalErrors(mainframe); }
	else {
	    JOptionPane.showMessageDialog(
		mainframe.getFrame(),
		"This option is not available for the\n"+
		"periodic time shift mode!",
		"Please note:", JOptionPane.INFORMATION_MESSAGE);
	}
    }

    public void updateDisplay(boolean clearFreqList) {
	//---
	//--- update the appropriate data 
	//---
	if (fitmode==NORMAL_MODE) {
	    l_Selected.setText(
		Period04.projectGetActiveFrequenciesString());    // # active freqs
	    l_Zero     .setText(Period04.projectGetZeropoint());    // zeropoint
	    l_Residuals.setText(Period04.projectGetResiduals());    // residuals
	    t_Weights  .setText(Period04.projectGetWeightString()); // set Weight
	} else if (fitmode==BINARY_MODE) { 
	    l_Selected.setText(
		Period04.projectGetBMActiveFrequenciesString());   // active freqs
	    l_Zero.setText(Period04.projectGetBMZeropoint());      // zeropoint
	    l_Residuals.setText(Period04.projectGetBMResiduals()); // residuals
	    t_Weights.setText(Period04.projectGetWeightString());  // set Weight
	}
	//---
	//--- set the appropriate tab:
	//--- if error panel is active AND no error calculation has 
	//--- been done, switch to the main frequency panel
	//---
	if (tabbedPane.getSelectedIndex()==1 && 
	    !Period04.projectIsCalcErrors()) {
	    tabbedPane.setSelectedIndex(0); 
	}
	//---
	//--- update the frequency list
	//---
	if (clearFreqList) {
	    cleanFrequencyList(true);  // remove all data from frequency list
	}
	updateFrequencyListData();        // update main frequency list
	updatePhasePlot();             // update the phase plot
	updateErrorPanel();            // finally update the uncertainties
	mainframe.getFourierTab().updateNoiseDialogDisplay(); // update frequencies in the noise dialog
    }

    //--- add harmonics of frequency with ID to the frequency list
    //---
    public void addHarmonics(int ID) {
	XDialogInput dialog = new XDialogInput(
	      mainframe.getFrame(), "Add harmonics", "Add harmonics up to:",
	      "5", 1 /*numbercheck*/);
	Integer i_maxnum = dialog.getValidatedIntegerNumber();
	if (i_maxnum == null) { return; }  // cancel has been pressed
	int maxnum = i_maxnum.intValue();
	if (maxnum<=1) 
	    return;

	int missingfields=maxnum-freqlist.size();
	while (missingfields>0)
	{
		addEmptyFrequencyEntry();
		missingfields--;
	}

	int inum=2;
	for ( JFrequencyListEntry e : freqlist ) {
	    if (inum>maxnum) break;
	    if (e.isEmpty()) {
		String ssnum= "="+inum+"f"+Integer.toString(ID+1);
		if (getFitMode()==XTabFit.BINARY_MODE) {
		    // check if we have to resize the period array
		    if ( e.getID()>=Period04.projectGetBMTotalFrequencies()) {
			Period04.projectSetBMTotalFrequencies(e.getID());
		    }
		    Period04.projectSetBMFrequency( e.getID(),ssnum);
		    Period04.projectSetBMActive( e.getID(),true);
		} else { // NORMAL_MODE
		    // check if we have to resize the period array
		    if ( e.getID()>=Period04.projectGetTotalFrequencies()) {
			Period04.projectSetTotalFrequencies(e.getID());
		    }
		    Period04.projectSetFrequency( e.getID(),ssnum);
		    Period04.projectSetActive( e.getID(),true);
		}
		inum++;
	    }
	}
	updateFrequencyListData();
    }

    public void addHarmonicsBatch(String snum) {
	batchMode = true;
	int num = Integer.parseInt(snum);
	if (num<=1) 
	    return;
	int inum=2;
	for ( JFrequencyListEntry e : freqlist ) {
	    if (inum>num) break;
	    if (e.isEmpty()) {
		String ssnum= "="+inum+"f1";
		Period04.projectSetFrequency( e.getID(),ssnum);
		Period04.projectSetActive( e.getID(),true);
		inum++;
	    }
	}
	updateFrequencyListData();
    }

    public void addSubHarmonicBatch(String snum) {
	batchMode = true;
	int num = Integer.parseInt(snum);
	if (num<=1) 
	    return;
	for ( JFrequencyListEntry e : freqlist ) {
	    if (e.isEmpty()) {
		String ssnum= "="+(1.0/num)+"f1";
		Period04.projectSetFrequency( e.getID(),ssnum);
		Period04.projectSetActive( e.getID(),true);
		break;
	    }
	}
	updateFrequencyListData();
    }

    public void exportFrequenciesBatch(String file) {
	batchMode = true;
	setFrequencyListData();	// update the data
	if (fitmode==BINARY_MODE) {
	    if (Period04.projectGetBMTotalFrequencies()==0) { return; }
	} else { // NORMAL_MODE
	    if (Period04.projectGetTotalFrequencies()==0) { return; }
	} 
	
	// now go and save it
	Period04.projectSaveFrequencies(file, fitmode, false);
	// write to log
	mainframe.getLog().writeProtocol(
		  	 "Saved frequencies:\n"+
     			 "Saved frequencies in file:\n"+file+"\n",true);
    }

    public void updateWeights() {
	t_Weights.setText(Period04.projectGetWeightString());
    }

    public void updateErrorPanel() {
	//--- do not update this after each fit, but only after 
	//--- error calculations
	if (Period04.projectIsCalcErrors() || getFitMode()==BINARY_MODE) {
	    t_errors.setText(Period04.projectGetFitError());
	}
    }

    public void updateAmpVarDialog(String protocol) {
	//--- only update this dialog if it makes sense
	if (ampvarDialog==null || Period04.projectIsCalcErrors()) { return; }
	// cut off the first 3 lines (they are only for the log)
	String searchstring = "F"+Period04.projectGetFirstFrequencyNumber();
	int cut = protocol.indexOf(searchstring);
	if (cut==-1) {
	    // calculation must have failed or something strange happened
	    ampvarDialog.updateDisplay(protocol);
	} else {
	    ampvarDialog.updateDisplay(
		"   "+protocol.subSequence(cut, protocol.length()));
	}
    }
	
    //---
    //--- removes the old sigmas
    //---
    public void cleanErrorList() {
	Period04.projectCleanSigmas();
	t_errors.setText(
	    " ---------------------------------------------------------\n"+
	    " --          Results of the Least-Squares-Fit:          --\n"+
	    " ---------------------------------------------------------\n"); 
    }

    public void updateFrequencyFieldNumber() {
	int nEmpty=0;
	for ( JFrequencyListEntry e : freqlist ) {
	    if (e.isEmpty()) {nEmpty++;}
	    else { nEmpty=0; }
	}
	while (nEmpty<20) {
	    addEmptyFrequencyEntry();
	    nEmpty++;
	}
    }

    public void updateFrequencyListData() {
	if (fitmode==BINARY_MODE) {
	    updateBMData();
	}
	for (int i=0; i<Period04.projectGetTotalFrequencies(); i++) {
	    if (i>=freqlist.size()) {
		addEmptyFrequencyEntry();
	    }
	    ((JFrequencyListEntry)((ArrayList)freqlist).get(i)).updateData();
	}
	updateFrequencyFieldNumber(); // make sure we have enough empty fields
        Dimension s=frequencyListPanel.getSize();
	s.height=64+freqlist.size()*22;
	frequencyListPanel.setPreferredSize(s);
	frequencyListPanel.revalidate();
	setFrequencyListData();
    }

    public void setFrequencyListData() {
	synchronized (mutex) {
	for ( Iterator<JFrequencyListEntry> iterator = freqlist.iterator(); iterator.hasNext(); ) {
	    JFrequencyListEntry e = iterator.next();
	    if (e.getID()<Period04.projectGetTotalFrequencies()) {
		e.setData();
	    }
	}
	}
    }
    
    public void selectAllFrequencies() {
	for ( JFrequencyListEntry e : freqlist ) 
	    e.setSelected(true);
    }

    public void deselectAllFrequencies() {
	for ( JFrequencyListEntry e : freqlist ) 
	    e.setSelected(false);
    }

    //---
    //--- removes all data from unused frequency list entries
    //---
    public void cleanDeselectedFrequencyEntries() {
	for ( JFrequencyListEntry e : freqlist ) {
	    if (e.isUnused()) { e.clean(); }
	}
    }

   //---
    //--- removes all data from the frequency list
    //---
    public void cleanFrequencyList(boolean cleanAllLists) {
	for ( JFrequencyListEntry e : freqlist ) {
	    if (!e.isEmpty())
		e.clean();
	}
    }

    //---
    //--- remove all frequency data in active mode
    //---
    public void cleanAllFrequencies() {
	cleanFrequencyList(false);
	Period04.projectCleanPeriod();
	Period04.projectSetTotalFrequencies(freqlist.size());
	updateErrorPanel();
    }

    public void setUseCombiFreqs(boolean val) {
	for ( JFrequencyListEntry e : freqlist ) {
	    if (e.getID()<Period04.projectGetTotalFrequencies())
		if (e.isComposition()) {
		    e.showComposition(val);
		}
	}
    }
 
    //---
    //--- methods to retrieve the calculation mode (observed or adjusted data)
    //---
    public int getCalcMode() {
	int mode = 0;
	if      (checkCalcMode[0].isSelected()) { mode=OBSERVED; }
	else if (checkCalcMode[1].isSelected()) { mode=ADJUSTED; }
	lastCalculation = mode;
	return mode;
    }
    public String getCalcModeString() {
	if      (checkCalcMode[0].isSelected()) { return "Observed Data"; }
	else if (checkCalcMode[1].isSelected()) { return "Adjusted Data"; }
	return "";
    }
    public int getLastCalculationID() {	return lastCalculation; }
    
    //---
    //--- methods to set the selection mode (AMP_PHA, FRE_AMP_PHA, SPECIAL)
    //---
    public int  getSelectionMode()      { return selectionmode; }
    public void setSelectionMode(int i) { 
	selectionmode = i;   
	Period04.projectSetFitSelectionMode(i);
    }
    
    //---
    //--- a method to update the data mode (called when loading a project)
    //---
    public void updateDataMode() {
	int usedata;
	if (fitmode==NORMAL_MODE) { usedata=Period04.projectGetUseData(); }
	else    /* BINARY_MODE */ { usedata=Period04.projectGetBMUseData(); }
	checkCalcMode[usedata].setSelected(true);
	lastCalculation = usedata;
    }

    //---
    //--- methods to set the fit mode (NORMAL_MODE, BINARY_MODE)
    //---
    public int  getFitMode()      { return fitmode; }
    public void setFitMode(int i) { 
	if (fitmode==i) { return; } // nothing has to be done here 
	Period04.projectSetFitMode(i); // set the fitmode in c++ code
	fitmode = i; 	               // set fitmode in this class
	if (i==NORMAL_MODE) {
	    tabbedPane.remove(mainPanel);
	    tabbedPane.remove(errorPanel);
	    tabbedPane.addTab("   Main   ", null, mainPanel );
	    tabbedPane.addTab("  Goodness of Fit  ", null, errorPanel);
	    errorPanel.add(b_ErrorCalc); // this button only for normal mode
	    updateErrorPanel();
	    frequencyListPanel.remove(bmPanel);
	    header.setBounds(0,0,485,20);
	    Rectangle r;
	    for ( JFrequencyListEntry e : freqlist ) {
	        r = e.getBounds();
		r.y -= bmPanel.getHeight();
		e.setBounds(r);
	    }
	    r = l_FreqListLabel.getBounds();
	    r.y -= bmPanel.getHeight();
	    l_FreqListLabel.setBounds(r);
	    Dimension s = frequencyListPanel.getSize();
	    frequencyListPanel.setPreferredSize(
	    	new Dimension(s.width, s.height-bmPanel.getHeight()));
	    frequencyListPanel.revalidate();
	    copyFrequencies(false);      // copy the frequencies into the list
	    // display the fitting formula that is beeing used here
	    l_FittingFormula.setText("<html>Z + \u2211 \u0391<sub>i</sub> "+
	    "sin( 2\u03C0 (\u03A9<sub>i</sub> t + \u03A6<sub>i</sub>) )</html>");
	    l_FittingFormula.setBounds( 185, 77, 300, 28 );
	    //--- write to log
	    mainframe.getLog().writeProtocol(
		"Changed fitting formula:\n"+
		"Now using: Standard Formula\n"+
		"The current set of frequencies:" ,true);
	    writeFrequenciesToProtocol(false); 
	} else if (i==BINARY_MODE) {
	    tabbedPane.remove(mainPanel );
	    tabbedPane.remove(errorPanel);
	    errorPanel.remove(b_ErrorCalc); // this button only for normal mode
	    tabbedPane.addTab("  Periodic time shifts  ", null, mainPanel );
	    tabbedPane.addTab("  Goodness of Fit  ", null, errorPanel);
	    updateErrorPanel();
	    header.setBounds(0,bmPanel.getHeight(),485,20);
	    Rectangle r;
	    for ( JFrequencyListEntry e : freqlist ) {
		r = e.getBounds();
		r.y = r.y+bmPanel.getHeight();
		e.setBounds(r);
	    }
	    r = l_FreqListLabel.getBounds();
	    r.y += bmPanel.getHeight();
	    l_FreqListLabel.setBounds(r);
	    Dimension s = frequencyListPanel.getSize();
	    frequencyListPanel.setPreferredSize(
		  new Dimension(s.width, s.height+bmPanel.getHeight()));
	    frequencyListPanel.add(bmPanel);
	    frequencyListPanel.revalidate();
	    copyFrequencies(true);      // copy the frequencies into the list
	    // display the fitting formula that is beeing used here
	    l_FittingFormula.setText(
		  "<html>Z+ \u2211 \u0391<sub>i</sub> sin(2\u03C0( \u03A9<sub>i</sub> "+
		  "[t+\u03B1<sub>pts</sub>sin(2\u03C0(\u03C9<sub>pts</sub>t+\u03C6<sub>pts</sub>))] + \u03A6<sub>i</sub> ))</html>");
	    l_FittingFormula.setBounds( 170, 77, 315, 28 );
	    //--- write to log
	    mainframe.getLog().writeProtocol(
		"Changed fitting formula:\n"+
		"Now using: Periodic-time-shift formula\n"+
		"The current set of frequencies:" ,true);
	    writeFrequenciesToProtocol(false); 
	}
    }
    
    //---
    //--- methods to write to the protocol
    //---
    public void writeFrequenciesToProtocol(boolean selected) {
	mainframe.getLog().writeProtocol(
	    Period04.projectWriteFrequenciesTabulated(selected), false);
    }

    //---
    //--- methods to adjust the positioning of the GUI-elements
    //---
    public void fitSize() {
	//--- as we don't use a layout manager we have to fit the size
	//--- of the displayed elements ourselves
	//---
	correct = mainframe.getSizeDiff();
	if (Period04.isMacOS) {
	    //correct -= 10;
	}
	// main panel
	frequencyScrollPane.setBounds( 10, 165, 470, 355-correct );
	b_Calculate     .setBounds(  10, 525-correct, 150, 20 );
	b_ImproveAll    .setBounds( 170, 525-correct, 150, 20 );
	b_ImproveSpecial.setBounds( 330, 525-correct, 150, 20 );
	b_CalcVariation .setBounds(  10, 550-correct, 250, 20 );
	b_PhasePlot     .setBounds( 330, 550-correct, 150, 20 );
	// error panel
	errorScrollPane  .setBounds(  10, 40, 470, 500-correct );
	b_PrintErrorList .setBounds(  10, 550-correct, 230, 20 );
	b_ExportErrorList.setBounds( 250, 550-correct, 230, 20 );
    }

    //---
    //--- copyFrequencies()
    //--- this copies the set of frequencies from the normal mode
    //--- to the binary mode or vice versa
    //---
    public void copyFrequencies(boolean toPTS) {
	if (toPTS) {
	    Period04.projectCopyFrequencies(toPTS);
	} else {
	    Period04.projectCopyFrequencies(toPTS);
	}
	updateFrequencyListData();
    }
   
    public void importFrequencies() {
	// show import dialog
	initializeFileChooser();
	int returnVal = fc.showOpenDialog(mainPanel);
	if (returnVal == JFileChooser.APPROVE_OPTION) {
	    cleanAllFrequencies();
	    // get the file name
	    File file = fc.getSelectedFile();
	    Period04.projectLoadFrequencies(file.getPath(), fitmode);
	    updateFrequencyListData(); 
	    mainframe.getLog().writeProtocol("Loaded Frequencies:\n"+
				  "Loaded a new set of frequencies from file:"+
				  "\n"+file.getPath()+"\n"+
				  "The loaded frequencies are:\n" ,true);
	    writeFrequenciesToProtocol(false); 
	    Period04.setCurrentDirectory(file);
	}
    }

    public void importFrequenciesBatch(String filename) {
	cleanAllFrequencies();
	// get the file name
	File file = new File(filename);
	if (file.exists()) {
	    Period04.projectLoadFrequencies(file.getPath(), fitmode);
	    updateFrequencyListData(); 
	    mainframe.getLog().writeProtocol("Loaded Frequencies:\n"+
					     "Loaded a new set of frequencies from file:"+
					     "\n"+file.getPath()+"\n"+
					     "The loaded frequencies are:\n" ,true);
	    writeFrequenciesToProtocol(false); 
	    Period04.setCurrentDirectory(file);
	}
    }


    public void exportFrequencies(boolean isUseLatex) {
	setFrequencyListData();	// update the data
	if (fitmode==BINARY_MODE) {
	    if (Period04.projectGetBMTotalFrequencies()==0) { return; }
	} else { // NORMAL_MODE
	    if (Period04.projectGetTotalFrequencies()==0) { return; }
	} 

	initializeFileChooser();
	int returnVal = fc.showSaveDialog(null);
	if (returnVal == JFileChooser.APPROVE_OPTION) {
	    File file = fc.getSelectedFile();
	    // check if the extension is missing
	    String name = file.getName();
	    if (name.indexOf(".")==-1) { 
		file = new File(file.getPath()+".per");
	    }
	    // check wether user wants to overwrite an existing file
	    if (file.exists()) {
		int v = JOptionPane.showConfirmDialog(
		    mainframe.getFrame(), "File does already exist!\n"+
		    "Do you really want to delete the old file?", 
		    "Warning", JOptionPane.YES_NO_OPTION);
		if (v==JOptionPane.NO_OPTION) {
		    return;
		}
	    }
 	    // now go and save it
	    Period04.projectSaveFrequencies(file.getPath(), fitmode, isUseLatex);
	    // write to log
	    mainframe.getLog().writeProtocol(
		"Saved frequencies:\n"+
		"Saved frequencies in file:\n"+file.getPath()+"\n",true);
	    Period04.setCurrentDirectory(file);
	} else { 
	    // canceled, do nothing
	}
    }

    //---
    //--- saves the error list to a file
    //---
    public void exportErrorList() {
	fc = new JFileChooser(Period04.getCurrentDirectory());
	int returnVal = fc.showSaveDialog(null);
	if (returnVal == JFileChooser.APPROVE_OPTION) {
	    File file = fc.getSelectedFile();
	    // check if the extension is missing
	    String name = file.getName();
	    if (name.indexOf(".")==-1) { 
		file = new File(file.getPath()+".err");
	    }
	    // check wether user wants to overwrite an existing file
	    if (file.exists()) {
		int v = JOptionPane.showConfirmDialog(
		    mainframe.getFrame(), "File does already exist!\n"+
		    "Do you really want to delete the old file?",
		    "Warning", JOptionPane.YES_NO_OPTION);
		if (v == JOptionPane.NO_OPTION) {
		    return;
		}
	    }
	    // now go and save it
	    Period04.projectSaveFitError(file.getPath());
	    mainframe.getLog().writeProtocol("Saved least-squares fit errors:"+
				  "\nSaved errors in file:\n"+file.getPath()+
				  "\n", true);
	    Period04.setCurrentDirectory(file);
	}
    }

    public void printFrequencies() {
	setFrequencyListData();	// update the data
	printText(Period04.projectGetFrequenciesForPrinting());
    }

    public void printErrorList() {
	printText(t_errors.getText());
    }

    public void dereferenceTask() {
	task = null;
    }

    public void dereferenceAmpVarDialog() {
	ampvarDialog=null;
    }

    //---
    //--- methods for extern panel / toggle buttons
    //---
    private boolean fourierExtern = false;
    public  void    setFourierExtern(boolean value) { fourierExtern = value; }
    public  boolean isFourierExtern()               { return fourierExtern; }
    private void expandFourierPanel() {
	mainframe.expandFourierPanel(!fourierExtern); 
	fourierExtern = !fourierExtern;
	flipFourier();
    }
    public  void flipFourier() {
	toggleRight.flip();
    }

    private boolean tsExtern = false;
    public  void    setTSExtern(boolean value) { tsExtern = value; }
    public  boolean isTSExtern()               { return tsExtern; }
    private void expandTimeStringPanel() {
	mainframe.expandTimeStringPanel(!tsExtern); 
	tsExtern = !tsExtern;
	flipTS();
    }
    public  void flipTS() {
	toggleLeft.flip(); 
    }


    //---
    //--- Toogle-Buttons
    //---
    class ToggleIcon extends ImageIcon {
	private boolean rightArrow;
	public ToggleIcon(boolean value)         { rightArrow = value; }
	public void setRightArrow(boolean value) { rightArrow = value; }
	public boolean isRightArrow()            { return rightArrow; }
	public void flip()                       { rightArrow = !rightArrow; }
	//--- now the implementation of the Icon interface
	public int getIconHeight() { return 50; }
	public int getIconWidth()  { return 10; }
	public void paintIcon(Component c, Graphics g, int x, int y) {
	    Polygon p = new Polygon();
	    if (rightArrow) {
		p.addPoint(x+2, y+20);
		p.addPoint(x+7, y+25);
		p.addPoint(x+2, y+30);
	    } else {
		p.addPoint(x+7, y+20);
		p.addPoint(x+2, y+25);
		p.addPoint(x+7, y+30);
	    }
	    g.setColor(Color.GRAY);
	    g.fillPolygon(p);
	}
    }

    //---
    //--- TextChangeListener
    //---
    class TextChangeListener implements DocumentListener {
	public void changedUpdate(DocumentEvent evt) { }
	public void insertUpdate(DocumentEvent evt) {
	    if (blockChangeEvents) { return; }
	    Document doc = evt.getDocument();
	    setBMData((JTextField)doc.getProperty("name"),
		    (String)    doc.getProperty("type"));
	}
	public void removeUpdate(DocumentEvent evt) {
	    if (blockChangeEvents) { return; }
	    Document doc = evt.getDocument();
	    setBMData((JTextField)doc.getProperty("name"),
		    (String)    doc.getProperty("type"));
	}
    }

    // ---
    // --- Comparator to sort the list according to frequency ID
    // ---
    class FreqIDComparator implements Comparator<JFrequencyListEntry> 
    { 
	@Override public int compare( JFrequencyListEntry c1, JFrequencyListEntry c2 ) { 
	    return c1.getID() - c2.getID(); 
	} 
    } 

    // ---
    // --- Comparator to sort the list according to increasing frequency
    // ---
    class FrequencyComparator implements Comparator<JFrequencyListEntry> 
    { 
	@Override public int compare( JFrequencyListEntry c1, JFrequencyListEntry c2 ) { 
	    if (c2.isEmpty()) { return -1; }
	    return (c1.getFrequency() > c2.getFrequency() ? 1 : -1 ); 
	} 
    } 

    // ---
    // --- Comparator to sort the list according to decreasing amplitude
    // ---
    class AmplitudeComparator implements Comparator<JFrequencyListEntry> 
    { 
	@Override public int compare( JFrequencyListEntry c1, JFrequencyListEntry c2 ) { 
	    return (c1.getAmplitude() > c2.getAmplitude() ? -1 : 1 ); 
	} 
    } 
}



