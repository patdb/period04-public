/*----------------------------------------------------------------------*
 *  XDialogCombineSubstrings
 *  a dialog to combine substrings
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.*;                  // property change stuff
import javax.swing.*;
import javax.swing.event.*;

class XDialogSpectralWindow extends XDialog
    implements ActionListener
{
    //    private static final long serialVersionUID = -6316927621489428764L;

    private JOptionPane   optionPane;
    private final JButton b_OK     = createButton("Ok");
    private final JButton b_Cancel = createButton("Cancel");

    private JTextField t_label;
    private JComboBox  cb_frequency;

    private XProjectFrame  mainframe;
    private XTabFourier tab;

    public XDialogSpectralWindow(XProjectFrame frame, XTabFourier tab) {
	super(frame.getFrame(), true); // necessary to keep dialog in front
	mainframe = frame;
	tab       = tab;
	Object[] options = {b_OK, b_Cancel}; //set buttons
 
	JPanel inputpanel = new JPanel();
	inputpanel.setLayout(null);

	JLabel l_info = createLabel("<html>Please choose a frequency peak <br>to compute the corresponding spectral window.</html>");
	/*
	String [] s_frequencies = new String[4];
	for (int i=0; i<4; i++) {
	    s_frequencies[i] = "freqsnames";
	}
	cb_frequency = new JComboBox(s_frequencies);
	cb_frequency.setSelectedIndex(0);
	*/
	final JTextField t_frequency = createTextField("0.0");

	l_info      .setBounds( 10,  0, 280,  40);
	t_frequency.setBounds( 10, 50, 260,  20);

	inputpanel.add(l_info);
	inputpanel.add(t_frequency);

	optionPane = new JOptionPane((Object)inputpanel, 
				     JOptionPane.PLAIN_MESSAGE,
				     JOptionPane.YES_NO_OPTION,
				     null, options, options[0]);
	setContentPane(optionPane);
	setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent we) {
		    /*
		     * Instead of directly closing the window,
		     * we're going to change the JOptionPane's
		     * value property.
		     */
		    optionPane.setValue(b_Cancel);
		}
	    });

	optionPane.addPropertyChangeListener(new PropertyChangeListener() {
		public void propertyChange(PropertyChangeEvent e) {
		    String prop = e.getPropertyName();

		    if (isVisible()
			&& (e.getSource() == optionPane)
			&& (prop.equals(JOptionPane.VALUE_PROPERTY) ||
			    prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
			Object value = optionPane.getValue();
 
			if (value == JOptionPane.UNINITIALIZED_VALUE) {
			    return; //ignore reset
			}
		       
			// reset the JOptionPane's value.
			optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

			if (value.equals(b_OK)) {
			    double frequency = Double.valueOf(t_frequency.getText()).doubleValue();
			    //
			    Period04.projectSetSW2Frequency(frequency);
			    //
			    // write protocol
			    mainframe.getLog().writeProtocol(
				"Spectral Window 2:\n"+
				"\t\twith frequency "+frequency+"\n", true); 
			    setVisible(false);
			} else if (value.equals(b_Cancel)) {
			    setVisible(false);
			}
		    }
		}
	    });
	
	this.setTitle("Select Frequency");
	if (Period04.isMacOS)
		this.setSize(320,180);
	else
		this.setSize(320,160);
	this.setResizable(false);
	this.setLocationRelativeTo(mainframe.getFrame());
	this.setVisible(true);
    }

    public void actionPerformed(ActionEvent evt) {
	optionPane.setValue(evt.getSource());
    }

}

