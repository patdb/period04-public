/*-----------------------------------------------------------------------*
 * DragSelectionListener
 * defines the selection behaviour of JLists
 *-----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;     // mouse event
import javax.swing.*;
import javax.swing.event.*;  // mouse input adapter

class DragSelectionListener extends MouseInputAdapter {
     Point lastPoint=null;
    
    public void mousePressed( MouseEvent e) {
	// need to hold onto starting mousepress location...
	lastPoint=e.getPoint();     
    }

    public void mouseReleased( MouseEvent e) {
	lastPoint=null;
    }
    
    public void mouseDragged( MouseEvent e) {
	final JList list=((JList) e.getSource());
	if(lastPoint!=null&&!e.isConsumed()&&SwingUtilities.isLeftMouseButton(e)&&!e.isShiftDown()) {
	    
	    final int row=list.locationToIndex(e.getPoint());
	    if(row!=-1) {
		final int leadIndex=list.locationToIndex(lastPoint);
		if(row!=leadIndex) { 
		    // ignore drag within row
		    final Rectangle cellBounds=list.getCellBounds(row,row);
		    if(cellBounds!=null) {
			list.scrollRectToVisible(cellBounds);
			final int anchorIndex=leadIndex;
			if(e.isControlDown()) {
			    if(list.isSelectedIndex(anchorIndex)) {
				// add selection
				list.removeSelectionInterval(anchorIndex,leadIndex);
				list.addSelectionInterval(anchorIndex,row);
			    } else {
				// remove selection
				list.addSelectionInterval(anchorIndex,leadIndex);
				list.removeSelectionInterval(anchorIndex,row);
			    }
			} else { 
			    // add selection
			    list.addSelectionInterval(leadIndex,row);
			}
		    }
		}
	    }
	}
    }
}
