/*---------------------------------------------------------------------*
 *  XTab.java
 *  defines some useful methods for the tabs
 *---------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import javax.swing.*;
import javax.swing.border.SoftBevelBorder;
import javax.print.*;
import javax.print.attribute.*;
import java.io.PrintWriter;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;

public abstract class XTab 
    implements ActionListener
{
    public JPanel mainPanel;

    public JPanel getMainPanel() {
	return mainPanel;
    }

    protected JButton createButton(String name) {
	JButton button = new JButton(name);
	button.setFont(new Font("Dialog", Font.PLAIN, 10));
	button.addActionListener(this);
	return button;
    } 

    protected JButton createBoldButton(String name) {
	JButton button = new JButton(name);
	button.setFont(new Font("Dialog", Font.BOLD, 10));
	button.addActionListener(this);
	return button;
    } 

    protected JButton createCalcButton(String name) {
	JButton button = new JButton(name);
	button.setFont(new Font("Dialog", Font.BOLD, 11));
	button.addActionListener(this);
	button.setBorder(  
	    BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(Color.red),
		new SoftBevelBorder(SoftBevelBorder.RAISED)));
	return button;
    }

    protected JCheckBox createCheckBox(String name) {
	JCheckBox check = new JCheckBox(name);
	check.setFont(new Font("Dialog", Font.PLAIN, 11));
	return check;
    }

    protected JLabel createLabel(String name) {
	JLabel label = new JLabel(name);
	label.setFont(new Font("Dialog", Font.PLAIN, 11));
	return label;
    }
    protected JLabel createBoldLabel(String name) { //TabTS
	JLabel label = new JLabel(name);
	label.setFont(new Font("Dialog", Font.BOLD, 11));
	return label;
    }
    protected JLabel createLargeLabel(String name) {
	JLabel label = new JLabel(name);
	label.setFont(new Font("Dialog", Font.BOLD, 11));
	return label;
    }

    protected JRadioButton createRadioButton(String name) {
	JRadioButton check = new JRadioButton(name);
	check.setFont(new Font("Dialog", Font.PLAIN, 11));
	check.addActionListener(this);           
	return check;
    }

    protected JTextField createTextField(String name) {
	JTextField text = new JTextField(name);
	text.setFont(new Font("Dialog", Font.PLAIN, 11));
	text.addActionListener(this);            
	return text;
    }

    protected String format(double value) {
	return format(value,6);
    }

    protected String format(double value, int digits) {
	NumberFormat fmt = NumberFormat.getInstance(Locale.UK);
	if (fmt instanceof DecimalFormat) {
	    ((DecimalFormat) fmt).setDecimalSeparatorAlwaysShown(true);
	}
	fmt.setGroupingUsed(false);
	fmt.setMaximumFractionDigits(digits);
	return String.valueOf(fmt.format(value));
    }

    public void printText(String text) {
		/*
		  // OLD VERSION
		  // print a simple ascii text
		  DocFlavor flavor = DocFlavor.STRING.TEXT_PLAIN;
		  Doc doc = new SimpleDoc(text, flavor, null);
		  PrintRequestAttributeSet set = new HashPrintRequestAttributeSet(); 
		  PrintService dps = PrintServiceLookup.lookupDefaultPrintService();
		  DocPrintJob job = dps.createPrintJob();
		  try { job.print(doc, set); }
		  catch (Exception e) {
		  JOptionPane.showMessageDialog(
		  null, "Sorry, printing failed!\n\n"+
		  "Please use 'Save' to save the data to file\n"+
		  "and print it using an other program.\n\n"+
		  "Detailed error message:\n"+e,"Printing failed!", 
		  JOptionPane.INFORMATION_MESSAGE);
		  }
		*/
		//		PrintText pt = new PrintText();
		//		pt.print(text);
		new PrintThread(text).start();
	}
	
	class PrintThread extends Thread {
		String text;
		public PrintThread(String str) {
			text =str;
		}
		
		public void run() {
			PrintText pt = new PrintText();
			pt.print(text);
		}
	}


}
