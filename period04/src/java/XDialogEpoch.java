/*----------------------------------------------------------------------*
 *  XDialogEpoch
 *  a dialog to calculate the epoch
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.*;                  // Property change stuff
import java.io.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;


class XDialogEpoch extends XDialog 
     implements ComponentListener
{
    private static final long serialVersionUID = 1747105259621440365L;
    private JOptionPane     optionPane;
    private XProjectFrame      mainframe;
    private final JButton   b_OK     = createButton("Calculate");
    private final JButton   b_Save   = createButton("Export");
    private final JButton   b_Print  = createButton("Print");
    private final JButton   b_Cancel = createButton("Close");

    private final int WIDTH  = 400;      // default frame width
    private final int HEIGHT = 360;      // default frame height
    private static int hDiff = 0;        // correction factor for height
    private static int wDiff = 0;        // correction factor for width
    
    private JComboBox   cb_mode;
    private JTextArea   textbox;
    private JScrollPane textpane;
    private JTextField  t_epoch;
    private String      result;

    private final String MAXIMUM_LIGHT = "Maximum light";
    private final String ZEROPOINT     = "Zeropoint";
    private final String MINIMUM_LIGHT = "Minimum light";
  
    public XDialogEpoch(XProjectFrame frame) {
	super(frame.getFrame(), false);
	mainframe = frame;
	Object[] options = {b_OK, b_Save, b_Print, b_Cancel}; //set buttons 
	
	JPanel inputpanel = new JPanel();
	inputpanel.setLayout(null);
	
	JLabel l_mode  = createBoldLabel("Calulate the time of:");
	String [] items = { MAXIMUM_LIGHT, ZEROPOINT, MINIMUM_LIGHT };
	cb_mode = new JComboBox(items);
	cb_mode.setSelectedItem(MAXIMUM_LIGHT);
	cb_mode.setFont(new Font("Dialog", Font.BOLD, 11));
	JLabel l_epoch = createBoldLabel("close to the time:");
	t_epoch = new JTextField("0");

	// the textbox and its heading
	JLabel heading = createLabel(
	    "Frequency                             Epoch");
	textbox = new JTextArea();
	textbox.setEditable(false);

	// make it scrollable
	textpane = new JScrollPane(
	    textbox, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
	    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	textpane.setBorder(
	    BorderFactory.createEtchedBorder(EtchedBorder.RAISED));

	// calculate epoch for epoch 0.00
	calculateEpoch();

	l_mode  .setBounds(  5,  0, 160,  20);
	cb_mode .setBounds(170,  0, 150,  20);
	l_epoch .setBounds(  5, 22, 160,  20);
	t_epoch .setBounds(170, 22, 150,  20);
	heading .setBounds(  5, 50, 300,  20);
	textpane.setBounds(  5, 70, 360, 200);

	inputpanel.add(l_mode);
	inputpanel.add(cb_mode);
	inputpanel.add(l_epoch);
	inputpanel.add(t_epoch);
	inputpanel.add(heading);
	inputpanel.add(textpane);

	optionPane = new JOptionPane((Object)inputpanel, 
				     JOptionPane.PLAIN_MESSAGE,
				     JOptionPane.YES_NO_OPTION,
				     null, options, options[0]);
	setContentPane(optionPane);
	setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent we) {
		    /*
		     * Instead of directly closing the window,
		     * we're going to change the JOptionPane's
		     * value property.
		     */
		    optionPane.setValue(
			new Integer(JOptionPane.CLOSED_OPTION));
		}
	    });
	// listen for frame size changes
	addComponentListener(this); 
	
	optionPane.addPropertyChangeListener(new PropertyChangeListener() {
		public void propertyChange(PropertyChangeEvent e) {
		    String prop = e.getPropertyName();
		    
		    if (isVisible() 
			&& (e.getSource() == optionPane)
			&& (prop.equals(JOptionPane.VALUE_PROPERTY) ||
			    prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
			Object value = optionPane.getValue();
			
			if (value == JOptionPane.UNINITIALIZED_VALUE) {
			    return; //ignore reset
			}
			
			// Reset the JOptionPane's value.
			// If you don't do this, then if the user
			// presses the same button next time, no
			// property change event will be fired.
			optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

			if (value.equals(b_Save))
			    saveEpoch();
			else if (value.equals(b_Print))
			    printEpoch();
			else if (value.equals(b_OK))
			    calculateEpoch();
			else
			    setVisible(false);
			
		    }
		}
	    });
	
	this.setTitle("Calculate epoch:");
	this.setSize(WIDTH,HEIGHT);
	this.setLocationRelativeTo(mainframe.getFrame());
	this.setVisible(true);
    }

    public void actionPerformed(ActionEvent evt) {
	optionPane.setValue(evt.getSource());
    }

    public void calculateEpoch() {
	String s_epoch = t_epoch.getText();

	// check wether this is a valid number
	NumberFormatException nan = null;
	try { Double.parseDouble(s_epoch); } 
	catch (NumberFormatException exception) { nan=exception; }
	if ( s_epoch.equals("") || (nan!=null) ) {
	    // text was no non-negative number
	    JOptionPane.showMessageDialog(
		XDialogEpoch.this, "Sorry, \""+s_epoch+"\" "+
		"isn't a valid number.\n", "Try again",
		JOptionPane.ERROR_MESSAGE);
	    return;
	} 	  

	String s_mode = (String)cb_mode.getSelectedItem();
	int mode = 0; // MAXIMUM_LIGHT is default
	if      (s_mode==ZEROPOINT)     { mode = 1; }
	else if (s_mode==MINIMUM_LIGHT) { mode = 2; }

	// fill listbox
	result = Period04.projectShowEpoch(Double.parseDouble(s_epoch), mode);
	textbox.setText("");   // clean the list
	textbox.append(result);

	StringBuffer text = new StringBuffer();
	text.append("Calculate epoch:\n");
	if (Period04.projectGetReverseScale()) {
	    text.append("Calculating the epoch in magnitudes ");
	} else {
	    text.append("Calculating the epoch in intensity ");
	}
	text.append("for the active frequencies\n"+
		    "for the following time:   "+s_epoch+"\n");
	text.append("Frequency                    Epoch"+"\n");
	text.append(result);
	mainframe.getLog().writeProtocol(text.toString(), true);
    }

    public void saveEpoch() {
	// create FileChooser
	JFileChooser fc = new JFileChooser(Period04.getCurrentDirectory());
	int returnVal = fc.showSaveDialog(mainframe.getFrame());
	if (returnVal == JFileChooser.APPROVE_OPTION) {
	    File file = fc.getSelectedFile();
	    // check if the extension is missing
	    String name = file.getName();
	    if (name.indexOf(".")==-1) { 
		file = new File(file.getPath()+".txt");
	    }
	    // check wether user wants to overwrite an existing file
	    if (file.exists()) {
		int v = JOptionPane.showConfirmDialog(
		    mainframe.getFrame(), "File does already exist!\n"+
		    "Do you really want to delete the old file?", 
		    "Warning", JOptionPane.YES_NO_OPTION);
		if (v == JOptionPane.NO_OPTION) {
		    return;
		} 
	    }
	    // now go and save it
	    Period04.setCurrentDirectory(file);
	    try {
		FileWriter fw = new FileWriter(file);
		fw.write("Frequency                    Epoch\n");
		fw.write(result);
		if ( fw!=null ) fw.close();
	    } catch (IOException e) {
		System.err.println(e);
	    }
	}
    }

    public void printEpoch() {
	StringBuffer text = new StringBuffer();
	text.append("Frequency               Epoch\n");
	text.append("----------------------------------------------\n");
	text.append(result);
	printResult(text.toString());    

/*	    JEditorPane doc = new JEditorPane("rtf", text.toString());
	    DocumentRenderer dr = new DocumentRenderer();
	    dr.print(doc);*/
    }    

    //---
    //--- methods to listen to ComponentEvents 
    //--- (we are listening to ComponentEvents that are caused by resizing 
    //---  the mainframe)
    //---
    public void componentResized(ComponentEvent e) {
	//--- the frame has been resized. 
	//--- set the framewidth back to the default
	//--- if the frameheight is to low set it back to the default value
	//--- if the frameheight is ok, then determine the difference to the 
	//--- default value and call fitSize()
	//--- this will size the GUI elements to fit to the new frameheight
	Component c = e.getComponent();
	Dimension frameSize = c.getSize();
	if (frameSize.height <= HEIGHT) {
	    frameSize.height = HEIGHT;
	    hDiff = 0;
	} else {
	    hDiff = HEIGHT-frameSize.height;
    	}
	if (frameSize.width <= WIDTH) {
	    frameSize.width = WIDTH;
	    wDiff = 0;
	} else {
	    wDiff = WIDTH-frameSize.width;
	}
	setSize(frameSize);
	fitSize();
    }
    public void componentHidden(ComponentEvent e) {}
    public void componentShown(ComponentEvent e)  {}
    public void componentMoved(ComponentEvent e)  {}

    private void fitSize() {
	textpane.setBounds(  5, 70, 360-wDiff, 200-hDiff );
    }

}

