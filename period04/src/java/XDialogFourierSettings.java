/*-----------------------------------------------------------------------*
 *  XDialogFourierSettings
 *  a dialog to set fourier settings
 *-----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

class XDialogFourierSettings extends XDialog
    implements ActionListener
{
    private XProjectFrame      mainframe;
    private JOptionPane        optionPane;
    private final String       s_OK = "Close";
    private final JCheckBox    checkAutoFourierDisplay;
    private final JCheckBox    checkFreqCompositions;
    private JPanel inputpanel, compopanel;
    private JTextField t_useres;

    public XDialogFourierSettings(XProjectFrame mframe) {
	super(mframe.getFrame(), true);  // necessary to keep dialog in front
	mainframe = mframe;

        Object[] options = {s_OK};

	inputpanel = new JPanel();
	inputpanel.setLayout(null);

	checkAutoFourierDisplay = createCheckBox("Automatically show Fourier spectrum after calculation");
	checkAutoFourierDisplay.setFont(new Font("Dialog", Font.BOLD, 11));
	checkAutoFourierDisplay.setSelected(mframe.getFourierTab().getAutoFourierDisplay());

	checkFreqCompositions = createCheckBox("Check whether new peak is harmonic or combination");
	checkFreqCompositions.setFont(new Font("Dialog", Font.BOLD, 11));
	checkFreqCompositions.setSelected(mframe.getFourierTab().getCheckFreqCompositions());

	compopanel = new JPanel();
	compopanel.setLayout(null);
	compopanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
	JLabel l_res=createBoldLabel("Frequency-Resolution:");
	JLabel l_rayleigh=createLabel("Rayleigh-Criterion");
	JLabel l_useres=createBoldLabel("Limit to be used:");
	double t_inv = 1.5/Period04.projectGetTotalTimeLength();
	JTextField t_rayleigh=createTextField(format(t_inv));
	t_rayleigh.setEditable(false);
	t_useres=createTextField(format(Period04.projectGetFreqResForCompoSearch()));
	JLabel spacer = new JLabel("    ");
	l_res      .setBounds( 10, 5,200,20);
	l_rayleigh .setBounds( 10,30,120,20);
	t_rayleigh .setBounds(130,30,100,20);
	l_useres   .setBounds(260, 5,120,20);
	t_useres   .setBounds(260,30,110,20);
	compopanel.add(l_res);
	compopanel.add(l_rayleigh);
	compopanel.add(t_rayleigh);
	compopanel.add(l_useres);
	compopanel.add(t_useres);

	checkAutoFourierDisplay.setBounds(10, 10, 400, 20 );
	checkFreqCompositions.setBounds(10, 40, 400, 20 );
	compopanel.setBounds(40,70,380,60);

	inputpanel.add( checkAutoFourierDisplay, null );
	inputpanel.add( checkFreqCompositions, null );
	if (checkFreqCompositions.isSelected()) {
	    inputpanel.add(compopanel,null);
	}

       	Object array = inputpanel;

        optionPane = new JOptionPane(array, 
                                    JOptionPane.PLAIN_MESSAGE,
                                    JOptionPane.YES_NO_OPTION,
                                    null,
                                    options,
                                    options[0]);
        setContentPane(optionPane);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent we) {
                /*
                 * Instead of directly closing the window,
                 * we're going to change the JOptionPane's
                 * value property.
                 */
                    optionPane.setValue(new Integer(JOptionPane.CLOSED_OPTION));
            }
        });

	optionPane.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
                String prop = e.getPropertyName();

                if (isVisible() 
                 && (e.getSource() == optionPane)
                 && (prop.equals(JOptionPane.VALUE_PROPERTY) ||
                     prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
                    Object value = optionPane.getValue();

                    if (value == JOptionPane.UNINITIALIZED_VALUE) {
                        //ignore reset
                        return;
                    }

                    // Reset the JOptionPane's value.
                    // If you don't do this, then if the user
                    // presses the same button next time, no
                    // property change event will be fired.
                    optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

                    if (value.equals(s_OK)) {
			if (checkFreqCompositions.isSelected()) {
			    if (checkDouble(t_useres,true)) {
				double df= Double.parseDouble(t_useres.getText());
				Period04.projectSetFreqResForCompoSearch(df);
				setVisible(false);
			    }
			} else {
			    setVisible(false);
			}
		    } else { // user closed dialog or clicked cancel
			setVisible(false);
		    }
                }
            }
        });

	this.setTitle("Fourier Transformation Settings");
	if (checkFreqCompositions.isSelected()) {
	    this.setSize(460,240);	    
	} else {
	    this.setSize(460,160);
	}
	this.setResizable(false);
	this.setLocationRelativeTo(mainframe.getFrame());
	this.setVisible(true);
    }

    public void actionPerformed(ActionEvent evt) {
	Object s=evt.getSource();
	if (s==checkAutoFourierDisplay) {
	    mainframe.getFourierTab().setAutoFourierDisplay(checkAutoFourierDisplay.isSelected());
	}
	if (s==checkFreqCompositions) {
	    mainframe.getFourierTab().setCheckFreqCompositions(checkFreqCompositions.isSelected());
	    if (checkFreqCompositions.isSelected()) {
		inputpanel.add(compopanel);
		this.setSize(460,240);
	    } else {
		inputpanel.remove(compopanel);
		this.setSize(460,160);
	    }
	}
    }

    protected String format(double value) {
	NumberFormat fmt = NumberFormat.getInstance(Locale.UK);
	if (fmt instanceof DecimalFormat) {
	    ((DecimalFormat) fmt).setDecimalSeparatorAlwaysShown(true);
	}
	fmt.setGroupingUsed(false);
	fmt.setMaximumFractionDigits(8);
	return String.valueOf(fmt.format(value));
    }

}
