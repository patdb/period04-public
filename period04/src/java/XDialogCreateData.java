/*----------------------------------------------------------------------*
 *  XDialogCreateData
 *  a dialog to create artificial time series data
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.*;            // property change stuff
import java.io.*;
import javax.swing.*;
import javax.swing.event.*;

class XDialogCreateData extends XDialog 
    implements ActionListener
{
    private static final long serialVersionUID = 7538580679787642980L;
    private JOptionPane   optionPane;
    private final JButton b_FromFile = 
    createButton("Read time ranges from file");
    private final JButton b_PtsFromFile = 
    createButton("Read time points from file");
    private final JButton b_Calculate = 
    createButton((Period04.isMacOS?"Write data to file ...":"Write data to the following file ..."));
    private final JButton b_Cancel = createButton("Close");
    private JTextField t_starttime, t_endtime, t_step, t_leading;

    private XProjectFrame mainframe;
    private XTabFit    tab;

    private File    infile;
    private boolean useFile = false;
    private boolean isRangeInFile=true;

    public XDialogCreateData(XProjectFrame frame, XTabFit tabFit) {
	super(frame.getFrame(), true); // necessary to keep dialog in front
	mainframe = frame;
	tab = tabFit;
	Object[] options = {b_Calculate, b_Cancel}; //set buttons 
	
	JPanel inputpanel = new JPanel();
	inputpanel.setLayout(null);

	JLabel l_starttime = createLabel("Start-time:");
	JLabel l_endtime   = createLabel("End-time");
	JLabel l_step      = createLabel("Step:");
	JLabel l_leading   = createLabel("Leading/trailing time:");
	t_starttime   = createTextField(Period04.projectGetStartTime());
	t_endtime     = createTextField(Period04.projectGetEndTime());
	t_step        = createTextField("0.00000000");
	t_leading     = createTextField("0.00000000");

	l_starttime.setBounds( 10,  0, 150, 20);
	t_starttime.setBounds(160,  0, 160, 20);
	l_endtime  .setBounds( 10, 25, 150, 20);
	t_endtime  .setBounds(160, 25, 160, 20);
	b_FromFile .setBounds( 10, 50, 310, 20);
	b_PtsFromFile .setBounds( 10, 75, 310, 20);
	l_step     .setBounds( 10, 110, 150, 20);
	t_step     .setBounds(160, 110, 160, 20);
	l_leading  .setBounds( 10, 135, 150, 20);
	t_leading  .setBounds(160, 135, 160, 20);

	// calculate stepping
	double highest = 0;
	if (tab.getFitMode()==XTabFit.BINARY_MODE) {
	    int fr = Period04.projectGetBMTotalFrequencies();
	    for (int i=0; i<fr; i++) {
		if (Period04.projectGetBMActive(i)) {
		    double freq = Period04.projectGetBMFrequencyValue(i);
		    if (freq>highest) {
			highest = freq;
		    }
		}
	    }
	} else {
	    int fr = Period04.projectGetTotalFrequencies();
	    for (int i=0; i<fr; i++) {
		if (Period04.projectGetActive(i)) {
		    double freq = Period04.projectGetFrequencyValue(i);
		    if (freq>highest) {
			highest = freq;
		    }
		}
	    }
	}
	
	if (highest==0) { highest=1; }
	else { highest=(1./20.)/highest; }
	t_step.setText(Double.toString(highest));

	inputpanel.add(l_starttime);
	inputpanel.add(t_starttime);
	inputpanel.add(l_endtime);
	inputpanel.add(t_endtime);
	inputpanel.add(b_FromFile);
	inputpanel.add(b_PtsFromFile);
	inputpanel.add(l_step);
	inputpanel.add(t_step);
	inputpanel.add(l_leading);
	inputpanel.add(t_leading);

	optionPane = new JOptionPane((Object)inputpanel, 
				     JOptionPane.PLAIN_MESSAGE,
				     JOptionPane.YES_NO_OPTION,
				     null, options, options[0]);
	setContentPane(optionPane);
	setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent we) {
		    /*
		     * Instead of directly closing the window,
		     * we're going to change the JOptionPane's
		     * value property.
		     */
		    optionPane.setValue(
			new Integer(JOptionPane.CLOSED_OPTION));
		}
	    });
	
	optionPane.addPropertyChangeListener(new PropertyChangeListener() {
		public void propertyChange(PropertyChangeEvent e) {
		    String prop = e.getPropertyName();
		    
		    if (isVisible() 
			&& (e.getSource() == optionPane)
			&& (prop.equals(JOptionPane.VALUE_PROPERTY) ||
			    prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
			Object value = optionPane.getValue();
			
			if (value == JOptionPane.UNINITIALIZED_VALUE) {
			    return; //ignore reset
			}
			
			// Reset the JOptionPane's value.
			// If you don't do this, then if the user
			// presses the same button next time, no
			// property change event will be fired.
			optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

			if (value.equals(b_Calculate)) {
			    // check whether a calculation is already running
			    if (mainframe.isWorking()) { return; }
			    // check if entries are valid
			    if (!checkDouble(t_starttime, false)) { return; }
			    String s_starttime = t_starttime.getText();
			    if (!checkDouble(t_endtime, false)) { return; }
			    String s_endtime   = t_endtime.getText();
			    if (!checkDouble(t_step, true)) { return; }
			    String s_step      = t_step.getText();
			    if (!checkDouble(t_leading, false)) { return; }
			    String s_leading   = t_leading.getText();
			    
			    String outfile = null;
			    boolean append = false;
			    
			    JFileChooser fc = new JFileChooser(
				Period04.getCurrentDirectory());
			    int val = fc.showSaveDialog(
				XDialogCreateData.this);
			    if (val==JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				// check if the extension is missing
				String name = file.getName();
				if (name.indexOf(".")==-1) { 
				    file = new File(file.getPath()+".dat");
				}
				outfile = file.getPath();
				// check wether user wants to 
				// overwrite an existing file
				if (file.exists()) {
				    String [] options = {"Append data",
							 "Replace file",
							 "Cancel"};
				    int v = JOptionPane.showOptionDialog(
					XDialogCreateData.this,
					"File does already exist!\n", 
					"Append or Replace",
					JOptionPane.YES_NO_CANCEL_OPTION,
					JOptionPane.INFORMATION_MESSAGE,
					null, options, options[2]);
				    if (v==JOptionPane.YES_OPTION) { // append
					append = true; 
				    } else if (v==JOptionPane.NO_OPTION) { // replace
					// nothing to do
				    } else { // cancel
					return;
				    }
				}
				// create a progress bar
				mainframe.setSaving(true); 

				// create a task for output
				FileTask task;
				if (useFile) {
				    task = new FileTask(
					mainframe, 
					FileTask.ARTIFICIAL_DATA_FILE,
					outfile, infile.getPath(),
					isRangeInFile,
					Double.parseDouble(s_step), 
					Double.parseDouble(s_leading), 
					append);
				} else {
				    task = new FileTask(
					mainframe, FileTask.ARTIFICIAL_DATA,
					outfile, 
					Double.parseDouble(s_starttime), 
					Double.parseDouble(s_endtime),
					Double.parseDouble(s_step), 
					Double.parseDouble(s_leading),
					append);
				}
				task.go();
			    } else { // user pressed cancel
				return;
			    } // file approved
			} else {
			    setVisible(false);
			}
		    }
		}
	    });
	
	this.setTitle("Create artificial data:");
	if (Period04.isMacOS)
		this.setSize(400,260);
	else
		this.setSize(360,260);
	this.setResizable(false);
	this.setLocationRelativeTo(mainframe.getFrame());
	this.setVisible(true);
    }

    public void actionPerformed(ActionEvent evt) {
	if (evt.getSource()==b_FromFile) {
	    JFileChooser fc=new JFileChooser(Period04.getCurrentDirectory());
	    int  returnVal = fc.showOpenDialog(XDialogCreateData.this);
	    if (returnVal == JFileChooser.APPROVE_OPTION) {
		// get the file name
		infile = fc.getSelectedFile();
		if (!infile.canRead()) {
		    JOptionPane.showMessageDialog(
			XDialogCreateData.this, "Sorry, cannot read file \""+
			infile.getName()+"\" ", "Error",
			JOptionPane.ERROR_MESSAGE);
		    return;
		}
		useFile = true;
		t_starttime.setEditable(false);
		t_endtime  .setEditable(false);
		t_starttime.setText("");
		t_endtime  .setText("");
		b_FromFile.setText(
		    "Using time ranges from file: "+infile.getName());
		isRangeInFile=true;
	    }
	    return;
	}
	else if (evt.getSource()==b_PtsFromFile) {
	    JFileChooser fc=new JFileChooser(Period04.getCurrentDirectory());
	    int  returnVal = fc.showOpenDialog(XDialogCreateData.this);
	    if (returnVal == JFileChooser.APPROVE_OPTION) {
		// get the file name
		infile = fc.getSelectedFile();
		if (!infile.canRead()) {
		    JOptionPane.showMessageDialog(
			XDialogCreateData.this, "Sorry, cannot read file \""+
			infile.getName()+"\" ", "Error",
			JOptionPane.ERROR_MESSAGE);
		    return;
		}
		useFile = true;
		t_starttime.setEditable(false);
		t_endtime  .setEditable(false);
		t_starttime.setText("");
		t_endtime  .setText("");
		b_FromFile.setEnabled(false);
		b_PtsFromFile.setText(
		    "Using time points from file: "+infile.getName());
		t_leading.setEditable(false);
		t_step.setEditable(false);
		isRangeInFile=false;
	    }
	    return;
	}
	optionPane.setValue(evt.getSource());
    }


}

