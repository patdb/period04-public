/*----------------------------------------------------------------------*
 *  ReadingTask
 *  starts a task to display the progress while reading in data
 *----------------------------------------------------------------------*/

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URL;
import javax.swing.JOptionPane;

public class FileTask extends Task {

    public static final int PROJECT_IN=0;
    public static final int PROJECT_OUT=1;
    public static final int TIMESTRING_IN=2;
    public static final int TIMESTRING_OUT=3;
    public static final int TIMESTRING_PHASE_OUT=4;
    public static final int TIMESTRING_BINNED_PHASE_OUT=5;
    public static final int ARTIFICIAL_DATA=6;
    public static final int ARTIFICIAL_DATA_FILE=7;
    public static final int FOURIER_OUT=8;
    public static final int FOURIER_ALL_OUT=9;
    public static final int FOURIER_SELECTED_OUT=10;
    public static final int LOG_OUT=11;

    private int what;
    private XProjectFrame mainframe;
    private String filename;

    private XTabTimeString tabTimeString;
    private XTabFit tabFit;
    private XTabLog tabLog;

    private boolean isBatchMode;

    // for project
    private boolean message;
    
    // for timestrings
    private File [] files;
    private boolean append;
    private String fileformat;
	private boolean votable;
    private double frequency;
    private double binSpacing;
    private int useData;
    private int points;
    private double[] phaseArray;
    private double[] amplitudeArray;
    private double[] sigmaArray;
    private double zeropoint;

    // for artificial data
    private double starttime;
    private double endtime;
    private double step;
    private double leading;
    private String infile;
    private boolean isRange;

    // for fourier
    private int id;
    private int[] selectedindices;
    private int selected;
    private boolean usePrefix;
    private String prefix;
    private String directory;

    // for the log
    private String log;
    private File file;

    //---
    //--- constructor for project reading task
    //---
    public FileTask( XProjectFrame mainframe,  int what,  XTabTimeString tabTimeString,  XTabFit tabFit,  XTabLog tabLog,  String filename) {
	this.mainframe=mainframe;
	this.what=what;
	this.tabTimeString=tabTimeString;
	this.tabFit=tabFit;
	this.tabLog=tabLog;
	this.filename=filename;
	this.isBatchMode=false;
    }

    //---
    //--- constructor for project saving task
    //---
    public FileTask( XProjectFrame mainframe,  int what,  String filename,  boolean message) {
	this.mainframe=mainframe;
	this.what=what;
	this.filename=filename;
	this.message=message;
	this.isBatchMode=false;
    }

/*    //---
    //--- constructor for import timestring reading task
    //---
    public FileTask( XProjectFrame mainframe,  int what,  String filename,  String fileformat,  int appendID,  XTabTimeString tabTimeString) {
	this.mainframe=mainframe;
	this.what=what;
pha	this.filename=filename;
	this.fileformat=fileformat;
	this.appendID=appendID;
	this.tabTimeString=tabTimeString;
	this.isBatchMode=false;
    }
*/
    //---
    //--- constructor for import timestring reading task
    //---
    public FileTask(XProjectFrame mainframe, int what, File [] files, boolean append) {
	this.mainframe=mainframe;
	this.what=what;
	this.files = files;
	this.append=append;
	//	this.tabTimeString=tabTimeString;
	this.isBatchMode=false;
    }
    
    //---
    //--- constructor for batch import timestring reading task
    //---
    public FileTask(XProjectFrame mainframe, int what, File [] files, boolean append, String fileformat) {
	this.mainframe=mainframe;
	this.what=what;
	this.files = files;
	this.append=append;
	this.fileformat=fileformat;
	//	this.tabTimeString=tabTimeString;
	this.isBatchMode=true;
    }
    
    //---
    //--- constructor for export timestring task
    //---
    public FileTask( XProjectFrame mainframe, boolean votable, int what,  String filename, String outputformat) {
	this.mainframe=mainframe;
	this.what=what;
	this.filename=filename;
	this.fileformat=outputformat;
	this.votable=votable;
	this.isBatchMode=false;
    } 

    //---
    //--- constructor for export timestring task (using phases)
    //---
    public FileTask(XProjectFrame mainframe, boolean votable, int what, 
					String filename, String outputformat, double frequency, 
					double zeropoint) {
	this.mainframe  = mainframe;
	this.what       = what;
	this.filename   = filename;
	this.fileformat = outputformat;
	this.frequency  = frequency;
	this.zeropoint  = zeropoint;
	this.votable    = votable;
	this.isBatchMode=false;
    }

    //---
    //--- constructor for export timestring task (using binned phases)
    //---
    public FileTask( XProjectFrame mainframe,  int what,  String filename,  String outputformat,  double frequency,  double binSpacing,  int useData,  int points,  double[] phase,  double[] amplitude,  double[] sigma) {
	this.mainframe=mainframe;
	this.what=what;
	this.filename=filename;
	this.fileformat=outputformat;
	this.frequency=frequency;
	this.binSpacing=binSpacing;
	this.useData=useData;
	this.points=points;
	this.phaseArray=phase;
	this.amplitudeArray=amplitude;
	this.sigmaArray=sigma;
	this.isBatchMode=false;
    }

    //---
    //--- constructor for the create artificial data task (standard)
    //---
    public FileTask( XProjectFrame mainframe,  int what,  String outfile,  double starttime,  double endtime,  double step,  double leading,  boolean append) {
	this.mainframe=mainframe;
	this.what=what;
	this.filename=outfile;
	this.starttime=starttime;
	this.endtime=endtime;
	this.step=step;
	this.leading=leading;
	this.append=append;
	this.isBatchMode=false;
    }

    //---
    //--- constructor for the create artificial data task (file mode)
    //---
    public FileTask( XProjectFrame mainframe,  int what,  String outfile,  String infile, boolean isRange, double step,  double leading,  boolean append) {
	this.mainframe=mainframe;
	this.what=what;
	this.filename=outfile;
	this.infile=infile;
	this.isRange=isRange;
	this.step=step;
	this.leading=leading;
	this.append=append;
	this.isBatchMode=false;
    }

    //---
    //--- constructor for the fourier export task
    //---
    public FileTask( XProjectFrame mainframe,  int what,  int id,  String filename) {
	this.mainframe=mainframe;
	this.what=what;
	this.filename=filename;
	this.id=id;
	this.isBatchMode=false;
    }

    //---
    //--- constructor for the fourier export ALL task
    //---
    public FileTask( XProjectFrame mainframe,  int what,  String directory,  boolean usePrefix,  String prefix) {
	this.mainframe=mainframe;
	this.what=what;
	this.directory=directory;
	this.usePrefix=usePrefix;
	this.prefix=prefix;
	this.isBatchMode=false;
    }

    //---
    //--- constructor for the fourier export SELECTED task
    //---
    public FileTask( XProjectFrame mainframe,  int what,  String directory,  int[] selectedindices,  int selected,  boolean usePrefix,  String prefix) {
	this.mainframe=mainframe;
	this.what=what;
	this.directory=directory;
	this.selectedindices=selectedindices;
	this.selected=selected;
	this.usePrefix=usePrefix;
	this.prefix=prefix;
	this.isBatchMode=false;
    }

    //---
    //--- constructor for the log export task
    //---
    public FileTask( XProjectFrame mainframe,  int what,  File file,  String log) {
	this.mainframe=mainframe;
	this.what=what;
	this.file=file;
	this.log=log;
	this.isBatchMode=false;
    }

	//---
	//--- get/set methods
	//---
	public void setVOTable(boolean val) {
		votable = val;
	}


    //---
    //--- start the task.
    //---
    public void go() {
		worker=new SwingWorker() {
				public Object construct() {
					done=false;
					canceled=false;
					return new ActualFileTask();
				}
			};
        worker.start();
    }

    public void get() {
	worker.get();
    }

    //---
    //--- the actual long running task. this runs in a SwingWorker thread.
    //---
    class ActualFileTask {
        ActualFileTask() {
	    while(!isCanceled()&&!isDone()) {
		switch(what) {
		    case PROJECT_IN: 
		    {
			// now go and read in
			String protocol=Period04.projectLoadProject(filename);
			done=true;
			mainframe.setReading(false);

			// update displays
			tabFit.setFitMode(Period04.projectGetFitMode());
			tabFit.updateDataMode();
			mainframe.updateExpertMode();
			mainframe.updateDisplays(); // update the rest
			
			// write to protocol
			tabLog.cleanProtocol(false);
			mainframe.getLog().writeProtocol(protocol,false);
			mainframe.noChanges();
			mainframe.updateDataManager();
			break;
		    }
		    case PROJECT_OUT: 
		    {
			Period04.projectSaveProject(filename);
			done=true;
			mainframe.setSaving(false);
 			if(message) {
			    JOptionPane.showMessageDialog(
				mainframe.getFrame(),
				"The project has been saved in file\n"+
				filename,"",JOptionPane.INFORMATION_MESSAGE);
			}
			mainframe.getLog().writeProtocol(
			    "Saved project:\nSaved project to file: " +
			    filename + "\n",true);
			mainframe.noChanges();
			mainframe.updateDataManager();
			break;
 		    }
		    case TIMESTRING_IN:
		    {
			int first = 0;
			boolean checkFileFormat = false;
			    
			//--- check if files are readable
			//---
			while (!files[first].canRead()) { 
			    JOptionPane.showMessageDialog(
							  mainframe.getFrame(),"File\n"+files[first].getPath()+
							  "does not exist!",
							  "Error", JOptionPane.ERROR_MESSAGE);
			    first++;
			    if (first>=files.length) { return; }
			}
			
			if (isBatchMode) {
			    for (int i=first; i<files.length; i++) {
				if (i>first) { append=true; }
				int ptsbefore=Period04.projectGetTotalPoints();
				//--- replace "no data file(s)"
				mainframe.getTimeStringTab().addFileToFileBox(files[i].getPath());
				// import timestring
				//--- now load the timestring
				Period04.projectLoadTimeString(
							       files[i].getPath(),fileformat,/*append*/0, /*votable*/false);
				//--- write to protocol
				mainframe.getLog().writeProtocol(
								 "\nLoaded "+Period04.projectGetTotalPoints()+
								 " points of data from file: \n"+files[i].getPath()+
								 "\n (fileformat: " + fileformat + ")\n",true);
			    }
			    //--- remove progress bar
			    mainframe.setReading(false);
			    mainframe.updateDisplays();
			    done=true;
			    
			    //--- if fileformat contains u,w then activate weights!
			    if (fileformat.indexOf("w")!=-1 || fileformat.indexOf("u")!=-1) {
				Period04.projectSetUsePointWeight(true);
				StringBuffer text = new StringBuffer();
				text.append("New weight settings:\n"+
					    "Weights are calculated ...\n");
				text.append(
					    "- using the "+
					    "individual weight for each point\n");
				Period04.projectSetUseWeightFlags(); // set flags
				Period04.projectSelect();
				mainframe.getLog().writeProtocol(text.toString(), true);
			    }
			    break;

			} else {
			    
			    //--- check possible input-format of the first file
			    //--- and then check if it fits the user
			    Period04.setCurrentDirectory(files[first]); // set current directory
			    String fileformat = 
				Period04.predictFileFormat(files[first].getPath());
			    XDialogTSFileFormat dialog = 
				new XDialogTSFileFormat(mainframe.getFrame(), fileformat,
							files[first].getPath(), true, false, false);
			    //--- assign new input-format
			    //--- returns "null" if canceled
			    if (fileformat==null) { return; }
			    fileformat = dialog.getFileFormat(); 
			    votable = dialog.getUseVOTableFormat();
			    
			    //--- if multiple files should be read in then ask 
			    //--- the user whether he wants to read in them 
			    //--- using the same file format
			    if (files.length>1) {
				int v = JOptionPane.showConfirmDialog(
								      mainframe.getFrame(), "Do all of your time string files\n"+
								      "have the same file format?", 
								      "Confirm", JOptionPane.YES_NO_OPTION);
				if (v == JOptionPane.NO_OPTION) { 
				    checkFileFormat=true;  
				} else {
				    checkFileFormat=false; 
				}
			    }
						
			    
			    //--- ok, now read in the files one by one
			    //---
			    for (int i=first; i<files.length; i++) {
				if (i>first) { append=true; }
				int ptsbefore=Period04.projectGetTotalPoints();
				
				// check if file is readable
				if (!files[i].canRead()) { 
				    JOptionPane.showMessageDialog(
								  mainframe.getFrame(),"Cannot read from file\n"+
								  files[i].getPath(),
								  "Error", JOptionPane.ERROR_MESSAGE);
				    continue;
				}
				
				if (checkFileFormat && i>first) {
				    // check possible input-format
				    fileformat = Period04.predictFileFormat(files[i].getPath());
				    
				    // and then check if it fits the user
				    dialog = 
					new XDialogTSFileFormat(
							    mainframe.getFrame(), fileformat,
							    files[i].getPath(), true, false, false);
				    // assign new input-format
				    // ... returns "null" if canceled
				    if (fileformat==null) { return; }
				    fileformat = dialog.getFileFormat(); 
				    votable = dialog.getUseVOTableFormat();
				}
				
				if (fileformat!=null) { 
				    //--- put filename into textbox, 
				    //--- replace "no data file(s)"
				    mainframe.getTimeStringTab().addFileToFileBox(files[i].getPath());
				    if (!append) { 
					// import timestring
					//--- now load the timestring
					Period04.projectLoadTimeString(
								       files[i].getPath(),fileformat,/*append*/0, votable);
					//--- write to protocol
					mainframe.getLog().writeProtocol(
									 "\nLoaded "+Period04.projectGetTotalPoints()+
									 " points of data from file: \n"+files[i].getPath()+
									 "\n (fileformat: " + fileformat + ")\n",true);
				    } else { 
					// append timestring
					//--- now load the timestring
					Period04.projectLoadTimeString(
								       files[i].getPath(),fileformat,/*append=*/1, votable);
					//--- write to protocol
					mainframe.getLog().writeProtocol(
									 "\nAppended "+
									 (Period04.projectGetTotalPoints() - ptsbefore)+
									 " points of data from file: \n"+files[i].getPath()+
									 "\n (fileformat: " + fileformat + ")\n",true);
				    }
				    //--- update the time string display
				    mainframe.updateTSDisplay();
				    mainframe.updateDataManager();
				}
			    }
			    
			    if (fileformat!=null && fileformat.indexOf('g')!=-1)
				JOptionPane.showMessageDialog(mainframe.getFrame(),
							      "To use the point errors/weights you've just loaded\n"+
							      "you have to activate them in the\n"+
							      "'Weight selection' dialog.",
							      "",JOptionPane.INFORMATION_MESSAGE);
			    
			    //--- remove progress bar
			    mainframe.setReading(false);
			    done=true;
			    break;
			}
		    }
		    case TIMESTRING_OUT:
		    {
			// now go and save it
			Period04.projectSaveTimeString(filename,fileformat,votable);
			done=true;
			mainframe.setSaving(false);

			// write to protocol
			mainframe.getLog().writeProtocol(
			    "\nSaved "+Period04.projectGetSelectedPoints()+
			    " points to file:\n"+filename+"\n (fileformat: "+
			    fileformat+")\n",true);
			break;
		    }
		    case TIMESTRING_PHASE_OUT:
		    {
			// now go and save it
			Period04.projectSaveTimeStringPhase(
			    filename,fileformat,frequency, zeropoint,votable);
			done=true;
			mainframe.setSaving(false);

			// write to protocol
			mainframe.getLog().writeProtocol(
			    "\nSaved "+Period04.projectGetSelectedPoints()+
			    " points to file:\n"+filename+"\n (fileformat: "+
			    fileformat + ")\n" + "with a frequency of " +
			    frequency + "\nand a zero point at time "+
			    zeropoint+" used to calculate the phases\n",true);
			break;
		    }
		    case TIMESTRING_BINNED_PHASE_OUT:
		    {
			// now go and save it
			Period04.projectSaveTimeStringPhaseBinned(
			    filename,fileformat,frequency,binSpacing,useData,
			    points,phaseArray,amplitudeArray,sigmaArray);
			done=true;
			mainframe.setSaving(false);

			// write to protocol
			mainframe.getLog().writeProtocol(
			    "\nSaved " + Period04.projectGetSelectedPoints() +
			    " binned points to file:\n" + filename + 
			    "\n (fileformat: " + fileformat + ")\n" + 
			    "with a frequency of " + frequency + 
			    "used to calculate the phases\n" + 
			    "and a binned spacing of:" + binSpacing + "\n" + 
			    "using: " + useData + " as amplitudes\n",true);
			break;
		    }
		    case ARTIFICIAL_DATA:
		    {
			Period04.projectCreateArtificialData(
			    filename,starttime,endtime,step,leading,append);
			done=true;
			mainframe.setSaving(false);

			if (isBatchMode) {
			    JOptionPane.showMessageDialog(mainframe.getFrame(),
				"The generated data have been saved in file\n" + 
			        filename,"",JOptionPane.INFORMATION_MESSAGE);
			}

			// write to protocol
			 String s_append;
			if(append) { s_append="Appended"; }
			else        { s_append="Created";  }
			mainframe.getLog().writeProtocol(
			    s_append + " artificial data:" + 
			    "\nfrom:                     " + starttime + 
			    "\nto:                       " + endtime + 
			    "\nStepsize:                 " + step + 
			    "\nLeading/trailing:         " + leading + 
			    "\nwrote results to file:    "+filename+"\n",true);
			break;
		    }
		    case ARTIFICIAL_DATA_FILE:
		    {
			Period04.projectCreateArtificialData(
							     filename,infile,isRange,step,leading,append);
			done=true;
			mainframe.setSaving(false);

			 String s_append;
			if(append) { s_append="Appended"; }
			else        { s_append="Created";  }
			mainframe.getLog().writeProtocol(
			    s_append + " artificial data:" + 
			    "\nusing time "+(isRange?"ranges":"points")+" from file: " + infile + 
			    "\nStepsize:                 " + step + 
			    "\nLeading/trailing:         " + leading + 
			    "\nwrote results to file:    "+filename+"\n",true);
			break;
		    }
		    case FOURIER_OUT:
		    {
			// now go and save it
			Period04.projectSaveFourier(id,filename);
			done=true;
			mainframe.setSaving(false);

			// write to protocol
			mainframe.getTimeStringTab().writeSelectionToProtocol();
			mainframe.getLog().writeProtocol(
			    "Saved Fourier Spectrum:\n" + 
			    "Saved Fourier data with the title \"" + 
			    Period04.fourierGetTitle() + "\"\nto file: " +
			    filename + "\n",true);
			break;
		    }
		    case FOURIER_ALL_OUT:
		    {
			Period04.projectSaveAllFourier(
			    directory,usePrefix,prefix);
			done=true;
			mainframe.setSaving(false);
			mainframe.updateDataManager();

			// write to protocol
			mainframe.getTimeStringTab().writeSelectionToProtocol();
			mainframe.getLog().writeProtocol(
			    "Saved Fourier Spectra:\n" + 
			    "Saved all Fourier spectra in directory\n" + 
			    directory + "\n",true);
			break;
		    }
		    case FOURIER_SELECTED_OUT: 
		    {
			Period04.projectSaveSpecialFourier(
			    directory,selectedindices,selected,usePrefix,prefix);
			done=true;
			mainframe.setSaving(false);
			mainframe.updateDataManager();

			// write to protocol
			mainframe.getTimeStringTab().writeSelectionToProtocol();
			mainframe.getLog().writeProtocol(
			    "Saved Fourier Spectra:\nSaved " + selected + 
			    " Fourier spectra in directory\n" + directory + 
			    "\n",true);
			break;
		    }
		    case LOG_OUT:
		    {
			try {
			    FileWriter fw=new FileWriter(file.getPath());
			    fw.write(log);
			    if(fw!=null) {
				fw.close();
			    }
			} catch( IOException e) {
			    System.err.println(e);
			}
			done=true;
			mainframe.setSaving(false);
			break;
		    }
		}
	    }
	}
    }
}
