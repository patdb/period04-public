/*-----------------------------------------------------------------------*
 *  XDialogWeight
 *  a dialog to set weights
 *-----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.*;                  // Property change stuff
import javax.swing.*;


class XDialogWeight extends XDialog
    implements ActionListener
{
    private static final long serialVersionUID = -8619538247943729369L;

    private XProjectFrame      mainframe;
    private JOptionPane        optionPane;
    private String             typedCutoff = null;
    private final String       s_OK = "OK", s_Cancel = "Cancel";
    private final JCheckBox    checkPointWeight, checkDeviationWeight;
    private final JCheckBox [] checkLabel = new JCheckBox[4];
    private final JTextField   t_Cutoff;

 
    public XDialogWeight(XProjectFrame mframe) {
	super(mframe.getFrame(), true);  // necessary to keep dialog in front
	mainframe = mframe;

        Object[] options = {s_OK, s_Cancel};

	JPanel inputpanel = new JPanel();
	inputpanel.setLayout(null);

	String text = "Please select the weights you "+
	    "want to use for your calculation:";
	JLabel l_text = createBoldLabel(text);
	l_text.setBounds( 10, 0, 390, 30);
	inputpanel.add(l_text, null);

	final int weightflags = Period04.projectGetUseNameWeight();

	// create name checkboxes
	for (int i=0; i<4; i++) {
	    checkLabel[i] = createCheckBox(Period04.projectGetNameSet(i));
	    checkLabel[i].setFont(new Font("Dialog", Font.BOLD, 11));
	    if ( (weightflags&(2<<i)) != 0 ) {
		checkLabel[i].setSelected(true);
	    }
	    checkLabel[i].setBounds( 20, 40+i*20, 100, 20 );
	    inputpanel.add( checkLabel[i], null );
	}
	checkPointWeight = createCheckBox("Point weight");
	checkPointWeight.setFont(new Font("Dialog", Font.BOLD, 11));
	checkPointWeight.setSelected(Period04.projectGetUsePointWeight());
	checkDeviationWeight = createCheckBox("Deviation weight");
	checkDeviationWeight.setFont(new Font("Dialog", Font.BOLD, 11));
	checkDeviationWeight.setSelected(Period04.projectGetUseDeviationWeight());

	final JLabel l_Cutoff = createLabel("Cutoff:");
        t_Cutoff = createTextField(
	    String.valueOf(Period04.projectGetDeviationCutoff()));
	t_Cutoff.setEditable(false);

	JLabel l_Info = createBoldLabel(
	    "<html>Weights assigned to attributes "+
	    "("+Period04.projectGetNameSet(0)+", "+
	    Period04.projectGetNameSet(1)+",...) can be set via "+
	    "\"Edit substring\" in the Time string tab.</html>");

	checkPointWeight    .setBounds(160, 40, 150, 20 );
	checkDeviationWeight.setBounds(160, 60, 150, 20 );
	l_Cutoff            .setBounds(185, 80, 50, 20 );
	t_Cutoff            .setBounds(240, 80, 100, 20 );
	l_Info              .setBounds( 10, 140, 350, 30 );
	inputpanel.add( checkPointWeight, null );
	inputpanel.add( checkDeviationWeight, null );
	inputpanel.add( l_Cutoff, null );
	inputpanel.add( t_Cutoff, null );
	inputpanel.add( l_Info, null );
	Object array = inputpanel;

        optionPane = new JOptionPane(array, 
                                    JOptionPane.PLAIN_MESSAGE,
                                    JOptionPane.YES_NO_OPTION,
                                    null,
                                    options,
                                    options[0]);
        setContentPane(optionPane);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent we) {
                /*
                 * Instead of directly closing the window,
                 * we're going to change the JOptionPane's
                 * value property.
                 */
                    optionPane.setValue(new Integer(JOptionPane.CLOSED_OPTION));
            }
        });

	optionPane.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
                String prop = e.getPropertyName();

                if (isVisible() 
                 && (e.getSource() == optionPane)
                 && (prop.equals(JOptionPane.VALUE_PROPERTY) ||
                     prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
                    Object value = optionPane.getValue();

                    if (value == JOptionPane.UNINITIALIZED_VALUE) {
                        //ignore reset
                        return;
                    }

                    // Reset the JOptionPane's value.
                    // If you don't do this, then if the user
                    // presses the same button next time, no
                    // property change event will be fired.
                    optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

                    if (value.equals(s_OK)) {
			typedCutoff = t_Cutoff.getText();

			// check wether someone put letters into the weight field 
			NumberFormatException nan = null;
			try { Double.valueOf(typedCutoff); } 
			catch (NumberFormatException exception) { nan=exception; }		   

			if (typedCutoff.equals("") || (nan!=null)) {
			    // text was invalid
			    t_Cutoff.selectAll();
			    JOptionPane.showMessageDialog(
				XDialogWeight.this,
				"Sorry, \"" + typedCutoff + "\" "
				+ "isn't a valid number.\n",
				"Try again",
				JOptionPane.ERROR_MESSAGE);
			    typedCutoff = null;
			} 
			else {
			    // we're done; set weights and dismiss the dialog
			    
			    // clean period & fourier weights
			    Period04.projectSetPeriodUseWeight(false);
			    Period04.projectSetFourierUseWeight(false);
			    
			    StringBuffer text = new StringBuffer();
			    text.append("New weight settings:\n"+
					"Weights are calculated ...\n");

			    // calculate the name weight flag
			    int nameflags = 0;
			    for (int i=0; i<4; i++) {
				if (checkLabel[i].isSelected()) {
				    nameflags|=2<<i;
				}
			    }
			    if (weightflags!=nameflags) { 
				// only set it if it's different
				Period04.projectSetUseNameWeight(nameflags);
			    }
			    if (nameflags!=0) { 
				text.append(
				    "- using the weights from the attributes: ");
				for (int j=0;j<4;j++) {
				    if ((nameflags & (2<<j))!=0) {
					text.append(
					    " "+Period04.projectGetNameSet(j));
				    }
				}
				text.append("\n");
			    } else { 
				text.append(
				    "- ignoring the weights from the attributes\n"); 
			    }
 
			    // point weight
			    boolean flag = checkPointWeight.isSelected();
			    Period04.projectSetUsePointWeight(flag);
			    if (flag) {
				text.append(
				    "- using the "+
				    "individual weight for each point\n");
			    } else {
				text.append(
				    "- ignoring the "+
				    "individual weight for each point\n");
			    }
			    // deviation weight
			    flag = checkDeviationWeight.isSelected();
			    Period04.projectSetUseDeviationWeight(flag);
			    if (flag) {
				String cut = t_Cutoff.getText();
				Period04.projectSetDeviationCutoff(cut);
				text.append(
				    "- based on the "+
				    "individual point deviation"+
				    " (Deviation Weight Cutoff value: "
				    +cut+")\n");
			    } else {
				text.append(
				    "- ignoring the "
				    +"individual point deviation\n");
			    }
			    Period04.projectSetUseWeightFlags(); // set flags
			    Period04.projectSelect();
			    mainframe.getLog().writeProtocol(text.toString(), true);
			    setVisible(false);
			}
		    } else { // user closed dialog or clicked cancel
			typedCutoff = null;
			setVisible(false);
		    }
                }
            }
        });

	this.setTitle("Weight selection:");
	this.setSize(400,270);
	this.setResizable(false);
	this.setLocationRelativeTo(mainframe.getFrame());
	this.setVisible(true);
    }

    public void actionPerformed(ActionEvent evt) {
	Object s = evt.getSource();
	if ( s==checkLabel[0] || s==checkLabel[1] || 
	     s==checkLabel[2] || s==checkLabel[3] || 
	     s==checkPointWeight ) {
	    return;
	}
	if (s==checkDeviationWeight) {
	    if (checkDeviationWeight.isSelected()) {
		t_Cutoff.setEditable(true);
	    } else {
		t_Cutoff.setEditable(false);
	    }
	    return;
	}
	optionPane.setValue(s_OK);
    }
}
