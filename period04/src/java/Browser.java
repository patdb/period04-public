

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Vector;
import javax.swing.JOptionPane;

/**
* A simple, static class to display a URL in the system browser.
*
* Under Windows, this will bring up the default browser under windows,
* usually either Netscape or Microsoft IE.  The default browser is
* determined by the OS.  This has been tested under Windows 95/98/NT.
*
* Note - you must include the url type -- either "http://" or
* "file://".
*/
public class Browser
{
    /**
     * Display a file in the system browser.  If you want to display a
     * file, you must include the absolute path name.
     */
    public static void displayURL( String url) {
	final String[] cmds=defaultCommands();
	if(cmds==null||cmds.length==0) {
	    JOptionPane.showMessageDialog(
		null, "Period04 failed to find a HTML-Browser!\n\n" + 
		"Please install a HTML-Browser in order to use Period04-Help.\n" + 
		"If an existent Browser was not found, please\n" +
		"report this Bug on the Period04 Webpage:\n" +
		"http://www.dsn.astro.univie.ac.at/~dsn/dsn/Period04\n\n" + 
		"Meanwhile you can have a look at the Help HTML-Files\n" +
		"which are situated in the Period04 installation directory.",
		"Error opening Browser",JOptionPane.ERROR_MESSAGE);
	    return;
	}
	final String[] urlArray=new String[1];
	urlArray[0]=url;
	boolean found=false;
	String command=null;

	try {
	    for( int i=0;i<cmds.length&&!found;i++) {
		try {
		    command=MessageFormat.format(cmds[i],(Object [])urlArray);
		    final Process p=(Runtime.getRuntime()).exec(command);

		    // give the browser a bit of time to fail.
		    // I have found that sometimes sleep doesn't work
		    // the first time, so do it twice.  My tests
		    // seem to show that 1000 milliseconds is enough
		    // time for the browsers I'm using.
		    for( int j=0;j<2;j++) {
			try
			{
			    (Thread.currentThread()).sleep(1000);
			} catch( InterruptedException inte) {
			}
		    }
		    if(p.exitValue()==0) {
			// this is a weird case.  The browser exited after
			// a couple seconds saying that it successfully
			// displayed the url.  Either the browser is lying
			// or the user closed it *really* quickly.  Oh well.
			found=true;
		    }
		} catch( IOException x) {
		    // the command was not a valid command.
		    System.err.println("warning" + " " + x.getMessage());
		}
	    }
	    if(!found) {
		// we never found a command that didn't terminate with an error.
		System.err.println("failed");
	    }
	} catch( IllegalThreadStateException e) {
	    // the browser is still running.  This is a good sign.
	    // lets just say that it is displaying the url right now!
	}

    }public static String[] defaultCommands() { String[] exec=null;
	if((System.getProperty("os.name")).startsWith("Windows")) {
	    exec=new String[]{"rundll32 url.dll,FileProtocolHandler {0}"};
	} else if((System.getProperty("os.name")).startsWith("Mac")) {
	    final Vector browsers=new Vector();
	    try {
		final Process p=(Runtime.getRuntime()).exec("which open");
		if(p.waitFor()==0) {
		    browsers.add("open {0}");
		}
	    } catch( IOException e) {
	    } catch( InterruptedException e) {
	    }
	    if(browsers.size()==0) {
		exec=null;
	    } else {
		exec=((String[]) browsers.toArray(new String[0]));
	    }
	} else {
	    final Vector browsers=new Vector();
	    try {
		final Process p=(Runtime.getRuntime()).exec("which firefox");
		if(p.waitFor()==0) {
		    browsers.add("firefox -remote openURL({0})");
		    browsers.add("firefox {0}");
		}
	    } catch( IOException e) {
	    } catch( InterruptedException e) {
	    }
	    try {
		final Process p=(Runtime.getRuntime()).exec("which firebird");
		if(p.waitFor()==0) {
		    browsers.add("firebird -remote openURL({0})");
		    browsers.add("firebird {0}");
		}
	    } catch( IOException e) {
	    } catch( InterruptedException e) {
	    }
	    try {
		final Process p=(Runtime.getRuntime()).exec("which mozilla");
		if(p.waitFor()==0) {
		    browsers.add("mozilla -remote openURL({0})");
		    browsers.add("mozilla {0}");
		}
	    } catch( IOException e) {
	    } catch( InterruptedException e) {
	    }
	    try {
		final Process p=(Runtime.getRuntime()).exec("which opera");
		if(p.waitFor()==0) {
		    browsers.add("opera -remote openURL({0})");
		    browsers.add("opera {0}");
		}
	    } catch( IOException e) {
	    } catch( InterruptedException e) {
	    }
	    try {
		final Process p=(Runtime.getRuntime()).exec("which galeon");
		if(p.waitFor()==0) {
		    browsers.add("galeon {0}");
		}
	    } catch( IOException e) {
	    } catch( InterruptedException e) {
	    }
	    try {
		final Process p=(Runtime.getRuntime()).exec("which konqueror");
		if(p.waitFor()==0) {
		    browsers.add("konqueror {0}");
		}
	    } catch( IOException e) {
	    } catch( InterruptedException e) {
	    }
	    try {
		final Process p=(Runtime.getRuntime()).exec("which netscape");
		if(p.waitFor()==0) {
		    browsers.add("netscape -remote openURL({0})");
		    browsers.add("netscape {0}");
		}
	    } catch( IOException e) {
	    } catch( InterruptedException e) {
	    }
	    try {
		 Process p=(Runtime.getRuntime()).exec("which xterm");
		if(p.waitFor()==0) {
		    p=(Runtime.getRuntime()).exec("which lynx");
		    if(p.waitFor()==0) {
			browsers.add("xterm -e lynx {0}");
		    }
		}
	    } catch( IOException e) {
	    } catch( InterruptedException e) {
	    }
	    if(browsers.size()==0) {
		exec=null;
	    } else {
		exec=((String[]) browsers.toArray(new String[0]));
	    }
	}
	return exec;
    }

}
