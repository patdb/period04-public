/*---------------------------------------------------------------------*
 *  XPlotPhase.java
 *  creates the phase plot
 *---------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.awt.print.PrinterJob;
import java.awt.print.PrinterException;
import java.beans.PropertyVetoException;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import javax.help.CSH;
import javax.swing.*;
import javax.swing.border.*;

import gov.noaa.pmel.sgt.AbstractPane;
import gov.noaa.pmel.sgt.Axis;
import gov.noaa.pmel.sgt.PlainAxis;
import gov.noaa.pmel.sgt.AxisNotFoundException;
import gov.noaa.pmel.sgt.Attribute;
import gov.noaa.pmel.sgt.PointAttribute;
import gov.noaa.pmel.sgt.LineAttribute;
import gov.noaa.pmel.sgt.CartesianGraph;
import gov.noaa.pmel.sgt.PlotMark;//use modified PlotMark class
import gov.noaa.pmel.sgt.Graph;
import gov.noaa.pmel.sgt.SGLabel;
import gov.noaa.pmel.sgt.dm.SGTData;
import gov.noaa.pmel.sgt.dm.SGTLine;
import gov.noaa.pmel.sgt.dm.SimplePoint;
import gov.noaa.pmel.sgt.dm.SimpleLine;
import gov.noaa.pmel.sgt.dm.SGTMetaData;
import gov.noaa.pmel.sgt.dm.Collection;
import gov.noaa.pmel.sgt.dm.PointCollection;

import gov.noaa.pmel.sgt.swing.XPlotLayout;

import gov.noaa.pmel.util.Domain;
import gov.noaa.pmel.util.Range2D;
import gov.noaa.pmel.util.Point2D;
import gov.noaa.pmel.util.SoTPoint;

public class XPlotPhase extends XPlot 
    implements ComponentListener
{

    private JMenuBar    menuBar = new JMenuBar();
    private JMenu       menuFile, menuColor, menuData, menuZoom, menuHelp;
    private JMenuItem   i_Print, i_ExportJPG, i_ExportEPS, i_ExportPhases;
    private JMenuItem   i_ExportBinnedPhases, i_Close;
    private JMenuItem   i_DisplayAll, i_SelectViewport, i_Back;
    private JMenuItem   i_PlotHelp;
    private JCheckBoxMenuItem [] i_Color = new JCheckBoxMenuItem[4];
    private JCheckBoxMenuItem [] i_Data  = new JCheckBoxMenuItem[4];
    private ActionListener action;
	private int mFreqs;

    // toppanel
    private Color      toppanelColor = new Color(230,225,217);
    private JComboBox  cb_UseFrequency;
    private JTextField t_useFrequency;
    private JCheckBox  cb_UseBinning;
    private JTextField t_binning;
    private JTextField t_zeropoint;

    private double     zeropoint = 0.0;
    private double     frequency = 1;
    private double  [] xArray;         // arrays to hold spline data
    private double  [] yArray;
    private SGTData [] data;

    private double [] binAmplitude;
    private double [] binPower;
    private double [] binPhase;

    private double [] domain;
    private int     colorAttribute;
    private int     useData;
    private boolean isPlotBinning = false;
    private int     binsize = 0;
    private int     bins = 10;
    private int []  freqindices;
    private String [] frequencies;

    private XProjectFrame mainframe;
    private boolean    blockFrequencyUpdate = false;

    public XPlotPhase(XProjectFrame mainframe) {
		this.mainframe = mainframe;
		mFreqs = 0;

		layout = new XPlotLayout();
		mainpanel = new JPanel();
		frequency = Period04.projectGetFirstFrequency();
		
		makeMenuBar();
		statusBar = new JLabel(stdtext);
		statusBar.setFont(new Font("Dialog", Font.PLAIN, 11));
		statusBar.setBorder(
			BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
		
		JPanel plotpanel = new JPanel();
		plotpanel.setLayout(new BorderLayout());
		plotpanel.add(layout, BorderLayout.CENTER);
		
		Container content = getContentPane();
		content.setLayout(new BorderLayout());
		content.add(plotpanel, BorderLayout.CENTER);
		content.add(createTopPanel(), BorderLayout.NORTH);
		content.add(statusBar, BorderLayout.SOUTH);

		//---
		//--- add listeners
		//---
		// listen for window closing events and release resources
		addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent event) {
					JFrame fr = (JFrame)event.getSource();
					fr.setVisible(false);
					fr.dispose();
					XPlotPhase.this.mainframe.dereferencePhasePlot();
				}
				public void windowActivated(WindowEvent we) {
					XPlotPhase.this.mainframe.updateCurrentProject();
				}
			});
		// listen for frame size changes
		addComponentListener(this);
		// listen for mouse motions
		layout.addMouseMotionListener(new MyMouse());
		  // listen for key events
		/*addKeyListener(new KeyListener() {
			  @Override
			  public void keyPressed(KeyEvent e) {
System.out.println(e.getKeyCode());
			      if (e.getKeyCode()==521)
			      {
				PlotMark.increaseMarkSize();
				layout.revalidate();
			      }
				if (e.getKeyCode()==45)
			      {
				PlotMark.decreaseMarkSize();
				layout.revalidate();
			      }
			  }

			  @Override
			  public void keyTyped(KeyEvent e) { }

			  @Override
			  public void keyReleased(KeyEvent e) { }
		  });*/

		layout.setBatch(true);  // Batch changes to layout.
		
		// read in the first inactive frequency
		readData();             // read the data and add to layout
		layout.setPlotTitle("Using Frequency:    "+format(frequency,9));
		layout.setBatch(false); // now execute changes to layout
		
		setTitle("Phase plot");        // configure frame
		setSize(500,450);
		setLocationRelativeTo(mainframe.getFrame());
		setVisible(true);                    // show it
    }

    public void makeMenuBar() {
	// create main-menu
	menuFile  = XMenuHelper.addMenuBarItem( menuBar, "_Graph" );
	menuColor = XMenuHelper.addMenuBarItem( menuBar, "_Colors" );
	menuData  = XMenuHelper.addMenuBarItem( menuBar, "_Data" );
	menuZoom  = XMenuHelper.addMenuBarItem( menuBar, "_Zoom" );
	menuHelp  = XMenuHelper.addMenuBarItem( menuBar, "_Help" );

	// create action listener for menu items
	action = new ActionListener() {
	    public void actionPerformed( ActionEvent evt ) {
		Object s=evt.getSource();
		// graph menu
		if (s==i_Print)      { print(); return; }
		else if (s==i_ExportEPS) { exportImage(XPlot.EPS); return; }
		else if (s==i_ExportJPG) { exportImage(XPlot.JPG); return; }
		else if (s==i_ExportPhases) { exportPhases(false); return; }
		else if (s==i_ExportBinnedPhases) { 
		    exportPhases(true); return; 
		}
		else if (s==i_Close) { dispose(); return;}
		// color menu
		for (int i=0; i<4; i++) {
		    if (s==i_Color[i]) {
			if (i_Color[i].isSelected()) { 
			    setUseAttribute(i);  return;
			}
		    }
		}
		// data menu
		for (int i=0; i<4; i++) {
		    if (s==i_Data[i]) {
			if (i_Data[i].isSelected()) { setUseData(i); return; }
		    }
		}
		// zoom menu
		if (s==i_DisplayAll)          { resetZoom(); return; }
		else if (s==i_SelectViewport) { setZoom("Phase","Magnitude"); return; } 
		else if (s==i_Back)           { layout.undoZoom(); return; }
		//
		else if (s==cb_UseFrequency) {
		    if (blockFrequencyUpdate) { return; }
		    if (cb_UseFrequency.getSelectedItem()=="Other Frequency") {
			t_useFrequency.setEditable(true);
			t_useFrequency.setBackground(Color.WHITE);
		    } else {
			t_useFrequency.setEditable(false);
			t_useFrequency.setBackground(toppanelColor);
			int i= freqindices[cb_UseFrequency.getSelectedIndex()];
			t_useFrequency.setText(
			    Period04.projectGetFrequency(i));

			frequency = Period04.projectGetFrequencyValue(i);
			layout.setPlotTitle("Using Frequency:    "+
					    Period04.projectGetFrequency(i));
			replot();
		    }
		    return;
		}
		else if (s==t_useFrequency) {
		    if (!checkDouble(t_useFrequency, false)) { return; }
		    frequency = Double.parseDouble(t_useFrequency.getText());
		    layout.setPlotTitle("Using Frequency:    "+frequency);
		    replot();
		    return;
		}
		else if (s==cb_UseBinning) {
		    if (cb_UseBinning.isSelected()) { // use binning
			t_binning.setEditable(true);
			t_binning.setBackground(Color.WHITE);
			isPlotBinning = true;
			replot();
		    } else { // no binning
			t_binning.setEditable(false);
			t_binning.setBackground(toppanelColor);
			isPlotBinning = false;
			replot();
		    }
		    return;
		}
		else if (s==t_binning) {
		    if (!checkDouble(t_binning, true)) { return; }
		    bins = Integer.parseInt(t_binning.getText());
		    if (isPlotBinning) { 
			replot();
		    }
		    return;
		}
		else if (s==t_zeropoint) {
		    if (!checkDouble(t_zeropoint, false)) { return; }
		    zeropoint = Double.parseDouble(t_zeropoint.getText());
		    replot();
		}
	    }
	};
	//---
	//--- file menu	
	//---
	i_Print = XMenuHelper.addMenuItem(menuFile, "Print Graph", action);
	JMenu export = new JMenu("Export Graph As ");
	menuFile.add( export );
	i_ExportEPS = XMenuHelper.addMenuItem(export, "eps", action);
	i_ExportJPG = XMenuHelper.addMenuItem(export, "jpg", action);
	// ...	menuFile.addSeparator();
	i_ExportPhases = XMenuHelper.addMenuItem(
	    menuFile, "Export Phases",  action);
	i_ExportBinnedPhases = XMenuHelper.addMenuItem(
	    menuFile, "Export Binned Phases",  action);
	menuFile.addSeparator();
	i_Close = XMenuHelper.addMenuItem(menuFile, "Close", action);
	//---
	//--- color menu
	//---
	ButtonGroup bg1 = new ButtonGroup(); 
	for (int i=0; i<4; i++) {
	    i_Color[i] = XMenuHelper.addCheckBoxMenuItem(
		menuColor, Period04.projectGetNameSet(i), action);
	    bg1.add(i_Color[i]);
	}
	i_Color[0].setSelected(true);
	colorAttribute=0;       
	//---
	//--- data menu
	//---
	ButtonGroup bg2 = new ButtonGroup();
	String [] entries = {"Observed", "Adjusted", 
			     "Residuals-Observed", "Residuals-Adjusted"};
	for (int i=0; i<4; i++) {
	    i_Data[i] = XMenuHelper.addCheckBoxMenuItem(
		menuData, entries[i], action);
	    bg2.add(i_Data[i]);
	}
	useData = 2;
	i_Data[useData].setSelected(true);
	//---
	//--- zoom menu
	//---
	i_DisplayAll = XMenuHelper.addMenuItem(
	    menuZoom,"_Display all", 'D', action);
	i_SelectViewport = XMenuHelper.addMenuItem(
	    menuZoom,"Select _viewport", 'V', action);
	i_Back = XMenuHelper.addMenuItem(
	    menuZoom, "_Back", 'B', action);
	//---
	//--- help
	//---
	i_PlotHelp = XMenuHelper.addMenuItem(
	    menuHelp, "Phase plots",
	    new CSH.DisplayHelpFromSource(XProjectFrame.getHelpBroker()));
	CSH.setHelpIDString(i_PlotHelp,"gui.phaseplots"); // set link

	// add it to the panel
	this.setJMenuBar( menuBar );
    }

    private JPanel createTopPanel() {
	JPanel panel = new JPanel();
	panel.setLayout(new BoxLayout(panel,BoxLayout.PAGE_AXIS));
	panel.setBorder(BorderFactory.createEtchedBorder());
	int fr = updateFrequencies();

	//---
	//--- first line
	//---
	JPanel panel1 = new JPanel();
	panel1.setLayout(new FlowLayout(FlowLayout.LEFT, 2, 2));
	panel1.setBackground(toppanelColor);

	cb_UseFrequency = new JComboBox(frequencies);
	cb_UseFrequency.setSelectedIndex(0);
	cb_UseFrequency.setBackground(toppanelColor);
	cb_UseFrequency.addActionListener(action);
	t_useFrequency = new JTextField(format(frequency,9), 10);
	if (fr>1) {
	    t_useFrequency.setEditable(false);
	    t_useFrequency.setBackground(toppanelColor);
	} else { // only "Other Frequency" in list
	    t_useFrequency.setEditable(true);
	    t_useFrequency.setBackground(Color.WHITE);
	}
	t_useFrequency.addActionListener(action);

	JLabel spacer1 = new JLabel("     ");

	cb_UseBinning = new JCheckBox("Use Binning");
	cb_UseBinning.setSelected(false);
	cb_UseBinning.setBackground(toppanelColor);
	cb_UseBinning.addActionListener(action);
	t_binning = new JTextField("10", 5);
	t_binning.setEditable(false);
	t_binning.setBackground(toppanelColor);
	t_binning.addActionListener(action);

	panel1.add(cb_UseFrequency);
	panel1.add(t_useFrequency);
	panel1.add(spacer1);
	panel1.add(cb_UseBinning);
	panel1.add(t_binning);

	//---
	//--- second line
	//---
	JPanel panel2 = new JPanel();
	panel2.setLayout(new FlowLayout(FlowLayout.LEFT, 2, 2));
	panel2.setBackground(toppanelColor);
	
	JLabel l_zeropoint = new JLabel("Zero point (time): ");
	l_zeropoint.setPreferredSize(new Dimension(130,20));
	t_zeropoint = new JTextField("0.0");
	t_zeropoint.setPreferredSize(new Dimension(120,20));
	t_zeropoint.addActionListener(action);
	panel2.add(l_zeropoint);
	panel2.add(t_zeropoint);

	panel.add(panel1);
	panel.add(panel2);

	return panel;
    }

    private int updateFrequencies() {
		// count number of unused frequencies
		int points = 0;
		mFreqs = Period04.projectGetTotalFrequencies();
		for (int i=0; i<mFreqs; i++) {
			if (!Period04.projectGetActive(i)) {
				if (!Period04.periodIsEmpty(i) && 
					Period04.projectGetFrequencyValue(i)!=0.0) {
					points++;
				}
			}
		}
		points++;  // -> for "Other Frequency"
		freqindices = new int[points];
		// create the array with inactive frequencies
		frequencies = new String[points];
		
		int pos = 0;
		if (points>1) {
			for (int i=0; i<mFreqs; i++) {
				if (!Period04.projectGetActive(i)) {
					
					if (!Period04.periodIsEmpty(i) && 
						Period04.projectGetFrequencyValue(i)!=0.0) 
						{
							freqindices[pos] = i;
							frequencies[pos] = 
								"Using "+Period04.projectGetNumber(i)+
								Period04.projectGetFrequency(i)+": ";
							pos++;
						}
				}
			}
		} // if (points>1)
		frequencies[pos] = "Other Frequency";
		freqindices[pos] = -1;
		return points;
    }

    private void readData() {

	if (Period04.projectGetSelectedPoints()<=0) { return; }
	int firstPoint = 0;
	int lastPoint  = Period04.projectGetSelectedPoints();
	int names = Period04.projectNumberOfNames(colorAttribute);
	PointCollection [] data = new PointCollection[names];
	int idName=0;
	int [] idColor      = new int[names];
	int [] idConversion = new int[names];
	boolean reverse = false;
	boolean found = false;
	int i;

	// use the appropriate index !!
	for (i=0; i<names; i++) {
	    idConversion[i] = Period04.projectGetIndexNameID(colorAttribute,i);
	}

	double maxAmp=-1.0E20, minAmp=1.0E20;
	for (i=firstPoint; i<lastPoint; i++) {
	    idName = Period04.projectTimePointGetIDName(colorAttribute,i);
	    found = false;
	    for (int j=0; j<names; j++) {
		if (idName==idConversion[j]) { idName = j; found=true; break; }
	    }
	    if (!found) {
		System.err.println("Error in readData: Cannot find index!");
	    }

	    if (data[idName]==null)
		data[idName] = new PointCollection();

	    double time, amp, error;
            //
	    //...error?
	    time = Period04.timepointGetPhasedTime(i, frequency, zeropoint);

	    amp = Period04.projectTimePointGetAmplitude(i, useData);
	    data[idName].add(new SimplePoint(time, amp, "phase"));
	    if (amp>maxAmp) { maxAmp=amp; }
	    if (amp<minAmp) { minAmp=amp; }
	}

	for (i=0; i<names; i++) {
	    int id = mainframe.getTimeStringTab().retrieveListEntryID(
		colorAttribute, i);
	    idColor[i] = Period04.projectGetIDNameColor(id, colorAttribute);
	    if (data[i]!=null) {
		SGTMetaData meta = new SGTMetaData("Phase","", false, false);
		data[i].setXMetaData(meta);
		reverse = Period04.projectGetReverseScale();
		switch (useData) {
		    case 1:
			meta = new SGTMetaData("Adjusted","", reverse, false);
			break;
		    case 2:
			meta = new SGTMetaData(
			    "Residuals (Obs)","", reverse, false);
			break;
		    case 3:
			meta = new SGTMetaData(
			    "Residuals (Adj)","", reverse, false);
			break;
		    default:
			meta = new SGTMetaData("Observed","", reverse, false);
			break;
		}
		data[i].setYMetaData(meta);
		data[i].setId("Data "+i);
	    }
	}

	int counter=0;
	for (i=0; i<names; i++) {
	    if (data[i]!=null) {
		PointAttribute pa = new PointAttribute(
		    51, JColor.getColor(idColor[i]));
		pa.setMarkHeightP( 0.2 );
		layout.addData(data[i],pa,"", XPlotLayout.POINTS);  
		counter=i;
		break;
	    }
	}
	if(counter+1<names) {
	    for (i=counter+1; i<names; i++) {
		if (data[i]!=null) {
		    PointAttribute pa = new PointAttribute(
			51, JColor.getColor(idColor[i]));
	      	    pa.setMarkHeightP( 0.2 );
		    layout.addData(data[i], pa, "", XPlotLayout.POINTS);
		}
	    }
	}
	this.data = data; // save data

	// adjust range
	Range2D yr = new Range2D(minAmp,maxAmp);
	Range2D xr = new Range2D(0.0,1.0);
	Domain d = new Domain(xr,yr);
	//	d.setYReversed(reverse);
	try {
	    layout.setRange(d);   // set user range 
	} catch(PropertyVetoException e) {
	    System.err.println(e);
	}
    }

    private void plotBinning() {

	binsize = 1+bins;
	ArrayList splinelist = new ArrayList();
	binAmplitude = new double[binsize];
	binPower     = new double[binsize];
	binPhase     = new double[binsize];
	int [] binCount     = new int[binsize];
	Point2D.Double point;
	int i;
	boolean reverse = Period04.projectGetReverseScale();

	// filling in data
	for (i=0; i<binsize; i++) {
	    binAmplitude[i]=0;
	    binPower[i]=0;
	    binPhase[i]=(double)i/bins + 1.0/(2.0*bins);
	    binCount[i]=0;
        }
	for (i=0; i<Period04.projectGetSelectedPoints(); i++) {
	    //get cordinates
	    double time     =Period04.timepointGetPhasedTime(i, frequency, zeropoint);
	    double amplitude=Period04.projectTimePointGetAmplitude(i, useData);
	    int bin = (int)(time*bins);
	    if (bin<binsize) {
		binCount[bin]++;
		binAmplitude[bin] += amplitude;
		binPower[bin] += amplitude*amplitude;
            } else {
		System.err.println("Something wrong with binning!!!");
	    }
        }
	// normalize data
	for (i=0; i<bins; i++) {
	    int n = binCount[i];
	    if (n!=0) { 
		binAmplitude[i]=binAmplitude[i]/n;
		if (n!=1) {
		    binPower[i] = Math.sqrt(
			(binPower[i]-n*binAmplitude[i]*binAmplitude[i]) /
			((n-1)*n) );
                } else {
		    binPower[i]=0.0;
                }
            }
        }
    
	// a collection to plot the binning points
	PointCollection pointdata = new PointCollection();

	// add the first point to the spline point list
	// using an appropriate amplitude (note: binAmp[binsize-1]==0.0)
	double mAmp = (binAmplitude[0]+binAmplitude[binsize-2])/2.0;
	splinelist.add(new Point2D.Double(0.0, mAmp));

	//--- calculate the binning points, 
	//--- set plotmarkID=2 in order to visualize them as crosses,
	//--- and add them to the plot
	for (i=0; i<binsize; i++) {
	    if ( binCount[i]!=0 ) {
		pointdata.add(new SimplePoint(
			     binPhase[i], binAmplitude[i], "binpoints"));
		splinelist.add(
		    new Point2D.Double(binPhase[i], binAmplitude[i]));
	    }
	}

	// finally add the last point to the spline point list
	splinelist.add(new Point2D.Double(1.0, mAmp));

	SGTMetaData meta = new SGTMetaData("Phase","", false, false);
	pointdata.setXMetaData(meta);
	switch (useData) {
	    case 1:
		meta = new SGTMetaData("Adjusted","", reverse, false);
		break;
	    case 2:
		meta = new SGTMetaData("Residuals (Obs)","", reverse, false);
		break;
	    case 3:
		meta = new SGTMetaData("Residuals (Adj)","", reverse, false);
		break;
	    default:
		meta = new SGTMetaData("Observed","", reverse, false);
		break;
	}
	pointdata.setYMetaData(meta);
	pointdata.setId("Binning Points");
	PointAttribute pa = new PointAttribute();
	pa.setMark(2); // plotmark = "+"      // muss noch adaptiert werden!!!
	pa.setMarkHeightP(1.2);
	layout.addData(pointdata, pa, "Binning Points", XPlotLayout.POINTS);

	//---
	//--- calculate a spline for the binning points
	//---
	int pts = splinelist.size();
	double [][] points = new double[pts][2];
	Iterator it = splinelist.iterator();
	i = 0;
	while(it.hasNext()) {
	    point = (Point2D.Double)it.next();
	    points[i][0]= point.x;
	    points[i][1]= point.y;
	    i++;
	}
	SplineInterpolation interpol = new SplineInterpolation(points);
	SplineInterpolation.Spline spline = interpol.getSpline();
	
	//--- clear the splinelist, 
	//--- fill it with splinepoints for every screen pixel
	//--- and add the spline curve to the plot

	splinelist.clear();

	CartesianGraph g = 
	    (CartesianGraph) layout.getFirstLayer().getGraph();
	Axis a;
	try {
	    a = g.getXAxis("Bottom Axis");    
	    Range2D xr = a.getRangeP();   // get physical range of x-axis
	    a = g.getYAxis("Left Axis");
	    Range2D yr = a.getRangeP();   // get physical range of y-axis
		
	    double x0P = xr.start;        // physical coordinates 
	    double x1P = xr.end;          // of the domain
	    double y0P = yr.start;
	    double y1P = yr.end;

	    // device coordinates of the domain
	    int x0D = g.getXUtoD(0.0);//g.getXPtoU( x0P )); 
	    int x1D = g.getXUtoD(1.0);//g.getXPtoU( x1P ));
	    int y0D = g.getYUtoD(g.getYPtoU( y0P ));
	    int y1D = g.getYUtoD(g.getYPtoU( y1P ));

	    for (int j=x0D; j<=x1D; j++) {
		Point2D.Double tmp = transformDtoU(j, y0D);
		point = new Point2D.Double(tmp.x, spline.function(tmp.x));

		splinelist.add(point);
	    }
	} catch(Exception e) {
	    System.out.println(e);
	}

	// copy splinelist into xArray/yArray
	xArray = new double[splinelist.size()];   
	yArray = new double[splinelist.size()];
	it = splinelist.iterator();
	i=0;
	while(it.hasNext()) {
	    point = (Point2D.Double)it.next();
	    xArray[i] = point.x;
	    yArray[i] = point.y;
	    i++;
	}

	layout.setClipping(true); // cut off elements outside the domain
	
	// add the spline curve to the plot
	SimpleLine splinedata = new SimpleLine(xArray, yArray, "");
	splinedata.setId("Binning Spline");
	meta = new SGTMetaData();
	splinedata.setXMetaData(meta);
	meta = new SGTMetaData("","", reverse, false);
	splinedata.setYMetaData(meta);
	SGTData sgtdata = splinedata;
	layout.addData(sgtdata, 
		       new LineAttribute(LineAttribute.HEAVY),
		       "Binning Spline", XPlotLayout.LINE);

	//---
	//--- draw a line at y=0 if neccessary
	//---
	Domain d = layout.getRange();
	Range2D yr = d.getYRange();
	if (yr.start*yr.end<0) {
	    //--- ok, the start and end of the range have different signs,
	    //--- so let's draw the line at zero
	    double [] xline0 = {0.0, 1.0};
	    double [] yline0 = {0.0, 0.0};
	    SimpleLine zeroline = new SimpleLine(xline0, yline0, "");
	    zeroline.setId("Zero");
	    meta = new SGTMetaData();
	    zeroline.setXMetaData(meta);
	    meta = new SGTMetaData("","", reverse, false);
	    zeroline.setYMetaData(meta);
	    SGTData linedata = zeroline;
	    layout.addData(linedata, 
			   new LineAttribute(LineAttribute.DASHED,Color.BLACK),
			   "Zero", XPlotLayout.LINE);
	}

	//---
	//--- adjust the ranges
	//---
	Range2D xr = new Range2D(0.0,1.0);
	d = new Domain(xr,yr);
	d.setYReversed(reverse);
	try {
	    layout.setRange(d);   // set user range 
	} catch(PropertyVetoException e) {
	    System.err.println(e);
	}

	layout.setClipping(false); // set old value
    }

    //---
    //--- replot using the given attribute
    //---
    private void setUseAttribute(int i) {
	colorAttribute = i;
	replot();
    }

    //---
    //--- replot using the given data
    //---
    private void setUseData(int i) {
	useData = i;
	replot();
    }

    public void update() {
		int f = Period04.projectGetTotalFrequencies();
		if (f==mFreqs) { return; }
		updateFrequencies();
		blockFrequencyUpdate = true;
		cb_UseFrequency.removeAllItems();
		for (int i=0; i<frequencies.length; i++) {
			cb_UseFrequency.addItem(frequencies[i]);
		}
		blockFrequencyUpdate = false;
		if (frequencies.length>1) {
			cb_UseFrequency.setSelectedIndex(frequencies.length-2);
		} else {
			t_useFrequency.setEditable(true);
			t_useFrequency.setBackground(Color.WHITE);
		}
		replot();
    }

    public void replot() {
	layout.setBatch(true);  // batch changes
	layout.resetZoom();     // ... necessary to prevent ugly effects
	layout.clear();         // clear the plot
	if (isPlotBinning) {    // calculate binning and add to layout
	    plotBinning();
	}
	readData();             // read the data and add to layout
	layout.setBatch(false); // now execute changes
    }

    protected void resetZoom() {
	layout.setBatch(true);
	layout.resetZoom();
	layout.setClipping(false);
	layout.setBatch(false);
    }

    //---
    //--- is it a valid double number?
    //---
    public boolean checkDouble(JTextField tf, boolean checknegative) {
	String text = tf.getText();
	// check if text contains letters 
	NumberFormatException nan = null;
	try { 
	    Double.parseDouble(text); 
	} catch (NumberFormatException exception) { 
	    nan=exception; 
	}		   
	// check if empty
	if (text.equals("") || (nan!=null)) {
	    // text was invalid
	    tf.selectAll();
	    JOptionPane.showMessageDialog(
		this,"Sorry, \"" + text + "\" "
		+ "isn't a valid entry.\n", "Invalid entry",
		JOptionPane.ERROR_MESSAGE);
	    return false;
	} 
	if (checknegative) { // check if negative
	    if (Double.parseDouble(text)<=0.0) {
		tf.selectAll();
		JOptionPane.showMessageDialog(
		    this,"Sorry, \"" + text + "\" "
		    + "isn't a valid entry.\n", "Invalid entry",
		    JOptionPane.ERROR_MESSAGE);
		return false;
	    }
	}
	return true;
    }
    
    public void exportPhases(boolean binned) {
		String outputformat = "to";
		boolean votable = false;
		if (binned && !isPlotBinning) {
			JOptionPane.showMessageDialog(
				this,"Sorry, you have to activate binning\n"+
				" to export binned phases.\n", "Error",
				JOptionPane.ERROR_MESSAGE);
			return;
		}
		if (!binned) {
			//--- check if output format fits the user
			XDialogTSFileFormat dialog = 
				new XDialogTSFileFormat(mainframe.getFrame(),
										Period04.timestringGetOutputFormat(), 
										"", false, true, false);
			//--- assign new output-format
			outputformat = 
				dialog.getFileFormat(); // returns "null" if canceled
			votable = dialog.getUseVOTableFormat();
			if (outputformat==null) { return; }
		}


    //--- create a filechooser
    JFileChooser fc = new JFileChooser(Period04.getCurrentDirectory());
    fc.setFileFilter( new TimestringFileFilter( ));
//    fc.addChoosableFileFilter( new TimestringFileFilter( ));


	int returnVal = fc.showSaveDialog(mainframe.getFrame());
	if (returnVal == JFileChooser.APPROVE_OPTION) {
	    File file = fc.getSelectedFile();
	    // check if the extension is missing
	    String name = file.getName();
	    if (name.indexOf(".")==-1) { 
			if (votable)
				file = new File(file.getPath()+".xml");			
			else
				file = new File(file.getPath()+".dat");
	    }
	    // check wether user wants to overwrite an existing file
	    if (file.exists()) {
		int v = JOptionPane.showConfirmDialog(
		    mainframe.getFrame(), "File does already exist!\n"+
		    "Do you really want to delete the old file?", 
		    "Warning", JOptionPane.YES_NO_OPTION);
		if (v == JOptionPane.NO_OPTION) {
		    return;
		}
	    }
	    Period04.setCurrentDirectory(file); 
	    mainframe.setSaving(true); // create a progress bar
	    // create a task for output
	    FileTask task;
	    if (binned) { 
		task = new FileTask(
		    mainframe, FileTask.TIMESTRING_BINNED_PHASE_OUT, 
		    file.getPath(), outputformat, frequency, 1.0/bins,
		    useData, binsize, binPhase, binAmplitude, binPower);
	    } else {
		task = new FileTask(
		    mainframe, votable, FileTask.TIMESTRING_PHASE_OUT,
		    file.getPath(), outputformat, frequency, zeropoint);
	    }
	    task.go();
	}
    }

    //---
    //--- methods to listen to ComponentEvents 
    //--- (we are listening to ComponentEvents that are caused by resizing 
    //---  the plotframe, this has become necessary to remove a SGT bug)
    //---
    public void componentResized(ComponentEvent e) {
		Range2D xrng = getDomainXRangeD();
		PlotMark.setDeviceXRange((int)xrng.start,(int)xrng.end);
		super.componentResized(e);
    }
    public void componentHidden(ComponentEvent e) {}
    public void componentShown(ComponentEvent e)  {}
    public void componentMoved(ComponentEvent e)  {}
}
