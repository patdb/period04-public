/*----------------------------------------------------------------------*
 *  XDialogCombineSubstrings
 *  a dialog to combine substrings
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.*;                  // property change stuff
import javax.swing.*;
import javax.swing.event.*;

class XDialogCombineSubstrings extends XDialog
    implements ActionListener
{
    private static final long serialVersionUID = -6316927621489428764L;

    private JOptionPane   optionPane;
    private final JButton b_OK     = createButton("Ok");
    private final JButton b_Cancel = createButton("Cancel");

    private JTextField t_label;
    private JComboBox  cb_attribute;

    private XProjectFrame  mainframe;
    private XTabTimeString tab;

    public XDialogCombineSubstrings(XProjectFrame frame, XTabTimeString tabTS, 
				    int attribute) {
	super(frame.getFrame(), true); // necessary to keep dialog in front
	mainframe = frame;
	tab       = tabTS;
	Object[] options = {b_OK, b_Cancel}; //set buttons
 
	JPanel inputpanel = new JPanel();
	inputpanel.setLayout(null);

	JLabel l_attribute = createLabel(
	    "Combine selected substrings in attribute: ");
	String [] s_attributes = new String[4];
	for (int i=0; i<4; i++) {
	    s_attributes[i] = Period04.projectGetNameSet(i);
	}
	cb_attribute = new JComboBox(s_attributes);
	cb_attribute.setSelectedIndex(attribute);
	
	JLabel l_label = createLabel("New Name:");
	t_label = createTextField("JD");

	l_attribute .setBounds( 10,  0, 280,  20);
	cb_attribute.setBounds( 10, 20, 260,  20);
	l_label     .setBounds( 10, 50, 100,  20);
	t_label     .setBounds(110, 50, 160,  20);

	inputpanel.add(l_attribute);
	inputpanel.add(cb_attribute);
	inputpanel.add(l_label);
	inputpanel.add(t_label);

	optionPane = new JOptionPane((Object)inputpanel, 
				     JOptionPane.PLAIN_MESSAGE,
				     JOptionPane.YES_NO_OPTION,
				     null, options, options[0]);
	setContentPane(optionPane);
	setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent we) {
		    /*
		     * Instead of directly closing the window,
		     * we're going to change the JOptionPane's
		     * value property.
		     */
		    optionPane.setValue(b_Cancel);
		}
	    });

	optionPane.addPropertyChangeListener(new PropertyChangeListener() {
		public void propertyChange(PropertyChangeEvent e) {
		    String prop = e.getPropertyName();

		    if (isVisible()
			&& (e.getSource() == optionPane)
			&& (prop.equals(JOptionPane.VALUE_PROPERTY) ||
			    prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
			Object value = optionPane.getValue();
 
			if (value == JOptionPane.UNINITIALIZED_VALUE) {
			    return; //ignore reset
			}
		       
			// reset the JOptionPane's value.
			optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

			if (value.equals(b_OK)) {
			    int attribute = cb_attribute.getSelectedIndex();
			    String s_label = t_label.getText();
			    if (s_label.equals("")) {
				// text was invalid
				t_label.selectAll();
				JOptionPane.showMessageDialog(
				    XDialogCombineSubstrings.this,
				    "Sorry, \""+s_label+"\" "
				    +"isn't a valid name.\n","",
				    JOptionPane.ERROR_MESSAGE);
				return;
			    }
			    // ok, everything is fine
			    Period04.projectCombineSubstrings(attribute, s_label);
			    // update display
			    tab.updateList(attribute);
			    // write protocol
			    mainframe.getLog().writeProtocol(
				"Combined substrings:\n"+
				"Renamed currently selected substrings to "+
				s_label+" in column "+
				Period04.projectGetNameSet(attribute)+"\n", true);
			    setVisible(false);
			} else if (value.equals(b_Cancel)) {
			    setVisible(false);
			}
		    }
		}
	    });
	
	this.setTitle("Combine Substrings:");
	if (Period04.isMacOS)
		this.setSize(320,180);
	else
		this.setSize(320,160);
	this.setResizable(false);
	this.setLocationRelativeTo(mainframe.getFrame());
	this.setVisible(true);
    }

    public void actionPerformed(ActionEvent evt) {
	optionPane.setValue(evt.getSource());
    }

}

