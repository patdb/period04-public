/*-----------------------------------------------------------------------*
 *  XMenuHelper
 *  this class contains methods needed to create the menu-bar
 *-----------------------------------------------------------------------*/

import java.awt.event.*;
import javax.swing.*;

public class XMenuHelper
{
  /**
   * Add an element to the menubar.
   */
  public static JMenu addMenuBarItem( JMenuBar menuBar, String s ) {
      JMenu menu;

      if ( s.indexOf("_") > -1 ) {
	  int pos = s.indexOf("_");
	  char c = s.charAt( pos+1 );
	  StringBuffer sb = new StringBuffer(s).delete(pos,pos+1);
	  menu =  new JMenu( sb.toString() );
	  menu.setMnemonic( c );
      }
      else
	  menu = new JMenu( s );
      
      menuBar.add( menu );
      return menu;
  }

  public static JMenu addMenuBarItem( JMenuBar menuBar, String s, int index ) {
      JMenu menu;

      if ( s.indexOf("_") > -1 ) {
	  int pos = s.indexOf("_");
	  char c = s.charAt( pos+1 );
	  StringBuffer sb = new StringBuffer(s).delete(pos,pos+1);
	  menu =  new JMenu( sb.toString() );
	  menu.setMnemonic( c );
      }
      else
	  menu = new JMenu( s );
      
      menuBar.add( menu, index );
      return menu;
  }

  private static JMenuItem processMnemonic( String s ) {
      if ( s.indexOf("_") > -1 ) {
	  int pos = s.indexOf("_");
	  char c = s.charAt( pos+1 );
	  StringBuffer sb = new StringBuffer(s).delete(pos,pos+1);
	  return new JMenuItem( sb.toString(), c );
      }
      else
	  return new JMenuItem( s );
  }

  /**
   * Insert a JMenuItem to a given JMenu.
   */
  public static JMenuItem addMenuItem(JMenu m, String s, char keyChar,
                                      ActionListener al) {
      if ( s.startsWith("-") ) {
	  m.addSeparator();
	  return null;
      }
      
      JMenuItem menuItem = processMnemonic( s );
      m.add( menuItem  );

      if ( keyChar != 0 )
	  menuItem.setAccelerator(
	      KeyStroke.getKeyStroke(keyChar, InputEvent.CTRL_MASK) );

      if ( al != null )
	  menuItem.addActionListener( al );

      return menuItem;
  }

  public static JMenuItem addMenuItem(JMenu m, String s, char c) {
      return addMenuItem( m, s, c, null );
  }

  public static JMenuItem addMenuItem(JMenu m, String s) {
      return addMenuItem( m, s, (char)0, null );
  }

  public static JMenuItem addMenuItem(JMenu m, String s, ActionListener al) {
      return addMenuItem( m, s, (char)0, al );
  }

    public static JCheckBoxMenuItem addCheckBoxMenuItem(JMenu m, String s, ActionListener al) {
	JCheckBoxMenuItem menuItem = new JCheckBoxMenuItem( s );
	m.add( menuItem );
	menuItem.addActionListener(al);
	return menuItem;
    }
}
