/*----------------------------------------------------------------------*
 *  XDialog
 *  defines some useful methods
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import javax.swing.*;
import javax.print.*;
import javax.print.attribute.*;

public abstract class XDialog extends JDialog 
    implements ActionListener
{
    public XDialog(JFrame mainframe, boolean is) {
	super(mainframe, is);
    }

    protected JLabel createLabel(String name) {
	    JLabel label = new JLabel(name);
	    label.setFont(new Font("Dialog", Font.PLAIN, 11));
	    return label;
    }

    protected JLabel createBoldLabel(String name) {
	    JLabel label = new JLabel(name);
	    label.setFont(new Font("Dialog", Font.BOLD, 11));
	    return label;
    }
 
    protected JCheckBox createCheckBox(String name) {
	JCheckBox check = new JCheckBox(name);
	check.setFont(new Font("Dialog", Font.PLAIN, 10));
	check.addActionListener(this);
	return check;
    } 

    protected JComboBox createComboBox(Object [] entries) {
	JComboBox box = new JComboBox(entries);
	box.setFont(new Font("Dialog", Font.PLAIN, 10));
	box.addActionListener(this);
	return box;
    } 

    protected JTextField createTextField(String entry) {
        JTextField text = new JTextField(entry);
        text.setFont(new Font("Dialog", Font.PLAIN, 11));
        text.addActionListener(this);
        return text;
    }

    protected JTextField createTextFieldNoAction(String entry) {
        JTextField text = new JTextField(entry);
        text.setFont(new Font("Dialog", Font.PLAIN, 11));
        return text;
    }

    protected JRadioButton createRadioButton(String name) {
	JRadioButton check = new JRadioButton(name);
	check.setFont(new Font("Dialog", Font.PLAIN, 11));
	check.addActionListener(this);               //<--- itemListener
	return check;
    }

    protected JButton createButton(String name) {
	JButton button = new JButton(name);
	button.setFont(new Font("Dialog", Font.PLAIN, 11));
	button.addActionListener(this);
	return button;
    } 

    protected JButton createBoldButton(String name) {
	JButton button = new JButton(name);
	button.setFont(new Font("Dialog", Font.BOLD, 11));
	button.addActionListener(this);
	return button;
    } 

    protected String format(double value) {
		NumberFormat fmt = NumberFormat.getInstance(Locale.UK);
		if (fmt instanceof DecimalFormat) {
			((DecimalFormat) fmt).setDecimalSeparatorAlwaysShown(true);
		}
		fmt.setGroupingUsed(false);
		fmt.setMaximumFractionDigits(5);
		return String.valueOf(fmt.format(value));
    }

    public void printResult(String result) {
		/*
		  // OLD VERSION
		  // print a simple ascii text
		  DocFlavor flavor = DocFlavor.STRING.TEXT_PLAIN;
		  Doc doc = new SimpleDoc(result, flavor, null);
		  PrintRequestAttributeSet set = new HashPrintRequestAttributeSet(); 
		  PrintService dps = PrintServiceLookup.lookupDefaultPrintService();
		  
		  DocPrintJob job = dps.createPrintJob();
		  try { job.print(doc, set); }
		  catch (Exception e) {
		  System.out.println("printing failed");
		  }
		*/
		PrintText pt = new PrintText();
		pt.print(result);
	}
	

    protected boolean checkDouble(JTextField tf, boolean checknegative) {
	if (!tf.isEditable()) { return true; }
	String text = tf.getText();
	// check if text contains letters 
	NumberFormatException nan = null;
	try { 
	    Double.parseDouble(text); 
	} catch (NumberFormatException exception) { 
	    nan=exception; 
	}		   
	// check if empty
	if (text.equals("") || (nan!=null)) {
	    // text was invalid
	    tf.selectAll();
	    JOptionPane.showMessageDialog(
		this,"Sorry, \"" + text + "\" "
		+ "isn't a valid number.\n", "Invalid entry",
		JOptionPane.ERROR_MESSAGE);
	    return false;
	} 
	if (checknegative) { // check if negative
	    if (Double.parseDouble(text)<=0.0) {
		tf.selectAll();
		JOptionPane.showMessageDialog(
		    this,"Sorry, \"" + text + "\" "
		    + "isn't a valid number.\n", "Invalid entry",
		    JOptionPane.ERROR_MESSAGE);
		return false;
	    }
	}
	return true;
    }
}
