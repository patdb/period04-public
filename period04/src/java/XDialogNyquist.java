/*----------------------------------------------------------------------*
 *  XDialogNyquist
 *  a dialog to calculate the Nyquist frequency
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.*;                  // Property change stuff
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.*;
import javax.swing.border.*;

class XDialogNyquist extends XDialog
{
    private static final long serialVersionUID = 4036888749727805832L;
    private JOptionPane     optionPane;
    private XProjectFrame      mainframe;
    private final JButton   b_calc, b_close;
    private boolean         itemselected = false;

    private JTextField      t_from, t_to, t_result;

    private final int WIDTH  = 280;      // default frame width
    private final int HEIGHT = 240;      // default frame height
    private int sizeDiff = 0;     // correction factor for height



    public XDialogNyquist(XProjectFrame mainframe) {
	super(mainframe.getFrame(), false);
	this.mainframe = mainframe;

	// create option buttons
	b_calc   = createButton("Calculate");
	b_close  = createButton("Close");
	Object [] options = { b_calc, b_close};
	
	//---
	//--- create the inputpanel
	//---
	JPanel inputpanel = new JPanel();
	inputpanel.setLayout(null);
	
	JLabel l_info = createLabel(
	    "<html>Calculate the Nyquist-frequency<br>"+
	    "using points within the timerange:</html>");
	JLabel l_from = createLabel("from:");
	JLabel l_to   = createLabel("to:");
	t_from = createTextField(Period04.projectGetStartTime());
	t_to   = createTextField(Period04.projectGetEndTime());
	t_from.addActionListener(this);
	t_to  .addActionListener(this);
	JLabel l_result = createLabel("Resulting Nyquist-frequency:");
	t_result = createTextField("");
//	t_result.setEditable(false);

	l_info  .setBounds( 10,  0, 230, 30);
	l_from  .setBounds( 10, 40,  60, 20);
	t_from  .setBounds( 70, 40, 170, 20);
	l_to    .setBounds( 10, 60,  60, 20);
	t_to    .setBounds( 70, 60, 170, 20);
	l_result.setBounds( 10, 90, 230, 20);
	t_result.setBounds( 10,110, 230, 30);

	inputpanel.add(l_info, null);
	inputpanel.add(l_from, null);
	inputpanel.add(t_from, null);
	inputpanel.add(l_to  , null);
	inputpanel.add(t_to  , null);
	inputpanel.add(l_result, null);
	inputpanel.add(t_result, null);

	Object array = inputpanel;

        optionPane = new JOptionPane(
	    array, JOptionPane.PLAIN_MESSAGE,
	    JOptionPane.YES_NO_OPTION,
	    null, options, options[0]);
        setContentPane(optionPane);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent we) {
		    /*
		     * Instead of directly closing the window,
		     * we're going to change the JOptionPane's
		     * value property.
		     */
                    optionPane.setValue(b_close);
			//	new Integer(JOptionPane.CLOSED_OPTION));
            }
        });

	optionPane.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
                String prop = e.getPropertyName();

                if (isVisible() 
                 && (e.getSource() == optionPane)
                 && (prop.equals(JOptionPane.VALUE_PROPERTY) ||
                     prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
                    Object value = optionPane.getValue();

                    if (value == JOptionPane.UNINITIALIZED_VALUE) {
                        //ignore reset
                        return;
                    }

                    // Reset the JOptionPane's value.
                    // If you don't do this, then if the user
                    // presses the same button next time, no
                    // property change event will be fired.
                    optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

                    if (value.equals(b_calc) || value.equals(t_from) || 
			value.equals(t_to) ) {
			double from, to;
			if (!checkDouble(t_from, false)) { return; }
			from = Double.parseDouble(t_from.getText());
			if (!checkDouble(t_to, false)) { return; }
			to = Double.parseDouble(t_to.getText());
			t_result.setText(
			    Double.toString(
				Period04.projectGetSpecialNyquist(from,to)));
			writeResultsToProtocol();
		    } 
		    else if (value.equals(b_close)) {
			setVisible(false);
		    }
		}
	    }
	    });
   
	this.setTitle("Calculate Nyquist Frequency:");
	this.setSize(WIDTH,HEIGHT);
	this.setLocationRelativeTo(mainframe.getFrame());
	this.setVisible(true);
    }

    public boolean checkDouble(JTextField tf, boolean checknegative) {
	String text = tf.getText();
	// check if text contains letters 
	NumberFormatException nan = null;
	try { 
	    Double.parseDouble(text); 
	} catch (NumberFormatException exception) { 
	    nan=exception; 
	}		   
	// check if empty
	if (text.equals("") || (nan!=null)) {
	    // text was invalid
	    tf.selectAll();
	    JOptionPane.showMessageDialog(
		XDialogNyquist.this,"Sorry, \"" + text + "\" "
		+ "isn't a valid number.\n", "Invalid entry",
		JOptionPane.ERROR_MESSAGE);
	    return false;
	} 
	if (checknegative) { // check if negative
	    if (Double.parseDouble(text)<=0.0) {
		tf.selectAll();
		JOptionPane.showMessageDialog(
		    XDialogNyquist.this,"Sorry, \"" + text + "\" "
		    + "isn't a valid number.\n", "Invalid entry",
		    JOptionPane.ERROR_MESSAGE);
		return false;
	    }
	}
	return true;
    }

    public void actionPerformed(ActionEvent evt) {
	Object s = evt.getSource();
	optionPane.setValue(s);
    }

    public void writeResultsToProtocol() {
	// write results to protocol
	StringBuffer text = new StringBuffer();
	text.append("Calculated Nyquist frequency"+
		    "\nStart time:                "+t_from.getText()+
		    "\nEnd time:                  "+t_to.getText()+
		    "\nResulting Nyquist frequency: "+t_result.getText()+"\n");
       	mainframe.getLog().writeProtocol(text.toString(), true);
    }

}
