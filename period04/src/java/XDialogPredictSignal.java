/*----------------------------------------------------------------------*
 *  XDialogPredictSignal
 *  a dialog to predict the amplitude at a certain time
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.*;                  // Property change stuff
import javax.swing.*;
import javax.swing.event.*;

class XDialogPredictSignal extends XDialog 
{
    private static final long serialVersionUID = -7054674580666207472L;
    private JOptionPane   optionPane;
    private final JButton b_OK     = createButton("Calculate");
    private final JButton b_Cancel = createButton("Close");
    private JTextField    t_time, t_signal;

    private XProjectFrame mainframe;
    private XTabFit    tab;

    public XDialogPredictSignal(XProjectFrame frame, XTabFit tabFit) {
	super(frame.getFrame(), false);
	mainframe = frame;
	tab = tabFit;
	Object[] options = {b_OK, b_Cancel}; //set buttons 
	
	JPanel inputpanel = new JPanel();
	inputpanel.setLayout(null);

	String starttime = Period04.projectGetStartTime();
	
	JLabel l_starttime = createLabel("Start time of the time string:");
	JLabel l_showtime  = createLabel(starttime);
	JLabel l_time      = createLabel("Predict signal at time:");
	t_time = createTextField(starttime);
	JLabel l_signal    = createLabel("The signal is:");
	t_signal = createTextField("not yet calculated");
	t_signal.setEditable(false);

	l_starttime.setBounds( 10,  0, 190,  20);
	l_showtime .setBounds(200,  0, 110,  20);
	l_time     .setBounds( 10, 25, 150,  20);
	t_time     .setBounds(170, 25, 140,  20);
	l_signal   .setBounds( 10, 50, 150,  20);
	t_signal   .setBounds(170, 50, 140,  20);

	inputpanel.add(l_starttime);
	inputpanel.add(l_showtime);
	inputpanel.add(l_time);
	inputpanel.add(t_time);
	inputpanel.add(l_signal);
	inputpanel.add(t_signal);

	optionPane = new JOptionPane((Object)inputpanel, 
				     JOptionPane.PLAIN_MESSAGE,
				     JOptionPane.YES_NO_OPTION,
				     null, options, options[0]);
	setContentPane(optionPane);
	setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent we) {
		    /*
		     * Instead of directly closing the window,
		     * we're going to change the JOptionPane's
		     * value property.
		     */
		    optionPane.setValue(
			new Integer(JOptionPane.CLOSED_OPTION));
		}
	    });
	
	optionPane.addPropertyChangeListener(new PropertyChangeListener() {
		public void propertyChange(PropertyChangeEvent e) {
		    String prop = e.getPropertyName();
		    
		    if (isVisible() 
			&& (e.getSource() == optionPane)
			&& (prop.equals(JOptionPane.VALUE_PROPERTY) ||
			    prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
			Object value = optionPane.getValue();
			
			if (value == JOptionPane.UNINITIALIZED_VALUE) {
			    return; //ignore reset
			}
			
			// Reset the JOptionPane's value.
			// If you don't do this, then if the user
			// presses the same button next time, no
			// property change event will be fired.
			optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

			if (value.equals(b_OK) || value.equals(t_time)) { 
			    String s_time = t_time.getText();

			    // is it a valid number? 
			    NumberFormatException nan = null;
			    try { Double.parseDouble(s_time); } 
			    catch (NumberFormatException exception) { 
				nan=exception; 
			    }		   
			    if (s_time.equals("") || (nan!=null)) {
				// text was invalid
				t_time.selectAll();
				JOptionPane.showMessageDialog(
				    XDialogPredictSignal.this,
				    "Sorry, \"" +s_time+ "\" "
				    + "isn't a valid time.\n",
				    "Try again",
				    JOptionPane.ERROR_MESSAGE);
				return;
			    } 
			    
			    // read in current frequencies
			    tab.setFrequencyListData();
			    if (tab.getFitMode()==XTabFit.NORMAL_MODE) {
				if (Period04.projectGetActiveFrequencies()==0) {
				    JOptionPane.showMessageDialog(
					XDialogPredictSignal.this, createLabel(
					    "<html>No frequencies selected!<br>"+
					    "<br>Please select the frequencies"+
					    "<br>you want to use for this"+
					    " prediction."),
					"No active frequencies",
				    JOptionPane.ERROR_MESSAGE);
				    return;
				}
			    } else if (tab.getFitMode()==XTabFit.BINARY_MODE) {
				if (Period04.projectGetBMActiveFrequencies()==0) {
				    JOptionPane.showMessageDialog(
					XDialogPredictSignal.this, createLabel(
					    "<html>No frequencies selected!<br>"+
					    "<br>Please select the frequencies"+
					    "<br>you want to use for this"+
					    " prediction."),
					"No active frequencies",
				    JOptionPane.ERROR_MESSAGE);
				    return;
				}
			    }

			    String s_amplitude = Period04.projectPredictAmplitude(
				Double.parseDouble(s_time));
			    t_signal.setText(s_amplitude);

			    mainframe.getLog().writeProtocol(
				"Predict Amplitude:\n"+
 				"Predicted signal for time: "+s_time+
				"\ngives amplitude:          "+s_amplitude+
				"\n", true);
			} else {
			    setVisible(false);
			}

		    }
		}
	    });
	
	this.setTitle("Predict signal at a given time:");
	this.setSize(350,170);
	this.setResizable(false);
	this.setLocationRelativeTo(mainframe.getFrame());
	this.setVisible(true);
    }

    public void actionPerformed(ActionEvent evt) {
	optionPane.setValue(evt.getSource());
    }

}

