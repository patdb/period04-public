/*----------------------------------------------------------------------*
 *  XDialogMonteCarlo
 *  a dialog 
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.*;                  // property change stuff
import javax.swing.*;

class XDialogMonteCarlo extends XDialog
    implements ActionListener
{
    private static final long serialVersionUID = -8407483508887332955L;
    private JOptionPane   optionPane;
    private final JButton b_OK     = createButton("Ok");
    private final JButton b_Cancel = createButton("Cancel");
    private boolean cancel = false;

    private JTextField t_processes;
    private JCheckBox  c_shiftTimes, c_randInit;
    private String typed = null;

    private XProjectFrame mainframe;
    private XTabFit       tabFit;
 
    public XDialogMonteCarlo(XProjectFrame mainframe) {
	super(mainframe.getFrame(), true); // necessary to keep dialog in front
	this.mainframe = mainframe;
	this.tabFit    = mainframe.getFitTab();
	Object[] options = {b_OK, b_Cancel}; //set option buttons
 
	JPanel inputpanel = new JPanel();
	inputpanel.setLayout(null);

	JLabel l_info      = createLabel(
	    "<html>For this calculation the program will use "+
	    "the same settings as for your last calculation.<br>"+
	    "For every process a time string of simulated data "+
	    "is created that has the same noise level (residuals) "+
	    "as the original time string.</html>");
	JLabel l_processes = createLabel("Number of processes: ");
	t_processes = createTextField("100");
	c_shiftTimes = createCheckBox(
	    "Uncouple Freq. and Phase Uncertainties");
	c_randInit = createCheckBox(
	    "Use system time to initialize random generator");
	c_shiftTimes.setSelected(true);
	c_randInit  .setSelected(true);
	
	l_info      .setBounds( 10,  0, 260,  80);
	l_processes .setBounds( 10, 90, 150,  20);
	t_processes .setBounds(160, 90, 100,  20);

	inputpanel.add(l_info);
	inputpanel.add(l_processes);
	inputpanel.add(t_processes);
	if (tabFit.getFitMode()!=XTabFit.BINARY_MODE &&
	    tabFit.getSelectionMode()!=0) {
	    c_shiftTimes.setBounds( 10,120, 250,  20);
	    c_randInit  .setBounds( 10,140, 280,  20);
	    inputpanel.add(c_shiftTimes);
	} else {
	    c_randInit  .setBounds( 10,120, 280,  20);
	}
	inputpanel.add(c_randInit);

	optionPane = new JOptionPane((Object)inputpanel, 
				     JOptionPane.PLAIN_MESSAGE,
				     JOptionPane.YES_NO_OPTION,
				     null, options, options[0]);
	setContentPane(optionPane);
	setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent we) {
		    /*
		     * Instead of directly closing the window,
		     * we're going to change the JOptionPane's
		     * value property.
		     */
		    optionPane.setValue(b_Cancel);
		}
	    });

	optionPane.addPropertyChangeListener(new PropertyChangeListener() {
		public void propertyChange(PropertyChangeEvent e) {
		    String prop = e.getPropertyName();

		    if (isVisible()
			&& (e.getSource() == optionPane)
			&& (prop.equals(JOptionPane.VALUE_PROPERTY) ||
			    prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
			Object value = optionPane.getValue();
 
			if (value == JOptionPane.UNINITIALIZED_VALUE) {
			    return; //ignore reset
			}
		       
			// reset the JOptionPane's value.
			optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

			if (value.equals(b_OK) || value.equals(t_processes)) {
			    //--- check wether it is a valid number
			    String text = t_processes.getText();
			    NumberFormatException nan = null;
			    try { Integer.parseInt(text); } 
			    catch (NumberFormatException exception) { 
				nan=exception; 
			    }
			    if ( text.equals("") || (nan!=null) ) {
				// text was no number
				JOptionPane.showMessageDialog(
				    XDialogMonteCarlo.this, "Sorry, \""+ 
				    text+"\" "+"isn't a valid number.\n", 
				    "Error", JOptionPane.ERROR_MESSAGE);
				return;
			    } 	
			    //--- ok, it's a number - but is it non-negativ?
			    if  (Integer.parseInt(text)<=0) {
				JOptionPane.showMessageDialog(
				    XDialogMonteCarlo.this, "Sorry, \""+ 
				    text+"\" "+"isn't a valid number.\n"+
				    "Only non-negative numbers allowed!", 
				    "Error", JOptionPane.ERROR_MESSAGE);
				return;
			    }
			    if  (Integer.parseInt(text)<100) {
				JOptionPane.showMessageDialog(
				    XDialogMonteCarlo.this, 
				    "You decided to calculate "+text+
				    " processes.\n"+
				    "Please note that for a reliable\n"+
				    "estimation of uncertainties a high\n"+
				    "number of processes is neccessary.", 
				    "Please note:", 
				    JOptionPane.INFORMATION_MESSAGE);
			    }
			    //--- everything ok, dispose
			    typed = text;
			    cancel = false;
			    setVisible(false);
			} else { // user closed dialog or clicked cancel
			    typed = null;
			    cancel = true;
			    setVisible(false);
			}
		    }
		}
	    });
	
	this.setTitle("Monte Carlo Simulation");
	if (tabFit.getFitMode()==XTabFit.BINARY_MODE &&
	    tabFit.getSelectionMode()!=0)
	    this.setSize(320,240);
	else 
	    this.setSize(320,260);
	this.setResizable(false);
	this.setLocationRelativeTo(mainframe.getFrame());
	this.setVisible(true);
    }

    public int getValidatedNumber() {
	if (typed==null) { return 0; }
	return (new Integer(typed)).intValue();
    }

    public boolean isShiftTimes() {
	if (tabFit.getFitMode()==XTabFit.BINARY_MODE) { return false; }
	return c_shiftTimes.isSelected();
    }

    public boolean isInitRand() {
	return c_randInit.isSelected();
    }

    public boolean isCanceled() {
	return cancel;
    }

    public void actionPerformed(ActionEvent evt) {
	Object s = evt.getSource();
	if (s==c_shiftTimes) { return; }
	if (s==c_randInit)   { return; }
	optionPane.setValue(s);
    }

}

