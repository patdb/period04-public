/*----------------------------------------------------------------------*
 *  XDialogShowTimes
 *  a dialog to subdivide the timestring
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.*;                  // Property change stuff
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.event.*;
import javax.swing.text.BadLocationException;

class XDialogFrequencyConversion extends XDialog 
{
	//    private static final long serialVersionUID;
    private JOptionPane  optionPane;
    private final String s_OK="Close", s_Export="Export", s_Print="Print";
    
    private JTextArea   textarea;
    private JScrollPane textScrollPane;   

    private final int WIDTH  = 400;      // default frame width
    private final int HEIGHT = 400;      // default frame height

	private final int CD_MHZ = 0;
	private final int MHZ_CD = 1;

    
    public XDialogFrequencyConversion(JFrame frame, int conversionMode) {
		super(frame, true);
		Object[] options = {s_Export, s_Print, s_OK}; // set button names
	    
		JPanel inputpanel = new JPanel();
		
		textarea = new JTextArea();
        textarea.setFont(new Font("Monospaced", Font.PLAIN, 11));
		textScrollPane = new JScrollPane(
			textarea, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
			JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		textScrollPane.setPreferredSize(new Dimension(350,300));
		textScrollPane.setBorder(
			BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
		setText(conversionMode);
		inputpanel.add(textScrollPane);
		
		//---
		//--- create the optionpane and add the inputpanel and the buttons
		//---
		optionPane = new JOptionPane((Object)inputpanel, 
									 JOptionPane.PLAIN_MESSAGE,
									 JOptionPane.YES_NO_OPTION,
									 null, options, options[2]);
		setContentPane(optionPane);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent we) {
					/*
					 * Instead of directly closing the window,
					 * we're going to change the JOptionPane's
					 * value property.
					 */
					optionPane.setValue(
						new Integer(JOptionPane.CLOSED_OPTION));
				}
			});
		
		optionPane.addPropertyChangeListener(new PropertyChangeListener() {
				public void propertyChange(PropertyChangeEvent e) {
					String prop = e.getPropertyName();
					
					if (isVisible() 
						&& (e.getSource() == optionPane)
						&& (prop.equals(JOptionPane.VALUE_PROPERTY) ||
							prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
						Object value = optionPane.getValue();
						
						// reset 
						optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

						if (value == JOptionPane.UNINITIALIZED_VALUE) {
							//ignore reset
							return;
						}
						
						if (value=="Print")
							print();
						else if (value=="Export")
							export();
						else 
							setVisible(false);
						
					}
				}
			});
	    
		this.setTitle("Unit conversion:");
		this.setSize(WIDTH,HEIGHT);
		this.setLocationRelativeTo(frame);
		this.setVisible(true);
    }

    public void actionPerformed(ActionEvent evt) {
		optionPane.setValue(s_OK);
    }
 

	private void setText(int mode) {
		StringBuffer buffer = new StringBuffer();
		double conversionFactor = 1.0;
		switch (mode) {
		case CD_MHZ:
			buffer.append("#\tFreq [c/d]\tFreq [microHz]\n"+
						  "-------------------------------------------\n");
			conversionFactor = 1000000.0/(24.0*60.0*60.0);
			break;
		case MHZ_CD:
			buffer.append("#\tFreq [microHz]\tFreq [c/d]\n"+
						  "-------------------------------------------\n");
			conversionFactor = (24.0*60.0*60.0)/1000000.0;
			break;
		}
		for (int i=0; i<Period04.projectGetTotalFrequencies(); i++) {
			if (Period04.projectGetFitMode()==0) {
				if (Period04.projectGetActive(i)) {
					buffer.append(
						Period04.projectGetNumber(i)+"\t"+
						Period04.projectGetFrequency(i)+"\t"+
						format(Period04.projectGetFrequencyValue(i)*
							   conversionFactor)+"\n");
				}
			} else { 
				if (Period04.projectGetBMActive(i)) {
					buffer.append(
						Period04.projectGetBMNumber(i)+"\t"+
						Period04.projectGetBMFrequency(i)+"\t"+
						format(Period04.projectGetFrequencyValue(i)*
							   conversionFactor)+"\n");
				}
			}
		}
		textarea.setText(buffer.toString());
	}

    public void export() {
	// create FileChooser
	JFileChooser fc = new JFileChooser(Period04.getCurrentDirectory());
	int returnVal = fc.showSaveDialog(this);
	if (returnVal == JFileChooser.APPROVE_OPTION) {
	    File file = fc.getSelectedFile();
	    // check wether user wants to overwrite an existing file
	    if (file.exists()) {
		int v = JOptionPane.showConfirmDialog(
		    this, "File does already exist!\n"+
		    "Do you really want to delete the old file?", 
			"Warning", JOptionPane.YES_NO_OPTION);
		if (v == JOptionPane.NO_OPTION) {
		    return;
		} 
	    }
	    // now go and save it
	    Period04.setCurrentDirectory(file);
	    try {
		FileWriter fw = new FileWriter(file);
		fw.write(textarea.getText());
		if ( fw!=null ) fw.close();
	    } catch (IOException e) {
		System.err.println(e);
	    }
	}
    }

    public void print() {
		printResult(textarea.getText());
    }



}

