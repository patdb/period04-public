/*----------------------------------------------------------------------*
 *  XDialogFourierNoise
 *  a dialog to calculate a noise fourier spectrum or 
 *  the noise at a certain frequency
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.*;                  // Property change stuff
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;

class XDialogFourierNoise extends XDialog
    implements ItemListener, ComponentListener
{
    private static final long serialVersionUID = 8835286887092027447L;
    private JOptionPane     optionPane;
    private XProjectFrame   mainframe;
    private JPanel          inputpanel;
    private final JButton   b_calc, b_close, b_print, b_export;
    private static boolean  itemselected = false;
    private static boolean  blockDocumentEvents = false;
    private static boolean  blockActionEvents   = false;

    private final int WIDTH  = 390;      // default frame width
    private final int HEIGHT = 460;      // default frame height
    private static int sizeDiff = 0;     // correction factor for height
	private boolean isFindPeak = true;
	private double findPeakBinScale=-1;

    private JTextField t_from, t_to, t_spacing, t_boxsize, t_StepRate, t_freq;
    private JTextArea list;
    private JRadioButton [] cbDataMode = new JRadioButton[6];
    private String []  stepRateEntry= {"High", "Medium", "Low", "Custom"};
    private JComboBox  cbStepQuality, cbFrequency;   
    private int []  freqindices;
    private JScrollPane listPane;
    private JPanel  resultpanel;

    // noise settings
    private double from, to, spacing, boxsize, step, freq;
    private int stepquality, datamode, weight;
    private final boolean spectrum;

    // calculation related
    private FourierNoiseTask  task;
    private int interval; 

    public XDialogFourierNoise(XProjectFrame mFrame, boolean mSpectrum) {
	super(mFrame.getFrame(), false);           // call constructor
	this.mainframe = mFrame;        // assign variables
	this.spectrum  = mSpectrum;
	//--- for clarity:
	//--- spectrum = true -> calculate noise spectrum
	//--- spectrum = false -> calculate noise at frequency

	// create option buttons
	b_calc   = createButton("Calculate");
	b_print  = createButton("Print");
	b_export = createButton("Export");
	b_close  = createButton("Close");
	Object [] options = { b_calc, b_print, b_export, b_close};
	
	//---
	//--- create the inputpanel
	//---
	inputpanel = new JPanel();
	inputpanel.setLayout(null);
	
	JLabel s_range=null, s_from=null, s_to=null; 
	JLabel s_spacing=null, s_freq=null;
	if (spectrum) {
	    s_range= createBoldLabel("Frequency range");
	    s_from = createBoldLabel("from:");
	    s_to   = createBoldLabel("to:");
	    t_from = createTextFieldNoAction(Period04.fourierGetFrom());
	    t_to   = createTextFieldNoAction(Period04.fourierGetTo());
	    s_spacing = createBoldLabel("Spacing:");
	    t_spacing = createTextFieldNoAction("10");
	} else {
	    s_freq = createBoldLabel("Calculate noise at Frequency:");
	    t_freq = createTextFieldNoAction("10");
	    // add a document listener in order to get notified if
	    // the user changes the frequency in the text field
	    t_freq.getDocument().addDocumentListener(new DocumentListener() {
		    public void changedUpdate(DocumentEvent evt) {
				/*if (!checkDouble(t_freq, true))
					return;
				double fr = Double.parseDouble(t_freq.getText());
				if (!(fr<Double.parseDouble(Period04.fourierGetFrom()) || 
					fr>Double.parseDouble(Period04.fourierGetTo()))) {
					freq = fr;
					}*/
			}
		    public void insertUpdate(DocumentEvent evt) {
			if (blockDocumentEvents || cbFrequency==null) { 
			    return; 
			}
			blockActionEvents=true;
			cbFrequency.setSelectedItem("Other Frequency Peak");
			blockActionEvents=false;
		    }
		    public void removeUpdate(DocumentEvent evt) {
			if (blockDocumentEvents || cbFrequency==null) { 
			    return; 
			}
			blockActionEvents=true;
			cbFrequency.setSelectedItem("Other Frequency Peak");
			blockActionEvents=false;
		    }
		});
	    // now create the JComboBox with the frequencies
	    updateFrequencies();
	}
	
	//--- 
	//--- create noise calculation settings panel
	//--- 
	JPanel calcpanel = new JPanel();
	calcpanel.setOpaque(false);
	calcpanel.setBorder(
	    BorderFactory.createCompoundBorder(
		BorderFactory.createTitledBorder(
		    BorderFactory.createEtchedBorder(), 
		    " Noise calculation settings: ",
		    TitledBorder.LEADING, TitledBorder.TOP, 
		    new Font("Dialog", Font.BOLD, 10), new Color(20,30,150) ),
		BorderFactory.createEmptyBorder(5,5,5,5)));

	//--- 
	//--- create setting panel components
	//--- 
	JLabel s_calcmode = createBoldLabel("Calculations based on:");
	JLabel s_boxsize  = createBoldLabel("Box size:");
	t_boxsize = createTextFieldNoAction("2");

	//--- create steprate selection box
	JLabel s_StepRate = createBoldLabel("Step rate: ");
	cbStepQuality = createComboBox(stepRateEntry);
	cbStepQuality.setFont(new Font("Dialog", Font.PLAIN, 10));
	cbStepQuality.addItemListener(this);
	cbStepQuality.setSelectedIndex(0); // StepRate=HIGH
	t_StepRate = createTextFieldNoAction(
	    Period04.projectGetStepRateString("High", 0));
	t_StepRate.setEditable(false);
	
	//--- create radiobuttons
	cbDataMode[0] = createRadioButton("Original data");
	cbDataMode[1] = createRadioButton("Adjusted data");
	cbDataMode[2] = createRadioButton("Spectral window");
	cbDataMode[3] = createRadioButton("Freq folded w SW");
	cbDataMode[4] = createRadioButton("Residuals at original");
	cbDataMode[5] = createRadioButton("Residuals at adjusted");
	cbDataMode[0].setSelected(true);
	ButtonGroup bg1 = new ButtonGroup();
	for (int i=0; i<6; i++) {
	    bg1.add(cbDataMode[i]);
	}

	//--- 
	//--- create result calculation settings panel
	//--- 
	
	resultpanel = new JPanel();
	resultpanel.setOpaque(false);
	resultpanel.setBorder(
	    BorderFactory.createCompoundBorder(
		BorderFactory.createTitledBorder(
		    BorderFactory.createEtchedBorder(), 
		    " Noise calculation results: ",
		    TitledBorder.LEADING, TitledBorder.TOP, 
		    new Font("Dialog", Font.BOLD, 10), 
		    new Color(20,30,150) ),
		BorderFactory.createEmptyBorder(5,5,5,5)));

	JLabel l_freq =  createBoldLabel("Frequency");
	JLabel l_noise = createBoldLabel("Noise");
	list = new JTextArea();
        // list.setEditable(false);
	list.setFont(new Font("Monospaced", Font.PLAIN, 12));
	listPane = new JScrollPane(
	    list, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, 
	    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

	if (spectrum) {
	    // first line
	    s_range  .setBounds( 10,  5, 100, 20);
	    s_from   .setBounds(150,  5,  40, 20);
	    t_from   .setBounds(190,  5, 160, 20);
	    // second line
	    s_to     .setBounds(150, 25,  40, 20);
	    t_to     .setBounds(190, 25, 160, 20);
	    // third line
	    s_spacing.setBounds( 15, 70,  60, 20);
	    t_spacing.setBounds( 75, 70, 100, 20);
	    s_boxsize.setBounds(190, 70,  60, 20);
	    t_boxsize.setBounds(250, 70, 100, 20);
	} else {
	    // first line
	    s_freq     .setBounds( 10,  5, 190, 20);
	    t_freq     .setBounds(200,  5, 150, 20);
	    // second line
	    if (cbFrequency!=null) {
	    	cbFrequency.setBounds(200, 25, 150, 20);
	    }
	    // third line
	    s_boxsize  .setBounds( 15, 70, 190, 20);
	    t_boxsize  .setBounds( 75, 70, 100, 20);
	}
	// fourth line
	s_StepRate   .setBounds( 15, 90, 70, 20);
	cbStepQuality.setBounds( 75, 90, 100, 20);
	t_StepRate   .setBounds(175, 90, 130, 20);
	// fifth "line"
	s_calcmode   .setBounds( 15, 115, 150, 20 );
	cbDataMode[0].setBounds( 40, 135, 120, 17 );
	cbDataMode[4].setBounds(190, 135, 150, 17 );
	cbDataMode[1].setBounds( 40, 152, 120, 17 );
	cbDataMode[5].setBounds(190, 152, 150, 17 );
	cbDataMode[2].setBounds( 40, 169, 140, 17 );
	cbDataMode[3].setBounds(190, 169, 140, 17 );
	calcpanel    .setBounds(  0,  50, 360,150 );
	//---
	//--- lay out noise calculation panel
	//---
	resultpanel.setBounds(  0, 210, 360, 160 );
	l_freq     .setBounds( 10, 225, 100, 20);
	l_noise    .setBounds(140, 225, 100, 20);
	listPane   .setBounds( 10, 245, 340, 110);

	//---
	//--- add everything to the inputpanel
	//---
	if (spectrum) {
	    inputpanel.add(s_range, null);
	    inputpanel.add(s_from, null);
	    inputpanel.add(s_to, null);
	    inputpanel.add(t_from, null);
	    inputpanel.add(t_to, null);
	    inputpanel.add(s_spacing, null);
	    inputpanel.add(t_spacing, null);
	} else {
	    inputpanel.add(s_freq, null);
	    if (cbFrequency!=null) {
	    	inputpanel.add(cbFrequency, null);
	    }
	    inputpanel.add(t_freq, null);
	}
	inputpanel.add(s_boxsize, null);
	inputpanel.add(t_boxsize, null);
	inputpanel.add(s_StepRate, null);
	inputpanel.add(cbStepQuality, null);
	inputpanel.add(t_StepRate, null);
	inputpanel.add(s_calcmode, null);
	inputpanel.add(calcpanel, null);
	for (int i=0; i<6; i++) { 
	    inputpanel.add(cbDataMode[i], null);
	}
	inputpanel.add(l_freq);
	inputpanel.add(l_noise);
	inputpanel.add(listPane);
	inputpanel.add(resultpanel);

	Object array = inputpanel;

        optionPane = new JOptionPane(
	    array, JOptionPane.PLAIN_MESSAGE,
	    JOptionPane.YES_NO_OPTION,
	    null, options, options[0]);
        setContentPane(optionPane);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent we) {
		    /*
		     * Instead of directly closing the window,
		     * we're going to change the JOptionPane's
		     * value property.
		     */
                    optionPane.setValue(
			new Integer(JOptionPane.CLOSED_OPTION));
            }
        });
	addComponentListener(this);	// listen for frame size changes

	optionPane.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
                String prop = e.getPropertyName();

                if (isVisible() 
                 && (e.getSource() == optionPane)
                 && (prop.equals(JOptionPane.VALUE_PROPERTY) ||
                     prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
                    Object value = optionPane.getValue();

                    if (value == JOptionPane.UNINITIALIZED_VALUE) {
                        //ignore reset
                        return;
                    }

                    // Reset the JOptionPane's value.
                    // If you don't do this, then if the user
                    // presses the same button next time, no
                    // property change event will be fired.
                    optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

                    if (value.equals(b_calc)) {
			// if a task is already running, then return
			if (mainframe.isWorking()) { return; }
			if (spectrum) {
			    if (!checkDouble(t_from, false)) { return; }
			    from = Double.parseDouble(t_from.getText());
			    if (!checkDouble(t_to, false)) { return; }
			    to = Double.parseDouble(t_to.getText());
			    if (!checkDouble(t_spacing, true)) { return; }
			    spacing = Double.parseDouble(t_spacing.getText());
			} else {
			    if (!checkDouble(t_freq, false)) { return; }
			    freq = Double.parseDouble(t_freq.getText());
			}
			if (!checkDouble(t_boxsize, true)) { return; }
			boxsize = Double.parseDouble(t_boxsize.getText());
			if (cbStepQuality.getSelectedItem()=="Custom") {
			    if (checkDouble(t_StepRate, true)) {
				step = Double.parseDouble(t_StepRate.getText());
			    }
			}
			stepquality = cbStepQuality.getSelectedIndex();
			for (int i=0; i<6; i++) {
			    if (cbDataMode[i].isSelected()) { 
				datamode = i;
			    }
			}
			calculateNoise();
		    } 
		    else if (value.equals(b_export)) {
			// if a task is already running, then return
			if (mainframe.isWorking()) { return; }
			saveNoise(); 
		    }
		    else if (value.equals(b_print)) {
			// if a task is already running, then return
			if (mainframe.isWorking()) { return; }
			printNoise();
		    }
		    else {
			setVisible(false);
			if (mainframe.isWorking()) {
			    mainframe.stopCalculation();
			}
		    }
		}
	    }
	    });
   
	this.setTitle("Calculate noise:");
	this.setSize(WIDTH,HEIGHT);
	Point pnt = mainframe.getFrame().getLocation();
	this.setLocation((int)(pnt.getX()+mainframe.getFrame().getWidth()/2-WIDTH/2),
			 (int)(pnt.getY()+140));
	this.setVisible(true);
    }


    public void actionPerformed(ActionEvent evt) {
	if (blockActionEvents) { return; }
	Object s = evt.getSource();
	if (s==cbFrequency) {
	    blockDocumentEvents=true;
	    if (cbFrequency.getSelectedItem()=="Other Frequency Peak") {
		t_freq.setText("");
	    } else {
		int i= freqindices[cbFrequency.getSelectedIndex()];
		if (mainframe.getFitTab().getFitMode()==0) { // NORMAL_MODE
		    t_freq.setText(Period04.projectGetFrequency(i));
		} else { // BINARY_MODE
		    t_freq.setText(Period04.projectGetBMFrequency(i));
		}
	    }
	    blockDocumentEvents=false;
	    return;
	}
	
	if (s==cbStepQuality)
	    return;
	for (int i=0; i<6; i++) {
	    if (s==cbDataMode[i])
		return;
	}
	optionPane.setValue(s);
    }

    public void itemStateChanged(ItemEvent evt) {
	if (itemselected==true) { // a real event
	    Object s=evt.getSource();
	    if (s==cbStepQuality) {
		String item = (String)cbStepQuality.getSelectedItem();
		double step = 0.0;
		String stepquality = item;
	        updateStepping(stepquality, step);
	    }
	    itemselected = false;
 	} else { // just a secondary event - ignore
	    itemselected = true;
	}
    }

	public void setFindPeak(boolean value) {
		isFindPeak = value;
	}

	public void setOtherFrequencyPeak(double fr, double binscale) {
		t_freq.setText(Double.toString(fr));
		findPeakBinScale = binscale;
	}

    public void updateStepping(String stepquality, double step) {
	String steprate = Period04.projectGetStepRateString(stepquality, step);
	t_StepRate.setEditable(false); // set textbox not editable
	// select the right one
	if (stepquality=="Low") {
	    cbStepQuality.setSelectedIndex(2); 
	} else if (stepquality=="Medium") { 
	    cbStepQuality.setSelectedIndex(1);
	} else if (stepquality=="Custom") {
	    cbStepQuality.setSelectedIndex(3);
	    t_StepRate.setEditable(true); 
	} else { // stepquality=="High"
	    cbStepQuality.setSelectedIndex(0);
	}
	// fill in the value
	t_StepRate.setText(steprate);
    }

    public boolean checkDouble(JTextField tf, boolean checknegative) {
	String text = tf.getText();
	// check if text contains letters 
	NumberFormatException nan = null;
	try { 
	    Double.parseDouble(text); 
	} catch (NumberFormatException exception) { 
	    nan=exception; 
	}		   
	// check if empty
	if (text.equals("") || (nan!=null)) {
	    // text was invalid
	    tf.selectAll();
	    JOptionPane.showMessageDialog(
		XDialogFourierNoise.this,"Sorry, \"" + text + "\" "
		+ "isn't a valid number.\n", "Invalid entry",
		JOptionPane.ERROR_MESSAGE);
	    return false;
	} 
	if (checknegative) { // check if negative
	    if (Double.parseDouble(text)<=0.0) {
		tf.selectAll();
		JOptionPane.showMessageDialog(
		    XDialogFourierNoise.this,"Sorry, \"" + text + "\" "
		    + "isn't a valid number.\n", "Invalid entry",
		    JOptionPane.ERROR_MESSAGE);
		return false;
	    }
	}
	return true;
    }

    public boolean checkFourierSettings() {
	// check for ranges
	if (Period04.projectGetSelectedPoints()<5) {
	    JOptionPane.showMessageDialog(mainframe.getFrame(),
		"Not enough points for fourier-calculation!", "Error",
		JOptionPane.ERROR_MESSAGE);
	    return false;
	}
	// check if weights should be used
	if (Period04.projectGetPeriodUseWeight()==0) { 
	    if (Period04.projectGetSelectedPoints()!=  
		Period04.timestringGetWeightSum()) {
		int value = JOptionPane.showConfirmDialog(mainframe.getFrame(),
		    "Should I use the given weights for calculation?",
		    "Confirm", JOptionPane.YES_NO_OPTION);
		if (value == JOptionPane.YES_OPTION) {
		    weight=1; 
		}
	    }
	}
	// check if to is higher than nyquist
	double nyquist = Period04.projectGetNyquist();
	if (to > nyquist) {
	    int value = JOptionPane.showConfirmDialog(mainframe.getFrame(),
		"The final frequency to calculate ("+to+")\n"+
		"is higher than the calculated Nyquist frequency ("+
		format(nyquist)+")\n"+"Do you want to continue?",
		"Confirm" ,JOptionPane.YES_NO_OPTION);
	    if (value==JOptionPane.NO_OPTION) {
		return false;
	    }
	}
	return true;
    }

    public void calculateNoise() {
	if (task!=null) { return; }   // last task is not yet finished, return
	weight = Period04.projectGetUseFourierWeight(); 
	if (!spectrum) { // we just calculate the noise at a certain frequency
	    from = freq;
	    to = freq;
	} 
	if (!checkFourierSettings()) { return; } // check if values are ok
	String s_quality = (String)cbStepQuality.getSelectedItem();
	String steprate = Period04.projectGetStepRateString(s_quality, step);

	// is it a long calculation?
	Double longcalc = new Double(
	    Period04.projectGetSelectedPoints()*this.boxsize*
	    (this.to-this.from)/(this.spacing*
	    Period04.projectGetStepRate(
		(String)cbStepQuality.getSelectedItem(), this.step)));
	boolean isLongCalc;
	if (longcalc.isNaN() || longcalc.isInfinite()) { 
	    isLongCalc = true; 
	} else {
	    isLongCalc = (longcalc.doubleValue()>1.0E8);
	}
	// show the calculation panel for long calculations only
	// and set an appropriate time interval to update the progressbar
	if (isLongCalc) {          
	    interval = (int)(longcalc.doubleValue()/5.0E6);
	    String s_label;
	    if (spectrum) { 
		s_label = "Calculating Noise Spectrum...";
	    } else {
		s_label = "Calculating Noise ...";
	    }
	    mainframe.setCalculating(true, getCalcSettingsPanel(),
				     s_label, interval, true);
	}

	// write to protocol
	StringBuffer text = new StringBuffer();
	if (spectrum) {
	    text.append("Noise spectrum calculation\n");
	    if (step>0) { // stepquality = custom
		text.append("From frequency:           "+this.from+"\n"+
			    "To frequency:             "+this.to+"\n"+
			    "Steps:                    "+this.spacing+"\n"+
			    "Boxsize:                  "+this.boxsize+"\n"+
			    "Step rate:                "+steprate+"\n");
	    } else {
		text.append("From frequency:           "+this.from+"\n"+
			    "To frequency:             "+this.to+"\n"+
			    "Steps:                    "+this.spacing+"\n"+
			    "Boxsize:                  "+this.boxsize+"\n"+
			    "Step quality:             "+s_quality+"\n"+
			    "Step rate:                "+steprate+"\n");
	    }
	} else {
	    text.append("Noise calculation\n");
	    if (step>0) { // stepquality = custom
		text.append("At frequency:             "+this.from+"\n"+
			    "Boxsize:                  "+this.boxsize+"\n"+
			    "Step rate:                "+steprate+"\n");
	    } else {
		text.append("At frequency:             "+this.freq+"\n"+
			    "Boxsize:                  "+this.boxsize+"\n"+
			    "Step quality:             "+s_quality+"\n"+
			    "Step rate:                "+steprate+"\n");
	    }
	}

	String s_weight = "";
	if (this.weight!=0) { s_weight=" weighted"; }
	text.append("Calculation based on:     ");
	switch (datamode) {
	    case 0: // original
		text.append("the "+Period04.projectGetSelectedPoints()+
			    s_weight+" original measurements\n");
		break;
	    case 1: // adjusted
		text.append("the "+Period04.projectGetSelectedPoints()+
			    s_weight+" adjusted measurements\n");
		break;
	    case 2: // spectral window
		text.append("the "+Period04.projectGetSelectedPoints()+
			    s_weight+" points for the spectral window\n");
		break;
	    case 3: // residuals at original
		text.append("the "+Period04.projectGetSelectedPoints()+s_weight+
			    " residuals at the original measurements\n"+
			    "                          with "+
			    Period04.projectGetActiveFrequenciesString()+
			    " frequencies prewithened\n");
		break;
	    case 4: // residuals at adjusted
		text.append("the "+Period04.projectGetSelectedPoints()+s_weight+
			    " residuals at the adjusted measurements\n"+
			    "                          with "+
			    Period04.projectGetActiveFrequenciesString()+
			    " frequencies prewithened\n");
		break;
	} 

	// find out if zeropoint should be subtracted
	double zero=0;
	if ((datamode==0)||(datamode==1)) {
	    int oldDataMode = Period04.projectGetDataMode(); // save old mode
	    Period04.projectSetDataMode(datamode);           // set new mode
	    zero = Period04.timestringAverage(this.weight);  // get zeropoint
	    int value = JOptionPane.showConfirmDialog(mainframe.getFrame(),
		"Noise Calculation requires that the data\n"+
		"does not contain a zero point shift.\n"+
		"Otherwise additional features centered\n"+
		"at frequency 0.0 will appear.\n"+
		"These features may even dominate\n"+
		"the whole frequency spectrum.\n\n"+
		"Do you want to subtract the\n"+
		"average zero point of "+format(zero)+" ?",
		"Subtract zero point?", JOptionPane.YES_NO_OPTION);

	    if (value == JOptionPane.YES_OPTION) {
		Period04.projectSetDataMode(datamode);
		text.append("Zero point:               "+
			    "subtracted a zeropoint of "+zero+"\n");
	    } else {
		zero = 0;                      // ueberpruefen
		Period04.projectSetDataMode(oldDataMode); // hier wurde geaendert!!
	    }
	}
	text.append("Calculation started on:   "+XTabLog.getDate()+"\n");
	mainframe.getLog().writeProtocol(text.toString(), true);

	if (isLongCalc) { // create the calculation task for long calculations
	    if (spectrum) {
		task = new FourierNoiseTask(mainframe,
		    this, this.from, this.to, this.spacing, this.boxsize, 
		    this.stepquality, this.step, this.datamode, zero);
	    } else {
		task = new FourierNoiseTask(mainframe,
		    this, this.freq, this.boxsize, 
		    this.stepquality, this.step, this.datamode, zero);
	    }
	    task.go();                  // start calculation
	    mainframe.startTimer(task); // start the timer for the progressbar
	} else { // it's only a short calculation
	    // now go and calculate
	    if (spectrum) {
		Period04.projectCalculateNoiseSpectrum(
		    this.from, this.to, this.spacing, this.boxsize, 
		    this.stepquality, this.step, this.datamode, zero);
	    } else {
		Period04.projectCalculateNoiseAtFrequency(
		    this.freq, this.boxsize, this.stepquality, this.step, 
		    this.datamode, zero);
	    }
	    writeResultsToProtocol();
	    showResults();
	}
    }

    public JPanel getCalcSettingsPanel() { 
	JPanel panel = new JPanel();
	panel.setLayout(null);
	panel.setOpaque(false);
	panel.setBorder(BorderFactory.createCompoundBorder(
		BorderFactory.createTitledBorder(
		    BorderFactory.createEtchedBorder(),
		    " Calculation settings: ",
		    TitledBorder.LEADING, TitledBorder.TOP,
		    new Font("Dialog", Font.BOLD, 12),
		    new Color(20,30,150) ),
		BorderFactory.createEmptyBorder(5,5,5,5))
	    );

	String s_dmode = "Original Data";
	for (int i=0; i<6; i++) {
	    if (cbDataMode[i].isSelected()) {
		s_dmode = cbDataMode[i].getText();
	    } 
	}
	JLabel l_range, l_range_f, l_spacing=null, l_spacing_f=null;
	if (spectrum) {
	    l_range     = createLabel("Frequency range: ");
	    l_range_f   = createLabel("[ "+from+" , "+to+" ]");
	    l_spacing   = createLabel("Spacing:");
	    l_spacing_f = createLabel(Double.toString(spacing));
	} else {
	    l_range     = createLabel("Frequency: ");
	    l_range_f   = createLabel(Double.toString(freq));
	}
	JLabel l_boxsize       = createLabel("Boxsize:");
	JLabel l_boxsize_f     = createLabel(Double.toString(boxsize));
	JLabel l_stepquality   = createLabel("Step quality: ");
	JLabel l_stepquality_f = createLabel(
	    (String)cbStepQuality.getSelectedItem());
	JLabel l_step          = createLabel("Step rate: ");
	JLabel l_step_f        = createLabel(t_StepRate.getText());
	JLabel l_calcmode      = createLabel("Calculation based on: ");
	JLabel l_calcmode_f    = createLabel(s_dmode);

	int x0 = 15;
	int x1 = x0+140;
	int xdiff=0;
	if (!spectrum) { xdiff=20; }
	l_range        .setBounds( x0   ,  20, 150, 20);
	l_range_f      .setBounds( x1   ,  20, 150, 20);
	if (spectrum) {
	    l_spacing  .setBounds( x0   ,  40, 150, 20);
	    l_spacing_f.setBounds( x1   ,  40, 150, 20);
	}
	l_boxsize      .setBounds( x0   ,  60-xdiff, 150, 20);
	l_boxsize_f    .setBounds( x1   , 60-xdiff, 150, 20);
	l_stepquality  .setBounds( x0   , 80-xdiff, 150, 20);
	l_stepquality_f.setBounds( x1   , 80-xdiff, 150, 20);
	l_step         .setBounds( x0   , 100-xdiff, 150, 20);
	l_step_f       .setBounds( x1   , 100-xdiff, 150, 20);
	l_calcmode     .setBounds( x0   , 120-xdiff, 150, 20);
	l_calcmode_f   .setBounds( x1   , 120-xdiff, 150, 20);

	panel.setLayout(null);
	panel.add(l_range);
	panel.add(l_range_f);
	if (spectrum) {
	    panel.add(l_spacing);
	    panel.add(l_spacing_f);
	}
	panel.add(l_boxsize);
	panel.add(l_boxsize_f);
	panel.add(l_stepquality);
	panel.add(l_stepquality_f);
	panel.add(l_step);
	panel.add(l_step_f);
	panel.add(l_calcmode);
	panel.add(l_calcmode_f);

	return panel;
    }

    public void updateFrequencies() {
	if (cbFrequency!=null) { inputpanel.remove(cbFrequency); }
	int pos = 0;
	int freqs = 0;
	String [] entries = new String[1];
	// create a combobox with the available frequencies
	if (mainframe.getFitTab().getFitMode()==0) { // NORMAL_MODE
	    freqs = Period04.projectGetTotalFrequencies();
	    if (freqs>0) {
		entries = new String[freqs+1];
		freqindices = new int[freqs+1];
		for (int i=0; i<freqs; i++) {
		    if (!Period04.periodIsEmpty(i) || 
			Period04.projectIsComposition(i)) 
		    {
			freqindices[pos] = i;
			if (Period04.projectIsComposition(i)) {
			    entries[pos] = Period04.projectGetNumber(i)+": "+
				Period04.projectGetComposite(i);
			} else {
			    entries[pos] = Period04.projectGetNumber(i)+": "+
				Period04.projectGetFrequency(i);
			}
			pos++;
		    }
		}
	    }
	} else if (mainframe.getFitTab().getFitMode()==1) { // BINARY_MODE
	    freqs = Period04.projectGetBMTotalFrequencies();
	    if (freqs>0) {
		entries = new String[freqs+1];
		freqindices = new int[freqs+1];
		for (int i=0; i<freqs; i++) {
		    if (!Period04.periodIsBMEmpty(i) || 
			Period04.projectIsComposition(i)) 
		    {
			freqindices[pos] = i;
			if (Period04.projectIsBMComposition(i)) {
			    entries[pos] = Period04.projectGetBMNumber(i)+
				": "+Period04.projectGetBMComposite(i);
			} else {
			    entries[pos] = Period04.projectGetBMNumber(i)+
				": "+Period04.projectGetBMFrequency(i);
			}
			pos++;
		    }
		}
	    }
	}
	if (freqs>0) {
	    entries[pos] = "Other Frequency Peak";
	    freqindices[pos] = -1;
	    
	    if (cbFrequency!=null) {
		inputpanel.remove(cbFrequency);
		inputpanel.repaint();
	    }
	    cbFrequency = createComboBox(entries);
	    cbFrequency.addActionListener(this);
	    cbFrequency.setSelectedIndex(pos-1);
	    cbFrequency.setBounds(200, 25, 150, 20);
	    inputpanel.add(cbFrequency);
	    inputpanel.repaint();
	    cbFrequency.validate();
	}
    }

    public void writeResultsToProtocol() {
	// write results to protocol
	StringBuffer text = new StringBuffer();
	if (spectrum) {
	    text.append("Frequency\tNoise-amplitude\n");
	    int points = Period04.projectGetNoisePoints();
	    for (int i=0; i<points; i++) {
		text.append(Period04.projectGetNoiseString(i)+"\n");
	    }
	} else {
	    text.append("The noise is:             ");
	    text.append(Period04.projectGetNoise()+"\n");
	}
	String fin = " finished ";         
	if (Period04.cancel!=0) { fin = " canceled "; }
	text.append("Calculation"+fin+"on: "+XTabLog.getDate()+"\n");
	mainframe.getLog().writeProtocol(text.toString(), false);
    }
 

    void showResults() {
		if (spectrum) { list.setText(""); } // delete the old list
		// fill the list
		if (spectrum) {
			int points = Period04.projectGetNoisePoints();
			for (int i=0; i<points; i++) {
				list.append(Period04.projectGetNoiseString(i)+"\n");
			}
		} else { 
			if (cbFrequency==null) {
				list.append(Period04.projectGetNoiseString(0)+"\n");
				return;
			}
			int ind = freqindices[cbFrequency.getSelectedIndex()];
			if (!spectrum && ind!=-1) { // add S/N
				list.append(Period04.projectGetNoiseString(0));
				double snr = Period04.projectGetAmplitudeValue(ind)/
					Period04.projectGetNoiseValue();
				list.append("\tsnr: "+format(snr)+"\n");
			} else {	
				if (isFindPeak && 
					freq>Double.parseDouble(Period04.fourierGetFrom()) &&
					freq<Double.parseDouble(Period04.fourierGetTo())
					) {
					list.append(Period04.projectGetSubPeakNoiseString(freq,findPeakBinScale,0));
					double signal = Period04.projectGetSubPeakAmplitude(freq,findPeakBinScale);
					if (signal>0.0) {
						double snr = signal/Period04.projectGetNoiseValue();
						list.append("\tsnr: "+format(snr)+"\n");
					} else {
						list.append("\n");
					}
				} else {
					list.append(Period04.projectGetNoiseString(0)+"\n");
				}
			}
		}
    }
   
    public void dereferenceTask() {
		task = null;
    }

    public void saveNoise() {
	// create FileChooser
	JFileChooser fc = new JFileChooser(Period04.getCurrentDirectory());
	int returnVal = fc.showSaveDialog(mainframe.getFrame());
	if (returnVal == JFileChooser.APPROVE_OPTION) {
	    File file = fc.getSelectedFile();
	    // check if the extension is missing
	    String name = file.getName();
	    if (name.indexOf(".")==-1) { 
		file = new File(file.getPath()+".dat");
	    }
	    // check wether user wants to overwrite an existing file
	    if (file.exists()) {
		int v = JOptionPane.showConfirmDialog(
		    mainframe.getFrame(), "File does already exist!\n"+
		    "Do you really want to delete the old file?", 
			"Warning", JOptionPane.YES_NO_OPTION);
		if (v == JOptionPane.NO_OPTION) {
		    return;
		} 
	    }
	    // now go and save it
	    Period04.setCurrentDirectory(file);
	    try {
		FileWriter fw = new FileWriter(file);
		fw.write("Frequency         Noise-amplitude\n");
		fw.write(list.getText());
		if ( fw!=null ) fw.close();
	    } catch (IOException e) {
		System.err.println(e);
	    }
	}
    }
    
    public void printNoise() {
	StringBuffer text = new StringBuffer();
	text.append("Frequency         Noise-amplitude\n");
	text.append("----------------------------------------\n");
	text.append(list.getText());
	printResult(text.toString());
    }

    //---
    //--- methods to listen to ComponentEvents 
    //--- ComponentEvents are caused by i.e. resizing the mainframe
    //---
    public void componentResized(ComponentEvent e) {
	//--- the frame has been resized. 
	//--- set the framewidth back to the default
	//--- if the frameheight is to low set it back to the default value
	//--- if the frameheight is ok, then determine the difference to 
	//--- the default value and call fitSize()
	//--- this will size the GUI elements to fit to the new frameheight
	Component c = e.getComponent();
	Dimension frameSize = c.getSize();
	if (frameSize.height <= HEIGHT) {
	    frameSize.height = HEIGHT;
	    sizeDiff = 0;
	} else {
	    sizeDiff = HEIGHT-frameSize.height;
	}
	frameSize.width = WIDTH;
	setSize(frameSize);
	fitSize();
    }
    public void componentHidden(ComponentEvent e) {}
    public void componentShown(ComponentEvent e)  {}
    public void componentMoved(ComponentEvent e)  {}
 
    private void fitSize() {
	resultpanel.setBounds(  0, 210, 360, 160-sizeDiff);
	listPane   .setBounds( 10, 245, 340, 110-sizeDiff);
	inputpanel.revalidate();
	inputpanel.repaint();
    }

}
