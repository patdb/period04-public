import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

//---
//--- when compiling for Mac uncomment 
//---      .) the following 3 imports
//---      .) 'extends Application'
//---      .) p04.addApplicationListener( ....
//--- and  
//---      .) set variable isMacOS = true  
//---
//import com.apple.eawt.Application;
//import com.apple.eawt.ApplicationAdapter;
//import com.apple.eawt.ApplicationEvent;

public class Period04 //extends Application
{
    static {
        System.loadLibrary("period04");
    }

    public  final static boolean isMacOS = System.getProperty("os.name").toLowerCase().startsWith("mac os x"); // are we compiling for Mac?
    private       static SplashMaker splash;      // the Splash Window

    public static ArrayList projectframes       = new ArrayList();
    public static ArrayList availableProjectIDs = new ArrayList();

    private static File currentDirectory = new File(System.getProperty("user.dir"));
    public  static File [] recentFiles;
     
    //---
    //--- set/get the current directory
    //---
    public static File getCurrentDirectory() {
	return currentDirectory;
    }
    public static void setCurrentDirectory(File file) {
	currentDirectory = file;
    }

    //---
    //--- add a project file to the list of recent files
    //---
    public static void newRecentFile(String file) {
	Period04.addRecentFile(file);
	//--- update the menues
	int projects = projectframes.size();
	for (int i=0; i<projects; i++) {
	    ((XProjectFrame)projectframes.get(i)).updateRecentFiles();	    
	}
    }

    //---
    //--- set the font of the gui
    //---
    public static void setUIFont( javax.swing.plaf.FontUIResource f) {
	final java.util.Enumeration keys=(UIManager.getDefaults()).keys();
	while(keys.hasMoreElements()) {
	    final Object key=keys.nextElement();
	    final Object value=UIManager.get(key);
	    if(value instanceof javax.swing.plaf.FontUIResource) {
		UIManager.put(key,f);
	    }
	}
    }    

    private static boolean isNew=true;



  public static void main ( String[] args)
  {
	//--- create a Splash Window 
	//--- a replacement of this by the java6 command line option
	//--- was removed again because it affects the Locale setting!
	//---(replaced by java6 command line option -splash:Period04Splash.png)
      
      if (isMacOS)
	 {
	     System.setProperty("apple.laf.useScreenMenuBar", "true");
	  }

      if (!(args.length > 0 && args[0].startsWith( "-batch=") /*&& !isMacOS*/)) {
	  splash = new SplashMaker();
      }

      //--- always use UK localization to make sure to have 
      //--- a point as decimal separator
      Locale.setDefault(Locale.UK);

	//--- we prefer to use a specific font for all swing components
	setUIFont(new javax.swing.plaf.FontUIResource("Dialog",Font.BOLD,11));

	//--- setup all project IDs are available at this point
	int max = Period04.getMaxProjects();
	for (int i=0; i<max; i++) {
	    availableProjectIDs.add(new Integer(i));
	}

    Period04 p04 = new Period04();

	//---
	//--- enable application events for mac
	//---
	// thanks to a mac-bug (sometimes the applicationlistener does not
	// open a newProjectFrame by itself, especially when started freshly)
	// so we circumvent this bug in a very unelegant way:
	// we open a newProjectFrame here and try to stop the ApplicationListener
	// to open a second empty projectframe with help of 'isNew' 

    //if (! isMacOS)
    //{
      if (args.length > 0)
      { // now create a project frame
	  if (args[0].startsWith( "-batch="))
        {
	    setBatchMode(1); // switch to batch mode at JNI interface
          newProjectFrame( new File( args[0].substring( 7, args[0].length( ))), true);
        }
        else
        { 
          newProjectFrame( new File( args[0]),false);
        }
      }
      else
      {
        newProjectFrame( null, false);
      }
      //}

    if (isMacOS)
    {
	//Desktop desktop = Desktop.getDesktop();

	//newProjectFrame(null,false);


	Desktop.getDesktop().setAboutHandler(e -> {
	    ((XProjectFrame)projectframes.get(0)).showAbout();
    });

	Desktop.getDesktop().setOpenFileHandler(e -> {
		List<File> files = e.getFiles();
	        for (File fl: files) {
		    String filename = fl.getName();
            if (isWorking()) { return; }
		    if (isProjectAlreadyOpen(filename)) { return; }                                            
		    XProjectFrame pf = (XProjectFrame)projectframes.get(0);                                           
		    if (!pf.hasChanged() && pf.getProjectFile()=="") {                                                
			pf.loadProject(fl);                                                    
		    } else {                                                                                          
			if (filename==null) {                                                                  
			    newProjectFrame(null,false); return;                                                      
			}                                                                                             
			newProjectFrame(fl,false);                                             
		    }     
		}

	  }
        );


        Desktop.getDesktop().setQuitHandler((e,r) -> {
           shutdownAll();
        });
    }



    /*
    if (isMacOS) {
	//	    newProjectFrame(null,false);
		p04.addApplicationListener(new ApplicationAdapter() {
		public void handleAbout(ApplicationEvent e) {
		    ((XProjectFrame)projectframes.get(0)).showAbout();
		}
		public void handleOpenApplication(ApplicationEvent e) {
		    if (e.getFilename()==null) { 
		        if (isNew==true) {isNew=false; return;}
		        newProjectFrame(null,false); return;
		    }
		    newProjectFrame(new File(e.getFilename()),false);
		}
		public void handleReOpenApplication(ApplicationEvent e) {}
		public void handleOpenFile(ApplicationEvent e) {
		    if (isWorking()) { return; }
		    if (isProjectAlreadyOpen(e.getFilename())) { return; }
		    XProjectFrame pf = (XProjectFrame)projectframes.get(0);
		    if (!pf.hasChanged() && pf.getProjectFile()=="") {
			pf.loadProject(new File(e.getFilename()));
		    } else {
		        if (e.getFilename()==null) { 
			    newProjectFrame(null,false); return; 
			}
			newProjectFrame(new File(e.getFilename()),false);
		    }
		}
		public void handlePreferences(ApplicationEvent e) {}
		public void handlePrintFile(ApplicationEvent e) {}
		public void handleQuit(ApplicationEvent e) {
		    shutdownAll();
		}
	    });
	    }*/


  }

    public static int getNumberOfProjects() {
	return projectframes.size();
    }

    public static boolean isProjectAlreadyOpen(String newproject) {
	if (newproject.equals("")) { return false; }
	int projects = getNumberOfProjects();
	for (int i=0; i<projects; i++) {
	    String file=((XProjectFrame)projectframes.get(0)).getProjectFile();
	    if (newproject.equals(file)) {
		JOptionPane.showMessageDialog(
		    new JFrame(),
		    "This project is already open!\n"+
		    "If you want to open this project please\n"+
		    "make a copy and save it using an other name\n"+
		    "before opening it in period04.\n",
		    "Please note:", JOptionPane.INFORMATION_MESSAGE);
		return true;
	    }
	}
	return false;
    }
    
    public static void newProjectFrame(File file, boolean isBatchProcess) {
	int projectID = getNewProjectID();
	if (projectID==-1) {
	    JOptionPane.showMessageDialog(new JFrame(),
	        "The maximum number of projects that can\n"+
		"be opened at the same time is '"+
		Period04.getMaxProjects()+"'.\n\n"+
		"Please close at least one of the active\n"+
		"Period04 projects to open another\n"+
		"project file.", "Please note:",
		JOptionPane.INFORMATION_MESSAGE);
	    return;
 	}
	XProjectFrame p = new XProjectFrame(projectID, isBatchProcess); //create a projectframe
	projectframes.add(p);                     // add it to other projects
	if (file!=null) { 
	    if (isBatchProcess) {
		// deaktivate splash now, and then start processing
		if (splash!=null) { splash.dispose(); }   
		p.processBatchFile(file);
	    } else {
		p.loadProject(file); // open the project file
	    }
	}  
	//if (isMacOS) {
	if (splash!=null) { splash.dispose(); }   // remove splash window
	//}
    }

    //---
    //--- getNewProjectID()
    //--- check available project IDs and return the first found
    //---
    public static int getNewProjectID() {
	//--- check wether further project IDs are available
	if (availableProjectIDs.isEmpty()) { return -1;	}  
	//--- get the first available ID and remove it from the list
	int id = ((Integer)availableProjectIDs.get(0)).intValue();   
	availableProjectIDs.remove(0); 
	return id;
    }
 
    public static boolean isWorking() {
	int size = projectframes.size();
	boolean isworking = false;
	for (int i=0; i<size; i++) {
	    if (((XProjectFrame)projectframes.get(i)).isWorking()==true) { 
		isworking=true; 
		break;
	    }
	}
	return isworking;
    }

    public static void blockProjectWindows(boolean value, int pid) {
	int size = projectframes.size();
	for (int i=0; i<size; i++) {
	    if (i!=pid) {
		((XProjectFrame)projectframes.get(i)).blockProject(
		    !value, false);
	    } else {
		((XProjectFrame)projectframes.get(i)).blockProject(
		    !value, true);
	    }
	}
    }

    public static void shutdown(XProjectFrame p) {
	availableProjectIDs.add(             // project ID is available again
	    new Integer(p.getProjectID()));
	projectframes.remove(p);             // remove the project
	System.gc();                         // collect garbage
	(Runtime.getRuntime()).freeMemory(); // free memory
	if (projectframes.size()==0) {       // no more projects active?
	    System.exit(0);                  // finally exit program
	}
    }

    public static void shutdownAll() {
	while (projectframes.size()>0) {
	    XProjectFrame p = (XProjectFrame)projectframes.get(0);
	    p.getFrame().toFront();                       // show the respective frame
	    setUseProjectID(p.getProjectID()); //make sure to use right project
	    if (p.shutdown()==true) {          // shutdown respective project
		break;                         // -> shutdown has been canceled
	    }                   
	}
    }

    //--- ----------------------------------------------------- ---//
    //---                                                       ---//
    //---                   native methods                      ---//
    //---                                                       ---//
    //--- ----------------------------------------------------- ---//

    //---
    //--- native methods for command interface
    //---
    public static native String command ( String filename);

    //---
    //--- native methods for initialization / termination of the program
    //---
    public static native void initializeProject(int projectID, String userDir); 
    public static native String getWorkingDirectory();
    public static native void setBatchMode(int mode);
    
    //---
    //--- native methods to get and set recent files
    //---
    public static native int getNumberOfRecentFiles();
    public static native String[] getRecentFiles();
    public static native void addRecentFile(String file);

    //---
    //--- project related native methods
    //---
    public static native int  getMaxProjects();
    public static native void setUseProjectID(int projectID);
    public static native boolean isExpertMode();
    public static native void setExpertMode( boolean b);
    public static native int projectHasChanged(); 
    public static native void projectNoChanges();
    public static native void projectSetChanged(); // project has changed
    public static native void projectNewProject();
    public static native void projectClean(boolean fourier, boolean periods, 
				    boolean timestring);
    public static native String projectLoadProject( String filename);
    public static native void projectSaveProject( String filename);
    public static native String projectGetProjectFileName(); // returns path+filename
    public static native String projectGetWeightString();

    //--- 
    //--- timestring related native methods
    //---     
    public static native int projectGetSelectedPoints();
    public static native int projectGetTotalPoints();
    public static native String projectGetStartTime();
    public static native String projectGetEndTime();
    public static native double projectGetTotalTimeLength();
    public static native boolean projectGetReverseScale();
    public static native void projectSetReverseScale( int selected);
    public static native String projectGetNameSet( int i);
    public static native int projectNumberOfNames( int id);
    public static native int projectGetActiveNames( int attribute);
    public static native int projectGetIDName( int id,  int i);
    public static native String projectGetIDNameStr( int id,  int i);
    public static native String projectGetIDNameWeight( int id,  int i);
    public static native int projectGetIDNameColor( int id,  int attribute);
    public static native String projectGetIDNameColorStr( int id,  int attribute);
    public static native int projectTimeStringGetIDName( int row,  int attribute);
    public static native double projectGetIDNameAverageOrig( int attribute,  int selectedindex);
    public static native double projectGetIDNameAverageOrigWeight( int attribute,  int selectedindex);
    public static native double projectGetIDNameAverageAdj( int attribute,  int selectedindex);
    public static native double projectGetIDNameAverageAdjWeight( int attribute,  int selectedindex);
    public static native int projectGetIDNamePoints( int attribute,  int selectedindex);
    public static native int projectGetIndexName( int attribute,  int i);
    public static native String projectGetIndexNameStr( int attribute,  int i);
    public static native int projectGetIndexNameID( int attribute,  int i);
    public static native int projectGetIndexNameSelect( int attribute,  int i);
    public static native void projectChangeHeading( int i,  String heading);
    public static native void projectChangeName( int id,  int i,  String name,  String weight,  String color);
    public static native void projectCalcWeights();
    public static native void projectSelect();
    public static native void projectLoadTimeString( String file,  String fileformat,  int append, boolean isVOTable);
      public static native String predictFileFormat( String file); 
    public static native void projectSetIDNameSelect( int listboxID,  int j,  boolean selected);
    public static native double timestringAverage( int weight);
    public static native double projectGetTimeAverage();
    public static native boolean projectIsCalcErrors();
    public static native void projectSetCalcErrors( boolean value);
    public static native void projectSetShiftTimes( boolean value);
    public static native void projectSetMoCaSimulations( int i);
    public static native void projectSetInitialCalcMode( int i);
    public static native void projectInitRand( boolean sysTime);
    public static native void projectCalculateAverage();
    public static native String projectGetNameStatistics( int attribute,  int entryID,  boolean useweights);
    public static native String projectGetTimeStatistics( int attribute,  boolean isDays);

    public static native double projectTimePointGetTime ( int i);
    public static native double projectTimePointGetAmplitude ( int i,  int useData);
    public static native double [] projectTimePointGet ( int i,  int useData);

    public static native int projectTimePointGetIDName( int attribute,  int i);
//  public static native static int    projectTimePointGetIDNameColor(int attribute, 
    //						       int i);
    public static native String projectDeletePoint( int i);
    public static native String projectDeletePointsWithinBox( double x_start,  double y_start,  double x_end,  double y_end,  int mode);
    public static native String projectRelabelPoint( int i,  String attribute1,  String attribute2,  String attribute3,  String attribute4);
    public static native Object[] tstableGetHeader();
    public static native Object[] projectGetData( int columnindex);
    public static native String timestringGetOutputFormat();
    public static native String timestringGetFileName();
    public static native double timestringGetWeightSum();
    public static native void projectSaveTimeString( 
	String filename,  String outputformat, boolean votable);
    public static native void projectSaveTimeStringPhase(
	String filename,  String outputformat,  double frequency, 
	double zeropoint, boolean votable);
    public static native void projectSaveTimeStringPhaseBinned( 
	String filename, String outputformat, double frequency, 
	double binSpacing, int useData, int points, 
	double[] phase, double[] amplitude, double[] sigma);
    public static native int  projectGetDataMode();
    public static native void projectSetDataMode( int mode);
    public static native void projectSubdivide(
	double gapsize,  int attribute, boolean useCounter,  String prefix, 
	int digits);
    public static native void projectSubdivide( 
	double start, double interval, int attribute,  boolean useCounter, 
	String prefix,  int digits);
    public static native void projectCombineSubstrings( int attribute,  String label);
    public static native void projectAdjustData( int attribute,  int usePeriodWeight,  int selected,  int[] selectedindices,  boolean useWeights);
	public static native String timestringSubtractZeroPoint();
    public static native void projectSaveStat( int attribute,  boolean useWeights,  String filename);
    public static native int projectGetDeletePointAttribute();
    public static native String projectGetDeletePointName();
    public static native void projectSetDeletePointInfo( int attribute,  String name);
    public static native void projectDeleteSelectedPoints();
    public static native String projectMakeObservedAdjusted();
    public static native String projectMakeDataResidualsObserved();
    public static native String projectMakeAdjResidualsObserved();
    public static native String projectMakeCalculatedObserved();
    public static native String projectMakeAdjustedObserved();
//    public static native static int    timepointGetColor(int id, int i);
    public static native double timepointGetPhasedTime(int i, double freq, double zeropoint);

    //---
    //--- fourier related native methods
    //---
    public static native double projectGetStepRate( String stepquality,  double step);
    public static native String projectGetStepRateString( String stepquality,  double step);
    public static native String fourierGetProperties();
    public static native String fourierGetTitle();
    public static native String projectCheckFourierTitle( String title);
    public static native String fourierGetFrom();
    public static native String fourierGetTo();
    public static native double projectGetNyquist();
    public static native double projectGetSpecialNyquist( double t_start,  double t_end);
    public static native String projectGetNyquistString();
    public static native String fourierGetStepQuality();
    public static native double fourierGetStepping();
    public static native String[] fourierActiveGetPeak();
    public static native String[] fourierGetPeak( int i);
    public static native String projectIsFrequencyPeakComposition();
    public static native void projectSetFreqResForCompoSearch(double df);
    public static native double projectGetFreqResForCompoSearch();
    public static native int fourierGetMode();
    public static native void fourierSetMode( int mode);
    public static native int fourierGetCompact();
    public static native void fourierSetCompact( int compact);
    public static native int projectGetUseFourierWeight();
    public static native void projectCalculateFourier( 
	String title,  double from,  double to,  int stepquality,  
	double customsteprate,  int datamode,  int compactmode,  
	int weight,  double zeropoint);
    public static native void projectCalculateNoiseSpectrum( 
	double from, double to, double spacing, double boxsize, 
	int stepquality, double customsteprate, int datamode, double zeropoint);
    public static native void projectCalculateNoiseAtFrequency( double freq,  double boxsize,  int stepquality,  double steprate,  int datamode,  double zeropoint);
    public static native int projectGetFourierEntries();
    public static native String projectGetFourierTitle( int i);
    public static native String projectGetFourierActiveTitle();
    public static native int projectGetFourierActiveIndex();
    public static native void projectActivateFourier( int fourierID);
    public static native void projectDeleteActiveFourier();
    public static native void projectRenameActiveFourier( String newname);
    public static native int projectGetFourierActivePoints();
    public static native double projectGetFourierPointFrequency( int i);
    public static native double projectGetFourierPointAmplitude( int i);
    public static native Object[] fourierTableGetData( int columnindex);
    public static native void projectSaveFourier( int selectedID,  String filename);
    public static native void projectSaveAllFourier( String directory,  boolean usePrefix,  String prefix);
    public static native void projectSaveSpecialFourier( String directory,  int[] selectedindices,  int selected,  boolean usePrefix,  String prefix);
    public static native int projectAddFrequencyPeak(); 
                               // this adds the highest peak and returns the
                               // number of the peak in the frequency list
    public static native int projectAddFrequencySubPeak(double fr, double amp);
                               // this adds the given peak and returns the
                               // number of the peak in the frequency list
   public static native int projectGetNoisePoints();
    public static native String projectGetNoiseString( int i);
    public static native String projectGetNoise();
    public static native double projectGetNoiseValue();
    public static native double projectGetSubPeakFrequency(double fr, double binscale);
    public static native double projectGetSubPeakAmplitude(double fr, double binscale);
    public static native String projectGetSubPeakNoiseString(double fr, double binscale, int i);
    public static native String projectGetFrequencyAdjustment();
    public static native void projectSetFrequencyAdjustment( double adj);
    public static native void projectSetSW2Frequency(double freq);
    public static native void projectCleanFourier();

    //---
    //--- period related native methods
    //---
    public static native void projectLoadFrequencies(String filename,int mode);
    public static native void projectCopyFrequencies(boolean toPTS);
    public static native void projectSaveFrequencies(String filename,int mode,boolean latex);
	public static native String projectGetFrequenciesForPrinting();
    public static native String projectGetAnalyticalErrors();
    public static native String projectGetFitError();
    public static native void projectSaveFitError( String filename);
    public static native int projectGetMaxFreq();
    public static native int projectGetUseData();
    public static native int projectGetBMUseData();
    public static native int projectGetActiveFrequencies();
    public static native String projectGetActiveFrequenciesString();
    public static native int projectGetBMActiveFrequencies();
    public static native String projectGetBMActiveFrequenciesString();
    public static native String projectWriteActiveFrequencies();
    public static native String projectGetZeropoint();
    public static native String projectGetBMZeropoint();
    public static native String projectGetZeropointError();
    public static native String projectGetResiduals();
    public static native String projectGetBMResiduals();
    public static native double projectGetResidualsValue();
    public static native int projectGetIterations();
    public static native int projectGetTotalFrequencies();
    public static native void projectSetTotalFrequencies( int i);
    public static native int projectGetBMTotalFrequencies();
    public static native void projectSetBMTotalFrequencies( int i);
    public static native double projectGetFirstFrequency();
    public static native int projectGetFirstFrequencyNumber();
    public static native String projectGetNumber( int i);
    public static native String projectGetBMNumber( int i); 
    public static native boolean periodIsEmpty( int i);
    public static native boolean periodIsBMEmpty( int i);
    public static native boolean projectGetActive( int i);
    public static native void projectSetActive( int i,  boolean active);
    public static native boolean projectGetBMActive( int i);
    public static native void projectSetBMActive( int i,  boolean active);
    public static native boolean periodIsSelected( int i,  int what);
    public static native boolean periodIsBMSelected( int i,  int what);
    public static native void periodSetSelection( int i,  int what);
    public static native void periodSetBMSelection( int i,  int what);
    public static native void periodUnsetSelection( int i,  int what);
    public static native void periodUnsetBMSelection( int i,  int what);
    public static native boolean projectIsComposition( int i);
    public static native boolean projectIsBMComposition( int i);
    public static native String projectGetComposite( int i);
    public static native String projectGetBMComposite( int i);
    public static native double projectGetFrequencyValue( int i);
    public static native double projectGetBMFrequencyValue( int i);
    public static native String projectGetFrequency( int i);
    public static native String projectSetFrequency( int i,  String freq);
    public static native String projectGetBMFrequency( int i);
    public static native String projectSetBMFrequency( int i,  String freq);
    public static native String projectGetAmplitude( int i);
    public static native double projectGetAmplitudeValue( int i);
    public static native String projectGetBMAmplitude( int i);
    public static native double projectGetBMAmplitudeValue( int i);
    public static native void projectSetAmplitude( int i,  String amp);
    public static native void projectSetBMAmplitude( int i,  String amp);
    public static native String projectGetPhase( int i);
    public static native void projectSetPhase( int i,  String pha);
    public static native String projectGetBMPhase( int i);
    public static native void projectSetBMPhase( int i,  String pha);
	public static native boolean projectGetCompoUseFreqValue(int i);
	public static native void projectSetCompoUseFreqValue(int i, boolean val);
	public static native void projectSetCompoUseFreqValues(boolean val);
	public static native boolean projectGetBMCompoUseFreqValue(int i);
	public static native void projectSetBMCompoUseFreqValue(int i,boolean val);
	public static native void projectSetBMCompoUseFreqValues(boolean val);
    public static native void projectCleanSigmas();
    public static native int projectGetFitMode();
    public static native void projectSetFitMode( int fitmode);
    public static native void projectSetFitSelectionMode( int selmode);
    public static native String projectCalculatePeriod( int mode);
    public static native String projectImprovePeriod( int mode);
    public static native String projectImproveSpecialPeriod( int mode);
    public static native String projectImprovePTS( int mode);
    public static native String projectCalculateAmpVar( int mode,  int varmode,  int attribute,  int[] frequencies,  int selected);
    public static native String projectSearchPTSStartValues(int mode, int shots);
    public static native int projectGetPeriodUseData();
    public static native void projectResetLambda();
    public static native void projectCleanPeriod();
    public static native void projectCleanPeriod(int id);
    public static native String projectShowEpoch( double epoch,  int mode);
    public static native void projectRefit( double zeropoint);
    public static native String projectPredictAmplitude( double time);
    public static native double projectPredictAmplitude( double time,  int id);
    public static native double projectPredictBMAmplitude( double time,  int id);
    public static native void projectCreateArtificialData( String outfile,  double starttime,  double endtime,  double step,  double leading,  boolean append);
    public static native void projectCreateArtificialData( String outfile,  String infile, boolean isRange, double step,  double leading,  boolean append);
    public static native void projectCheckActive();
    public static native void projectCheckBMActive();
    public static native void periodRestoreSelection();

    //---
    //--- weight related native methods
    //---
    public static native int projectGetUseNameWeight();
    public static native void projectSetUseNameWeight( int flags);
    public static native boolean projectGetUsePointWeight();
    public static native void projectSetUsePointWeight( boolean value);
    public static native boolean projectGetUseDeviationWeight();
    public static native void projectSetUseDeviationWeight( boolean value);
    public static native double projectGetDeviationCutoff();
    public static native void projectSetDeviationCutoff( String cutoff);
    public static native void projectSetFourierUseWeight( boolean value);
    public static native int projectGetPeriodUseWeight();
    public static native void projectSetPeriodUseWeight( boolean value);
    public static native void projectSetUseWeightFlags();
	public static native void projectSetUsePointErrors(boolean value);

    public static native void projectSetMCRange( double freq_low,  double freq_up,  double amp_low,  double amp_up);

    //---
    //--- protocol related native methods
    //---
    public static native String projectWriteFrequenciesTabulated( boolean markselected);
    public static native void projectSaveProtocolText( String text);

    //---
    //--- methods for setting the progressbar in calculations
    //---
    public static int completed=0;
    public static String s_completed="-";
    public static int cancel=0;
    public static int getProgressBarValue()      { return completed; }
    public static int setProgressBarValue( int n) {
	completed=n;
	return cancel;
    }
    public static String getProgressString()     { return s_completed; }
    public static int setProgressString( String txt) {
	s_completed=txt;
	return cancel;
    }
    public static void setCalculationCanceled(int value) { cancel=value; }
    public static int isCalculationCanceled()            { return cancel; }
}

//---
//--- the following two classes became obsolete with Java 1.6
//---

//---
//--- SplashMaker 
//--- make a splash window
//---
class SplashMaker extends Frame {
    private SplashWindow sw;
    private Image splashIm;

    public SplashMaker() {
	MediaTracker mt=new MediaTracker(this);
	//--- define the location of the Splashwindow image
	String imagepath = 
	    System.getProperty("java.class.path").replaceAll(
		"period04.jar","Period04Splash.png");
	StringTokenizer st = new StringTokenizer(imagepath, ":");
	while (st.hasMoreTokens()) {
		String tmp = st.nextToken();
		if (tmp.indexOf("Period04Splash.png")!=-1) {
			imagepath=tmp;
			break;
		}
	}
	//--- now get the image
	splashIm = (Toolkit.getDefaultToolkit()).getImage(imagepath);
	mt.addImage(splashIm,0);
	try { mt.waitForID(0); } catch( InterruptedException ie) { }
	sw = new SplashWindow(this,splashIm);
    } 

    public void dispose() { sw.dispose(); }
}

//---
//--- SplashWindow 
//--- defines a splash window
//---
class SplashWindow extends Window {
    Image splashIm;
    
    SplashWindow( Frame parent,  Image splashIm) {
        super(parent);
        this.splashIm=splashIm;
        setSize(splashIm.getWidth(this),splashIm.getHeight(this));
	
        //--- center the window
	Dimension screenDim=(Toolkit.getDefaultToolkit()).getScreenSize();
	Rectangle winDim=getBounds();
        setLocation((screenDim.width -winDim.width) /2,
		    (screenDim.height-winDim.height)/2);
        setVisible(true);
    }
    
    public void paint( Graphics g) {
	if(splashIm!=null) {
	    g.drawImage(splashIm,0,0,this);
	}
    }
}

