/*---------------------------------------------------------------------*
 *  SplineInterpolation
 *  provides methods to perform a spline interpolation
 *---------------------------------------------------------------------*/

import java.util.Enumeration;
import java.util.Vector;

public class SplineInterpolation {

    private Spline  spline;    // contains the spline
    private Polynom poly;      // contains the polynom
    private int[] piv;         // pivot elements
    private int pivsign, m, n; 

    public SplineInterpolation(double[][] points) {
	this.calcPoly(points);
	this.calcSpline(points);
    }

    //---
    //--- calculate the spline
    //---
    private void calcSpline(double[][] points) {
	double[][] A;
	double[][] C;
	double[][] X;
	
        if (points.length<2) return;         // not enough points!
        if (points.length<3) {               // calculate a line
            this.spline = new Spline();
            this.spline.addPolynom(this.poly, points[0][0], points[1][0]);
	    return;
	} else {
 	    final int lgth=(points.length-1)<<2; //=4*(points.length-1)
	    A = new double[lgth][lgth];
	    C = new double[lgth][1];

            for (int i=0; i<points.length-1; i++) {
 		final int i4 = i<<2;
                /* P_i(x_i) = y_1 */
		A[i4][i4  ]=Math.pow(points[i][0],3);
		A[i4][i4+1]=Math.pow(points[i][0],2);
		A[i4][i4+2]=points[i][0];
		A[i4][i4+3]=1;
		C[i4][0]   =points[i][1];
                /* P_i(x_(i+1)) = y_(1+1) */
		A[i4+1][i4  ]=Math.pow(points[i+1][0],3);
		A[i4+1][i4+1]=Math.pow(points[i+1][0],2);
		A[i4+1][i4+2]=points[i+1][0];
		A[i4+1][i4+3]=1;
		C[i4+1][0]   =points[i+1][1];
            
                if (i<points.length-2) {
		    /* P_i'x_(i+1) = P_(i+1)'x_(i+1) */
		    A[i4+2][i4  ]=3 * Math.pow(points[i+1][0],2);
		    A[i4+2][i4+1]=2 * points[i + 1][0];
		    A[i4+2][i4+2]=1;
		    A[i4+2][i4+4]=-3 * Math.pow(points[i+1][0],2);
		    A[i4+2][i4+5]=-2 * points[i + 1][0];
		    A[i4+2][i4+6]=-1;
		    C[i4+2][0]=0;

                    /* P_i''x_(i+1) = P_(i+1)''x_(i+1) */
		    A[i4+3][i4  ]=6 * points[i + 1][0];
		    A[i4+3][i4+1]=2;
		    A[i4+3][i4+4]=-6 * points[i + 1][0];
		    A[i4+3][i4+5]=-2;
		    C[i4+3][0]=0;
                } else {
                    /* P_0''(x_0) = 0             boundary condition */
		    A[i4 + 2][0]=6 * points[0][0];
		    A[i4 + 2][1]=2;
		    C[i4 + 2][0]=0;
		    
                    /* P_(i+1)''(x_(i+1)) = 0     boundary condition */
		    A[i4 + 3][i4    ]=6 * points[0][0];
		    A[i4 + 3][i4 + 1]=2;
		    C[i4 + 3][0]=0;
                }
            }
        }
	// solve the system by a LU-Decomposition
	X = calcLUDecomposition(A, C);
	if (X==null) return; // LUDecomposition not possible

        // save back the coefficients for the polynomial
        spline = new Spline();
	final double [] values = new double[4];
        for ( int i=0; i<X.length; i+=4 )
        {
	    values[0] = X[i+0][0]; values[1] = X[i+1][0];
	    values[2] = X[i+2][0]; values[3] = X[i+3][0];
	    spline.addPolynom(
		new Polynom(values), points[i/4][0], points[i/4+1][0]);
        }
    }

    private double [][] calcLUDecomposition(double [][] A, double [][] C) {
	m = A.length;
	n = A[0].length;
	if (m!=n) return null; // LUDecomposition not possible
	
	double [][] X;

	piv = new int[m];
	for (int i=0; i<m; i++) { piv[i] = i; }
	pivsign = 1;
	double[] LUrowi;
	double[] LUcolj = new double[m];
       
	// outer loop.
	for (int j=0; j<n; j++) {
	    // make a copy of the j-th column to localize references.
	   
	    for (int i=0; i<m; i++) {
		LUcolj[i] = A[i][j];
	    }
	    
	    // apply previous transformations.
	    for (int i=0; i<m; i++) {
		LUrowi = A[i];
		// most of the time is spent in the following dot product.
		int kmax = Math.min(i,j);
		double s = 0.0;
		for (int k=0; k<kmax; k++) {
		    s += LUrowi[k]*LUcolj[k];
		}
		LUrowi[j] = LUcolj[i] -= s;
	    }
     
	   // find pivot and exchange if necessary.
	   int p = j;
	   for (int i=j+1; i<m; i++) {
	       if (Math.abs(LUcolj[i]) > Math.abs(LUcolj[p])) {
		   p = i;
	       }
	   }
	   if (p!=j) {
	       for (int k=0; k<n; k++) {
		   double t=A[p][k]; A[p][k]=A[j][k]; A[j][k] = t;
	       }
	       int k = piv[p]; piv[p] = piv[j]; piv[j] = k;
	       pivsign = -pivsign;
	   }

	   // compute multipliers.
	   if (j < m & A[j][j] != 0.0) {
	       for (int i = j+1; i < m; i++) {
		   A[i][j] /= A[j][j];
	       }
	   }
	}
       
	if (C.length != m) {
	    throw new IllegalArgumentException(
		"Matrix row dimensions must agree.");
	}

	// check if one of the matrices is singular
	boolean isNonsingular = true;
	for (int j=0; j<n; j++) {
	    if (A[j][j]==0)
		isNonsingular = false;
	}
	if (!isNonsingular) {
	    throw new RuntimeException("Matrix is singular.");
	}

	// copy right hand side with pivoting
	int nx = C[0].length;
 	X = new double[piv.length][nx];
	for (int i=0; i<piv.length; i++) {
	    for (int j=0; j<=nx-1; j++) {
		X[i][j] = C[piv[i]][j];
	    }
	}

	// solve L*Y = C(piv,:)
	for (int k=0; k<n; k++) {
	    for (int i=k+1; i<n; i++) {
		for (int j=0; j<nx; j++) {
		    X[i][j] -= X[k][j]*A[i][k];
		}
	    }
	}
	// Solve U*X = Y;
	for (int k=n-1; k>=0; k--) {
	    for (int j=0; j<nx; j++) {
		X[k][j] /= A[k][k];
	    }
	    for (int i=0; i<k; i++) {
		for (int j=0; j<nx; j++) {
		    X[i][j] -= X[k][j]*A[i][k];
		}
	    }
	}
	return X;
    }
    
    
    //---
    //--- calculates the substitute polynomial
    //---
    private void calcPoly(double[][] points) {

        double [][] A = new double[points.length][points.length];
        double [][] C = new double[points.length][1];
	double [][] X;

        for (int i=0; i<points.length; i++) {
            for (int j=0; j<points.length; j++)
                A[i][j] = Math.pow(points[i][0], points.length-j-1);
            C[i][0] = points[i][1];
        }

	// solve the system
	X = calcLUDecomposition(A, C);
	m = X.length;
	n = X[0].length;

        // save back the coefficients for the polynomial
	double [] vals = new double[m*n];
	for (int i=0; i<m; i++) {
	    for (int j=0; j<n; j++) {
		vals[i*n+j] = X[i][j];
	    }
	}
	poly = new Polynom(vals);
   }

    public Spline getSpline() {
        return this.spline;
    }

    //---
    //--- class Spline
    //---
    class Spline extends Polynom {
	public Spline() {
	    super(null);
	}

	class SplinePart {
	    double from;
	    double to;
	    Polynom poly;

	    public SplinePart(Polynom poly, double from, double to) {
		this.poly = poly;
		this.from = from;
		this.to = to;
	    }
	
	    public double function(double x) {
		return poly.function(x);
	    }
	}

	Vector polys = new Vector();

	public void addPolynom(Polynom poly, double from, double to) {
	    polys.add(new SplinePart(poly, from, to));
	}

	public double function(double x) {
	    // search for the appropriate polynomial
	    for ( Enumeration e=polys.elements(); e.hasMoreElements(); ) {
		SplinePart part = (SplinePart)e.nextElement();
		if ( x >= part.from && x <= part.to ) return part.function(x);
	    }
	    return java.lang.Double.NaN;
	}
    }	
   
    //---
    //--- class Polynom
    //---
    class Polynom {
	double[] values;
	
	public Polynom(double[] values) {
	    if ( values != null ) this.values = (double[])values.clone();
	}
	
	public double function(double x) {
	    double res = 0;
	    for ( int i=0; i<values.length; i++)
		res += values[i] * Math.pow(x, values.length-(i+1));
	    return res;
	}
    }
    
}
