/*-----------------------------------------------------------------------*
 *  TimestringFileFilter
 *  defines file extensions used by Period04
 *-----------------------------------------------------------------------*/

import java.io.File;
import javax.swing.*;
import javax.swing.filechooser.*;

public class TimestringFileFilter extends FileFilter 
{
  public boolean accept ( File f)
  {
    return f.isDirectory( )
        || f.getName( ).toLowerCase( ).endsWith( ".dat")
        || f.getName( ).toLowerCase( ).endsWith( ".txt")
        || f.getName( ).toLowerCase( ).endsWith( ".xml")
    ;
  }

  public String getDescription ( )
  {
    return "Time-string files(*.dat,*.txt,*.xml)";
  }
}


