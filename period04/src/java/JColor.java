/*----------------------------------------------------------------------*
 *  JColor
 *  defines some colors
 *----------------------------------------------------------------------*/

import java.awt.Color;

public class JColor {

    private final static int mColors = 16;

    private final static Color VIOLET = new Color(
	(float)0.62, (float)0.37, (float)0.62);
    private final static Color NAVY   = new Color(
	(float)0, (float)0, (float)0.5);
    private final static Color ORCHID = new Color(
	(float)0.86, (float)0.44, (float)0.86);
    private final static Color MAROON = new Color(
	(float)0.55, (float)0.14, (float)0.42);
    private final static Color PLUM   = new Color(
	(float)0.91, (float)0.68, (float)0.91);
    private final static Color BROWN  = new Color(
	(float)0.65, (float)0.16, (float)0.16);

    public final static Color getColor(int colorID) {
	switch (colorID) {
	    case 0:  return Color.RED;
	    case 1:  return Color.GREEN;
	    case 2:  return Color.BLUE;
	    case 3:  return Color.CYAN;
	    case 4:  return Color.YELLOW;
	    case 5:  return JColor.VIOLET;
	    case 6:  return Color.GREEN.darker();
	    case 7:  return Color.GRAY;
	    case 8:  return Color.ORANGE;
	    case 9:  return JColor.NAVY;
	    case 10: return Color.PINK;
	    case 11: return Color.WHITE;
	    case 12: return JColor.ORCHID;
	    case 13: return JColor.MAROON;
	    case 14: return JColor.PLUM;
	    case 15: return JColor.BROWN;
	    default: return Color.BLACK;
	}
    }

    public final static String getColorString(int colorID) {
	switch (colorID) {
	    case 0:  return "Red";
	    case 1:  return "Green";
	    case 2:  return "Blue";
	    case 3:  return "Cyan";
	    case 4:  return "Yellow";
	    case 5:  return "Violet";
	    case 6:  return "Dark green";
	    case 7:  return "Grey";
	    case 8:  return "Orange";
	    case 9:  return "Navy";
	    case 10: return "Pink";
	    case 11: return "White";
	    case 12: return "Orchid";
	    case 13: return "Maroon";
	    case 14: return "Plum";
	    case 15: return "Brown";
	    default: return "Black";
	}
    }

    public static int getNumberOfColors() {
	return mColors;
    }
}
