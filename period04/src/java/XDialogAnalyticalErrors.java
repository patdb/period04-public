/*----------------------------------------------------------------------*
 *  XDialogAnalyticalErrors
 *  a dialog that shows the analytical errors
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.*;                  // Property change stuff
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.help.CSH;
import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.event.*;
import javax.swing.text.BadLocationException;

class XDialogAnalyticalErrors extends XDialog 
{
    private static final long serialVersionUID = -4355612347900824281L;
    private JOptionPane  optionPane;
    private JButton      b_OK, b_Export, b_Print, b_Help;
    
    private JTextArea   textarea;
    private JScrollPane textScrollPane;   

    private XProjectFrame     mainframe;

    public XDialogAnalyticalErrors(XProjectFrame frame) {
	super(frame.getFrame(), false);
	mainframe = frame;
	b_Help   = createButton("Help");
	b_Export = createButton("Export");
	b_Print  = createButton("Print");
	b_OK     = createButton("Close");
	Object[] options = {b_Help, b_Export, b_Print, b_OK}; // set buttons

	b_Help.addActionListener(
	    new CSH.DisplayHelpFromSource(XProjectFrame.getHelpBroker()));
	CSH.setHelpIDString(b_Help,"gui.analyticalerrorsdialog"); // set link
	    
	//--- create a textarea
	textarea = new JTextArea(Period04.projectGetAnalyticalErrors());
        textarea.setFont(new Font("Monospaced", Font.PLAIN, 11));

	//--- make the list scrollable
	textScrollPane = new JScrollPane(
	    textarea, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
	    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	textScrollPane.setBorder(
	    BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
	
	//--- put it onto a panel
	JPanel inputpanel = new JPanel();
	inputpanel.add(textScrollPane);
	
	//---
	//--- create the optionpane and add the inputpanel and the buttons
	//---
	optionPane = new JOptionPane((Object)inputpanel, 
				     JOptionPane.PLAIN_MESSAGE,
				     JOptionPane.YES_NO_OPTION,
				     null, options, options[3]);
	setContentPane(optionPane);
	setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent we) {
		    /*
		     * Instead of directly closing the window,
		     * we're going to change the JOptionPane's
		     * value property.
		     */
		    optionPane.setValue(
			new Integer(JOptionPane.CLOSED_OPTION));
		}
	    });
	
	optionPane.addPropertyChangeListener(new PropertyChangeListener() {
		public void propertyChange(PropertyChangeEvent e) {
		    String prop = e.getPropertyName();
		    
		    if (isVisible() 
			&& (e.getSource() == optionPane)
			&& (prop.equals(JOptionPane.VALUE_PROPERTY) ||
			    prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
			Object value = optionPane.getValue();
			
			// reset 
			optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

			if (value == JOptionPane.UNINITIALIZED_VALUE) {
			    //ignore reset
			    return;
			}
		
			if (value.equals(b_Help)) { /*help is on the way*/ }
			else if (value.equals(b_Print)) { print(); }
			else if (value.equals(b_Export)) { export(); }
			else { setVisible(false); }
		    }
		}
	    });
	    
	this.setTitle("Analytical uncertainties:");
	this.pack();
	this.setLocationRelativeTo(mainframe.getFrame());
	this.setVisible(true);
    }

    public void actionPerformed(ActionEvent evt) {
	optionPane.setValue(evt.getSource());
    }
 

    public void export() {
	// create FileChooser
	JFileChooser fc = new JFileChooser(Period04.getCurrentDirectory());
	int returnVal = fc.showSaveDialog(mainframe.getFrame());
	if (returnVal == JFileChooser.APPROVE_OPTION) {
	    File file = fc.getSelectedFile();
	    // check if the extension is missing
	    String name = file.getName();
	    if (name.indexOf(".")==-1) { 
		file = new File(file.getPath()+".err");
	    }
	    // check wether user wants to overwrite an existing file
	    if (file.exists()) {
		int v = JOptionPane.showConfirmDialog(
		    mainframe.getFrame(), "File does already exist!\n"+
		    "Do you really want to delete the old file?", 
			"Warning", JOptionPane.YES_NO_OPTION);
		if (v == JOptionPane.NO_OPTION) {
		    return;
		} 
	    }
	    // now go and save it
	    Period04.setCurrentDirectory(file);
	    try {
		FileWriter fw = new FileWriter(file);
		fw.write(textarea.getText());
		if ( fw!=null ) fw.close();
	    } catch (IOException e) {
		System.err.println(e);
	    }
	}
    }

    public void print() {
	printResult(textarea.getText());
    }
}

