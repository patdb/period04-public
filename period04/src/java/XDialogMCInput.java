import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.*;
import javax.swing.event.*;

   //---
    //--- class XDialogMCInput
    //---
    //---
    class XDialogMCInput extends XDialog 
	implements ActionListener
    {
	private XProjectFrame mainframe;
	private JOptionPane  optionPane;
	private JCheckBox    c_randInit;
	private final String s_OK     = "OK";
	private final String s_Cancel = "Cancel";
	private boolean      cancel = false;
 
	//---
	//--- variables for the search of pts start parameters
	//---
	private double mc_freq_low = 0.0;
	private double mc_freq_up  = 0.0;
	private double mc_amp_low  = 0.0;
	private double mc_amp_up   = 0.0;
	private int    mc_shots    = 5000;
	
	public XDialogMCInput(final XProjectFrame mainframe) {
	    super(mainframe.getFrame(), true);  // necessary to keep dialog in front

	    Object[] options = {s_OK, s_Cancel};

	    JPanel inputpanel = new JPanel();
	    inputpanel.setLayout(null);
	    final JLabel l_info = createBoldLabel(
		"Search for good start values within ...");
	    final JLabel     l_freqrange = createBoldLabel("Frequency range:");
	    final JLabel     l_amprange  = createBoldLabel("Amplitude range:");

	    final double freq_low_bound = 1.0/Period04.projectGetTotalTimeLength();
	    final JTextField t_freq_low  = createTextField(format(freq_low_bound));
	    final JTextField t_freq_up   = createTextField("0.02");
	    final JTextField t_amp_low   = createTextField("0.0");
	    final JTextField t_amp_up    = createTextField("0.05");
	    final JLabel     l_shots     = createBoldLabel("Number of shots:");
	    final JTextField t_shots     = createTextField(
		(new Integer(mc_shots)).toString());
	    c_randInit  = createCheckBox(
		"Use system time to initialize random generator");
	    c_randInit  .setSelected(true);

	    l_info     .setBounds(  10,  5, 300, 20 );
	    l_freqrange.setBounds(  10, 30, 120, 20 );
	    t_freq_low .setBounds( 130, 30,  80, 20 );
	    t_freq_up  .setBounds( 215, 30,  80, 20 );
	    l_amprange .setBounds(  10, 55, 120, 20 );
	    t_amp_low  .setBounds( 130, 55,  80, 20 );
	    t_amp_up   .setBounds( 215, 55,  80, 20 );
	    l_shots    .setBounds(  10, 80, 120, 20 );
	    t_shots    .setBounds( 130, 80,  80, 20 );
	    c_randInit .setBounds(  10,110, 280, 20);

	    inputpanel.add( l_info,      null );
	    inputpanel.add( l_freqrange, null );
	    inputpanel.add( t_freq_low,  null );
	    inputpanel.add( t_freq_up,   null );
	    inputpanel.add( l_amprange,  null );
	    inputpanel.add( t_amp_low,   null );
	    inputpanel.add( t_amp_up,    null );
	    inputpanel.add( l_shots,     null );
	    inputpanel.add( t_shots,     null );
	    inputpanel.add( c_randInit,  null );

	    Object array = inputpanel;
	    optionPane = new JOptionPane(
		array, JOptionPane.PLAIN_MESSAGE,
		JOptionPane.YES_NO_OPTION, null, options, options[0]);
	    setContentPane(optionPane);
	    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	    addWindowListener(new WindowAdapter() {
		    public void windowClosing(WindowEvent we) {
			optionPane.setValue(
			    new Integer(JOptionPane.CLOSED_OPTION));
		    }
		});

	    optionPane.addPropertyChangeListener(new PropertyChangeListener() {
		    public void propertyChange(PropertyChangeEvent e) {
			String prop = e.getPropertyName();
			
			if (isVisible() 
			    && (e.getSource() == optionPane)
			    && (prop.equals(JOptionPane.VALUE_PROPERTY) ||
				prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
			    Object value = optionPane.getValue();
			    
			    if (value == JOptionPane.UNINITIALIZED_VALUE) {
				return;	//ignore reset
			    }
			    
			    //--- Reset the JOptionPane's value.
			    //--- If you don't do this, then if the user
			    //--- presses the same button next time, no
			    //--- property change event will be fired.
			    optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);
			    
			    if (value.equals(s_OK)) {
				//--- check validity
				if (!isDouble(t_freq_low, true)) { return; }
				if (!isDouble(t_freq_up, true)) { return; }
				if (!isDouble(t_amp_low, true)) { return; }
				if (!isDouble(t_amp_up, true)) { return; }
				if (!isDouble(t_shots, true)) { return; }
				//--- assign the values
				mc_freq_low = Double.parseDouble(t_freq_low.getText());
				mc_freq_up  = Double.parseDouble(t_freq_up.getText());
				mc_amp_low  = Double.parseDouble(t_amp_low.getText());
				mc_amp_up   = Double.parseDouble(t_amp_up.getText());
				mc_shots    = Integer.parseInt(t_shots.getText());
				if (mc_freq_low>=mc_freq_up) {
				    JOptionPane.showMessageDialog(
					mainframe.getFrame(), "There's something wrong "+
					"with the frequency range!\n", "Invalid entry",
					JOptionPane.ERROR_MESSAGE);
				    return;
				}
				if (mc_amp_low>=mc_amp_up) {
				    JOptionPane.showMessageDialog(
					mainframe.getFrame(), "There's something wrong "+
					"with the amplitude range!\n", "Invalid entry",
					JOptionPane.ERROR_MESSAGE);
				    return;
				}
				if (mc_freq_low<freq_low_bound) {
				    // notify the user that the chosen lower frequency bound is too small
				    int v = JOptionPane.showConfirmDialog(
					mainframe.getFrame(), "The lower frequency bound is too low!\n"+
					"The current time string has a total time base of "+
					format(Period04.projectGetTotalTimeLength())+".\n"+
					"It is recommended not to set the value of the lower\n"+
					"frequency limit below "+format(freq_low_bound)+
					" to obtain proper results.\n"+
					"Do you really want to proceed?", 
					"Warning", JOptionPane.YES_NO_OPTION);
				    if (v==JOptionPane.NO_OPTION) {
					return;
				    }
				}
				//--- initialize random generator
				Period04.projectInitRand(c_randInit.isSelected());
				//--- close dialog
				setVisible(false);
			    } else { // user closed dialog or clicked cancel
				cancel = true;
				setVisible(false);
			    }
			}
		    }
		});

	    this.setTitle("Search within range:");
	    this.setSize(330,230);
	    this.setResizable(false);
	    this.setLocationRelativeTo(mainframe.getFrame());
	    this.setVisible(true);
	}

	public double getLowFreq()           { return mc_freq_low; }
	public void   setLowFreq(double mfl) { mc_freq_low=mfl; }
	public double getUpFreq()            { return mc_freq_up; }
	public void   setUpFreq(double mfu)  { mc_freq_up=mfu; }
	public double getLowAmp()            { return mc_amp_low; }
	public void   setLowAmp(double mal)  { mc_amp_low=mal; }
	public double getUpAmp()             { return mc_amp_up; }
	public void   setUpAmp(double mau)   { mc_amp_up=mau; }
	public int    getShots()             { return mc_shots; }
	public void   setShots(int sh)       { mc_shots=sh; }

	public boolean isDouble(JTextField tf, boolean checknegative) {
	    String text = tf.getText();
	    // check if text contains letters 
	    NumberFormatException nan = null;
	    try { 
		Double.parseDouble(text); 
	    } catch (NumberFormatException exception) { 
		nan=exception; 
	    }		   
	    // check if empty
	    if (text.equals("") || (nan!=null)) {
		// text was invalid
		tf.selectAll();
		JOptionPane.showMessageDialog(
			 mainframe.getFrame(),"Sorry, \"" + text + "\" "
			 + "isn't a valid number.\n", "Invalid entry",
			 JOptionPane.ERROR_MESSAGE);
		return false;
	    } 
	    if (checknegative) { // check if negative
		if (Double.parseDouble(text)<0.0) {
		    tf.selectAll();
		    JOptionPane.showMessageDialog(
			mainframe.getFrame(), "Sorry, \"" + text + "\" "
			 + "isn't a valid number.\n", "Invalid entry",
			 JOptionPane.ERROR_MESSAGE);
		    return false;
		}
	    }
	    return true;
	}
	
	public boolean isCanceled() { return cancel; }

	public void actionPerformed(ActionEvent evt) {
	    if (evt.getSource()==c_randInit) { return; }
	    optionPane.setValue(s_OK);
	}
    }
