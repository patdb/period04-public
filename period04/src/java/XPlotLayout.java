/*--------------------------------------------------------------------------*
 *  XPlotLayout.java
 *  defines the layout of the plots
 *--------------------------------------------------------------------------*/

package gov.noaa.pmel.sgt.swing;

import gov.noaa.pmel.sgt.Layer;
import gov.noaa.pmel.sgt.CartesianGraph;
import gov.noaa.pmel.sgt.CartesianRenderer;
import gov.noaa.pmel.sgt.LineCartesianRenderer;
import gov.noaa.pmel.sgt.PointCartesianRenderer;
import gov.noaa.pmel.sgt.LinearTransform;
import gov.noaa.pmel.sgt.PlainAxis;
import gov.noaa.pmel.sgt.Axis;
import gov.noaa.pmel.sgt.JPane;
import gov.noaa.pmel.sgt.LayerNotFoundException;
import gov.noaa.pmel.sgt.Attribute;
import gov.noaa.pmel.sgt.LineAttribute;
import gov.noaa.pmel.sgt.PointAttribute;
import gov.noaa.pmel.sgt.Graph;
import gov.noaa.pmel.sgt.AxisNotFoundException;
import gov.noaa.pmel.sgt.CartesianGraph;
import gov.noaa.pmel.sgt.SGException;
import gov.noaa.pmel.sgt.SGLabel;
import gov.noaa.pmel.sgt.StackedLayout;
import gov.noaa.pmel.sgt.TransformAccess;
import gov.noaa.pmel.sgt.DataNotFoundException;
import gov.noaa.pmel.sgt.AttributeChangeEvent;

//import gov.noaa.pmel.sgt.swing.JGraphicLayout;

import gov.noaa.pmel.util.SoTValue;
import gov.noaa.pmel.util.SoTPoint;
import gov.noaa.pmel.util.SoTRange;
import gov.noaa.pmel.util.SoTDomain;
import gov.noaa.pmel.util.Domain;
import gov.noaa.pmel.util.Dimension2D;
import gov.noaa.pmel.util.Rectangle2D;
import gov.noaa.pmel.util.Point2D;
import gov.noaa.pmel.util.Range2D;

import gov.noaa.pmel.sgt.dm.SGTData;
import gov.noaa.pmel.sgt.dm.SGTLine;
import gov.noaa.pmel.sgt.dm.SimpleLine;
import gov.noaa.pmel.sgt.dm.SGTMetaData;
import gov.noaa.pmel.sgt.dm.Collection;
import gov.noaa.pmel.sgt.dm.PointCollection;

import java.util.Enumeration;
import java.util.Vector;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Color;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Component;
import java.awt.geom.AffineTransform;

import java.awt.Point;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.*;

import org.jibble.epsgraphics.EpsGraphics2D;

import java.beans.PropertyVetoException;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;


public class XPlotLayout extends XGraphicLayout
    implements PropertyChangeListener 
{
    private static final long serialVersionUID = 378436364039809598L;

    private Layer firstLayer;
    private int   layerCount;
    private int   plotType;
    private boolean isFourier = false;

    private static final Color[] colorList =
    { Color.blue, Color.cyan.darker(), Color.green, Color.orange.darker(),
      Color.red, Color.magenta, Color.black, Color.gray};

    private boolean computeScroll = false;
    boolean revXAxis = false;
    boolean revYAxis = false;
    boolean inZoom   = false;

    boolean autoRangeX = false;
    boolean autoRangeY = false;
    int autoXIntervals = 10;
    int autoYIntervals = 10;

    private static String LEFT_AXIS   = "Left Axis";
    private static String RIGHT_AXIS  = "Right Axis";
    private static String BOTTOM_AXIS = "Bottom Axis";
    private static String TOP_AXIS    = "Top Axis";

    public static final int POINTS = 0;
    public static final int LINE   = 1;
    
    private Domain originalDomain = null;
    private final double MAXIMUM_ZOOM_FACTOR = 100000.0;
    private boolean isMaximumZoom = false;     // has maximum zoom been reached
    private boolean offset = false;            // add an offset to the ranges

    private static double xSize = 10.0;          // size of the graph pane in
    private static double ySize =  7.0;          // physical coordinates
    private static double xMin  =  1.5;//1.0;    // position of the axes
    private static double xMax  =  9.0;//9.0;
    private static double yMin  =  1.0;//0.75;
    private static double yMax  =  6.25;//6.25;
    private static double labelHeight = 0.26;//0.25
    private static double titleHeight = 0.24;//0.22
    private static double mainTitleHeight = 0.26;
    private static double titleOffset = 0.18;

    private SGLabel mainTitle;

    private Domain         lastViewport;      // keeps last viewport used
    private Domain         oldViewport;       // to save a certain viewport
    private static boolean maintainViewport = false;
    private static boolean blockSaveViewport = false;

    public XPlotLayout() {
	super("", null, new Dimension(500,380));
	Layer           layer;
	CartesianGraph  graph;
	LinearTransform xt, yt;
	PlainAxis xbot  = null;
	PlainAxis xtop  = null;
	PlainAxis yleft = null;
	PlainAxis yright= null;

	//
	// create Pane and descendants for the LineProfile layout
	//
	setOpaque(true);
	setLayout(new StackedLayout());
	setBackground(Color.WHITE);
	layer = new Layer("Layer 1", new Dimension2D(xSize, ySize));
	firstLayer = layer;
	add(layer,0);
	layerCount = 0;
	//
	// title
	//
	Font titleFont = new Font("Helvetica", Font.BOLD, 14);
	mainTitle = new SGLabel("Plot Title", ""/* no title as default */,
				mainTitleHeight,
				new Point2D.Double(
				    (xMin+xMax)*0.5, 
				    ySize-1.0f*mainTitleHeight-titleOffset),
				SGLabel.BOTTOM, SGLabel.CENTER);
	mainTitle.setFont(titleFont);
	layer.addChild(mainTitle);
	//
	// create LineCartesianGraph and transforms
	//
	graph = new CartesianGraph("Graph 1");
	SoTRange xRange, yRange;
	xRange = new SoTRange.Double(10.0, 20.0, 2.0);
	yRange = new SoTRange.Double(400.0, 0.0, -50.0);
	SoTPoint origin, antiorigin;
	xt = new LinearTransform(new Range2D(xMin, xMax), xRange);
	yt = new LinearTransform(new Range2D(yMin, yMax), yRange);
	origin = new SoTPoint(xRange.getStart(), yRange.getStart());
	antiorigin = new SoTPoint(xRange.getEnd(), yRange.getEnd());
	graph.setXTransform(xt);
	graph.setYTransform(yt);
	//
	// create axes
	//
	Font axfont = new Font("Helvetica", Font.ITALIC, 14);
	xbot = new PlainAxis(BOTTOM_AXIS);
	xbot.setRangeU(xRange);
	xbot.setNumberSmallTics(0);
	xbot.setTicPosition(Axis.POSITIVE_SIDE);
	xbot.setLocationU(origin);
	xbot.setLabelHeightP(labelHeight);
	xbot.setLabelFont(axfont);
	xbot.setSelectable(false);
	graph.addXAxis(xbot);
	xtop = new PlainAxis(TOP_AXIS);
	xtop.setRangeU(xRange);
	xtop.setNumberSmallTics(0);
	xtop.setTicPosition(Axis.NEGATIVE_SIDE);
	xtop.setLocationU(antiorigin);
	xtop.setLabelPosition(Axis.NO_LABEL);
	xtop.setSelectable(false);
	graph.addXAxis(xtop);
	yleft = new PlainAxis(LEFT_AXIS);
	yleft.setRangeU(yRange);
	yleft.setNumberSmallTics(0);
	yleft.setTicPosition(Axis.POSITIVE_SIDE);
	yleft.setLabelHeightP(labelHeight);
	yleft.setLocationU(origin);
	yleft.setLabelFont(axfont);
	yleft.setSelectable(false);
	graph.addYAxis(yleft);
	yright = new PlainAxis(RIGHT_AXIS);
	yright.setRangeU(yRange);
	yright.setNumberSmallTics(0);
	yright.setTicPosition(Axis.NEGATIVE_SIDE);
	yright.setLocationU(antiorigin);
	yright.setLabelPosition(Axis.NO_LABEL);
	yright.setSelectable(false);
	graph.addYAxis(yright);
	// add graph to layer
	layer.setGraph(graph);
    }

    public void addData(SGTData datum, String descrip) {
	addData(datum, null, descrip);
    }

    public void addData(SGTData datum, Attribute attr, 
			String descrip, int type) {
	plotType = type;
	addData(datum, attr, descrip);
    }

    public void addData(SGTData datum, Attribute attr, String descrip) {
	Layer          layer, newLayer;
	CartesianGraph graph, newGraph;
	PlainAxis           bottom=null, left=null, top=null, right=null;
	LinearTransform xt, yt;
	SGLabel xtitle, ytitle;
	SGTData        data;
	SGTLine        line = null;
	PointCollection points = null;
	LineAttribute  lineAttr = null;
	PointAttribute pointAttr = null;
	SoTRange       xRange = null;
	SoTRange       yRange = null;
	SoTRange       xnRange = null;
	SoTRange       ynRange = null;
	SoTPoint       origin = null, antiorigin = null;
	Range2D        vRange = null;
	boolean        data_good = true;
	boolean        flipX = false;
	boolean        flipY = false;
	double         save;
	int            len;

	if(data_.size() == 0) {	    // only one data set...
	    super.addData(datum);
	    data = (SGTData)data_.firstElement();
	    if(plotType == POINTS){
		points = (PointCollection)data;
	    } else if(plotType == LINE){
		line = (SGTLine)data;
	    }
	    //
	    // find range 
	    //
	    xRange = data.getXRange();
	    yRange = data.getYRange();
	    flipX = data.getXMetaData().isReversed();
	    flipY = data.getYMetaData().isReversed();
	    revXAxis = flipX;
	    revYAxis = flipY;
	    //--- the following should only affect fourier plots:
	    //--- set y-start to 0.0 if the lower end of the range is quite 
	    //--- close to 0.0
	    if (isFourier) {
/*		if (Math.abs(((SoTValue.Double)yRange.getStart()).getValue())
		    <1e-10 &&
		    Math.abs(((SoTValue.Double)yRange.getEnd()).getValue()-
			     ((SoTValue.Double)yRange.getStart()).getValue())
		    >1e-05) {
*/		    yRange.setStart(new SoTValue.Double(0.0));
//		}
	    }
	    //
	    // check for good points
	    //
	    data_good = !(xRange.isStartOrEndMissing() ||
			  yRange.isStartOrEndMissing());
	    if(data_good) {
		//
		// flip range if data_good and flipped
		//
		if(flipX) { xRange.flipStartAndEnd(); }
		if(flipY) { yRange.flipStartAndEnd(); }
		//
		// compute "nice" range
		//
		if(autoRangeX) {
		    xnRange = Graph.computeRange(xRange, autoXIntervals);
		} else {
		    xnRange = xRange;
		    ((SoTRange.Double)xnRange).delta = 
			((SoTRange.Double)Graph.computeRange(
			    xRange, autoXIntervals)).delta;
		}
		if(autoRangeY) {
		    ynRange = Graph.computeRange(yRange, autoYIntervals);
		} else {
		    ynRange = yRange;
		    ((SoTRange.Double)ynRange).delta = 
			((SoTRange.Double)Graph.computeRange(
			    yRange, autoYIntervals)).delta;
		}
		//
		// test xnRange and ynRange
		//
		adjustRange(xnRange);
		adjustRange(ynRange);
		origin = new SoTPoint(xnRange.getStart(), ynRange.getStart());
		antiorigin = new SoTPoint(xnRange.getEnd(), ynRange.getEnd());
	    } // data_good

	    //
	    // attach information to pane and descendents
	    //
	    try {
		layer = getLayer("Layer 1");
	    } catch (LayerNotFoundException e) {
		return;
	    }
	    graph = (CartesianGraph)layer.getGraph();
	    //
	    // create axes
	    //
	    try {
		Font tfont = new Font("Helvetica", Font.PLAIN, 14);
		xtitle = new SGLabel("xaxis title",
				     data.getXMetaData().getName(),
				     new Point2D.Double(0.0, 0.0));
		xtitle.setFont(tfont);
		xtitle.setHeightP(titleHeight);
		ytitle = new SGLabel("yaxis title",
				     data.getYMetaData().getName(),
				     new Point2D.Double(0.0, 0.0));
		ytitle.setFont(tfont);
		ytitle.setHeightP(titleHeight);
		bottom = (PlainAxis)graph.getXAxis(BOTTOM_AXIS);
		top    = (PlainAxis)graph.getXAxis(TOP_AXIS);
		left   = (PlainAxis)graph.getYAxis(LEFT_AXIS);
		right  = (PlainAxis)graph.getYAxis(RIGHT_AXIS);
		bottom.setRangeU(xRange);
		top   .setRangeU(xRange);
		left  .setRangeU(ynRange);
		right .setRangeU(ynRange);
		bottom.setLocationU(origin);
		top   .setLocationU(antiorigin);
		left  .setLocationU(origin);
		right .setLocationU(antiorigin);
		bottom.setTitle(xtitle);
		left  .setTitle(ytitle);
	    } catch (AxisNotFoundException e) {}
	    if(data_good) {
		// transforms
		xt = (LinearTransform)graph.getXTransform();
		xt.setRangeU(xnRange);
		yt = (LinearTransform)graph.getYTransform();
		yt.setRangeU(ynRange);
	    }
	    //
	    // attach data
	    //
	    if(plotType == POINTS) {
		// SGTPoint
		if(attr != null && attr instanceof PointAttribute) {
		    pointAttr = (PointAttribute)attr;
		} else {
		    pointAttr = new PointAttribute(
			51 ,colorList[layerCount%8]);
		    pointAttr.setMarkHeightP(0.2);
		    pointAttr.setLabelHeightP(0.15);
		    pointAttr.setDrawLabel(false);
		    pointAttr.setLabelColor(Color.red);
		    pointAttr.setLabelPosition(PointAttribute.NE);
		}
		pointAttr.addPropertyChangeListener(this);
		addAttribute(datum, pointAttr);
		graph.setData(points, pointAttr);
	    } else if(plotType == LINE) {
		len = line.getYArray().length;
		if(attr != null && attr instanceof LineAttribute) {
		    lineAttr = (LineAttribute)attr;
		} else {
		    if(len >= 2) {
			lineAttr = new LineAttribute(LineAttribute.SOLID);
		    } else {
			lineAttr = new LineAttribute(LineAttribute.MARK);
		    }
		}
		lineAttr.addPropertyChangeListener(this);
		addAttribute(datum, lineAttr);
		graph.setData(line, lineAttr);
	    }
	    if(!isShowing()) computeScroll = true;
	} else {     // #of datasets
	    //
	    // more than one data set...
	    // add new layer
	    //
	    if(datum instanceof SGTLine) {
		if(datum.getYMetaData().isReversed() != revYAxis) {
		    SGTData modified = flipY(datum);
		    datum = modified;
		}
	    }
	    super.addData(datum);

	    data_good = false;
	    layerCount++;
	    if(isOverlayed()) {
		try {
		    layer = getLayer("Layer 1");
		} catch (LayerNotFoundException e) {
		    return;
		}
		graph = (CartesianGraph)layer.getGraph();
		//
		// transforms
		//
		xt = (LinearTransform)graph.getXTransform();
		yt = (LinearTransform)graph.getYTransform();
		try {
		    bottom = (PlainAxis)graph.getXAxis(BOTTOM_AXIS);
		    left   = (PlainAxis)graph.getYAxis(LEFT_AXIS);
		    top    = (PlainAxis)graph.getXAxis(TOP_AXIS);
		    right  = (PlainAxis)graph.getYAxis(RIGHT_AXIS);
		} catch (AxisNotFoundException e) {}
		if(!inZoom) {
		    //
		    // loop over data sets, getting ranges
		    //
		    SoTRange xTotalRange = null;
		    SoTRange yTotalRange = null;

		    boolean first = true;
		    
		    for (Enumeration e=data_.elements();e.hasMoreElements();) {
			data = (SGTData)e.nextElement();
			xRange = data.getXRange();
			yRange = data.getYRange();
			flipX = data.getXMetaData().isReversed();
			flipY = data.getYMetaData().isReversed();
			revXAxis = flipX;
			revYAxis = flipY;
			if(flipX) { xRange.flipStartAndEnd(); }
			if(flipY) { yRange.flipStartAndEnd(); }
			if(first) {
			    data_good = !(xRange.isStartOrEndMissing() ||
					  yRange.isStartOrEndMissing());
			    if(!data_good) {
				first = true;
			    } else {
				first = false;
				data_good = true;
				xTotalRange = xRange;
				yTotalRange = yRange;
			    }
			} else {
			    //
			    // not first
			    //
			    data_good = !(xRange.isStartOrEndMissing() ||
					  yRange.isStartOrEndMissing());
			    if(data_good) {
				xTotalRange.add(xRange);
				yTotalRange.add(yRange);
			    } // data_good
			} // first
		    } // loop over data elements

		    if(data_good) {
			if(autoRangeX) {
			    xnRange = Graph.computeRange(
				xTotalRange, autoXIntervals);
			} else {
			    xnRange = xTotalRange;
			    ((SoTRange.Double)xnRange).delta =
				((SoTRange.Double)Graph.computeRange(
				    xTotalRange, autoXIntervals)).delta;
			}
			if(autoRangeY) {
			    ynRange = Graph.computeRange(
				yTotalRange, autoYIntervals);
			} else {
			    ynRange = yTotalRange;
			    ((SoTRange.Double)ynRange).delta =
				((SoTRange.Double)Graph.computeRange(
				    yTotalRange, autoYIntervals)).delta;
			}
                        //
			// fix xnRange and ynRange
			//
			adjustRange(xnRange);
			adjustRange(ynRange);
			origin = new SoTPoint(
			    xnRange.getStart(), ynRange.getStart());
			antiorigin = new SoTPoint(
			    xnRange.getEnd(), ynRange.getEnd());
			//
			// axes
			//
			bottom.setRangeU(xnRange);
			bottom.setLocationU(origin);
			top.setRangeU(xnRange);
			top.setLocationU(antiorigin);
			left.setRangeU(ynRange);
			left.setLocationU(origin);
			right.setRangeU(ynRange);
			right.setLocationU(antiorigin);
			xt.setRangeU(xnRange);
			yt.setRangeU(ynRange);
		    } // data_good
		} // end of !inZoom

		//
		// create new layer and graph
		//
		newLayer = new Layer("Layer "+(layerCount+1), 
				     new Dimension2D(xSize, ySize));
		newGraph = new CartesianGraph("Graph "+(layerCount+1),xt,yt);
		if(inZoom) {
		    SoTRange xr = null;
		    SoTRange yr = null;
		    xr = bottom.getSoTRangeU();
		    yr = left.getSoTRangeU();
		    newGraph.setClip(xr, yr);
		    newGraph.setClipping(true);
		} // inZoom
		add(newLayer,0);
		newLayer.setGraph(newGraph);
		newLayer.invalidate();
		validate();

		//
		// attach data
		//
	        if(plotType == POINTS) { // SGTPoint
		    if(attr != null && attr instanceof PointAttribute) {
			pointAttr = (PointAttribute)attr;
		    } else {
			pointAttr = new PointAttribute(
			    50,colorList[layerCount%8]);
			pointAttr.setMarkHeightP(0.15);
			pointAttr.setLabelHeightP(0.15);
			pointAttr.setDrawLabel(false);
			pointAttr.setLabelColor(Color.red);
			pointAttr.setLabelPosition(PointAttribute.NE);
		    }
		    pointAttr.addPropertyChangeListener(this);
		    addAttribute(datum, pointAttr);
		    newGraph.setData(datum, pointAttr);
		} else if(plotType == LINE) { // SGTLine
		    len = ((SGTLine)datum).getXArray().length;
		    if(attr != null && attr instanceof LineAttribute) {
			lineAttr = (LineAttribute)attr;
		    } else {
			if(len >= 2) {
			    lineAttr = new LineAttribute(
				LineAttribute.SOLID, 50,
				colorList[layerCount%8]);
			} else {
			    lineAttr = new LineAttribute(
				LineAttribute.MARK, 50,
				colorList[layerCount%8]);
			}
		    }
		    lineAttr.addPropertyChangeListener(this);
		    addAttribute(datum, lineAttr);
		    newGraph.setData(datum, lineAttr);
		}
	    } // overlayed
	} // # of datasets

	// save the original domain, will be used to prevent users
	// from zooming into plot beyond a maximum zooming limit
	if (originalDomain==null) 
	    originalDomain = getRange(); 
    }

    //---
    //--- If start == end fix
    //---
    private void adjustRange(SoTRange range) {
	double end = ((SoTRange.Double)range).end;
	double st = ((SoTRange.Double)range).start;
	double dlt = ((SoTRange.Double)range).delta;
	if(dlt == 0) { // delta computation failed
	    end = st;
	}
	if(end == st) {
	    if(end == 0.0) {
		st = -1.0;
		end =  1.0;
	    } else {
		if(end > 0.0) { end = 1.1*end; } 
		else          { end = 0.9*end; }
		if(st > 0.0)      { st = 0.9*st; } 
		else              { st = 1.1*st; }
	    }
	    ((SoTRange.Double)range).end = end;
	    ((SoTRange.Double)range).start = st;
	    ((SoTRange.Double)range).delta =
		((SoTRange.Double)Graph.computeRange(range, 10)).delta;
	} // end == st
	if (offset) {
	    double factor = (end-st)/50;
	    ((SoTRange.Double)range).end = end+factor;
	    ((SoTRange.Double)range).start = st-factor;
	    ((SoTRange.Double)range).delta =
		((SoTRange.Double)Graph.computeRange(range, 10)).delta;
	}
    }

    //---
    //--- Flip the yaxis.  Reverse the direction of the y axis by changing 
    //--- the sign of the axis values and isReversed flag.
    //---
    private SGTData flipY(SGTData in) {
	SGTMetaData zmetaout;
	SGTMetaData zmetain;
	SimpleLine out = null;
	SGTLine line = (SGTLine) in;
	double[] values;
	double[] newValues;
	values = line.getYArray();
	newValues = new double[values.length];
	for(int i=0; i < values.length; i++) {
	    newValues[i] = -values[i];
	}
	out = new SimpleLine(line.getXArray(), newValues, line.getTitle());
	zmetain = line.getYMetaData();
	zmetaout = new SGTMetaData(zmetain.getName(), zmetain.getUnits(),
				   zmetain.isReversed(), zmetain.isModulo());
	zmetaout.setModuloValue(zmetain.getModuloValue());
	zmetaout.setModuloTime(zmetain.getModuloTime());
	out.setXMetaData(line.getXMetaData());
	out.setYMetaData(zmetaout);
	return (SGTData)out;
    }
    
    public void resetZoom() {
	Attribute attr;
	SGTData data;
	SoTRange xRange = null;
	SoTRange yRange = null;
	SoTRange xTotalRange = null;
	SoTRange yTotalRange = null;
	boolean data_good = false;
	boolean flipY = false;
	boolean flipX = false;
	double save;
	boolean batch = isBatch();
    
	setBatch(true, "JPlotLayout: resetZoom");
	inZoom = false;
	isMaximumZoom=false;
	setAllClipping(false);
	setClipping(false);
	//
	// loop over data sets, getting ranges
	//
	boolean first = true;
	Enumeration e = data_.elements();
	while (e.hasMoreElements()) {
	    data = (SGTData)e.nextElement();
	    try {
		attr = getAttribute(data);
	    } catch(DataNotFoundException except) {
		System.out.println(except);
		attr = null;
	    }
	    if((plotType == POINTS) || (plotType == LINE)) {
		xRange = data.getXRange();
		yRange = data.getYRange();
	    }
	    flipX = data.getXMetaData().isReversed();
	    flipY = data.getYMetaData().isReversed();
	    revXAxis = flipX;
	    revYAxis = flipY;
	    
	    if(flipX) { xRange.flipStartAndEnd(); }
	    if(flipY) { yRange.flipStartAndEnd(); }
	    if(first) {
		data_good = !(xRange.isStartOrEndMissing() ||
			      yRange.isStartOrEndMissing());
		if(!data_good) { 
		    first = true;
		} else {
		    first = false;
		    data_good = true;
		    xTotalRange = xRange;
		    yTotalRange = yRange;
		}
	    } else {
		data_good = !(xRange.isStartOrEndMissing() ||
			      yRange.isStartOrEndMissing());
		if(data_good) {
		    xTotalRange.add(xRange);
		    yTotalRange.add(yRange);
		} // data_good
	    } // first
	} // for loop
	//
	// fix ranges
	//
	if(xTotalRange != null && yTotalRange != null) {
	    adjustRange(xTotalRange);
	    adjustRange(yTotalRange);
	    if(data_good) {
		try {
		    setRange(new SoTDomain(xTotalRange, yTotalRange, flipX, flipY));
		} catch (PropertyVetoException ve) {
		    System.out.println("zoom reset denied! " + ve);
		}
	    }
	}
	// turn off clipping and clip coastline
	//
	inZoom = false;
	if(!batch) setBatch(false, "JPlotLayout: resetZoom");
    }
    
    //---
    //--- reset zoom rectangle
    //---
    public void reset() { 
	resetZoom();
    }

    public void undoZoom() {
	if (lastViewport==null) { return; }
	Domain tmp = lastViewport;
	resetZoom();
	setClipping(true);
	setXRange(tmp.getXRange());   // set the old xrange
	setYRange(tmp.getYRange());   // set the old yrange
    }

    public void setUseOffset(boolean value) { 
	offset = value;
    }

    //---
    //--- save the current viewport (this method is used for replots)
    //---
    public boolean setMaintainViewport(boolean value) {
	boolean oldvalue = maintainViewport;
	maintainViewport = value;
	return oldvalue;
    }
    public void saveViewport() {
	if (blockSaveViewport) { return; }
	lastViewport = getRange();
    }
    public void saveCertainViewport() {
	oldViewport = getRange();
    }
    public Domain getCertainViewport() {
	return oldViewport;
    }
    public void blockSaveViewport(boolean value) {
	blockSaveViewport = value;
    }
    public void setViewport(boolean rescaleY) {
	// if we cannot or don't want to set the old viewport then return
	if (lastViewport==null || !maintainViewport) { return; }
	// otherwise set the old viewport
	setClipping(true);
	setXRange(lastViewport.getXRange());     // set old xrange
	if (rescaleY) {
	    setYRange(getRange().getYRange());   // rescale yrange
	} else {
	    setYRange(lastViewport.getYRange()); // set old yrange
	}
    }

    public Domain getRange() {
	CartesianGraph graph = (CartesianGraph)firstLayer.getGraph();
	LinearTransform xt = (LinearTransform)graph.getXTransform();
	LinearTransform yt = (LinearTransform)graph.getYTransform();
	Range2D xr = null;
	Range2D yr = null;
	xr = xt.getRangeU();
	yr = yt.getRangeU();
	return new Domain(xr, yr);
    }

    public void setRange(SoTDomain std) throws PropertyVetoException {
	saveViewport();
	// now set the new viewport
	Domain domain = new Domain();
	SoTRange.Double dbl = (SoTRange.Double)std.getXRange();
	domain.setXRange(new Range2D(dbl.start, dbl.end, dbl.delta));
	dbl = (SoTRange.Double)std.getYRange();
	domain.setYRange(new Range2D(dbl.start, dbl.end, dbl.delta));
	domain.setXReversed(std.isXReversed());
	domain.setYReversed(std.isYReversed());
	setRange(domain);
    }

    public void setRange(Domain domain) throws PropertyVetoException {
	saveViewport();
	Domain oldRange = getRange();
	if(!domain.equals(oldRange)) {
	    boolean batch = isBatch();
	    setBatch(true, "XPlotLayout: setRange");
	    vetos_.fireVetoableChange("domainRange", oldRange, domain);
	    inZoom = true;
	    setXRange(domain.getXRange());
	    setYRange(domain.getYRange(), domain.isYReversed());
	    changes_.firePropertyChange("domainRange", oldRange, domain);
	    if(!batch) setBatch(false, "XPlotLayout: setRange");
	}
    }

    /**
     * Reset the x range. This method is designed to provide
     * zooming functionality.
     *
     * @param rnge new x range
     */
    void setXRange(Range2D rnge) {
	Axis     bottom, left, top, right;
	SoTRange xr = new SoTRange.Double(checkMaximumXRangeZoom(rnge));
	SoTRange yr;
	SoTPoint origin, antiorigin;
	SoTRange xnRange;
	CartesianGraph  graph = (CartesianGraph)firstLayer.getGraph();
	LinearTransform xt    = (LinearTransform)graph.getXTransform();

	if(autoRangeX) {
	    xnRange = Graph.computeRange(xr, autoXIntervals);
	} else {
	    xnRange = xr;
	    ((SoTRange.Double)xnRange).delta =
		((SoTRange.Double)Graph.computeRange(
		    xr, autoXIntervals)).delta;
	}
	//---
	//--- i noticed that whenever delta is very small (<1.0e-06)
	//--- the whole program freezes, to prevent this i define a limit
	//--- for delta here.
	//---
	//if (Math.abs(((SoTRange.Double)xnRange).delta)<1.0e-06) {
	if (Math.abs(((SoTRange.Double)xnRange).delta)<1.0e-04) {
	    setXRange(lastViewport.getXRange());
	    isMaximumZoom=true;
	    return;
	}
	xt.setRangeU(xnRange);
	try {
	    bottom = graph.getXAxis(BOTTOM_AXIS);
	    top    = graph.getXAxis(TOP_AXIS);
	    left   = graph.getYAxis(LEFT_AXIS);
	    right  = graph.getYAxis(RIGHT_AXIS);

	    bottom.setRangeU(xnRange);
	    top.setRangeU(xnRange);
	    yr = left.getSoTRangeU();
	    xr = bottom.getSoTRangeU();
	    
	    origin     = new SoTPoint(xr.getStart(), yr.getStart());
	    antiorigin = new SoTPoint(xr.getEnd(), yr.getEnd());
	    bottom.setLocationU(origin);
	    left  .setLocationU(origin);
	    top   .setLocationU(antiorigin);
	    right .setLocationU(antiorigin);
	    //
	    // set clipping
	    //
	    if(clipping_) {
		setAllClip(xr, yr);
	    } else {
		setAllClipping(false);
	    }
	} catch (AxisNotFoundException e) {}
    }

    private Range2D checkMaximumXRangeZoom(Range2D newRange) { 
	if (originalDomain==null) { return newRange; }
	Range2D origXRange = originalDomain.getXRange();
	double zoomfactor = (origXRange.end-origXRange.start)/
	    (newRange.end-newRange.start);
	if (zoomfactor > MAXIMUM_ZOOM_FACTOR) {
	    double maximumRange = (origXRange.end-origXRange.start)/MAXIMUM_ZOOM_FACTOR;
	    double rangeDifference = (newRange.end-newRange.start)-maximumRange;
	    newRange.end = newRange.end-rangeDifference/2;
	    newRange.start = newRange.start+rangeDifference/2;
	    isMaximumZoom = true;
	}
	return newRange;	    
    }

    private Range2D checkMaximumYRangeZoom(Range2D newRange) { 
	if (originalDomain==null) { return newRange; }
	Range2D origYRange = originalDomain.getYRange();
	double zoomfactor = (origYRange.end-origYRange.start)/
	    (newRange.end-newRange.start);
	if (Math.abs(zoomfactor)>MAXIMUM_ZOOM_FACTOR) {
	    double maximumRange;
	    if (zoomfactor>0) {
		maximumRange = (origYRange.end-origYRange.start)/MAXIMUM_ZOOM_FACTOR;
	    } else {
		maximumRange = -(origYRange.end-origYRange.start)/MAXIMUM_ZOOM_FACTOR;
	    }
	    double rangeDifference = (newRange.end-newRange.start)-maximumRange;
	    newRange.end = newRange.end-rangeDifference/2;
	    newRange.start = newRange.start+rangeDifference/2;
	    isMaximumZoom = true;
	}
	return newRange;	    
    }

    public boolean isMaximumZoom() {
	return isMaximumZoom;
    }
    
    /**
     * Reset the y range. This method is designed to provide
     * zooming functionality.
     *
     * @param rnge new y range
     */
    void setYRange ( Range2D rnge)
    {
      setYRange( rnge, revYAxis);
    }

    /**
     * Reset the y range. This method is designed to provide
     * zooming functionality.
     *
     * @param rnge new y range
     * @param reversed data is reversed
     */
    void setYRange ( Range2D rnge, boolean reversed)
    {
      SGTData grid;
      Axis bottom, top, left, right;
      SoTRange xr;
      SoTRange yr = new SoTRange.Double( checkMaximumYRangeZoom( rnge));
      SoTRange ynRange;
      SoTPoint origin, antiorigin;
      boolean flip;
      CartesianGraph graph = (CartesianGraph) firstLayer.getGraph( );
      LinearTransform yt = (LinearTransform) graph.getYTransform( );

      if (! data_.isEmpty( ))
      {
        grid = (SGTData) data_.elements( ).nextElement( );

        if ( data_.size( ) > 0
          && (reversed != grid.getYMetaData( ).isReversed( ))
           )
        {
          yr.flipStartAndEnd( );
        }
      }

      if (autoRangeY)
      {
        ynRange = Graph.computeRange( yr, autoYIntervals);
      }
      else
      {
        ynRange = yr;

        ((SoTRange.Double) ynRange).delta
          = ((SoTRange.Double) Graph.computeRange( yr, autoYIntervals)).delta
        ;
      }
      //---
      //--- i noticed that whenever delta is very small (<1.0e-06)
      //--- the whole program freezes, to prevent this i define a limit
      //--- for delta here.
      //---
      if (Math.abs( ((SoTRange.Double) ynRange).delta) < 1.0e-06)
      {
// prevent stack overflow during plot of Fourier spectrum with low peak amplitudes.
//        setYRange( lastViewport.getYRange( ));
        isMaximumZoom = true;
        return;
      }

      yt.setRangeU( ynRange);

      try
      {
          bottom = (PlainAxis) graph.getXAxis( BOTTOM_AXIS);
          top    = (PlainAxis) graph.getXAxis( TOP_AXIS);
          left   = (PlainAxis) graph.getYAxis( LEFT_AXIS);
          right  = (PlainAxis) graph.getYAxis( RIGHT_AXIS);

          left.setRangeU(ynRange);
          right.setRangeU(ynRange);
          xr = bottom.getSoTRangeU();
          yr = left.getSoTRangeU();

          origin = new SoTPoint(xr.getStart(), yr.getStart());
          antiorigin = new SoTPoint(xr.getEnd(), yr.getEnd());

          bottom.setLocationU( origin);
          left  .setLocationU( origin);
          top   .setLocationU( antiorigin);
          right .setLocationU( antiorigin);
          
          //
          // set clipping
          //
          if (clipping_)
          {
            setAllClip( xr, yr);
          }
          else
          {
            setAllClipping( false);
          }
      } 
      catch (AxisNotFoundException e)
      {
      }
    }

    /**
     * Find a dataset from the data's id.
     *
     * @param data_id the id
     * @return <code>SGTData</code>
     */
    public SGTData getData(String data_id) {
	try {
	    Layer ly = getLayerFromDataId(data_id);
	    if(ly != null) {
		CartesianRenderer rend = 
		    ((CartesianGraph)ly.getGraph()).getRenderer();
		if(rend != null) {
		    if(rend instanceof LineCartesianRenderer) {
			return (SGTData)((LineCartesianRenderer)rend).getLine();
		    } 
		}
	    }
	} catch (LayerNotFoundException e) {}
	return null;
    }
    /**
     * Find a dataset from the renderer.
     *
     * @param rend the renderer
     * @return <code>SGTData</code>
     */
    public SGTData getData(CartesianRenderer rend) {
	if(rend instanceof LineCartesianRenderer) {
	    return (SGTData)((LineCartesianRenderer)rend).getLine();
	}
	return null;
    }
    /**
     * Remove all data from the <code>JPlotLayout</code>
     */
    public void clear() {
	data_.removeAllElements();
	((CartesianGraph)firstLayer.getGraph()).setRenderer(null);
	removeAll();
	add(firstLayer,0);   // restore first layer
	inZoom = false;
    }
    /**
     * Remove a specific dataset from the <code>JPlotLayout</code>
     *
     * @param data_id the data id
     */
    public void clear(String data_id) {
	Layer ly = null;
	SGTData dat;
	try {
	    ly = getLayerFromDataId(data_id);
	    remove(ly);
	} catch (LayerNotFoundException e) {}
	for(Enumeration it=data_.elements(); it.hasMoreElements();) {
	    dat = (SGTData)it.nextElement();
	    if(dat.getId().equals(data_id)) {
		data_.removeElement(dat);
	    }
	}
	if(getComponentCount() <= 0 || ly.equals(firstLayer)) {
	    ((CartesianGraph)firstLayer.getGraph()).setRenderer(null);
	    add(firstLayer,0);  // restore first layer
	}
    }

    /**
     * Get the <code>JPlotLayout</code> layer size in physical
     * coordinates.
     */
    public Dimension2D getLayerSizeP() {
	return new Dimension2D(xSize, ySize);
    }

    public Layer getFirstLayer() {
	return firstLayer;
    }
    /**
     * Set the axes origin in physical units
     */
    public void setAxesOriginP(Point2D.Double pt) {
	xMin = pt.x;
	yMin = pt.y;
    }


    public SoTDomain getGraphDomain() {
	SoTRange xRange = null;
	SoTRange yRange = null;
	CartesianGraph graph = null;
	try {
	    Layer layer = getLayer("Layer 1");
	    graph = (CartesianGraph)layer.getGraph();
	} catch (LayerNotFoundException e) {
	    return null;
	}
	try {
	    Axis bottom = graph.getXAxis(BOTTOM_AXIS);
	    Axis left   = graph.getYAxis(LEFT_AXIS);
	    xRange = bottom.getSoTRangeU();
	    yRange = left.getSoTRangeU();
	} catch (AxisNotFoundException e) {
	    return null;
	}
	return new SoTDomain(xRange, yRange);
    }
    
    /**
     * Set the layer size in physical units
     */
    public void setLayerSizeP(Dimension2D d) {
	Component[] comps = getComponents();
	CartesianGraph graph = (CartesianGraph)firstLayer.getGraph();
	LinearTransform yt = (LinearTransform)graph.getYTransform();
	LinearTransform xt = (LinearTransform)graph.getXTransform();
	xMax = d.width - (xSize - xMax);
	yMax = d.height - (ySize - yMax);
	xSize = d.width;
	ySize = d.height;
	for(int i=0; i < comps.length; i++) {
	    if(comps[i] instanceof Layer) {
		((Layer)comps[i]).setSizeP(d);
	    }
	}
	yt.setRangeP(new Range2D(yMin, yMax));
	xt.setRangeP(new Range2D(xMin, xMax));
    }

    private void resetAxes() { 
	Domain domain = getRange();
	setXRange(domain.getXRange());
	setYRange(domain.getYRange());
    }
    
    /**
     * Used by <code>JPlotLayout</code> to listen for changes in line,
     * grid, vector, and point attributes.
     */
    public void propertyChange(PropertyChangeEvent evt) {
	if(evt.getSource() instanceof Attribute) {
	    boolean local = true;
	    if(evt instanceof AttributeChangeEvent) {
		local = ((AttributeChangeEvent)evt).isLocal();
	    }
	    changes_.firePropertyChange(
		new AttributeChangeEvent(
		    this, "attribute", null, evt.getSource(), local));
	}
    }

    /**
     * Implements the <code>print</code> method in
     * <code>java.awt.print.Printable</code>.  Overrides <code>JPane</code> behavior.
     */
    public int print(Graphics g, PageFormat pf, int pageIndex) {
	if(pageIndex > 0) {
	    return NO_SUCH_PAGE;
	} else {
	    // for resolution
	    Dimension oldsize = this.getSize();
	    Dimension newsize = new Dimension(2000, 1400);
	    this.setSize(newsize);
	    this.validate();

	    Graphics2D g2 = (Graphics2D) g;
	    double scale = 1.0;
	    Dimension d = getSize();        // retrieve the size of the plot
	    double xfactor = pf.getImageableWidth()/d.getWidth();
	    double yfactor = pf.getImageableHeight()/d.getHeight();
	    if      (xfactor<yfactor) { scale = xfactor; } 
	    else if (yfactor<xfactor) { scale = yfactor; }
	    g2.translate((int)pf.getImageableX(),(int)pf.getImageableY());
	    g2.scale(scale,scale);
	    this.draw(g2);

	    this.setSize(oldsize);
	    this.validate();

	    return PAGE_EXISTS;
	}
    }
    /**
     * Turn on/off the auto range feature for the x axis. Auto range creates a
     * "nice" range with a delta of 1., 2., 5. or 10^n of these.  The start
     * and end of the range is extended to the next full delta.
     */
    public void setXAutoRange(boolean xauto) {
	autoRangeX = xauto;
    }
    /**
     * Turn on/off the auto range feature for the y axis. Auto range creates a
     * "nice" range with a delta of 1., 2., 5. or 10^n of these.  The start
     * and end of the range is extended to the next full delta.
     */
    public void setYAutoRange(boolean yauto) {
	autoRangeY = yauto;
    }
    /**
     * Turn on/off the auto range feature for the x and y axes. Auto range
     * creates a "nice" range with a delta of 1., 2., 5. or 10^n of these.
     * The start and end of the range is extended to the next full delta.
     */
    public void setAutoRange(boolean xauto, boolean yauto) {
	autoRangeX = xauto;
	autoRangeY = yauto;
    }
    /**
     * Tests if the auto range feature is enabled for the x axis.
     */
    public boolean isXAutoRange() {
	return autoRangeX;
    }
    /**
     * Tests if the auto range feature is enabled for the y axis.
     */
    public boolean isYAutoRange() {
	return autoRangeY;
    }
    /**
     * Set the approximate number of x axis intervals for auto range.
     */
    public void setXAutoIntervals(int xint) {
	autoXIntervals = xint;
    }
    /**
     * Set the approximate number of y axis intervals for auto range.
     */
    public void setYAutoIntervals(int yint) {
	autoYIntervals = yint;
    }
    /**
     * Set the approximate number of x and y axes intervals for auto range.
     */
    public void setAutoIntervals(int xint, int yint) {
	autoXIntervals = xint;
	autoYIntervals = yint;
    }
    /**
     * Return the number of intervals for the x axis.
     */
    public int getXAutoIntervals() {
	return autoXIntervals;
    }
    /**
     * Return the number of intervals for the y axis.
     */
    public int getYAutoIntervals() {
	return autoYIntervals;
    }

    public void setPlotTitle(String title) {
	mainTitle.setText(title);
    }

	public void adjustLabels(double wfac, double hfac) {
		Point2D.Double mloc = mainTitle.getLocationP();
		double y = (mloc.y+1.0f*mainTitleHeight+titleOffset)*hfac
			-1.0f*mainTitleHeight-titleOffset;
		mainTitle.setLocationP(new Point2D.Double(mloc.x*wfac,y));
	}
	public void resetLabels() {
		mainTitle.setLocationP(new Point2D.Double(
				    (xMin+xMax)*0.5, 
				    ySize-1.0f*mainTitleHeight-titleOffset));
	}

    public void setIsFourier(boolean b) { isFourier=b; }

    public String getLocationSummary(SGTData grid) { return ""; }

    public void setKeyBoundsP(Rectangle2D.Double d) {}
    public Rectangle2D.Double getKeyBoundsP() { return null;}

    /**
     * Override JPane init method.  The scrolling list parameters are computed
     * here if necessary.
     */
    public void init() {
	if(computeScroll) {
	    computeScroll = false;
	    int rowHeight = 1;
	}
    }

}
