/*---------------------------------------------------------------------*
 *  XTabTimeString.java
 *  creates and manages the TimeString tab
 *---------------------------------------------------------------------*/

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import java.net.URL;
import java.net.MalformedURLException;
import java.nio.channels.FileChannel;
import java.util.*;                 // sorting list
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.filechooser.*;
import javax.swing.border.*;         // TitledBorder stuff

import gov.noaa.pmel.sgt.swing.XPlotLayout;

import org.votech.plastic.managers.PlasticApplication;

import uk.ac.starlink.table.StarTable;
import uk.ac.starlink.table.StarTableWriter;
import uk.ac.starlink.votable.VOTableWriter;
import uk.ac.starlink.table.StarTableOutput;

public class XTabTimeString extends XTab
    implements ItemListener, ListSelectionListener
{
    private JButton    b_Import, b_Export, b_Append;
    private JButton [] b_Label;
    private JButton [] b_Edit;
    private JButton    b_DisplayTable, b_DisplayPlot;
    //private JList listFiles;
    private JTextArea fileLog; 
    public  JLabel l_SelectedPoints, l_StartTime, l_TotalPoints, l_EndTime;
    public  JCheckBox checkReverseScale;

    private JFileChooser fc;
    private XProjectFrame mainframe;
    private int correct;
    private boolean selectionChanged = false;
    public  boolean blockSelectionEvent = false;
    private XPlotTimeString plot = null;

    private DefaultListModel [] listboxModel;
    private JList     [] listbox;
    private ArrayList        [] array = new ArrayList[4];
   
    private JScrollPane [] listScrollPane = new JScrollPane[4];


    // popup menu
    private JPopupMenu popup;
    private MouseEvent popupEvent;
    private JMenuItem  iPop_Edit/*, iPop_Delete*/, iPop_SelectAll, iPop_CombineSelected;

    public XTabTimeString(XProjectFrame m_frame) {

	mainframe = m_frame;
	correct = mainframe.getSizeDiff();

	// create main panel
	mainPanel = new JPanel();
	mainPanel.setLayout(null);
	createMainPanel();

	// create a popupmenu for attribute list
	createPopupMenu();
    }

    private void createMainPanel() {
	// create opaque panel with border line
	JPanel topPanel = new JPanel();
	topPanel.setLayout(null);
	topPanel.setOpaque(false);
	topPanel.setBorder(BorderFactory.createCompoundBorder(
		BorderFactory.createTitledBorder(
		    BorderFactory.createEtchedBorder(EtchedBorder.RAISED), 
		    " Current data file ",
		    TitledBorder.LEADING, TitledBorder.TOP, 
		    new Font("Dialog", Font.BOLD, 10), 
		    new Color(20,30,150) ),
		BorderFactory.createEmptyBorder(5,5,5,5))
	    );

	// create buttons
	b_Import = createButton("Import time string");
	b_Append = createButton("Append time string");
	b_Export = createButton("Export time string");
	b_Import.setMnemonic('t');
	b_Export.setMnemonic(KeyEvent.VK_E);
	b_Import.setMargin(new Insets(0,0,0,0)); 
	b_Append.setMargin(new Insets(0,0,0,0)); 
	b_Export.setMargin(new Insets(0,0,0,0)); 

	fileLog = new JTextArea("no data file(s)");  
        fileLog.setFont(new Font("Helvetica", Font.PLAIN, 10));
        fileLog.setEditable(false);
        JScrollPane logScrollPane = new JScrollPane(
	    fileLog, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, 
	    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	logScrollPane.setBorder(
	    BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
	// limit the height of the horizontal scrollbar
	JScrollBar hscroll = logScrollPane.getHorizontalScrollBar();
	Dimension hdim = hscroll.getSize();
	hscroll.setPreferredSize(new Dimension(hdim.width, 11));
	// limit the width of the vertical scrollbar
	JScrollBar vscroll = logScrollPane.getVerticalScrollBar();
	Dimension vdim = vscroll.getSize();
	vscroll.setPreferredSize(new Dimension(11, vdim.height));

	JLabel l_Start = createBoldLabel("Start time:");
	l_StartTime = createBoldLabel("no points");
	JLabel l_Selected = createBoldLabel("Points selected:");
	l_SelectedPoints = createBoldLabel("0");

	JLabel l_End = createBoldLabel("End time:");
	l_EndTime = createBoldLabel("no points");
	JLabel l_Total = createBoldLabel("Total points:");
	l_TotalPoints = createBoldLabel("0");

	checkReverseScale = new JCheckBox("Time string is in magnitudes",
					  Period04.projectGetReverseScale());
	checkReverseScale.addItemListener(this);
	checkReverseScale.setFont(new Font("Dialog", Font.BOLD, 10));
	
	b_Label = new JButton[4];
	b_Edit = new JButton[4];
	for (int i=0; i<4; i++) {
	    b_Label[i] = createButton(Period04.projectGetNameSet(i));
	}

	listboxModel = new DefaultListModel[4];
	listbox = new JList[4];
	JScrollBar hlistscroll[] = new JScrollBar[4];
	JScrollBar vlistscroll[] = new JScrollBar[4];
	Dimension hscrolldim[] = new Dimension[4];
	Dimension vscrolldim[] = new Dimension[4];
	for (int i=0; i<4; i++) {
	    listboxModel[i] = new DefaultListModel();
	    listbox[i] = new JList(listboxModel[i]);
	    listbox[i].setSelectionMode(
		ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	    listbox[i].setFont(new Font("Dialog", Font.PLAIN, 10));

	    // add the selectionlistener
	    MouseInputListener mil = new DragSelectionListener();
	    listbox[i].addMouseMotionListener(mil);
	    listbox[i].addMouseListener(mil);

	    // make the list scrollable
	    listScrollPane[i] = new JScrollPane(listbox[i],
		JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
		JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	    listScrollPane[i].setBorder(
		BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
	    //--- limit the height of the horizontal scrollbar
	    hlistscroll[i] = listScrollPane[i].getHorizontalScrollBar();
	    hscrolldim[i]  = hlistscroll[i].getSize();
	    hlistscroll[i].setPreferredSize(
		new Dimension(hscrolldim[i].width, 11));
	    //--- limit the width of the vertical scrollbar
	    vlistscroll[i] = listScrollPane[i].getVerticalScrollBar();
	    vscrolldim[i]  = vlistscroll[i].getSize();
	    vlistscroll[i].setPreferredSize(
		new Dimension(11, vscrolldim[i].height));

	    //--- We don't want the JList implementation to compute the width
	    //--- or height of all of the list cells, so we give it a string
	    //--- that's as big as we'll need for any cell.  It uses this to
	    //--- compute values for the fixedCellWidth and fixedCellHeight
	    //--- properties.
	    listbox[0].setPrototypeCellValue("Prototype List Entry");
	}

	for (int i=0; i<4; i++) {
	    b_Edit[i] = createButton("Edit substring");
	    b_Edit[i].setToolTipText(
		"<html>Edit the properties<br>of the selected substring</html>");
	}

	b_DisplayTable = createButton("Display table");
	b_DisplayPlot  = createBoldButton("Display graph");
	b_DisplayPlot.setMnemonic(KeyEvent.VK_D);
	b_DisplayPlot.setToolTipText(
	    "<html>Display the time string graph</html>");

	//---
	//--- layout the components
	//---
	if (Period04.isMacOS) {
		b_Import         .setBounds( 10, 20,135, 20);
		b_Append         .setBounds( 10, 40,135, 20);
		b_Export         .setBounds( 10, 60,135, 20);
	} else {
		b_Import         .setBounds( 10, 20,125, 20);
		b_Append         .setBounds( 10, 40,125, 20);
		b_Export         .setBounds( 10, 60,125, 20);
	}
	l_Start          .setBounds( 30, 90, 80, 15);
	l_StartTime      .setBounds(120, 90,120, 15);
	l_Selected       .setBounds(260, 90,120, 15);
	l_SelectedPoints .setBounds(380, 90,100, 15);
	l_End            .setBounds( 30,105, 80, 15);
	l_EndTime        .setBounds(120,105,120, 15);
	l_Total          .setBounds(260,105,100, 15);
	l_TotalPoints    .setBounds(380,105,100, 15);
	checkReverseScale.setBounds( 30,130,200, 15);
	if (Period04.isMacOS) { // mac os specific layout
	    topPanel     .setBounds(  0,  0,509,160);
	    logScrollPane.setBounds(150, 20,350, 60);
	    for (int i=0; i<4; i++) {
		b_Label[i]       .setBounds(10+i*125,170,115, 20);
		listScrollPane[i].setBounds(10+i*125,190,115,350-correct);
		b_Edit[i]        .setBounds(10+i*125,540-correct,115, 20);
	    }
	    b_DisplayTable.setBounds( 10,570-correct,240, 20);
	    b_DisplayPlot .setBounds(260,570-correct,240, 20);
	} else { // layout for windows/linux
	    topPanel     .setBounds(  0,  0,489,160);
	    logScrollPane.setBounds(140, 20,340, 60);
	    for (int i=0; i<4; i++) {
		b_Label[i]       .setBounds(10+i*120,170,110, 20);
		listScrollPane[i].setBounds(10+i*120,190,110,350-correct);
		b_Edit[i]        .setBounds(10+i*120,540-correct,110, 20);
	    }
	    b_DisplayTable.setBounds( 10,570-correct,230, 20);
	    b_DisplayPlot .setBounds(250,570-correct,230, 20);
	}
	//---
	//--- add to main panel
	//---
	mainPanel.add( topPanel, null);
	mainPanel.add( b_Import, null);
	mainPanel.add( b_Append, null);
	mainPanel.add( b_Export, null);
	mainPanel.add( logScrollPane, null);
	mainPanel.add( l_Selected, null);
	mainPanel.add( l_SelectedPoints, null);
	mainPanel.add( l_Start, null);
	mainPanel.add( l_StartTime, null);
	mainPanel.add( l_Total, null);
	mainPanel.add( l_TotalPoints, null);
	mainPanel.add( l_End, null);
	mainPanel.add( l_EndTime, null);
	mainPanel.add( checkReverseScale, null);
	for (int i=0; i<4; i++) { 
	    mainPanel.add( b_Label[i], null); 
	    mainPanel.add( listScrollPane[i], null);
	    mainPanel.add( b_Edit[i], null);
	}
	mainPanel.add( b_DisplayTable, null);
	mainPanel.add( b_DisplayPlot, null);
    }

    private void initializeFileChooser() {
	// create FileChooser
	fc = new JFileChooser(Period04.getCurrentDirectory());
	fc.setFileFilter( new TimestringFileFilter() );
	//XFileFilter ff = new XFileFilter();
	//ff.setExtensionMode("timestring");
	//fc.addChoosableFileFilter(ff);
    }

    public void actionPerformed(ActionEvent evt) { 
	if (mainframe.isWorking()) { return; }
	Object s=evt.getSource();
	if (s==b_Import) { 
	    if (!checkForTimeString()) { return; } // user canceled import
	    if (Period04.projectGetSelectedPoints()!=0) {
		new XDialogTSImport(mainframe, this, true, null);
	    } else {
	        importTimeString(false, null);
	    }
	}
	else if (s==b_Append)       { importTimeString(true, null); }
	else if (s==b_Export)       { exportTimeString(); }
	else if (s==b_DisplayTable) { displayTSTable(); }
	else if (s==b_DisplayPlot)  { displayTSPlot(); }
	//--- popup menu
	if (s==iPop_SelectAll) {
	    Object popupsource = popupEvent.getSource();
	    int boxID = -1;
	    for (int i=0; i<4; i++) {
		if (popupsource==listbox[i]) {
		    boxID = i;
		}
	    }
	    if (boxID!=-1) {
		listbox[boxID].clearSelection();
		listbox[boxID].addSelectionInterval(
		    0, Period04.projectNumberOfNames(boxID));
		listbox[boxID].repaint(); // force a repaint
	    }
	}
	if (s==iPop_CombineSelected) {
	    Object popupsource = popupEvent.getSource();
	    int boxID = -1;
	    for (int i=0; i<4; i++) {
		if (popupsource==listbox[i]) {
		    boxID = i;
		}
	    }
	    if (boxID!=-1) {
		combineSubStrings(boxID);
	    }
	}
	if (s==iPop_Edit) {
	    Object popupsource = popupEvent.getSource();
	    for (int i=0; i<4; i++) {
		if (popupsource==listbox[i]) {
		    int index = -1;
		    index = listbox[i].locationToIndex(popupEvent.getPoint());
		    if (index!=-1) { editItemSettings(i, index); } 
		}
	    }
	} else {
	    for (int i=0; i<4; i++) { 
		if (s==b_Label[i])  { editHeading(i); } // check label-buttons
		if (s==b_Edit[i])   { 
		    editSettings(i);  // check edit-buttons or popup menu
		}
	    }
	}
    }
    
    public void createPopupMenu() {
	popup = new JPopupMenu();
	iPop_Edit      = new JMenuItem("Edit substring properties");
//	iPop_Delete    = new JMenuItem("Delete item");
	iPop_CombineSelected 
	               = new JMenuItem("Combine selected substrings");
	iPop_SelectAll = new JMenuItem("Select all substrings");
	iPop_Edit           .addActionListener(this);
//	iPop_Delete         .addActionListener(this);
	iPop_CombineSelected.addActionListener(this);
	iPop_SelectAll      .addActionListener(this);
        popup.add(iPop_Edit);
//      popup.add(iPop_Delete);
        popup.add(iPop_CombineSelected);
        popup.add(iPop_SelectAll);
	MouseListener popupListener = new PopupListener(popup);
	for (int i=0; i<4; i++) {
	    listbox[i].addMouseListener(popupListener);
	}
    }
    
    public void setCheckBoxesEnabled(boolean value) {
	checkReverseScale.setEnabled(value);
    }

    public void itemStateChanged(ItemEvent evt) { 
	Object s=evt.getSource();
	if (s==checkReverseScale) {
	    setReverseScale();
	    if (plot!=null) {
		plot.replot(true, true, false);
	    }
	}
    }

    /*
      public void valueChanged ( ListSelectionEvent event)
      {
      if (mainframe.isWorking( ))
      {
      return; 
      }
      
      //System.err.println( event.getFirstIndex( ) +" "+ event.getLastIndex( ) );
      
      if (! blockSelectionEvent && ! event.getValueIsAdjusting( ))
      {
      JList list = (JList) event.getSource( );

      int j_last = event.getLastIndex( );
      if (event.getLastIndex( )>list.getMaxSelectionIndex()) { j_last=list.getMaxSelectionIndex(); }

      for
	  ( int j = 0 //event.getFirstIndex( ) -> does not work, indices not correct!!
		; j <= list.getMaxSelectionIndex() //j_last
        ; ++j
        )
      {
        boolean selected = list.isSelectedIndex( j);
        IdValue selectedItem = (IdValue) ((JList) event.getSource( )).getModel( ).getElementAt( j);


        int boxID = selectedItem.attribute;
        int ID = selectedItem.ID;

		System.out.println(j+" "+selected+" "+ID);

        System.err.println
          ( Integer.toString( event.getFirstIndex( ))
          + " " + Integer.toString( j)
          + " " + Integer.toString( event.getLastIndex( ))
          + " sel " + Integer.toString( list.isSelectedIndex( j) ? 1 : 0)
          + " item " + Integer.toString( selectedItem.attribute)
          + " " + Integer.toString( selectedItem.getID( ))
          + " " + selectedItem.toString( )
          + " " + Integer.toString( selectedItem.ID)
          )
        ;

        Period04.projectSetIDNameSelect( boxID, ID, selected);

        listbox[boxID].repaint( ); // force a repaint
      }

      Period04.projectSelect( );   // select data
      updateSelected( );           // display the new values
      updateTSPlot( );             // update the timestring plot
      selectionChanged = true;     // set it changed
    }
  }
*/

    public void valueChanged(ListSelectionEvent evt) {
	if (mainframe.isWorking()) { return; }
	    if (!blockSelectionEvent && !evt.getValueIsAdjusting()) {
		for (int boxID=0; boxID<4; boxID++) {
		    for (int j=0; j<Period04.projectNumberOfNames(boxID); j++) {
			ListEntry tmp = retrieveListEntry(
			    boxID,(String)listboxModel[boxID].getElementAt(j));
			boolean selected = listbox[boxID].isSelectedIndex(j);
			Period04.projectSetIDNameSelect(boxID, tmp.id, selected);
		    }
		    listbox[boxID].repaint(); // force a repaint
		}
		Period04.projectSelect();   // select data
		updateSelected();           // display the new values
		updateTSPlot();             // update the timestring plot
		selectionChanged = true;    // set it changed
	    }
    }

    public boolean checkForTimeString() {
	if (Period04.projectGetTotalPoints()!=0) {
	    // ask if user wants to replace current timestring
	    int value = JOptionPane.showConfirmDialog(
		mainframe.getFrame(), "Do you really want to remove the old timestring?",
		"Remove old timestring?", JOptionPane.YES_NO_OPTION);
	    if (value == JOptionPane.YES_OPTION) {
		for (int boxID=0; boxID<4; boxID++) { 
		    // in order to prevent the program from crashing 
		    // stop to listen for list selection events
		    listbox[boxID].removeListSelectionListener(this);
		}
		
		//mainframe.newProject(/*nocheck=*/ true); // clean displays
		// proceed 
		return true;
	    } else { // JOptionPane.NO_OPTION
		return false; 
	    }
	}
	return true;
    }

    public void importTimeStringBatch(String file, String fileformat) {
	File [] files = new File[1];;
	files[0]=new File(file);
	//--- make progress bar
	mainframe.setReading(true);

	/*
	// without swingworker:
	int ptsbefore=Period04.projectGetTotalPoints();
	//--- replace "no data file(s)"
	mainframe.getTimeStringTab().addFileToFileBox(files[0].getPath());
	// import timestring
	//--- now load the timestring
	Period04.projectLoadTimeString(files[0].getPath(),fileformat,0,false);
	//--- write to protocol
	mainframe.getLog().writeProtocol(
					 "\nLoaded "+Period04.projectGetTotalPoints()+
					 " points of data from file: \n"+files[0].getPath()+
					 "\n (fileformat: " + fileformat + ")\n",true);
    
	//--- remove progress bar
	mainframe.setReading(false);
	mainframe.updateDisplays();
		
	//--- if fileformat contains u,w then activate weights!
	if (fileformat.indexOf("w")!=-1 || fileformat.indexOf("u")!=-1) {
	    Period04.projectSetUsePointWeight(true);
	    StringBuffer text = new StringBuffer();
	    text.append("New weight settings:\n"+
			"Weights are calculated ...\n");
	    text.append(
			"- using the "+
			"individual weight for each point\n");
	    Period04.projectSetUseWeightFlags(); // set flags
	    Period04.projectSelect();
	    mainframe.getLog().writeProtocol(text.toString(), true);
	}
	*/
	
	// with swingworker
	//--- make progress bar
	mainframe.setReading(true);
	//--- create a file task
	FileTask task = new FileTask(mainframe,FileTask.TIMESTRING_IN, files, false, fileformat);
	task.go();
	mainframe.startTimer(task);
	task.get();
    }

    public void importTimeString() {
	if (Period04.projectGetTotalPoints()!=0) {
	    new XDialogTSImport(mainframe, this, false, null);
	} else {
	    importTimeString(false, null);
	}
    }

	public void importVOTTimeString(URL url) {
		//--- if temporary file is sent via PLASTIC 
		//--- make a less temporary copy first
		try {
			String newname = System.getProperty("user.home")+
				System.getProperty("file.separator")+
				".period04-voimport";
			System.out.println(newname);
			String minurl = url.toString();
			FileChannel ic = new FileInputStream(minurl).getChannel();
			FileChannel oc = new FileOutputStream(newname).getChannel();
			ic.transferTo(0, ic.size(), oc);
			ic.close();
			oc.close();	
			url = (new File(newname)).toURL();
		} catch (IOException e) {
			System.err.println(e);
			return;
		}
		//---
		//--- ok, start reading
		//---
		if (Period04.projectGetTotalPoints()!=0) {
			new XDialogTSImport(mainframe, this, false, url);
		} else {
			mainframe.setReading(true);
			// create a file task
			File [] files = new File[1];
			files[0] = new File(url.getFile());
			FileTask task = new FileTask(mainframe,FileTask.TIMESTRING_IN, 
										 files, /*append=*/false);
			task.setVOTable(true);
			task.go();
			mainframe.startTimer(task);
		}
	}

	public void importVOTTimeString(StarTable table) {
		URL url = null;
		//--- table sent via SAMP, write to file and use standard import
		try {
			String newname = System.getProperty("user.home")+
				System.getProperty("file.separator")+
				".period04-voimport";
			StarTableWriter writer = new VOTableWriter();
			writer.writeStarTable(table, newname, new StarTableOutput());
			url = (new File(newname)).toURL();
		} catch (Exception e) {
			System.err.println(e);
			return;
		}
		//---
		//--- ok, start reading
		//---
		if (Period04.projectGetTotalPoints()!=0) {
			new XDialogTSImport(mainframe, this, false, url);
		} else {
			mainframe.setReading(true);
			// create a file task
			File [] files = new File[1];
			files[0] = new File(url.getFile());
			FileTask task = new FileTask(mainframe,FileTask.TIMESTRING_IN, 
										 files, /*append=*/false);
			task.setVOTable(true);
			task.go();
			mainframe.startTimer(task);
		}
	}

    public void importTimeString(boolean append, URL url) {
		if (url==null) {
			int returnVal;
			//--- show the import dialog
			initializeFileChooser();
			fc.setMultiSelectionEnabled(true);
			returnVal = fc.showOpenDialog(mainPanel);
			//--- 
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File [] file = fc.getSelectedFiles();  // get the file names
				//--- make progress bar
				mainframe.setReading(true);
				//--- create a file task
				FileTask task = new FileTask(mainframe,FileTask.TIMESTRING_IN, 
											 file, append);
				task.go();
				mainframe.startTimer(task);
			}
			fc.setMultiSelectionEnabled(false);
		} else {
			// create a file task
			File [] files = new File[1];
			files[0] = new File(url.getFile());
			mainframe.setReading(true);
			FileTask task = new FileTask(mainframe,FileTask.TIMESTRING_IN, 
								files, append);
			task.setVOTable(true);
			task.go();
			mainframe.startTimer(task);
		}
    }

    public void exportTimeString() {
	// check if there are enough points 
	if (Period04.projectGetSelectedPoints()==0) {
	    JOptionPane.showMessageDialog(mainframe.getFrame(),
		"Not enough points!", "Error",
		JOptionPane.ERROR_MESSAGE);
	    return;
	}
	// check if output format fits the user
	XDialogTSFileFormat dialog = 
	    new XDialogTSFileFormat(mainframe.getFrame(),
				    Period04.timestringGetOutputFormat(), 
				    "", false, false, false);
	// assign new output-format
	String outputformat = 
	    dialog.getFileFormat(); // returns "null" if canceled
	boolean votable = dialog.getUseVOTableFormat();
	if (outputformat==null) { return; }
	initializeFileChooser();
	int returnVal = fc.showSaveDialog(mainPanel);
	if (returnVal == JFileChooser.APPROVE_OPTION) {
	    File file = fc.getSelectedFile();
	    // check if the extension is missing
	    String name = file.getName();
	    if (name.indexOf(".")==-1) { 
			if (votable)
				file = new File(file.getPath()+".xml");			
			else
				file = new File(file.getPath()+".dat");
	    }
	    // check wether user wants to overwrite an existing file
	    if (file.exists()) {
		int v = JOptionPane.showConfirmDialog(
		    mainframe.getFrame(), "File does already exist!\n"+
		    "Do you really want to delete the old file?", 
		    "Warning", JOptionPane.YES_NO_OPTION);
		if (v == JOptionPane.NO_OPTION) {
		    return;
		}
	    }
	    Period04.setCurrentDirectory(file); 
	    mainframe.setSaving(true); // create a progress bar
	    // create a task for output
	    FileTask task = new FileTask(
		mainframe, votable, FileTask.TIMESTRING_OUT, file.getPath(),
		outputformat);
	    task.go();
	}
    }

    public File exportVOTTimeString() {
	// check if there are enough points 
	if (Period04.projectGetSelectedPoints()==0) {
	    JOptionPane.showMessageDialog(mainframe.getFrame(),
		"Not enough points!", "Error",
		JOptionPane.ERROR_MESSAGE);
	    return null;
	}
	// check if output format fits the user
	XDialogTSFileFormat dialog = 
	    new XDialogTSFileFormat(mainframe.getFrame(),
								Period04.timestringGetOutputFormat(), "", 
								/*ImportDialog=*/ false, 
								/*usePhase=*/     false, 
								/*forceVOExport=*/true);
	// assign new output-format
	String outputformat = 
	    dialog.getFileFormat(); // returns "null" if canceled
	if (outputformat==null) { return null; }
	//---
	//--- create a temporary file for transferring it to the other VO Appl.
	File file = null;
	try {
        file = File.createTempFile("p04-export-", ".dat");
        file.deleteOnExit();       // delete file when exiting period04
	    mainframe.setSaving(true); // create a progress bar
 	    // create a task for output
	    FileTask task = new FileTask(
		mainframe, true, FileTask.TIMESTRING_OUT, file.getPath(),
		outputformat);
	    task.go();
    } catch (IOException e) {
		System.err.println(e);
    }
	return file;
    }

    //---
    //--- edit the name of an attribute (= column header)
    //---
    public void editHeading(int i) {
	writeSelectionChanges(); // in case of changes write them to protocol 
	XDialogInput dialog = new XDialogInput(
	    mainframe.getFrame(), "Change name:", "New Name: ",
	    Period04.projectGetNameSet(i), 0 /*no numberchecks*/);
	
	String heading = dialog.getValidatedText();
	if (heading != null) { // the text is valid.
	    // write changes into the log
	    mainframe.getLog().writeProtocol("\nRenamed the attribute heading:  from \""
				  +Period04.projectGetNameSet(i)+
				  "\" to \""+heading+"\"\n", true);
	    Period04.projectChangeHeading(i,heading);
	    b_Label[i].setText(heading);  // change it on the gui
	}
    }

    //---
    //--- edit the settings for a list
    //---
    public void editSettings(int i) {
	// find active settings...
	int [] selection = listbox[i].getSelectedIndices();
	int selected = selection.length;
	if (selected==0) {
	    JOptionPane.showMessageDialog(
		mainframe.getFrame(), "Nothing selected..." );
	    return;
	}
	// loop through all selected entries...
	for (int j=0; j<selected; j++) {
	    if (!editItemSettings(i, selection[j]))
		return;
	}
    }

    //---
    //--- edit the settings for one list item
    //---
    public boolean editItemSettings(int i, int index) {
	writeSelectionChanges(); // in case of changes write them to protocol 
	// retrieve string from list
	String tmp = (String)
	    listboxModel[i].getElementAt(index);
	
	// now get the appropriate listentry object
	ListEntry myentry = retrieveListEntry(i, tmp);
	// get ID of the string
	int ID = myentry.id;
	
	// create strings
	String s_name   = Period04.projectGetIDNameStr(ID,i);
	String s_weight = Period04.projectGetIDNameWeight(ID,i);
	String s_color  = Period04.projectGetIDNameColorStr(ID,i);
	    
	// create the dialog
	XDialogEdit dialog = new XDialogEdit(
	    mainframe.getFrame(), s_name, s_weight, s_color);
	if (dialog.isCanceled()) { // user canceled dialog, return
	    return false; 
	}

	// change settings
	Period04.projectChangeName(ID, i, dialog.getName(),
				   dialog.getWeight(), dialog.getColor());
	// write to protocol
	mainframe.getLog().writeProtocol(
	    "\nChanged name (weights, and color) in "+
	    Period04.projectGetNameSet(i)+":\n"+"from "+s_name+" ("+
	    s_weight+","+s_color+") to "+dialog.getName()+" ("+
	    dialog.getWeight()+","+dialog.getColor()+")\n" ,true);
	
	Period04.projectCalcWeights();
	updateDisplay();
	updateTSPlot();
	return true;
    }

    //---
    //--- do we have to revers the y-axis?
    //---
    public void setReverseScale() {
	writeSelectionChanges(); // in case of changes write them to protocol 
	int selected = 0;
	if (checkReverseScale.isSelected()) { selected = 1; }
	Period04.projectSetReverseScale(selected);
	// write to protocol
	if (selected==1) {
	    mainframe.getLog().writeProtocol("The time string is in magnitudes\n", true);
	} else { 
	    mainframe.getLog().writeProtocol("The time string is in intensity\n", true);
	}
    }

    public void subdivideTimeString() {
	writeSelectionChanges(); // in case of changes write them to protocol
	//---
	//--- create a dialog
	//---
	final JDialog dialog        = new JDialog(mainframe.getFrame(), "Subdivide data");
	final JRadioButton rb_gaps  = new JRadioButton("Subdivide by gaps");
	final JRadioButton rb_blocks= new JRadioButton(
	    "Subdivide by fixed intervals");
	JPanel panel = new JPanel();
	JLabel l_sub = new JLabel("What kind of subdivision?");
	JButton b_ok = new JButton("Ok");
	b_ok.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    dialog.dispose();
		    if (rb_gaps.isSelected()) { 
			new XDialogSubdivideGaps(
			    mainframe, XTabTimeString.this);
		    } else if (rb_blocks.isSelected()) { 
			new XDialogSubdivideBlocks(
			    mainframe, XTabTimeString.this); 
		    }
		    updateTSPlot();
		}
	    });
	ButtonGroup bg = new ButtonGroup();
	bg.add(rb_gaps); bg.add(rb_blocks);
	rb_gaps.setSelected(true);
	//---
	//--- set the layout, add the panel to the dialog and show it
	//---
	panel.setLayout(null);
	l_sub    .setBounds(20, 10,200, 20);
	if (Period04.isMacOS) {
		rb_gaps  .setBounds(30, 30,240, 20);
		rb_blocks.setBounds(30, 50,240, 20);
		b_ok     .setBounds(90, 80, 60, 23);
	} else {
		rb_gaps  .setBounds(30, 30,200, 20);
		rb_blocks.setBounds(30, 50,200, 20);
		b_ok     .setBounds(90, 80, 60, 23);
	}
	panel.add(l_sub);
	panel.add(rb_gaps);
	panel.add(rb_blocks);
	panel.add(b_ok);
	dialog.getContentPane().add(panel);
	if (Period04.isMacOS) 
		dialog.setSize(320, 155);
	else
		dialog.setSize(250, 155);
	dialog.setLocationRelativeTo(mainframe.getFrame());
	dialog.setVisible(true);
    }

    public void combineSubStrings(int attribute) {
	writeSelectionChanges(); // in case of changes write them to protocol 
	new XDialogCombineSubstrings(mainframe, this, attribute);
	updateTSPlot();
    }

    public void showTimes() {
	writeSelectionChanges(); // in case of changes write them to protocol 
	new XDialogShowTimes(mainframe, this);
    }

    public void adjustTimeString() {
	writeSelectionChanges(); // in case of changes write them to protocol 
	Period04.projectCalculateAverage();
	new XDialogAdjustData(mainframe, this);
    }

    public void restoreZeropoints() {
	writeSelectionChanges(); // in case of changes write them to protocol 
	mainframe.getLog().writeProtocol(Period04.projectMakeObservedAdjusted(), true);
    }

    public void deleteDefaultLabel() {
	writeSelectionChanges(); // in case of changes write them to protocol 
	new XDialogDeleteDefaultLabel(mainframe, this);
    }

    public void deleteSelectedPoints() {
	int value = JOptionPane.showConfirmDialog(
	    mainframe.getFrame(), "Do you really want to delete the selected points?",
	    "Delete points", JOptionPane.YES_NO_OPTION);
	if (value==JOptionPane.YES_OPTION) {
	    mainframe.getLog().writeProtocol("Deleted selected points\n",true);
	    writeSelectionToProtocol();
	    Period04.projectDeleteSelectedPoints();
	    for (int i=0; i<4; i++) {
		updateList(i);
	    }
	    updateSelected();
	    updateTSPlot();
	}
    }
	    
    public void writeSelectionToProtocol() {
	StringBuffer buffer = new StringBuffer();
	for (int i=0;i<4;i++) {
	    String s_attribute = Period04.projectGetNameSet(i);
	    //--- due to optical reasons fill with empty spaces
	    int emptyspaces = 11-s_attribute.length();
	    if (emptyspaces<1) emptyspaces=1;
	    char [] s_rest = new char[emptyspaces];
	    for (int j=0; j<emptyspaces; j++) { s_rest[j]=' '; }
	    String s_tmp = 
		"Selected from "+s_attribute+":"+new String(s_rest);
	    int length = s_tmp.length();        // length of the line
	    buffer.append(s_tmp);               // append string to buffer
	    int ent=Period04.projectNumberOfNames(i);
	    int count=0;
	    for (int j=0;j<ent;j++) {
		if (listbox[i].isSelectedIndex(j)) {
		    if (!(count==0)) { buffer.append(", "); length+=2; }
		    s_tmp = Period04.projectGetIndexNameStr(i,j);
		    length += s_tmp.length(); 
		    //--- make no lines longer than 2046 characters
		    if (length>=2046) {
			String t = "\n                          ";
			buffer.append(t);
			length = t.length();    // reset length of line
		    }
		    buffer.append(s_tmp);       // append string to buffer
		    count++;
		}
	    }
	    buffer.append("\n");
	}
	buffer.append(Period04.projectGetSelectedPoints()+
		      " out of "+Period04.projectGetTotalPoints()+
		      " points selected\n");
	mainframe.getLog().writeProtocol(buffer.toString(), false);
        resetSelectionChanged();
    }

    public void writeSelectionChanges() {
	if (selectionChanged) {
	    mainframe.getLog().writeProtocol("New Selection:", true);
	    writeSelectionToProtocol();
	}
    }
    
    public boolean hasSelectionChanged() {
	return selectionChanged;
    }

    public void resetSelectionChanged() {
	selectionChanged=false;
    }
    
    public void updateDisplay() {
	l_TotalPoints.setText( 	                  // get total points
	    new Integer(Period04.projectGetTotalPoints()).toString() );
	
	updateFileBox();
	updateSelected();
	
	// check if we are using magnitudes or not
	checkReverseScale.setSelected(Period04.projectGetReverseScale());
	
	// now set the lists
	for (int boxID=0; boxID<4; boxID++) {
	    updateList(boxID);   // fill the list
	    // set the heading
	    b_Label[boxID].setText(Period04.projectGetNameSet(boxID)); 
	}
    }
    
    public void updateSelected() {
	//--- display new number of selected points
	int points = Period04.projectGetSelectedPoints();
	l_SelectedPoints.setText(new Integer(points).toString());
	points = Period04.projectGetTotalPoints();
	l_TotalPoints.setText(new Integer(points).toString());
	//--- display new start and end time
	if (points>0) { 
	    l_StartTime.setText( Period04.projectGetStartTime() );
	    l_EndTime.setText( Period04.projectGetEndTime() );
	} else {                            
	    l_StartTime.setText("no points");
	    l_EndTime.setText("no points");
	}
	mainframe.updateNyquist();   // update Nyquist freq in the Fourier tab
	mainframe.updateMenu();      // update menu 
    }

    public void updateFileBox() { // only to be called from
	String txt = Period04.timestringGetFileName();
	if (txt.equals("")) { fileLog.setText("no data file(s)"); }
	else                { fileLog.setText(txt); }
    }

    public void addFileToFileBox(String file) {
	if (Period04.projectGetTotalPoints()==0) { // first file in box
	    fileLog.setText(file);
	} else { // here one really appends the timestring
	    fileLog.append(",\n"+file);
	}
    }

    public void cleanFileBox() {
	fileLog.setText("no data file(s)");  
    }

    class ListEntry {
	public int id;
	public int select;
	public String string;
	public int getID() { return id; } // sinnvoll?? id ist ja schon public
    }

    public void cleanLists() {
	for (int boxID=0; boxID<4; boxID++) {
	    listbox[boxID].removeListSelectionListener(this); 
	    listboxModel[boxID].clear(); //listbox[boxID].repaint();
	    listbox[boxID].addListSelectionListener(this); 
	}
    }

    /*
  public void updateList( int boxID)
  {
 
    // remove the listselectionlistener in order to prevent unwanted 
    // listselectionevents while updating the list
    listbox[boxID].removeListSelectionListener( this); 

    listboxModel[boxID].clear( );

    int entries = Period04.projectNumberOfNames( boxID);

    // add entries to list
    for ( int j = 0; j < entries; j++)
    {
      IdValue idValue = new IdValue
        ( j
        , Period04.projectGetIndexNameStr( boxID, j)
        )
      ;

      // We store temporarily all the other stuff here too.
      // Till I found a way to get rid of it.
      idValue.attribute = boxID;
      idValue.ID = Period04.projectGetIndexNameID( boxID, j);

//      listboxModel[boxID].add( j, idValue);
      listboxModel[boxID].addElement( idValue);
    }

    // set selection
    listbox[boxID].clearSelection( );

    for ( int j = 0; j < entries; j++)
    {
      if (0 != Period04.projectGetIndexNameSelect
          ( boxID
          , ((IdValue) listboxModel[boxID].getElementAt( j)).getID( )
          )
        )
      {
	  
       listbox[boxID].addSelectionInterval( j, j);
      } 
    }

    selectionChanged = true;    // set it changed

    // finally add the ListSelectionListener
    listbox[boxID].addListSelectionListener( this);
    listbox[boxID].repaint( ); // force a repaint
    listbox[boxID].repaint( );
  }

    */


    public void updateList(int boxID) {
	// remove the listselectionlistener in order to prevent unwanted 
	// listselectionevents while updating the list
	listbox[boxID].removeListSelectionListener(this); 
	
	int j;
	listboxModel[boxID].clear();
	int entries = Period04.projectNumberOfNames(boxID);

	// create sorted table
	array[boxID] = new ArrayList();
	ArrayList stringarray = new ArrayList();
	for (j=0;j<entries;j++) {
	    ListEntry entry = new ListEntry();
	    entry.string = Period04.projectGetIndexNameStr(boxID,j);
 	    entry.id     = Period04.projectGetIndexNameID(boxID,j);
	    entry.select = Period04.projectGetIndexNameSelect(boxID,j);
	    array[boxID].add(entry);
	    stringarray.add(entry.string);
	}
	// sort array
	Collections.sort(stringarray); 	

	// add entries to list
	for (j=0; j<entries; j++) {
	    listboxModel[boxID].add( j, (String)stringarray.get(j) );
	}

	// set selection
	listbox[boxID].clearSelection();
	for (j=0; j<entries; j++) {
	    ListEntry tmp = retrieveListEntry(
		boxID, ((String)stringarray.get(j)));
	    if (tmp.select!=0) {
		listbox[boxID].addSelectionInterval(j,j);
	    } 
	}

	// finally add the ListSelectionListener
	listbox[boxID].addListSelectionListener(this);
	listbox[boxID].repaint(); // force a repaint
	listbox[boxID].repaint();
    }

    /*
      public int retrieveListEntryID ( int boxID, int index)
      {
      return ((IdValue) listboxModel[boxID].getElementAt( index)).ID;
      }
    */
    
    public ListEntry retrieveListEntry(int boxID, String myentry) {
	for (int i=0; i<array[boxID].size(); i++) {
	    ListEntry tmp = (ListEntry)array[boxID].get(i);
	    if (tmp.string==myentry) {
		return tmp;
	    }
	}
	// list does not contain such an element
	System.err.println(
	    "XTabTimeString::retrieveItem: no such list member!");
	return null;
    }

    public int retrieveListEntryID(int boxID, int index) {
	// retrieve string from list
	String s_entry = (String)listboxModel[boxID].getElementAt(index);
	// now search the appropriate listentry object and return its ID
	for (int i=0; i<array[boxID].size(); i++) {
	    ListEntry tmp = (ListEntry)array[boxID].get(i);
	    if (tmp.string==s_entry) {
		return tmp.id;
	    }
	}
	return -1;
    }

    public ListEntry getListEntry(int boxID, int entry) {
	try {
	    return (ListEntry)array[boxID].get(entry);
	} catch (Exception e) {
	    System.err.println(
		"XTabTimeString::getListEntry: no such list member!");
	}
	return null;
    }

    public void displayTSTable() {
	new XTSTable(mainframe);
    }

    public void displayTSPlot() {
	if (plot!=null) { // there is already an active plot!
	    plot.toFront(); return;
	}
	plot = new XPlotTimeString(mainframe); // show plot
    }

    public XPlotTimeString getTSPlot() {
	if (plot!=null) {
	    return plot;
	}
	return null;
    }

    public void updateTSPlot() {
	if (plot!=null) {
	    plot.replot(true, true, false);
	}
    }
    
    public void dereferenceTSPlot() {
	plot = null;
    }

    //---
    //--- resize the components to fit the new frame size
    //---
    public void fitSize() {
	correct = mainframe.getSizeDiff(); 
	if (Period04.isMacOS) {
		correct -= 20;
	    for (int i=0; i<4; i++) {
		listScrollPane[i].setBounds( 10+i*125, 190, 115, 350-correct );
		b_Edit[i]        .setBounds( 10+i*125, 540-correct, 115, 20 );
	    }
	    b_DisplayTable.setBounds( 10, 570-correct, 240, 20 );
	    b_DisplayPlot.setBounds( 260, 570-correct, 240, 20 );
	} else {
	    for (int i=0; i<4; i++) {
		listScrollPane[i].setBounds( 10+i*120, 190, 110, 350-correct );
		b_Edit[i]        .setBounds( 10+i*120, 540-correct, 110, 20 );
	    }
	    b_DisplayTable.setBounds( 10, 570-correct, 230, 20 );
	    b_DisplayPlot.setBounds( 250, 570-correct, 230, 20 );
	}
	mainPanel.revalidate();
	mainframe.getFrame().repaint();
    }

    //---
    //--- for the attributelist-popup-menu:
    //---
    class PopupListener extends MouseAdapter {
        JPopupMenu popup;

        PopupListener(JPopupMenu popupMenu)       { popup = popupMenu; }
        public  void mousePressed(MouseEvent e)   { maybeShowPopup(e); }
        public  void mouseReleased(MouseEvent e)  { maybeShowPopup(e); }
        private void maybeShowPopup(MouseEvent e) {
            if (e.isPopupTrigger()) {
                popup.show(e.getComponent(), e.getX(), e.getY());
		popupEvent = e;
            }
        }
    }
}


