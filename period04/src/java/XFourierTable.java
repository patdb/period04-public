/*----------------------------------------------------------------------*
 *  XFourierTable 
 *  displays a table with fourier spectrum data
 *----------------------------------------------------------------------*/

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import java.util.StringTokenizer;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.filechooser.*;
import javax.swing.table.*;

import javax.print.*;
import javax.print.attribute.*;

class XFourierTable extends JFrame
{
    private static final long serialVersionUID = -7080937852248872575L;
    private JPanel listPanel;
    private JMenuBar  menuBar = new JMenuBar();
    private JMenu     menuFile, menuHelp;
    private JMenuItem i_Print, i_Export, i_Close;
    private JFileChooser fc;
    private XProjectFrame mainframe;
    private MyTableModel model;
    private JTable    table;

    public XFourierTable(XProjectFrame mframe) {
	mainframe = mframe;

	// create main-menu
	menuFile = XMenuHelper.addMenuBarItem( menuBar, "Table" );
	menuHelp = XMenuHelper.addMenuBarItem( menuBar, "Help" );

	// create action listener for menu items
	ActionListener actionProcCmd = new ActionListener() {
		public void actionPerformed( ActionEvent evt ) {
		    Object s=evt.getSource();
		    if (s==i_Print)        { printFourierTable();  return; }
		    else if (s==i_Export)  { exportFourierTable(); return; }
		    else if (s==i_Close)   { dispose();            return; }
		}
	    };

	// file menu	
	i_Print = XMenuHelper.addMenuItem( 
	    menuFile, "Print Table", actionProcCmd );
	i_Export = XMenuHelper.addMenuItem( 
	    menuFile, "Export Table", actionProcCmd );
	i_Close = XMenuHelper.addMenuItem( 
	    menuFile, "Close", actionProcCmd );
	this.setJMenuBar( menuBar );

	// help menu

        model = new MyTableModel();
	table              = new JTable(model);

	JTableHeader header = table.getTableHeader();
	header.setReorderingAllowed(false);
	header.setBackground(new Color(230,225,217));
	header.setFont(new Font("Dialog", Font.BOLD, 11));

	// set column width
	table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	TableColumnModel colModel = table.getColumnModel();
	for (int i=0; i<table.getColumnCount(); i++) {
	    colModel.getColumn(i).setPreferredWidth(150);
	}

 	table.setShowHorizontalLines(true);
	table.setFont(new Font("Dialog", Font.PLAIN, 11));
        table.setPreferredScrollableViewportSize(new Dimension(150*3,410) );
	table.setBounds( 0, 18, 150*table.getColumnCount(), 
			 table.getRowHeight()*table.getRowCount()+30 );

        //Create the scroll pane and add the table to it. 
        JScrollPane scrollPane = 
	    new JScrollPane(table,
			    JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, 
			    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        //Add the scroll pane to this window.
        this.getContentPane().add(scrollPane, null);

	this.setTitle(Period04.projectGetFourierActiveTitle());
	this.setSize(490,410);
	this.setLocationRelativeTo(mainframe.getFrame());
        this.setVisible(true);
    }

    private void exportFourierTable() {
		// start export dialog
		initializeFileChooser();
		int returnVal = fc.showSaveDialog(mainframe.getFrame());
		if (returnVal == JFileChooser.APPROVE_OPTION) {
		    File file = fc.getSelectedFile();
		    Period04.setCurrentDirectory(file); 
		    String path = file.getPath();
		    if (!path.endsWith(".fou")) { // add extension
			path += ".fou";
		    }
		    // check wether user wants to overwrite an existing file
		    if (file.exists()) {
			int v = JOptionPane.showConfirmDialog(
			    mainframe.getFrame(), "File does already exist!\n"+
			    "Do you really want to delete the old file?", 
			    "Warning", JOptionPane.YES_NO_OPTION);
			if (v == JOptionPane.NO_OPTION) {
			    return;
			} else {
			    JOptionPane.showMessageDialog(
				mainframe.getFrame(), "The project has been saved in file\n"+
				path, "", JOptionPane.INFORMATION_MESSAGE);
			    // go on and save it
			}
		    }
		    // now go and save it
		    Period04.projectSaveFourier(
			Period04.projectGetFourierActiveIndex(), path);
    	}
	}   
    
    private void printFourierTable() {
	StringBuffer text = new StringBuffer();
	text.append("Frequency               Amplitude               Power\n");
	text.append("------------------------------------------------------------------\n");
	for (int i=0; i<model.getRowCount(); i++) {
	    text.append(model.getValueAt(i,0)+"\t");
	    text.append(model.getValueAt(i,1)+"\t");
	    text.append(model.getValueAt(i,2)+"\n");
	}

	// print a simple ascii text
	DocFlavor flavor = DocFlavor.STRING.TEXT_PLAIN;
	Doc doc = new SimpleDoc(text.toString(), flavor, null);
	PrintRequestAttributeSet set = new HashPrintRequestAttributeSet(); 
	PrintService dps = PrintServiceLookup.lookupDefaultPrintService();
	DocPrintJob job = dps.createPrintJob();
	try { job.print(doc, set); }
	catch (Exception e) {
	    System.out.println("printing failed");
	}
    }
    
    
  private void initializeFileChooser ( )
  {
    // create FileChooser
    fc = new JFileChooser( Period04.getCurrentDirectory( ));
    fc.setFileFilter( new FourierFileFilter( ));
  }


    class MyTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 8028893267779947003L;
        final Object[]   columnNames = {"Frequency","Amplitude","Power"};
 	final int        columns = 3;
	final int        rows = Period04.projectGetFourierActivePoints();
	final Object[][] data = new Object[rows][columns];

	public MyTableModel() {
	    readData();
	}

        public int getColumnCount() {
            return columns;
        }
        
        public int getRowCount() {
            return rows;
        }

        public String getColumnName(int col) {
            return columnNames[col].toString();
        }

        public Object getValueAt(int row, int col) {
            return data[row][col];
        }

        public boolean isCellEditable(int row, int col) {
	    return false;
        }

	public void readData() {
	    // get the data
	    for (int j=0; j<columns;j++) {	    
		Object [] datacolumn = Period04.fourierTableGetData(j);	
		for (int i=0; i<rows; i++) {
		    this.data[i][j] = datacolumn[i];
		}
	    }	
	}
    }
}


