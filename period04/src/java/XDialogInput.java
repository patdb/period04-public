/*----------------------------------------------------------------------*
 *  XDialogInput
 *  a dialog for input of text or numbers
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.*;         // Property change stuff
import javax.swing.*;


class XDialogInput extends JDialog {

    private static final long serialVersionUID = -3296492113831597225L;
    private String typedText = null;
    private int checknumber;    // 0=nocheck, 1=check, 2=check+non-negative
    private JOptionPane optionPane;
    private boolean cancel = false;

    public boolean isCanceled() {
	return cancel;
    }

    public String getValidatedText() {
        return typedText;
    }

    public Double getValidatedNumber() {
	if (typedText==null) { return null; }
	return new Double(typedText);
    }

    public Integer getValidatedIntegerNumber() {
	if (typedText==null) { return null; }
	return new Integer(typedText);
    }

    public double getValidatedNonNegativeNumber() {
	return Double.parseDouble(typedText);
    }

    public XDialogInput(JDialog dialog, String title,
			String text, String oldname, int numbercheck) {
        super(dialog, true);
		createDialog(title, text, oldname, numbercheck);
		this.pack();
		this.setResizable(false);
		this.setLocationRelativeTo(dialog);
		this.setVisible(true);
	}

    public XDialogInput(JFrame mainframe, String title,
			String text, String oldname, int numbercheck) {
        super(mainframe, true);
		createDialog(title, text, oldname, numbercheck);
		this.pack();
		this.setResizable(false);
		this.setLocationRelativeTo(mainframe);
		this.setVisible(true);
	}

    private void createDialog(String title, String text, 
							  String oldname, int numbercheck) {
		checknumber = numbercheck;
        typedText = oldname;
        setTitle(title);

        final String message = text;
        final JTextField textField = new JTextField(typedText,10);
        Object[] array = {message, textField};

        final String btnOK_s = "OK";
        final String btnCancel_s = "Cancel";
        Object[] options = {btnOK_s, btnCancel_s};

        optionPane = new JOptionPane(array, JOptionPane.PLAIN_MESSAGE,
				     JOptionPane.YES_NO_OPTION, null,
				     options, options[0]);
        setContentPane(optionPane);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent we) {
                /*
                 * Instead of directly closing the window,
                 * we're going to change the JOptionPane's
                 * value property.
                 */
                    optionPane.setValue(new Integer(
                                        JOptionPane.CLOSED_OPTION));
            }
        });

        textField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                optionPane.setValue(btnOK_s);
            }
        });

        optionPane.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
                String prop = e.getPropertyName();

                if (isVisible() 
                 && (e.getSource() == optionPane)
                 && (prop.equals(JOptionPane.VALUE_PROPERTY) ||
                     prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
                    Object value = optionPane.getValue();

                    if (value == JOptionPane.UNINITIALIZED_VALUE) {
                        //ignore reset
                        return;
                    }

                    // Reset the JOptionPane's value.
                    // If you don't do this, then if the user
                    // presses the same button next time, no
                    // property change event will be fired.
                    optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

                    if (value.equals(btnOK_s)) {
			typedText = textField.getText();
			String text = typedText;
			   
			if (checknumber!=0) {
			    // check wether this is a valid number
			    NumberFormatException nan = null;
			    try { Double.parseDouble(typedText); } 
			    catch (NumberFormatException exception) { 
				nan=exception; 
			    }
			    if ( text.equals("") || (nan!=null) ) {
				// text was no number
				JOptionPane.showMessageDialog(
				    XDialogInput.this, "Sorry, \""+ 
				    typedText+"\" "+
				    "isn't a valid number.\n", "Try again",
				    JOptionPane.ERROR_MESSAGE);
				typedText = null;
				return;
			    } 	
			    if (checknumber==2) {
				if  (Double.parseDouble(typedText)<=0.0) {
				    // text was no non-negative number
				    JOptionPane.showMessageDialog(
					XDialogInput.this, "Sorry, \""+ 
					typedText+"\" "+
					"isn't a valid number.\n"+
					"Only non-negative numbers allowed!", 
					"Try again", JOptionPane.ERROR_MESSAGE);
				    typedText = null;
				    return;
				}
			    }			   
			}
			if (text.equals("")) { 
			    // this checks if the typed string (which is
			    // no number) is a valid string.
			    textField.selectAll();
			    JOptionPane.showMessageDialog(
				XDialogInput.this, "Sorry, \"" +typedText+"\" "
				+ "isn't a valid name.\n", "Try again",
				JOptionPane.ERROR_MESSAGE);
			    typedText = null;
			} 
			// we're done; dismiss the dialog
			setVisible(false);
			cancel = false;
		    } 
		    else { // user closed dialog or clicked cancel
			typedText = null;
			cancel = true;
			setVisible(false);
		    }
                }
            }
        });
	
    }
}

