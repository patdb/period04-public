/*----------------------------------------------------------------------*
 *  FitTask
 *  starts a task for a long least-squares fit
 *----------------------------------------------------------------------*/

import javax.swing.JOptionPane;

public class FitTask extends Task {

    private XTabFit tab;
    private XProjectFrame mainframe;
    private int datamode, datamode_alt, improvementMode;
    // for amplitude variation:
    private int varmode, attribute, shots;
    private int [] selectedindices;

    public FitTask(XProjectFrame mainframe, XTabFit tab, int datamode, 
		   int improvementMode) {
	this.mainframe       = mainframe;
	this.tab             = tab;
	this.datamode        = datamode;
	this.improvementMode = improvementMode;
    }

    public FitTask(XProjectFrame mainframe, XTabFit tab, int datamode, 
		   int improvementMode, int shots) {
	this.mainframe       = mainframe;
	this.tab             = tab;
	this.datamode        = datamode;
	this.improvementMode = improvementMode;
	this.shots           = shots;
    }

    public FitTask(XProjectFrame mainframe, XTabFit tab, int datamode, 
		   int improvementMode, int varmode, int attribute, 
		   int [] selectedindices) {
	this.mainframe       = mainframe;
	this.tab             = tab;
	this.datamode        = datamode;
	this.improvementMode = improvementMode;
	//--- the following variables are for amplitude variations
	this.varmode         = varmode;
	this.attribute       = attribute;
	this.selectedindices = selectedindices;
    }

    //---
    //--- Called from XTabFourier to start the task:
    //---
    public void go() {
	worker = new SwingWorker() {
		public Object construct() {
		    done        = false;
		    canceled    = false;
		    return new ActualFitTask();
		}
	    };
        worker.start();
    }

    public void get() {
	worker.get();
    }

    //---
    //--- the actual long running task. this runs in a SwingWorker thread.
    //---
    class ActualFitTask {
        ActualFitTask() {
	    while (!isCanceled() && !isDone()) {
		//--- if the timestring selection has changed then
		//--- reset the lambda value
		if (mainframe.getTimeStringTab().hasSelectionChanged()) {
		    Period04.projectResetLambda();
		}
		// now go and calculate
		String protocol = "";
		switch (improvementMode) {
		    case XTabFit.AMP_PHA:
			protocol = Period04.projectCalculatePeriod(datamode); 
			break;
		    case XTabFit.FRE_AMP_PHA:
			protocol = Period04.projectImprovePeriod(datamode); 
			break;
		    case XTabFit.SPECIAL:
			protocol = Period04.projectImproveSpecialPeriod(
			    datamode); 
			break;
		    case XTabFit.AMP_VAR:
			protocol = Period04.projectCalculateAmpVar(
			    datamode, varmode, attribute, selectedindices,
			    selectedindices.length);
			tab.updateAmpVarDialog(protocol);
			break;
		    case XTabFit.BM_START:
			protocol = Period04.projectSearchPTSStartValues(
			    datamode, shots);
			break;
		    case XTabFit.BM_VAR:
			protocol = Period04.projectImprovePTS(datamode);
			break;
		}
		done = true;
		if (isCanceled()) {
		    JOptionPane.showMessageDialog(
			mainframe.getFrame(), "The results may not be reliable!",
			"Canceled calculation",
			JOptionPane.INFORMATION_MESSAGE);
		}
		//--- remove the calculating panel
		mainframe.setCalculating(false, null ,"", 0, false); 
		//--- update the display
		tab.updateDisplay(false);
		//--- in case of an error calculation, reset default state
		if (Period04.projectIsCalcErrors()) {
		    tab.resetErrorCalculation();
		}
		//--- update protocol
		mainframe.getLog().writeProtocol(protocol, true);
		//--- finalize
		tab.dereferenceTask();
		//---
		//--- update fit in time string plot
		//---
		XPlotTimeString plot = mainframe.getTimeStringTab().getTSPlot();
		if (plot!=null) {
		    plot.replotFit();//(true, true, false);
		}
	    }
	}
    }
}
