/*----------------------------------------------------------------------*
 *  XDialogTSFileFormat
 *  a dialog that asks the user to specify the columns of the 
 *  time-string file to be loaded (or exported)
 *
 *  TODO: for import and export there are different format labels for
 *        Pnt.weight, ... -> generalize it!!
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.*;      //Property change stuff 
import java.io.*;        
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.event.*;
import javax.swing.table.*;


class XDialogTSFileFormat extends XDialog 
    implements ActionListener
{
    private static final long serialVersionUID = -9168131596155727033L;

    private JOptionPane  optionPane;
    private JPanel       listPanel;
    private JComboBox [] box;
	private JComboBox    cb_format;
    private String       formatstring;
	private boolean      votable;
    private int          rows = 20;     // number of lines to be read in
    private int          columns = 17;  // this value is valid only for export
    private JLabel    [] l_columns;
    private JTable       table; 
    private JList        list;
    private DefaultListModel lmexport;
    private String       filename;
    private final boolean io;
    private boolean       phase;
    private final String message;
	private boolean      forcedVOExport;

    public XDialogTSFileFormat(JFrame mainframe, 
			       String formatOffered,
			       String fname,
			       boolean value,   // true=import, false=export
			       boolean usePhase,   // true=phase, false=time
			       boolean forceVOExport) // true = only VO option is shown
    {    
        super(mainframe, true);
	io       = value;
	phase    = usePhase;
	forcedVOExport = forceVOExport;
	filename = fname;
	if (io) { 
	    message =
		"<html>The file '"+(new File(filename)).getName()+
		"' contains "+formatOffered.length()+
		" columns.<br>Please specify an appropriate attribute"+
		" for each of the columns in your data file.<br>"+
		"To ignore a certain column select \"Ignore\".<br></html>";
	    initializeImport(formatOffered);
	} else {
	    message = "Please select the columns you want to export:\n";
	    initializeExport(formatOffered);
	}	

	this.setLocationRelativeTo(mainframe);
	this.setVisible(true);
    }


    private void initializeImport(String formatOffered) 
    {
	columns = formatOffered.length();
	String [] labels = {"Time", "Observed", "Adjusted", "Calculated", 
						"Pnt.weight", "Pnt.error",
						"Residuals(Obs)", "Residuals(Adj)", 
						Period04.projectGetNameSet(0), 
						Period04.projectGetNameSet(1),
						Period04.projectGetNameSet(2),
						Period04.projectGetNameSet(3), 
						"Weight("+Period04.projectGetNameSet(0)+")", 
						"Weight("+Period04.projectGetNameSet(1)+")", 
						"Weight("+Period04.projectGetNameSet(2)+")", 
						"Weight("+Period04.projectGetNameSet(3)+")", 
						"Ignore"};

	listPanel = new JPanel();	   // create the panel
	listPanel.setLayout(null);
	listPanel.setBackground(new Color(255,255,255));  //=WHITE

	l_columns = new JLabel[columns];

	// create table 
        MyTableModel model = new MyTableModel();
        table = new JTable(model);
	// set column width
	table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	TableColumnModel colModel = table.getColumnModel();
	for (int i=0; i<table.getColumnCount(); i++) {
	    colModel.getColumn(i).setPreferredWidth(120);
	}
 	table.setShowHorizontalLines(false);
	table.setFont(new Font("Dialog", Font.PLAIN, 11));
	table.setBounds( 0, 40, 120*table.getColumnCount(), 300 );
	listPanel.add( table, null );

	// set the offered format 
	box = new JComboBox[columns];
	for (int i=0; i<columns; i++) {
	    box[i] = createComboBox(labels);	    // create comboboxes
	    switch(formatOffered.charAt(i))	    // set selected item
	    {
		case 't': // time
		    box[i].setSelectedIndex(0); break;
		case 'o': // magnitude
		    box[i].setSelectedIndex(1); break;
		case 'a': // adjusted
		    box[i].setSelectedIndex(2); break;
		case 'c': // calculated
		    box[i].setSelectedIndex(3); break;	   
		case 'p': // point weight
		    box[i].setSelectedIndex(4); break;
		case 'u': // point error
		    box[i].setSelectedIndex(5); break;
		case 'r': // Data Residuals
		    box[i].setSelectedIndex(6); break;
		case 'R': // Adjusted Residuals
		    box[i].setSelectedIndex(7); break;
		case '1': // name 1
		    box[i].setSelectedIndex(8); break; 
		case '2': // name 2
		    box[i].setSelectedIndex(9); break;
		case '3': // name 3
		    box[i].setSelectedIndex(10); break;   
		case '4': // name 4
		    box[i].setSelectedIndex(11); break;   	
		case '5': // weight(name 1)
		    box[i].setSelectedIndex(12); break; 
		case '6': // weight(name 2)
		    box[i].setSelectedIndex(13); break;
		case '7': // weight(name 3)
		    box[i].setSelectedIndex(14); break;   
		case '8': // weight(name 4)
		    box[i].setSelectedIndex(15); break;   	
		case 'i': // ignore 
		    box[i].setSelectedIndex(16); break;   	
		default:
		    box[i].setSelectedIndex(16); break;
	    }
	}

	// create column numbers
	for (int i=0; i<columns; i++) {
	    l_columns[i] = createLabel("       Column #"+String.valueOf(i+1));
	    l_columns[i].setBounds( 0+i*120, 0, 120, 20 );
	    listPanel.add( l_columns[i], null );

	    box[i].setFont(new Font("Dialog", Font.PLAIN, 11));
	    box[i].setBounds( 0+i*120, 20, 120, 20 );
	    listPanel.add( box[i], null );
	}

        // create the scroll pane and add the table to it.
	listPanel.setPreferredSize(new Dimension(120*columns,300));
        JScrollPane tableScrollPane = 
	    new JScrollPane(listPanel, 
			    JScrollPane.VERTICAL_SCROLLBAR_NEVER, 
			    JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS); 

	createDialog(message, tableScrollPane);

	this.setTitle("Please enter file format for file "+
		      (new File(filename)).getName());
	this.setSize(510,410);
	this.setResizable(false);
    }
    
    private void initializeExport(String formatOffered) {

	listPanel = new JPanel();	               // create the panel
	listPanel.setLayout(null);

	final Object [] entry = Period04.tstableGetHeader();   // field names
	final JList choiceList = new JList(entry);          // create listbox 
	lmexport = new DefaultListModel();
	list = new JList(lmexport);  // create listbox that gives ouput format

	int selected=0;
	int [] selectedIndices = new int[columns];
	if (phase) { entry[0]="Phase"; } else { entry[0]="Time"; }
	for (int i=0; i<formatOffered.length(); i++) {
	    switch (formatOffered.charAt(i)) {
		case 't': selectedIndices[selected]=0; selected++; break;
		case 'o': selectedIndices[selected]=1; selected++; break;
		case 'a': selectedIndices[selected]=2; selected++; break;
		case 'c': selectedIndices[selected]=3; selected++; break;
		case 'p': selectedIndices[selected]=4; selected++; break;
		case 'b': selectedIndices[selected]=5; selected++; break;
		case 'g': selectedIndices[selected]=6; selected++; break;
		case 'u': selectedIndices[selected]=7; selected++; break;
		case 'w': selectedIndices[selected]=8; selected++; break;
		case '1': selectedIndices[selected]=9; selected++; break;
		case '5': selectedIndices[selected]=10; selected++; break;
		case '2': selectedIndices[selected]=11; selected++; break;
		case '6': selectedIndices[selected]=12; selected++; break;
		case '3': selectedIndices[selected]=13; selected++; break;
		case '7': selectedIndices[selected]=14; selected++; break;
		case '4': selectedIndices[selected]=15; selected++; break;
		case '8': selectedIndices[selected]=16; selected++; break;
	    }
	}
	choiceList.setSelectedIndices(selectedIndices);
	choiceList.setFont(new Font("Dialog", Font.BOLD, 11));
	list      .setFont(new Font("Dialog", Font.BOLD, 11));
	JScrollPane choiceListPane = new JScrollPane(choiceList,
	    JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
	    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	JScrollPane listPane = new JScrollPane(list,
	    JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
	    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	choiceListPane.setBorder(
	    BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
	listPane.setBorder(
	    BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
	//--- set an appropriate size for 2nd list
	list.setPreferredSize(choiceList.getSize()); 

	//--- add the selectionlistener
	MouseInputListener mil1 = new DragSelectionListener();
	choiceList.addMouseMotionListener(mil1);
	choiceList.addMouseListener(mil1);
	MouseInputListener mil2 = new DragSelectionListener();
	list.addMouseMotionListener(mil2);
	list.addMouseListener(mil2);

	//--- create buttons
	JButton b_AddSelected = new JButton("Add ->");
	b_AddSelected.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
		    //--- add selected elements to list
		    int [] indies = choiceList.getSelectedIndices();
		    for (int i=0; i<indies.length; i++) {
			lmexport.addElement(entry[indies[i]]);
		    }
		}
	    });
	JButton b_RemoveSelected = new JButton("Remove <-");
	b_RemoveSelected.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
		    int [] indies = list.getSelectedIndices();
		    int shifted=0; // removing 1 element causes a index shift
		    for (int i=0; i<indies.length; i++) {
			lmexport.remove(indies[i]-shifted);
			shifted++;
		    }
		}
	    });

	//--- create labels
	JLabel l_list1 = createLabel("Available columns:");
	JLabel l_list2 = createLabel("Columns to export:");
	JLabel l_format = createLabel("Output Format:");

	if (!forcedVOExport) {
		String [] options = {"ASCII","VOTable"};
		cb_format = new JComboBox(options);
		cb_format.setSelectedItem(options[0]);
	}

	//--- define layout
	l_list1         .setBounds( 20, 15,150, 15);
	choiceListPane  .setBounds( 20, 30,150,280);
	b_AddSelected   .setBounds(180,120,120, 30);
	b_RemoveSelected.setBounds(180,170,120, 30);
	l_list2         .setBounds(310, 15,150, 15);
	listPane        .setBounds(310, 30,150,280);
	if (!forcedVOExport) {
		l_format        .setBounds( 10,320,120, 20);
		cb_format       .setBounds(140,320,100, 20);
	}

	//--- add elements to the panel
	listPanel.add(l_list1);
	listPanel.add(choiceListPane);
	listPanel.add(b_AddSelected);
	listPanel.add(b_RemoveSelected);
	listPanel.add(l_list2);
	listPanel.add(listPane);
	if (!forcedVOExport) {
		listPanel.add(l_format);
		listPanel.add(cb_format);
	}

	createDialog( message, listPanel );

	this.setTitle("Please enter file format");
	if (!forcedVOExport) 
		this.setSize(500,450);
	else
		this.setSize(500,430);
	this.setResizable(false);
    }

    private void createDialog (Object message, Object content){
	// add message and listPanel to array
        Object[] array = {message, content};
	Object[] options = {"OK", "Cancel"};

        optionPane = new JOptionPane(array, JOptionPane.PLAIN_MESSAGE,
                                            JOptionPane.YES_NO_OPTION,
                                            null, options, options[0]);
        setContentPane(optionPane);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent we) {
                /*
                 * Instead of directly closing the window,
                 * we're going to change the JOptionPane's
                 * value property.
                 */
                    optionPane.setValue(
			new Integer(JOptionPane.CLOSED_OPTION));
            }
        });

	optionPane.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
                String prop = e.getPropertyName();

                if (isVisible() 
                 && (e.getSource() == optionPane)
                 && (prop.equals(JOptionPane.VALUE_PROPERTY) ||
                     prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
                    Object answer = optionPane.getValue();

                    if (answer == JOptionPane.UNINITIALIZED_VALUE) {
                        //ignore reset
                        return;
                    }

                    // Reset the JOptionPane's value.
                    // If you don't do this, then if the user
                    // presses the same button next time, no
                    // property change event will be fired.
                    optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

                    if (answer.equals("OK")) {
			if (!checkFormat()) // check the input or output format
			    return; 
			setVisible(false);
		    } else {   
			// user closed dialog or clicked cancel
			setVisible(false);
		    }
                }
            }
        });
    }

    public boolean checkFormat() {
	// check the fileformat
	StringBuffer format = new StringBuffer();

	int [] val;
	if (io) { // import timestring format
	    val = new int[columns];    // an array to save selected indices
	    String tmp;
	    for (int i=0; i<columns; i++) {
		switch(box[i].getSelectedIndex()) {
		    case 0: tmp = "t"; break;  // time
		    case 1: tmp = "o"; break;  // observed
		    case 2: tmp = "a"; break;  // adjusted
		    case 3: tmp = "c"; break;  // calculated
		    case 4: tmp = "g";         // point weight 
				Period04.projectSetUsePointErrors(false); 
				break;  
		    case 5: tmp = "u";         // point error 
				Period04.projectSetUsePointErrors(true);
				break;  
		    case 6: tmp = "p"; break;  // data residuals
		    case 7: tmp = "b"; break;  // adjusted residuals
		    case 8: tmp = "1"; break;  // name 1
		    case 9: tmp = "2"; break;  // name 2
		    case 10: tmp = "3"; break; // name 3
		    case 11: tmp = "4"; break; // name 4
		    case 12: tmp = "5"; break; // weight(name 1)
		    case 13: tmp = "6"; break; // weight(name 2)
		    case 14: tmp = "7"; break; // weight(name 3)
		    case 15: tmp = "8"; break; // weight(name 4)
		    case 16: tmp = "i"; break; // ignore 
		    default: tmp = "4"; break; // default: name 4
		}

		if (format.indexOf(tmp)!=-1 && tmp!="i") {
		    JOptionPane.showMessageDialog(
			this, "Item \""+box[i].getSelectedItem()+
			"\" has been selected twice!",
			"Error", JOptionPane.ERROR_MESSAGE);
		    return false;
		} else {
		    format.append(tmp);
		}
	    }
	} else { // export timestring format
	    //--- check number of selected items
	    int columns = lmexport.getSize();
	    if (columns==0) {
		JOptionPane.showMessageDialog(
		    this, "Nothing selected!",
		    "Error", JOptionPane.ERROR_MESSAGE);
		return false;
	    }
	    Object [] entry = Period04.tstableGetHeader();  
	    if (phase) { entry[0]="Phase"; }

	    //--- now convert the selection into the format string
	    for (int i=0; i<columns; i++) {
		String tmp = (String)lmexport.getElementAt(i);
		if (tmp.equals((String)entry[0])) {
		    format.append("t"); continue; // time
		}
		if (tmp.equals((String)entry[1])) {
		    format.append("o"); continue; // observed
		}
		if (tmp.equals((String)entry[2])) {
		    format.append("a"); continue; // adjusted
		}
		if (tmp.equals((String)entry[3])) {
		    format.append("c"); continue; // calculated
		}
		if (tmp.equals((String)entry[4])) {
		    format.append("p"); continue; // residuals observed
		}
		if (tmp.equals((String)entry[5])) {
		    format.append("b"); continue; // residuals adjusted
		}
		if (tmp.equals((String)entry[6])) {
		    format.append("w"); continue; // weight
		}
		if (tmp.equals((String)entry[7])) {
		    format.append("g"); continue; // point weight
		}
		if (tmp.equals((String)entry[8])) {
		    format.append("u"); continue; // point error
		}
		if (tmp.equals((String)entry[9])) {
		    format.append("1"); continue; // name 1
		}
		if (tmp.equals((String)entry[10])) {
		    format.append("5"); continue; // name 1 weight
		}
		if (tmp.equals((String)entry[11])) {
		    format.append("2"); continue; // name 2
		}
		if (tmp.equals((String)entry[12])) {
		    format.append("6"); continue; // name 2 weight
		}
		if (tmp.equals((String)entry[13])) {
		    format.append("3"); continue; // name 3
		}
		if (tmp.equals((String)entry[14])) {
		    format.append("7"); continue; // name 3 weight
		}
		if (tmp.equals((String)entry[15])) {
		    format.append("4"); continue; // name 4
		}
		if (tmp.equals((String)entry[16])) {
		    format.append("8"); continue; // name 4 weight
		}
	    }
		if (!forcedVOExport)
			votable = (cb_format.getSelectedIndex()==1);
		else
			votable = true;
	}
	formatstring = format.toString();
	return true;
    }

    public String getFileFormat() {
	return formatstring;
    }

	public boolean getUseVOTableFormat() {
		return votable;
	}

    protected JLabel createLabel(String name) {
	JLabel label = new JLabel(name);
	label.setFont(new Font("Dialog", Font.BOLD, 11));
	return label;
    }

    public void actionPerformed(ActionEvent evt) {
	Object s = evt.getSource();
	for (int i=0; i<columns; i++) {
	    if (s==box[i]) 
		return;
	}
	// in case of OK or CANCEL:
	optionPane.setValue("OK");
    }

    class MyTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 7572808366614496984L;

 	final int        columns = XDialogTSFileFormat.this.columns;
	final int        rows = XDialogTSFileFormat.this.rows;
	final Object[][] data = new Object[rows][columns];

	public MyTableModel() {
	    readData();
	}

        public int getColumnCount() { return columns; }
		public int getRowCount()    { return rows;    }
		
        public Object getValueAt(int row, int col) {
            return data[row][col];
        }

        public boolean isCellEditable(int row, int col) {
			return false;
        }


	public void readData() {
	    //--- read in some lines from the file
	    //---
	    try {
		//--- is file written in VOTable format?
		//---
		votable = false;
		BufferedReader br = new BufferedReader(new FileReader(filename));
		String sline = br.readLine();
		while (sline!=null) {
			if (sline.contains("<VOTABLE") ||
				sline.contains("<votable")) {
			//		    if (sline.indexOf("<VOTABLE")!=-1 ||
			//sline.indexOf("<votable")!=-1) {
			votable = true;
			break;
		    }
		    sline = br.readLine();
		}
		br.close();
		
		//--- extract some lines to display
		//---
		br = new BufferedReader(new FileReader(filename)); // reopen
		if (votable) {
		    boolean isTableInput = false;
		    boolean isDataLineInput = false;
		    boolean isDataValueInput = false;
		    boolean isDone = false;
		    int icol=0,irow=0,id;
		    sline = br.readLine();
		    
		    while (sline!=null && !isDone) {
			//--- first look for table tag
			//---			
			if (isTableInput) {
			    StringTokenizer st = new StringTokenizer(sline);
			    while (st.hasMoreTokens() && !isDone) {
				String tmp = st.nextToken();
				//--- examine line
				//---
				while (tmp!=null && !isDone) {
				    if (!isDataLineInput) {
					id = tmp.indexOf("<");   // first tag
					if (id!=-1) {
					    tmp = tmp.substring(id,tmp.length());
					    if (tmp.toLowerCase().startsWith("<tr")) {		
						id = tmp.indexOf(">");
						isDataLineInput = true;
					    } else if (tmp.toLowerCase().startsWith("</tr")) {
						id = tmp.indexOf(">");
						isDataLineInput = false;
						icol=0;
						irow++;
						if (irow==rows)
						    isDone = true;
					    }
					    if (id+1 == tmp.length()) {
						tmp = null;
					    } else {
						tmp = tmp.substring(id+1,tmp.length());
					    }
					    
					}
				    } else { // isDataLineInput
					//--- extract data values
					if (!isDataValueInput) {
					    id = tmp.indexOf("<");   // first tag
					    if (id!=-1) {
						tmp = tmp.substring(id,tmp.length());
						if (tmp.toLowerCase().startsWith("<td")) {			
						    id = tmp.indexOf(">");
						    isDataValueInput = true;
						} else if (tmp.toLowerCase().startsWith("</td")) {
						    id = tmp.indexOf(">");
						    isDataValueInput = false;
						    icol++;
						    if (icol==columns)
							isDataLineInput = false;
						}
						if (id+1 == tmp.length()) {
						    tmp = null;
						} else {
						    tmp = tmp.substring(id+1,tmp.length());
						}
					    }
					} else { // isDataValueInput
					    id = tmp.indexOf("<");   // first tag
					    if (id!=-1) { 
						String value = tmp.substring(0,id);
						//System.out.println("-> "+tmp.substring(0,id));////////////
						try {
						    this.data[irow][icol] = value;
						} catch (NoSuchElementException e) {
						    this.data[irow][icol] = "-";	
						}
						tmp = tmp.substring(id,tmp.length());
					    } else {  // </td> in next line
						System.out.println("->! "+tmp);//////////////
						try {
						    this.data[irow][icol] = tmp;
						} catch (NoSuchElementException e) {
						    this.data[irow][icol] = "-";	
						}
						tmp = null;
					    }
					    isDataValueInput = false;
					}
				    }
				}
			    }
			}

			
			if (sline.contains("<TABLEDATA") ||
			    sline.contains("<tabledata")) {
				//if (sline.indexOf("<TABLEDATA")!=-1 ||
			    //sline.indexOf("<tabledata")!=-1) {
			    isTableInput = true;
			} 
			sline = br.readLine();
		    }
		    

		} else { // ASCII data
		    for (int i=0; i<rows; i++) {
			String tmp = br.readLine();
			if (tmp==null) { break; } // end of file has already been reached
			StringTokenizer st = new StringTokenizer(tmp);
			for (int j=0; j<columns; j++) { 
			    try {
				this.data[i][j] = st.nextToken();
			    } catch (NoSuchElementException e) {
				this.data[i][j] = "-";	
			    }
			}
		    }
		}
		br.close();

	    } catch (IOException e) {
		System.out.println("error!! reading of file "
				   +filename+" failed\n"+e);
		for (int i=0; i<rows; i++) {
		    for (int j=0; j<columns; j++) { 
			this.data[i][j] = "not available";
		    }
		}
	    }
	}
    }
}
