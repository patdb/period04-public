/*----------------------------------------------------------------------*
 *  XDialogTSImport
 *  a dialog to ...
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.*;                  // Property change stuff
import java.net.URL;
import javax.swing.*;

public class XDialogTSImport extends XDialog 
    implements ActionListener
{
    private static final long serialVersionUID = 3956719915156935426L;

    private JOptionPane    optionPane;
    private XTabTimeString tab;
    private XProjectFrame  mainframe;
    private JButton        b_OK     = createButton("Ok");
    private JButton        b_Cancel = createButton("Cancel");
    private JRadioButton   r_append, r_replace;
    private JCheckBox      c_keepFreqs, c_keepFourier, c_keepLog;
    private boolean        cancel = false, miniversion;
	private boolean        isVoImport = false; 

    public XDialogTSImport(XProjectFrame mframe, XTabTimeString tab, 
			   boolean mv, final URL url) {
	super(mframe.getFrame(), true);  // necessary to keep dialog in front
	this.tab = tab;
	this.mainframe = mframe;
	this.miniversion = mv;
	this.isVoImport = ((url!=null)?true:false);

	//--- create a panel
	JPanel inputpanel = new JPanel();
	inputpanel.setLayout(null);
	
	//--- create content for panel
	JLabel l_what;
	if (!miniversion) {
	    l_what = createBoldLabel("<html>Your project already contains data.<br>What do you want to do?</html>");
	} else {
	    l_what = createBoldLabel("Replace time string and ...");
	}
	if (!miniversion) {
	    r_append      = createRadioButton("Append "+(isVoImport?"the":"a")+" new time-string");
	    r_replace     = createRadioButton("Replace the old time-string");
	}
	c_keepFreqs   = createCheckBox("Do not delete frequency data");
	c_keepFourier = createCheckBox("Do not delete Fourier data");
	c_keepLog     = createCheckBox("Do not delete the log");
	c_keepFreqs  .setSelected(false); // initial setting
	c_keepFourier.setSelected(false); // initial setting
	c_keepLog    .setSelected(false); // initial setting
	c_keepFreqs  .setEnabled(miniversion); // initial setting
	c_keepFourier.setEnabled(miniversion); // initial setting
	c_keepLog    .setEnabled(miniversion); // initial setting
	if (!miniversion) {
	    ButtonGroup bg = new ButtonGroup();
	    bg.add(r_append);
	    bg.add(r_replace);
	    ActionListener radioaction = new ActionListener() {
		    public void actionPerformed(ActionEvent evt) {
			c_keepFreqs  .setEnabled(r_replace.isSelected());
			c_keepFourier.setEnabled(r_replace.isSelected());
			c_keepLog    .setEnabled(r_replace.isSelected());
		    }
		};
	    r_append .addActionListener(radioaction);
	    r_replace.addActionListener(radioaction);
	}
	
	//--- set positions
	if (!miniversion) {
	    l_what       .setBounds( 10,  5, 200, 40);
	    r_append     .setBounds( 20, 50, 200, 20);
	    r_replace    .setBounds( 20, 70, 200, 20);
	    c_keepFreqs  .setBounds( 50, 90, 200, 20);
	    c_keepFourier.setBounds( 50,110, 200, 20);
	    c_keepLog    .setBounds( 50,130, 200, 20);
	} else {
	    l_what       .setBounds( 10,  5, 200, 20);
	    c_keepFreqs  .setBounds( 50, 30, 200, 20);
	    c_keepFourier.setBounds( 50, 50, 200, 20);
	    c_keepLog    .setBounds( 50, 70, 200, 20);
	}
	
	//--- add content to panel
	inputpanel.add(l_what);
	if (!miniversion) {
	    inputpanel.add(r_append);
	    inputpanel.add(r_replace);
	}
	inputpanel.add(c_keepFreqs);
	inputpanel.add(c_keepFourier);
	inputpanel.add(c_keepLog);
	Object choice = inputpanel;
	
	//--- create options
	Object [] options = { b_OK, b_Cancel };

        optionPane = new JOptionPane(
	    choice, JOptionPane.PLAIN_MESSAGE,
	    JOptionPane.OK_CANCEL_OPTION, null, options, options[0]);
        setContentPane(optionPane);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent we) {
                /*
                 * Instead of directly closing the window,
                 * we're going to change the JOptionPane's
                 * value property.
                 */
                    optionPane.setValue(
			new Integer(JOptionPane.CLOSED_OPTION));
            }
        });

	optionPane.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
                String prop = e.getPropertyName();

                if (isVisible() 
                 && (e.getSource() == optionPane)
                 && (prop.equals(JOptionPane.VALUE_PROPERTY) ||
                     prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
                    Object value = optionPane.getValue();

                    if (value == JOptionPane.UNINITIALIZED_VALUE) {
                        //ignore reset
                        return;
                    }

                    // Reset the JOptionPane's value.
                    // If you don't do this, then if the user
                    // presses the same button next time, no
                    // property change event will be fired.
                    optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

                    if (value.equals(b_OK)) {
			boolean append;
			boolean replace; 
			if (!miniversion) {
			    append  = r_append.isSelected();
			    replace = r_replace.isSelected();
			} else {
			    append = false;
			    replace = true;
			}
			if ( !(append || replace) ) {
			    JOptionPane.showMessageDialog(
				XDialogTSImport.this, 
				"Please make a choice!", null,
				JOptionPane.ERROR_MESSAGE);
			    return;
			}
			if (replace) {
			    //--- let's tidy up a bit
			    XDialogTSImport.this.mainframe.cleanProject(
				!c_keepFourier.isSelected(), 
				!c_keepFreqs.isSelected(),
				true, !c_keepLog.isSelected());
			    setVisible(false); // make dialog dissapear
			    XDialogTSImport.this.tab.importTimeString(append, url);
			} else {
			    setVisible(false); // make dialog dissapear
			    //--- ok, we're appending
			    XDialogTSImport.this.tab.importTimeString(append, url);
			}
		    } 
		    else { // user closed dialog or clicked cancel
			cancel = true;
			setVisible(false);
		    }
                }
            }
        });

	this.setTitle("Import time-string");
	if (miniversion) {
	    this.setSize(300,190);
	} else {
	    this.setSize(300,250);
	}
	this.setResizable(false);
	this.setLocationRelativeTo(mainframe.getFrame());
	this.setVisible(true);
    }


    public boolean isCanceled() { return cancel; }

    public void actionPerformed(ActionEvent evt) {
	Object s = evt.getSource();
	if (s==r_append || s==r_replace || s==c_keepFreqs || s==c_keepFourier || s==c_keepLog)
	    return;
	optionPane.setValue(s);
    }
}
