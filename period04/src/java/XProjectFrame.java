/*-----------------------------------------------------------------------*
 *  XProjectFrame
 *  this class creates the mainframe (tabs, menubar)
 *-----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.OutputStream;
import java.io.FileReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;                
import java.util.Calendar;                       
import java.util.Collection;
import java.util.GregorianCalendar;                     
import java.util.Iterator;
import java.util.List;
import java.util.Locale;                     
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*; // ChangeListener
import javax.swing.filechooser.*;
import javax.help.*; 

import org.astrogrid.samp.Message;
import org.astrogrid.samp.Metadata;
import org.astrogrid.samp.Subscriptions;
import org.astrogrid.samp.client.AbstractMessageHandler;
import org.astrogrid.samp.client.ClientProfile;
import org.astrogrid.samp.client.HubConnection;
import org.astrogrid.samp.client.HubConnector;
import org.astrogrid.samp.client.MessageHandler;
import org.astrogrid.samp.client.SampException;
import org.astrogrid.samp.xmlrpc.StandardClientProfile;
import org.astrogrid.samp.Response;
import org.astrogrid.samp.ErrInfo;
import org.astrogrid.samp.Client;
import org.astrogrid.samp.SampMap;
import org.astrogrid.samp.httpd.ResourceHandler;
import org.astrogrid.samp.httpd.HttpServer;
import org.astrogrid.samp.httpd.ServerResource;

import uk.ac.starlink.votable.VOTableBuilder;
import uk.ac.starlink.votable.VOTableWriter;
import uk.ac.starlink.table.StarTable;
import uk.ac.starlink.table.StarTableWriter;
import uk.ac.starlink.table.StarTableFactory;
import uk.ac.starlink.votable.VOStarTable;
import uk.ac.starlink.table.WrapperStarTable;
import uk.ac.starlink.util.URLUtils;

public class XProjectFrame 
//	implements ManagerObserver, PlasticRegistryListener 
{
    //private boolean showPlasticMessages = false;//true;

    private final int  projectID;  // the ID of the project window
    private boolean expertMode;    // are we using the expert mode?

    private JFrame frame = new JFrame();
    private Container contentPane = frame.getContentPane();
    private ActionListener     actionProcCmd;
    private FileTask           filetask;
    private JFileChooser       fc;
    public  XDialogDataManager datamanager;

    private String projectFile = "";// saves path+name of current project

    //---
    //--- menu bar
    //---
    private JMenuBar  menuBar = new JMenuBar();
    private JMenu     menuFile, menuSpecial, menuConfig, menuHelp, files;
    private JMenu     menuExport, menuVoTools, menuSendTS;
    private JMenuItem item;  
    //--- 
    //--- file menu
    //---
    private JMenuItem i_NewProject,i_LoadProject,i_SaveProject,i_SaveProjectAs;
    private JMenuItem [] i_RecentFiles;
    private JMenuItem i_SendTimeStringVOTable;
    private JMenuItem i_ImportTimeString, i_ExportTimeString;
    private JMenuItem i_ImportFrequencies, i_ExportFrequencies, i_ExportFrequenciesLatex;
    private JMenuItem [] i_SendToVoApps;
    private JMenuItem i_SaveLogfile;
    private JCheckBoxMenuItem i_ExpertMode;
    private JMenuItem i_Quit;
    //---
    //--- special menu
    //---
    private JMenuItem i_WeightSelection, i_ManageData;
    private JMenuItem i_SubdivideTS,i_ShowTimes,i_AdjustTS,i_RestoreZeroAdj;
    private JMenuItem i_CombineSubStrings,/*i_DeleteDefaultLabel,*/i_DeletePoints;
    private JMenuItem i_CleanAllFreqs, i_SelectAllFreqs, i_DeselectAllFreqs;
    private JMenuItem i_CleanDeselectedFreqs;
    private JMenu     i_FreqSortMenu;
    private JMenuItem [] i_SortFreqs;
    private JCheckBoxMenuItem i_UseCombiFreqs;
    private JMenuItem i_Epochs, i_RecalcResiduals, i_PredictSignal;
    private JMenuItem i_CreateArtData, /*i_SetAliasGap,*/ i_AnalyticalErrors;
    private JMenuItem i_CalcNoiseAtFreq, i_CalcNoiseSpectrum, i_CalcNyquist;
    //---
    //--- config menu
    //---
    private JMenuItem [] i_FittingFunction;
    private JMenuItem i_FourierSettings;
    private JMenu fit;
    //---
    //--- help menu
    //---
    private JMenuItem i_About, i_Copyright, i_Topics, i_Tutorials, i_Shortcuts;
    private JMenuItem i_ReportBug, i_Homepage, i_F1Help;
    private HelpSet hs; 
    private JMenuItem menu_help;
    private static HelpBroker hb;

    //---
    //--- frame related variables
    //---
    public final int initHeight = 700; // initial default frame height
    public int height = 700;      // default frame height
    public int width = 505;      // default frame width
    public int sizeDiff=0;      // !=0 if screen<frame
    private Dimension screenSize;
    //---
    //--- status bar
    //---
    private JPanel     statusBar       = new JPanel();
    private JLabel     statusBarLabel  = new JLabel();
    private CardLayout statusBarLayout = new CardLayout();
    //---
    //--- tabbed frame
    //---
    private JTabbedPane    tabbedFrame     = new JTabbedPane();
    private JTabbedPane    tabFit;
    private JPanel         tabTimeString, tabFourier, tabLog;
    private XTabTimeString panelTimeString;
    private XTabFit        panelFit;
    private XTabFourier    panelFourier;
    private XTabLog        panelLog;
    //---
    //--- for the calculating panel
    //---
    private JPanel       panelCalculating;
    private JProgressBar progress;
    private JLabel       progressLabel;
    private int          activeTab;
    private Timer        timer;
    private Task         task;
    private boolean      blockStateChangedEvent;
    private JButton      b_cancel;
    private boolean      working = false;
    //---
    //--- extern panels
    //---
    private JFrame w_ExternTimeString;
    private JFrame w_ExternFourier;
	//---
	//--- VO Tools
	//---
	Vector voApplications;

    public XProjectFrame(int id, boolean isBatchProcess) {
	//--- set the id of the project frame
	this.projectID = id;
	//--- initialize c++ part when starting project
	Period04.initializeProject(id, System.getProperty("user.home")+
				   System.getProperty("file.separator"));
	//--- set working directory
	File tmp = new File(Period04.getWorkingDirectory());
	if (tmp.exists()) {
	    Period04.setCurrentDirectory(tmp);
	} else {
	    Period04.setCurrentDirectory(
		new File(System.getProperty("user.home")+
			 System.getProperty("file.separator")));
	}
	//--- initialize java part
	try { initialize(isBatchProcess); } catch(Exception e) { e.printStackTrace(); }
	// set project unchanged
	Period04.projectNoChanges();          
    }

	public JFrame getFrame()
	{
		return frame;
	}
	public XProjectFrame getThis() {return this;}

    //---
    //--- initialize()
    //--- initializes the menubar and the tabbed frame
    //---
    public void initialize(boolean isBatchProcess) throws Exception {  
	contentPane = (Container) frame.getContentPane();
	expertMode = Period04.isExpertMode();
	//---
	//--- initialize the tabs
	//---
	panelTimeString = new XTabTimeString(this);
	panelFit        = new XTabFit(this);
	panelFourier    = new XTabFourier(this);
	panelLog        = new XTabLog(this);
	//---
	//--- configure the status bar
	//---
	statusBarLayout = new CardLayout(0,0);
	statusBar.setLayout(statusBarLayout);
	Dimension size = statusBar.getSize();
	size.height=15;
	statusBar.setPreferredSize(size);
	statusBarLabel = new JLabel();
	setDefaultStatusBarText();
	statusBar.add(statusBarLabel, "label");
	//---
	//--- create main-menu
	//---
	menuFile    = XMenuHelper.addMenuBarItem( menuBar, "_File" );
	menuSpecial = XMenuHelper.addMenuBarItem( menuBar, "_Special" );
	menuConfig  = XMenuHelper.addMenuBarItem( menuBar, "C_onfiguration" );
	menuHelp    = XMenuHelper.addMenuBarItem( menuBar, "_Help" );
	//---
	//--- create action listener for menu items
	//---
	actionProcCmd = new ActionListener() {
		public void actionPerformed( ActionEvent evt ) {
		    if (working) { return; } // program is doing a task, return
		    Object s=evt.getSource();
		    // file menu
		    if (s==i_NewProject)
			newProject(/*nocheck=*/ false);
		    else if (s==i_LoadProject) {
			loadProject(null);
		    }
		    else if (s==i_SaveProject)
			saveProject();            
		    else if (s==i_SaveProjectAs)
			saveProjectAs();
		    if (Period04.recentFiles!=null) {
			for (int i=0; i<Period04.recentFiles.length; i++) {
			    if (s==i_RecentFiles[i]) {
				loadProject(Period04.recentFiles[i]);
			    }
			}
		    }
		    if (s==i_ImportTimeString) 
			panelTimeString.importTimeString();
		    else if (s==i_ExportTimeString)
			exportTimeString();
		    else if (s==i_ImportFrequencies) 
			importFrequencies();
		    else if (s==i_ExportFrequencies)
			exportFrequencies(false);
		    else if (s==i_ExportFrequenciesLatex)
			exportFrequencies(true);
			if (i_SendToVoApps!=null) {
				for (int i=0; i<i_SendToVoApps.length; i++) {
					if (s==i_SendToVoApps[i])
						sendTSToVoApp(i);
				}
			}
		    if (s==i_SaveLogfile)
			panelLog.exportLog();
		    else if (s==i_ManageData) {
			datamanager.resetDialog();
			datamanager.setLocationRelativeTo(frame);
			datamanager.setVisible(true);
		    }
		    else if (s==i_ExpertMode) 
			setExpertMode(i_ExpertMode.isSelected());
		    //---
		    //--- special menu
		    //---
		    else if (s==i_WeightSelection) {
			new XDialogWeight(getThis());
			panelFourier.updateWeights();
			panelFit.updateWeights();
		    }
		    else if (s==i_CalcNyquist)
			panelFourier.calcSpecialNyquist();
		    else if (s==i_SubdivideTS)
			panelTimeString.subdivideTimeString();
		    else if (s==i_AdjustTS)
			panelTimeString.adjustTimeString();
		    else if (s==i_RestoreZeroAdj)
			panelTimeString.restoreZeropoints();
		    else if (s==i_CombineSubStrings)
			panelTimeString.combineSubStrings(0);
		    else if (s==i_ShowTimes)
			panelTimeString.showTimes();
		    /*		    else if (s==i_DeleteDefaultLabel)
				    panelTimeString.deleteDefaultLabel();*/
		    else if (s==i_DeletePoints) 
			panelTimeString.deleteSelectedPoints();
		    else if (s==i_CleanAllFreqs)
			cleanAllFrequencies();
 		    else if (s==i_CleanDeselectedFreqs)
 			panelFit.cleanDeselectedFrequencyEntries();
		    else if (s==i_SelectAllFreqs)
			panelFit.selectAllFrequencies();
		    else if (s==i_DeselectAllFreqs)
			panelFit.deselectAllFrequencies();
		    else if (s==i_SortFreqs[0])
 			panelFit.sortFrequencyList(0);
		    else if (s==i_SortFreqs[1])
 			panelFit.sortFrequencyList(1);
 		    else if (s==i_SortFreqs[2])
 			panelFit.sortFrequencyList(2);
		    else if (s==i_UseCombiFreqs)
			panelFit.setUseCombiFreqs(!i_UseCombiFreqs.isSelected());
		    else if (s==i_Epochs) {
			panelFit.setFrequencyListData();
			if (panelFit.getFitMode()==XTabFit.BINARY_MODE) {
			    if (Period04.projectGetBMActiveFrequencies()==0) {
				JOptionPane.showMessageDialog(
				    frame, 
				    "No frequencies selected ...", "", 
				    JOptionPane.ERROR_MESSAGE);
				return;
			    }
			} else { // NORMAL_MODE
			    if (Period04.projectGetActiveFrequencies()==0) {
				JOptionPane.showMessageDialog(
				    frame, 
				    "No frequencies selected ...", "", 
				    JOptionPane.ERROR_MESSAGE);
				return;
			    }
			}
			panelFit.calculateEpoch();
		    }
		    else if (s==i_RecalcResiduals) 
			panelFit.recalcResiduals();
		    else if (s==i_PredictSignal)
			panelFit.predictSignal();
		    else if (s==i_CreateArtData) 
			panelFit.createArtificialData();
		    /*else if (s==i_SetAliasGap) {
			XDialogInput dialog = new XDialogInput(
			    frame, "Change alias-gap", 
			    "Frequency adjustment:", 
			    Period04.projectGetFrequencyAdjustment(), 
			    2);
			if (dialog.isCanceled()) { return; }
			double adj = dialog.getValidatedNonNegativeNumber();
			Period04.projectSetFrequencyAdjustment(adj);
			getLog().writeProtocol(
			    "Changed frequency adjustment:\n"+
			    "New alias-gap:            "+adj, true);
		    } */
		    else if (s==i_AnalyticalErrors) 
			showAnalyticalErrors();
		    else if (s==i_CalcNoiseAtFreq) 
			calculateNoiseAtFrequency();
		    else if (s==i_CalcNoiseSpectrum) 
			panelFourier.calculateNoiseSpectrum();
		    //--- config menu
		    else if (s==i_FourierSettings) {
			showFourierSettings();
		    }
		    else if (s==i_FittingFunction[0]) {
			panelFit.setFitMode(XTabFit.NORMAL_MODE);
		    }
		    else if (s==i_FittingFunction[1]) {
			panelFit.setFitMode(XTabFit.BINARY_MODE);
		    }
		    //--- help menu
		    else if (s==i_About)
			showAbout();
		    else if (s==i_ReportBug) {
			JOptionPane.showMessageDialog(
			    frame, 
			    "<html><center>In order to report a bug, "+
			    "please send an email to<br>"+
			    "ptklenz@gmail.com</center></html>",
			    "Report Bug", JOptionPane.PLAIN_MESSAGE);
		    }
		    else if (s==i_Homepage) {
			Browser.displayURL(
			    "http://www.period04.net");
		    }
		    else if (s==i_Quit) { shutdown(); }
		}
	    };

	//---
	//--- create menus
	//---
	createFileMenu();       // add entries to file menu	
	createSpecialMenu();    // add entries to special-menu
	createConfigMenu();    // add entries to special-menu
	//---
	//--- add entries to options-menu
	//---
	fit = new JMenu( "Set fitting function" );
	i_FittingFunction = new JMenuItem[2];
	i_FittingFunction[0] = XMenuHelper.addMenuItem(
	    fit, " Standard formula", actionProcCmd);
	i_FittingFunction[1] = XMenuHelper.addMenuItem(
	    fit, " Standard formula with periodic time shift", actionProcCmd);
	if (expertMode)
	    menuConfig.add(fit);

	// Find the HelpSet file and create the HelpSet object: 
	String helpHS = "Master.hs"; 
	ClassLoader cl = this.getClass().getClassLoader();
	try { 
	    URL hsURL = HelpSet.findHelpSet(cl, helpHS); 
	    hs = new HelpSet(null, hsURL); 
	} catch (Exception ee) { 
	    // Say what the exception really is 
	    System.out.println( "HelpSet " + ee.getMessage()); 
	    System.out.println("HelpSet "+ helpHS +" not found");
	    return;
	}
	//--- Create a HelpBroker object: 
	hb = hs.createHelpBroker(); 
	//--- Create a "Help" menu item to trigger the help viewer: 
	menu_help = XMenuHelper.addMenuItem( 
	    menuHelp, "Period04 Help", new CSH.DisplayHelpFromSource( hb ) );
	menu_help.setAccelerator(KeyStroke.getKeyStroke("F1"));
	panelFit.addHelp(); // for the Info button in the Fit tab

	//---
	//--- add further entries to the help-menu
	//---
	menuHelp.addSeparator();
	i_Topics = XMenuHelper.addMenuItem( 
	    menuHelp, "Topics", new CSH.DisplayHelpFromSource(hb));
	i_Tutorials = XMenuHelper.addMenuItem( 
	    menuHelp, "Tutorials", new CSH.DisplayHelpFromSource(hb));
	i_Shortcuts = XMenuHelper.addMenuItem( 
	    menuHelp, "Shortcuts", new CSH.DisplayHelpFromSource(hb));
	menuHelp.addSeparator();
	i_Homepage = XMenuHelper.addMenuItem( 
	    menuHelp, "Period04 Homepage", actionProcCmd );
	i_ReportBug = XMenuHelper.addMenuItem( 
	    menuHelp, "Report a bug", actionProcCmd );
	menuHelp.addSeparator();
	i_Copyright = XMenuHelper.addMenuItem( 
	    menuHelp, "Copyright", new CSH.DisplayHelpFromSource(hb));
	i_About = XMenuHelper.addMenuItem( 
	    menuHelp, "About", actionProcCmd );
	//--- set the appropriate help sites
	CSH.setHelpIDString(i_Topics,"howto.index");
	CSH.setHelpIDString(i_Tutorials,"tutorials.index");
	CSH.setHelpIDString(i_Shortcuts,"shortcuts");
	CSH.setHelpIDString(i_Copyright,"copyright");

	// configure the tabbed frame
	tabbedFrame.setTabPlacement(JTabbedPane.TOP);
	tabbedFrame.setFont(new java.awt.Font("Dialog", 1, 11));

	// create tabs
	tabTimeString = panelTimeString.getMainPanel();
	tabFourier    = panelFourier.getMainPanel();
	tabFit        = panelFit.getTabPanel();
	tabLog        = panelLog.getMainPanel();

	// put it together
	frame.setJMenuBar( menuBar );
	tabbedFrame.addTab("  Time String  ", null, tabTimeString);
	tabbedFrame.addTab("      Fit      ", null, tabFit);
	tabbedFrame.addTab("    Fourier    ", null, tabFourier);
	tabbedFrame.addTab("      Log      ", null, tabLog);
	tabbedFrame.addChangeListener(new ChangeListener() {
		public void stateChanged(ChangeEvent e) {
		    if (!blockStateChangedEvent) {
			updateDisplays();
			setDefaultStatusBarText();
		    }
		}
	    });

	//---
	//--- calculate final frame size.
	//--- if the screen is to small resize to fit
	//---
	screenSize=(Toolkit.getDefaultToolkit()).getScreenSize();
	if(Period04.isMacOS) { width=535; } // adjust width for mac
	Dimension frameSize=new Dimension(width,height);
	if(frameSize.height>screenSize.height) {
	    sizeDiff=frameSize.height - (screenSize.height-80);
	    frameSize.height=screenSize.height-80;
	    if (frameSize.height<height) { // very low resolution
		frame.setResizable(false);
		height = frameSize.height;	    // set default frame height
	    }
	}
	if(frameSize.width>screenSize.width) {
	    frameSize.width=screenSize.width;
	}
	//--- 
	//--- add final parts
	//---
	contentPane.add(tabbedFrame, BorderLayout.CENTER);
	contentPane.add(statusBar,  BorderLayout.SOUTH);
	//---
	//--- create the frame
	//---
	frame.validate();
	frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	frame.addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent we) { shutdown(); }
		public void windowActivated(WindowEvent we) {
		    updateCurrentProject();
		}
	    });	
	//---
	//--- center the frame and show it
	//---
	frame.setLocation((screenSize.width - frameSize.width )/2 + 20*projectID,
		    (screenSize.height- frameSize.height)/2 + 20*projectID);
	frame.setSize(frameSize);
	frame.addComponentListener(new JFrameSizeListener(frame,this));
	frame.setResizable(true);
	frame.setTitle("Period04 ["+Period04.command( "version")+"]: ");
	if (!isBatchProcess) {
	    frame.setVisible(true);
	    frame.toFront();
	}

	//initializePlastic();
	initializeSAMP();
	updateMenu();           // general update for enable/disable
    }

    /////////////////////////////////
    private static final String VO_NAME = "Period04";
    private static final String VO_DESCRIPTION = "An application for time series analysis";    
	private static final String VO_ICON = "http://www.univie.ac.at/tops/Period04/Period04.ico";
    private HubConnector hubconnector;
	private HttpServer httpserver;
	private ResourceHandler resourcehandler;
	private int iRes;
	private Map resourceMap;
	private StarTableWriter writer;

    public void initializeSAMP() {
	// Construct a connector 
	ClientProfile profile = StandardClientProfile.getInstance();
	hubconnector = new HubConnector(profile);

	// Configure it with metadata about this application
	Metadata meta = new Metadata();
	meta.setName(VO_NAME);
	meta.setDescriptionText(VO_DESCRIPTION);
	meta.setIconUrl(VO_ICON);
	hubconnector.declareMetadata(meta);

	// Prepare to receive messages with specific MType(s)
	hubconnector.addMessageHandler( new HubRegisterHandler() );
	hubconnector.addMessageHandler( new HubUnRegisterHandler() );
	hubconnector.addMessageHandler( new VOImportHandler() );
			
	// This step required even if no custom message handlers added.
	hubconnector.declareSubscriptions(hubconnector.computeSubscriptions());

	// Keep a look out for hubs if initial one shuts down
	hubconnector.setAutoconnect(10);

	try {
		initializeHttpServer();
	} catch (IOException e) {
		System.err.println(e);
	}

	menuVoTools.setEnabled(true);

	return;
    }


    public class VOImportHandler extends AbstractMessageHandler {
	VOImportHandler() {
	    super( "table.load.votable" );
	}
		
        public Map processCall( HubConnection connection, String senderId,
                                Message msg ) throws IOException {
            String tableId = (String) msg.getParam( "table-id" );
            String url = (String) msg.getParam( "url" );
			
            // We have the URL.  Now use STIL classes
            // (http://www.starlink.ac.uk/stil/stil.jar) to turn it into
            // a table.
            StarTable table = new StarTableFactory( true )
				.makeStarTable( url, "votable" );
	    panelTimeString.importVOTTimeString(table);
	    return null;
        }
    }

    public class HubRegisterHandler extends AbstractMessageHandler {
	HubRegisterHandler() {
	    super( "samp.hub.event.register" );
	}
	
        public Map processCall( HubConnection connection, String senderId,
                                Message msg ) throws IOException {
	    return new HashMap();
        }
	
	public void receiveNotification( HubConnection connection,
					 String senderId,
					 Message msg ) {
	    try {
		Thread.sleep(1000);
	    } catch (java.lang.InterruptedException e) {
		
	    }
	    updateVOMenu(); 
	}
    }
    
    public class HubUnRegisterHandler extends AbstractMessageHandler {
	HubUnRegisterHandler() {
	    super( "samp.hub.event.unregister" );
	}
	
        public Map processCall( HubConnection connection, String senderId,
                                Message msg ) throws IOException {
	    return new HashMap();
        }
	
	public void receiveNotification( HubConnection connection,
					 String senderId,
					 Message msg ) {
	    try {
		Thread.sleep(1000);
	    } catch (java.lang.InterruptedException e) {
		
	    }
	    updateVOMenu(); 
	}
    }
    
	
	private void updateVOMenu() {
		if (Period04.projectGetSelectedPoints()>0) {
			voApplications = getVOTableReceiverApplications();
			if (voApplications.size()>0) {
				menuSendTS.setEnabled(true);
				menuSendTS.removeAll();
				i_SendToVoApps = new JMenuItem[voApplications.size()];
				for (int i=0; i<voApplications.size(); i++) {
					i_SendToVoApps[i] = XMenuHelper.addMenuItem(
						menuSendTS, ((Client)voApplications.get(i)).getMetadata().getName(), actionProcCmd);
				}
			} else {
				menuSendTS.setEnabled(false);			
			}
		}
	}

	private void initializeHttpServer() throws IOException {
		httpserver = new HttpServer();
		httpserver.setDaemon( true );
		resourcehandler = new ResourceHandler( httpserver, "/dynamic" );
		httpserver.addHandler( resourcehandler );
	}

	private void sendTSToVoApp(int i) {
		Client client = (Client)voApplications.get(i);
		File file = panelTimeString.exportVOTTimeString();
		if (file==null)
			System.err.println("problem");
		try {
			// Broadcast a message
			Map paramMap = new HashMap();
			URL url = URLUtils.makeFileURL(file);
			paramMap.put("url", url.toString());
			Message msg = new Message("table.load.votable", paramMap);
			hubconnector.getConnection().notify(client.getId(),msg);
		} catch (Exception e) {
			System.err.println(e);
		}

	}

	private ServerResource createResource( final StarTable table ) {
		return new ServerResource() {
                public long getContentLength() {
                    return -1L;
                }
                public String getContentType() {
                    return writer.getMimeType();
                }
                public void writeBody( OutputStream out ) throws IOException {
                   writer.writeStarTable( table, out );
                }
            };
	}

	private Vector getVOTableReceiverApplications() {
		Vector recApps = new Vector();
		Map clients = hubconnector.getClientMap();
		Collection<Client> clientcoll = clients.values();
		Iterator it = clientcoll.iterator();
		while (it.hasNext()) {
			Client client = (Client) it.next();
			Subscriptions subs = client.getSubscriptions();
			if (subs.isSubscribed("table.load.votable")) {
				//System.out.println(client.getMetadata().getName());
				if (!client.getMetadata().getName().equals(VO_NAME))
					recApps.add(client);
			}
		}
		return recApps;
	}


    public int getHeight()   { return height; }
    public int getSizeDiff() { return sizeDiff; }
    public void setSizeDiff(int diff) { sizeDiff=diff; }

    public void createFileMenu() {
	menuFile.removeAll(); // first clear the file menu
	i_NewProject = XMenuHelper.addMenuItem( 
	    menuFile, "_New Project", 'N', actionProcCmd );
	i_LoadProject= XMenuHelper.addMenuItem( 
	    menuFile, "L_oad Project", 'O', actionProcCmd );
	i_SaveProject= XMenuHelper.addMenuItem( 
	    menuFile, "_Save Project", 'S', actionProcCmd );
	i_SaveProjectAs = XMenuHelper.addMenuItem( 
	    menuFile, "Save Project As", actionProcCmd );
	menuFile.addSeparator();
	files = new JMenu( "Recent project-files" );
	menuFile.add( files );
	menuFile.addSeparator();
	JMenu importMenu = new JMenu( "Import" );
	menuFile.add(importMenu);
	i_ImportTimeString = XMenuHelper.addMenuItem( 
	    importMenu, "_Time string", 'T', actionProcCmd );
	i_ImportFrequencies = XMenuHelper.addMenuItem(
	    importMenu, "_Frequencies",'F', actionProcCmd );
	menuExport = new JMenu( "Export" );
	menuFile.add(menuExport);
	i_ExportTimeString = XMenuHelper.addMenuItem( 
	    menuExport, "Time string data", actionProcCmd );
	i_ExportFrequencies = XMenuHelper.addMenuItem( 
	    menuExport, "Frequencies", actionProcCmd );
	i_ExportFrequenciesLatex = XMenuHelper.addMenuItem( 
	    menuExport, "Frequencies (Latex table)", actionProcCmd );
	menuVoTools = new JMenu( "VO Tools" );
	menuVoTools.setEnabled(false);
	menuFile.add(menuVoTools);
	menuSendTS = new JMenu( "Send Time String to ..." );
	menuVoTools.add(menuSendTS);
	i_SaveLogfile = XMenuHelper.addMenuItem( 
	    menuExport, "Log file", actionProcCmd );
	menuFile.addSeparator();
	i_ExpertMode = XMenuHelper.addCheckBoxMenuItem(
	    menuFile, "Expert mode", actionProcCmd);
	if (expertMode)
	    i_ExpertMode.setSelected(true);
	menuFile.addSeparator();
	i_Quit = XMenuHelper.addMenuItem( 
	    menuFile, "_Quit", 'Q', actionProcCmd );

	updateRecentFiles();
    }
    
    public void updateRecentFiles() {
	//--- set up recent files
	int recent = Period04.getNumberOfRecentFiles();
	if (recent>0) {
	    String [] rf = Period04.getRecentFiles();
	    Period04.recentFiles = new File[recent];
	    i_RecentFiles        = new JMenuItem[recent];
	    if (files!=null) 
		files.removeAll();         // remove all items from the menu
	    for (int i=0; i<recent; i++ ) {
		Period04.recentFiles[i] = new File(rf[i]);
		i_RecentFiles[i] = XMenuHelper.addMenuItem(
		    files, Period04.recentFiles[i].getName(), actionProcCmd);
		i_RecentFiles[i].setToolTipText(
		    Period04.recentFiles[i].getPath());
	    }
	    files.repaint();
	}
    }

    public void createSpecialMenu() {
	menuSpecial.removeAll(); // first clean the special menu
	if (expertMode) {
	    i_ManageData = XMenuHelper.addMenuItem( 
		menuSpecial, "Data _Manager", 'M', actionProcCmd );
	    datamanager = new XDialogDataManager(this);
	}
	i_WeightSelection = XMenuHelper.addMenuItem( 
	    menuSpecial, "_Weight selection", 'W', actionProcCmd );
	menuSpecial.addSeparator();
	i_SubdivideTS     = XMenuHelper.addMenuItem( 
	    menuSpecial, "Subdivide time string", actionProcCmd );
	i_CombineSubStrings = XMenuHelper.addMenuItem( 
	    menuSpecial, "Combine substrings", actionProcCmd );
	i_ShowTimes       = XMenuHelper.addMenuItem( 
	    menuSpecial, "Show time structuring", actionProcCmd );
	if (expertMode)
	    i_CalcNyquist = XMenuHelper.addMenuItem(
		menuSpecial,"Calculate specific Nyquist frequency",actionProcCmd);
	i_AdjustTS        = XMenuHelper.addMenuItem( 
	    menuSpecial, "Adjust time string", 'A', actionProcCmd );
//	i_RestoreZeroAdj  = XMenuHelper.addMenuItem( 
//	    menuSpecial, "Restore zero-point adjustments", actionProcCmd );
//	i_DeleteDefaultLabel = XMenuHelper.addMenuItem( 
//	    menuSpecial, "Set default label for deleted points",actionProcCmd);
	i_DeletePoints       = XMenuHelper.addMenuItem( 
	    menuSpecial, "Delete selected points", actionProcCmd);
	menuSpecial.addSeparator();
	i_SelectAllFreqs     = XMenuHelper.addMenuItem(
	    menuSpecial, "Select all frequencies", actionProcCmd );
	i_DeselectAllFreqs     = XMenuHelper.addMenuItem(
	    menuSpecial, "Deselect all frequencies", actionProcCmd );
	i_CleanAllFreqs      = XMenuHelper.addMenuItem( 
	    menuSpecial, "Clean all frequencies", actionProcCmd );
 	i_CleanDeselectedFreqs = XMenuHelper.addMenuItem( 
 	    menuSpecial, "Remove deselected frequencies", actionProcCmd );
 	i_FreqSortMenu = new JMenu( "Sort frequencies" );
 	i_SortFreqs    = new JMenuItem[3];
 	i_SortFreqs[0] = XMenuHelper.addMenuItem(
 	    i_FreqSortMenu, "Sort according to ID", actionProcCmd );
 	i_SortFreqs[1] = XMenuHelper.addMenuItem(
 	    i_FreqSortMenu, "Sort according to frequency", actionProcCmd );
 	i_SortFreqs[2] = XMenuHelper.addMenuItem(
 	    i_FreqSortMenu, "Sort according to amplitude", actionProcCmd );
 	menuSpecial.add(i_FreqSortMenu);
	if (expertMode)
	    i_UseCombiFreqs  = XMenuHelper.addCheckBoxMenuItem(
							       menuSpecial, "Use frequency values instead of combination strings",
							       actionProcCmd );
	i_Epochs             = XMenuHelper.addMenuItem( 
	    menuSpecial, "Calculate epochs", actionProcCmd );
	i_RecalcResiduals    = XMenuHelper.addMenuItem( 
	    menuSpecial, "Recalculate residuals", actionProcCmd);
	i_PredictSignal      = XMenuHelper.addMenuItem( 
	    menuSpecial, "Predict signal", actionProcCmd );
	i_CreateArtData      = XMenuHelper.addMenuItem( 
	    menuSpecial, "Create artificial data", actionProcCmd );
	/*if (expertMode)
	    i_SetAliasGap    = XMenuHelper.addMenuItem( 
		menuSpecial, "Set alias-gap", actionProcCmd );*/
	i_AnalyticalErrors   = XMenuHelper.addMenuItem(
		menuSpecial, "Show analytical uncertainties", actionProcCmd );
	menuSpecial.addSeparator();
	i_CalcNoiseAtFreq    = XMenuHelper.addMenuItem( 
	    menuSpecial, "Calculate noise at frequency", actionProcCmd);
	i_CalcNoiseSpectrum  = XMenuHelper.addMenuItem( 
	    menuSpecial, "Calculate noise spectrum", actionProcCmd);
	}


    public void createConfigMenu() {
	menuConfig.removeAll();
	i_FourierSettings = XMenuHelper.addMenuItem( 
	    menuConfig, "Fourier Settings", actionProcCmd);
    }

    public void showExpertMenuItems(boolean show) {
	if (show) {
	    menuConfig.add(fit);
	    menuBar.validate();
	} else {
	    menuConfig.remove(fit);
	    menuBar.validate();
	}
    }

    public void setExpertMode(boolean isExpert) {
	if (isExpert==expertMode) { return; }
	expertMode = isExpert;
	createFileMenu();             // recreate the file menu
	createSpecialMenu();          // recreate the special menu
	showExpertMenuItems(isExpert);
	Period04.setExpertMode(isExpert);
	//--- if we are switching to the normal mode, make sure that the
	//--- periodic time shift mode is deactivated 
	if (!isExpert && panelFit.getFitMode()==XTabFit.BINARY_MODE)
	    panelFit.setFitMode(XTabFit.NORMAL_MODE);
	updateMenu();
    }			

    public void showFourierSettings() {
	new XDialogFourierSettings(this);
    }

    public void showAbout() {
	JFrame about       = new JFrame("Period04 - About");
	//---
	//--- now create the dialog
	//---

  // for release versions
//  String program_version = "1.2.1";
//  String release_date = "2011-01-18";

      // for non-release versions
//      String program_version = "$Revision: 100 $";
//      String version = "version";
//      String program_version = Period04.command( version);
//      String release_date = "$Date: 2012-07-24 09:46:10 +0200 (Di, 24 Jul 2012) $";


      JLabel text = new JLabel
        ( "<html><center><table width=\"100%\">"
        + "<tr><td align=\"center\">Period04</td></tr>"
        + "</table></center>"
        + "<table><tr><td width=\"120\" align=\"right\">"
        + "Version Number:</td><td width=\"120\">"
//        + program_version
        + Period04.command( "version")
        + "</td></tr><tr><td align=\"right\">"
        + "Released on:</td><td>"
//        + release_date
        + Period04.command( "date")
        + "</td></tr>"
        + "<tr><td align=\"right\">Author:</td>"
        + "<td>Patrick Lenz</td></tr>"
	+ "<tr><td>With contributions of:</td><td>Christian Bucher</td></tr></table></html>"
        )
      ;

      Container content = about.getContentPane();
      content.add(text);
      about.pack();                     // fit frame size
      Dimension dim = about.getSize();  // add 20 pixels
      about.setSize(dim.width, dim.height+20);
      about.setResizable(false);
      about.setLocationRelativeTo(frame);
      about.setVisible(true);
    }

    public void newProject(boolean nocheck) {
	//--- if project has changed ask for confirmation
	if ( (!nocheck) && (hasChanged()) ) { 
	    int value = JOptionPane.showConfirmDialog(frame,
		"Do you really want to delete the current project?",
		"Delete current project?", JOptionPane.YES_NO_OPTION);
	    if (value == JOptionPane.NO_OPTION) {
		return; 
	    }
	}
	setTab(0); // set timestring tab
	panelLog.cleanProtocol(true);
	panelFit.cleanFrequencyList(true);
	panelFit.cleanErrorList();
	Period04.projectNewProject();
	frame.setTitle("Period04 ["+Period04.command( "version")+"]:");  // set default frame title
	projectFile = "";
	Period04.projectSetFitMode(   // set current fitmode 
				   panelFit.getFitMode()); 
	updateDisplays();             // update fourier, fit and log display
	panelTimeString.updateFileBox(); // update filebox
	noChanges();
    }
 
    public boolean hasChanged() {
	if (Period04.projectHasChanged()!=0 ||
	    panelTimeString.hasSelectionChanged())
	    return true;
	return false;
    }

    public void noChanges() {
	Period04.projectNoChanges();            // set c++ part unchanged
	panelTimeString.resetSelectionChanged(); // set java part unchanged
    }

    public void cleanProject(boolean fourier, boolean periods,
			     boolean timestring, boolean log) {
	//--- clean/update the user interface
	if (periods)    { panelFit.cleanFrequencyList(true); }
	if (timestring) { panelTimeString.cleanFileBox(); }
      	if (log) { panelLog.cleanProtocol(true); }
	else { 
	    StringBuffer text = new StringBuffer("Clean Project:\n");
	    if (timestring) { text.append("Cleaned timestring data\n"); }
	    if (periods)    { text.append("Cleaned period data\n"); }
	    if (fourier)    { text.append("Cleaned Fourier data\n"); }
	    getLog().writeProtocol(text.toString(),true); 
	}
	//--- clean the database
	Period04.projectClean(fourier, periods, timestring);
	Period04.projectSetChanged();
	//--- finally update the displaysdf
	updateDisplays();
	updateDataManager();
    }

    public void processBatchFile(File batch) {
	// decode batch file
	try {
	    BufferedReader br = new BufferedReader(new FileReader(batch)); 
	    String sline = br.readLine();
	    while (sline!=null) {
		if (sline.startsWith("#")) { // skip comment line
		    sline = br.readLine();
		    continue;
		}
		StringTokenizer st = new StringTokenizer(sline);
		if (!st.hasMoreTokens()) {
		    sline = br.readLine();
		    continue;
		}
		// wait for potential file/calculation task to finish
		while (Period04.isWorking()) {
		    try {
			Thread.sleep(100);
		    }
		    catch (Exception e)
			{}
		}
		String command = st.nextToken();
		if (command.equals("import")) {
		    String fileformat = st.nextToken();
		    String file = st.nextToken();
		    panelTimeString.importTimeStringBatch(file,fileformat);
		    //System.out.println("done");
		}
		if (command.equals("importfreqs")) {
		    String file = st.nextToken();
		    panelFit.importFrequenciesBatch(file);
		}
		if (command.equals("loadproject")) {
		    String file = st.nextToken();
		    loadProjectBatch(file);
		}
		if (command.equals("fourier")) {
		    String sfrom = st.nextToken();
		    String sto = st.nextToken();
		    String swhat = st.nextToken();
		    String scompact = st.nextToken();
		    panelFourier.calculateFourierBatch(sfrom,sto,swhat,scompact);
		    tabbedFrame.setSelectedIndex(2);
		}		
		if (command.equals("fit")) {
		    String swhat = st.nextToken();
		    String smode = st.nextToken();
		    panelFit.calculateFitBatch(swhat,smode);
		    tabbedFrame.setSelectedIndex(1);
		}
		if (command.equals("addharmonics")) {
		    String snum = st.nextToken();
		    panelFit.addHarmonicsBatch(snum);
		}
		if (command.equals("addsubharmonic")) {
		    String snum = st.nextToken();
		    panelFit.addSubHarmonicBatch(snum);
		}
		if (command.equals("createartificial"))	{
		    String sfilemode = st.nextToken();
		    if (sfilemode.equals("f")) {
			String sinfile = st.nextToken();
			String sfile = st.nextToken();
			String sappend = st.nextToken();
			panelFit.createArtificialDataBatch(true,sinfile,"","","","",sfile,sappend);
		    } else {
			String sstart = st.nextToken();
			String send = st.nextToken();
			String sstep = st.nextToken();
			String sleading = st.nextToken();
		       	String sfile = st.nextToken();
			String sappend = st.nextToken();
			panelFit.createArtificialDataBatch(false,"",sstart,send,sstep,sleading,sfile,sappend);
		    }
		}
		if (command.equals("calcsnr")) {
		    String boxsize = st.nextToken();
		    String steprate = st.nextToken();
		    String what = st.nextToken();
		    String file = "";
		    try { 
			file = st.nextToken();
		    } catch (NoSuchElementException e) {
			// do nothing	
		    }

		    if (file!="") {
			try (PrintWriter out = new PrintWriter(file)) {
			    out.println(calculateSNRBatch(boxsize,steprate,what));
			}
		    }
		    else
		    {
			System.out.println(calculateSNRBatch(boxsize,steprate,what));
		    }
		    
		}
		if (command.equals("calcerrors")) {
		    String mode = st.nextToken();
		    String file = "";
		    try { 
			file = st.nextToken();
		    } catch (NoSuchElementException e) {
			// do nothing	
		    }
		    if (mode.equals("a")) {
			if (file!="") {
  			    try (PrintWriter out = new PrintWriter(file)) {
				out.println(Period04.projectGetAnalyticalErrors());
			    }
			}
			else
			{
			    System.out.println(Period04.projectGetAnalyticalErrors());
			}
		    } else if (mode.equals("ls")) {
			String swhat = st.nextToken();
		    	String smode = st.nextToken();
			String shiftmode = "";
			    try { 
				shiftmode = st.nextToken();
			    } catch (NoSuchElementException e) {
				// do nothing	
			    }

		        Period04.projectSetShiftTimes(!shiftmode.equals("t0"));

			panelFit.calculateFitBatch(swhat,smode);
		
			if (file!="") {
  			    try (PrintWriter out = new PrintWriter(file)) {
				out.println(Period04.projectGetFitError());
			    }
			}
			else
			{
			    System.out.println(Period04.projectGetFitError());
			}
		    } else if (mode.equals("mc")) {

			int processes = 100;
			String processlbl = st.nextToken();
			try {
			    processes = Integer.parseInt(processlbl);
			} catch (Exception e)
			{
			    System.out.println("Could not parse number of MC processes!");
			}

			boolean sysTime = false;
			boolean shiftTime = true;

			while (st.hasMoreTokens()) {
			    String token = st.nextToken();
			    switch (token)
			    {
				case "r":
					sysTime=true;
					break;
				case "t0":
					shiftTime = false;
					break;
			    }
			}

			panelFit.calculateMonteCarloErrorsBatch(processes,shiftTime,sysTime);   

			if (file!="") {
  			    try (PrintWriter out = new PrintWriter(file)) {
				out.println(Period04.projectGetFitError());
			    }
			}
			else
			{
			    System.out.println(Period04.projectGetFitError());
			}
		    }
		}
		if (command.equals("savefreqs")) {
		    String sfile = st.nextToken();
		    panelFit.exportFrequenciesBatch(sfile);
		}
		if (command.equals("savefourier")) {
		    String sfrom = st.nextToken();
		    String sto = st.nextToken();
		    String swhat = st.nextToken();
		    String scompact = st.nextToken();
		    String sfile = st.nextToken();
		    panelFourier.calculateFourierBatch(sfrom,sto,swhat,scompact);
		    panelFourier.updateList();
		    panelFourier.exportActiveFourierBatch(sfile);
		}
		if (command.equals("saveproject")) {
		    String sfile = st.nextToken();
		    saveProjectBatch(sfile);
		}
		if (command.equals("exit")) {
		    shutdownBatch();
		}
		sline = br.readLine();
	    }
		  
	    br.close();
	
	} catch (IOException e) {
	    System.err.println("error reading batch file! ("+e.toString());
            if (Period04.isMacOS) { System.err.println("Please add absolute path to batch file"); }
	}		 
    }

    public void loadProject(File project) {
	// ask for confirmation to clean project in case
	if (hasChanged()) {
	    int value = JOptionPane.showConfirmDialog(frame,
		"Are you sure? The current project will be deleted!\n"+
		"Unsaved project data will get lost!",
		"Delete current project?", JOptionPane.YES_NO_OPTION);
	    if (value == JOptionPane.YES_OPTION) {
		// proceed 
		Period04.projectNoChanges();    // sinnvoll hier???
	    } else {
		return; // we may not delete old settings
	    }
	}
	if (project==null) { // ask for filename
	    initializeFileChooser();
	    int returnVal = fc.showOpenDialog(frame);
	    if (returnVal == JFileChooser.APPROVE_OPTION) {
		project = fc.getSelectedFile();   // get the file name
	    } else {
		return;  // canceled, do nothing
	    }
	}
	tabbedFrame.setSelectedIndex(0);// set timestring panel selected
	panelLog.cleanProtocol(false);
	panelFit.cleanFrequencyList(true);
	panelTimeString.cleanLists();
	panelTimeString.cleanFileBox();

	// save filename in variable projectFile
	projectFile = project.getPath();
	Period04.setCurrentDirectory(project);
	Period04.newRecentFile(projectFile);
	
	// change frame title
	setNewTitle(project.getName());
	setReading(true); // create a progress bar

	// now load the project
	filetask = new FileTask(this, FileTask.PROJECT_IN, 
					   panelTimeString, panelFit,
					   panelLog, project.getPath());
	filetask.go();        // start calculation
	startTimer(filetask); // start the timer for the progressbar
    }

    public void loadProjectBatch(String file) {
	// ask for confirmation to clean project in case
	File project = new File(file);
	tabbedFrame.setSelectedIndex(0);// set timestring panel selected
	panelLog.cleanProtocol(false);
	panelFit.cleanFrequencyList(true);
	panelTimeString.cleanLists();
	panelTimeString.cleanFileBox();

	// save filename in variable projectFile
	projectFile = project.getPath();
	Period04.setCurrentDirectory(project);
	Period04.newRecentFile(projectFile);
	
	// change frame title
	setNewTitle(project.getName());
	setReading(true); // create a progress bar

	// now load the project
	filetask = new FileTask(this, FileTask.PROJECT_IN, 
					   panelTimeString, panelFit,
					   panelLog, project.getPath());
	filetask.go();        // start calculation
	startTimer(filetask); // start the timer for the progressbar
    }

    public void saveProject() {
	if (!hasChanged()) { 
	    JOptionPane.showMessageDialog(
		frame, "The project has not been changed,\n"+
		"so no need to save it...", "",
		JOptionPane.INFORMATION_MESSAGE);
	    return;
	}

	// copy the protocol text into c++ part
	Period04.projectSaveProtocolText(panelLog.getProtocolText());

	// if no filename is specified then ask for it
	if ( (projectFile=="") || (projectFile==null)) {
	    initializeFileChooser();
	    fc.setDialogType(JFileChooser.SAVE_DIALOG);
	    int returnVal = fc.showSaveDialog(frame);
	    if (returnVal == JFileChooser.APPROVE_OPTION) {
		// get the file name
		File file = fc.getSelectedFile();
		// check if the extension is missing
		String path = file.getPath();
		String filename = file.getName();
		if ( (!path.endsWith(".p04")) && (!path.endsWith(".p98")) ) {
		    path += ".p04";
		    filename += ".p04";
		}
		// check wether user wants to overwrite an existing file
		if (file.exists()) {
		    int v = JOptionPane.showConfirmDialog(
			frame, "File does already exist!\n"+
			"Do you really want to delete the old file?", 
			"Warning", JOptionPane.YES_NO_OPTION);
		    if (v == JOptionPane.NO_OPTION) {
			return;
		    } 
		}
		// save filename in variable projectFile
		projectFile = path; 
		Period04.setCurrentDirectory(new File(path));
		Period04.newRecentFile(path);
		// change frame title
		setNewTitle(filename);
		setSaving(true); // create progress bar
		// start the task to save the project
		filetask = new FileTask(this, FileTask.PROJECT_OUT, 
					path, false);
		filetask.go();        // start saving
	    } else {
		return;  // canceled, do nothing
	    }
	} else if (projectFile!="") {
	    setSaving(true); // create progress bar
	    filetask = new FileTask(this, FileTask.PROJECT_OUT, 
				    projectFile, true);
	    filetask.go();        // start saving
	}
    }

    public void saveProjectBatch(String fname) {
	if (!hasChanged()) { 
	    return;
	}

	// copy the protocol text into c++ part
	Period04.projectSaveProtocolText(panelLog.getProtocolText());

	File file = new File(fname);

	// check if the extension is missing
	String path = file.getPath();
	String filename = file.getName();
	if ( (!path.endsWith(".p04")) && (!path.endsWith(".p98")) ) {
	    path += ".p04";
	    filename += ".p04";
	}
	
	// save filename in variable projectFile
	projectFile = path; 
	Period04.setCurrentDirectory(new File(path));
	Period04.newRecentFile(path);
	// change frame title
	setNewTitle(filename);
	setSaving(true); // create progress bar
	// start the task to save the project
	filetask = new FileTask(this, FileTask.PROJECT_OUT, 
				path, false);
	filetask.go();        // start saving
    }
 
    public void saveProjectAs() {
	  projectFile="";
	  Period04.projectSetChanged();	  // make it have changed...
	  saveProject();	          // and save it...
    }

    public String getProjectFile() {
	return projectFile;
    }

    public void setProjectFile(File newfile) {
	projectFile = newfile.getPath();
	frame.setTitle(newfile.getName());
	Period04.newRecentFile(newfile.getPath());
    }

    public void importTimeString(boolean append) {
		panelTimeString.importTimeString(append, null);
    }

    public void showImportTimeStringDialog() {
		new XDialogTSImport(this, panelTimeString, true, null);
    }

    public void exportTimeString() {
	panelTimeString.exportTimeString();
    }
    
    public void importFrequencies() {
	panelFit.importFrequencies();
    }

    public void exportFrequencies(boolean isUseLatex) {
	panelFit.exportFrequencies(isUseLatex);
    }

	public void deleteDefaultLabel() {
		panelTimeString.deleteDefaultLabel();
	}

    public void showAnalyticalErrors() {
	panelFit.showAnalyticalErrors();
    }

    public void calculateNoiseAtFrequency() {
	panelFourier.calculateNoiseAtFrequency();
    }

    public String calculateSNRBatch(String boxsize, String stepquality, String swhat) {
	String log = "";

	double bs = 2.0;
	try {
	    bs = Double.valueOf(boxsize);
	} catch (NumberFormatException e) {
	    log = "\nError: could not parse box size";
	    return log;
	}

	int sq = 0;
	try {
	    sq = Integer.valueOf(stepquality);
	} catch (NumberFormatException e) {
	    log = "\nError: could not parse step quality";
	    return log; 
	}
	if (sq>3 || sq<0) {
	    log = "\nError: incomprehensible step quality, expecting [0,1,2]";
	    return log;
	}

	int datamode = -1;
	switch(swhat.charAt(0)) {
	case 'o': datamode=0; break;
	case 'a': datamode=1; break;
	case 'r': datamode=4; break;
	case 'R': datamode=5; break;
	case '1': datamode=2; break;
	case '2': datamode=3; break;
	}   

	double zero=0;
	if ((datamode==0)||(datamode==1)) {
	    Period04.projectSetDataMode(datamode);           // set new mode
	    zero = Period04.timestringAverage(0);  // get zeropoint
	    //zero = Period04.timestringAverage(this.weight);  // get zeropoint
	}

	log += "# Freq\tNoise\tSNR\n";
	for (int i=0; i<Period04.projectGetActiveFrequencies(); i++) {
	    Period04.projectCalculateNoiseAtFrequency(
		    Period04.projectGetFrequencyValue(i), bs, sq, 0, 
		    datamode, zero);

	    log += Period04.projectGetNoiseString(0);
	    double snr = Period04.projectGetAmplitudeValue(i)/
		 Period04.projectGetNoiseValue();
	    log += "\t"+snr+"\n";
	}

	return log;
    }

     

  private void initializeFileChooser ( )
  {
    // create FileChooser
    fc = new JFileChooser( Period04.getCurrentDirectory( ));

    fc.setFileFilter( new FileFilter ( )
      {
        public boolean accept ( File f)
        {
          return f.isDirectory( )
              || f.getName( ).toLowerCase( ).endsWith( ".p04")
              || f.getName( ).toLowerCase( ).endsWith( ".p98")
          ;
        }

        public String getDescription ( )
        {
          return "Project files(*.p98,*.p04)";
        }
      }
    );
  }

    public void updateDisplays() {
		panelFourier.updateDisplay();
		//--- updating the frequency lists does set the project to have changed
		//--- here is a workaround to maintain the "no changes" status
		int before = Period04.projectHasChanged();
		panelFit.updateDisplay(false /* but don't clean the frequency list */);
		if (before==0) { Period04.projectNoChanges(); }
		panelLog.updateDisplay();
		panelTimeString.updateDisplay(); // update timestring display as well
		updateDataManager();
		updatePlots();
   }

	public void updateMenu() {
		if (Period04.projectGetSelectedPoints()==0) {
			menuSendTS.setEnabled(false);
			menuExport.setEnabled(false);
			for (int i=0; i<menuSpecial.getItemCount(); i++) {
				if (menuSpecial.getItem(i)!=null)
					menuSpecial.getItem(i).setEnabled(false);
			}
			// some exceptions
			i_CreateArtData.setEnabled(true);
			i_CleanAllFreqs.setEnabled(true);
			i_CleanDeselectedFreqs.setEnabled(true);
			i_SelectAllFreqs.setEnabled(true);
			i_DeselectAllFreqs.setEnabled(true);
			i_FreqSortMenu.setEnabled(true);
		} else {
			menuSendTS.setEnabled(true);
			menuExport.setEnabled(true);
			for (int i=0; i<menuSpecial.getItemCount(); i++) {
				if (menuSpecial.getItem(i)!=null)
					menuSpecial.getItem(i).setEnabled(true);
			}
			updateVOMenu();
		}
	}

    public void updateDataManager() {
	if (datamanager==null) { return; }
	datamanager.update();
    }

    public void updateTSDisplay() {
	panelTimeString.updateDisplay(); // update timestring display as well
    }

    public void updateFitDisplay() {
	panelFit.updateDisplay(false /* but don't clean the frequency list */);
    }

	public void updateFourierDisplay() {
		panelFourier.updateDisplay();
	}

    public void updateNyquist() {
	panelFourier.updateNyquist();
    }

    public void updateExpertMode() {
	setExpertMode(Period04.isExpertMode());
	i_ExpertMode.setSelected(Period04.isExpertMode());
    }

    public void updatePlots() {
	panelTimeString.updateTSPlot();
	panelFit       .updatePhasePlot();
    }

    public void dereferenceTSPlot() {
	panelTimeString.dereferenceTSPlot();
    }

    public void dereferencePhasePlot() {
	panelFit.dereferencePhasePlot();
    }

    public void dereferenceFileTask() {
	filetask = null;
    }

    public void cleanAllFrequencies() {
	panelFit.cleanAllFrequencies();
    }

    public void cleanProtocol(boolean printHeader) {
	panelLog.cleanProtocol(printHeader);
    }

    public void cleanFourier() {
	Period04.projectCleanFourier();
    }

    public void expandFourierPanel(boolean expand) {
	if (expand) {
	    //--- remove the fourier tab
	    tabbedFrame.remove(tabFourier);
	    tabFourier = null;
	    //--- create a extern frame for the fourier calculation panel
	    w_ExternFourier = new JFrame(
		new File(getProjectFile()).getName()+": Fourier");
	    w_ExternFourier.setDefaultCloseOperation(
		JFrame.DO_NOTHING_ON_CLOSE);
	    w_ExternFourier.addWindowListener( 
		new WindowAdapter() {
		    public void windowClosing(WindowEvent we) {
			w_ExternFourier.dispose();
			expandFourierPanel(false);
			panelFit.setFourierExtern(false);
			panelFit.flipFourier();
		    }
		    public void windowActivated(WindowEvent we) {
			updateCurrentProject();
		    }
		});
	    w_ExternFourier.addComponentListener
		(new ExternJFrameSizeListener());
	    w_ExternFourier.getContentPane().add(
		panelFourier.getMainPanel());
	    int width = frame.getWidth();
		w_ExternFourier.setSize(width,frame.getHeight()-50);
	    //--- position the windows
	    Point pt = frame.getLocation();
	    Dimension screenDim = 
		Toolkit.getDefaultToolkit().getScreenSize();
	    int factor = 2;
	    if (panelFit.isTSExtern()) { factor++; } // there are 3 windows!
	    if ((pt.x+2*width)>screenDim.width &&
		(factor*width)<screenDim.width) {
		//--- we need some more space on the right side, so move
		//--- both frames to the left:
		//--- move the mainframe
		int newPosition = pt.x-(pt.x+2*width-screenDim.width);
		frame.setLocation(newPosition, pt.y);
		//--- now position the fourier frame
		w_ExternFourier.setLocation(newPosition+width, pt.y+50);
		//--- and timestring frame (if neccessary)
		if (panelFit.isTSExtern()) {
		    w_ExternTimeString.setLocation(newPosition-width, pt.y+50);
		}
	    } else {
		//--- in the other cases
		w_ExternFourier.setLocation(pt.x+width, pt.y+50);
	    }
	    w_ExternFourier.setVisible(true);
	} else {
	    //--- close the extern window 
	    w_ExternFourier.dispose();
	    w_ExternFourier = null;
	    //--- activate the fourier tab again and put it on the right place
	    tabFourier = panelFourier.getMainPanel();
	    if (tabbedFrame.indexOfComponent(tabTimeString)==0) 
		tabbedFrame.add(tabFourier, "    Fourier    ", 2);
	    else
		tabbedFrame.add(tabFourier, "    Fourier    ", 1);
	    if (isWorking()) { // user closed fourier window ...
		tabbedFrame.setEnabledAt(
		    tabbedFrame.indexOfComponent(tabFourier), false);
		tabbedFrame.setSelectedComponent(panelCalculating);
	    }
	    //--- size the fourier panel properly
	    panelFourier.fitSize();     
	}
    }

    public void expandTimeStringPanel(boolean expand) {
	if (expand) {
	    //--- remove the fourier tab
	    tabbedFrame.remove(tabTimeString);
	    tabbedFrame.setSelectedComponent(tabFit);
	    tabTimeString = null;
	    //--- create a extern frame for the timestring panel
	    w_ExternTimeString = new JFrame(
		new File(getProjectFile()).getName()+": Time string");
	    w_ExternTimeString.setDefaultCloseOperation(
		JFrame.DO_NOTHING_ON_CLOSE);
	    w_ExternTimeString.addWindowListener( 
		new WindowAdapter() {
		    public void windowClosing(WindowEvent we) {
			w_ExternTimeString.dispose();
			expandTimeStringPanel(false);
			panelFit.setTSExtern(false);
			panelFit.flipTS();
		    }
		    public void windowActivated(WindowEvent we) {
			updateCurrentProject();
		    }
		});
	    w_ExternTimeString.addComponentListener
		(new ExternJFrameSizeListener());
	    w_ExternTimeString.getContentPane().add(
		panelTimeString.getMainPanel());
	    int width = frame.getWidth();
		w_ExternTimeString.setSize(width,frame.getHeight()-50);
	    //--- position the windows
	    Point pt = frame.getLocation();
	    Dimension screenDim = 
		Toolkit.getDefaultToolkit().getScreenSize();
	    int factor = 2;
	    if (panelFit.isFourierExtern()) { factor++; } //there are 3 windows
	    if (pt.x-width<0 && factor*width<screenDim.width) {
		//--- we need some more space on the left side, so move
		//--- both frames to the right:
		//--- move the mainframe
		frame.setLocation(width, pt.y);
		//--- now position the timestring frame
		w_ExternTimeString.setLocation(0, pt.y+50);
		//--- and the fourier frame (if neccessary) 
		if (panelFit.isFourierExtern()) {
		    w_ExternFourier.setLocation(2*width, pt.y+50);
		}
	    } else {
		//--- in the other case
		int x = pt.x-width;
		if (x<0) { x=0; }
		w_ExternTimeString.setLocation(x, pt.y+50);
	    }
	    w_ExternTimeString.setVisible(true);
	} else {
	    //--- close the extern window 
	    w_ExternTimeString.dispose();
	    w_ExternTimeString = null;
	    //--- activate the fourier tab again and put it on the right place
	    tabTimeString = panelTimeString.getMainPanel();
	    tabbedFrame.add(tabTimeString, "  Time string  ", 0);
	    tabbedFrame.setSelectedComponent(tabFit);
	    if (isWorking()) { // user closed timestring window ...
		tabbedFrame.setEnabledAt(
		    tabbedFrame.indexOfComponent(tabTimeString), false);
		tabbedFrame.setSelectedComponent(panelCalculating);
	    }
	    //--- size the timestring panel properly
	    panelTimeString.fitSize();     
	}
    }

	public void setNewTitle(String newName) {
		frame.setTitle("Period04 ["+Period04.command( "version")+"]: "+newName);
		if (w_ExternTimeString!=null)
			w_ExternTimeString.setTitle(newName+": Time string");
		if (w_ExternFourier!=null)
			w_ExternFourier   .setTitle(newName+": Fourier");
	}

    //---
    //--- methods to prevent user input during time consuming file i/o
    //--- or calculations
    //---
    public void setWorking(boolean value) {
	working = value;
	for (int i=0; i<tabbedFrame.getTabCount(); i++) 
	    tabbedFrame.setEnabledAt(i, !value);
	panelTimeString.setCheckBoxesEnabled(!value);
	if (datamanager!=null) 
	    datamanager.setWorking(value);
	Period04.blockProjectWindows(value, projectID);
	//--- after the task has finished activate the project window
	if (value==false) { frame.toFront(); }
    }
    public boolean isWorking() { return working; }

    //---
    //--- setReading()
    //--- adds/removes a progress bar to display the progress
    //---
    public void setReading(boolean value) {
	if (value) {
	    setWorking(true);
	    // create a timer.
	    timer = new Timer(50, new ActionListener() {
		    public void actionPerformed(ActionEvent evt) {
			if (task.isDone()) {
			    timer.stop();
			} else {
			    if (progress!=null)
				progress.setValue(
				    Period04.getProgressBarValue());
			}
		    }
		});
	    progress = new JProgressBar(JProgressBar.HORIZONTAL,0,100);
	    progress.setValue(0);
	    Period04.setProgressBarValue(0);
	    progress.setStringPainted(true);
	    statusBar.add(progress, "progress");
	    statusBarLayout.show(statusBar, "progress");
	} else {
	    statusBarLayout.show(statusBar, "label");
	    setDefaultStatusBarText();
	    statusBar.remove(progress);
	    progress = null;
	    setWorking(false);
	}
    }   

    //---
    //--- setSaving()
    //--- adds/removes a progress bar to display the progress
    //---
    public void setSaving(boolean value) {
	if (value) {
	    setWorking(true);
	    progress = new JProgressBar(JProgressBar.HORIZONTAL,0,100);
	    progress.setIndeterminate(true);
	    progress.setStringPainted(true);
	    progress.setString("Saving data ...");
	    statusBar.add(progress, "progress");
	    statusBarLayout.show(statusBar, "progress");
	} else {
	    statusBarLayout.show(statusBar, "label");
	    setDefaultStatusBarText();
	    statusBar.remove(progress);
	    progress = null;
	    setWorking(false);
	}
    }   

    //---
    //--- setCalculating(...)
    //--- adds/removes a tab to display the progress of a calculation
    //---
    public void setCalculating(boolean value, JPanel settingspanel,
			       String s_label, int interval, 
			       boolean showProgressBar) {
	if (value) {
	    setWorking(true);
	    blockStateChangedEvent = true;
	    initializeCalcPanel(settingspanel, s_label, interval, showProgressBar);
	    Period04.cancel    = 0;
	    Period04.completed = 0;
	    // save the index of the activated tab
	    activeTab = tabbedFrame.getSelectedIndex();  
	    tabbedFrame.addTab(" Calculating ", null, panelCalculating);
	    tabbedFrame.setSelectedComponent(panelCalculating);
	} else if (!value) {
	    tabbedFrame.setSelectedIndex(activeTab);  // select fourier tab
	    tabbedFrame.remove(panelCalculating);
	    panelCalculating = null;  // dereference the calculation panel
	    blockStateChangedEvent = false;
	    setWorking(false);
	}
    }
	
    //---
    //--- block any user input
    //---
    public void blockProject(boolean value, boolean externalWindowsOnly) {
	if (!externalWindowsOnly) {	
	    frame.enable(value);
	}
	if (w_ExternTimeString!=null) { w_ExternTimeString.enable(value); }
	if (w_ExternFourier!=null)    { w_ExternFourier.enable(value); }
    }

    //---
    //--- show the requested Tab
    //---
    public void setTab(int i) {
	tabbedFrame.setSelectedIndex(i); 
    }

    //---
    //--- initializeCalcPanel(...)
    //--- creates the panel for the calculation tab
    //---
    private void initializeCalcPanel(JPanel settingspanel, String s_label, 
				     int interval, boolean showProgressBar) {
	panelCalculating = new JPanel();
	JLabel label = new JLabel(s_label);

	b_cancel = new JButton("Cancel Calculation");
	b_cancel.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    stopCalculation();
		}
	    });

	int x0 = 100;
	if (Period04.isMacOS) { x0=115; }
	int x1 = x0+140;
	label        .setBounds( x0-10,  10, 300, 20);
	settingspanel.setBounds( x0-20,  80, 320, 155);
	b_cancel     .setBounds( x0-10, 250, 300, 30);
	
	if (showProgressBar) {
	    progress = new JProgressBar(0,100);
	    progress.setValue(0);
	    progress.setStringPainted(true);
	    progress     .setBounds( x0-10,  40, 300, 20);
	    panelCalculating.add(progress);
	    // create a timer.
	    timer = new Timer(interval, new ActionListener() {
		    public void actionPerformed(ActionEvent evt) {
			progress.setValue(Period04.getProgressBarValue());
			if (task.isDone()) {
			    timer.stop();
			}
		    }
		});
	} else {
	    progressLabel = new JLabel("Starting calculation ...",
				       JLabel.CENTER);
	    progressLabel.setBorder(BorderFactory.createEtchedBorder());
	    progressLabel.setBounds( x0-10,  40, 300, 25);
	    panelCalculating.add(progressLabel);
	    // create a timer.
	    timer = new Timer(interval, new ActionListener() {
		    public void actionPerformed(ActionEvent evt) {
			progressLabel.setText(Period04.getProgressString());
			if (task!=null && task.isDone()) {
			    timer.stop();
			    progressLabel.setText("");
			}
		    }
		});
	}
	
	panelCalculating.setLayout(null);
	panelCalculating.add(label);
	panelCalculating.add(settingspanel);
	panelCalculating.add(b_cancel);
    }

    //---
    //--- startTimer(...)
    //--- starts the calculation task and the timer that will update the
    //--- progress bar in certain intervals
    //---
    public void startTimer(Task task) {
	this.task = task;
	timer.start();
    }
    public void setProgressBarValue(int n) {
	progress.setValue(n);
    }

    public void stopCalculation() {
	b_cancel.setEnabled(false);
	b_cancel.setText("Calculation canceled, please wait ...");
	Period04.setCalculationCanceled(1);
	task.stop();
    }

    //---
    //--- set status bar text
    //---
    public void setDefaultStatusBarText() {
	statusBarLabel.setFont(new Font("Dialog", 0, 10));
	statusBarLabel.setText(" For help press F1");
    }
    public void setStatusBarText(String newText) {
 	statusBarLabel.setText(newText);
    }
    public void setBoldStatusBarText(String newText) {
 	statusBarLabel.setFont(new Font("Dialog", Font.BOLD, 10));
 	statusBarLabel.setText(newText);
    }
   

    public final int getProjectID() { return projectID; }
    public void setCurrentProject() { Period04.setUseProjectID(projectID); }

    public void updateCurrentProject() {
	if (Period04.isWorking()) { return; }
	setCurrentProject();		
    }

    public XTabLog getLog() { return panelLog; }

    public boolean shutdown() {
	Period04.setUseProjectID(getProjectID());
	if (hasChanged()) {
	    int value = JOptionPane.showConfirmDialog(
		frame, 
		"Do you want to save your project?",
		"Project has changed", 
		JOptionPane.YES_NO_CANCEL_OPTION);
	    if (value == JOptionPane.YES_OPTION) {
		saveProject();
		return true;
	    } else if (value==JOptionPane.CANCEL_OPTION) {
		return true;
	    } else {
		if (w_ExternTimeString!=null) { w_ExternTimeString.dispose(); }
		if (w_ExternFourier!=null)    { w_ExternFourier.dispose(); }
		frame.dispose();                  // free resources
		Period04.projectClean(      // clean all data in the c++ part
		    true,true,true);   
		Period04.shutdown(this);    // remove this project
	    }
	} else { 
	    if (w_ExternTimeString!=null) { w_ExternTimeString.dispose(); }
	    if (w_ExternFourier!=null)    { w_ExternFourier.dispose(); }
	    frame.dispose();                      // free resources
	    Period04.projectClean(          // clean all data in the c++ part
		true,true,true);  
	    Period04.shutdown(this);        // remove this project
	}
	return false;
    }
    
    public boolean shutdownBatch() {
	frame.dispose();                      // free resources
	Period04.projectClean(          // clean all data in the c++ part
              true,true,true);  
	Period04.shutdown(this);        // remove this project
	return false;
    }
    
    //---
    //--- resize the panels to fit the new framesize
    //---
    public void fitSize() {
	if (!panelFit.isTSExtern())      { panelTimeString.fitSize(); }
	if (!panelFit.isFourierExtern()) { panelFourier.fitSize(); }
	panelFit.fitSize();
	panelLog.fitSize();
    }


    public XTabTimeString    getTimeStringTab() { return panelTimeString; }
    public XTabFourier       getFourierTab()    { return panelFourier; }
    public XTabFit           getFitTab()        { return panelFit; }
    public static HelpBroker getHelpBroker()    { return hb; }

    //---
    //--- JFrameSizeListener
    //--- a class to listen to ComponentEvents that are caused by resizing 
    //--- the external panels
    //---
    class JFrameSizeListener extends ComponentAdapter
    {
	private JFrame mainframe;
	private XProjectFrame projectframe;
	public JFrameSizeListener(JFrame mainframe, XProjectFrame projectframe) {
	    super();
	    this.mainframe=mainframe;
            this.projectframe = projectframe;
	}
	//---
	//--- methods to listen to ComponentEvents 
	//--- (we are listening to ComponentEvents that are caused by resizing 
	//---  the mainframe)
	//---
	public void componentResized( ComponentEvent e) {
	    //--- the frame has been resized. 
	    //--- set the framewidth back to the default
	    //--- if the frameheight is to low set it back to the default value
	    //--- if the frameheight is ok, then determine the difference to the 
	    //--- default value and call fitSize()
	    //--- this will size the GUI elements to fit to the new frameheight
	    final Component c=e.getComponent();
	    final Dimension frameSize=c.getSize();
	    int height = projectframe.getHeight();
	    if (height<initHeight) { // very low resolution
		frameSize.height = height;
		mainframe.setSize(frameSize);
		projectframe.fitSize();
		return; 
	    } 
	    int sizeDiff;
	    if(frameSize.height<=height) {
		frameSize.height=height;
		sizeDiff=0;
	    } else {
		sizeDiff=height - frameSize.height;
	    }
	    frameSize.width=width;
	    tabbedFrame.resize(frameSize.width,frameSize.height);
	    mainframe.resize(frameSize.width,frameSize.height);
	    projectframe.setSizeDiff(sizeDiff);
	    projectframe.fitSize();
	}
	public void componentHidden( ComponentEvent e) { }
	public void componentShown( ComponentEvent e)  {  }
	public void componentMoved( ComponentEvent e)  {  }
    }
    
    //---
    //--- JFrameSizeListener
    //--- a class to listen to ComponentEvents that are caused by resizing 
    //--- the external panels
    //---
    class ExternJFrameSizeListener extends ComponentAdapter {
	public void componentResized(ComponentEvent e) {
	    //--- the frame has been resized. 
	    //--- set the framewidth back to the default
	    //--- if the frameheight is to low set it back to the default value
	    //--- if the frameheight is ok, then determine the difference to the 
	    //--- default value and call fitSize()
	    //--- this will size the GUI elements to fit to the new frameheight
	    Component c = e.getComponent();
	    Dimension frameSize = c.getSize();
		if (frameSize.height <= height-50) {
		frameSize.height = height-50;
		sizeDiff = 0;
	    } else {
		sizeDiff = height-50-frameSize.height;
	    }
	    frameSize.width = width;
	    c.setSize(frameSize);
	    if (c.equals(w_ExternTimeString))
		panelTimeString.fitSize();
	    if (c.equals(w_ExternFourier))
		panelFourier.fitSize();
	}
	public void componentHidden(ComponentEvent e) {}
	public void componentShown(ComponentEvent e)  {}
	public void componentMoved(ComponentEvent e)  {}
    }
}

