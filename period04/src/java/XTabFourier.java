/*---------------------------------------------------------------------*
 *  XTabFourier.java
 *  creates the Fourier tab
 *---------------------------------------------------------------------*/

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;            // TitledBorder stuff
import javax.swing.event.*;
import javax.swing.filechooser.*;

public class XTabFourier extends XTab
    implements ItemListener, ListSelectionListener
{
    private JPanel           settingsPanel;
    private JButton          b_Weights,  b_Calculate, b_Delete, b_Rename;
    private JButton          b_DispData, b_DispGraph, b_ExportFourier;
    private JRadioButton []  cbDataMode    = new JRadioButton[6];
    private JRadioButton []  cbCompactMode = new JRadioButton[2];
    private JList            fourierList;
    private DefaultListModel fourierListModel;
    private JScrollPane      fourierListPane;
    private JFileChooser     fc;
    private JTextField       t_CalcName, t_From, t_To, t_StepRate, t_Weights;
 
    private String    s_CalcName, s_From, s_To, s_StepRate;   
    private JLabel    l_NyquistFreq;
    private String [] stepRateEntry= {"High", "Medium", "Low", "Custom"};
    private JComboBox cbStepQuality;   
    private JLabel    l_Freq, l_Amp;

    // mainframe and correction factor for small screensizes
    private XProjectFrame  mainframe;   // bloedsinn, gleich machen
    private int     correct;

    // to prevent double eventhandling
    private boolean itemselected = false;

    // calculation
    private int          interval; 
    private FourierTask  task;

    // fourier settings (initial values)
    private String title;
    public double from=0, to=50, step=0;
    public int    stepquality=0, weight=1;
    public int    datamode=0, compactmode=Period04.fourierGetCompact();
    public boolean isAutoFourierDisplay = false;
    public boolean isCheckFreqCompos = false;

    // pop-up menu
    private JMenuItem iPop_Rename, iPop_Delete, iPop_DisplayGraph;
    private JMenuItem iPop_DisplayData, iPop_Export, iPop_Properties;

    private XDialogFourierNoise noiseDialog = null;


    public XTabFourier(XProjectFrame m_frame) {
	mainframe = m_frame;
	correct   = mainframe.getSizeDiff();

	// initialize
	s_CalcName = Period04.fourierGetTitle();
	s_From     = Period04.fourierGetFrom();
	if (Period04.projectGetNyquist()<Double.valueOf(Period04.fourierGetTo())) {
	    s_To   = Period04.projectGetNyquistString();
	} else {
	    s_To   = Period04.fourierGetTo();
	}

	// create main panel
	mainPanel = new JPanel();
	mainPanel.setLayout(null);
	createMainPanel();

	// create a pop-up menu for fourierList
	createPopupMenu();
    }

    protected void createMainPanel() {

	// create opaque panel with border line
	JPanel settingsPanel = new JPanel();
	settingsPanel.setLayout(null);
	settingsPanel.setOpaque(false);
	settingsPanel.setBorder(BorderFactory.createCompoundBorder(
		BorderFactory.createTitledBorder(
		    BorderFactory.createEtchedBorder(EtchedBorder.RAISED),
		    " Fourier Calculation Settings ",
		    TitledBorder.LEADING, TitledBorder.TOP, 
		    new Font("Dialog", Font.BOLD, 10), 
		    new Color(20,30,150) ),
		BorderFactory.createEmptyBorder(5,5,5,5)));

	JLabel l_Title = createLabel("Title:   ");
	t_CalcName = createTextField(s_CalcName);
	JLabel l_From = createLabel("From: ");
	t_From = createTextField(s_From);

	JLabel l_StepRate = createLabel("Step rate: ");
	cbStepQuality = new JComboBox(stepRateEntry);
	cbStepQuality.setFont(new Font("Dialog", Font.PLAIN, 10));
	cbStepQuality.addItemListener(this);
	cbStepQuality.setSelectedIndex(0); // StepRate=HIGH
	t_StepRate = createTextField("0");
	t_StepRate.setEditable(false);

	JLabel l_To = createLabel("To: ");
	t_To        = createTextField(s_To);
	JLabel l_Nyquist = createLabel("Nyquist: ");
	l_NyquistFreq    = createLabel("0");

	JLabel l_UseWeights = createLabel("Use Weights:");
	t_Weights = createTextField(Period04.projectGetWeightString());
	t_Weights.setEditable(false);
	t_Weights.setToolTipText(
	    "<html>Weight string:<br>"+
	    "If no weights are used \"none\" will be displayed."+
	    "<table><tr><td width=\"50\" align=\"center\">1</td><td>"+
	    "use the weights assigned to attribute 1</td></tr>"+
	    "<tr><td align=\"center\">2</td><td>use the weights assigned to "+
	    "attribute 2</td></tr><tr><td align=\"center\">3</td><td>use the "+
	    "weights assigned to attribute 3</td></tr><tr>"+
	    "<td align=\"center\">4</td><td>use the weights assigned to "+
	    "attribute 4</td></tr><tr><td align=\"center\">p</td><td>use "+
	    "point weight</td></tr><tr><td align=\"center\">d</td><td>use "+
	    "deviation weight</td></tr></table></html>");
	t_Weights.setHorizontalAlignment(JTextField.CENTER);
	b_Weights = createButton("Edit weight settings");
	
	JLabel l_CalcMode = createLabel("Calculations based on:");
	cbDataMode[0] = createRadioButton("Original data");
	cbDataMode[4] = createRadioButton("Residuals at original");
	cbDataMode[2] = createRadioButton("Spectral window");
	cbDataMode[1] = createRadioButton("Adjusted data");
	cbDataMode[5] = createRadioButton("Residuals at adjusted");
	cbDataMode[3] = createRadioButton("Freq folded w SW");
	cbDataMode[0].setSelected(true);
	ButtonGroup bg1 = new ButtonGroup();
	for (int i=0; i<6; i++) {
	    bg1.add(cbDataMode[i]);
	}

	JLabel l_CompactMode = createLabel("Compact mode:");
	cbCompactMode[1] = createRadioButton("Peaks only");
	cbCompactMode[0] = createRadioButton("All");
	cbCompactMode[compactmode].setSelected(true);
	ButtonGroup bg2 = new ButtonGroup();
	bg2.add(cbCompactMode[1]);
	bg2.add(cbCompactMode[0]);

	JLabel l_Highest1 = createLabel("Highest Peak at:");
	l_Highest1.setFont(new Font("Dialog", Font.BOLD, 10));
	JLabel l_Highest1a = createLabel("Frequency =");
	l_Freq = createLabel("0");
	JLabel l_Highest2 = createLabel("Amplitude =");
	l_Amp = createLabel("0");


	b_Calculate = createCalcButton("Calculate");
	b_Calculate.setToolTipText(
	    "<html>Calculate the Fourier spectrum</html>");
	b_Calculate.setMnemonic(KeyEvent.VK_C);             //<--- shortcut
	b_Calculate.setActionCommand("calculate");

	// fourier calculation list
	fourierListModel = new DefaultListModel();
	fourierList = new JList(fourierListModel);
	fourierList.setToolTipText("<html>Double click to plot Fourier spectrum,"+
							   "<br>or right click to open the popup menu.</html>");
	fourierList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	fourierList.setFont(new Font("Dialog", Font.PLAIN, 11));
        fourierList.addListSelectionListener(this);
	fourierListPane = new JScrollPane(fourierList,
	    JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
	    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	fourierListPane.setBorder(BorderFactory.createEtchedBorder(
				  EtchedBorder.RAISED));

	b_Rename        = createButton("Rename spectrum");
	b_ExportFourier = createButton("Export spectrum");
	b_Delete        = createButton("Delete spectrum");
	b_DispData      = createButton("Display table");
	b_DispGraph     = createButton("Display graph");
	b_DispGraph.setFont(new Font("Dialog", Font.BOLD, 11));
	b_DispGraph.setMnemonic(KeyEvent.VK_D); // set a shortcut
	b_DispGraph.setActionCommand("display");

	//---
	//--- layout the components
	//--- 
	l_Title         .setBounds( 10, 20, 60, 20);
	t_CalcName      .setBounds( 60, 20,400, 20);
	l_From          .setBounds( 10, 45, 60, 20);
	t_From          .setBounds( 60, 45, 60, 20);
	l_StepRate      .setBounds(150, 45, 60, 20);
	cbStepQuality   .setBounds(210, 45,100, 20);
	t_StepRate      .setBounds(310, 45,150, 20);
	l_To            .setBounds( 10, 65, 60, 20);
	t_To            .setBounds( 60, 65, 60, 20);
	l_Nyquist       .setBounds(150, 65, 60, 20);
	l_NyquistFreq   .setBounds(210, 65, 60, 20);
	l_UseWeights    .setBounds( 10, 90, 80, 20);
	t_Weights       .setBounds(100, 90, 70, 20);
	b_Weights       .setBounds(170, 90,140, 20);
	l_CalcMode      .setBounds( 10,115,150, 15);
	cbDataMode[0]   .setBounds( 60,130,120, 15);
	cbDataMode[4]   .setBounds(180,130,150, 15);
	cbDataMode[2]   .setBounds(330,130,140, 15);
	cbDataMode[1]   .setBounds( 60,150,120, 15);
	cbDataMode[5]   .setBounds(180,150,150, 15);
	cbDataMode[3]   .setBounds(330,150,140, 15);
	l_CompactMode   .setBounds( 10,175,120, 15);
	cbCompactMode[1].setBounds(130,175,100, 15);
	cbCompactMode[0].setBounds(230,175, 60, 15);
	l_Highest1      .setBounds( 10,210,100, 15);
	l_Highest1a     .setBounds(120,210,100, 15);
	l_Freq          .setBounds(220,210, 80, 15);
	l_Highest2      .setBounds(300,210,100, 15);
	l_Amp           .setBounds(400,210, 80, 15);
	if (Period04.isMacOS) { // positions for mac os x layout
		//correct -= 20;
		settingsPanel  .setBounds( 0, 0, 509, 205 );
	    b_Calculate    .setBounds( 10, 230, 490, 25);
	    fourierListPane.setBounds( 10, 260, 490, 285-correct);
	    b_Rename       .setBounds( 10, 545-correct, 150, 20);
	    b_ExportFourier.setBounds(180, 545-correct, 150, 20);
	    b_Delete       .setBounds(350, 545-correct, 150, 20);
	    b_DispData     .setBounds( 10, 570-correct, 240, 20);
	    b_DispGraph    .setBounds(260, 570-correct, 240, 20);
	} else { // layout for linux/windows os
	    settingsPanel  .setBounds( 0, 0, 489, 205 );
	    b_Calculate    .setBounds( 10, 230, 470, 25);
	    fourierListPane.setBounds( 10, 260, 470, 285-correct);
	    b_Rename       .setBounds( 10, 545-correct, 150, 20);
	    b_ExportFourier.setBounds(170, 545-correct, 150, 20);
	    b_Delete       .setBounds(330, 545-correct, 150, 20);
	    b_DispData     .setBounds( 10, 570-correct, 230, 20);
	    b_DispGraph    .setBounds(250, 570-correct, 230, 20);
	}
	//---
	//--- finally add everything to main panel
	//---
	mainPanel.add(settingsPanel, null);   
	mainPanel.add(l_Title, null);
	mainPanel.add(t_CalcName, null);
	mainPanel.add(l_From, null);
	mainPanel.add(t_From, null);
	mainPanel.add(l_StepRate, null);
	mainPanel.add(cbStepQuality, null);
	mainPanel.add(t_StepRate, null);
	mainPanel.add(l_To, null);
	mainPanel.add(t_To, null);
	mainPanel.add(l_Nyquist, null);
	mainPanel.add(l_NyquistFreq, null);
	mainPanel.add(l_UseWeights, null);
	mainPanel.add(t_Weights, null);
	mainPanel.add(b_Weights, null);
	mainPanel.add(l_CalcMode, null);
	mainPanel.add(cbDataMode[0], null);
	mainPanel.add(cbDataMode[3], null);
	mainPanel.add(cbDataMode[1], null);
	mainPanel.add(cbDataMode[4], null);
	mainPanel.add(cbDataMode[2], null);
	mainPanel.add(cbDataMode[5], null);
	mainPanel.add(l_CompactMode, null);
	mainPanel.add(cbCompactMode[0], null);
	mainPanel.add(cbCompactMode[1], null);
	mainPanel.add(l_Highest1, null);
	mainPanel.add(l_Highest1a, null);
	mainPanel.add(l_Freq, null);
	mainPanel.add(l_Highest2, null);
	mainPanel.add(l_Amp, null);
	mainPanel.add(b_Calculate, null);
	mainPanel.add(fourierListPane, null);
	mainPanel.add(b_Rename, null);
	mainPanel.add(b_Delete, null);
	mainPanel.add(b_DispData, null);
	mainPanel.add(b_ExportFourier, null);
	mainPanel.add(b_DispGraph, null);
    }

  public void initializeFileChooser ( )
  {
    // create FileChooser
    fc = new JFileChooser( Period04.getCurrentDirectory( ));
    fc.setFileFilter( new FourierFileFilter( ));
//    fc.addChoosableFileFilter( new FourierFileFilter( ));
  }

    public void actionPerformed(ActionEvent evt) { 
	if (mainframe.isWorking()) { return; }
	Object s=evt.getSource();
	// weights
	if (s==b_Weights) { 
	    new XDialogWeight(mainframe);
	    updateWeights();
	}
	// calculation mode
	for (int i=0; i<6; i++) {
	    if (s==cbDataMode[i]) { Period04.fourierSetMode(i); }
	}
	// compact mode
	for (int i=0; i<2; i++) {
	    if (s==cbCompactMode[i]) { Period04.fourierSetCompact(i); }
	}
	// calculate button
	if (s==b_Calculate)           { calculateFourier(); }
	// fourier management
	else if (s==b_Delete)         { deleteFourier();    }
	else if (s==b_Rename)         { renameFourier();    }
	// display buttons
	else if (s==b_DispData)       { displayFourierData();  }
	else if (s==b_DispGraph)      { displayFourierGraph(); }
	else if (s==b_ExportFourier)  { exportActiveFourier(); }
	// shortcut for calculate
	else if ("calculate".equals(evt.getActionCommand())) {
	    calculateFourier();
	}
	else if ("display".equals(evt.getActionCommand())) {
	    displayFourierGraph();
	}
	// pop-up menu
	else if (s==iPop_Rename)       { renameFourier(); }
	else if (s==iPop_Delete)       { deleteFourier(); }
	else if (s==iPop_Export)       { exportActiveFourier(); }
	else if (s==iPop_DisplayGraph) { displayFourierGraph(); }
	else if (s==iPop_DisplayData)  { displayFourierData(); }
	else if (s==iPop_Properties)   { displayFourierProperties(); }
    }

    public void createPopupMenu() {
	JPopupMenu popup = new JPopupMenu();
        iPop_Rename        = new JMenuItem("Rename");
        iPop_Delete        = new JMenuItem("Delete");
        iPop_Export        = new JMenuItem("Export");
        iPop_DisplayGraph  = new JMenuItem("Display graph");
        iPop_DisplayData   = new JMenuItem("Display table");
        iPop_Properties    = new JMenuItem("Properties");
		iPop_Rename      .addActionListener(this);
        iPop_Delete      .addActionListener(this);
        iPop_Export      .addActionListener(this);
        iPop_DisplayGraph.addActionListener(this);
        iPop_DisplayData .addActionListener(this);
		iPop_Properties  .addActionListener(this);
        popup.add(iPop_Rename);
        popup.add(iPop_Delete);
        popup.add(iPop_Export);
        popup.add(iPop_DisplayGraph);
        popup.add(iPop_DisplayData);
        popup.add(iPop_Properties);
        MouseListener popupListener = new PopupListener(popup);
        fourierList.addMouseListener(popupListener);
    }

    public void itemStateChanged(ItemEvent evt) {
	if (itemselected==true) { // a real event
	    Object s=evt.getSource();
	    if (s==cbStepQuality) {
			String item = (String)cbStepQuality.getSelectedItem();
			double step = 0.0;
			String stepquality = item;
	        updateStepping(stepquality, step);
	    }
	    itemselected = false;
 	} else { // just a secondary event - ignore
	    itemselected = true;
	}
    }

    public void valueChanged(ListSelectionEvent evt) {
        if (evt.getValueIsAdjusting()==true) {
	    if (fourierListModel.getSize()>1) { 
		int selectedID = fourierList.getSelectedIndex(); 
		Period04.projectActivateFourier(selectedID);
		updateDisplay(1);
	    }
	}
    }

    public void getCalculationSettings() {
	Double from = new Double(0.0);
	Double to = new Double(50.0);
	String s_from = t_From.getText();  // read values from textbox
	String s_to   = t_To.getText();
	try {                              // parse values to String
	    from = Double.valueOf(s_from);
	} catch (NumberFormatException e) {
	    JOptionPane.showMessageDialog(
		mainframe.getFrame(), "\""+s_from+"\" is not a valid frequency !!\n"+
		"Please choose an other start value \n"+
		"for the calculation range!",
		"Error", JOptionPane.ERROR_MESSAGE);
	    return;
	}
	try {
	    to = Double.valueOf(s_to);
	} catch (NumberFormatException e) {
	    JOptionPane.showMessageDialog(
		mainframe.getFrame(), "\""+s_to+"\" is not a valid frequency !!\n"+
		"Please choose an other end value \n"+
		"for the calculation range!",
		"Error", JOptionPane.ERROR_MESSAGE);
	    return;
	}
	Double step = new Double(0);
	int stepquality = cbStepQuality.getSelectedIndex();
	if (stepquality==3) {
	    String s_step = t_StepRate.getText();
	    try {
		step = Double.valueOf(s_step);
	    } catch (NumberFormatException e) {
		JOptionPane.showMessageDialog(
		    mainframe.getFrame(), "\""+s_step+
		    "\" is not a valid step rate !!\n"+
		    "Please choose an other value!",		
		    "Error", JOptionPane.ERROR_MESSAGE);
		return;
	    }
	}
	// assign values
	this.from = from.doubleValue();
	this.to   = to.doubleValue();
	this.step = step.doubleValue();
	this.stepquality = stepquality;
	this.weight = Period04.projectGetUseFourierWeight(); 

	for (int i=0; i<6; i++) {
	    if (cbDataMode[i].isSelected()) { datamode=i; } 
	}
	for (int i=0; i<2; i++) {
	    if (cbCompactMode[i].isSelected()) { compactmode=i; } 
	}
    }

    private int batchFourierCounter=1;
    public void calculateFourierBatch(String sfrom, String sto, String swhat, String scompact) {
	String title = "f"+ batchFourierCounter;
	// parse values to String
	try { 
	    this.to = Double.valueOf(sto);
	} catch (NumberFormatException e) {
	    this.to = Period04.projectGetNyquist();
	}
	try { 
	    this.from = Double.valueOf(sfrom);
	} catch (NumberFormatException e) {
	    this.from = 0.0;
	} 

	switch(swhat.charAt(0)) {
	case 'o': this.datamode=0; break;
	case 'a': this.datamode=1; break;
	case 'r': this.datamode=4; break;
	case 'R': this.datamode=5; break;
	case '1': this.datamode=2; break;
	case '2': this.datamode=3; break;
	}   
	switch(scompact.charAt(0)) {
	case 'y': this.compactmode=0; break;
	case 'n': this.compactmode=1; break;
	}   

	double zero=0.0;
	if (datamode==0 || datamode==1) {
	    Period04.projectSetDataMode(datamode);           // set new mode
	    zero = Period04.timestringAverage(this.weight);  // get zeropoint
	}
	batchFourierCounter++;
	mainframe.setCalculating(
				 true, getCalcSettingsPanel(),
				 "Calculating Fourier Spectrum ...", interval, true);

	//swingworker
	task = new FourierTask(
			       mainframe, this, title, this.from, this.to,
			       stepquality, this.step, 
			       datamode, compactmode, this.weight, zero, true);
	task.go();                  // start calculation
	mainframe.startTimer(task); // start the timer for the progressbar
	task.get();   
    }

    public void calculateFourier() {
	if (task!=null) { return; }   // last task is not yet finished, return
	// get and check settings
	title =	Period04.projectCheckFourierTitle(t_CalcName.getText());
	getCalculationSettings();
	if (!checkFourierSettings()) { return; }
	// have there been relevant changes in the timestring list?
	mainframe.getTimeStringTab().writeSelectionChanges();

	if (datamode==3) {
	    new XDialogSpectralWindow(mainframe, this);
	}

	// is it a long calculation?
	Double longcalc = new Double(
	    Period04.projectGetSelectedPoints()*
	    (this.to-this.from)/
	    Period04.projectGetStepRate(
		(String)cbStepQuality.getSelectedItem(), this.step));
	boolean isLongCalc;
	if (longcalc.isNaN() || longcalc.isInfinite()) { 
	    isLongCalc = true; 
	} else {
	    isLongCalc = (longcalc.doubleValue()>2.5E7);
	}
	// show the calculation panel for long calculations only
	// and set an appropriate time interval to update the progressbar
	if (isLongCalc) {          
	    interval = (int)(longcalc.doubleValue()/1.0E7);
	    mainframe.setCalculating(
		true, getCalcSettingsPanel(),
		"Calculating Fourier Spectrum ...", interval, true);
	} else {
		Period04.cancel=0;
	    Period04.completed = 0;
	}

	// write to protocol
	StringBuffer text = new StringBuffer();
	text.append("Fourier Calculation:\n"+
		    "Title:                    "+title+"\n"+
		    "From frequency:           "+this.from+"\n"+
		    "To frequency:             "+this.to+"\n");
	if (stepquality==3 /*"Custom"*/) {
	    text.append("Step quality:             Custom\n"+
			"Step rate:                "+this.step+"\n");
	} else {
	    String s_qual = (String)cbStepQuality.getSelectedItem();
	    text.append("Step quality:             "+s_qual+"\n"+
			"Step rate:                "+
			Period04.projectGetStepRateString(s_qual, this.step)+"\n");
	}
	String s_weight = "";
	if (this.weight!=0) { s_weight=" weighted"; }
	text.append("Calculation based on:     ");
	switch (datamode) {
	    case 0: // original
		text.append("the "+Period04.projectGetSelectedPoints()+
			    s_weight+" original measurements\n");
		break;
	    case 1: // adjusted
		text.append("the "+Period04.projectGetSelectedPoints()+
			    s_weight+" adjusted measurements\n");
		break;
	    case 2: // spectral window 1
		text.append("the "+Period04.projectGetSelectedPoints()+
			    s_weight+" points for the spectral window\n");
		break;
	    case 3: // spectral window 2
		text.append("the "+Period04.projectGetSelectedPoints()+
			    s_weight+" points for the spectral window folded with frequency\n");
		break;
	    case 4: // residuals at original
		text.append("the "+Period04.projectGetSelectedPoints()+s_weight+
			    " residuals at the original measurements\n\t\t  "+
			    "with "+Period04.projectGetActiveFrequenciesString()+
			    " frequencies prewhitened\n");
		break;
	    case 5: // residuals at adjusted
		text.append("the "+Period04.projectGetSelectedPoints()+s_weight+
			    " residuals at the adjusted measurements\n\t\t  "+
			    "with "+Period04.projectGetActiveFrequenciesString()+
			    " frequencies prewhitened\n");
		break;
	} 

	// find out if zeropoint should be subtracted
	double zero=0;
	if ((datamode==0)||(datamode==1)) {
	    int oldDataMode = Period04.projectGetDataMode(); // save old mode
	    Period04.projectSetDataMode(datamode);           // set new mode
	    zero = Period04.timestringAverage(this.weight);  // get zeropoint
	    int value = JOptionPane.showConfirmDialog(mainframe.getFrame(),
		"Fourier analysis requires that the data\n"+
		"does not contain a zero point shift.\n"+
		"Otherwise additional features centered\n"+
		"at frequency 0.0 will appear.\n"+
		"These features may even dominate\n"+
		"the whole frequency spectrum.\n\n"+
		"Do you want to subtract the\n"+
		"average zero point of "+format(zero)+" ?",
		"Subtract zero point?", JOptionPane.YES_NO_OPTION);
	    if (value == JOptionPane.YES_OPTION) {
		Period04.projectSetDataMode(datamode);
		text.append("Zero point:               subtracted a zeropoint of "+zero+"\n");
	    } else {
		zero = 0;
		Period04.projectSetDataMode(oldDataMode); 
	    }
	}
	text.append("Calculation started on:   "+XTabLog.getDate());
	mainframe.getLog().writeProtocol(text.toString(), true);

	if (isLongCalc) { // create the calculation task for long calculations
	    task = new FourierTask(
							   mainframe, this, title, this.from, this.to,
							   stepquality, this.step, 
							   datamode, compactmode, this.weight, zero);
	    task.go();                  // start calculation
	    mainframe.startTimer(task); // start the timer for the progressbar
	} else { // it's only a short calculation
	    Period04.projectCalculateFourier(
										 this.title, this.from, this.to, stepquality, this.step, 
										 datamode, compactmode, this.weight, zero);
	    updateDisplay();
	    writeCalculationResultsToProtocol(false);
	}
    }

    public void calculateNoiseSpectrum() {
	new XDialogFourierNoise(mainframe, true);
    }

    public void calculateNoiseAtFrequency() {
	noiseDialog = new XDialogFourierNoise(mainframe, false);
    }

	public XDialogFourierNoise getNoiseDialog() {
		return noiseDialog;
	}

    public void calcSpecialNyquist() {
	if (Period04.projectGetSelectedPoints()<5) {
	    JOptionPane.showMessageDialog(mainframe.getFrame(),
		"Not enough points!", "Error",
		JOptionPane.ERROR_MESSAGE);
	    return;
	}
	new XDialogNyquist(mainframe);
    }

    public boolean checkFourierSettings() {
	// check for ranges
	if (Period04.projectGetSelectedPoints()<5) {
	    JOptionPane.showMessageDialog(mainframe.getFrame(),
		"Not enough points for fourier-calculation!", "Error",
		JOptionPane.ERROR_MESSAGE);
	    return false;
	}
	// check if weights should be used
	if (Period04.projectGetPeriodUseWeight()==0) {
	    if (Period04.projectGetSelectedPoints()!=
		Period04.timestringGetWeightSum()) {
		int value = JOptionPane.showConfirmDialog(mainframe.getFrame(),
		    "Should I use the given weights for calculation?",
		    "Confirm", JOptionPane.YES_NO_OPTION);
		if (value == JOptionPane.YES_OPTION) {
		    weight=1; 
		}
	    }
	}
	// check if to is higher than nyquist
	double nyquist = Period04.projectGetNyquist();
	if (to > nyquist) {
	    int value = JOptionPane.showConfirmDialog(mainframe.getFrame(),
		"The final frequency to calculate ("+to+")\n"+
		"is higher than the calculated\nNyquist frequency ("+
		format(nyquist)+")\n"+"Do you want to continue?",
		"Confirm" ,JOptionPane.YES_NO_OPTION);
	    if (value==JOptionPane.NO_OPTION) {
		return false;
	    }
	}
	String s_step = t_StepRate.getText();
	try {
	    double stp = Double.parseDouble(s_step);
	    //--- is step rate equal to zero?
	    if (stp==0.0) {
		JOptionPane.showMessageDialog(mainframe.getFrame(),
		    "Step rate is 0.0!", "Error",
		    JOptionPane.ERROR_MESSAGE);
		return false;
	    }	    
	    //--- number of resulting data points is very high, show warning
	    long pnts = (long)((to-from)/stp);
	    if (pnts>1.0e+06 && compactmode==0) {
		int value = JOptionPane.showConfirmDialog(mainframe.getFrame(),
		    "Using the chosen settings "+pnts+" data points"+
		    "will be created.\nIt's recommended to either ...\n"+
		    "- use the compact option \"Peaks only\",\n"+
		    "- to limit the frequency range or\n"+
		    "- to choose a larger step rate.\n"+
		    "Do you want to change your calculation settings?",
		    "Confirm" ,JOptionPane.YES_NO_OPTION);
		if (value==JOptionPane.YES_OPTION) {
		    return false;
		}
	    }
	} catch (NumberFormatException e) { /*do nothing*/ }

	return true;
    }

    //--- 
    //--- creates a calculation panel with a progress bar for long calculations
    //--- 
    public JPanel getCalcSettingsPanel() {
	JPanel panel = new JPanel();
	panel.setLayout(null);
	panel.setOpaque(false);
	panel.setBorder(BorderFactory.createCompoundBorder(
		BorderFactory.createTitledBorder(
		    BorderFactory.createEtchedBorder(),
		    " Calculation settings: ",
		    TitledBorder.LEADING, TitledBorder.TOP,
		    new Font("Dialog", Font.BOLD, 12),
		    new Color(20,30,150) ),
		BorderFactory.createEmptyBorder(5,5,5,5))
	    );

	String s_dmode = "Original Data";
	for (int i=0; i<6; i++) {
	    if (cbDataMode[i].isSelected()) {
		s_dmode = cbDataMode[i].getText();
	    } 
	}
	String s_cmode = "no";
	if (cbCompactMode[1].isSelected()) { s_cmode = "yes"; } 

	JLabel l_range         = new JLabel("Frequency range: ");
	JLabel l_range_f       = new JLabel("[ "+from+" , "+to+" ]");
	JLabel l_stepquality   = new JLabel("Step quality: ");
	JLabel l_stepquality_f = new JLabel(
	    (String)cbStepQuality.getSelectedItem());
	JLabel l_step          = new JLabel("Step rate: ");
	JLabel l_step_f        = new JLabel(t_StepRate.getText());
	JLabel t_weights       = new JLabel("Use weights: ");
	JLabel l_weights_f     = new JLabel(Period04.projectGetWeightString());
	JLabel l_calcmode      = new JLabel("Calculation based on: ");
	JLabel l_calcmode_f    = new JLabel(s_dmode);
	JLabel l_compactmode   = new JLabel("Compacting: ");
	JLabel l_compactmode_f = new JLabel(s_cmode);

	int x0 = 15;
	int x1 = x0+140;
	l_range        .setBounds( x0   , 20, 150, 20);
	l_range_f      .setBounds( x1   , 20, 150, 20);
	l_stepquality  .setBounds( x0   , 40, 150, 20);
	l_stepquality_f.setBounds( x1   , 40, 150, 20);
	l_step         .setBounds( x0   , 60, 150, 20);
	l_step_f       .setBounds( x1   , 60, 150, 20);
	t_weights      .setBounds( x0   , 80, 150, 20);
	l_weights_f    .setBounds( x1   , 80, 150, 20);
	l_calcmode     .setBounds( x0   , 100, 150, 20);
	l_calcmode_f   .setBounds( x1   , 100, 150, 20);
	l_compactmode  .setBounds( x0   , 120, 150, 20);
	l_compactmode_f.setBounds( x1   , 120, 150, 20);

	panel.add(l_range);
	panel.add(l_range_f);
	panel.add(l_stepquality);
	panel.add(l_stepquality_f);
	panel.add(l_step);
	panel.add(l_step_f);
	panel.add(t_weights);
	panel.add(l_weights_f);
	panel.add(l_calcmode);
	panel.add(l_calcmode_f);
	panel.add(l_compactmode);
	panel.add(l_compactmode_f);

	return panel;
    }

    public void writeCalculationResultsToProtocol(boolean isAutoAcceptFreq) {
	// write results to protocol
	StringBuffer text = new StringBuffer();;
	String fin = " finished ";
	if (Period04.cancel!=0) { fin = " canceled "; }
	text.append("Calculation"+fin+"on:  "+XTabLog.getDate()+"\n");
	if (compactmode==1) {
	    text.append("The peak-only spectrum contains "+
			Period04.projectGetFourierActivePoints()+" points.\n");
	} else {
	    text.append("The spectrum contains "+
			Period04.projectGetFourierActivePoints()+" points.");
	}
	String peakstring = "The following amplitude is the highest:\n";
	String [] peak = Period04.fourierActiveGetPeak();
	peakstring+="Frequency:   "+peak[0]+"\n"+
	    "Amplitude:   "+peak[1];
	text.append(peakstring);
	mainframe.getLog().writeProtocol(text.toString(), false);
	    // ask whether to include this frequency in the frequency list
	if ((datamode!=2 && datamode!=3) /*spectral window*/ && Period04.isCalculationCanceled()==0 && !isAutoAcceptFreq) { 
	    int value = JOptionPane.showConfirmDialog(
							  mainframe.getFrame(), peakstring+
		"\nDo you want to include this frequency?",
		"Confirm", JOptionPane.YES_NO_OPTION);
	    if (value==JOptionPane.YES_OPTION) {
		int freqindex = Period04.projectAddFrequencyPeak();
		String scompo = Period04.projectIsFrequencyPeakComposition();
		if (scompo.length()>0 && isCheckFreqCompos 
		    && datamode!=2 && datamode!=3) {
		    
		    JOptionPane.showMessageDialog(
				      mainframe.getFrame(),
				      "Info: "+peak[0]+" ~ "+scompo.replace("1f","f")+"\nbut this could be an accidental agreement!",
		"Frequency combination", JOptionPane.INFORMATION_MESSAGE);
		}
		mainframe.updateFitDisplay();
		mainframe.getLog().writeProtocol(
		    "and has been included as frequency "+(freqindex+1)+"\n",
		    false);
	    }
	}
	if (isAutoAcceptFreq) {
	    int freqindex = Period04.projectAddFrequencyPeak();
	    mainframe.getLog().writeProtocol(
					     "and has been included as frequency "+(freqindex+1)+"\n",
					     false);
	    //--- select frequency in fit tab
	    Period04.projectSetActive(freqindex,true);
	    mainframe.updateFitDisplay();
	}
    }

    public void deleteFourier() {
	Period04.projectDeleteActiveFourier();
	updateDisplay(0);
    }

    public void renameFourier() {
	if (fourierListModel.getSize()>0) { 
	    int selectedID = fourierList.getSelectedIndex(); 
	    String fouriername = Period04.projectGetFourierTitle(selectedID);
	    XDialogInput dialog = new XDialogInput(
		mainframe.getFrame(), "Change name:",
		"New name for the currently selected fourier spectrum:",
		fouriername, 0 /*no numbercheck*/);
	    fouriername = dialog.getValidatedText();
	    Period04.projectRenameActiveFourier(fouriername);
	    updateDisplay(0);
	}
    }

    public void exportActiveFourierBatch(String file) {
	if (fourierListModel.getSize()==0) {
	    System.out.println("savefourier failed");
	}
	int selectedID = fourierList.getSelectedIndex();
	mainframe.setSaving(true); // create a progress bar
	// create a task for output
	FileTask task = new FileTask(
	     mainframe, FileTask.FOURIER_OUT, selectedID, file);
	task.go();
	task.get(); // wait until finished!
    }

    public void exportActiveFourier() {
	if (fourierListModel.getSize()==0) {
	    JOptionPane.showMessageDialog(
		mainframe.getFrame(), "There is no fourier-spectrum to be saved...",
		"Error", JOptionPane.ERROR_MESSAGE);
	    return;
	}
	initializeFileChooser();
	int selectedID = fourierList.getSelectedIndex();
	int returnVal = fc.showSaveDialog(mainPanel);
	if (returnVal == JFileChooser.APPROVE_OPTION) {
	    File file = fc.getSelectedFile();
	    // check if the extension is missing
	    String name = file.getName();
	    if (name.indexOf(".")==-1) { 
		file = new File(file.getPath()+".fou");
	    }
	    // check wether user wants to overwrite an existing file
	    if (file.exists()) {
		int v = JOptionPane.showConfirmDialog(
		    mainframe.getFrame(), "File does already exist!\n"+
		    "Do you really want to delete the old file?", 
		    "Warning", JOptionPane.YES_NO_OPTION);
		if (v == JOptionPane.NO_OPTION) {
		    return;
		}
	    }
	    Period04.setCurrentDirectory(file);
	    mainframe.setSaving(true); // create a progress bar
	    // create a task for output
	    FileTask task = new FileTask(
		mainframe, FileTask.FOURIER_OUT, selectedID, file.getPath());
	    task.go();
	}	
    }

    public boolean getCheckFreqCompositions() {
	return isCheckFreqCompos;
    }

    public void setCheckFreqCompositions(boolean val) {
	isCheckFreqCompos = val;
    }

    public boolean getAutoFourierDisplay() {
	return isAutoFourierDisplay;
    }

    public void setAutoFourierDisplay(boolean val) {
	isAutoFourierDisplay = val;
    }

    public void displayFourierData() {
	if (Period04.projectGetFourierEntries()==0) { return; }
	XFourierTable table = new XFourierTable(mainframe);
    }

    public void displayFourierProperties() {
		JTextArea prop = new JTextArea(Period04.fourierGetProperties());
		prop.setFont(new Font("Dialog",Font.BOLD,11));
		prop.setEditable(false);
		JScrollPane scrollprop = new JScrollPane(prop);
		JFrame fr = new JFrame("Properties of '"+
							   Period04.fourierGetTitle()+"'");
		fr.getContentPane().add(scrollprop);
		fr.setSize(450,300);
		fr.setLocationRelativeTo(mainframe.getFrame());
		fr.setVisible(true);
	}

    public void displayFourierGraph() {
	if (Period04.projectGetFourierEntries()==0) { return; }
	new XPlotFourier(mainframe);
    }

    public void updateDisplay() {
	updateDisplay(0);
    }

    public String updateFourierGetTo() {
	if ((Period04.projectGetNyquist()<Double.valueOf(Period04.fourierGetTo())
	     && (Period04.projectGetNyquist()>0.0))) {
	    return Period04.projectGetNyquistString();
	} else {
	    return Period04.fourierGetTo();
	}

    }

    public void updateDisplay(int i) {
	t_CalcName.setText(Period04.fourierGetTitle());	// update title
	t_From    .setText(Period04.fourierGetFrom());  // update freq-range
 	t_To      .setText(updateFourierGetTo());
	l_NyquistFreq.setText(Period04.projectGetNyquistString()); // nyquist
	updateStepping(/*Period04.fourierGetStepQuality()*/
	               (String)cbStepQuality.getItemAt(this.stepquality), // update stepping
		       Period04.fourierGetStepping());
	// set data mode
	int modeID = Period04.fourierGetMode();
	cbDataMode[modeID].setSelected(true); 
	// set compact mode
	int compactID = Period04.fourierGetCompact();
	cbCompactMode[compactID].setSelected(true);
	// update peak
	String [] peak = Period04.fourierActiveGetPeak(); 
	l_Freq.setText(peak[0]);
	l_Amp.setText(peak[1]);
	// update list
	if (i==0) { updateList(); }
	// update weight label
	t_Weights.setText(Period04.projectGetWeightString());
	// update the noise dialog (if it is active)
	updateNoiseDialogDisplay();
    }

	public void updateNoiseDialogDisplay() {
	// update the noise dialog (if it is active)
	if (noiseDialog!=null) { noiseDialog.updateFrequencies(); }
	}

    public void updateStepping(String stepquality, double step) {
	String steprate = Period04.projectGetStepRateString(stepquality, step);
	t_StepRate.setEditable(false); // set textbox not editable
	// select the right one
	if (stepquality=="Low") {
	    cbStepQuality.setSelectedIndex(2); 
	} else if (stepquality=="Medium") { 
	    cbStepQuality.setSelectedIndex(1);
	} else if (stepquality=="Custom") {
	    cbStepQuality.setSelectedIndex(3);
	    t_StepRate.setEditable(true); 
	} else { // stepquality=="High"
	    cbStepQuality.setSelectedIndex(0);
	}
	// fill in the value
	t_StepRate.setText(steprate);
    }

    public void updateWeights() {
	t_Weights.setText(Period04.projectGetWeightString());
    }

    public void updateNyquist() {
	String txt;
	if (Period04.projectGetNyquist()>0.0) { 
	    txt=Period04.projectGetNyquistString();
	} else {
	    txt="n/a";
	}
	l_NyquistFreq.setText(txt);
    }

    public void updateList() {
	fourierListModel.clear();
	int entries = Period04.projectGetFourierEntries();
	if (entries==0) {
	    return;
	}
	for (int i=0; i<entries; i++) {
	    String [] peak = Period04.fourierGetPeak(i);
	    fourierListModel.add(
		i, Period04.projectGetFourierTitle(i)+
		" ( F="+peak[0]+", A="+peak[1]+" )");
	}
	fourierList.setSelectedIndex(
	    Period04.projectGetFourierActiveIndex());
	fourierList.repaint(); // force a repaint
	updateScrollViewPort();
	fourierList.repaint(); // force a second repaint
    }
  
    private void updateScrollViewPort() {
	fourierListPane.getVerticalScrollBar().setValue(
	    fourierListPane.getVerticalScrollBar().getMaximum());
    }
  
    public void dereferenceTask() {
	task = null;
    }
   
    //---
    //--- resize the components to fit the new frame size
    //---
    public void fitSize() {
	correct = mainframe.getSizeDiff(); 
	if (Period04.isMacOS) {
		correct -= 20;
	    fourierListPane.setBounds( 10, 260, 490, 285-correct);
	    b_Rename       .setBounds( 10, 545-correct, 150, 20);
	    b_ExportFourier.setBounds(180, 545-correct, 150, 20);
	    b_Delete       .setBounds(350, 545-correct, 150, 20);
	    b_DispData     .setBounds( 10, 570-correct, 240, 20);
	    b_DispGraph    .setBounds(260, 570-correct, 240, 20);
	} else {
	    fourierListPane.setBounds( 10, 260, 470, 285-correct);
	    b_Rename       .setBounds( 10, 545-correct, 150, 20);
	    b_ExportFourier.setBounds(170, 545-correct, 150, 20);
	    b_Delete       .setBounds(330, 545-correct, 150, 20);
	    b_DispData     .setBounds( 10, 570-correct, 230, 20);
	    b_DispGraph    .setBounds(250, 570-correct, 230, 20);
	}
    }

    //---
    //--- A MouseListener for the Fourier list
    //--- (including popup menu and double click action)
    //---
    class PopupListener extends MouseAdapter {
        JPopupMenu popup;

        public PopupListener(JPopupMenu popupMenu) { popup = popupMenu; }
        public  void mousePressed(MouseEvent e)    { maybeShowPopup(e); }
        public  void mouseReleased(MouseEvent e)   { maybeShowPopup(e); }
        private void maybeShowPopup(MouseEvent e)  {
            if (e.isPopupTrigger()) {
                popup.show(e.getComponent(), e.getX(), e.getY());
		int index = fourierList.locationToIndex(e.getPoint());
		Period04.projectActivateFourier(index);
		fourierList.setSelectedIndex(index);
            }
        }
	public void mouseClicked(MouseEvent e) {
	    if(e.getClickCount() == 2) {
		// display the currently selected Fourier spectrum
		displayFourierGraph();
	    }
	}
    }
}


