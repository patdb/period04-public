/*----------------------------------------------------------------------*
 *  FourierNoiseTask
 *  starts a task for a long fourier calculation
 *----------------------------------------------------------------------*/

public class FourierNoiseTask extends Task {

    private double from, to, freq, spacing, boxsize, step, zeropoint;
    private int    stepquality, datamode, index;
    private XProjectFrame mainframe;
    private XDialogFourierNoise tab;
    private boolean spectrum;

    //---
    //--- noise-spectrum task constructor
    //---
    public FourierNoiseTask(XProjectFrame mainframe, XDialogFourierNoise tab,
			    double from, double to,
			    double spacing, double boxsize,
			    int stepquality, double step, int datamode,
			    double zeropoint) {
	this.mainframe   = mainframe;
	this.tab         = tab;
	this.from        = from;
	this.to          = to;
	this.spacing     = spacing;
	this.boxsize     = boxsize;
	this.stepquality = stepquality;
	this.step        = step;
	this.datamode    = datamode;
	this.zeropoint   = zeropoint;
	this.spectrum    = true;
    }

    //---
    //--- noise-at-frequency task constructor
    //---
    public FourierNoiseTask(XProjectFrame mainframe, XDialogFourierNoise tab,
			    double freq, double boxsize,
			    int stepquality, double step, int datamode,
			    double zeropoint) {
	this.mainframe   = mainframe;
	this.tab         = tab;
	this.freq        = freq;
	this.boxsize     = boxsize;
	this.stepquality = stepquality;
	this.step        = step;
	this.datamode    = datamode;
	this.zeropoint   = zeropoint;
	this.spectrum    = false;
    }

    //---
    //--- Called from XTabFourier to start the task.
    //---
    public void go() {
	worker = new SwingWorker() {
		public Object construct() {
		    done        = false;
		    canceled    = false;
		    return new ActualFourierNoiseTask();
		}
	    };
        worker.start();
    }

    //---
    //--- the actual long running task. this runs in a SwingWorker thread.
    //---
    class ActualFourierNoiseTask {
        ActualFourierNoiseTask() {
	    while (!isCanceled() && !isDone()) {
		int noise=0;
		// now go and calculate
		if (spectrum) {
		    Period04.projectCalculateNoiseSpectrum(
			from, to, spacing, boxsize, stepquality,
			step, datamode, zeropoint);
		} else {
		    Period04.projectCalculateNoiseAtFrequency(
			freq, boxsize, stepquality, step, datamode, zeropoint);
		}
		done = true;
		mainframe.setCalculating(false, null, "", 0, true);
		if (Period04.cancel==0) {
		    tab.writeResultsToProtocol();
		    tab.showResults();
		}
		tab.dereferenceTask();
	    }
	}
    }
}
