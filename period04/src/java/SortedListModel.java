
import javax.swing.*;
import java.util.*;

public class SortedListModel extends AbstractListModel
{
  private SortedSet sortedSet;

  public SortedListModel ( )
  {
    sortedSet = new TreeSet( );
  }

  public int size ( )
  {
    return sortedSet.size( );
  }

  public int getSize ( )
  {
    return sortedSet.size( );
  }

  public Object get ( int index)
  {
    return sortedSet.toArray( )[index];
  }

  public Object getElementAt ( int index)
  {
    return sortedSet.toArray( )[index];
  }

  public void add ( int index, Comparable element)
  {
    // ignore index.
    if (sortedSet.add( element))
    {
      fireContentsChanged( this, 0, getSize( ));
    }
  }

  public void addElement ( Comparable element)
  {
    if (sortedSet.add( element))
    {
      fireContentsChanged( this, 0, getSize( ));
    }
  }

  public void addAll ( Comparable elements [])
  {
    Collection c = Arrays.asList( elements);
    sortedSet.addAll( c);
    fireContentsChanged( this, 0, getSize( ));
  }

  public void clear ( )
  {
    sortedSet.clear( );
    fireContentsChanged( this, 0, getSize( ));
  }

  public boolean contains ( Object element)
  {
    return sortedSet.contains( element);
  }

  public Object firstElement ( )
  {
    return sortedSet.first( );
  }

  public Iterator iterator( )
  {
    return sortedSet.iterator( );
  }

  public Object lastElement( )
  {
    return sortedSet.last( );
  }

  public boolean removeElement ( Comparable element)
  {
    boolean removed = sortedSet.remove( element);

    if (removed)
    {
      fireContentsChanged( this, 0, getSize( ));
    }

    return removed;
  }
}
