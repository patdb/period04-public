/*----------------------------------------------------------------------*
 *  JPrintableTable
 *  defines a JTable with some printing function
 *----------------------------------------------------------------------*/

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.RepaintManager;


public class JPrintableTable extends JTable implements Printable 
{
    private JPrintableTable table;
    
    public JPrintableTable( AbstractTableModel model) {
	super(model);
	table=this;	
    }
		
    public int print( Graphics g,  PageFormat pageFormat,  int pageIndex) {
	// for faster printing turn double buffering off
	(RepaintManager.currentManager(this)).setDoubleBufferingEnabled(false);

	final Graphics2D g2=((Graphics2D) g);
	g2.setColor(Color.black);

	final int fontHeight=(g2.getFontMetrics()).getHeight();
	final int fontDesent=(g2.getFontMetrics()).getDescent();
	
	// leave room for page number
	final double pageHeight=pageFormat.getImageableHeight() - fontHeight;
	final double pageWidth=pageFormat.getImageableWidth();
	final double tableWidth=(table.getColumnModel()).getTotalColumnWidth();
	
	 double scale=1;
	if(tableWidth>=pageWidth) {
	    scale=pageWidth / tableWidth;
	}
	
	final double headerHeightOnPage=(table.getTableHeader()).getHeight() * scale;
	
	final double tableWidthOnPage=tableWidth * scale; 
	final double oneRowHeight=(table.getRowHeight() + table.getRowMargin()) * scale; 
	
	final int numRowsOnAPage=((int) ((pageHeight - headerHeightOnPage) / oneRowHeight)); 
	
	final double pageHeightForTable=oneRowHeight * numRowsOnAPage; 
	final int totalNumPages=((int) Math.ceil(table.getRowCount() / numRowsOnAPage));
	
	if(pageIndex>=totalNumPages) {
	    return NO_SUCH_PAGE;
	}

	g2.translate(pageFormat.getImageableX(),pageFormat.getImageableY());
	
	// put page no. at the bottom center
	g2.drawString("Page: " + (pageIndex + 1),(((int) pageWidth) / 2) - 35,((int) ((pageHeight + fontHeight) - fontDesent)));
	
	g2.translate(0f,headerHeightOnPage);
	g2.translate(0f,-pageIndex * pageHeightForTable);
	
	if(pageIndex + 1==totalNumPages) {
	    final int lastRowPrinted=numRowsOnAPage * pageIndex;
	    final int numRowsLeft=table.getRowCount() - lastRowPrinted;
	    g2.setClip(0,((int) (pageHeightForTable * pageIndex)),((int) Math.ceil(tableWidthOnPage)),((int) Math.ceil(oneRowHeight * numRowsLeft)));
	}
	// else clip to the entire area available.
	else {
	    g2.setClip(0,((int) (pageHeightForTable * pageIndex)),((int) Math.ceil(tableWidthOnPage)),((int) Math.ceil(pageHeightForTable)));
	}
	
	g2.scale(scale,scale);
	
	// paint the table
	table.paint(g2);
	
	g2.scale(1 / scale,1 / scale);
	g2.translate(0f,pageIndex * pageHeightForTable);
	g2.translate(0f,-headerHeightOnPage);
	g2.setClip(0,0,((int) Math.ceil(tableWidthOnPage)),((int) Math.ceil(headerHeightOnPage)));
	g2.scale(scale,scale);
	
	// paint header at top
	(table.getTableHeader()).paint(g2); 

	return Printable.PAGE_EXISTS;
    } 
 
}
