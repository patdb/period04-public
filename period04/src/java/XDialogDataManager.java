/*----------------------------------------------------------------------*
 *  XDialogDataManager
 *  a dialog to ...
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.border.EtchedBorder;

public class XDialogDataManager extends XDialog
    implements ActionListener
{
    private static final long serialVersionUID = 1739349333287001432L;
    
	private XProjectFrame mainframe;
	private JTabbedPane   tabpane;

	private JPanel tabTimeString, tabFourier, tabFit;
	private JPanel buttonpane, p_importexport;

	private JButton b_close, b_aliasGap, b_importExportTS, b_rearrange;
	private JButton b_setDeleted, b_importFreqs, b_exportFreqs, b_convertFreqs;
	private JButton b_exportFourier, b_cleanTS, b_cleanFourier, b_cleanFreqs;

	private JRadioButton r_importTS, r_exportTS, r_replaceTS, r_subtractZero;
    private JRadioButton r_oRes2Obs, r_aRes2Obs, r_calc2Obs, r_adj2Obs;
	private JRadioButton r_allSpectra, r_specificSpectra;
    private JRadioButton r_usePrefix, r_useTitles;
	
	private JComboBox cb_convertFromTo;

    private JLabel l_total, l_selected, l_active;
	private JLabel l_fourierInfo, l_fitInfo, l_TSInfo;

	private JTextField t_freqAdjust, t_prefix, t_deletedlabel, t_deletedcol;

	private String [] freqConvertOptions = {"from 'cycles/day' to 'microHz'",
											"from 'microHz' to 'cycles/day'"};
	

	//--- ---------------------------------------------------- ---//
	//---                      Methods                         ---//
	//--- ---------------------------------------------------- ---//

    public XDialogDataManager(final XProjectFrame mainframe) {
		super(mainframe.getFrame(), true);  // necessary to keep dialog in front
		this.mainframe = mainframe;

		//--- create a tabbed pane
		tabpane = new JTabbedPane();
		tabpane.setTabPlacement(JTabbedPane.TOP);
		tabpane.setFont(new java.awt.Font("Dialog", Font.BOLD, 11));
		createTabs();
		addTabs();

		//--- create southern button region
		buttonpane = new JPanel();
		b_close = createButton("Close");
		buttonpane.add(b_close);
		this.getRootPane().setDefaultButton(b_close);

		this.getContentPane().add(tabpane,    BorderLayout.CENTER);
		this.getContentPane().add(buttonpane, BorderLayout.SOUTH);
		this.setTitle("Data Manager");
		this.setSize(500,410);
		this.setResizable(false);
		this.setLocationRelativeTo(mainframe.getFrame());
    }

	private void createTabs() {
		createTimeStringTab();
		createFitTab();
		createFourierTab();
	}

	private void addTabs() {
		tabpane.add("  Time string  ", tabTimeString);
		tabpane.add("      Fit      ", tabFit);
		tabpane.add("    Fourier    ", tabFourier);
	}

	private void createTimeStringTab() {
		tabTimeString = new JPanel(null);

		// default delete label
		// specific nyquist frequency

		l_TSInfo   = createBoldLabel(
            "<html>Your current project contains "+
			new Integer(Period04.projectGetTotalPoints()).toString()+
			" points<br>of which "+
			new Integer(Period04.projectGetSelectedPoints()).toString()+
			" are selected.</html>");
		JLabel l_deletedlabel = createLabel("Default label: ");
		JLabel l_deletedcol = createLabel("Column:");
		JLabel l_replace  = createBoldLabel("<html>or replace<br>\"Observed\" and \"Adjusted\" ...</html>");
		JLabel l_note     = createBoldLabel("Please note:");
		JLabel l_replace2 = createLabel(
		    "<html>\"Calculated\" will be set 0.0</html>");

		t_deletedlabel = createTextField(
			Period04.projectGetDeletePointName());
		t_deletedcol   = createTextField(
			Period04.projectGetNameSet(Period04.projectGetDeletePointAttribute()));
		t_deletedlabel.setEditable(false);
		t_deletedcol  .setEditable(false);

		boolean showReplace = (Period04.projectGetTotalPoints()!=0);
		if (showReplace) {
			r_importTS  = createRadioButton("Append a time string");
		} else {
			r_importTS  = createRadioButton("Import a time string");
		}
		r_replaceTS     = createRadioButton("Replace old time string");
		r_exportTS      = createRadioButton("Export time string data");

		b_cleanTS        = createBoldButton("Delete time string");
		b_importExportTS = createBoldButton("Do it");
		b_setDeleted     = createBoldButton("Edit");
		b_rearrange      = createBoldButton("Do it");

		r_subtractZero = createRadioButton("Subtract fitted zero point");
		r_oRes2Obs     = createRadioButton("by Residuals(Obs.)");
		r_aRes2Obs     = createRadioButton("by Residuals(Adj.)");
		r_calc2Obs     = createRadioButton("by Calculated");
		r_adj2Obs      = createRadioButton("by Adjusted");

		ButtonGroup  bg1 = new ButtonGroup();
		bg1.add(r_importTS);
		bg1.add(r_replaceTS);
		bg1.add(r_exportTS);
		r_importTS.setSelected(true);

		ButtonGroup bg2 = new ButtonGroup();
		bg2.add(r_subtractZero);
		bg2.add(r_oRes2Obs);
		bg2.add(r_aRes2Obs);
		bg2.add(r_calc2Obs);
		bg2.add(r_adj2Obs);
		r_oRes2Obs.setSelected(true);

		JPanel p_options    = createTitledBorderPanel(" Options: ");
		p_importexport      = createTitledBorderPanel(" Import / export: ");
		JPanel p_deletedpts = createTitledBorderPanel(" Settings for deleted points ");
		JPanel p_rearrange  = createTitledBorderPanel(" Rearrange data: ");


		int offset = 0;
		if (showReplace)
			offset = 20;
		l_TSInfo        .setBounds( 20, 10, 300, 30);
		p_options       .setBounds( 10, 50, 470,265);
		p_importexport  .setBounds( 30, 65, 200,100+offset);
		r_importTS      .setBounds( 40, 90, 180, 20);
		if (showReplace) 
			r_replaceTS .setBounds( 40,110, 180, 20);
		r_exportTS      .setBounds( 40,110+offset, 180, 20);
		b_importExportTS.setBounds(140,135+offset,  80, 20);
		p_deletedpts    .setBounds( 30,210, 200, 95);
		l_deletedlabel  .setBounds( 40,230,  80, 20);
		t_deletedlabel .setBounds(120,230, 100, 20);
		l_deletedcol    .setBounds( 40,250,  80, 20);
		t_deletedcol   .setBounds(120,250, 100, 20);
		b_setDeleted    .setBounds(140,275,  80, 20);
		b_cleanTS       .setBounds(320, 15, 150, 20); 
		p_rearrange     .setBounds(260, 65, 200,240);
		r_subtractZero  .setBounds(270, 85, 180, 20);
		l_replace       .setBounds(270,105, 180, 40);
		r_oRes2Obs      .setBounds(270,145, 180, 20);
		r_aRes2Obs      .setBounds(270,165, 180, 20);
		r_calc2Obs      .setBounds(270,185, 180, 20);
		r_adj2Obs       .setBounds(270,205, 180, 20);
		l_note          .setBounds(270,230, 180, 15);
		l_replace2      .setBounds(270,245, 180, 15);
		b_rearrange     .setBounds(370,275,  80, 20);


		tabTimeString.add(l_TSInfo);
		tabTimeString.add(p_options);
		tabTimeString.add(p_importexport);
		tabTimeString.add(r_importTS);
		if (showReplace) 
			tabTimeString.add(r_replaceTS);
		tabTimeString.add(r_exportTS);
		tabTimeString.add(b_importExportTS);
		tabTimeString.add(p_deletedpts);
		tabTimeString.add(l_deletedlabel);
		tabTimeString.add(t_deletedlabel);
		tabTimeString.add(l_deletedcol);
		tabTimeString.add(t_deletedcol);
		tabTimeString.add(b_setDeleted);
		tabTimeString.add(b_cleanTS);
		tabTimeString.add(p_rearrange);
		tabTimeString.add(r_subtractZero);
		tabTimeString.add(l_replace);
		tabTimeString.add(r_oRes2Obs);
		tabTimeString.add(r_aRes2Obs);
		tabTimeString.add(r_calc2Obs);
		tabTimeString.add(r_adj2Obs);
		tabTimeString.add(l_note);
		tabTimeString.add(l_replace2);
		tabTimeString.add(b_rearrange);
	}

	private void createFitTab() {
		tabFit = new JPanel(null);

		l_fitInfo = createBoldLabel("Your current project contains "+
			(new Integer(Period04.projectGetActiveFrequencies())).toString()+
						  " frequencies.");
		b_importFreqs   = createBoldButton("Import frequencies");
		b_exportFreqs   = createBoldButton("Export frequencies");
		b_cleanFreqs    = createBoldButton("Delete frequencies");
		b_convertFreqs  = createBoldButton("Show results");
		cb_convertFromTo = createComboBox(freqConvertOptions);
		JLabel l_convert  = createLabel("Convert frequencies ...");
		JLabel l_aliasGap = createLabel(
			   "<html>Alias-gap for frequency adjustments:</html>");

		t_freqAdjust  = createTextField(Period04.projectGetFrequencyAdjustment());
		t_freqAdjust.setEditable(false);
		b_aliasGap    = createBoldButton("Edit");

		JPanel p_options      = createTitledBorderPanel(" Options: ");
		JPanel p_importexport = createTitledBorderPanel(" Import / export: ");
		JPanel p_conversion   = createTitledBorderPanel(" Unit Conversion: ");
		JPanel p_aliasGap     = createTitledBorderPanel(" Frequency adjustments: ");

		l_fitInfo       .setBounds( 20, 15, 300, 20);
		p_options       .setBounds( 10, 50, 470,265);
		// first column
		p_importexport  .setBounds( 30, 65, 200, 85);
		b_importFreqs   .setBounds( 40, 90, 180, 20); 
		b_exportFreqs   .setBounds( 40,115, 180, 20); 
		p_conversion    .setBounds( 30,200, 200,105);
		l_convert       .setBounds( 40,220, 180, 20);
		cb_convertFromTo.setBounds( 40,240, 180, 20);
		b_convertFreqs  .setBounds(100,270, 120, 20);
		// second column
		b_cleanFreqs    .setBounds(320, 15, 150, 20); 
		p_aliasGap      .setBounds(260, 65, 200, 90);
		l_aliasGap      .setBounds(270, 85, 180, 30);
		t_freqAdjust    .setBounds(270,120, 100, 20);
		b_aliasGap      .setBounds(380,120,  70, 20);
		
		tabFit.add(l_fitInfo);
		tabFit.add(p_options);
		tabFit.add(p_importexport);
		tabFit.add(b_importFreqs);
		tabFit.add(b_exportFreqs);
		tabFit.add(p_conversion);
		tabFit.add(l_convert);
		tabFit.add(b_convertFreqs);
		tabFit.add(cb_convertFromTo);
		tabFit.add(p_aliasGap);
		tabFit.add(l_aliasGap);
		tabFit.add(t_freqAdjust);
		tabFit.add(b_aliasGap);
		tabFit.add(b_cleanFreqs);
	}

	private void createFourierTab() {
		tabFourier = new JPanel(null);
		
		l_fourierInfo = createBoldLabel("Your current project contains "+
			(new Integer(Period04.projectGetFourierEntries())).toString()+
			" Fourier spectra.");
		b_cleanFourier  = createBoldButton("Delete all spectra");
		r_allSpectra     = createRadioButton("all Fourier spectra");
		r_specificSpectra= createRadioButton("specific Fourier spectra");
		JLabel l_filenames = createLabel("File names:");
		r_useTitles = createRadioButton("use the titles of the spectra");
		r_usePrefix = createRadioButton("use a prefix + counter");
		t_prefix    = createTextField("fourier");
		t_prefix.setEditable(false);
		b_exportFourier = createBoldButton("Start export");

		ButtonGroup bg1 = new ButtonGroup();
		bg1.add(r_allSpectra);
		bg1.add(r_specificSpectra);
		ButtonGroup bg2 = new ButtonGroup();
		bg2.add(r_useTitles);
		bg2.add(r_usePrefix);
		r_specificSpectra.setSelected(true);
		r_useTitles.setSelected(true);

		JPanel p_options = createTitledBorderPanel(" Options: ");
		JPanel p_export  = createTitledBorderPanel(" Export Fourier spectra: ");

		l_fourierInfo    .setBounds( 20, 15, 300, 20);
		p_options        .setBounds( 10, 50, 470,265);
		p_export         .setBounds( 30, 65, 200,205);
		r_specificSpectra.setBounds( 40, 90, 180, 20);
		r_allSpectra     .setBounds( 40,110, 180, 20);
		l_filenames      .setBounds( 40,140, 180, 20);
		r_useTitles      .setBounds( 40,160, 180, 20);
		r_usePrefix      .setBounds( 40,180, 180, 20);
		t_prefix         .setBounds( 70,200, 150, 20);
		b_exportFourier  .setBounds( 40,235, 180, 20);
		b_cleanFourier   .setBounds(320, 15, 150, 20); 
				
		tabFourier.add(p_options);
		tabFourier.add(p_export);
		tabFourier.add(l_fourierInfo);
		tabFourier.add(r_allSpectra);
		tabFourier.add(r_specificSpectra);
		tabFourier.add(l_filenames);
		tabFourier.add(r_useTitles);
		tabFourier.add(r_usePrefix);
		tabFourier.add(t_prefix);
		tabFourier.add(b_exportFourier);
		tabFourier.add(b_cleanFourier);
	}

	private JPanel createTitledBorderPanel(String title) {
		JPanel tmp = new JPanel(null);
		tmp.setOpaque(false);
		tmp.setBorder(
			BorderFactory.createTitledBorder( 
				 BorderFactory.createEtchedBorder(
					   EtchedBorder.RAISED),
					   title,
				       TitledBorder.CENTER, TitledBorder.TOP, 
					   new Font("Dialog", Font.BOLD, 10), 
					   new Color(20,30,150)) );
		return tmp;
	}

    public void resetDialog() {
		update();
    }

    public void update() {
		l_TSInfo.setText(
            "<html>Your current project contains "+
			new Integer(Period04.projectGetTotalPoints()).toString()+
			" points<br>of which "+
			new Integer(Period04.projectGetSelectedPoints()).toString()+
			" are selected.</html>");
		boolean showReplace = (Period04.projectGetTotalPoints()!=0);
		if (showReplace) {
			r_importTS.setText("Append a time string");
			p_importexport  .setBounds( 30, 65, 200,120);
			r_replaceTS     .setBounds( 40,110, 180, 20);
			r_exportTS      .setBounds( 40,130, 180, 20);
			b_importExportTS.setBounds(140,155,  80, 20);
			tabTimeString.add(r_replaceTS);
			this.repaint();
		} else {
			r_importTS.setText("Import a time string");
			tabTimeString.remove(r_replaceTS);
			p_importexport  .setBounds( 30, 65, 200,100);
			r_replaceTS     .setBounds( 40,110, 180, 20);
			r_exportTS      .setBounds( 40,110, 180, 20);
			b_importExportTS.setBounds(140,135,  80, 20);
			this.repaint();
		}
		t_deletedlabel.setText(Period04.projectGetDeletePointName());
		t_deletedcol.setText(Period04.projectGetNameSet(
								 Period04.projectGetDeletePointAttribute()));
		l_fourierInfo.setText("Your current project contains "+
			(new Integer(Period04.projectGetFourierEntries())).toString()+
						  " Fourier spectra.");
		l_fitInfo.setText("Your current project contains "+
			(new Integer(Period04.projectGetActiveFrequencies())).toString()+
						  " frequencies.");
		t_freqAdjust.setText(Period04.projectGetFrequencyAdjustment());
	}


	public void exportFourier() {
		if (Period04.projectGetFourierEntries()==0) {
			JOptionPane.showMessageDialog(
				XDialogDataManager.this, 
				"There are no Fourier spectra to export!",
				null, JOptionPane.ERROR_MESSAGE);
				return;
		}
		if (!(r_allSpectra.isSelected() ||
			  r_specificSpectra.isSelected())) {
			JOptionPane.showMessageDialog(
				XDialogDataManager.this, 
				"Please choose an export option!", null,
				JOptionPane.ERROR_MESSAGE);
			    return;
		}
		if (!(r_useTitles.isSelected() ||
			  r_usePrefix.isSelected())) {
			JOptionPane.showMessageDialog(
				XDialogDataManager.this, 
				"Please choose one of the file name options!",
				null, JOptionPane.ERROR_MESSAGE);
			    return;
		}
		//--- ask the user for a directory
		String directory;
		JFileChooser fc = new JFileChooser(Period04.getCurrentDirectory());
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int val = fc.showSaveDialog(mainframe.getFrame());
		if (val==JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			if (!file.canWrite()) {
				JOptionPane.showMessageDialog(
					XDialogDataManager.this, 
					"Cannot write the files!",
					null, JOptionPane.ERROR_MESSAGE);
				return;
			}
			directory = file.getPath();
			StringBuffer text = new StringBuffer();
			text.append(directory);
			text.append(System.getProperty("file.separator"));
			directory = text.toString();
			Period04.setCurrentDirectory(file);
		} else {
			return;
		}
		//--- export either all or a selection
		if (r_allSpectra.isSelected()) {
			//--- prevent from being null ...
			String prefix = t_prefix.getText();
			if (prefix==null) { prefix="-"; }
			//--- create a progress bar
			mainframe.setSaving(true);
			//--- create a task for output
			FileTask task = new FileTask(mainframe, FileTask.FOURIER_ALL_OUT, 
										 directory, r_usePrefix.isSelected(), 
										 prefix);
			task.go();
		} else {
			//--- make a selection
			XDialogSelectFourier dialog = new 
				XDialogSelectFourier(mainframe.getFrame());
			if (dialog.isCanceled()) {return;}
			//--- prevent from being null ...
			String prefix = t_prefix.getText();
			if (prefix==null) { prefix="-"; }
			//--- create a progress bar
			mainframe.setSaving(true);
			//--- create a task for output
			FileTask task = new FileTask(mainframe, 
										 FileTask.FOURIER_SELECTED_OUT, 
										 directory,dialog.getSelectedIndices(),
										 dialog.getSelectedIndices().length,
										 r_usePrefix.isSelected(), 
										 prefix);
			task.go();
		}

	}

    public void setWorking(boolean value) {
		b_aliasGap      .setEnabled(!value);
		b_importExportTS.setEnabled(!value);
		b_setDeleted    .setEnabled(!value);
		b_rearrange     .setEnabled(!value);
		b_importFreqs   .setEnabled(!value);
		b_exportFreqs   .setEnabled(!value);
		b_convertFreqs  .setEnabled(!value);
		b_exportFourier .setEnabled(!value);
		b_cleanTS       .setEnabled(!value);
		b_cleanFreqs    .setEnabled(!value);
		b_cleanFourier  .setEnabled(!value);
    }

    public void actionPerformed(ActionEvent evt) {
		if (mainframe.isWorking()) { return; }
		Object s = evt.getSource();
		//---
		//--- Time string
		//---
		if (s==b_cleanTS) {
			int val = JOptionPane.showConfirmDialog(
				XDialogDataManager.this, 
                "Do you really want to delete the time string?",
				null, JOptionPane.YES_NO_OPTION);
			if (val==JOptionPane.YES_OPTION) {
				mainframe.cleanProject(false, false, true, false); 
			}
			return;
		}
		if (s==b_importExportTS) {
			if (r_importTS.isSelected()) {
				mainframe.importTimeString(true); 
			} else if (r_replaceTS.isSelected()) {
				//--- let's tidy up a bit
				StringBuffer text = new StringBuffer();
				text.append("Erasing old timestring\n");
				/*				if (c_cleanFreqs.isSelected())
					text.append("Cleaning frequencies\n");
				if (c_cleanFou.isSelected())
					text.append("Cleaning Fourier spectra\n");
				mainframe.getLog().writeProtocol(text.toString(), true);
				mainframe.cleanProject(c_cleanFou.isSelected(),
									   c_cleanFreqs.isSelected(),
									   true, c_cleanLog.isSelected());*/
			
				mainframe.showImportTimeStringDialog();


			} else if (r_exportTS.isSelected()) {
				if (Period04.projectGetTotalPoints()==0) {
				    JOptionPane.showMessageDialog(
					XDialogDataManager.this, 
					"There are no points to export!",
					null, JOptionPane.ERROR_MESSAGE);
				    return;
				}
				mainframe.exportTimeString();
			}
			update();
			return;
		}
		if (s==b_setDeleted) {
			mainframe.deleteDefaultLabel(); 
			update(); return;
		}
		if (s==b_rearrange) {
			StringBuffer text = new StringBuffer();
			String message = "I'm confused now!";
			if (r_subtractZero.isSelected()) {
				text.append(Period04.timestringSubtractZeroPoint());
				message = "Successfully subtracted zero point\nfrom '" +
					(Period04.projectGetPeriodUseData()==1 ? 
					 "Adjusted" : "Observed")+ "'!";
			} else if (r_oRes2Obs.isSelected()) {
				text.append("Rearrange data:"+
							Period04.projectMakeDataResidualsObserved());
				message = "Successfully replaced \n'Observed' by 'Residuals(Observed)'!";
			} else if (r_aRes2Obs.isSelected()) {
				text.append("Rearrange data:"+
							Period04.projectMakeAdjResidualsObserved());
				message = "Successfully replaced \n'Observed' by 'Residuals(Adjusted)'!";
			} else if (r_calc2Obs.isSelected()) {
				text.append("Rearrange data:"+
							Period04.projectMakeCalculatedObserved());
				message = "Successfully replaced \n'Observed' by 'Calculated'!";
			} else if (r_adj2Obs.isSelected()) {
				text.append("Rearrange data:"+
							Period04.projectMakeAdjustedObserved());
				message = "Successfully replaced \n'Observed' by 'Adjusted'!";
			}
			mainframe.getLog().writeProtocol(text.toString(), true);
			update();
			JOptionPane.showMessageDialog(
				XDialogDataManager.this, message,
				null, JOptionPane.INFORMATION_MESSAGE);
			return;
		}
		//---
		//--- Fit
		//---
		if (s==b_cleanFreqs) {
			int val = JOptionPane.showConfirmDialog(
				XDialogDataManager.this, 
                "Do you really want to delete all frequencies?",
				null, JOptionPane.YES_NO_OPTION);
			if (val==JOptionPane.YES_OPTION) {
				mainframe.cleanProject(false, true, false, false); 
			}
			update();
			return;
		}
		if (s==b_importFreqs) {
			mainframe.importFrequencies(); 
			l_fitInfo.setText(
				 "Your current project contains "+
				 (new Integer(Period04.projectGetActiveFrequencies())).toString()+
				 " frequencies.");
			update();
			return;
		}
		if (s==b_exportFreqs) {
			mainframe.exportFrequencies(false); 
			return;
		}
		if (s==b_aliasGap) {
			XDialogInput dialog = new XDialogInput(
				this,"Change alias-gap","Frequency adjustment:", 
			    Period04.projectGetFrequencyAdjustment(), 
			    2 /*non-neg. number check*/);
			if (dialog.isCanceled()) { return; }
			double adj = dialog.getValidatedNonNegativeNumber();
			Period04.projectSetFrequencyAdjustment(adj);
			mainframe.getLog().writeProtocol(
			    "Changed frequency adjustment:\n"+
			    "New alias-gap:            "+adj, true);
			t_freqAdjust.setText(Period04.projectGetFrequencyAdjustment());
		}
		if (s==b_convertFreqs) {
			if (Period04.projectGetActiveFrequencies()<1) {
				JOptionPane.showMessageDialog(
					XDialogDataManager.this, 
					"There are no frequencies to convert!",
					null, JOptionPane.ERROR_MESSAGE);
				return;
			}
			XDialogFrequencyConversion conv = new XDialogFrequencyConversion(
				mainframe.getFrame(), cb_convertFromTo.getSelectedIndex());
			return;
		}
		//---
		//--- Fourier
		//---
		if (s==b_cleanFourier) {
			int val = JOptionPane.showConfirmDialog(
				XDialogDataManager.this, 
                "Do you really want to delete all Fourier spectra?",
				null, JOptionPane.YES_NO_OPTION);
			if (val==JOptionPane.YES_OPTION) {
				mainframe.cleanProject(true, false, false, false); 
			}
			update();
			return;
		}
		if (s==r_usePrefix) { 
			t_prefix.setEditable(r_usePrefix.isSelected()); return;
		} else if (s==r_useTitles) {
			t_prefix.setEditable(r_usePrefix.isSelected()); return;
		}
		if (s==b_exportFourier) {
			exportFourier(); return;
		}
		//---
		//--- general
		//---
		if (s==b_close) {
			setVisible(false);
			mainframe.updateDisplays();
		}
    }
}
