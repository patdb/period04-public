import java.awt.*;
import java.awt.font.*;
import java.awt.geom.*;
import java.awt.print.*;
import java.text.*;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.print.attribute.HashPrintRequestAttributeSet;


public class PrintText implements Printable {

	private AttributedString mStyledText;
	private int tabCount, retCount;
	private int numberOfPages;
	private float [] tabStops;
	private int [] tabLocations;
	private int [] retLocations;
	private int iteratorPositionForNextPage = 0;
	private int stopNow = 0;

	private void reset() {
		retCount=0; tabCount=0;
		numberOfPages=0;
		iteratorPositionForNextPage = 0;
		stopNow = 0;
		tabStops = null;
		tabLocations = null;
		retLocations = null;
	}

	public void print(String mText) {
		reset();
		scanText(mText);
		mStyledText = new AttributedString(mText);
		mStyledText.addAttribute(TextAttribute.FONT, 
								 new Font ("Monospaced", Font.PLAIN, 9));

		PrinterJob printerJob = PrinterJob.getPrinterJob();

		Book book = new Book();
		for (int i=0; i<numberOfPages; i++)
			book.append(this, new PageFormat());
		printerJob.setPageable(book);

		boolean doPrint = printerJob.printDialog();
	
		if (doPrint) {
			try {
				printerJob.print();
			} catch (PrinterException exception) {
				JOptionPane.showMessageDialog(
                    null, "Printing error: " + exception,
					"Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}


	private void scanText(String text) {
		// scan for tabs
		//
		tabCount =0;
		int index = 0;
		String tab = "\t";
		boolean stop = false;
		while (!stop)  {
			int newindex=text.indexOf(tab,index);
			if (newindex!=-1) {
				tabCount++;
				index=newindex+1;
			} else {
				stop=true;
			}
		}

		// scan for line breaks
		//
		retCount =0;
		index = 0;
		stop = false;
		String ret = "\n";
		while (!stop)  {
			int newindex=text.indexOf(ret,index);
			if (newindex!=-1) {
				retCount++;
				index=newindex+1;
			} else {
				stop=true;
			}
		}

		// number of pages
		//
		// OBS!
		// This produces an overestimation of the number of pages
		// because we do not know the correct line height. The number of 
		// pages is shortened within the print() method.
		//
		PageFormat format = new PageFormat(); // default page format
		double lineheight = 12;
		int numberOfLinesPerPage = (int)(format.getImageableHeight()/lineheight)-1;
		numberOfPages = 1+(int)(retCount/numberOfLinesPerPage);
	}


	public int print(Graphics g, PageFormat format, int pageIndex) {
		AttributedCharacterIterator charIterator = mStyledText.getIterator();

		// set margins and tabstops
		//
		float leftMargin  = 2;
		float rightMargin = (float)(format.getImageableWidth()-leftMargin);
		float tabWidth = 30;
		int totalTabs  = (int)((rightMargin-leftMargin)/tabWidth);
	    if (pageIndex==0) {
			tabStops = new float[totalTabs];
			for (int i=0; i<totalTabs; i++) {
				tabStops[i] = leftMargin+(i+1)*tabWidth;
			}
			iteratorPositionForNextPage = 0;
		}

		// get locations of tabstops 
		//
		if (pageIndex==0) {
			tabLocations = new int[tabCount+1];
			retLocations = new int[retCount+1];
			int i=0, k=0;
			for (char c=charIterator.first(); 
				 c!=charIterator.DONE; 
				 c=charIterator.next()) {
				if (c == '\t') {
					tabLocations[i++] = charIterator.getIndex();
				} else if (c == '\n') {
					retLocations[k++] = charIterator.getIndex();
				}
			}
			tabLocations[tabCount] = charIterator.getEndIndex() - 1;
		} 
		int currentTab = 0, currentRet = 0;
		float verticalPos = 0;


		Graphics2D g2d = (Graphics2D) g;
		g2d.translate(format.getImageableX(), format.getImageableY());
		g2d.setPaint(Color.black);

		//  break our text into lines that fit the page.
		//
		Point2D.Float pen = new Point2D.Float();
		LineBreakMeasurer measurer = new LineBreakMeasurer(charIterator, g2d.getFontRenderContext());
		measurer.setPosition(iteratorPositionForNextPage);
		
		// search for the position of the next tab / user-defined line break
		//
		while (measurer.getPosition()>=retLocations[currentRet]+1) {
			currentRet++;
		}
		while (measurer.getPosition()>=tabLocations[currentTab]+1) {
			currentTab++;
		}

		// extract lines
		//
		while (measurer.getPosition() < charIterator.getEndIndex()) {
			boolean lineContainsText = false;
			boolean lineComplete = false;
			float maxAscent = 0, maxDescent = 0;
			float horizontalPos = leftMargin;
			Vector layouts = new Vector(1);
			Vector penPositions = new Vector(1);
			
			while (!lineComplete) {
				float wrappingWidth = rightMargin - horizontalPos;
				while (measurer.getPosition()>=retLocations[currentRet]+1) {
					currentRet++;
				}

				TextLayout layout =
					measurer.nextLayout(wrappingWidth,
										Math.min(tabLocations[currentTab]+1,
												 retLocations[currentRet]+1),
										lineContainsText);

				/*System.out.println("pos "+measurer.getPosition()+" "+
								   (tabLocations[currentTab]+1)+" "+
								   (retLocations[currentRet]+1));*/
				
				// layout can be null if lineContainsText is true
				if (layout != null) {
					layouts.addElement(layout);
					penPositions.addElement(new Float(horizontalPos));
					horizontalPos += layout.getAdvance();
					maxAscent = Math.max(maxAscent, layout.getAscent());
					maxDescent = Math.max(maxDescent,
										  layout.getDescent() + 
										  layout.getLeading());
				} else {
					lineComplete = true;
				}
				
				lineContainsText = true;

				if (measurer.getPosition() == tabLocations[currentTab]+1) {
					currentTab++;
				}

				if (measurer.getPosition()== retLocations[currentRet]+1) {
					lineComplete = true;
				}
				
				if (measurer.getPosition() == charIterator.getEndIndex())
					lineComplete = true;
				else if (horizontalPos >= tabStops[tabStops.length-1])
					lineComplete = true;
				
				if (!lineComplete) {
					// move to next tab stop
					int j;
					for (j=0; horizontalPos >= tabStops[j]; j++) {}
					horizontalPos = tabStops[j];
					//System.out.println("new horizontalpos: "+horizontalPos);
				}
			}

			verticalPos += maxAscent;
			
			Enumeration layoutEnum = layouts.elements();
			Enumeration positionEnum = penPositions.elements();
			
			// now iterate through layouts and draw them
			while (layoutEnum.hasMoreElements()) {
				TextLayout nextLayout = (TextLayout) layoutEnum.nextElement();
				Float nextPosition = (Float) positionEnum.nextElement();
				nextLayout.draw(g2d, nextPosition.floatValue(), verticalPos);
			}

			verticalPos += maxDescent;

			if (measurer.getPosition()==charIterator.getEndIndex()) {
				System.out.println("p"+pageIndex+" stop");
				stopNow++;
			}

			if (verticalPos >= format.getImageableHeight()) {
				// page is complete
				iteratorPositionForNextPage = measurer.getPosition();
				break;
			}
		}

		

		if (stopNow==3) {
			numberOfPages=pageIndex; 
		}

		if (pageIndex >= numberOfPages) {
			return Printable.NO_SUCH_PAGE;
		} 
		return Printable.PAGE_EXISTS;
	}

}
