/*----------------------------------------------------------------------*
 *  FourierTask
 *  starts a task for a long fourier calculation
 *----------------------------------------------------------------------*/

public class FourierTask extends Task {

    private String title;
    private double from, to, step, zeropoint;
    private int    stepquality, datamode, compactmode, weight;
    private XProjectFrame mainframe;
    private XTabFourier tab;
    private boolean isAutoAcceptFreq;

    public FourierTask(XProjectFrame mainframe, XTabFourier tab, 
		       String title, double from, double to,
		       int stepquality, double step, int datamode,
		       int compactmode, int weight, double zeropoint, 
		       boolean autoaccept) {
	this.mainframe   = mainframe;
	this.tab         = tab;
	this.title       = title;
	this.from        = from;
	this.to          = to;
	this.stepquality = stepquality;
	this.step        = step;
	this.datamode    = datamode;
	this.compactmode = compactmode;
	this.weight      = weight;
	this.zeropoint   = zeropoint;
	this.isAutoAcceptFreq = autoaccept;
    }

    public FourierTask(XProjectFrame mainframe, XTabFourier tab, 
		       String title, double from, double to,
		       int stepquality, double step, int datamode,
		       int compactmode, int weight, double zeropoint) {
	this.mainframe   = mainframe;
	this.tab         = tab;
	this.title       = title;
	this.from        = from;
	this.to          = to;
	this.stepquality = stepquality;
	this.step        = step;
	this.datamode    = datamode;
	this.compactmode = compactmode;
	this.weight      = weight;
	this.zeropoint   = zeropoint;
	this.isAutoAcceptFreq = false;
    }

    //---
    //--- called from XTabFourier to start the task.
    //---
    public void go() {
	worker = new SwingWorker() {
		public Object construct() {
		    done        = false;
		    canceled    = false;
		    return new ActualFourierTask();
		}
	    };
        worker.start();
    }

    public void get() {
	worker.get();
    }

    //---
    //--- the actual long running task. this runs in a SwingWorker thread.
    //---
    class ActualFourierTask {
        ActualFourierTask() {
	    while (!isCanceled() && !isDone()) {
		// now go and calculate
		Period04.projectCalculateFourier(title, from, to, stepquality,
					      step, datamode, 
					      compactmode, weight, zeropoint);
		done = true;
		// remove the calculating panel
		mainframe.setCalculating(false, null, "", 0, true);
		if (tab.getAutoFourierDisplay()==true) {
		    tab.displayFourierGraph();
		}
		tab.writeCalculationResultsToProtocol(isAutoAcceptFreq);
		tab.updateDisplay();
		tab.dereferenceTask();
	    }
	}
    }
}
