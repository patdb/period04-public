/*-----------------------------------------------------------------------*
 *  XPlotFourier
 *  plots the active fourier spectrum
 *-----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.help.CSH;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.MouseInputAdapter;

import gov.noaa.pmel.sgt.dm.SGTData;
import gov.noaa.pmel.sgt.dm.SGTLine;
import gov.noaa.pmel.sgt.dm.SimpleLine;
import gov.noaa.pmel.sgt.dm.SGTMetaData;

import gov.noaa.pmel.sgt.swing.XPlotLayout;

import gov.noaa.pmel.sgt.Axis; 
import gov.noaa.pmel.sgt.CartesianGraph;
import gov.noaa.pmel.sgt.CartesianRenderer;
import gov.noaa.pmel.sgt.Graph;
import gov.noaa.pmel.sgt.LineAttribute;
import gov.noaa.pmel.sgt.SGLabel;
import gov.noaa.pmel.sgt.AxisNotFoundException;

import gov.noaa.pmel.util.Point2D;
import gov.noaa.pmel.util.Domain;
import gov.noaa.pmel.util.Range2D;
import gov.noaa.pmel.util.SoTPoint;

public class XPlotFourier extends XPlot {

    private JMenuBar    menuBar = new JMenuBar();
    private JMenu       menuFile, menuDisplay, menuZoom, menuHelp;
    private JMenuItem   i_Print, i_ExportJPG, i_ExportEPS, i_ExportTable;
    private JMenuItem   i_Close, i_DisplayAll, i_SelectViewport, i_Back;
    private JMenuItem   i_PlotHelp;
    private JCheckBoxMenuItem i_DisplayHeader;
    private JCheckBoxMenuItem i_Power;
    private ActionListener action;

    private Color      toppanelColor = new Color(230,225,217);
    private JButton b_Back, b_DisplayAll;

    private double [] xArray;
    private double [] yArray;
    private SGTData data;

    private int fourierID;

    public XPlotFourier(XProjectFrame mainframe) {
	this.mainframe = mainframe;
	fourierID      = Period04.projectGetFourierActiveIndex();
	mainpanel      = new JPanel();
	layout         = new XPlotLayout();
	layout.setIsFourier(true);
	createMenu(); 

	Container content = getContentPane();
	JPanel plotpanel = new JPanel();
	plotpanel.setLayout(new BorderLayout());
	plotpanel.add(layout, BorderLayout.CENTER);

	statusBar = new JLabel(stdtext);
	statusBar.setFont(new Font("Dialog", Font.PLAIN, 11));
	statusBar.setBorder(
	    BorderFactory.createEtchedBorder(EtchedBorder.RAISED));


	/*
	 * Listen for window closing events and release resources
	 */
	addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent event) {
		    JFrame fr = (JFrame)event.getSource();
		    fr.setVisible(false);
		    fr.dispose();
		}
		public void windowActivated(WindowEvent we) {
		    XPlotFourier.this.mainframe.updateCurrentProject();
		}
	    });

	/*
	 * read the data and add to layout
	 */
	layout.setBatch(true); 
	data = readFourierData();
	layout.addData(data, new LineAttribute(LineAttribute.SOLID),
		       "Fourier", XPlotLayout.LINE);
	// slightly adjust yrange
	try {
	    Range2D yrange = 
		computeYRange(layout.getRange().getXRange().start, 
			      layout.getRange().getXRange().end);
	    layout.setRange(new Domain(layout.getRange().getXRange(),
				       yrange));
	} catch (Exception e) {}
	displayHeader(true);

	content.setLayout(new BorderLayout());
	content.add(createTopPanel(), BorderLayout.NORTH);
	content.add(plotpanel, BorderLayout.CENTER);
	content.add(statusBar, BorderLayout.SOUTH);

    	/*
	 * Turn batching off.  All batched changes to the XPlotLayout will
	 * now be executed.
	 */
	layout.setBatch(false);
	layout.addMouseListener(new MyMouseInputAdapter());
	layout.addMouseMotionListener(new MyMouse());
	
	setTitle("Fourier Graph: "+Period04.fourierGetTitle());
	setSize(500,400);
	layout.resetLabels();
	setLocationRelativeTo(mainframe.getFrame());
	setVisible(true);
    }

    void createMenu() {
	// create main-menu
	menuFile    = XMenuHelper.addMenuBarItem( menuBar, "_Graph" );
	menuDisplay = XMenuHelper.addMenuBarItem( menuBar, "_Display" );
	menuZoom    = XMenuHelper.addMenuBarItem( menuBar, "_Zoom" );
	menuHelp    = XMenuHelper.addMenuBarItem( menuBar, "_Help" );

	// create action listener for menu items
	action = new ActionListener() {
		public void actionPerformed( ActionEvent evt ) {
		    Object s=evt.getSource();
		    // file menu
		    if (s==i_Print)          { print();       }
		    else if (s==i_ExportEPS) { exportImage(XPlot.EPS); }
		    else if (s==i_ExportJPG) { exportImage(XPlot.JPG); }
			else if (s==i_ExportTable) { exportTable(); }
		    else if (s==i_Close)     { dispose();     }
		    // display menu
		    else if (s==i_Power) { repaint(i_Power.isSelected()); }
		    else if (s==i_DisplayHeader) { 
			displayHeader(i_DisplayHeader.isSelected()); 
		    }
		    // zoom menu
		    else if (s==i_DisplayAll || s==b_DisplayAll)     { resetZoom(); }
		    else if (s==i_SelectViewport) { setZoom("Frequency","Amplitude"); }
		    else if (s==i_Back || s==b_Back)           { layout.undoZoom(); }
		}
	    };

	//---
	//--- file menu	
	//---
	i_Print = XMenuHelper.addMenuItem(menuFile, "Print Graph", action);
	JMenu export = new JMenu("Export Graph As ");
	menuFile.add( export );
	i_ExportEPS = XMenuHelper.addMenuItem(export, "eps", action);
	i_ExportJPG = XMenuHelper.addMenuItem(export, "jpg", action);
	// ...
	i_ExportTable = XMenuHelper.addMenuItem(menuFile, "Export data", action);
	i_Close = XMenuHelper.addMenuItem(menuFile, "Close", action);
	//---
	//--- display menu
	//---
	i_Power = XMenuHelper.addCheckBoxMenuItem(
	    menuDisplay, "Display power", action);
	i_DisplayHeader = XMenuHelper.addCheckBoxMenuItem(
	    menuDisplay, "Display header", action);
	i_DisplayHeader.setSelected(true);
	//---
	//--- zoom menu
	//---
	i_DisplayAll = XMenuHelper.addMenuItem(
	    menuZoom,"_Display all", 'D', action);
	i_SelectViewport = XMenuHelper.addMenuItem(
	    menuZoom,"Select _viewport", 'V', action);
	i_Back = XMenuHelper.addMenuItem(
	    menuZoom, "_Back", 'B', action);
	//---
	//--- help menu
	//---
	i_PlotHelp = XMenuHelper.addMenuItem(
	    menuHelp, "Fourier plots",
	    new CSH.DisplayHelpFromSource(XProjectFrame.getHelpBroker()));
	CSH.setHelpIDString(i_PlotHelp,"gui.fourierplots"); // set link

	// add it to the panel
	this.setJMenuBar( menuBar );
    }


    private JPanel createTopPanel() {
	JPanel panel = new JPanel();
	panel.setLayout(new BoxLayout(panel,BoxLayout.PAGE_AXIS));
	panel.setBorder(BorderFactory.createEtchedBorder());

	//---
	//--- first line
	//---
	JPanel panel1 = new JPanel();
	panel1.setLayout(new FlowLayout(FlowLayout.RIGHT, 2, 2));
	panel1.setBackground(toppanelColor);

	b_Back = new JButton("Back");
	b_Back.setFont(new Font("Dialog", Font.PLAIN, 10));
	b_Back.setMargin(new Insets(0,0,0,0));
	b_Back.setBackground(toppanelColor);
	b_Back.addActionListener(action);

	JLabel spacer1 = new JLabel(" ");

	b_DisplayAll = new JButton("Show All");
	b_DisplayAll.setFont(new Font("Dialog", Font.PLAIN, 10));
	b_DisplayAll.setMargin(new Insets(0,0,0,0));
	b_DisplayAll.setBackground(toppanelColor);
	b_DisplayAll.addActionListener(action);

	panel1.add(b_Back);
	panel1.add(spacer1);
	panel1.add(b_DisplayAll);

	panel.add(panel1);

	return panel;
    }


    SGTLine readFourierData() {
	ArrayList alist = new ArrayList(100);
	Point2D.Double point = null;

	for (int i=0; i<Period04.projectGetFourierActivePoints(); i++) {
	    point = new Point2D.Double(
		Period04.projectGetFourierPointFrequency(i),
		Period04.projectGetFourierPointAmplitude(i));
	    alist.add(point);
	}

        xArray = new double[alist.size()];
	yArray = new double[alist.size()];
	Iterator it = alist.iterator();
	int i=0;
	while(it.hasNext()) {
	    point = (Point2D.Double)it.next();
	    xArray[i] = point.x;
	    yArray[i] = point.y;
	    i++;
	}

	SimpleLine data = new SimpleLine(xArray, yArray, "");
	SGTMetaData meta = new SGTMetaData("Frequency", "", false, false);
	data.setXMetaData(meta);
	meta = new SGTMetaData("Amplitude", "", false, false);
	data.setYMetaData(meta);

	return data;
    }

    private void repaint(boolean isPower) {
	layout.setBatch(true);
	Domain d = layout.getRange();  // save the old graph domain
	layout.clear();
	SGTData data;
	if (isPower) {
	    int i=0;
	    while(i<yArray.length) {
		yArray[i] = yArray[i] * yArray[i];
		i++;
	    }
	    SimpleLine newdata = new SimpleLine(xArray, yArray, "");
	    newdata.setXMetaData(new SGTMetaData("Frequency", "", false, false));
	    newdata.setYMetaData(new SGTMetaData("Power", "", false, false));
	    data = newdata;
	} else {
	    data = readFourierData();
	}
	layout.addData(data, new LineAttribute(LineAttribute.SOLID),
		       "Fourier", XPlotLayout.LINE);
	// set the old xrange and calculate an appropriate yrange
	try {
	    Range2D yrange = 
		computeYRange(d.getXRange().start, d.getXRange().end);
	    layout.setRange(new Domain(d.getXRange(),yrange));//
	} catch (Exception e) {}
	layout.setBatch(false);
    }

    private Range2D computeYRange(double start, double end) {
	double ymax = 0.0;
	for (int i=0; i<xArray.length; i++) {
	    if (start>xArray[i]) { continue; }
	    if (end<xArray[i])   { break; }
	    if (yArray[i]>ymax)  { ymax = yArray[i]; }
	}
	return new Range2D(0.0, 1.02*ymax);
    }

    public void displayHeader(boolean show) {
	if (show) {
	    StringBuffer text = new StringBuffer();
	    text.append(Period04.projectGetFourierTitle(fourierID));
	    String [] peak = Period04.fourierGetPeak(fourierID);
	    text.append(" ( F="+peak[0]+", A="+peak[1]+" )");
	    layout.setPlotTitle(text.toString());
	} else {
	    layout.setPlotTitle("");
	}
    }

	public void exportTable() {
		mainframe.getFourierTab().exportActiveFourier();
	}

	class MyMouseInputAdapter extends MouseInputAdapter {
		public void mouseReleased(MouseEvent evt) {
			// only for the right mouse button
			if (!SwingUtilities.isRightMouseButton(evt)) { return; } 
			int xD = evt.getX();    // get position of the point where
			int yD = evt.getY();    // MouseEvent has been detected
			final Point2D.Double ptU=transformDtoU(xD,yD); //transform to user coord.
			final Point2D.Double ptUp3=transformDtoU(xD+3,yD); //transform to user coord.
			final double binscale = ptUp3.x-ptU.x;
			if (isWithinDomain()) {   
				if (!evt.isControlDown()) {
					JPopupMenu popup = new JPopupMenu();
					JMenuItem menuItemSNR = new JMenuItem("Compute S/N for this frequency peak");
					JMenuItem menuItemAdd = new JMenuItem("Add peak to frequency list");
					menuItemSNR.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent evt) {
								if (mainframe.getFourierTab().getNoiseDialog()==null)
									mainframe.getFourierTab().calculateNoiseAtFrequency();
								mainframe.getFourierTab().getNoiseDialog().setOtherFrequencyPeak(Period04.projectGetSubPeakFrequency(ptU.x, binscale), binscale);
								mainframe.getFourierTab().getNoiseDialog().setVisible(true);
							}
						});
					menuItemAdd.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent evt) {
								double fr = Period04.projectGetSubPeakFrequency(ptU.x, binscale);
								double ampl= Period04.projectGetSubPeakAmplitude(ptU.x, binscale);
								int freqindex = Period04.projectAddFrequencySubPeak(fr, ampl);
								mainframe.updateFitDisplay();
								mainframe.getLog().writeProtocol(
									 "Frequency:   "+fr+"\n"+
									 "Amplitude:   "+ampl+"\n"+
									 "has been included as frequency "+
									 (freqindex+1)+"\n", false);
							}
						});
					popup.add(menuItemSNR);
					popup.add(menuItemAdd);
					popup.show(evt.getComponent(), evt.getX(), evt.getY());
				} else { 
					if (mainframe.getFourierTab().getNoiseDialog()==null)
						mainframe.getFourierTab().calculateNoiseAtFrequency();
					mainframe.getFourierTab().getNoiseDialog().setOtherFrequencyPeak(Period04.projectGetSubPeakFrequency(ptU.x, binscale), binscale);
					mainframe.getFourierTab().getNoiseDialog().setVisible(true);
				}
			}
		}
	}
}
