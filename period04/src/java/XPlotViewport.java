/*---------------------------------------------------------------------*
 *  XPlotViewport.java
 *  shows a dialog to define a viewport and plots it
 *---------------------------------------------------------------------*/

import java.awt.*;                    
import java.awt.event.*;
import javax.swing.*;
import java.util.Iterator;

import java.beans.PropertyVetoException;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import gov.noaa.pmel.sgt.swing.XGraphicLayout;
import gov.noaa.pmel.sgt.AbstractPane;
import gov.noaa.pmel.sgt.dm.SGTData;
import gov.noaa.pmel.sgt.dm.SGTLine;
import gov.noaa.pmel.sgt.dm.SimpleLine;
import gov.noaa.pmel.sgt.dm.SGTMetaData;

import gov.noaa.pmel.sgt.Axis; 
import gov.noaa.pmel.sgt.CartesianGraph;
import gov.noaa.pmel.sgt.CartesianRenderer;
import gov.noaa.pmel.sgt.Graph;
import gov.noaa.pmel.sgt.LineAttribute;
import gov.noaa.pmel.sgt.SGLabel;
import gov.noaa.pmel.sgt.AxisNotFoundException;

import gov.noaa.pmel.util.Domain;
import gov.noaa.pmel.util.Range2D;

class XPlotViewport extends JDialog implements ActionListener {
    private static final long serialVersionUID = -2966569231186194516L;    
    private JOptionPane optionPane;
    private JButton     b_ok, b_cancel;
    private JTextField  t_FreqMin, t_FreqMax, t_AmpMin, t_AmpMax;
    private Double      xstart, xend, ystart, yend;

    private JFrame      mainframe;
    private XGraphicLayout layout;
    private Domain      range;
    private Range2D     xrange , yrange;
    
    public XPlotViewport(JFrame mainframe, XGraphicLayout layout, 
			 String xAxis, String yAxis) {
	this.mainframe = mainframe;
	this.layout = layout;
	range = layout.getRange();
	xrange = range.getXRange();
	yrange = range.getYRange();
	t_FreqMin = new JTextField(format(xrange.start));
	t_FreqMax = new JTextField(format(xrange.end));
	t_AmpMin  = new JTextField(format(yrange.start));
	t_AmpMax  = new JTextField(format(yrange.end));

	JPanel panel = new JPanel();
	panel.setLayout(new GridLayout(4,2));
	panel.add(new JLabel(xAxis+" min:"));
	panel.add(t_FreqMin);
	panel.add(new JLabel(xAxis+" max:"));
	panel.add(t_FreqMax);
	panel.add(new JLabel(yAxis+" min:"));
	panel.add(t_AmpMin);
	panel.add(new JLabel(yAxis+" max:"));
	panel.add(t_AmpMax);
	Object pane = panel;
	
	b_ok     = new JButton("OK");
	b_cancel = new JButton("Cancel");
	b_ok.addActionListener(this);
	b_cancel.addActionListener(this);
	Object [] options = {b_ok, b_cancel};
	
	optionPane = new JOptionPane(
	    pane, JOptionPane.PLAIN_MESSAGE, JOptionPane.YES_NO_OPTION,
	    null, options, options[0]);
	setContentPane(optionPane);
	
	this.setTitle("Set viewport for plot");
	this.pack();
	Point loc = mainframe.getLocation();
	this.setLocationRelativeTo(mainframe);
	this.setVisible(true);
    }
    
    public void actionPerformed(ActionEvent evt) {
	Object s = evt.getSource(); 
	if (s==b_ok) {
	    Double freqmin = checkNumber(t_FreqMin.getText());
	    if (freqmin==null) { return; }
	    Double freqmax = checkNumber(t_FreqMax.getText());
	    if (freqmax==null) { return; }
	    Double ampmin  = checkNumber(t_AmpMin.getText());
	    if (ampmin ==null) { return; }
	    Double ampmax  = checkNumber(t_AmpMax.getText());
	    if (ampmax ==null) { return; }
	    // ok all values have been accepted now set the range
	    Range2D xrange = new Range2D(freqmin.doubleValue(), 
					 freqmax.doubleValue());
	    Range2D yrange = new Range2D(ampmin.doubleValue(), 
					 ampmax.doubleValue());
	    Domain range = new Domain(xrange, yrange);
	    if ((mainframe instanceof XPlotTimeString) ||
		(mainframe instanceof XPlotPhase)) {
		if (Period04.projectGetReverseScale()) {
		    range.setYReversed(true);
		}
	    }
	    try {
		layout.setBatch(true);
		layout.setClipping(true);
		layout.setRange(range);
		layout.setBatch(false);
	    } catch(PropertyVetoException e) {
		System.err.println(e);
	    }
	}	
	dispose();
    }

    //---
    //--- check wether the input is a valid number
    //---
    public Double checkNumber(String s_num) {
	Double num = null;
	// check wether this is a valid number
	NumberFormatException nan = null;
	try { 
	    num = new Double(s_num);
	} catch (NumberFormatException exception) { 
	    nan=exception; 
	}
	if ( s_num.equals("") || (nan!=null) ) {
	    // text was no number
	    JOptionPane.showMessageDialog(
		XPlotViewport.this, "Sorry, \""+s_num+"\" "+
		"isn't a valid number.\n", "Error",
		JOptionPane.ERROR_MESSAGE);
	    return null;
	}
	return num;
    }

    //---
    //--- format numbers and return them as string
    //---
    protected String format(double value) {
	NumberFormat fmt = NumberFormat.getInstance(Locale.UK);
	if (fmt instanceof DecimalFormat) {
	    ((DecimalFormat) fmt).setDecimalSeparatorAlwaysShown(true);
	}
	fmt.setGroupingUsed(false);
	fmt.setMaximumFractionDigits(5);
	return String.valueOf(fmt.format(value));
    }

}

