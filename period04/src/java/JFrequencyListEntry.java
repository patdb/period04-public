/*--------------------------------------------------------------------*
 *  JFrequencyListEntry
 *  creates on entry for the frequency list in the Fit panel
 *--------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.Document;


public class JFrequencyListEntry 
    extends JPanel
{
    protected XTabFit       fitTab;
    protected int           ID=-1;     // zero-based!
    private JCheckBox       cb_Active;
    protected JTextField    t_Frequency, t_Amplitude, t_Phase;
    private JFreqCompButton b_FreqComp;
    private boolean         isComposition;
    private boolean         blockChangeEvents = false;

    private TextChangeListener textlistener = new TextChangeListener();

    public JFrequencyListEntry(XTabFit tab, int eidi) {
	fitTab=tab;
       	ID=eidi;
	setLayout(null);
	
	Font listFont = new Font("Dialog", Font.PLAIN, 11);

	cb_Active = new JCheckBox("F"+Integer.toString(ID+1));
	cb_Active.setFont(listFont);
	cb_Active.setMargin(new Insets(0,0,0,0));
	cb_Active.setSelected(false);
	
	b_FreqComp = new JFreqCompButton();
	b_FreqComp.setVisible(false);
	
	t_Frequency = new JTextField("0");
	t_Amplitude = new JTextField("0");
	t_Phase     = new JTextField("0");
	t_Frequency .setFont(listFont);
	t_Amplitude .setFont(listFont);
	t_Phase     .setFont(listFont);

	cb_Active  .setBounds(   2, 0,  57, 16 );
	b_FreqComp .setBounds(  60, 1,  19, 15 );
	t_Frequency.setBounds(  80, 0, 130, 17 );
	t_Amplitude.setBounds( 230, 0, 110, 17 );
	t_Phase    .setBounds( 360, 0,  90, 17 );
		
	add( cb_Active, null );
	add( b_FreqComp, null );
	add( t_Frequency, null );
	add( t_Amplitude, null );
	add( t_Phase, null );

	addListener();
    }

    private void addListener() {
	cb_Active.addItemListener(new ItemListener() {
		public void itemStateChanged(ItemEvent evt) {
		    setActive(cb_Active.isSelected());
		    Period04.projectResetLambda();
		}
	    });
	
	t_Frequency.getDocument().addDocumentListener(textlistener);
	t_Frequency.getDocument().putProperty("mode", "normal");
	t_Frequency.getDocument().putProperty("name", t_Frequency);
	t_Frequency.getDocument().putProperty("type", "Frequency");
	
	t_Amplitude.getDocument().addDocumentListener(textlistener);
	t_Amplitude.getDocument().putProperty("mode", "normal");
	t_Amplitude.getDocument().putProperty("name", t_Amplitude);
	t_Amplitude.getDocument().putProperty("type", "Amplitude");
		
	t_Phase.getDocument().addDocumentListener(textlistener);
	t_Phase.getDocument().putProperty("mode", "normal");
	t_Phase.getDocument().putProperty("name", t_Phase);
	t_Phase.getDocument().putProperty("type", "Phase");

	// for the Popup Menu
	t_Frequency.addMouseListener(new MyMouseInputAdapter());
    }

    public int getID() { return ID; }

    public double getFrequency() { 
	if (fitTab.getFitMode()==XTabFit.BINARY_MODE) {
	    return Period04.projectGetBMFrequencyValue(ID); 
	} 
	// normal mode:
	return Period04.projectGetFrequencyValue(ID); 
    }


    public double getAmplitude() { 
	if (fitTab.getFitMode()==XTabFit.BINARY_MODE) {
	    return Period04.projectGetBMAmplitudeValue(ID); 
	} 
	// normal mode:
	return Period04.projectGetAmplitudeValue(ID); 
    }

    public boolean isComposition() {
	return isComposition;
    }

    public boolean isEmpty() {
	boolean val=true;
	String s_freq = t_Frequency.getText();
	if(!s_freq.equals("0") && !s_freq.equals("")) {  val=false; }
	String s_amp = t_Amplitude.getText();
	if(!s_amp.equals("0") && !s_amp.equals("")) {  val=false; }
	String s_pha = t_Phase.getText();
	if(!s_pha.equals("0") && !s_pha.equals("")) {  val=false; }
	return val;
    }

    public boolean isUnused() {
	// is this a deselected non-empty entry?
	if (!isEmpty() && !cb_Active.isSelected())
	    return true;
	return false;
    }

    public void showComposition(boolean val) {
	blockChangeEvents = true;
	b_FreqComp.showComposition(val);
	blockChangeEvents = false;
    }


    public void setSelected(boolean select) {
	if (select) {
	    String s_freq = t_Frequency.getText();
	    if(s_freq.equals("0") || s_freq.equals("")) { return; }
	    // to be secure check other zero values too
	    NumberFormatException nan = null;
	    double fr=-1;
	    try { 
		fr = Double.parseDouble(s_freq); 
	    } catch (NumberFormatException exception) { 
		nan=exception;
	    }		   
	    if (nan==null && fr==0.0) { return; }
	    if ( fr!=0 ) {
		cb_Active.setSelected(true);
	    }
	} else {
		cb_Active.setSelected(false);	    
	}
    } 

    private void setActive(boolean value) {
	if (fitTab.getFitMode()==XTabFit.BINARY_MODE) {
	    // check if we have to resize the period array
	    if (ID>=Period04.projectGetBMTotalFrequencies()) {
		Period04.projectSetBMTotalFrequencies(ID);
	    }
	    Period04.projectSetBMActive(ID, value);
	} else { // NORMAL_MODE
	    // check if we have to resize the period array
	    if (ID>=Period04.projectGetTotalFrequencies()) {
		Period04.projectSetTotalFrequencies(ID);
	    }
	    Period04.projectSetActive(ID, value);
	}
    }

    public void updateData() {
	if (fitTab.getFitMode()==XTabFit.BINARY_MODE) {
	    if (ID>=Period04.projectGetBMTotalFrequencies()) {
		Period04.projectSetBMTotalFrequencies(ID+1);
	    }
	} else { // NORMAL MODE
	    if (ID>=Period04.projectGetTotalFrequencies()) {
		Period04.projectSetTotalFrequencies(ID+1);
	    }
	}
	blockChangeEvents = true;
	String comp;
	if (fitTab.getFitMode()==XTabFit.BINARY_MODE) {
	    // set active frequencies selected
	    cb_Active.setSelected(Period04.projectGetBMActive(ID));
	    // is it a composition?
	    comp = Period04.projectGetBMComposite(ID); 
	} else { // NORMAL MODE
	    // set active frequencies selected
	    cb_Active.setSelected(Period04.projectGetActive(ID));
	    // is it a composition?
	    comp = Period04.projectGetComposite(ID); 
	}
	if (comp.length()!=0) {
	    b_FreqComp.setValues();
	    b_FreqComp.setVisible(true);
	    isComposition = true;
	} else {
	    b_FreqComp.setVisible(false);
	    isComposition = false;
	    if (fitTab.getFitMode()==XTabFit.BINARY_MODE) {
		t_Frequency.setText(Period04.projectGetBMFrequency(ID));
	    } else {
		t_Frequency.setText(Period04.projectGetFrequency(ID));
	    }
	}
	if (fitTab.getFitMode()==XTabFit.BINARY_MODE) {
	    // set amplitude
	    t_Amplitude.setText(Period04.projectGetBMAmplitude(ID));
	    // set phase
	    t_Phase.setText(Period04.projectGetBMPhase(ID));
	} else { // NORMAL_MODE
	    // set amplitude
	    t_Amplitude.setText(Period04.projectGetAmplitude(ID));
	    // set phase
	    t_Phase.setText(Period04.projectGetPhase(ID));
	}
	t_Frequency.setCaretPosition(0);
	blockChangeEvents = false;
    }
    
    public void setData() {
	if (fitTab.getFitMode()==XTabFit.BINARY_MODE) {
	    String protocol=Period04.projectSetBMFrequency(ID,t_Frequency.getText());
	    //mainframe.getLog().writeProtocol(protocol, true);
	    //--- setFrequency() activates the frequency,
	    //--- in order to maintain the old value which is still 
	    //--- shown on the user interface, we call:
	    Period04.projectSetBMActive(ID, cb_Active.isSelected());
	    Period04.projectCheckBMActive();   // perform composition check
	} else { // NORMAL_MODE
	    String protocol;
	    if (isComposition)
		protocol=Period04.projectSetFrequency(ID,b_FreqComp.getComposite());
	    else
		protocol=Period04.projectSetFrequency(ID,t_Frequency.getText());
	    //mainframe.getLog().writeProtocol(protocol, true);
	    //--- setFrequency() activates the frequency,
	    //--- in order to maintain the old value which is still 
	    //--- shown on the user interface, we call:
	    Period04.projectSetActive(ID, cb_Active.isSelected());
	    Period04.projectCheckActive();   // perform composition check
	}
	updateData();                    // update frequency data
    }
	
    private void setData(JTextField tf, String type) {
	if (!blockChangeEvents) {
	    int mode=fitTab.getFitMode();
	    if (mode==XTabFit.BINARY_MODE) {
		if (ID>=Period04.projectGetBMTotalFrequencies()) {
		    Period04.projectSetBMTotalFrequencies(ID+1);  
		}		
	    } else { // NORMAL MODE
		if (ID>=Period04.projectGetTotalFrequencies()) {
		    Period04.projectSetTotalFrequencies(ID+1);  
		}
	    }
	    String tftext=tf.getText();
	    if (tftext.length()==0) { tftext="0"; }
	    if (type=="Frequency") {
		isComposition = false;
		if (tftext.startsWith("="))
		    return; // it's a composition, set this later, when complete
		//--- set the frequency
		if (mode==XTabFit.BINARY_MODE) {Period04.projectSetBMFrequency(ID, tftext);}
		else { Period04.projectSetFrequency(ID, tftext); }
		//--- setFrequency() activates the frequency,
		//--- in order to maintain the old value which is still shown
		//--- on the user interface, we call:
		if (mode==XTabFit.BINARY_MODE) {
		    Period04.projectSetBMActive(ID,cb_Active.isSelected());
		    Period04.projectCheckBMActive(); // perform composition check
		} else {
		    Period04.projectSetActive(ID,cb_Active.isSelected());
		    Period04.projectCheckActive(); // perform composition check
		}
	    } else if (type=="Amplitude") {
		if (mode==XTabFit.BINARY_MODE) {Period04.projectSetBMAmplitude(ID, tftext);} 
		else { Period04.projectSetAmplitude(ID, tftext); }
	    } else if (type=="Phase") {
		if (mode==XTabFit.BINARY_MODE){Period04.projectSetBMPhase(ID, tftext);} 
		else { Period04.projectSetPhase(ID, tftext); }
	    }	    
	    fitTab.updateFrequencyFieldNumber();
	}
    }

    public void clean() {
	isComposition = false;
	b_FreqComp.setVisible(false);
	cb_Active .setSelected(false);  // deselect all freq check boxes
	t_Frequency.setText("0");   // reset frequencies
	t_Amplitude.setText("0");   // reset amplitudes
	t_Phase    .setText("0");   // reset phase
    }

    //---
    //--- JFreqCompButton
    //--- this creates a button that makes it possible to switch between
    //--- frequency and composition string
    //---
    class JFreqCompButton extends JButton {
	private boolean lastvalue;
	private String  frequency, composition;

	public JFreqCompButton() {
	    setText("C");
	    setFont(new Font("Dialog", Font.PLAIN, 9));
	    setMargin(new Insets(0,0,0,0));
	    addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent evt) {
			JFreqCompButton b=(JFreqCompButton)evt.getSource();
			b.flip();
		    }
		});
	} 
	public void setValues() {
	    //	    if (ID<0) return;
	    boolean isUseFreqValue = false;
	    if (fitTab.getFitMode()==XTabFit.BINARY_MODE) {
		frequency = Period04.projectGetBMFrequency(ID);
		composition = Period04.projectGetBMComposite(ID);
		isUseFreqValue= Period04.projectGetBMCompoUseFreqValue(ID);
	    } else { // NORMAL_MODE
		frequency = Period04.projectGetFrequency(ID);
		composition = Period04.projectGetComposite(ID);
		isUseFreqValue= Period04.projectGetCompoUseFreqValue(ID);
	    }
	    showComposition(!isUseFreqValue);
	}
	public void showComposition(boolean value) {
	    //	    if (ID<0) return;
	    blockChangeEvents = true;
	    if (value) { t_Frequency.setText(composition); } 
	    else       { t_Frequency.setText(frequency); }
	    if (fitTab.getFitMode()==XTabFit.BINARY_MODE) {
		Period04.projectSetBMCompoUseFreqValue(ID, !value);
	    } else { // NORMAL_MODE
		Period04.projectSetCompoUseFreqValue(ID, !value);
	    }
	    lastvalue=value;
	    blockChangeEvents = false;
	}
	public void   flip()         { showComposition(!lastvalue); }
	public String getComposite() { return composition; }
	public String getFrequency() { return frequency; }
    }

    //---
    //--- TextChangeListener
    //---
    class TextChangeListener implements DocumentListener {
	public void changedUpdate(DocumentEvent evt) { }
	public void insertUpdate(DocumentEvent evt) {
	    if (blockChangeEvents) { return; }
	    Document doc = evt.getDocument();
	    setData((JTextField)doc.getProperty("name"),
		    (String)    doc.getProperty("type"));
	}
	public void removeUpdate(DocumentEvent evt) {
	    if (blockChangeEvents) { return; }
	    Document doc = evt.getDocument();
	    setData((JTextField)doc.getProperty("name"),
		    (String)    doc.getProperty("type"));
	}
    }

    //---
    //--- Popup Menu for List Item
    //---
    class MyMouseInputAdapter extends MouseInputAdapter {
	public void mousePressed(MouseEvent evt) {
	    // only for the right mouse button
	    if (!SwingUtilities.isRightMouseButton(evt)) { return; } 
	    if (isEmpty()) { return; }

	    JPopupMenu popup = new JPopupMenu();
	    JMenuItem menuItemHarmonics = new JMenuItem("Add harmonics of this frequency");
	    menuItemHarmonics.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent evt) {
			fitTab.addHarmonics(getID());
		    }
		});
	    popup.add(menuItemHarmonics);
	    popup.show(evt.getComponent(), evt.getX(), evt.getY());
	}
    }
}
