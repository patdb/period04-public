/*----------------------------------------------------------------------*
 *  XDialogAdjustData
 *  a dialog for adjusting the zeropoints of the data
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterJob;
import java.beans.*;                  // property change stuff
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;


class XDialogAdjustData extends XDialog 
    implements ActionListener, ItemListener//, Printable
{
    private JOptionPane   optionPane;
    private final JButton b_OK     = createButton("Adjust");
    private final JButton b_Save   = createButton("Export");
    private final JButton b_Print  = createButton("Print");
    private final JButton b_Cancel = createButton("Close");
    
    private JCheckBox        checkUseWeights;
    private JComboBox        cb_attribute;   
    private JList            listbox;
    private SortedListModel  listmodel;

    private XProjectFrame mainframe;
    private XTabTimeString tab;

    // a variable to prevent double eventhandling
    private boolean itemselected = false;

    
    public XDialogAdjustData(XProjectFrame frame, XTabTimeString tabTS) {
	super(frame.getFrame(), true); // necessary to keep dialog in front
	mainframe = frame;
	tab = tabTS;
	Object[] options = {b_OK, b_Save, b_Print, b_Cancel}; //set buttons 
	
	//---
	//--- create the panel and add compontents to it
	//---

	JPanel inputpanel = new JPanel();
	inputpanel.setLayout(null);
	
	JLabel l_show = createLabel("Show attribute:");
	String [] s_attributes = new String[4];
	for (int i=0; i<4; i++) {
	    s_attributes[i] = Period04.projectGetNameSet(i);
	}	    
	cb_attribute = new JComboBox(s_attributes);
	cb_attribute.addItemListener(this);

	checkUseWeights = new JCheckBox("Use weights");
	if (Period04.projectGetPeriodUseWeight()!=0) {
	    checkUseWeights.setSelected(true);
	}

	// create list header
	JLabel heading;
	if (Period04.projectGetPeriodUseData()==0 /*Observed*/) {
	    heading = createLabel("Label                    Original   "+
				  "Sigma(Obs.)    Points Adjusted");
	} else {
	    heading = createLabel("Label                    Adjusted   "+
				 "Sigma(Adj.)    Points  Adjusted");
	}
	heading.setFont(new Font("Monospaced", Font.PLAIN, 11));

  // create list
  listmodel = new SortedListModel( );
  listbox = new JList( listmodel);
  listbox.setSelectionMode( ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
  listbox.setFont( new Font( "Monospaced", Font.PLAIN, 11));

  // add the selectionlistener
  MouseInputListener mil = new DragSelectionListener( );
  listbox.addMouseMotionListener( mil);
  listbox.addMouseListener( mil);

  // make the list scrollable
  JScrollPane listScrollPane = new JScrollPane
    ( listbox
    , JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED
    , JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED
    )
  ;

  listScrollPane.setBorder( BorderFactory.createEtchedBorder( EtchedBorder.RAISED));
  updateList( 0);

	// set position
	l_show         .setBounds( 10,  0, 150,  20);
	cb_attribute   .setBounds(160,  0, 290,  20);
	checkUseWeights.setBounds( 10, 30, 200,  20);
	heading        .setBounds( 5, 60, 480,  20);
	listScrollPane .setBounds( 5, 80, 480, 230);

	// add to the panel
	inputpanel.add(l_show);
	inputpanel.add(cb_attribute);
	inputpanel.add(checkUseWeights);
	inputpanel.add(heading);
	inputpanel.add(listScrollPane);
	
	//---
	//--- create the optionpane and add the inputpanel and the buttons
	//---
	optionPane = new JOptionPane((Object)inputpanel, 
				     JOptionPane.PLAIN_MESSAGE,
				     JOptionPane.YES_NO_OPTION,
				     null, options, options[0]);
	setContentPane(optionPane);
	setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent we) {
		    /*
		     * Instead of directly closing the window,
		     * we're going to change the JOptionPane's
		     * value property.
		     */
		    optionPane.setValue(
			new Integer(JOptionPane.CLOSED_OPTION));
		}
	    });
	
  optionPane.addPropertyChangeListener( new
    PropertyChangeListener ( )
    {
      public void propertyChange ( PropertyChangeEvent e)
      {
        String prop = e.getPropertyName( );

        if ( isVisible( ) 
          && (e.getSource( ) == optionPane)
          && (prop.equals( JOptionPane.VALUE_PROPERTY) || prop.equals( JOptionPane.INPUT_VALUE_PROPERTY))
           )
        {
          Object value = optionPane.getValue();
          
          if (value == JOptionPane.UNINITIALIZED_VALUE)
          {
            return; //ignore reset
          }

          //--- Reset the JOptionPane's value.
          //--- If you don't do this, then if the user
          //--- presses the same button next time, no
          //--- property change event will be fired.
          optionPane.setValue( JOptionPane.UNINITIALIZED_VALUE);

          if (value.equals( b_Save)) // save
            saveStatistics( );
          else if (value.equals( b_Print)) // print
            printStatistics( );
          else if (value.equals( b_OK))
          { // adjust data

            // get the settings
            boolean useweights = checkUseWeights.isSelected( );
            int attribute = cb_attribute.getSelectedIndex( );
            int datamode = Period04.projectGetPeriodUseData( );
            int [] selectedindices = listbox.getSelectedIndices( );

            int selected = selectedindices.length;

            // retrieve the appropriate ID
            for ( int i = 0; i < selected; i++)
            {
              int j = selectedindices[i];

              IdValue selectedItem = (IdValue) listbox.getModel( ).getElementAt( j);
/*
              System.err.println
                ( Integer.toString( j)
                + " item " + Integer.toString( selectedItem.attribute)
                + " " + Integer.toString( selectedItem.getID( ))
                + " " + selectedItem.toString( )
                + " " + Integer.toString( selectedItem.ID)
                )
              ;
*/
              // reuse of the selectedindices array for the call of projectAdjustData( )
              selectedindices[i] = selectedItem.ID;
            }

            if (0 == selected)
            {
              JOptionPane.showMessageDialog
                ( mainframe.getFrame()
                , "Nothing selected ...", "Error"
                , JOptionPane.ERROR_MESSAGE
                )
              ;

              return;
            } 

          // write to protocol
          StringBuffer text = new StringBuffer( );
          text.append( "Adjust data:\n");

          String s_weighted = "";
          if (useweights) { s_weighted = "weighted"; }

          String s_datamode = "adjusted";
          if (0 == datamode) /*Observed*/
          {
            s_datamode = "original";
          }

          text.append
            ( "Adjusted the selected data of attribute \""
            + Period04.projectGetNameSet( attribute) + "\"\n"
            + "with the " + s_weighted
            + " adjustments to the " + s_datamode + " data.\n"
            )
          ;

          // adjust data
          Period04.projectAdjustData( attribute, datamode, selected, selectedindices, useweights);

			    for (int i=0; i<selected; i++) {
				double shift;
				
				if (datamode==0 /*Observed*/) { 
				    if (useweights) {
					shift=Period04.projectGetIDNameAverageOrigWeight(
					    attribute,selectedindices[i]);
				    } else {
					shift=Period04.projectGetIDNameAverageOrig(
					    attribute,selectedindices[i]);
				    }
				} else { // adjusted war orig !!!
				    if (useweights) {
					shift=Period04.projectGetIDNameAverageAdjWeight(
					    attribute,selectedindices[i]);
				    } else {
					shift=Period04.projectGetIDNameAverageAdj(
					    attribute,selectedindices[i]);
				    }
				}
				text.append(
				    "Adjusted the "+
				    Period04.projectGetIDNamePoints(
					attribute, selectedindices[i])+
				    " points of "+
				    Period04.projectGetIDNameStr(
					selectedindices[i], attribute)+
				    " by "+format(shift)+"\n");
			    }
			    
			    if (datamode==0 /*Observed*/) { 
				JOptionPane.showMessageDialog(mainframe.getFrame(),
				    "Please calculate a new frequency fit "+
				    "using the adjusted values.");
			    } else {
				JOptionPane.showMessageDialog(mainframe.getFrame(),
				    "Please calculate a new frequency fit.");
			    }
			    mainframe.getLog().writeProtocol(text.toString(), true);
			    // calculate zeropoints
			    Period04.projectCalculateAverage();
			    // update display
			    updateList(attribute);
			    // update timestring plot (if active)
			    tab.updateTSPlot();
			} else {
			    setVisible(false);
			}
		    }



		}
	    });
	
	this.setTitle("Adjust Data:");
	this.setSize(520,400);
	this.setResizable(false);
	this.setLocationRelativeTo(mainframe.getFrame());
	this.setVisible(true);
    }

    public void actionPerformed(ActionEvent evt) {
	optionPane.setValue(evt.getSource());
    }

    public void itemStateChanged(ItemEvent evt) {
	if (itemselected==true) { // a real event
	    Object s=evt.getSource();
	    if (s==cb_attribute) {
	        updateList(cb_attribute.getSelectedIndex());
	    }
	    itemselected = false;
 	} else { // just a secondary event - ignore
	    itemselected = true;
	}
    }


  public void updateList ( int attribute)
  {
    listmodel.clear( );
    int entries = Period04.projectNumberOfNames( attribute);
    boolean useweights = checkUseWeights.isSelected( );

    for ( int i = 0; i < entries; ++i)
    {
      String tmp = Period04.projectGetNameStatistics( attribute, i, useweights);

      if (! tmp.equals( ""))
      { // only add string if it has content
        IdValue idValue = new IdValue
          ( i
//          , Period04.projectGetIndexNameStr( attribute, i) // short
          , Period04.projectGetNameStatistics( attribute, i, useweights) // long
          )
        ;

        // We store temporarily all the other stuff here too.
        // Till I found a way to get rid of it.
        idValue.attribute = attribute;
        idValue.ID = Period04.projectGetIndexNameID( attribute, i);

        listmodel.addElement( idValue);
      }
    }
  }


    public void saveStatistics() {
	// create FileChooser
	JFileChooser fc = new JFileChooser(Period04.getCurrentDirectory());
	int returnVal = fc.showSaveDialog(mainframe.getFrame());
	if (returnVal == JFileChooser.APPROVE_OPTION) {
	    File file = fc.getSelectedFile();
	    // check if the extension is missing
	    String name = file.getName();
	    if (name.indexOf(".")==-1) { 
		file = new File(file.getPath()+".txt");
	    }
	    // check wether user wants to overwrite an existing file
	    if (file.exists()) {
		int v = JOptionPane.showConfirmDialog(
		    mainframe.getFrame(), "File does already exist!\n"+
		    "Do you really want to delete the old file?", 
		    "Warning", JOptionPane.YES_NO_OPTION);
		if (v == JOptionPane.NO_OPTION) {
		    return;
		} 
	    }
	    // now go and save it
	    Period04.projectSaveStat(cb_attribute.getSelectedIndex(),
				     checkUseWeights.isSelected(), 
				     file.getPath());
	}
    }

    public void printStatistics ( )
    {
      StringBuffer text = new StringBuffer( );

      text.append( "Name                      Average        Sigma    Points  Adj\n");
      text.append( "-------------------------------------------------------------\n");

      for ( int i = 0; i < listmodel.size( ); i++)
      {
        text.append( listmodel.get( i).toString( ) + "\n");
      }

      printResult( text.toString( ));
    }

}
