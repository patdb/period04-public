/*----------------------------------------------------------------------*
 *  XDialogDeleteDefaultLabel
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.*;                  // Property change stuff
import javax.swing.*;
import javax.swing.event.*;

class XDialogDeleteDefaultLabel extends XDialog 
    implements ActionListener
{
    private static final long serialVersionUID = 8317972133899081113L;
    private JOptionPane     optionPane;
    private final JButton   b_OK     = createButton("Ok");
    private final JButton   b_Cancel = createButton("Cancel");
    
    private JComboBox  cb_attribute;   
    private JTextField t_label;

    private XProjectFrame mainframe;
    private XTabTimeString tab;

    public XDialogDeleteDefaultLabel(XProjectFrame frame, XTabTimeString tabTS) {
	super(frame.getFrame(), true); // necessary to keep dialog in front
	mainframe = frame;
	tab = tabTS;
	Object[] options = {b_OK, b_Cancel}; //set buttons 
	
	JPanel inputpanel = new JPanel();
	inputpanel.setLayout(null);

	// get values
	int    att  = Period04.projectGetDeletePointAttribute();
	String name = Period04.projectGetDeletePointName();
	
	JLabel l_attribute = createLabel("Put deleted points in column:");
	String [] s_attributes = new String[4];
	for (int i=0; i<4; i++) {
	    s_attributes[i] = Period04.projectGetNameSet(i);
	}	    
	cb_attribute = new JComboBox(s_attributes);
	cb_attribute.setSelectedIndex(att);

	JLabel l_label = createLabel("Label to use for deleted points:");
	t_label = new JTextField(name);

	l_label     .setBounds( 10,  0, 210, 20);
	t_label     .setBounds( 220, 0, 150, 20);
	l_attribute .setBounds( 10, 30, 210, 20);
	cb_attribute.setBounds( 220,30, 150, 20);

	inputpanel.add(l_attribute);
	inputpanel.add(cb_attribute);
	inputpanel.add(l_label);
	inputpanel.add(t_label);

	optionPane = new JOptionPane((Object)inputpanel, 
				     JOptionPane.PLAIN_MESSAGE,
				     JOptionPane.YES_NO_OPTION,
				     null, options, options[0]);
	setContentPane(optionPane);
	setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent we) {
		    /*
		     * Instead of directly closing the window,
		     * we're going to change the JOptionPane's
		     * value property.
		     */
		    optionPane.setValue(
			new Integer(JOptionPane.CLOSED_OPTION));
		}
	    });
	
	optionPane.addPropertyChangeListener(new PropertyChangeListener() {
		public void propertyChange(PropertyChangeEvent e) {
		    String prop = e.getPropertyName();
		    
		    if (isVisible() 
			&& (e.getSource() == optionPane)
			&& (prop.equals(JOptionPane.VALUE_PROPERTY) ||
			    prop.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
			Object value = optionPane.getValue();
			
			if (value == JOptionPane.UNINITIALIZED_VALUE) {
			    return; //ignore reset
			}
			
			// Reset the JOptionPane's value.
			// If you don't do this, then if the user
			// presses the same button next time, no
			// property change event will be fired.
			optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

			if (value.equals(b_OK)) {
			    // get settings
			    int attribute = cb_attribute.getSelectedIndex();
			    String s_label = t_label.getText();
			    if (s_label.equals("")) { // is it a valid string?
				t_label.selectAll();
				JOptionPane.showMessageDialog(
				    XDialogDeleteDefaultLabel.this, 
				    "Sorry, \"" +s_label+"\" "+
				    "isn't a valid name.\n", "Try again",
				    JOptionPane.ERROR_MESSAGE);
				return;
			    }
			    Period04.projectSetDeletePointInfo(attribute,s_label);
			    tab.updateDisplay();
			}
			setVisible(false);

		    }
		}
	    });
	
	this.setTitle("Change settings for deleted points");
	this.setSize(410,140);
	this.setResizable(false);
	this.setLocationRelativeTo(mainframe.getFrame());
	this.setVisible(true);
    }

    public void actionPerformed(ActionEvent evt) {
	optionPane.setValue(evt.getSource());
    }
}

