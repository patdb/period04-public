/*---------------------------------------------------------------------*
 *  XPlot
 *  a superclass for plot frames
 *---------------------------------------------------------------------*/

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.RepaintManager;
import javax.imageio.ImageIO;

import gov.noaa.pmel.sgt.AbstractPane;
import gov.noaa.pmel.sgt.Axis; 
import gov.noaa.pmel.sgt.CartesianGraph;
import gov.noaa.pmel.sgt.PlotMark;// using modified PlotMark class
import gov.noaa.pmel.sgt.swing.XPlotLayout;
import gov.noaa.pmel.util.Range2D;
import gov.noaa.pmel.util.Point2D;
import gov.noaa.pmel.util.Dimension2D;

import java.awt.PrintJob;
import java.awt.Toolkit;
import java.awt.print.PrinterJob;
import java.awt.print.Printable;
import java.awt.print.PageFormat;
import java.awt.Graphics2D;
import java.awt.print.PrinterException;
import java.beans.PropertyVetoException;

import java.io.ByteArrayOutputStream;     
import java.io.IOException;       
import java.io.FileOutputStream;       
import java.io.FileWriter;

import java.awt.image.BufferedImage;         
import java.awt.Dimension;
import org.jibble.epsgraphics.*;

public class XPlot 
	extends JFrame 
	implements ComponentListener
{
    private static final long serialVersionUID = 1897730509449032547L;
    protected XProjectFrame  mainframe;
    protected XPlotLayout layout;
    protected JPanel      mainpanel;
    protected JLabel      statusBar;
    protected String      stdtext = 
    " Please move your mouse to display coordinates";
    protected String      maxZoomText = " Maximum zoom reached!";
    protected boolean     isWithinDomain = false;
	protected Dimension   oldFrameSize;

    public final static int EPS = 0;
    public final static int JPG = 1;
    private   JFileChooser fc;


	public XPlot() {
		this.addComponentListener(this);
	}

    protected void exportImage(int format) {
	fc = new JFileChooser(Period04.getCurrentDirectory());
	int val = fc.showSaveDialog(this);
	if (val == JFileChooser.APPROVE_OPTION) {
	    File file = fc.getSelectedFile();	    // get the file name
	    Period04.setCurrentDirectory(file);
	    //--- check if the extension is missing
	    String path = file.getPath();
	    switch (format) {  // add extension if neccessary
		case EPS:
		    if (!path.endsWith(".eps")) { path += ".eps"; }
		    break;
		case JPG:
		    if (!path.endsWith(".jpg")) { path += ".jpg"; }
		    break;
	    }
	  
	    // check wether user wants to overwrite an existing file
	    if ((new File(path)).exists()) {
		int v = JOptionPane.showConfirmDialog(
		    this, "File does already exist!\n"+
		    "Do you really want to delete the old file?", 
		    "Warning", JOptionPane.YES_NO_OPTION);
		if (v == JOptionPane.NO_OPTION) {
		    return;
		}
	    }

	    // take the chosen fileformat
	    switch (format) {
		case EPS:	// saves the plot as an EPS image
		{
		    try {
			Dimension oldsize = layout.getSize();
			int width  = (int)layout.getSize().getWidth();//1500;
			int height = (int)layout.getSize().getHeight();//1050;

			EpsGraphics2D g = new EpsGraphics2D(
			    "test",new File(path),0,0,width,height);
			layout.setSize(new Dimension(width,height));
			layout.validate();
			layout.draw(g,width,height);
			g.flush();
			g.close();
			
			layout.setSize(oldsize);
			layout.validate();
		    } catch(IOException e) {
			System.err.println(e);
		    }
		    break;
		}
		case JPG:	// saves the plot as a JPG image
		{ 
		    try 
			{
			    BufferedImage img = new BufferedImage( 
				layout.getWidth(), layout.getHeight(),
				BufferedImage.TYPE_INT_RGB );
			    Graphics g = img.getGraphics();
			    layout.draw(g,layout.getWidth(),layout.getHeight());
			    ImageIO.write(img,"jpeg",new File(path));
			} 
		    catch(IOException e) 
			{
			    System.err.println(e);
			}
		    break;
		}

	    }
	}
    }

    protected void print() {
	PlotMark.setIsPrint(true);
	Color saveColor;
	PrinterJob printJob = PrinterJob.getPrinterJob();
	printJob.setPrintable(layout);
	printJob.setJobName("Period04 - Plot");
	if(printJob.printDialog()) {
	    try {
		saveColor = layout.getBackground();
		if(!saveColor.equals(Color.white)) {
		    layout.setBackground(Color.white);
		}
		layout.setPageAlign(AbstractPane.TOP,
				    AbstractPane.CENTER);
		RepaintManager currentManager = 
		    RepaintManager.currentManager(layout);
		currentManager.setDoubleBufferingEnabled(false);
		printJob.print();
		currentManager.setDoubleBufferingEnabled(true);
		layout.setBackground(saveColor);
	    } catch (PrinterException e) {
		System.out.println("Error printing: " + e);
	    }
	}
	PlotMark.setIsPrint(false);
    }

    protected void resetZoom() {
	layout.resetZoom();
	layout.setClipping(false);
    }

    protected void setZoom(String xAxisTitle, String yAxisTitle) {
	new XPlotViewport(this, layout, xAxisTitle, yAxisTitle);
    }

    //---
    //--- format numbers and return them as string
    //---
    protected String format(double value) {
	return format(value, 6);
    }
    protected String format(double value, int digits) {
	NumberFormat fmt = NumberFormat.getInstance(Locale.UK);
	if (fmt instanceof DecimalFormat) {
	    ((DecimalFormat) fmt).setDecimalSeparatorAlwaysShown(true);
	}
	fmt.setGroupingUsed(false);
	fmt.setMaximumFractionDigits(digits);
	return String.valueOf(fmt.format(value));
    }

    //---
    //--- transform device coordinates [pixels] into user coordinates
    //---
    protected Point2D.Double transformDtoU(int xD, int yD) {
	Axis a;
	double xP = 0.0;
	double yP = 0.0;
	CartesianGraph g = (CartesianGraph) layout.getFirstLayer().getGraph();
	try {
	    a = g.getXAxis("Bottom Axis");    
	    Range2D xr = a.getRangeP();   // get physical range of x-axis
	    a = g.getYAxis("Left Axis");
	    Range2D yr = a.getRangeP();   // get physical range of y-axis
	    
	    double x0P = xr.start;        // physical coordinates 
	    double x1P = xr.end;          // of the domain
	    double y0P = yr.start;
	    double y1P = yr.end;

	    // device coordinates of the domain
	    double x0D = g.getXUtoD(g.getXPtoU( x0P )); 
	    double x1D = g.getXUtoD(g.getXPtoU( x1P ));
	    double y0D = g.getYUtoD(g.getYPtoU( y0P ));
	    double y1D = g.getYUtoD(g.getYPtoU( y1P ));

	    // check wether point is in domain
	    isWithinDomain=true;
	    if (xD<x0D || xD>x1D || yD<y1D || yD>y0D) {
		isWithinDomain=false;
	    }

	    // convert device coordinates into physical
	    xP = x0P+(xD-x0D)*(x1P-x0P)/(x1D-x0D); 
	    yP = y0P+(yD-y0D)*(y1P-y0P)/(y1D-y0D); 
	} catch (Exception e) {
	    System.out.println(e);
	}
	return new Point2D.Double(g.getXPtoU(xP),g.getYPtoU(yP));
    }

    //---
    //--- return the x range of the plot in device coordinates
    //---
    protected Range2D getDomainXRangeD() {
	Axis a;
	CartesianGraph g = (CartesianGraph) layout.getFirstLayer().getGraph();
	try {
	    a = g.getXAxis("Bottom Axis");    
	    Range2D xr = a.getRangeP();   // get physical range of x-axis
	    double x0P = xr.start;        // physical coordinates 
	    double x1P = xr.end;          // of the domain

	    // device coordinates of the domain
	    double x0D = g.getXUtoD(g.getXPtoU( x0P )); 
	    double x1D = g.getXUtoD(g.getXPtoU( x1P ));

	    return new Range2D(x0D, x1D);
	} catch (Exception e) {
	    return null;
	}
    }

    //---
    //--- is it within the graph ranges?
    //---
    protected boolean isWithinDomain() {
	return isWithinDomain;
    }

    //---
    //--- MyMouse
    //--- a MouseMotionAdapter, that retrieves the user coordinates and
    //--- writes them to the statusbar.
    //---
    class MyMouse extends MouseMotionAdapter {
	public void mouseMoved(MouseEvent evt) {
	    //--- if we reached maximum zoom show a message!
	    if (layout.isMaximumZoom()) { 
		statusBar.setText(maxZoomText);
		return;
	    }

	    int xD = evt.getX();      // get position of the point where the 
	    int yD = evt.getY();      // MouseEvent has been detected
	    Point2D.Double ptU=transformDtoU(xD,yD); //transform to user coord.
	    if (isWithinDomain()) {   // display coordinates or standard text
		statusBar.setText(
		    " (x,y) = (" + format(ptU.x) + ", " + format(ptU.y) + ")");
	    } else {
		statusBar.setText(stdtext);
	    }
	}
    }

	//---
	//--- listen to frame size changes and change plot size correspondingly
	//---
	public void componentResized(ComponentEvent e) {
		Dimension newFrameSize = this.getSize();
		if (oldFrameSize==null) {
			oldFrameSize = newFrameSize;
			return;
		}
		double hFactor = newFrameSize.getHeight()/oldFrameSize.getHeight();
		double wFactor = newFrameSize.getWidth()/oldFrameSize.getWidth();
		Dimension2D dim = layout.getLayerSizeP();
		layout.setLayerSizeP(new Dimension2D(dim.getWidth() *wFactor,
											 dim.getHeight()*hFactor));////
		layout.adjustLabels(wFactor,hFactor);
		oldFrameSize = newFrameSize;
	}
	public void componentMoved(ComponentEvent e)  { }
	public void componentShown(ComponentEvent e)  { }
	public void componentHidden(ComponentEvent e) { }

	// set initial size
	public void setSize(int w, int h) {
		super.setSize(w,h);
		layout.setLayerSizeP(new Dimension2D(10,7)); // set init values
		oldFrameSize = new Dimension(w,h);
	}
}
