/*----------------------------------------------------------------------*
 *  XDialogAmplitudeVariation
 *  a dialog to select the settings for a amplitude/phase variation
 *----------------------------------------------------------------------*/

import java.awt.*;
import java.awt.event.*;
import java.beans.*;               // property change stuff
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.help.CSH;
import javax.swing.*;
import javax.swing.border.*;       // TitledBorder stuff
import javax.swing.event.*;        // mouseinputlistener


class XDialogAmplitudeVariation extends XDialog 
    implements ActionListener, ComponentListener
{
    private static final long serialVersionUID = -6539131119261753252L;
    private XProjectFrame  mainframe;
    private XTabFit     fitTab;
    private JPanel      inputpanel;

    private final int WIDTH  = 500;      // default frame width
    private final int HEIGHT = 590;      // default frame height
    private int sizeDiff = 0;     // correction factor for height

    private Timer        timer;
    private FitTask      task;

    private final JList      listBox;
    private DefaultListModel listBoxModel;
    private JComboBox        cb_attribute, cb_variation;
    private JRadioButton     b_ImproveAll, b_ImproveSpecial;
    private String           protocol;
    private JButton          b_Calc, b_Close, b_Print, b_Save, b_Help;
    private JTextArea        textbox; 
    private JPanel           resultpanel;
    private JScrollPane      resultScrollPane; 
    
    private int [] listindex;
    private int datamode;

    public XDialogAmplitudeVariation(XProjectFrame mainframe, XTabFit fitTab, int datamode) {
	super(mainframe.getFrame(), false);
	this.mainframe = mainframe;
	this.fitTab    = fitTab;
	this.datamode  = datamode;

	inputpanel = new JPanel();
	inputpanel.setLayout(null);

	//--- 
	//--- create an opaque panel for the calculation settings
	//--- 
	JPanel calcpanel = new JPanel();
	calcpanel.setOpaque(false);
	calcpanel.setBorder(
	    BorderFactory.createCompoundBorder(
		BorderFactory.createTitledBorder(
		    BorderFactory.createEtchedBorder(), 
		    " Calculation settings: ",
		    TitledBorder.LEADING, TitledBorder.TOP, 
		    new Font("Dialog", Font.BOLD, 10), new Color(20,30,150) ),
		BorderFactory.createEmptyBorder(5,5,5,5)));
	//---
	//--- create the labels
	//---
	JLabel l_attribute = createLabel("Attribute to use:");
	JLabel l_free      = createLabel("Parameters to improve:");
	JLabel l_variation = createLabel("Type of variation:");
	JLabel l_frequency = createLabel(
	    "<html>Select the frequencies for which to calculate"+
	    " the selected type of variation:</html>");
	JButton b_Help     = createButton("Show help");
	b_Help.addActionListener(
	    new CSH.DisplayHelpFromSource(XProjectFrame.getHelpBroker()));
	CSH.setHelpIDString(b_Help,"gui.improveampvardialog"); // set link

	//---
	//--- create the comboboxes ...
	//---
	String [] s_attributes = new String[4];
	for (int i=0; i<4; i++) { 
	    s_attributes[i] = Period04.projectGetNameSet(i);
	}	
	String [] s_variation = { "amplitude variations", "phase variations",
				  "amplitude and phase variations"};
	cb_attribute = createComboBox(s_attributes);
	cb_variation = createComboBox(s_variation);
	cb_attribute.setFont(new Font("Dialog", Font.PLAIN, 11));
	cb_variation.setFont(new Font("Dialog", Font.PLAIN, 11));

	b_ImproveAll     = createRadioButton("all ampl. & phases");
	b_ImproveSpecial = createRadioButton("special");
	ButtonGroup bg = new ButtonGroup();
	bg.add(b_ImproveAll);
	bg.add(b_ImproveSpecial);
	b_ImproveAll.setSelected(true);
	//---
	//--- create the list of frequencies
	//---
	listBoxModel = new DefaultListModel();
	listBox = new JList(listBoxModel);
	listBox.setSelectionMode(
	    ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	listBox.setFont(new Font("Dialog", Font.PLAIN, 11));

	listindex = new int[Period04.projectGetTotalFrequencies()];
	int count = 0;
	for (int i=0; i<Period04.projectGetTotalFrequencies(); i++) {
	    listindex[count] = 0;
	    if (Period04.projectGetActive(i)) {
		String freqnumber = Period04.projectGetNumber(i);
		if (!Period04.projectIsComposition(i) || 
			Period04.projectGetCompoUseFreqValue(i)) {
		    listBoxModel.addElement(
			freqnumber.toString()+": frequency="+
			Period04.projectGetFrequency(i));
		    listindex[count] = i;
		    count++;
		}
	    }
	}

	// add the selectionlistener
	MouseInputListener mil = new DragSelectionListener();
	listBox.addMouseMotionListener(mil);
	listBox.addMouseListener(mil);

	// make the list scrollable
	final JScrollPane listScrollPane = new JScrollPane(
	    listBox, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, 
	    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	listScrollPane.setBorder(
	    BorderFactory.createEtchedBorder(EtchedBorder.RAISED));

	//--- 
	//--- create an opaque panel for the results
	//--- 
	resultpanel = new JPanel();
	resultpanel.setOpaque(false);
	resultpanel.setBorder(
	    BorderFactory.createCompoundBorder(
		BorderFactory.createTitledBorder(
		    BorderFactory.createEtchedBorder(), 
		    " Results: ",
		    TitledBorder.LEADING, TitledBorder.TOP, 
		    new Font("Dialog", Font.BOLD, 10), new Color(20,30,150) ),
		BorderFactory.createEmptyBorder(5,5,5,5)));
	textbox = new JTextArea(protocol);
	textbox.setFont(new Font("Monospaced", Font.PLAIN, 11));
	textbox.setEditable(false);
	// make the text box scrollable
	resultScrollPane = new JScrollPane(
	    textbox, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
	    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	resultScrollPane.setBorder(
	    BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
	resultpanel.setLayout(null);
	resultScrollPane.setBounds(  8, 10, 454, 210 );
	resultpanel.add(resultScrollPane);
	//---
	//--- create buttons
	//---
	b_Calc  = createButton("Calculate");
	b_Save  = createButton("Save");
	b_Print = createButton("Print");
	b_Close = createButton("Close");
	//---
	//--- define positions
	//---
	calcpanel       .setBounds(  10,  10, 470, 250 );
	l_attribute     .setBounds(  30,  30, 180,  20 );
	cb_attribute    .setBounds( 230,  30, 230,  20 );
	l_variation     .setBounds(  30,  60, 180,  20 );
	cb_variation    .setBounds( 230,  60, 230,  20 );
	l_frequency     .setBounds(  30,  90, 180,  40 );
	listScrollPane  .setBounds( 230,  90, 230, 130 );
	b_Help          .setBounds(  30, 175, 100,  30 );
	l_free          .setBounds(  30, 230, 180,  20 );
	b_ImproveAll    .setBounds( 230, 230, 140,  20 );
	b_ImproveSpecial.setBounds( 370, 230, 100,  20 );
	resultpanel     .setBounds(  10, 270, 470, 240 );
	b_Calc          .setBounds(  50, 510,  90,  20 );
	b_Print         .setBounds( 150, 510,  90,  20 );
	b_Save          .setBounds( 250, 510,  90,  20 );
	b_Close         .setBounds( 350, 510,  90,  20 );
	//---
	//--- add to panel
	//---
	inputpanel.add(calcpanel, null);
	inputpanel.add(l_attribute, null);
	inputpanel.add(cb_attribute, null);
	inputpanel.add(l_frequency, null);
	inputpanel.add(l_free, null);
	inputpanel.add(b_ImproveAll, null);
	inputpanel.add(b_ImproveSpecial, null);
	inputpanel.add(b_Help, null);
	inputpanel.add(l_variation, null);
	inputpanel.add(cb_variation, null);
	inputpanel.add(listScrollPane, null);
	inputpanel.add(resultpanel, null);
	inputpanel.add(resultScrollPane, null);
	inputpanel.add(b_Calc, null);
	inputpanel.add(b_Print, null);
	inputpanel.add(b_Save, null);
	inputpanel.add(b_Close, null);
	this.getContentPane().add(inputpanel);

	//--- listen for frame size changes
	addComponentListener(this); 
	//--- if the frame is closed, make sure to stop the running calculation
	addWindowListener( new WindowAdapter() {
		public void windowClosing(WindowEvent we) {
		    //--- stop running calculations
		    if (task!=null) 
			XDialogAmplitudeVariation.this.mainframe.stopCalculation();
		    //--- commit suicide ;-)
		    XDialogAmplitudeVariation.this.fitTab.dereferenceAmpVarDialog(); 
		}
	    });	
	this.setTitle("Calculate Amplitude/Phase Variations:");
	this.setSize(WIDTH,HEIGHT);
	this.setResizable(true);
	Point pnt = mainframe.getFrame().getLocation();
	this.setLocation((int)pnt.getX()+50, (int)pnt.getY()+140);
	this.setVisible(true);
    }

    private void calculate() { 
	textbox.setText("Calculating ...");

	// update in relevant data
	fitTab.setSelectionMode(fitTab.AMP_VAR);    // set the selection mode
	int mode = fitTab.getCalcMode();
	fitTab.setFrequencyListData();	      // update data
	if (!fitTab.isCalculationPossible(true)) { //are there enough points/freqs?
	    return;
	} 
	fitTab.cleanErrorList();             // clean the errors

	int attribute = cb_attribute.getSelectedIndex();
	int varmode   = cb_variation.getSelectedIndex();
	int [] selectedindices = listBox.getSelectedIndices();
	//--- translate indices
	for (int i=0; i<selectedindices.length; i++) {
	    selectedindices[i]=listindex[selectedindices[i]];
	}
	if (b_ImproveSpecial.isSelected()) {
	    //--- the selection has already been defined,
	    //--- translate variation mode index
	    switch (varmode) {
		case 0:  varmode=2; /*2=AmpVar*/   break;
		case 1:  varmode=3; /*3=PhaseVar*/ break;
		case 2:  varmode=1; /*1=AllVar*/   break;
		default: varmode=0; /*0=NoVar*/    break;
	    }	    
	} else {
	    //--- set everything unselected
	    for (int i=0; i<Period04.projectGetTotalFrequencies(); i++) {
		if (Period04.projectGetActive(i)) {
		    Period04.periodUnsetSelection(i, 0 /*Fre*/);
		    Period04.periodUnsetSelection(i, 1 /*Amp*/);
		    Period04.periodUnsetSelection(i, 2 /*Pha*/);
		}
	    }
	    //--- set default selection too
	    for (int i=0; i<Period04.projectGetTotalFrequencies(); i++) {
		if (Period04.projectGetActive(i)) {
		    Period04.periodSetSelection(i,1/*Amp*/);
		    Period04.periodSetSelection(i,2/*Pha*/);
		}
	    }
	    //--- get the variation index and convert into
	    //--- appropriate indices as required by the c++ code
	    switch (varmode) {
		case 0: varmode=2; /*2=AmpVar*/   break;
		case 1: varmode=3; /*3=PhaseVar*/ break;
		case 2: varmode=1; /*1=AllVar*/   break;
		default: varmode=0; /*0=NoVar*/   break;
	    }
	}
	//---
	//--- save settings for later monte carlo calculation
	//---
	fitTab.store_varmode         = varmode;
	fitTab.store_attribute       = attribute;
	fitTab.store_selectedindices = selectedindices;
	//---
	//--- initialize the calculation task
	//---
	task = new FitTask(mainframe, fitTab, datamode, /*AMP_VAR=*/3,
			   varmode, attribute, selectedindices);
	task.go();                  // start calculation
	mainframe.startTimer(task); // start the timer for the progressbar

    }

    public void updateDisplay(String protocol) {
	textbox.setText(protocol);	// show the results
	this.protocol = protocol;       // save for saving & printing
    }

    //---
    //--- initializeCalcPanel(...)
    //--- creates the panel for the calculation tab
    //---
    private JPanel getCalcSettingsPanel() {
	return fitTab.getCalcSettingsPanel(false);
    }

    public void actionPerformed(ActionEvent evt) {
	if (mainframe.isWorking()) { return; }

	Object s = evt.getSource();
	if (s==b_ImproveSpecial) {
	    XDialogImproveSpecial dialog = 
		new XDialogImproveSpecial(mainframe.getFrame(), 0, "Ok");
	    if (dialog.isCanceled()) { 
		b_ImproveAll.setSelected(true);
	    }
	}
	if (s==b_Calc) {
	    mainframe.setCalculating(true, getCalcSettingsPanel(),
				     "Calculating Least-Squares Fit ...", 
				     /*interval=*/300, false);
	    calculate();
	} else if (s==b_Save) { 
	    // create FileChooser
	    JFileChooser fc = new JFileChooser(Period04.getCurrentDirectory());
	    int returnVal = fc.showSaveDialog(mainframe.getFrame());
	    if (returnVal == JFileChooser.APPROVE_OPTION) {
		File file = fc.getSelectedFile();
		//--- add an extension if not given
		String name = file.getName();
		if (name.indexOf(".")==-1) { 
		    file = new File(file.getPath()+".txt");
		}
		// check wether user wants to overwrite an existing file
		if (file.exists()) {
		    int v = JOptionPane.showConfirmDialog(
			mainframe.getFrame(), "File does already exist!\n"+
			"Do you really want to delete the old file?", 
			"Warning", JOptionPane.YES_NO_OPTION);
		    if (v == JOptionPane.NO_OPTION) {
			return;
		    } 
		}
		//--- now go and save it
		Period04.setCurrentDirectory(file);
		try {
		    FileWriter fw = new FileWriter(file);
		    fw.write(protocol);
		    if ( fw!=null ) fw.close();
		} catch (IOException e) {
		    System.err.println(e);
		}
	    }
	    return; 
	} else if (s==b_Print) {
	    if (protocol!=null) {
		printResult(protocol);
	    }
	    return;
	} else if (s==b_Close) { 
	    if (task!=null) { mainframe.stopCalculation(); }
	    dispose();
	    fitTab.dereferenceAmpVarDialog(); // commit suicide ;-)
	}
    }

    //---
    //--- methods to listen to ComponentEvents 
    //--- (we are listening to ComponentEvents that are caused by resizing 
    //---  the mainframe)
    //---
    public void componentResized(ComponentEvent e) {
	//--- the frame has been resized. 
	//--- set the framewidth back to the default
	//--- if the frameheight is too low set it back to the default value
	//--- if the frameheight is ok, then determine the difference to the 
	//--- default value and call fitSize()
	//--- this will size the GUI elements to fit to the new frameheight
	Component c = e.getComponent();
	Dimension frameSize = c.getSize();
	if (frameSize.height <= HEIGHT) {
	    frameSize.height = HEIGHT;
	    sizeDiff = 0;
	} else {
	    sizeDiff = HEIGHT-frameSize.height;
    	}
	frameSize.width = WIDTH;
	setSize(frameSize);
	fitSize();
    }
    public void componentHidden(ComponentEvent e) {}
    public void componentShown(ComponentEvent e)  {}
    public void componentMoved(ComponentEvent e)  {}

    private void fitSize() {
	resultpanel     .setBounds(  10, 260, 470, 240-sizeDiff );
	resultScrollPane.setBounds(  18, 280, 454, 210-sizeDiff );
	b_Calc          .setBounds(  50, 510-sizeDiff,  90,  20 );
	b_Print         .setBounds( 150, 510-sizeDiff,  90,  20 );
	b_Save          .setBounds( 250, 510-sizeDiff,  90,  20 );
	b_Close         .setBounds( 350, 510-sizeDiff,  90,  20 );
    }

}
