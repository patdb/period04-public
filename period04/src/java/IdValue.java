/*---------------------------------------------------------------------*
 *  IdValue.java
 *  Class to hold selection list entries.
 *---------------------------------------------------------------------*/

import java.io.*;
import java.util.*;                 // sorting list


public class IdValue implements Comparable
{
  private int m_id;
  private String m_value;

  // We store temporarily all the other stuff here too.
  // Till I found a way to get rid of it.
  public int attribute;
  public int ID;

  public IdValue ( int id, String value)
  {
    m_id = id;
    m_value = value;
  }

  public String toString ( )
  {
    return m_value;
  }

  public int getID ( )
  {
    return m_id;
  }

  public int compareTo ( Object o)
  {
    return m_value.compareTo( ((IdValue) o).m_value);
  }
}

