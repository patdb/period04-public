/*-----------------------------------------------------------------------*
 *  FourierFileFilter
 *  defines file extensions used by Period04
 *-----------------------------------------------------------------------*/

import java.io.File;
import javax.swing.*;
import javax.swing.filechooser.*;

public class FourierFileFilter extends FileFilter 
{
  public boolean accept ( File f)
  {
    return f.isDirectory( )
        || f.getName( ).toLowerCase( ).endsWith( ".fou")
    ;
  }

  public String getDescription ( )
  {
    return "Fourier files(*.fou)";
  }
}


