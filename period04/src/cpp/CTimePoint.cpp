/*----------------------------------------------------------------------------
 *  CTimePoint.cpp
 *                contains member functions of the CTimePoint Class
 *---------------------------------------------------------------------------*/

#include "CTimePoint.h"

int CTimePoint::mNextIndex = 0;
bool CTimePoint::mUsePointError = false;

CTimePoint::CTimePoint
  ( int i
  )
  : mTime( 0.0)
  , mObserved( 0.0)
  , mCalculated( 0.0)
  , mWeight( 1.0)
{
  if (i)
  {
    mNextIndex = 0;
  }

  mRunningIndex = mNextIndex++;
  mAdjusted = 0;

  mNames[0] = 0;
  mNames[1] = 0;
  mNames[2] = 0;
  mNames[3] = 0;
}

CTimePoint::CTimePoint
  ( double t
  , double o
  , double a
  , double c
  , double pw
  , int n1
  , int n2
  , int n3
  , int n4
  , int index
  )
  : mTime( t)
  , mObserved( o)
  , mCalculated( c)
  , mWeight( 1.0)
  , mPointWeight( pw)
{
  mRunningIndex = mNextIndex++;

  if (index)
  {
    mRunningIndex = index;
  }
  
  if (a == NO_ADJUST)
  {
    a = o;
  }

  mAdjusted = a;
  
  mNames[0] = n1;
  mNames[1] = n2;
  mNames[2] = n3;
  mNames[3] = n4;
  
  if (mUsePointError)
    mPointWeight = 1.0 / (pw * pw);
}

CTimePoint::~CTimePoint ( )
{
  // nothing to do !!!
}

// compare operators
int operator< ( CTimePoint const & t1, CTimePoint const & t2)
{
  return (t1.getTime( ) < t2.getTime( ));
}

int operator<= ( CTimePoint const & t1, CTimePoint const & t2)
{
  return (t1.getTime( ) <= t2.getTime( ));
}
#if 0
// unused
// output operator
ostream & operator<< ( ostream & str, CTimePoint const & t)
{
  return str
    << t.getRunningIndex( )
    << "\t" << t.getTime( )
    << "\t" << t.getObserved( )
    << "\t" << t.getAdjusted( )
    << "\t" << t.getCalculated( )
    << "\t" << t.getDataResidual( )
    << "\t" << t.getAdjustedResidual( )
    << "\t" << t.getWeight( )
    << "\t" << (t.mUsePointError ? 1.0 / sqrt( t.getPointWeight( )) : t.getPointWeight( ))
    << "\t" << t.getIDName( 0)
    << "\t" << t.getIDName( 1)
    << "\t" << t.getIDName( 2)
    << "\t" << t.getIDName( 3)
  ;
}
#endif