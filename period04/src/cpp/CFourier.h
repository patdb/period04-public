/*---------------------------------------------------------------------------
 *  CFourier.h
 *  contains: CFourier, CFourierPoint
 *---------------------------------------------------------------------------*/

#ifndef __fourier_h__
#define __fourier_h__

#include <iostream>
#include <vector>

#include "CTimeString.h"
#include "CPeriPoint.h"
#include "message.h"

enum CompactMode { ALL_DATA, PEAKS_ONLY };
enum StepQuality { HIGH=20, MEDIUM=15, LOW=10, CUSTOM=0};

/*
 * Enumeration FT_Algorithm
 *
 * KURTZ1985 ... Kurtz' implementation of Deemings algorithm
 *               1985, MNRAS, 213, 773
 */
enum FTAlgorithm 
  { 
    KURTZ1985=0 
  };


/*
 * CFourierPoint
 */

class CFourierPoint
{
 private:
    
    double mFrequency;
    double mPower;

 public:

    // constructors
    CFourierPoint();
    CFourierPoint(double f, double p);
    
    // methods
    double getFrequency() const { return mFrequency; }
    double getAmplitude() const { return sqrt(mPower); }
    double getPower()     const { return mPower; }
    int    readIn(std::string pt);

};

/*
 * input and output
 */
istream &operator>>(istream &str,CFourierPoint &f1);
ostream &operator<<(ostream &str,CFourierPoint const &f1);

/*
 * comperator needed for sorted list...
 */
int operator<(CFourierPoint const &f1, CFourierPoint const &f2);
int operator<=(CFourierPoint const &f1, CFourierPoint const &f2);


/*
 * CFourier
 *
 * Container for a Fourier spectrum and its calculation settings
 */

class CFourier : public vector<CFourierPoint>
{
 
 private:

    int            mFourierID;         // ID of the fourier spectrum
    static int     mFourierIDCounter;  // counts number of fourier spectra
    CFourierPoint *mData;              // array to save the fourier spectrum
    int            mPoints;            // number of CFourierPoints in spectrum

    // parameters of the fourier spectrum:
    //
    std::string    mTitle;             // title
    FTAlgorithm    mAlgorithm;         // Fourier algorithm
    double         mFrom;              // lower boundary of frequency range
    double         mTo;                // upper boundary of frequency range
    double         mStepping;          // step rate
    StepQuality    mStepQuality;       // step quality
    DataMode       mMode;              // calculation mode
    CompactMode    mCompact;           // compact mode
    CPeriPoint     mPeak;              // the peak of the spectrum
    int            mUseWeight;         // use weights?
    std::string    mInfo;              // some information on the spectrum
    double         mSW2Frequency;      // frequency to be used for spec wind 2

 public:

    CFourier
      ( std::string const & title
      , FTAlgorithm algo = KURTZ1985
      , double from = 0
      , double to = 0
      , StepQuality qual = HIGH
      , double step = 0.1
      , DataMode Mode = Observed
      , CompactMode Compact = ALL_DATA /*or:PEAKS_ONLY*/
      , int weight = 0
      )
    ;
    
    ~CFourier ( );
    
    int getFourierID() { return mFourierID;}
    std::string getProperties ( );
    
    void setTitle ( std::string const & title);
    std::string const & getTitle ( ) const { return mTitle; }

    void setInfo ( std::string const & info);
    std::string const & getInfo ( ) const { return mInfo; }

    void   setAlgorithm(FTAlgorithm algo);
    FTAlgorithm getAlgorithm() const { return mAlgorithm; }
  
    void   setFrom(double from);
    double getFrom() const { return mFrom; }
  
    void   setTo(double from);
    double getTo() const { return mTo; }

    void   setStepping(StepQuality type=HIGH, double step=0.1) 
	{ mStepQuality=type; mStepping=step; }
    double getStepping() const { return mStepping; }
    StepQuality getStepQuality() const { return mStepQuality; }

    void     setMode(DataMode Mode) { mMode=Mode; }
    DataMode getMode() const { return mMode; }

    void        setCompact(CompactMode compact) { mCompact=compact; }
    CompactMode getCompact() const { return mCompact; }
    
    void setUseWeight(int flag) { mUseWeight=flag; }
    int  getUseWeight() const { return mUseWeight; }

    void setSW2Frequency(double fr) { mSW2Frequency=fr; }
    int  getSW2Frequency() const { return mSW2Frequency; }

    CPeriPoint const &getPeak() const { return mPeak; }
	
    CPeriPoint getSubPeak(double fr, double binscale) {
      // find closest peak in fourier spectrum
      double halfbinsize = binscale;
      if (halfbinsize<=0.0)
	halfbinsize = (mTo-mFrom)/1000.0; 
      double maxAmp=0.0;
      int maxID=-1;
      for (int i=0; i<points(); i++) {
	//look only for frequencies close enough
	if (fabs(mData[i].getFrequency()-fr)<halfbinsize) {
	  if (mData[i].getAmplitude()>maxAmp) {
	    maxID=i;
	    maxAmp = mData[i].getAmplitude();
	  }
	}
      }
      return CPeriPoint(mData[maxID].getFrequency(),
			mData[maxID].getAmplitude(),0);
    }

    int points() const {
	if (mData==NULL) {
	    return vector<CFourierPoint>::size();
	} else {
	    return mPoints;
	}
    }

    // gives a point-estimation
    int pointEstimate(double baseline);
    
    ostream &writeHeader(ostream & stream) const;
    istream &readHeader(istream & stream);
    ostream &writeData(ostream &stream) const;
    istream &readData(istream &istream);
    
    void deallocateData();
    
    // add overload
    void add(CFourierPoint const &point);
    
    CFourierPoint &operator[](int i) const {
	if (mData==NULL) {
	    static CFourierPoint unfound;
	    return unfound;
	} else {
	    return mData[i];
	}
    }
    
    void makeStatic();  
    
 protected:
    
    int setPeak(double const &Fre, double const& Amp, double const &Pha) {
	  if (Amp>mPeak.getAmplitude(0)) {
	    mPeak=CPeriPoint(Fre,Amp,Pha);
	    // returns !=0 if this is a Peak
	    return 1;
	  }
	  return 0;
    }

};  



#endif


