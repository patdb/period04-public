
#include "CPeriPoint.h"

#include <stdio.h>      //sprintf, sscanf
#include <ctype.h>      // isspace

#include <sstream>
using std::ostringstream;
using std::istringstream;

#include <iostream>
using namespace std;

#include <string.h>       

CSimplePeriPoint & CSimplePeriPoint::operator =(CSimplePeriPoint const & tmp) {
  // Do not copy mNumber
  mFrequency=tmp.mFrequency;
  mAmplitude=tmp.mAmplitude;
  mPhase=tmp.mPhase;
  mSelection=tmp.mSelection;
  mActive=tmp.mActive;
  return *this;
}

bool CSimplePeriPoint::empty() const {
    if (getActive())
	return false;
    if (getFrequency()!=0.0)
	return false;
    if (getAmplitude(-1)!=0.0)
	return false;
    return true;
}

double CSimplePeriPoint::calcEpoch(double t, int mode, int intensity) const {
    double tmp=phaseCalc(t,-1)/MY2PI;
    switch (mode) {
	case 0: // maximum light
	{
	    if (intensity) { tmp+= 0.25; }
	    else           { tmp-= 0.25; } 
	    break;
	}
	case 1: // zeropoint
	    break; // nothing to do
	case 2: // minimum light
	{
	    if (intensity) { tmp-= 0.25; }
	    else           { tmp+= 0.25; } 
	    break;
	}
    }
    double epoch=t-tmp/getFrequency();

    return epoch;
}

double CSimplePeriPoint::calcBMEpoch(
    double t, int mode, int intensity, CSimplePeriPoint bm) const 
{
    double tmp=phaseCalcBM(t,-1,bm)/MY2PI;
    switch (mode) {
	case 0: // maximum light
	{
	    if (intensity) { tmp+= 0.25; }
	    else           { tmp-= 0.25; } 
	    break;
	}
	case 1: // zeropoint
	    break; // nothing to do
	case 2: // minimum light
	{
	    if (intensity) { tmp-= 0.25; }
	    else           { tmp+= 0.25; } 
	    break;
	}
    }
    double epoch=t-tmp/getFrequency();
    ////
    double pts1 = bm.getAmplitude(-1)*sin(bm.phaseCalc(t,-1));
    double pts0 = bm.getAmplitude(-1)*sin(bm.phaseCalc(epoch,-1));
    epoch+=(pts1-pts0);
    ////
    return epoch;
}

void CSimplePeriPoint::adapt() {
    //--- is amplitude negative?
    if (mAmplitude<0) {
	//--- has the phase been fixed?
	if (isSelected(Pha)&&isSelected(Amp)) {
	    mAmplitude=-mAmplitude;
	    mPhase+=.5;
	}
    }
    //--- shift zeropoint...
    mPhase-=(long)mPhase; 
    if (mPhase<0) {
	mPhase+=1;
    }
}

void CSimplePeriPoint::fixNumbers() {
    //--- is amplitude extremely small?
    if (fabs(mAmplitude)<1.0e-30) {
      mAmplitude=0.0;
    }
} 

ostream& CSimplePeriPoint::writeOut(ostream &s, int selected) const {
    s<<"F"<<mNumber+1<<"\t";           // write number
    int oldprec=s.precision(15);       // set precision
    if (!getActive()) { s<<"("; }      // set inactive frequency in parentheses

    //--- frequency
    if (selected && isSelected(Fre))   {s<<'*'<<getFrequency()<<'*'; }
    else                               {s<<' '<<getFrequency()<<' '; }

    //--- amplitude
    s.precision(15);
    if (selected && isSelected(Amp))   {s<<"\t*"<<getAmplitude(0)<<'*'; }
    else                               {s<<"\t "<<getAmplitude(0)<<' '; }
    
    //--- phase
    if (selected && isSelected(Pha))   {s<<"\t*"<<getPhase(0)<<'*'; }
    else                               {s<<"\t "<<getPhase(0)<<' '; }

    //--- composition
    if (isComposition())               { s<<"\t"<<getCompositeString(); }
  
    if (!getActive()) { s<<")"; }
    s.precision(oldprec);
    return s;
}

std::string CSimplePeriPoint::writeOutError() const {
    ostringstream ostr;

    ostr << "F" << mNumber+1;         // frequency number
    ostr.precision(15); 
    ostr << " \t" << getFrequency() ;  // frequency
    ostr << " \t" << getAmplitude(0);  // amplitude
    ostr << " \t" << getPhase(0)    ;  // phase

    double tmp;
    ostr.precision(10); 
    tmp = getFrequencySigma();
    if (tmp!=0.0) { ostr << " \t" << tmp; }
    else          { ostr << " \tfixed"; }
    tmp = getAmplitudeSigma(0);
    if (tmp!=0.0) { ostr << " \t" << tmp; }
    else          { ostr << " \tfixed"; }
    tmp = getPhaseSigma(0);
    if (tmp!=0.0) { ostr << " \t" << tmp; }
    else          { ostr << " \tfixed"; }

    return ostr.str( );
}

int CSimplePeriPoint::readIn(char *ptr) {
    double tmpf=0,tmpa=0.006,tmpp=0;
    int active=1;
    // check if starting with a "("
    char c=0;
    int read=sscanf(ptr," %c",&c);
    // check and read in parameters
    if (c=='(') {
	active=0;
	read = sscanf(ptr," %c%lf%lf%lf",&c,&tmpf,&tmpa,&tmpp);
	read--;
	std::cout << "simple " << tmpf << std::endl;
    } else {
	active=1;
	read = sscanf(ptr,"%lf%lf%lf",&tmpf, &tmpa, &tmpp);
    }
  
    //--- frequency
    if (read<1) { setActive(0); return 1;}
    setFrequency(tmpf);
    
    //--- amplitude
    if (read<2) { return 0;}
    setAmplitude(tmpa,0);
    
    //--- phase
    if (read<3) {return 0;}
    setPhase(tmpp,0);
    
    // set active
    setActive(active);
    
    // exit function
    return 0;
}

////////////////////////////////////////////////////////////////
// CAmpVarPeriPoint
////////////////////////////////////////////////////////////////

void CAmpVarPeriPoint::setAmpVar(CalcMode i)
{
  mIsAmpVar=i;
  delete [] mAmpVar;
  delete [] mPhaseVar;
  mAmpVar=0;
  mPhaseVar=0;
  mMaxID=0;
}

void CAmpVarPeriPoint::createAmplitudeVariationData(int MaxID)
{
  delete [] mAmpVar;
  delete [] mAmpSigmaVar;
  delete [] mPhaseVar;
  delete [] mPhaseSigmaVar;
  mAmpVar=0;
  mAmpSigmaVar=0;
  mPhaseVar=0;
  mPhaseSigmaVar=0;
  mMaxID=0;

  if (mIsAmpVar!=0)
    {
      mMaxID=MaxID+1;
      // this is some code to check for out of bounds...
      mAmpVar= new double [mMaxID];
      mAmpSigmaVar= new double [mMaxID];
      mPhaseVar= new double [mMaxID];
      mPhaseSigmaVar= new double [mMaxID];
      // fill up the arrays
      for (int i=0;i<mMaxID;i++)
	{
	  mAmpVar[i]=mAmplitude;
	  mAmpSigmaVar[i]=0;
	  mPhaseVar[i]=mPhase;
	  mPhaseSigmaVar[i]=0;
	}
    }
}

void CAmpVarPeriPoint::adapt() {
  //---
  //--- first adapt main data
  //---
  //--- is amplitude negative?
  if (mAmplitude<0) {
	//--- has the phase been fixed?
	if (isSelected(Pha)&&isSelected(Amp)) {
	  mAmplitude=-mAmplitude;
	  mPhase+=.5;
	}
  }
  //--- shift zeropoint...
  mPhase-=(long)mPhase; 
  if (mPhase<0) {
	mPhase+=1;
  }
  
  //---
  //--- now also adapt ampvar data
  //---
  for (int i=0;i<mMaxID;i++)	{
	//--- is amplitude negative?
	if (mAmpVar[i]<0) {
	  //--- has the phase been fixed?
	  if (isSelected(Pha)&&isSelected(Amp)) {
		mAmpVar[i]=-mAmpVar[i];
		mPhaseVar[i]+=.5;
	  }
	}
	//--- shift zeropoint...
	mPhaseVar[i]-=(long)mPhaseVar[i]; 
	if (mPhaseVar[i]<0) {
	  mPhaseVar[i]+=1;
	}
  }
}

void CAmpVarPeriPoint::fixNumbers() {
    //--- is amplitude extremely small?
    if (fabs(mAmplitude)<1.0e-30) {
      mAmplitude=0.0;
    }
} 


CAmpVarPeriPoint & CAmpVarPeriPoint::operator =(CAmpVarPeriPoint const & tmp)
{
  setAmpVar(NoVar);
  CSimplePeriPoint::operator=(tmp);
  return *this;
}

////////////////////////////////////////////////////////////////
// CFreqCompo
////////////////////////////////////////////////////////////////

std::string CFreqCompo::getCompositionString ( ) const
{
  if (-1 == mFreq)
  {
    return "";
  }

  // OK return the composition
  std::ostringstream s;

  if (factor_type_const( 1) == mFactor)
  {
    s << "f" << (mFreq + 1);             // no sign and factor needed.
  }
  else if (factor_type( -1) == mFactor)
  {
    s << "-f" << (mFreq + 1);            // just the minus sign needed.
  }
  else if (factor_type( 0) == mFactor)
  {
    // do nothing.
  }
  else
  {
    s << mFactor << "f" << (mFreq + 1);            // everything needed.
  }

  return s.str( );
}

void CFreqCompo::setPointer(int tmpfreq, CFreqCompPeriPoint * ptr)
{
  if (mFreq!=tmpfreq) {
	MYERROR("There is something wrong with setting a dependency pointer!!!");
  }
  pnt=ptr;
  // maybe we should register
}

//--- ------------------------------------------------------------- ---//
//---                      CFreqCompPeriPoint                       ---//
//--- ------------------------------------------------------------- ---//

void CFreqCompPeriPoint::setCompositeString
  ( char const * f
  )
{
  cleanComposition( );

  size_t const f_len = strlen( f);

  // strip spaces
  char tmp [ 256];
  size_t j = 0;

  for( size_t i = 0; i < f_len; ++i)
  {
    if (! isspace(f[i]))
    {
      tmp[j] = f[i];
      ++j;
    }
  }

  tmp[j] = 0;

  // is this a composition?
  if (tmp[0]=='=') {	// yes it seems to be..., is next character a numeric?
	unsigned int pos=1;
	double fac=1;
	int freq=-1;
	char num[256];
	char c=tmp[pos];
	unsigned int k;
	if (isdigit(c)||(c=='+')||(c=='-')) {
	  fac=0;
	  int i=0;
	  // get the sign
	  if ((c=='+')||(c=='-')) { num[i++]=c; pos++; }
	  // get the factor
	  while (isdigit(tmp[pos])||(tmp[pos]=='.')) { num[i++]=tmp[pos++]; } num[i]=0;
	  sscanf(num,"%lf",&fac);
	}
	if (tmp[pos]=='*') { pos++; /* ignore the factor sign*/}
	if ((tmp[pos]!='f')&&(tmp[pos]!='F')) {
	  // OK, there is no composition, 
	  // let's deactivate us 
	  // and set Frequency to 0
	  setActive(0);
	  setFrequency(0);
	  return;
	}
	// ok, get Frequency number !!!
	size_t i=0;
	pos++;
	while (isdigit(tmp[pos])) { num[i++]=tmp[pos++]; } num[i]=0;
	sscanf(num,"%i",&freq);     // now get the frequency
	freq--;                     // to make it 0-based 
	// check if it is correct
	if ((freq<=-1)||(fac==0)||(freq==mNumber)) {
	  // OK, there is only a bad composition, 
	  // let's deactivate us 
	  // and set Frequency to 0
	  setActive(0);
	  setFrequency(0);
	  // and return
	  return;
	}

	// OK, we are a composition
	isCompo=1;
	int ncomps=0;
	F.push_back(CFreqCompo());
	F[ncomps].setOtherFactor(fac);
	F[ncomps].setOtherFrequency(freq);
	ncomps++;
	
	//
	// now try to test for the next term
	//
	
  while (pos < f_len)
  {
	  c=tmp[pos];
	  if ((c=='+')||(c=='-')) {
		if ((tmp[pos+1]=='f')||(tmp[pos+1]=='F')) {
		  if (c=='-') { fac=-1; }
		  else    	  { fac=1; }
		  pos++;
		} else {
		  fac=0;
		  int i=0;
		  num[i++]=c;  // copy the sign
		  pos++;
		  // get the factor
		  while (isdigit(tmp[pos])||(tmp[pos]=='.')) { num[i++]=tmp[pos++]; } num[i]=0;
		  sscanf(num,"%lf",&fac);
		  if (tmp[pos]=='*') { pos++; /* ignore the factor sign*/}
		}
		if ((tmp[pos]!='f')&&(tmp[pos]!='F')) {
		  // OK, no next field
		  for (k=0; k<F.size(); k++) { 
			if (F[k].getOtherFactor()==1)	{
			  cleanComposition();
			  setActive(0);
			  setFrequency(0);
			}
		  }
		  return;
		}
		
		// now get the frequency number
		i=0;
		pos++;
		while (isdigit(tmp[pos])) { num[i++]=tmp[pos++]; } num[i]=0;
		sscanf(num,"%i",&freq);
		freq--;      	// to make it 0-based
		if ((freq<=-1)||(fac==0)) {
		  // OK, no second field
		  for (k=0; k<F.size(); k++) { 
			if (F[k].getOtherFactor()==1) {
			  // we do not want to similar frequencies !!!
			  cleanComposition();
			  setActive(0);
			  setFrequency(0);
			}
		  }
		  return;
		}
		// check if it is correct
		if (freq==mNumber) {
		  // OK, there is only a bad composition, 
		  // let's deactivate us 
		  // and set Frequency to 0
		  cleanComposition();
		  setActive(0);
		  setFrequency(0);
		  // and return
		  return;
		}
	  
		// OK, now do sanity-check  - we do not want af1+bf1
		bool isUnique=true;
		for ( k = 0; k < F.size( ); ++k)
                { 
		  if (freq == F[k].getOtherFrequency( ))
                  {
			fac = fac + F[k].getOtherFactor( );

                        if (0 == fac)
                        {
			  // remove that frequency
			  F.erase(F.begin()+k);
			  ncomps--; 
			  k--;
			} else {
			  F[k].setOtherFactor(fac);
			}
			isUnique=false;
		  }
		}

                // everything ok!
		if (isUnique) {
		  F.push_back(CFreqCompo());
		  F[ncomps].setOtherFactor(fac);
		  F[ncomps].setOtherFrequency(freq);
		  ncomps++;
		}
	  } else {
		// Only one frequency ...
		if (F[1].getOtherFactor()==1) {
		  // we do not want two similar frequencies !!!
		  cleanComposition();
		  setActive(0);
		  setFrequency(0);
		  return;
		}
	  }
	}
  } else {
	// No it is just a number...
	//CAmpVarPeriPoint::setFrequencyString(f);
  }

}

void CFreqCompPeriPoint::cleanComposition()
{
  // there is no composition any longer !!!
  isCompo=0;
  // deallocate depending frequencies
  int freq;
  CFreqCompPeriPoint * ptr;
  unsigned int k;
  for (k=0; k<F.size(); k++) {   
	F[k].getPointer(&freq,&ptr);
	if (ptr!=0)
	  { ptr->removeDependency(mNumber); } // deregister
  }

  // now clean pointers
  for (k=0; k<F.size(); k++) {   
	F[k].setOtherFrequency(-1);
  }
  F.clear();
}

std::string const CFreqCompPeriPoint::getCompositeString ( ) const
{
  if (! isComposition( ))
  {
    return "";
  }

  ostringstream ostr;

  ostr << "=" << F[0].getCompositionString( );

  for ( unsigned int k = 1; k < F.size( ); ++k)
  { 
    std::string composition = F[k].getCompositionString( );

    ostr
      << (
        (F[k].getCompositionString( )[0] != '-') && (F[k].getCompositionString( )[0] != '0')
          ? (
            (F[k].getCompositionString( )[0] != '0')
              ? "+"
              : ""
            )
          :""
        ) 
      << F[k].getCompositionString( )
    ;
  }

  return ostr.str( );
}

list<CFreqCompo>::iterator CFreqCompPeriPoint::findDependance(int freq) {
    // create constant iterator for list.
    list<CFreqCompo>::iterator iter;

    // iterate through list and output each element.
    for (iter=depending.begin(); iter!=depending.end(); iter++) {
	  if (((CFreqCompo)(*iter)).getOtherFrequency()==freq) { return iter; }
    }

    return depending.end();//NULL;
}

int CFreqCompPeriPoint::addDependency(double factor, int freq, CFreqCompPeriPoint * ptr) {
  if (ptr==0) {
	MYERROREXIT("NULL-pointer given to AddDependency!!!");
	return -1;
  }
  if (isComposition()) {
	// if we ourself are a dependancy don't do it !!!
	return -1;
  }
  list<CFreqCompo>::iterator found=findDependance(freq);
  if (found==depending.end()) {  //NULL
	// not found so add one
	CFreqCompo tmp;
	tmp.setOtherFrequency(freq);
	tmp.setOtherFactor(factor);
	tmp.setPointer(freq,ptr);
	//   depending.addhead(tmp);
	depending.push_front(tmp);
  } else {
	// just change the settings !!!
	((CFreqCompo)(*found)).setOtherFactor(factor);
	((CFreqCompo)(*found)).setPointer(freq,ptr);
  }
  return 0;
}

void CFreqCompPeriPoint::removeDependency(int freq) {
    list<CFreqCompo>::iterator found=findDependance(freq);
    if (found!=depending.end()) {  //NULL
	// OK we found it, so remove it
	depending.erase(found);
    } else {
	// give a warning !!!
	MYERROR("A dependance for Frequency "<<freq<<" was not found !!!");
    }
}

void CFreqCompPeriPoint::getDepending(int what,int *f) {
  *f=-1;
  if (isComposition() && !useFreqValue) {
	*f=F[what].getOtherFrequency();
  }
}

void CFreqCompPeriPoint::setDepending(int what,int f, CSimplePeriPoint* tmp) {
  if (isComposition() && !useFreqValue) {
	// register Pointer
	F[what].setPointer(f,(CFreqCompPeriPoint*)tmp);
	// inform other class
	((CFreqCompPeriPoint*)tmp)->addDependency(F[what].getOtherFactor(),
											  getNumber(),
											  this);	
  }
}

void CFreqCompPeriPoint::checkActive() {
  if (useFreqValue) { return; }// stays active!!

  CFreqCompPeriPoint *tmp;
  int freq;
  // now deactivate us if composing one is not active
  for (unsigned int k=0; k<F.size(); k++) {   
	F[k].getPointer(&freq,&tmp);
	if (tmp) { if (!(tmp->getActive())) {setActive(0);} }
  }
}

ostream& CFreqCompPeriPoint::writeOut(ostream &s, int selected) const {
    s<<"F"<<mNumber+1<<"\t";
    int oldprec=s.precision(15);
    if (!getActive()) { s<<"("; }

    //--- frequency
    if (selected && isSelected(Fre)) {
	    if (isComposition()) { s<<'*'<<getCompositeString()<<'*'; }
	    else                 { s<<'*'<<getFrequency()<<'*'; }
    } else {
	    if (isComposition()) { s<<getCompositeString(); }
	    else                 { s<<getFrequency(); }
    }

    //--- amplitude
    if (selected && isSelected(Amp)) { s<<"\t*"<<getAmplitude(0)<<'*'; }
    else                             { s<<"\t "<<getAmplitude(0)<<' '; }

    //--- phase
    if (selected && isSelected(Pha)) { s<<"\t*"<<getPhase(0)<<'*'; }
    else                             { s<<"\t "<<getPhase(0)<<' '; }

    //--- composition
    if (isComposition()) { s<<"\t"<<getFrequency(); }
    
    if (!getActive()) { s<<")"; }
    s.precision(oldprec);
    return s;
}

std::string CFreqCompPeriPoint::writeSpecialOut(bool selected, bool reversed) {
    ostringstream s;
    s<<"F"<<mNumber+1<<"\t";
    int oldprec=s.precision(15);
    if (!getActive()) { s<<"("; }

    //--- frequency
    if (selected && isSelected(Fre)) {
	if (reversed)            { s<<'*'<<getFrequency()<<'*'; }
	else {
	    if (isComposition()) { s<<'*'<<getCompositeString()<<'*'; }
	    else                 { s<<'*'<<getFrequency()<<'*'; }
	}
    } else {
	if (reversed)            { s<<getFrequency(); }
	else {
	    if (isComposition()) { s<<getCompositeString(); }
	    else                 { s<<getFrequency(); }
	}
    }

    //--- amplitude
    if (selected && isSelected(Amp)) { s<<"\t*"<<getAmplitude(0)<<'*'; }
    else                             { s<<"\t "<<getAmplitude(0)<<' '; }

    //--- phase
    if (selected && isSelected(Pha)) { s<<"\t*"<<getPhase(0)<<'*'; }
    else                             { s<<"\t "<<getPhase(0)<<' '; }

    //--- composition
    if (isComposition()) {
	if (reversed) { s<<"\t"<<getCompositeString(); }
	else          { s<<"\t"<<getFrequency(); }
    }
    
    if (!getActive()) { s<<")"; }
    s.precision(oldprec);
    return s.str( );
}

std::string CFreqCompPeriPoint::writeLatexTableOut() {
    ostringstream s;
    s<<"F"<<mNumber+1<<"&\t";
    int oldprec=s.precision(15);

    //--- frequency
    s<<getFrequency(); 
    
    //--- amplitude
    s<<"&\t "<<getAmplitude(0)<<' ';

    //--- phase
    s<<"&\t "<<getPhase(0)<<" \\\\"; 
   
    s.precision(oldprec);
    return s.str( );
}

int CFreqCompPeriPoint::readIn(char * ptr) {
    double tmpa=0.006,tmpp=0, dtmpf=0;
    char tmpf[256];
    int active=1;
    // check if starting with a "("
    char c='*';
    int read=sscanf(ptr," %c",&c);
    // check and read in parameters
    if (c=='(') {
	active=0;
	read = sscanf(ptr," %c%s%lf%lf",&c,tmpf,&tmpa,&tmpp);
	if (sscanf(tmpf,"%lg",&dtmpf)) {
	    read = sscanf(ptr," %c%lf%lf%lf%s",&c,&dtmpf,&tmpa,&tmpp,tmpf);
	}
	read--;
    } else {
	read = sscanf(ptr,"%s%lf%lf",tmpf, &tmpa, &tmpp);
	if (sscanf(tmpf,"%lg",&dtmpf)) {
	    read = sscanf(ptr,"%lf%lf%lf%s",&dtmpf,&tmpa,&tmpp,tmpf);
	}
	active=1;
    }

    // Frequency
    if (read<1) { setActiveNoCheck(0); return 1; }
    if (tmpf[0]=='=') {
      std::string stmpf=tmpf;
      if (active==0 
	  && stmpf[stmpf.length()-1]==')') 
	{
	  stmpf.erase(stmpf.length()-1,1); 
	}

      setCompositeString(stmpf.c_str());      // set composite
      setActiveNoCheck(active);
    } else {
	setFrequency(dtmpf);           // set frequency
	setActive(active);
    }

    // Amplitude
    // has there been an error reading, if so, return
    if (read<2) { return 0;}
    setAmplitude(tmpa,0);
    // Phase
    if (read<3) {return 0;}
    setPhase(tmpp,0);
    // exit function
    return 0;
}

