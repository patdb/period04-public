#ifndef __basic_progress_filter_h__
#define __basic_progress_filter_h__

// 
// progress filter.
// 
// count read bytes and call progress callback function.
//
// mostly taken from
//
// An example of implementing a custom streambuf (performs XOR encryption)
// http://www.dreamincode.net/code/snippet2499.htm
// 

#include <streambuf>

template < typename charT, typename traits = std::char_traits< charT> >
class basic_progress_filter
  : public std::basic_streambuf< charT, traits>
{
public:
  typedef charT char_type;
  typedef traits traits_type;
  typedef typename traits_type::int_type int_type;
  typedef typename traits_type::pos_type pos_type;
  typedef typename traits_type::off_type off_type;

  typedef std::basic_streambuf< char_type, traits_type>  __streambuf_type;

  // The size of the input and output buffers.
  static size_t const BUFF_SIZE = 1024;

private:
  // This is the streambuf object that we are to read/write from/to.
  __streambuf_type & stream_buf;

  // Input buffer
  char_type * in_buf;

  // Output buffer
  char_type * out_buf;

  // progress
  unsigned long n;
  unsigned long n_total;
  bool (*progress) ( unsigned long n, unsigned long total);

public:
  explicit basic_progress_filter
    ( std::basic_streambuf< charT, traits> & stream_buf
    , unsigned long n_total = 0
    , bool (*progress) ( unsigned long n, unsigned long total) = 0
    )
    : stream_buf( stream_buf)
    , in_buf( new charT [ BUFF_SIZE])
    , out_buf( new charT [ BUFF_SIZE])
    , n( 0)
    , n_total( n_total)
    , progress( progress)
  {
    // Initialize get pointer. This should be zero so that underflow is called upon first read.
      this->setg( 0, 0, 0);

    // Initialize the put pointer. Overflow won't get called until this buffer is filled up,
    // so we need to use valid pointers.
    this->setp( out_buf, out_buf + BUFF_SIZE - 1);
  }

  // It's a good idea to release any resources when done using them.
  ~basic_progress_filter ( )
  {
    delete [ ] in_buf;
    delete [ ] out_buf;
  }

protected:
  // This is called when there are too many characters in the buffer (thus, a write needs to be performed).
  virtual int_type overflow ( int_type c);

  // This is called when the buffer needs to be flushed.
  virtual int_type sync ( );

  // This is called when there are no more characters left in the buffer (reads a character).
  virtual int_type underflow ( );
};


// This function is called when the output buffer is filled.
// In this function, the buffer should be written to wherever it should
// be written to (in this case, the streambuf object that this is controlling).
template < typename charT, typename traits> typename basic_progress_filter< charT, traits>::int_type basic_progress_filter< charT, traits>::overflow   ( typename basic_progress_filter< charT, traits>::int_type c )
{
  charT * ibegin = this->out_buf;
  charT * iend = this->pptr( );

  // Reset the put pointers to indicate that the buffer is free
  // (at least it will be at the end of this function).
  this->setp( out_buf, out_buf + BUFF_SIZE + 1);

  // If this is the end, add an eof character to the buffer.
  // This is why the pointers passed to setp are off by 1 
  // (to reserve room for this).
  if (! traits_type::eq_int_type( c, traits_type::eof( )))
  {
    *iend++ = traits_type::to_char_type( c);
  }

  // Compute the write length.
  int_type ilen = iend - ibegin;

  // Write it using the sputn method of the streambuf
  // object in our control.
  stream_buf.sputn( out_buf, (std::streamsize) ilen);

  return traits_type::not_eof( c);
}

// This is called to flush the buffer.
// This is called when we're done with the file stream (or when .flush() is called).
template < typename charT, typename traits>
typename basic_progress_filter< charT, traits>::int_type basic_progress_filter< charT, traits>::sync ( )
{
  return traits_type::eq_int_type( this->overflow( traits_type::eof( )), traits_type::eof( )) ? -1 : 0;
}

// Fill the input buffer.  This reads from the streambuf and 
// decrypts the contents by xoring it.
template < typename charT, typename traits>
typename basic_progress_filter< charT, traits>::int_type basic_progress_filter< charT, traits>::underflow ( )
{
  // Read enough bytes to fill the buffer.
  std::streamsize len = stream_buf.sgetn( in_buf, BUFF_SIZE);

  // add len to read bytes and call progress callback.
  if (0 != progress)
    (*progress) ( n += len, n_total);

  // Since the input buffer content is now valid (or is new)
  // the get pointer should be initialized (or reset).
  this->setg( in_buf, in_buf, in_buf + len);

  // If nothing was read, then the end is here.
  if (0 == len)
    return traits_type::eof( );

  // Return the first character.
  return traits_type::not_eof( in_buf[ 0]);
}

#endif // __basic_progress_filter_h__
