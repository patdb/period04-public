/*----------------------------------------------------------------------------
 *  CPeriPoint.h
 *            defines enumerations: Parts
 *                                  CalcMode
 *            declares classes:     CSimplePeriPoint
 *                                  CAmpVarPeriPoint
 *                                  CFreqCompo
 *                                  CFreqCompPeriPoint
 *---------------------------------------------------------------------------*/

#ifndef __peripoint_h__
#define __peripoint_h__

#include <string>

#include <iostream>
using std::ios;
using std::cerr; 
using std::cout;      
using std::endl;

#include <list>
//using std::list;
using std::iterator;

#include <vector>
using std::vector;

#if 0
#include <boost/rational.hpp>
#endif

//#include <stdio.h>
//#include <cmath>
#include "CTimeString.h"
#include "message.h"

#define ZEROFREQ 0
#define ZEROAMP  0
#define ZEROPHASE 0

enum Parts    {Fre, Amp, Pha, BMFre, BMAmp, BMPha};
enum CalcMode {NoVar, AllVar, AmpVar, PhaseVar};

class CSimplePeriPoint;
class CAmpVarPeriPoint;
class CFreqCompPeriPoint;

// if the following is active there may still be problems with
// Amplitude-variations and Frequency-compositions
//typedef CAmpVarPeriPoint CPeriPoint;
typedef CFreqCompPeriPoint CPeriPoint;

//---
//--- CSimplePeriPoint
//---

class CSimplePeriPoint
{
 protected:

    int    mNumber;
    double mFrequency;
    double mFrequencySigma;
    double mAmplitude;
    double mAmplitudeSigma;
    double mPhase;
    double mPhaseSigma;
    int    mSelection;
    int    mActive;

    double history_fre_derive;
    double history_amp_derive;
    double history_pha_derive;

 public:

    // constructors
    CSimplePeriPoint(double f=ZEROFREQ, double a=ZEROAMP, double p=ZEROPHASE)
	:mNumber(0), mFrequency(f),mFrequencySigma(0.0), mAmplitudeSigma(0),
	mPhase(p), mPhaseSigma(0), mSelection(0), mActive(0)
	{
	    setAmplitude(a,0); 
	}

    virtual ~CSimplePeriPoint() { }

    CSimplePeriPoint &operator=(CSimplePeriPoint const & tmp);
  
    void setNumber(int i) { mNumber=i; }
    int  getNumber() const { return mNumber; }

    double getFreDerive() const { return history_fre_derive; }
    double getAmpDerive() const { return history_amp_derive; }
    double getPhaDerive() const { return history_pha_derive; }
 
    void setFrequency(double f)              { mFrequency=f; mActive=1; }
    virtual double getFrequency() const      { return mFrequency; }
    virtual double getFrequencySigma() const { return mFrequencySigma; }
  
    void setAmplitude(double a,int)             { mAmplitude=(a<0.0)?-a:a; }
    virtual double getAmplitude(int) const      { return mAmplitude; }
    virtual double getAmplitudeSigma(int) const { return mAmplitudeSigma; }

    void setPhase(double p,int)             { mPhase=std::fmod(p,1); }
    virtual double getPhase(int) const      { return mPhase; }
    virtual double getPhaseSigma(int) const { return mPhaseSigma; }

    void cleanSigmas() {
	mFrequencySigma = 0.0;
	mAmplitudeSigma = 0.0;
	mPhaseSigma     = 0.0;
    }

    // is this frequency already in use
    virtual bool empty() const;
    virtual void getDepending(int,int *f) { *f=-1; };
    virtual void setDepending(int,int, CSimplePeriPoint*) {;}

    // is for amplitude Variations
    virtual CalcMode getAmpVariation() const { return NoVar; }
    virtual void     setAmpVar(CalcMode) { }
    virtual void     createAmplitudeVariationData(int) { }
 
    virtual int getDegreesOfFreedom(int) {
	return (isSelected(Fre)?1:0)+
	       (isSelected(Amp)?1:0)+
	       (isSelected(Pha)?1:0);
    }

    // is this frequency a combination
    virtual int         isComposition() const { return 0; }
    virtual const std::string getCompositeString() const { return ""; }
    virtual void        setCompositeString(const char *) { }
    virtual void        cleanComposition() { }

    // stuff to do with selections
    virtual void setSelection(Parts p)   { mSelection|=1<<p; }
    void         unsetSelection(Parts p) { mSelection&=~(1<<p); }
    int          isSelected(Parts p) const {
	return (mActive)?(mSelection&(1<<p)):0;
    }

    void storeSelection() {
	int tmp=mSelection&7;
	mSelection=tmp|(tmp<<3);
    }
    void restoreSelection() { 
	int tmp=(mSelection>>3)&7;
	mSelection=tmp|(tmp<<3);
    }

    // activate/deactivate frequency
    void setActiveNoCheck(int i) { mActive=i;}
    void setActive(int i) {
	setActiveNoCheck(i);
	if (mFrequency==0.0) { setActiveNoCheck(0); }
    }
    // is this frequency activated
    int getActive() const { return mActive; }

    // for calculations
    void recalcFrequencies() { }

    double *getFrequencyRef() { return &mFrequency; }
    double *getFrequencySigmaRef() { return &mFrequencySigma; }
    double *getAmplitudeRef(int) { return &mAmplitude; }
    double *getAmplitudeSigmaRef(int) { return &mAmplitudeSigma; }
    double *getPhaseRef(int) { return &mPhase; }
    double *getPhaseSigmaRef(int) { return &mPhaseSigma; }

    double calc(double t, int ID) const {
	return getAmplitude(ID)*std::sin(phaseCalc(t,ID));
    }
    double calcBM(double t, int ID, CSimplePeriPoint bm) const {
	return getAmplitude(ID)*std::sin(phaseCalcBM(t,ID,bm));
    }

    double phaseCalc ( double t, int ID) const
    {
      double const pha
        = getFrequency( ) * t
        + getPhase( ID)
      ;
      
      double tmp;
      return MY2PI * std::modf( pha, &tmp);
    }
    
    double phaseCalcBM ( double t, int ID, CSimplePeriPoint bm) const
    {
      double const pha
        = getFrequency( ) * (t + bm.getAmplitude( ID) * std::sin( bm.phaseCalc( t, ID)))
        + getPhase( ID)
      ;
      double tmp;
      return MY2PI * std::modf( pha, &tmp);
    }
    //---
    //--- methods to calculate the derivatives:
    //--- derive   ... for the normal fitting function 
    //---              AMP*std::sin( FREQ*t+PHA )
    //--- deriveBM ... for fitting function containing a periodic time shift
    //---              AMP*std::sin( FREQ*[t+a*std::sin(w*t+pha)]+PHA )
    //---
    double derive(Parts what, double t, int ID) const {
	if (mActive) {
	    switch(what) {
		case Fre:
		    return getAmplitude(ID)*MY2PI*std::cos(phaseCalc(t,ID))*t;
		case Amp:
		    return (std::sin(phaseCalc(t,ID)));
		case Pha:
		    return (getAmplitude(ID)*MY2PI*std::cos(phaseCalc(t,ID)));
		default:
		    return 0;
	    }
	}
	return 0;
    }

   void deriveFAP(double t, int ID) {
	if (mActive) {
	    const double ph   = phaseCalc(t,ID);
	    const double ph_d = getAmplitude(ID)*MY2PI*std::cos(ph);
	    history_fre_derive = ph_d*t;
	    history_amp_derive = std::sin(ph);
	    history_pha_derive = ph_d;
	}
    }

    void deriveAP(double t, int ID) {
	if (mActive) {
	    const double ph = phaseCalc(t,ID);
	    history_amp_derive = std::sin(ph);
	    history_pha_derive = getAmplitude(ID)*MY2PI*std::cos(ph);
	}
    }

    double deriveBM(Parts what, double t, int ID, CSimplePeriPoint bm) {
	if (mActive) {
	    switch(what) {
		case Fre:
		    return getAmplitude(ID)*
			(t+bm.getAmplitude(ID)*std::sin(bm.phaseCalc(t,ID)))*
			MY2PI*std::cos(phaseCalcBM(t,ID,bm));
		case Amp:
		    return std::sin(phaseCalcBM(t,ID,bm));
		case Pha:
		    return getAmplitude(ID)*MY2PI*std::cos(phaseCalcBM(t,ID,bm));
		case BMFre:
		    return 
			getAmplitude(ID)*bm.getAmplitude(ID)*getFrequency()*t*
			MY2PI*std::cos(bm.phaseCalc(t,ID))*
			MY2PI*std::cos(phaseCalcBM(t,ID,bm));
		case BMAmp:
		    return getAmplitude(ID)*getFrequency()*
			std::sin(bm.phaseCalc(t,ID))*
			MY2PI*std::cos(phaseCalcBM(t,ID,bm));
		case BMPha:
		    return getAmplitude(ID)*bm.getAmplitude(ID)*getFrequency()*
			MY2PI*std::cos(bm.phaseCalc(t,ID))*
			MY2PI*std::cos(phaseCalcBM(t,ID,bm));
	    }
	}
	return 0;
    }

    void adapt();
    void fixNumbers();
    double calcEpoch(double t, int mode, int intensity) const;
    double calcBMEpoch(double t, int mode, int intensity, CSimplePeriPoint bm) const;

    // input / output
    virtual ostream& writeOut(ostream &s, int selected) const;
    virtual std::string writeOutError ( ) const;

    std::istream&    readIn(std::istream &s) {
	char buffer[1024];
	s.getline(buffer,1023);
	if (readIn(buffer)) {
	    // there has been an error, so let us set a flag
	    s.clear(ios::badbit);
	}
	return s;
    }

    virtual int readIn(char* ptr);

};

//---
//--- CAmpVarPeriPoint
//---

class CAmpVarPeriPoint : public CSimplePeriPoint
{
 protected:

    CalcMode mIsAmpVar;

    // normal data
    double *mPhaseVar;
    double *mPhaseSigmaVar;
    double *mAmpVar;
    double *mAmpSigmaVar;
    int mMaxID;

 public:
 
    CAmpVarPeriPoint(double f=ZEROFREQ, double a=ZEROAMP, double p=ZEROPHASE)
	:CSimplePeriPoint(f,a,p),
	mIsAmpVar(NoVar),
	mPhaseVar(0),mPhaseSigmaVar(0),
	mAmpVar(0),mAmpSigmaVar(0),
	mMaxID(0)
	{ }

    virtual ~CAmpVarPeriPoint() { 
	setAmpVar(NoVar); 
    }
    
    virtual CAmpVarPeriPoint & operator =(CAmpVarPeriPoint const & tmp);

    void setAmplitude(double a,int ID) {
	a=(a==0.0)?ZEROAMP:a;
	// have we really got amplitude variations ?
	if (mMaxID<=ID) { ID= -1; }
	// Check which one to set
	if (ID<0) {
	    mAmplitude=a;
	} else {
	    mAmpVar[ID]=a;
	}
    }
 
    double getAmplitude(int ID) const {
	if (mMaxID<=ID) { ID= -1; }
	// check which one to set
	if (ID<0) { return mAmplitude; }
	return mAmpVar[ID];
    }

    double getAmplitudeSigma(int ID) const {
	if (mMaxID<=ID) { ID= -1; }
	// check which one to set
	if (ID<0) {
	    return mAmplitudeSigma;
	}
	return mAmpSigmaVar[ID];
    }

    void setPhase(double p,int ID) {
	p=std::fmod(p,1);
	// have we really got Amplitude variations ?
	if (mMaxID<=ID) { ID= -1; }
	// Check which one to set
	if (ID<0) {
	    mPhase=p;
	} else {
	    mPhaseVar[ID]=p;
	}
    }

    double getPhase(int ID) const {
	if (mMaxID<=ID) { ID= -1; }
	// check which one to set
	if (ID<0) {
	    return mPhase;
	}
	return mPhaseVar[ID];
    }

    double getPhaseSigma(int ID) const {
	if (mMaxID<=ID) { ID= -1; }
	// check which one to set
	if (ID<0) {
	    return mPhaseSigma;
	}
	return mPhaseSigmaVar[ID];
    }

	void adapt();
	void fixNumbers();

    // is for amplitude variations
    CalcMode getAmpVariation() const { return mIsAmpVar; }
    void     setAmpVar(CalcMode i);

    void createAmplitudeVariationData(int MaxID);

    virtual int getDegreesOfFreedom(int names) {
	switch (mIsAmpVar) {
	    case NoVar:
		return (isSelected(Fre)?1:0)
		    +(isSelected(Amp)?1:0)
		    +(isSelected(Pha)?1:0);
	    case AllVar:
		return (isSelected(Fre)?1:0)
		    +(isSelected(Amp)?names:0)
		    +(isSelected(Pha)?names:0);
	    case AmpVar:
		return (isSelected(Fre)?1:0)
		    +(isSelected(Amp)?names:0)
		    +(isSelected(Pha)?1:0);
	    case PhaseVar:
		return (isSelected(Fre)?1:0)
		    +(isSelected(Amp)?1:0)
		    +(isSelected(Pha)?names:0);
	}
	return 0;
    }

    double *getAmplitudeRef(int ID) {
	if (mMaxID<=ID) { ID= -1; }
	// Check which one to set
	if (ID<0) {
	    return &mAmplitude;
	}
	return &mAmpVar[ID];
    }

    double *getAmplitudeSigmaRef(int ID) {
	if (mMaxID<=ID) { ID= -1; }
	// Check which one to set
	if (ID<0) {
	    return &mAmplitudeSigma;
	}
	return &mAmpSigmaVar[ID];
    }

    double *getPhaseRef(int ID) {
	if (mMaxID<=ID) { ID= -1; }
	// Check which one to set
	if (ID<0) {
	    return &mPhase;
	}
	return &mPhaseVar[ID];
    }

    double *getPhaseSigmaRef(int ID) {
	if (mMaxID<=ID) { ID= -1; }
	// Check which one to set
	if (ID<0) {
	    return &mPhaseSigma;
	}
	return &mPhaseSigmaVar[ID];
    }

    double calc ( double t, int ID) const
    {
      switch (getAmpVariation( ))
      {
      case NoVar:
        return CSimplePeriPoint::getAmplitude( ID) * std::sin( CSimplePeriPoint::phaseCalc( t, ID));
      case AllVar:
        return getAmplitude( ID) * std::sin( phaseCalc( t, ID));
      case AmpVar:
        return getAmplitude( ID) * std::sin( phaseCalc( t, -1));
      case PhaseVar:
        return getAmplitude( -1) * std::sin( phaseCalc( t, ID));
      }
      
      return 0;
    }

    double calcBM ( double t, int ID, CSimplePeriPoint bm) const
    {
      return CSimplePeriPoint::getAmplitude( ID) * std::sin( CSimplePeriPoint::phaseCalcBM( t, ID, bm));
    }

    double phaseCalc(double t,int ID) const {
	double pha=t*getFrequency()+getPhase(ID);
	double tmp;
	return MY2PI*std::modf(pha,&tmp);
    }
    double phaseCalcBM(double t,int ID, CSimplePeriPoint bm) const {
	double pha=getFrequency()*(
	    t+bm.getAmplitude(ID)*std::sin(bm.phaseCalc(t,ID)))+getPhase(ID);
	double tmp;
	return MY2PI*std::modf(pha,&tmp);
    }

    double derive(Parts what, double t, int ID) const {
	if (mActive) {
	    switch(what) {
		case Fre:
		    return getAmplitude(ID)*std::cos(phaseCalc(t,ID))*MY2PI*t;
		case Amp:
		    if ((mIsAmpVar==AllVar)||(mIsAmpVar==AmpVar)) {
			return (std::sin(phaseCalc(t,ID)));
		    } else {
			return (std::sin(phaseCalc(t,-1)));
		    }
		case Pha:
		    switch (mIsAmpVar) {
			case NoVar:
			    return (getAmplitude(-1)*MY2PI*std::cos(phaseCalc(t,-1)));
			case AllVar:
			    return (getAmplitude(ID)*MY2PI*std::cos(phaseCalc(t,ID)));
			case AmpVar:
			    return (getAmplitude(ID)*MY2PI*std::cos(phaseCalc(t,-1)));
			case PhaseVar:
			    return (getAmplitude(-1)*MY2PI*std::cos(phaseCalc(t,ID)));
		    }
		    break;
		default:
		    return 0;
	    }
	}
	return 0;
    }

};


//--- ------------------------------------------------------- ---//
//---                    CFreqCompo                           ---//
//--- ------------------------------------------------------- ---//

class CFreqCompo
{
public:
#if 0
  typedef boost::rational< int> factor_type;
#else
  typedef double factor_type;
#endif
  typedef factor_type const factor_type_const;

protected:  

  int mFreq;
  factor_type mFactor;
  CFreqCompPeriPoint * pnt;
    
public:

  CFreqCompo ( )
    : mFreq( -1)
    , mFactor( 0)
    , pnt( 0)
  {
  }

  CFreqCompo
    ( int freq
    , factor_type const & factor
    , CFreqCompPeriPoint * pnt
    )
    : mFreq( freq)
    , mFactor( factor)
    , pnt( pnt)
  {
  }

    factor_type getOtherFactor ( ) { return mFactor; }
    void setOtherFactor ( factor_type const & factor) { mFactor = factor; }
 
    int getOtherFrequency ( ) { return mFreq; }
    void setOtherFrequency ( int newFreq) { mFreq = newFreq; pnt = 0; }
    
    // should only be called from cleanComposition
    void setPointer(int fr, CFreqCompPeriPoint* pt);
    void getPointer(int *fr, CFreqCompPeriPoint **pt) {*fr=mFreq; *pt=pnt;};
    
    double getFrequency() {
	if (pnt!=0) {
	    if (mFreq!=((CAmpVarPeriPoint*)pnt)->getNumber()) {
		MYERROR("something about the pointers is wrong !!!\n"
			"Should be:"<<mFreq<<" is:"<<
			((CAmpVarPeriPoint*)pnt)->getNumber());
	    }
#if 0
          return ((CAmpVarPeriPoint *) pnt)->getFrequency( ) * boost::rational_cast< double>( mFactor);
#else
          return ((CAmpVarPeriPoint *) pnt)->getFrequency( ) * mFactor;
#endif
	}
	return 0;
    }

    std::string getCompositionString ( ) const;
};

//--- ------------------------------------------------------------- ---//
//---                    CFreqCompPeriPoint                         ---//
//--- ------------------------------------------------------------- ---//

class CFreqCompPeriPoint : public CAmpVarPeriPoint
{
 protected:
  
  int                isCompo;
  std::vector< CFreqCompo> F;
  std::list< CFreqCompo>   depending;
  bool               useFreqValue;        
  
 public:
  
  // constructor
  CFreqCompPeriPoint(double f=ZEROFREQ, 
					 double a=ZEROAMP, 
					 double p=ZEROPHASE)
	:CAmpVarPeriPoint(f,a,p),isCompo(0), useFreqValue(0)
	{}
 
  // destructor
  virtual ~CFreqCompPeriPoint() { cleanComposition(); }

  void recalcFrequencies() {
	if (isComposition() && !useFreqValue) {
	  mFrequency=0.0;
	  for (unsigned int i=0; i<F.size(); i++) {
		mFrequency+=F[i].getFrequency();
	  }
	}
  }

  void setFrequency(double freq) {
	cleanComposition();
	CAmpVarPeriPoint::setFrequency(freq);
  }

  int  isComposition() const { return isCompo; }
  void cleanComposition();

  int  getCompoTermNumber() const { return F.size(); }

  bool getCompoUseFreqValue() const { return useFreqValue; }
  void setCompoUseFreqValue(bool val) {
	useFreqValue=val;
  }

  const std::string getCompositeString() const;
  void        setCompositeString(const char *f);
  
  std::list< CFreqCompo>::iterator findDependance(int freq);
  int  addDependency(double factor, int freq, CFreqCompPeriPoint * ptr);
  void removeDependency(int freq);
  void checkActive();

  void setSelection(Parts p) {
	if (isComposition() && !useFreqValue && (p==Fre)) {
	  return; // cannot select composit frequency
	}
	CAmpVarPeriPoint::setSelection(p);
  }
  
  double *getFrequencyRef() {
	if (isComposition() && !useFreqValue) { 
	  MYERROR("Tried to get a reference on a composition !!!");
	  return NULL;
	}
	return CAmpVarPeriPoint::getFrequencyRef();
  }

  virtual void getDepending(int what,int *f);
  virtual void setDepending(int what,int f, CSimplePeriPoint* tmp);

  /////////////////////////compotriple
  double derive ( Parts what, double t, int ID) const
  {
    if (mActive)
    {
      switch (what)
      {
        case Fre:
        {
          double der = getAmplitude( ID) * std::cos( phaseCalc( t, ID)) * MY2PI * t;

          // Create constant iterator for list.
          std::list< CFreqCompo>::const_iterator iter;

          // Iterate through list and output each element.
          for ( iter = depending.begin( ); iter != depending.end( ); ++iter)
          {
            // do this for every dependent frequency
            CFreqCompPeriPoint * tmp;
            int f;

            ((CFreqCompo)(*iter)).getPointer( &f, &tmp);
            if (tmp->getActive( ))
            {
              // do this only if other frequency is active
#if 0
              der += boost::rational_cast< double>( ((CFreqCompo) (*iter)).getOtherFactor( ))
#else
              der += ((CFreqCompo) (*iter)).getOtherFactor( )
#endif
                   * tmp->derive( Fre, t, ID)
              ;
            }
          }

          return der;
        }
        case Amp:
          return std::sin( phaseCalc( t, ID));
        case Pha:
          return getAmplitude( ID) * MY2PI * std::cos( phaseCalc( t, ID));
        default:
          return 0;
      }
    }

    return 0;
  }

  /////////////////////////compotriple
  void deriveFAP(double t, int ID) {
	if (mActive) {
	  const double ph   = phaseCalc(t,ID);
	  const double ph_d = getAmplitude(ID)*MY2PI*std::cos(ph);
	  double der = ph_d*t;
	  // Create constant iterator for list.
	  std::list< CFreqCompo>::const_iterator iter;
	  // Iterate through list and output each element.
	  for (iter=depending.begin();iter!=depending.end();iter++){
		// do this for every dependent frequency
		CFreqCompPeriPoint * tmp;
		int f;
		((CFreqCompo)(*iter)).getPointer(&f,&tmp);
		if (tmp->getActive()) {
		  // do this only if other frequency is active
#if 0
                  der += boost::rational_cast< double>( ((CFreqCompo) (*iter)).getOtherFactor( ))
#else
                  der += ((CFreqCompo) (*iter)).getOtherFactor( )
#endif
                       * tmp->derive( Fre, t, ID);
		}
	  }
	  history_fre_derive = der;
	  history_amp_derive = std::sin(ph);
	  history_pha_derive = ph_d;
	}
  }
  
  // Input / Output
  std::string writeSpecialOut ( bool selected, bool reversed);
  std::string writeLatexTableOut ( );
  virtual ostream& writeOut(ostream &s, int selected) const; 
  
  virtual int readIn(char *ptr);
	
};

// General I/O
inline ostream& operator<<(ostream& s,const CPeriPoint &t)
{ return t.writeOut(s,0); }
inline std::istream& operator>>(std::istream& s,CSimplePeriPoint &t)
{ return t.readIn(s); }
inline ostream& operator<<(ostream& s,const CSimplePeriPoint &t)
{ return t.writeOut(s,0); }

#endif
