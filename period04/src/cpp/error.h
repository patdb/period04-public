/*-------------------------------------------------------------------------
 * ERROR.h
 *        prints debuginformation on command line
 *------------------------------------------------------------------------*/

#ifndef __error_h__
#define __error_h__

//#include <stdlib.h>

#include <iostream>
using std::cerr;
using std::endl;

#define ERROR_PREFIX     cerr << __FILE__ << ":" << __LINE__ << ": "

#define MYERROR(a)         ERROR_PREFIX << a << endl;
#define MYERROREXIT(a)     ERROR_PREFIX << a << endl; abort(); exit(255);

#endif
