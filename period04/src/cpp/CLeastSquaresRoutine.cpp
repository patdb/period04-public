
#include "CPeriod.h"

#include "Period04_java.h" // setProgressString()
#include "CSingularValueDecomposition.h"

#include <stdio.h>   // sprintf
#include <time.h>    // time

#include <stdlib.h>  // pseudo random number generator
#ifdef WIN32
#define RANDOM rand 
#define SRANDOM srand
#else
#define RANDOM random
#define SRANDOM srandom
#endif

#define PRCINT2 1e-10
#define PRCEXT2 1e-12
#define CH_INIT 99999
#define COND_LIMIT 1.0E+20 


int CPeriod::calc ( CTimeString & ti, bool checkIter)
{
  // now calculate everything and return with error-code
  return calcLoop( ti, checkIter);
}

int CPeriod::calcLoop ( CTimeString & ti, bool checkIter)
{
  fixNumbers( ); // makes sure that input amplitudes are not smaller than 1.0e-50
  // because this would create problems in the LS routine
  prepare( ti);           // initialises the data
  recalc( );              // recalculate the frequency-combinations
  mChisqr = chiSqr( ti);    // find ChiSqr

  if (mFlamda == 0.0 || mFlamda > 0.1)
  { // reset lambda if necessary
    mFlamda = mFlamda_def;
  }

  double mFlamda_before = mFlamda; // store the old value 
  mFlamda = 0.001;

  double ch = CH_INIT, chdiff;
  mIterations = 0;
  int result = 0, factor = 1, quit = 0;
  char txt [256];

  while (quit == 0)
  {                     
    // fill in percentage
    sprintf( txt, mProgressMessage, mIterations, mMaxIterations);
    quit = setProgressString( txt);
    result = curFit( );          // make a fit of the current data

    if (result != 0)
    {
      // the determinant could not be determined
      // or cancel was pressed, so let's quit
      quit = 1;
    }

    // check for conditions to quit loop
    chdiff = (0 == mIterations) ? PRCEXT2 : ch - mChisqr;
    ch = mChisqr;
    ++mIterations;
    
    if (checkIter)
    {
      if(mIterations >= factor * mMaxIterations)
      {
        char text [1024];

        sprintf
          ( text
          , "Maximum number of iterations reached\n"
            "Do you want to continue?"
          )
        ;
        
        if (confirm(text)==0)
        {
          quit=1;
        }
        else
        {
          ++factor;
        }   // continue - enlarge factor!
      }
    }
    else
    {
      ++factor;
    } // continue by default
  
    //cout << mIterations << " " << std::sqrt(mChisqr) << " " << mChisqr<<" "<< chdiff << endl;
    if (chdiff < PRCEXT2)
    {
      quit = 1;
    }
  }

  //--- check wether lambda decreased during calculation
  mFlamda_ratio = mFlamda / mFlamda_before;

  //--- check if the final matrix is ill-conditioned
  if (mCheckCondNumber)
  {
    CMatrix tmp( mVariables, mVariables);

    for( int j = 0; j < mVariables; ++j)
    {
      CVector const & mWorkMatrix_j = mWorkMatrix[j];
      double const & mBaseMatrix_j_j = mBaseMatrix[j][j];

      for( int k = 0; k < mVariables; ++k)
      {
        tmp[j][k]
          = mWorkMatrix_j[k]
          / std::sqrt( mBaseMatrix_j_j * mBaseMatrix[k][k]);
      }
    }

    if (tmp.cond( ) > COND_LIMIT && result == 0)
    {
      result=4; 
    }
  }

  terminate( );       // remove allocated memory, etc...
  adapt( );     // adapt phases and frequencies to fit sensible parameters
  setResiduals( ti);  // write out the residuals
  return result;     // return without an error
}

void CPeriod::prepare ( CTimeString & ti)
{
    isCalculationCanceled();
    mTimeString=&ti;
    // for amplitude variations:
    int ActiveNames = ti.getActiveNames(mWhat); // ... of attribute mWhat
    mActiveNames = ActiveNames;
    int HighestID   = ti.getHighestActiveIDName(mWhat)+1;
    // now prepare periods for amplitude-variations
    for (int i=0;i<getFrequencies();i++) {
	mData[i].createAmplitudeVariationData(HighestID);
    }
    // if we do not use frequencies, 
    mVariables=countDegreesOfFreedom(ActiveNames);
    mVariablesCopy = mVariables; // make a copy 
    // calculate the maximum number of iterations
    mMaxIterations=3*mVariables;
    if (mMaxIterations<20) { mMaxIterations=20; }
    if ((mMaxIterations<40) && mBinaryMode) { mMaxIterations=40; }
    // reset number of iterations
    mIterations=0;
    // prepare variables-dependent variables  
    mWorkMatrix.changesize(mVariables,mVariables);
    mBaseMatrix.changesize(mVariables,mVariables);
    mWork= new CDeriveData[mVariables+5];


  int where = 0;

  // first variable: zero-point
  mWork[where].result = &mZeropoint;
  mWork[where].sigma = &mZeropointSigma;
  mWork[where].start =  mZeropoint;
  mWork[where].ID = -1;
  ++where;

  
  // all other variables
//  int ID    =-1;   // starts with default-frequency
    
  
  for( int fr = 0; fr < getFrequencies( ); ++fr)
  {
    // mWork[where].ID=-1; // by default valid for all... // means what??

    //---
    //--- for the frequencies
    //---
    if (mData[fr].isSelected( Fre))
    {
      mWork[where].result = mData[fr].getFrequencyRef( );
      mWork[where].sigma = mData[fr].getFrequencySigmaRef( );
      mWork[where].start = mData[fr].getFrequency( );
      mWork[where].freq = fr;
      mWork[where].what = Fre;
      mWork[where].ID = -1;
      ++where;
    }

    //---
    //--- for the amplitudes
    //---
    if (mData[fr].isSelected( Amp))
    {
      // if amplitude =0 use a default of 0.006
      if (mData[fr].getAmplitude( -1) == 0.0)
      {
        mData[fr].setAmplitude( 0.006, -1);
      }

      // get the variation-mode
      CalcMode Var = mData[fr].getAmpVariation( );

      // now fill in the data
      switch (Var)
      {
      case NoVar:
      case PhaseVar: 
      {
        // no amplitude variations, so ID=-1
        int ID = -1;
        mWork[where].result = mData[fr].getAmplitudeRef( ID);
        mWork[where].sigma = mData[fr].getAmplitudeSigmaRef( ID);
        mWork[where].start = mData[fr].getAmplitude( ID);
        mWork[where].freq = fr;
        mWork[where].what = Amp;
        mWork[where].ID = ID;
        ++where;
        break;
      }
      case AmpVar:
      case AllVar: 
      {
        // amplitude variations
        for ( int i = 0; i < ti.numberOfNames( mWhat); ++i)
        {
          CName & tmp = ti.getIndexName( mWhat, i);

          if (tmp.getPoints() != 0)
          {
            // this is an active Frequency, so include it...
            int ID = tmp.getID( );
            // now add it...
            mWork[where].result = mData[fr].getAmplitudeRef( ID);
            mWork[where].sigma = mData[fr].getAmplitudeSigmaRef( ID);
            mWork[where].start = mData[fr].getAmplitude( ID);
            mWork[where].freq = fr;
            mWork[where].what = Amp;
            mWork[where].ID = ID;
            ++where;
          }
        }

        break;
      }
      } // switch
    } // if
    
	//---
	//--- for the phases
	//---
	if (mData[fr].isSelected(Pha)) {
	    // get The Variation-mode
	    CalcMode Var=mData[fr].getAmpVariation();
	    // now fill in the data
	    switch (Var) {
		case NoVar:
		case AmpVar:
		{
		    // no phase variations, so ID=-1
		    int ID=-1;
		    mWork[where].result=mData[fr].getPhaseRef(ID);
		    mWork[where].sigma=mData[fr].getPhaseSigmaRef(ID);
		    mWork[where].start=mData[fr].getPhase(ID);
		    mWork[where].freq=fr;
		    mWork[where].what=Pha;
		    mWork[where].ID=ID;
        ++where;
		    break;
		}
		case PhaseVar:
		case AllVar:
		{
		    // phase variations
		    for (int i=0;i<ti.numberOfNames(mWhat);i++) {
			CName &tmp=ti.getIndexName(mWhat,i);
			if (tmp.getPoints()!=0) {
			    // this is an active Frequency, so include it...
			    int ID=tmp.getID();
			    // now add it...
			    mWork[where].result=mData[fr].getPhaseRef(ID);
			    mWork[where].sigma=mData[fr].getPhaseSigmaRef(ID);
			    mWork[where].start=mData[fr].getPhase(ID);
			    mWork[where].freq=fr;
			    mWork[where].what=Pha;
			    mWork[where].ID=ID;
          ++where;
			}
		    }
		    break;
		}
	    }
	}
    }
    //---
    //--- add-on for periodic time shift mode
    //---
    if (mBinaryMode) { // add binary frequency as last frequency
	int fr = getFrequencies();
	mWork[where].ID=-1;
	//---
	//--- for the frequencies
	//---
	if (mBMPeriod.isSelected(Fre)) {
	    mWork[where].result= mBMPeriod.getFrequencyRef();
	    mWork[where].sigma = mBMPeriod.getFrequencySigmaRef();
	    mWork[where].start = mBMPeriod.getFrequency();
	    mWork[where].freq  = fr;
	    mWork[where].what  = BMFre;
	    mWork[where].ID    = -1;
      ++where;
	}
	//---
	//--- for the amplitudes
	//---
	if (mBMPeriod.isSelected(Amp)) {
	    // no amplitude variations, so ID=-1
	    int ID=-1;
	    mWork[where].result= mBMPeriod.getAmplitudeRef(ID);
	    mWork[where].sigma = mBMPeriod.getAmplitudeSigmaRef(ID);
	    mWork[where].start = mBMPeriod.getAmplitude(ID);
	    mWork[where].freq  = fr;
	    mWork[where].what  = BMAmp;
	    mWork[where].ID    = ID;
      ++where;
	}
	//---
	//--- for the phases
	//---
	if (mBMPeriod.isSelected(Pha)) {
	    // no phase variations, so ID=-1
	    int ID=-1;
	    mWork[where].result= mBMPeriod.getPhaseRef(ID);
	    mWork[where].sigma = mBMPeriod.getPhaseSigmaRef(ID);
	    mWork[where].start = mBMPeriod.getPhase(ID);
	    mWork[where].freq  = fr;
	    mWork[where].what  = BMPha;
	    mWork[where].ID    = ID;
      ++where;
	}
    }
    setActiveIndex();
}

void CPeriod::terminate() {
    mVariables=0;
    mTimeString=0;
    delete [] mWork;
    mWork=0;
    mWorkMatrix.changesize(0,0);
    mBaseMatrix.changesize(0,0);
}

void CPeriod::prepareMatrices ( )
{
  for( int j = 0; j < mVariables; ++j)
  {
    mWork[j].weighted = 0;                // clear weights
    mWork[j].start = *(mWork[j].result);  // use result(pointer!) as start

    // clear mBaseMatrix
    CVector & mBaseMatrix_j = mBaseMatrix[j];

    for( int k=0; k <= j; ++k)
    {
      mBaseMatrix_j[k]=0.0;            
    }
  }

  return;
}

//---
//--- prepareDerive()
//--- 1.) calculates mWork[i].weighted for every parameter[i]:
//---     in the unweighted case:
//---     mWork[i].weighted =     SUM     ( residual*mWork[i].derived  )
//---                         [timepoints] 
//--- 2.) sets up the elements of mBaseMatrix:
//---     in the unweighted case:
//---     mBaseMatrix[j][k] =     SUM     (mWork[j].derived * mWork[k].derived)
//---                         [timepoints] 
//---
//---
void CPeriod::prepareDerive ( )
{
  int const pts = mTimeString->getSelectedPoints( );

  void (CPeriod::*my_derive) ( double t, int id) = (SPECIAL == mFitMode) ? &CPeriod::derive : &CPeriod::deriveAll;
  double weight = 1.0;
  
  for ( int co = 0; co < pts; ++co)
  {
    double time, res;
    CTimePoint const & tmp = mTimeString->point( co, &time, &res);
    int id = tmp.getIDName( mWhat);

    res -= activePredict( time, id);
    (this->*my_derive)( time, id);

    if (getUseWeight( ))
    {
      // if we're using weights ...
      weight = tmp.getWeight( );
    }

    for ( int j = 0; j < mVariables; ++j)
    {
      CDeriveData & mWork_j = mWork[j];
      CVector & mBaseMatrix_j = mBaseMatrix[j];

      double const mWork_j_derived___weight = mWork_j.derived * weight;
      mWork_j.weighted += res * mWork_j_derived___weight;

      for ( int k = 0; k <= j; ++k)
      {
//        mBaseMatrix[j][k] += mWork[j].derived * mWork[k].derived * weight;
        mBaseMatrix_j[k] += mWork[k].derived * mWork_j_derived___weight;
      }
    }
  }

  return;
}

void CPeriod::prepareWorkMatrix ( )
{
  for( int j = 0; j < mVariables; ++j)
  {
    CVector & mWorkMatrix_j = mWorkMatrix[j];
    CVector const & mBaseMatrix_j = mBaseMatrix[j];

    for( int k = 0; k < mVariables; ++k)
    {
      mWorkMatrix_j[ k]
        = mBaseMatrix_j[ k]
        / std::sqrt( mBaseMatrix_j[ j] * mBaseMatrix[ k][ k])
      ;
    }

    mWorkMatrix_j[ j] = 1 + mFlamda;
  }
}

void CPeriod::deconvoluteMatrix ( )
{
  for( int j = 0; j < mVariables; ++j)
  {
    CVector const & mWorkMatrix_j = mWorkMatrix[j];
    CVector const & mBaseMatrix_j = mBaseMatrix[j];
    double const & mBaseMatrix_j_j = mBaseMatrix_j[ j];

    CDeriveData const & rev = mWork[ j];

    *(rev.result) = rev.start;

    for( int k = 0; k < mVariables; ++k)
    {
      *(rev.result)
        += mWork[ k].weighted * mWorkMatrix_j[ k]
        / std::sqrt( mBaseMatrix_j_j * mBaseMatrix[ k][ k]);
    }
  }
}

int CPeriod::curFit() {
    double tmp=0;
    double chisqrcp=mChisqr;       // make a copy of mChisqr

    if (isCalculationCanceled()) { return 2; }
	
    prepareMatrices( );            // prepare matrices
    prepareDerive( );              // prepare derivatives
    mBaseMatrix.mirror( );         // mirror base matrix

    do { // main loop
	prepareWorkMatrix();               // prepare work matrix
	double det=mWorkMatrix.invert();   // invert matrix
	//cout <<"det: "<< det << endl;
	if (det==0) { return 1; }          // failed to invert matrix, return
	deconvoluteMatrix();               // deconvolute matrix
	recalc();                          // recalc frequency-combinations
	mChisqr=chiSqr(*mTimeString);      // find out about the best fit
	tmp=chisqrcp+PRCINT2-mChisqr;
	//cout <<"\t\t\t\t\t "<<mChisqr <<" "<< PRCINT2 <<" "<< tmp <<" "<<mFlamda<< endl;
	if (tmp<0) { mFlamda*=10; }        // increase mFlamda
    } while(tmp<0);

    calcSigma();      // calculate errors
    mFlamda*=.1;      // decrease mFlamda for next iteration
    if (mFlamda==0) { // att the limit of precision
      return 3;
    }
    return 0;
}

void CPeriod::calcSigma() {
    for (int i=0; i<mVariables; i++) {
	*(mWork[i].sigma)=std::sqrt(mWorkMatrix[i][i]/mBaseMatrix[i][i]);
    }
}

//---
//--- calculate reduced Chi^2
//---
double CPeriod::chiSqr ( CTimeString & timestring)
{
  double chi = 0.0;
  
  if (getUseWeight( ))
  { // we're using weights
    for( int i = 0; i < timestring.getSelectedPoints( ); ++i)
    {
      double time, amplitude;
      CTimePoint & tpoint = timestring.point( i, &time, &amplitude);
      double const weight = tpoint.getWeight( );
      int const id = tpoint.getIDName( mWhat);
      double const tmp = amplitude - activePredict( time, id);
      chi += weight * tmp * tmp;
    }

    mChisqr_unreduced = chi; 
    return chi / timestring.getWeightSum( );
  }
  else
  { // no weights
    for( int i = 0; i < timestring.getSelectedPoints( ); ++i)
    {
      double time, amplitude;
      CTimePoint & tpoint = timestring.point( i, &time, &amplitude);
//      double const weight = 1.0;
      int const id = tpoint.getIDName( mWhat);
      double const tmp = amplitude - activePredict( time, id);
      chi += tmp * tmp;
    }

    mChisqr_unreduced = chi;
    return chi / timestring.getSelectedPoints( );
  }
}

//---
//--- derive()
//--- calculates the derivative for every parameter[i] at the given time
//--- and puts this value in mWork[i].derived (at least temporarily)
//---
void CPeriod::derive ( double t, int id)
{
  mWork[0].derived = 1; // for the zeropoint

  if (! mBinaryMode)
  {
    // for normal mode
    for( int i = 1; i < mVariables; ++i)
    {
      if (-1 == mWork[i].ID)
      {
        // no amplitude-variations, so this setting is valid for all
        mWork[i].derived = mData[mWork[i].freq].derive( mWork[i].what, t, id);
      }
      else
      {
        // we are working on an amplitude-variation-array
        if (id == mWork[i].ID)
        {
          // this is only for a special ID
          mWork[i].derived = mData[mWork[i].freq].derive( mWork[i].what, t, id);
        }
        else
        {
          // the derived is 0 otherwise
          mWork[i].derived=0;
        }
      }
    }
  } else { // for the binary mode

    // for the periodic-time-shift frequency no amplitude-variation 
    // calculation is available
    int const tmp = mVariables - mBMPeriod.getDegreesOfFreedom( mActiveNames);

    for ( int i = 1; i < tmp; ++i)
    {
      if (-1 == mWork[i].ID)
      {
        // no amplitude-variations, so this setting is valid for all
        mWork[i].derived = mData[mWork[i].freq].deriveBM( mWork[i].what, t, id, mBMPeriod);
      }
    }

    for (int i = tmp; i < mVariables; ++i)
    {
      // for the binary mode frequency
      mWork[i].derived = deriveBM( mWork[i].what, t, id);
    }
  }

  return;
}

//---
//--- deriveAll
//--- this is a faster version when we are doing 
//--- a 'Calculate' (improve amp/pha) 
//--- or an 'Improve All' (improve fre/amp/pha)
//---
void CPeriod::deriveAll ( double t, int id)
{
  mWork[0].derived = 1; // for the zeropoint
    
  for ( int i = 1; i < mVariables; ++i)
  {
    CPeriPoint & p = mData[mWork[i].freq];

    if (mWork[i].what == Fre)
    {
      p.deriveFAP( t, id);
      mWork[i    ].derived = p.getFreDerive( );
      mWork[i + 1].derived = p.getAmpDerive( );
      mWork[i + 2].derived = p.getPhaDerive( );
      i += 2;
    }
    else
    {
      p.deriveAP( t, id);
      mWork[i    ].derived = p.getAmpDerive( );
      mWork[i + 1].derived = p.getPhaDerive( );
      ++i;
    }
  }
}

//---
//--- deriveBM
//--- calculates the derivatives of the parameters for the periodic time shift
//--- frequency by taking into account all other active frequencies
//---
double CPeriod::deriveBM(Parts what, double t, int ID) { 
    double result = 0.0;
    for(int fr=0; fr<getFrequencies(); fr++) {
	// if the frequency is empty or not active then skip it
	if (mData[fr].empty() || !mData[fr].getActive()) { continue; } 
	result+=mData[fr].deriveBM(what, t, ID, mBMPeriod);
    }   
    return result;
}


int CPeriod::countDegreesOfFreedom(int actnames) {
    int v=0;
    for(int fr=0; fr<getFrequencies(); fr++) {
	v+=mData[fr].getDegreesOfFreedom(actnames);
    }
    if (mBinaryMode) { // for binary mode
	v+=mBMPeriod.getDegreesOfFreedom(actnames);
    }
    return v+1;    // +1 for zeropoint
}


//---
//--- searchStart
//--- find a good start value by MonteCarlo-Simulation
//--- (not used, can be deleted)
//---
int CPeriod::searchStart(CTimeString &timestring, int shots) {
    double f_max = mFreqUp;
    double f_low = mFreqLow;
    double a_max = mAmpUp;
    double a_low = mAmpLow;
    double p_max = 1.0;
    double p_low = 0.0;
    double f_delta = f_max-f_low;
    double a_delta = a_max-a_low;
    double p_delta = p_max-p_low;
 
    double frequency  = 0.0;
    double amplitude  = 0.0;
    double phase      = 0.0;
    double best_freq  = 0.0;
    double best_amp   = 0.0;
    double best_phase = 0.0;

    double chi        = 0.0;
    double best_chi   = 9999999;

    setActiveIndex();

    int quit=0;

    for (int i=0; i<shots; i++) { 

	//--- inform user of progress
	quit = setProgressBarValue(int((100.0*i)/shots));
	if (quit!=0) { break; }

	//--- calculate random parameters
	frequency = f_low+f_delta*RANDOM()/RAND_MAX; 
	amplitude = a_low+a_delta*RANDOM()/RAND_MAX; 
	phase     = p_low+p_delta*RANDOM()/RAND_MAX; 

	//--- set values
	mBMPeriod.setFrequency(frequency);    // set the new frequency
	mBMPeriod.setAmplitude(amplitude,-1); // set the new amplitude
	mBMPeriod.setPhase(phase,-1);         // set the new phase
	chi = chiSqr(timestring);             // calculate chisqr

	//--- find out about best values
	if (chi<best_chi && frequency>1e-04 && amplitude>1e-04) {
	    best_chi = chi;
	    best_freq  = frequency; 
	    best_amp   = amplitude; 
	    best_phase = phase; 
	}
    }

    //--- finally set the best values that have been found
    mBMPeriod.setFrequency(best_freq);
    mBMPeriod.setAmplitude(best_amp,-1);
    mBMPeriod.setPhase(best_phase,-1);

    adapt();     // adapt phases and frequencies to fit sensible parameters
    setResiduals(timestring);  // write out the residuals

    return quit;
}
