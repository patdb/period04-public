/*---------------------------------------------------------*
 *  VERSION.h
 *---------------------------------------------------------*/

#ifndef __version_h__
#define __version_h__

extern const char *versiontxt;
extern const char *versiondatetxt;

#endif
