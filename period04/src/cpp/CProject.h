/*----------------------------------------------------------------------------
 *  CProject.h
 *
 *---------------------------------------------------------------------------*/

#ifndef __project_h_
#define __project_h_

#include <string>
#include <iostream>       
using namespace std;      

#include "CTimeString.h"
#include "CPeriod.h"
#include "CFourier.h"
#include "CNoisePoints.h"

//---
//--- line counting 
//---
#include <fstream>
using std::ifstream;

#include <sstream>            
using std::istringstream;
using std::ostringstream;
using std::stringstream;

#include <list>

#include "Period04_java.h" 


class CProject
{
 protected:

    int               projectID;        // ID of this project
    std::string       projectFile;      // path of the project file
    CTimeString       timestring;       // manages timestring data

 protected:
    CPeriod           period;           // manages standard mode frequency data
    CPeriod           periodBM;         // manages binary mode frequency data

    int fitmode;     // indicates the fitting function to use
    int fitactive;   // is there an active fit?
                     // fitactive = 0 ... no active fit
                     // fitactive = 1 ... active fit (normal mode)
                     // fitactive = 2 ... active fit (binary mode)

 protected:
    CFourier          defaultfourier;   // this is the default fourier
    typedef CFourier *PFourier;
    PFourier          fourierActive;
    std::list< PFourier>   fourierList;      // manages fourier data

    std::string       protocoltext;
    std::string       fit_protocol;
    CNoisePoints      noisepoint;       // saves the noise spectrum

    double            freqstep;           // frequency step
    double            mSW2Frequency;      // Spectral Window 2 frequency
    double            mErrorScale;
    int               sel_change;         // selection change state
    int               useFourierWeight;   // use weights for fourier calc.s
    int               changed;            // project change state
    int               fileFormatVersion;  // column format of timestring file
    std::string       standardFileFormat; // user-defined file format, or auto
    std::string       mInputFormatBackUp; // back up of time string input format

    std::string recentFile [10];    // saves up to 10 recent files
    int      numberOfFiles;     // number of recent files (max 10)
    std::string userDirectory;     // saves the user directory
    std::string workingDirectory;  // working directory

    bool expertMode; // is the expert mode active?
    CMatrix  mMonteCarloData;
    CVector  mMonteCarloPhaseData;// saves phase values of the first mc run,
                                  // needed for scaling all phases within 0.5
    DataMode mMoCaInitMode;       // observed or adjusted (for initial LS calc)
    int      mMoCaSimulations;    // number of processes
    int      mFailedSimulations;  // number of failed processes
    CMatrix  mErrorMatrix;
    double  *mMeanParam;          // monte carlo mean parameter
    double  *mMoCaResiduals;      // an array for the residuals
    double   mFreqRes4CompoSearch; // stores the limit for the combination-
                                   // frequency-finder

    string  mFitSignature; // a string that indicates which parameters have 
                           // been variable in the last calculation

 public:

    CProject();                        
    CProject(int id);                  // constructor
    virtual ~CProject();               // destructor

    int getProjectID() const { return projectID; }
    void setProjectID(int id) { projectID=id; }

    bool isExpertMode() { return expertMode; }
    void setExpertMode(bool b) { expertMode=b; changed++; }

    void writeProgramSettings(ostream &ost);
    void readProgramSettings(istream &ist);

    void setUserDirectory ( std::string const & dir) { userDirectory = dir; }
    std::string const & getUserDirectory ( ) { return userDirectory; }

    void setWorkingDirectory ( std::string const & dir) { workingDirectory = dir; }
    std::string const & getWorkingDirectory ( ) { return workingDirectory; }

    std::string loadProject ( std::string const & path); // load a project
    void newProject ( );             // removes all data from the old project 
                                     // and starts a new one
    long saveProject( std::string const & path); // save the project

    long saveProjectAs();            // save the project under a different name
    static bool fileExist ( std::string const & path); // function to see if a file exists
    static bool canWrite ( std::string const & path); // function to see if a file is writeable

    int  hasChanged() const { return changed; } // relevant changes to project?
    void noChanges()  { changed=0; }   // force project into "not-changed" state
    void setChanged() { changed++; }   // force project into "changed" state


    void setProtocolText ( std::string const & txt) { protocoltext = txt; }

    std::string const & getProjectFileName ( ) const { return projectFile; }
    void setProjectFileName ( std::string const & path) { projectFile = path; }

    int getNumberOfRecentFiles ( ) { return numberOfFiles; }
    std::string const & getRecentFile ( int i) const { return recentFile[i]; }
    void addRecentFile ( std::string const & path);
    void removeRecentFile ( std::string const & path);
    void readRecentFiles ( );
    void saveRecentFiles ( );

    //---
    //--- time string related stuff
    //---  

    void loadTimeString
      ( std::string const & name
      , std::string const & fileformat
      , int append = 0
      , bool votable = false
      )
    ;

    void appendTimeString
      ( std::string const & name
      )
    {
// NOTE: I dont think this was meant like that.
//       But I will stay with it for the moment.
//      loadTimeString( name, 1);
      loadTimeString( name, "\001");
    }
    
    void saveTimeString
      ( std::string const & name
      , std::string const & format
      , bool votable = false
      )
    ;

    void saveTimeStringPhase
      ( std::string const & name
      , std::string const & format
      , double frequency = 1.0
      , double zeropoint = 0.0
      , bool votable = false
      )
    ;
    
    void saveTimeStringPhaseBinned
      ( std::string const & name
      , std::string const & format
      , double frequency = 1.0
      , double binSpacing = 0.1
      , DataMode useData = Observed
      , int points = 0
      , double * phase = 0
      , double * amplitude = 0
      , double * sigma = 0
      )
    ;

    void cleanTimeString();

    void setFileFormatString ( std::string const & format) { standardFileFormat = format; }
    std::string const & getFileFormatString ( ) { return standardFileFormat; }
    
    void setInputFormatBackUp ( std::string const & format) { mInputFormatBackUp = format; }
    std::string const & getInputFormatBackUp ( ) { return mInputFormatBackUp; }

    CTimeString &getTimeString() { return timestring;} ////// wurde geloescht!!
// unused
//    std::string const & getTSFileName ( ) const { return timestring.getFileName( ); } 

    int getSelectedPoints ( ) const { return timestring.getSelectedPoints( ); }
    int getTotalPoints ( ) const { return timestring.getTotalPoints(); }

    std::string getStartTime ( );
    std::string getEndTime ( );
    double getTotalTimeLength ( );
 
    double getBaseLine() const       { return timestring.baseLine(); }
    double getTimeAverage() const    { return timestring.getTimeAverage(); }
    double getNyquist() const        { return timestring.nyquist(); }
    double getSpecialNyquist(double t_start, double t_end) const {
	return timestring.nyquist(t_start, t_end);
    }
 
    int  getReverseScale()  {                  // intensity = 0
	return timestring.getReverseScale();   // magnitude = 1
    }

    void setReverseScale ( int scale);
    std::string writeSelectedData ( );
    
    //---
    //--- error calculation
    //---

    void setCalcErrors(bool b) { timestring.setCalcErrors(b); }
    bool isCalcErrors() const  { return timestring.isCalcErrors(); }
    void setShiftTimes(bool b) { timestring.setShiftTimes(b); }
    bool isShiftTimes() const  { return timestring.isShiftTimes(); } 

    int  getMoCaSimulations()      { return mMoCaSimulations; }
    void setMoCaSimulations(int i) { mMoCaSimulations=i; }
    DataMode  getMCInitialCalcMode()      { return mMoCaInitMode; }
    void setMCInitialCalcMode(DataMode i) { mMoCaInitMode=i; }
    

    std::string generalMonteCarloSimulation ( );
    void    saveMonteCarloParameters(int sim);
    void    saveMonteCarloInitialPhases();
    double  adaptMonteCarloPhase(double phase, int i);
    void    calculateErrorMatrix(int variables);
    std::string setMonteCarloFitError(int variables);

    //---
    //--- weight stuff
    //---    
    int  getUseNameWeight() const { return timestring.getUseNameWeight();}
    void setUseNameWeight(int i);
    
    int  getUsePointWeight() const { return timestring.getUsePointWeight();}
    void setUsePointWeight(int i);
	void setUsePointErrors(bool value) { CTimePoint::mUsePointError = value; }

    int  getUseDeviationWeight() const { 
	return timestring.getUseDeviationWeight();}
    void   setUseDeviationWeight(int i);
    double getDeviationCutoff() const  { 
	return timestring.getDeviationCutoff(); }
    void   setDeviationCutoff ( std::string const & c);

    double getError(int i) {
	double weight = timestring[i].getWeight();
	double error  = getErrorScale()/(weight*weight);
	return error;
    }
    void   setErrorScale( std::string const & s);
    double getErrorScale() { return mErrorScale; }

    // time string statistic methods
    void saveStat ( int what, int weighted, std::string const & filename);
    std::string statStrings ( int what, int weighted, int header = 1);
    std::string getNameStatistics ( int attribute, int index, bool useweighted);
    std::string getTimeStatistics ( int attribute, bool unitIsDays);
    void computeMeanTimes ( int attribute);

    int numberOfNames ( int i) { return timestring.numberOfNames( i); }

    // selection
    int  getIDNameSelect(int i,int ID) { return getIDName(i,ID).getSelect(); }
    void setIDNameSelect(int listboxID, int nameID, bool sel)
	{ getIDName(listboxID, nameID).setSelect(sel); }
    int  getIndexNameSelect(int i,int ID)
	{ return getIndexName(i,ID).getSelect(); }
    
  // string
  std::string const & getIDNameStr
    ( int i
    , int ID
    )
  {
    return getIDName( i, ID).getName( );
  }

  void setIDNameStr
    ( int i
    , int ID
    , std::string const & val
    ) 
  {
    getIndexName( i, ID).setName( val);
  }  

  std::string const & getIndexNameStr ( int col, int ID)
  {
    return getIndexName( col, ID).getName( );
  }

    // ID
    int  getIndexNameID(int i,int ID)    { return getIndexName(i,ID).getID(); }

    // weights
    std::string getIDNameWeight ( int i, int ID);

// unused.
//    std::string getIndexNameWeight ( int i, int ID);

    // colors
    char const * getIDNameColor( int i, int ID);

    // methods used by XDialogAdjustData
    CName &getIDName(int i,int ID)     // returns a reference to the name with
	{ return timestring.getIDName(i,ID); } // the id number ID in column i
    CName &getIndexName(int i,int l)   // returns a reference to the name with
	{ return timestring.getIndexName(i,l); }    // the index l in column i

    // heading
    void changeHeading( int i, char const * mName); ///// ersetzen durch :
    void setAttributeName ( int i, std::string const & name);

    std::string const & nameSet ( int i)                  // gives the heading for column i
    { return timestring.nameSet( i); }

    // name items
    void changeName
      ( int i
      , int ID           // changes name & weight for
      , std::string name            // an id of ID in the column i
      , std::string const & weight = "1.0"
      , std::string const & color = ""
      )
    ;

    // relabel datapoint i
    // with the new names
    std::string relabelPoint
      ( int i
      , std::string const & name1
      , std::string const & name2
      , std::string const & name3
      , std::string const & name4
      , bool quiet = false
      )
    ;

    std::string deletePoint ( int i);        // relabels the point to delete
    std::string deletePointsWithinBox      // relabels points within box to delete
      ( double x_start, double y_start,	double x_end, double y_end, int mode)
    ;
      
    void setDeletePointInfo
      ( int what
      , std::string const & name
      )       // set delete labels
    {
      timestring.setDeletePointInfo( what, name);
    }
    
    void getDeletePointInfo
      ( int & what
      , std::string & name
      )    // get Delete labels
    {
      timestring.getDeletePointInfo( what, name);
    }

    void   select(); // selects timepoints using the flags in the different
    // nameIDs as hints also write it out in the protocol

    // renames all currently selected
    //  points in "Column" to "Name"
    void combine( int Column, std::string const & Name);

    void deleteSelectedPoints ( );    // delete selected points

    double getAverage ( ) const            // returns average of all amplitudes
	{ return timestring.average( ); } // of the selected timestring
    void   calcAverage ( )                 // calculates an average amplitude
	{ timestring.calcAverage( ); }    // of the selected timestring

  // does zeropoint adjustment of all the
  // n ids of column what in the array ids
  // if Original is set use original data
  // else the allready shifted data 
  void adjust
    ( int what            
    , DataMode original
    , int n
    , int * ids
    , bool useweight
    )
  ;


    std::string subtractZeroPoint ( );
    std::string makeObservedAdjusted ( ); // copy the observed values to the adjusted
                                    // values for all currently selected points
    std::string makeDataResidualsObserved ( ); // copy residuals (obs) to observed 
    std::string makeAdjResidualsObserved ( );  // copy residuals (adj) to observed 
                                         // for all points of the timestring
    std::string makeCalculatedObserved ( );    // copy calculated to observed
    std::string makeAdjustedObserved ( );      // copy adjusted to observed

    void makeCalculatedSimulatedBase ( ) {  // for monte carlo
	timestring.makeCalculatedSimulatedBase(); 
    }
    void makeSimulatedBaseCalculated() {  // for monte carlo
	timestring.makeSimulatedBaseCalculated(); 
    }
    void   calcWeights()               // recalculates the composed weights
	{ timestring.calcWeights(); }  // for all the currently selected points

    // subdivides the currently
    // selected data
    void subdivide
      ( double timegap = 0.3
      , int column = 0
      , int useCounter = 0
      , std::string const & prefix = "JD"
      , int places = 0
      )
    ;
    
    // subdivides the currently
    // selected data
    void subdivide
      ( double start = 0
      , double interval = 10
      , int column = 0
      , int useCounter = 0
      , std::string const & prefix = "JD"
      , int places = 0
      )
    ;

    void refit ( double zeropoint);
    
 private:
    void readTimestring ( istream & file);
    void writeTimestring ( ostream & file);

 public:

    //---
    //--- period related stuff
    //---
    
    CPeriod &getPeriod()   { return period;}
    CPeriod &getBMPeriod() { return periodBM;}
    CPeriod &getActivePeriod() {
	if (fitmode==0) { return period; }   // normal mode
	else            { return periodBM; } // binary mode;
    }

    void loadPeriod( std::string const & path, int mode = 0); // loads a table of frequencies
    void savePeriod( std::string const & path, int mode = 0, bool isUseLatex = false); // saves the table of frequencies
    std::string getFrequenciesForPrinting ( );

    std::string getAnalyticalErrors ( ) const;
    std::string const & getFitError ( ) const;

    void setFitError ( );
    void saveFitError ( std::string const & path);  // saves the table of errors
    void copyPeriods(bool toPTS); // copy the set of frequencies from normal mode 
                            // to the periodic time shift mode
    void cleanPeriod(int id); // clean the a specific frequency in active mode
    void cleanPeriod();     // clean the table of frequencies of active mode
    void cleanAllPeriods(); // cleans the frequency tables for both modes

    void cleanSigmas() {    // cleans the calculated errors
	int i;
	for (i=0; i<period.getFrequencies(); i++) {
	    period[i].cleanSigmas();
	}
	periodBM.cleanBMSigmas();
	for (i=0; i<periodBM.getFrequencies(); i++) {
	    periodBM[i].cleanSigmas();
	}
    }                  

    // gives the number of
    // currently active frequencies
    int getActiveFrequencies() const
    {
      return period.getActiveFrequencies( );
    }

    std::string getActiveFrequenciesString ( ) const;
    std::string getBMActiveFrequenciesString ( ) const;

    int getBMActiveFrequencies ( ) const
    {
      return periodBM.getActiveFrequencies( );
    }


    //--- get/set the current total number of frequencies
    //---
    int  getTotalFrequencies() const { return period.getFrequencies(); }
    void setTotalFrequencies(int i)  { period.resizeLeast(i); }

    //--- get/set the current total number of frequencies for binary mode
    //---
    int  getBMTotalFrequencies() const { return periodBM.getFrequencies(); }
    void setBMTotalFrequencies(int i)  { periodBM.resizeLeast(i); }
    
    double getFirstFrequency();
    int    getFirstFrequencyNumber();
    
    std::string getZeropoint() const;          // returns the calculated solution
                                           // for the zeropoint
    std::string getBMZeropoint() const;        // returns the calculated solution
                                           // for the bm zeropoint
    std::string getZeropointError ( ) const;     // returns the associated error
    std::string getResiduals() const;          // returns the residuals of the
                                           // last calculation
    std::string getBMResiduals() const;        // returns the residuals of the
                                           // last bm calculation
    double getResidualsValue() const {     // dito
	switch (fitmode) {
	    case 0: return period.getResiduals();
	    case 1: return periodBM.getResiduals();
	    default: return 0.0;
	}
    }

    // returns the number of loops that where
    // necessary to come to the last solution
    int getLoops() const { return period.getIterations(); }

    DataMode getPeriodUseData() const { return period.getUseData(); }

    void setPeriodUseWeight(int flag) { 
	switch (fitmode) {
	    case 0: period.setUseWeight(flag);   break; // normal mode
	    case 1: periodBM.setUseWeight(flag); break; // binary mode
	}
	changed++; 
    }
    int  getPeriodUseWeight() const { 
	switch (fitmode) {
	    case 0: return period.getUseWeight();    // normal mode
	    case 1: return periodBM.getUseWeight();  // binary mode
	    default: return 0;
	}
    }

    //--- get/set frequencies selected
    //---
    int  getActive(int i) const      { return period[i].getActive(); }
    void setActive(int i, int act)   { period[i].setActive(act); changed++; }
    int  getBMActive(int i) const    { return periodBM[i].getActive(); }
    void setBMActive(int i, int act) { periodBM[i].setActive(act); changed++; }

    std::string getNumber(int i) const;   
    std::string getBMNumber(int i) const;   
    std::string getFrequency(int i) const;  
    std::string setFrequency(int i, std::string const & f);  
    std::string getBMFrequency(int i) const;  
    std::string setBMFrequency(int i, std::string const & f);  
    std::string getComposite(int i) const { return period[i].getCompositeString();}
    std::string getBMComposite(int i) const { return periodBM[i].getCompositeString();}

    std::string getAmplitude(int i, int what=-1) const; 
    double  getAmplitudeValue(int i, int what=-1) const;
    void    setAmplitude(int i, std::string const & a); 
    std::string getBMAmplitude(int i, int what=-1) const; 
    double  getBMAmplitudeValue(int i, int what=-1) const;
    void    setBMAmplitude(int i, std::string const & a); 

    std::string getPhase(int i, int what=-1) const;        
    void    setPhase(int i, std::string const & p);  
    std::string getBMPhase(int i, int what=-1) const;        
    void    setBMPhase(int i, std::string const & p);  

	bool getCompoUseFreqValue(int i) const;
	void setCompoUseFreqValue(int i, bool val);
	void setCompoUseFreqValues(bool val);
	bool getBMCompoUseFreqValue(int i) const;
	void setBMCompoUseFreqValue(int i, bool val);
	void setBMCompoUseFreqValues(bool val);

    void checkActive()   { period.checkActive(); changed++; }
    void checkBMActive() { periodBM.checkActive(); changed++; }
    int  getFitMode()         { return fitmode; }
    void setFitMode(int mode) { fitmode = mode;	setFitError(); }

    std::string calculatePeriod(DataMode mode);
    std::string improvePeriod(DataMode mode, bool isMonteCarlo=false);
    std::string improveSpecialPeriod(DataMode mode);
    std::string improvePTS(DataMode mode);
    std::string calculateAmpVar(DataMode datamode, CalcMode calcmode,
			    int attribute, int* freqs, int selected, 
			    bool isMonteCarlo=false);
    std::string calculateAmpVarPeriod(int n, int *IDs, int what, CalcMode mode);
    std::string calculateMoCaAmpVarPeriod(int n, int *IDs, int what, CalcMode mode);
    std::string searchPTSStartValues(DataMode mode, int shots);

    std::string predict(double time);            
    std::string showEpoch(double time, int mode);  

    void setMCRange(double f_low, double f_up, double a_low, double a_up) {
	periodBM.setMCRange(f_low, f_up, a_low, a_up);
    }

    void createArtificialData
      ( std::string const & outfile, double from, double to
      , double step, double leading, bool append
      )
    ;
    
    void createArtificialData
      ( std::string const & outfile, std::string const & infile, bool isRange
      , double step, double leading, bool append
      )
    ;

    void    setFrequencyAdjustment(double step);  
    std::string getFrequencyAdjustment() const ;       
    void    setSW2Frequency(double freq) { mSW2Frequency=freq; }  
    
    void setUseWeightFlags();
    std::string writeFrequenciesTabulated ( bool selected, bool heading = true);
    std::string writeErrorsTabulated(bool selected);

 protected:
    std::string generalCalcPeriod(DataMode mode);        // alle drei waren virt.
    int  getPeriodSelection();
    int *getAmpVarData(int *freqs, int* what, CalcMode *mode);
    void setFitSignature();
    
 public:

    //---
    //--- fourier related stuff
    //---

    void loadFourier ( std::string const & path);
    void saveFourier ( CFourier const & fourier, std::string const & path);
    void saveAllFourier(std::string const & directory, bool usePrefix, std::string const & prefix);
    void saveSpecialFourier(std::string const & directory, int *selectedindices, 
			    int selected, bool usePrefix, std::string const & prefix);
    void cleanFourier();

    CFourier *getFourierActive()           {return fourierActive;             }

    int getFourierEntries ( ) const
    {
      return fourierList.size( );
    }
    
    std::string getFourierTitle ( int i) const
    {
      return getFourierEntry( i)->getTitle( );
    }

    CFourier * getFourierEntry ( int i) const
    {
      std::list< PFourier>::const_iterator iter = fourierList.begin( );

      for
        ( int n = 0
        ; (iter != fourierList.end( )) && (n < i)
        ; ++iter, ++n
        )
      {
      }

      return (*iter);
    }
    
    void activateFourier ( int i)
    {
      if (fourierList.size( ) != 0)
      {
        fourierActive = getFourierEntry( i);
      }
      else
      {
        fourierActive = &defaultfourier;
      }
    }

    int    getFourierActiveListID(); // for plot purposes - returns fourierlist
                                     // index of the active fourier calculation
    void   deleteActiveFourier();
    void renameActiveFourier( std::string const & newName);
    std::string getActiveFourierProperties ( );
    
    char*  getStepQualityLabel(StepQuality mode);    
    double getStepRate(StepQuality qstep, double step) const;    
    
    void   setFourierUseWeight(int flag) { useFourierWeight=flag;}
    int    getFourierUseWeight()         { return useFourierWeight;}

    std::string checkFourierTitle ( std::string title);
    
    // performs a fourier-calculation in the range [from:to]
    // using an nominal steping of step
    // (if negative the real value is computed),
    // Mode as indicator for what data should be used,
    // Compact as an indicator of what compaction-routines should be used
    // and weight as an indicator if the weights for dthe data should be used
    void calculateFourier
      ( std::string const & title
      , double from = 0
      , double to = 0
      , StepQuality qstep = HIGH
      , double step = 0.1
      , DataMode mode = Observed
      , CompactMode compact = PEAKS_ONLY
      , int weight = 1
      , double zeropoint = 0
      )
    ;

    // performs a noise calculation for the frequency point
    // summing up the spectra in box of size box centered on this point
    // using an nominal steping of step,
    // Mode as indicator for what data should be used,
    // and weight as an indicator if the weights for the data should be used
    double calculateNoise(double point=10, double box=10, 
			  StepQuality stepq=HIGH, double step=0.1,
			  DataMode mode=Observed, double zeropoint=0);

    // performs a noise spectra calculation in the range [from:to]
    // summing up the spectra in box of size box centered on this point
    // using an nominal steping of step,
    // Mode as indicator for what data should be used,
    // and weight as an indicator if the weights for the data should be used
    int calculateNoiseSpectrum(double from, double to, double spacing,
			       double boxsize, StepQuality stepq, double step,
			       DataMode mode, double zero);
    
    int addFrequencyPeak();
    int addFrequencyPeak(double fr, double amp);

    double getSubPeakFrequency(double fr, double binscale) { return getFourierActive()->getSubPeak(fr, binscale).getFrequency(); }
    double getSubPeakAmplitude(double fr, double binscale) { return getFourierActive()->getSubPeak(fr, binscale).getAmplitude(0); }
    
    string isPeakComposition(double freq);
    double const getFreqResForCompoSearch() {return mFreqRes4CompoSearch; }
    void setFreqResForCompoSearch(double df) { mFreqRes4CompoSearch=df; }
    double getFrequencyResolution();
    
    // fourier noise spectrum
    int getNoisePoints() {                // retrieve number of noise points
	return noisepoint.getSize();
    }
    double getNoiseFrequency(int index) { // retrieve frequency[index]
	return noisepoint.getFrequency(index);
    }
    double getNoiseAmplitude(int index) { // retrieve noise[index]
	return noisepoint.getNoise(index);
    }

 private:

    void readFourier(istream & file);
    void writeFourier(ostream & file);
    
 protected:
    //---
    //--- protocol methods
    //---
    static std::string getDate ( );

 private:
    
    void cleanAll ( );
    std::string readProtocol ( istream & file);
    void writeProtocolToFile ( ostream & file);
    void readPeriod ( istream & file);
    void writePeriod ( ostream & file);
    void readBMPeriod ( istream & file);
    void writeBMPeriod ( ostream & file);

    // preferences
    void writeDefaultPreferences ( );

    void writePreferences
      ( std::string const & prog_mode
      , std::string const & working_dir
      , std::string const & max_freq
      , std::string const & freq_low
      , std::string const & freq_high
      , std::string const & compact
      , std::string const & alias_step
      , std::string const & scale
      , std::string const & name1
      , std::string const & name2
      , std::string const & name3
      , std::string const & name4
      , std::string const & inputformat
      )
    ;

    void setDefaultSettings();


 public:

    void readPreferences();
    void cleanSpecial(bool fourier, bool periods, bool timestring);

 protected:

    static double doubleFromString ( std::string const & str);
    static int    integerFromString ( std::string const & str);
};

//---
//--- this method returns a pointer to the current project,
//--- see file Period04.cpp for the implementation
//---
CProject* getCurrentProject();

#endif


