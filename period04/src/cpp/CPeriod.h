
#ifndef __period_h__
#define __period_h__

#include <iostream>
using std::cout;
using std::endl;

#include <string>
using std::string;

//#include <cmath>
#include "CMatrix.h"
#include "CTimeString.h"
#include "CPeriPoint.h"
#include "format.h"


enum FittingMode { AMP_PHA, FRE_AMP_PHA, SPECIAL };

//---
//--- CDeriveData
//--- a structure that holds information on the fitting parameters
//---
struct CDeriveData
{
    double derived;
    double weighted;
    double start;
    double *result;
    double *sigma;
    int    freq;
    Parts  what;
    int    ID;
};

/**
 *  CPeriod
 */
class CPeriod
{
 private:

    FittingMode  mFitMode;      // indicates the selection mode for this fit

    CMatrix      mBaseMatrix;   // matrices for least-squares routine
    CMatrix      mWorkMatrix;
    CDeriveData *mWork;         // array of parameters to be improved

    int    mVariables;          // number of parameters
    int    mVariablesCopy;      // a copy of the number of parameters
                                // will not be set to zero after calculations
    int    mIterations;         // number of iterations performed
    int    mMaxIterations;      // maximum number of iterations accepted
    double mFlamda;             // proportion of gradient search included
    double mFlamda_def;         // initial value of mFlamda
    double mFlamda_ratio;       // = mFlamda(after fit)/mFlamda(before fit)

    double mZeropoint;
    double mZeropointSigma;
    double mChisqr;
    double mChisqr_unreduced;

    CTimeString     *mTimeString;
    CPeriPoint      *mData;         // array of periods
    CSimplePeriPoint mBMPeriod;     // binary mode period

    double           mZeropoint_store;  // variables to store the parameters
    double           mChisqr_store;     // while a monte carlo simulation
    CPeriPoint      *mData_store;       // is being calculated
    CSimplePeriPoint mBMPeriod_store;

    double           mZeropoint_tmp;  // variables to store the parameters
    double           mChisqr_tmp;     // while a monte carlo simulation
    CPeriPoint      *mData_tmp;       // is being calculated
    CSimplePeriPoint mBMPeriod_tmp;

    int mFrequencies;           // number of frequencies
    int mMaximumFrequencies;    // maximum number of frequencies
    int mActive;                // number of active frequencies
    int *mActiveIndex;          // array of indices of active frequencies

    std::string  mFileName;         // filename of frequency file

    int      mUseWeight;        // use weights (1) or not (0)
    DataMode mUseData;          // datamode to use
    int      mWhat;             // attribute to use
    int      mActiveNames;      // ...   //min
    bool     mBinaryMode;       // defines the fitting function to be used
    bool     mCheckCondNumber;  // check condition number of the error-matrix
    char*    mProgressMessage;  // progress bar message for calculations

    // MC range:
    double mFreqLow;
    double mFreqUp;
    double mAmpLow;
    double mAmpUp;


 public:

    CPeriod(int size=2560);
    ~CPeriod();

    // return CPeriPoint of the frequency with index i
    CPeriPoint &operator[](int i) const;

    void        setFittingMode(FittingMode mode) { mFitMode=mode; }
    FittingMode getFittingMode()                 { return mFitMode; }

    int  findEmpty() const;     // find the first empty CPeriPoint
    void add();
    void remove();
    void removeOne();
    void removeAll();           // remove all frequencies
    void resizeLeast(int i);    // resize the CPeriPoint array

    void setUseWeight(int flag) { mUseWeight=flag; }
    int  getUseWeight() const { return mUseWeight; }

    void     setUseData(DataMode mode) { mUseData=mode; }
    DataMode getUseData() const { return mUseData; }

    int  getFrequencies() const { return mFrequencies; }
    int  getActiveFrequencies() const;

    void setFrequency(int i, double f)
	{(*this)[i].setFrequency(f); recalc();}
    void setCompositeString(int i, const char * f)
	{(*this)[i].setCompositeString(f);recalc();}

    void   setBMMode(bool value) { mBinaryMode=value; }
    bool   getBMMode() const { return mBinaryMode; }
    CSimplePeriPoint &getBMPeriod()  { return mBMPeriod; }

    void   setBMFrequency(double f)         { mBMPeriod.setFrequency(f); }
    void   setBMAmplitude(double a, int ID) { mBMPeriod.setAmplitude(a,ID); }
    void   setBMPhase(double p, int ID)     { mBMPeriod.setPhase(p,ID); }
    double getBMFrequency() const       { return mBMPeriod.getFrequency(); }
    double getBMAmplitude(int ID) const { return mBMPeriod.getAmplitude(ID);}
    double getBMPhase(int ID) const     { return mBMPeriod.getPhase(ID);  }
    double getBMFrequencySigma() const { return mBMPeriod.getFrequencySigma();}
    double getBMAmplitudeSigma() const { return mBMPeriod.getAmplitudeSigma(-1); }
    double getBMPhaseSigma() const     { return mBMPeriod.getPhaseSigma(-1); }
    void   setBMActive(int value)      { mBMPeriod.setActive(value); }
    int    getBMActive() const         { return mBMPeriod.getActive(); }
    void   cleanBMPeriod() {
	CSimplePeriPoint empty;
	mBMPeriod = empty;
	setBMDefaultValues();
    }
    void   cleanBMSigmas() {
	mBMPeriod.cleanSigmas();
    }
    void   setBMDefaultValues() {
	mBMPeriod.setFrequency(0.0);
	mBMPeriod.setAmplitude(0.0, -1);
	mBMPeriod.setPhase(0.0, -1);
    }
    double deriveBM(Parts what, double t, int ID);
    void   deriveAll(double t, int id);

    double getZeropoint() const { return mZeropoint; }
    double getZeropointSigma() const { return mZeropointSigma; }
    void   setZeropoint(double z) { mZeropoint=z; }

    double getResiduals()   const { return std::sqrt(mChisqr); }
    double getChiSqr()      const { return mChisqr; }
    double getChiSqrUnred() const { return mChisqr_unreduced; }
    void   setResiduals(double z) { mChisqr=z*z; }

    // this is for the errors
    int  getNumberOfParameters() const { return mVariablesCopy; }
    void storeParameters(int i=0);
    void restoreParameters(int i=0);
    void setCheckCondNumber(bool b) { mCheckCondNumber=b; }

    int getIterations() const { return mIterations; }
    int maxIterations() const { return mMaxIterations;}

    int  getUseID() const { return mWhat; }  // returns id of attribute
    void setUseID(int i) { mWhat=i; }

    // what parts to calculate
    void selectDefault();
    void selectBMDefault();
    void selectAll();
    void selectPTS();
    void storeSelection();
    void restoreSelection();

    void setPointers(int i=-1);
    void   resetLambda()    { mFlamda=mFlamda_def; }
    double getLambdaRatio() { return mFlamda_ratio; }

    // calculation-functions
    int  calc(CTimeString &times, bool checkMaxIter=true);
    int  calcLoop(CTimeString &times, bool checkMaxIter=true);
    int  calcGradLoop(CTimeString &times);
    void setResiduals(CTimeString &times);
    int  searchStart(CTimeString &timestring, int shots);

    //---
    //--- predict magnitude value at a certain time
    //---
    inline double predict ( double t, int ID)
    {
      double val = getZeropoint( );

      if (mBinaryMode)
      { // for binary mode
        for( int i = 0; i < getFrequencies( ); ++i)
        {
          if (mData[i].getActive( ))
          {
            val += mData[i].calcBM( t, ID, mBMPeriod);
          }
        }
      }
      else
      { // for normal mode
        for( int i = 0; i < getFrequencies( ); ++i)
        {
          if (mData[i].getActive( ))
          {
            val += mData[i].calc( t, ID);
          }
        }
      }

      return val;
    }

    void setActiveIndex();

    void checkActive() {
	for (int i=0;i<getFrequencies();i++) {
	    mData[i].checkActive();
	}
    }

    //---
    //--- predict magnitude value at a certain time
    //--- only using active frequencies
    //---
    double activePredict( double t, int ID)
    {
      double val = getZeropoint( );

      if (mBinaryMode)
      {
        // for binary mode
        for( int i = 0; i < mActive; ++i)
        {
          val += mData[mActiveIndex[i]].calcBM( t, ID, mBMPeriod);
        }
      }
      else
      {
        // for normal mode
        for( int i = 0; i < mActive; ++i)
        {
          val += mData[mActiveIndex[i]].calc( t, ID);
        }
      }

      return val;
    }

    // create artificial data for a given time range
    void createArtificialData ( std::string const & outfile, double from, double to,
			      double step, double leading, bool append);
    // create artificial data for given time ranges read from file "infile"
      void createArtificialData( std::string const & outfile, std::string const & infile, bool isRange,
			      double step, double leading, bool append);

    void load ( std::string const & path);
    void save ( std::string const & path, bool isUseLatex);
    string getFrequencyStringForPrinting ( );

    inline std::string const & getFileName ( ) const { return mFileName; }

    friend ostream& operator<<(ostream& s,const CPeriod &t);

    void writePeriods(ostream& ost, CPeriod &t); ///////// anstatt operator<<
    void readPeriods(istream& ist, CPeriod &t, char delim='\n');  //////// anstatt operator>>

    ostream &additionalOut(ostream &s);
    std::string writeSelected ( );

    void setMCRange(double f_low, double f_up, double a_low, double a_up) {
	mFreqLow = f_low;
	mFreqUp = f_up;
	mAmpLow = a_low;
	mAmpUp = a_up;
    }
    double getMCFreqLow() { return mFreqLow; }
    double getMCFreqUp()  { return mFreqUp; }
    double getMCAmpLow()  { return mAmpLow; }
    double getMCAmpUp()   { return mAmpUp; }

    void setProgressMessage(char *text) { // set the progress bar message
	mProgressMessage = text;
    }

 private:

    void deconvoluteMatrix();
    void prepareWorkMatrix();
    void prepareDerive();
    void prepareMatrices();

    int  countDegreesOfFreedom(int activeFreqs);
    void recalc();
    void checkPointers();
    void prepare(CTimeString &times);
    void terminate();

    int    curFit();
    int    gradSearch(double fract);
    void   derive(double t, int id);
    double chiSqr(CTimeString & tmp);
    void   calcGradient(double &chisq_1, double &chisq_2, double fract);
    void   calcSigma();
    void   adapt();
    void   fixNumbers();
};

#endif


