/*----------------------------------------------------------------------------
 *  CTimeString.cpp
 *               
 *---------------------------------------------------------------------------*/

#define PROGRESS 1

#include "CTimeString.h"

#if PROGRESS
#include "progress_filter.h"
#endif

#include <fstream>
using std::ofstream;

#include <stdlib.h>        // qsort
#include <ctype.h>

#include <iostream>      
using std::istream;
using std::cout;
using std::endl;

#include <sstream>         // string streams 
using std::ostringstream;

#include <string.h>        // only for  strcmp

#include "mt19937ar.h"     // include the MT random number generator
#include "Period04_java.h" // setProgressbarValue 
#include "CProject.h"

#ifdef WIN32
#include <float.h>
#define ISNAN _isnan
#define ISINF !_finite
#else
#define ISNAN isnan
#define ISINF isinf
#endif


CTimeString::CTimeString
  (
  )
  : mSelection( 0)
  , mSelected( 0)
  , mMode( Observed)
  , mCalcErrors( false)
  , mShiftTimes( false)
{
  setInputFormat( "to1234");
  setOutputFormat( "to1234");
  setLineSeparator( '\n');
  initNameSets( );
  clean( );
}

CTimeString::CTimeString
  ( std::string const & name
  , std::string const & from
  )
  : mSelection( 0)
  , mSelected( 0)
  , mMode( Observed)
  , mCalcErrors( false)
  , mShiftTimes( false)
{
  setInputFormat( from);
  setOutputFormat( "to1234");
  setLineSeparator( '\n');
  initNameSets( );
  clean( );
  load( name);
  selectAll( );
}

void CTimeString::initNameSets ( )
{
  mNameSet[0] = "";
  mNameSet[1] = "";
  mNameSet[2] = "";
  mNameSet[3] = "";
}

CTimeString::~CTimeString ( )
{
  clean( );
}

void CTimeString::clean ( )
{
    setReverseScale(1);  // the default is magnitudes, so reverse scale
    mTimeArray.clear( );  // clean the arrays
    selectClear();
    for (int i=0;i<4;i++) {
	mNames[i].cleanUp();
    }
    // set the default-names
    changeNameSet(0,"Date");
    changeNameSet(1,"Observatory");
    changeNameSet(2,"Observer");
    changeNameSet(3,"Other");
    mWeightSum=0;
    mFileName="";
    // reset the index for loading files...
    CTimePoint tmp(1);
    // set defaults for deleted
    mDeleteWhat=3;
    mDeleteName="deleted";
    mPointWeight=0;
    mNameWeight=0;
    mDeviationWeight=0;
    mDeviationCutoff=100000;
	mInputFormat="t"; // previously commented with "to"
}

void CTimeString::selectClear ( )
{
  delete [] mSelection;

  mSelection = 0;
  mSelected = 0;
  mWeightSum = 0;
}

int CTimeString::isSelected ( SelectedTime time)
{
  // this is the one to change selection-beaviour
  for ( int i = 0; i < 4; ++i)
  {
    if (mNames[i].getIDName( time->getIDName( i)).getSelect( ) == 0)
    {
      // not selected in all
      return 0;
    }
  }

  return 1;
}

int CTimeString::createName
  ( int column
  , int & useCounter
  , double time
  , std::string const & prefix
  , int places
  )
{
  char txt [256];
  
  if (0 == useCounter)
  { 
    char format [256];

    sprintf( format, "%%-.%if", places);
    sprintf( txt, format, time);
  }
  else
  { 
    sprintf( txt, "%i", useCounter);
    ++useCounter;
  }

  std::string filename = prefix + txt;
  int ID = getID( column, filename);
  getIDName( column, ID).setSelect( 1);

  return ID;
}

//---
//--- change the name of a substring
//---
void CTimeString::relabelName
  ( int column
  , int ID
  , double time
  , std::string const & prefix
  , int places
  )
{
  char txt[256]; char format[256];

  sprintf( format,"%%-.%if", places);   // format time
  sprintf( txt, format, time);

  std::string filename = prefix + txt;
  CName & tmp = getIDName( column, ID);  // get the old substring
  tmp.setName( filename);              // set the new name
  tmp.setSelect( 1);
}

//---
//--- subdivide by gaps
//---
void CTimeString::subdivide
  ( double timegap
  , int column
  , int useCounter
  , std::string const & prefix
  , int places
  )
{
    checkBounds(column);
    double timesum = 0.0;
    int    points   = 0; 

    //--- create the first name
    double lasttime = (*this)[0].getTime();
    int ID = createName(column,useCounter,lasttime,prefix,places);

    //--- now set all names
    for (int i=0;i<mSelected;i++) {
	double time=(*this)[i].getTime();
	if (time-lasttime>timegap) {
	    //--- include the average time in the name of the substring
	    //--- that is complete now
	    if (!useCounter) {
		relabelName(column,ID,timesum/points,prefix,places);
	    }
	    //--- reset the variables necessary for average time calculation
	    //--- of the new substring
	    timesum = 0.0;
	    points   = 0;
	    //--- create a name for the empty new substring
	    ID = createName(column,useCounter,time,prefix,places);
	}
	(*this)[i].setIDName(column,ID);  // add time point to substring
	timesum+=time; points++;          // update the sum of times
	lasttime=time;                    // new reference time
    }
    //--- for the last substring the average time has not yet been included
    //--- so do it now ...
    if (!useCounter) { relabelName(column,ID,timesum/points,prefix,places); }
    select();            // now reselect all
    garbageCollect();    // and now some GarbageCollection
}

//---
//--- subdivide by fixed intervals of time
//---
void CTimeString::subdivide
  ( double start
  , double interval
  , int column
  , int useCounter
  , std::string const & prefix
  , int places
  )
{
    checkBounds(column);
    double timesum = 0.0;
    int    points   = 0; 

    //--- create the first name
    double lasttime = start;//(*this)[0].getTime();
    int ID = createName(column,useCounter,lasttime,prefix,places);
		
    //--- now set all names
    for (int i=0;i<mSelected;i++) {
	  double time=(*this)[i].getTime();
	  if (time<start) {continue;}
	  if (time-lasttime>interval) {
	    //--- include the average time in the name of the substring
	    //--- that is complete now
	    if (!useCounter) {
		  relabelName(column,ID,timesum/points,prefix,places);
	    }
	    //--- reset the variables necessary for average time calculation
	    //--- of the new substring
	    timesum = 0.0;
	    points   = 0;
	    //--- create a name for the empty new substring
	    ID = createName(column,useCounter,time,prefix,places);
		while (time-lasttime>interval) {
		  lasttime+=interval;                // new reference time
		}
	  }
	  (*this)[i].setIDName(column,ID);  // add time point to substring
	  timesum+=time; points++;          // update the sum of times
    }
    //--- for the last substring the average time has not yet been included
    //--- so do it now ...
    if (!useCounter) { relabelName(column,ID,timesum/points,prefix,places); }
    select();            // now reselect all
    garbageCollect();    // and now some GarbageCollection
}


void CTimeString::combine ( int column, std::string const & name)
{
  checkBounds( column);
  int ID = getID( column, name);
  getIDName( column, ID).setSelect( 1);

  // now set all names
  for ( int i = 0; i < mSelected; ++i)
  {
    (*this)[i].setIDName( column, ID);
  }

  // now reselect all
  select( );
  // and now some GarbageCollection
  garbageCollect( );
}


void CTimeString::deleteSelection ( )
{
  if (mTimeArray.size( ) == 0)
  {          // check range
    return;
  }

  if (mTimeArray.size( ) == getSelectedPoints( ))
  {
    clean( );
    return;
  }

  for
    ( CTimeArray::iterator iter = mTimeArray.begin( )
	; iter != mTimeArray.end( ) 
	;
    )
  {
    if (isSelected( &(iter->second)))
      {
	// Since the call to erase invalidates the iterator, we have to 
	// increment iter before calling erase. Note that in the following
	// line, while the iterator is incremented before calling erase,
	// the old value of iter is passed to erase.
	mTimeArray.erase( iter++);  
      } 
    else
      {
	++iter;
      }
  }

  select( );            // now reselect all
  garbageCollect( );    // and now some GarbageCollection
}


void CTimeString::setWeight(SelectedTime time) {
    // this is the one to change to set weights accordingly
    double weight=1.0;
    //--- calculate weight
    for (int i=0;i<4;i++) {
	if (mNameWeight & (2<<i)) {
	    weight*=mNames[i].getIDName(time->getIDName(i)).getWeight();
	}
    }
    double finalweight;
    //--- check if user-weights should be used
    if (mNameWeight)  { finalweight=weight; }
    else              { finalweight=1.0; }
    //--- check if weights for specific point should be used
    if (mPointWeight) {	finalweight*=time->getPointWeight(); }
    //--- now set the deviation weight
    if (mDeviationWeight) {
	switch (mMode) {
	    case Observed: {
		finalweight*=time->getResidualWeight(1,mDeviationCutoff);
		break;
	    }
	    case Adjusted: {
		finalweight*=time->getResidualWeight(2,mDeviationCutoff);
		break;
	    }
	    case Simulated: {
		finalweight*=time->getResidualWeight(3,mDeviationCutoff);
		break;
	    }
	    default: // ignore all others
		break;
	}
    }
    // now set weights
    time->setWeight(finalweight);
}

void CTimeString::garbageCollect() {
    int i;
    for (i=0;i<4;i++) {
	mNames[i].storeSelection();    // make a copy of the current selection
    }
    selectAll();                       // select all
    calcAverage();                     // calc average
    for (i=0;i<4;i++) {
	mNames[i].garbageCollect();    // make GarbageCollection
    }
    for (i=0;i<4;i++) {
	mNames[i].restoreSelection();  // restore the current selection
    }
    select();                          // select data
}

void CTimeString::select ( )
{
  // clean Selection
  selectClear();

  // count selected timepoints first
  for
    ( CTimeArray::iterator iter = mTimeArray.begin( )
    ; iter != mTimeArray.end( )
    ; ++iter
    )
  {
    if (isSelected( &(iter->second)))
    {
      // this entry is selected, so increment total
      ++mSelected;
    }
  }

  if (0 == mSelected)
  {                 // there are no selected points at all!     
    return;
  }

  // now save the selected time points into mSelection
  mSelection = new SelectedTime[mSelected];
  int j = 0;

  for
    ( CTimeArray::iterator iter = mTimeArray.begin( )
    ; iter != mTimeArray.end( )
    ; ++iter
    )
  {
    if (isSelected( &(iter->second)))
    {
      // this entry is selected, so include it
      mSelection[j] = &(iter->second);
      // Set the relevant Weight
      setWeight( &(iter->second));
      ++j;
    }
  }

  calcAverage( );
  calcWeights( );
}

void CTimeString::calcWeights() {
    mWeightSum=0;
    // iterate over all selected datapoints
    for (int i=0;i<mSelected;i++) {
	setWeight(mSelection[i]);
	// add up weights
	mWeightSum+=mSelection[i]->getWeight();
    }
}

void CTimeString::selectAll()
{
  // for all entries..
  for (int i=0;i<4;i++)
    {
      for (int j=0;j<mNames[i].getNumberOfNames();j++)
	{
	  mNames[i].getIndexName(j).setSelect(1);
	}
    }
  // now select all
  select();
}

CTimePoint &CTimeString::operator[](int i) const {
    return *mSelection[i];
}

void CTimeString::setInputFormat
  ( std::string const & tmp
  )
{
  mInputFormat = tmp;
  // here should go some sort of check if tmp holds true value
}

void CTimeString::setOutputFormat
  ( std::string const & tmp
  )
{
  mOutputFormat = tmp;
  // here should go some sort of check if tmp holds true value
}

void CTimeString::setUseVOTableFormat(bool const val) {
    mVOTableFormat=val;
}

void CTimeString::save
  ( std::string const & name
  )
{
  std::ofstream file( name.c_str( ));
    
    if (!file) { return; } // file error
    file.precision(12);    // set output precision
	if (mVOTableFormat) {
	  // write votable header
	  file<< "<?xml version='1.0'?>\n<VOTABLE version=\"1.1\"\n>"
		  << "<!--\n !  VOTable written by Period04\n"
		  << " !  http://www.univie.ac.at/tops/Period04/\n"
		  << " !-->\n<RESOURCE>\n<TABLE name=\""<< name
		  << "\">"<< endl;
	}
    file<<(*this);
}

void CTimeString::savePhase
  ( std::string const & name
  , double Frequency
  , double zeropoint
  )
{
  std::ofstream file ( name.c_str( ));

  if (!file) {
	// file-error
	return;
    }
    // set output-format
    file.precision(12);
	if (mVOTableFormat) {
	  // write votable header
	  //
	  file<< "<?xml version='1.0'?>\n<VOTABLE version=\"1.1\"\n>"
		  << "<!--\n !  VOTable written by Period04\n"
		  << " !  http://www.univie.ac.at/tops/Period04/\n"
		  << " !-->\n<RESOURCE>\n<TABLE name=\""<< name
		  << "\">"<< endl;
	}
    // check for File-error MISSING !!!
    writeOut(file,Frequency, zeropoint);
}

void CTimeString::writeAll ( std::ostream & str, char * head)
{
  // store default-precission and set new 
  int prec = str.precision( );
  str.precision( 15);

  for
    ( CTimeArray::iterator iter = mTimeArray.begin( )
    ; iter != mTimeArray.end( )
    ; ++iter
    )
  {
    SelectedTime tmp = &(iter->second);

    str
      << head
       << "\t" << tmp->getRunningIndex( )
       << "\t" << tmp->getTime( )
       << "\t" << tmp->getObserved( )
       << "\t" << tmp->getAdjusted( )
       << "\t" << tmp->getCalculated( )
       << "\t" << tmp->getPointWeight( )
       << "\t" << getIDName( 0, tmp->getIDName( 0)).getName( )
       << "\t" << getIDName( 1, tmp->getIDName( 1)).getName( )
       << "\t" << getIDName( 2, tmp->getIDName( 2)).getName( )
       << "\t" << getIDName( 3, tmp->getIDName( 3)).getName( )
       << std::endl
     ;
  }
  
  // restore old precission
  str.precision( prec);

  return;
}

void CTimeString::readIn(istream & input,int version)
{
  double t, m,a,c,pw;
  int n1,n2,n3,n4;
  int index=0;
  // set defaults
  t=m=a=-100;c=0;n1=n2=n3=n4=0;
  pw=1.0;
  // read in data
  input>>index; // index
  input>>t; // time
  input>>m; // magnitude
  input>>a; // adjusted
  input>>c; // calculated
  if (version>10000) {
      input>>pw; // weight
  }

  std::string name;
  input >> name; // Name 1
  n1 = mNames[0].getID( name);
  input >> name; // Name 2
  n2 = mNames[1].getID( name);
  input >> name; // Name 3
  n3 = mNames[2].getID( name);
  input >> name; // Name 4
  n4 = mNames[3].getID( name);

  // now add point
  mTimeArray.insert( std::pair< double const, CTimePoint>( t, CTimePoint( t, m, a, c, pw, n1, n2, n3, n4, index)));
}

void CTimeString::loadVOTable
  ( std::string const & fname
  )
{
  char line [ 2048];
  double t, m, a, c, pw, w;
  int n1, n2, n3, n4;

  //---
  //--- read in the entries
  //---
  int inputlen=mInputFormat.length();

  std::string defaulttxt = "unknown";
  int n1d = mNames[0].getID( defaulttxt);
  int n2d = mNames[1].getID( defaulttxt);
  int n3d = mNames[2].getID( defaulttxt);
  int n4d = mNames[3].getID( defaulttxt);

  bool containsObserved = (mInputFormat.find( 'o') == std::string::npos) ? false : true;
  
  // check if a timestring using labels "unknown" has already been read in,
  // if so then create a new item
  while (mNames[0].getIDName( n1d).getPoints( ) != 0)
  {
    defaulttxt += "+";
    n1d=mNames[0].getID( defaulttxt);
  }

  //-- clear the input
  t=m=a=NO_ADJUST, c=0;;
  pw=1.0; w=1.0;
  n1=n1d;
  n2=n2d;
  n3=n3d;
  n4=n4d;

  //---
  //--- start extracting information from file
  //---
  bool isTableInput = false;
  bool isDataLineInput = false;
  bool isDataValueInput = false;
  int icol = 0;

#if PROGRESS
  ifstream file_sink( fname.c_str( ));

  // TODO: Is this really needed?
  if (! file_sink.is_open( ))
  {
    MYERROR( "Problems opening file:" << fname << "- does it exist?");
    return;
  }

  // Example
  // http://www.cplusplus.com/reference/iostream/istream/seekg/
  file_sink.seekg( 0, std::ios::end);
  std::streampos file_size = file_sink.tellg( );
  file_sink.seekg( 0);
  
  basic_progress_filter< char> progress_filter
    ( *(file_sink.rdbuf( ))
    , file_size
    , progress
    )
  ;

  std::istream infile( &progress_filter);
#else
  ifstream infile( fname.c_str( ));

  if (! infile.is_open( ))
  {
    MYERROR( "Problems opening file:" << fname << "- does it exist?");
    return;
  }
#endif


  infile.getline(line, 2047,getLineSeparator());

  while (! infile.eof( ))
  {

	// is something interesting in line?
	string sline(line);
	int startID = 0;
	
	if (sline.find("</TABLEDATA")!=string::npos ||
	    sline.find("</tabledata")!=string::npos) {
	  isTableInput = false;
	  //	  break;
	} 

	if (isTableInput) {
	  // examine line
	  
	  while (sline!="") {
	    if (!isDataLineInput) {
	      startID = sline.find("<");   // first tag
	      if (startID!=-1) {
		sline = sline.substr(startID,sline.length()-startID);
		startID=0;
		if (sline.find("<tr")!=string::npos || 
		    sline.find("<TR")!=string::npos) {		
		  startID = sline.find(">");
		  if (startID == (signed int)sline.length()) {
		    sline = "";
		  } else {
		    int endID = sline.find("<", startID);
		    sline = sline.substr(startID+1,endID-startID+1);
		  }
		  isDataLineInput = true;
		} else if (sline.find("</tr")!=string::npos ||
			   sline.find("</TR")!=string::npos) {
		  startID = sline.find(">");
		  if (startID == (signed int)sline.length()) {
		    sline = "";
		  } else {
		    int endID = sline.find("<");
		    sline = sline.substr(startID+1,endID-startID+1);
		  }
		  isDataLineInput = false;
		  icol=0;
		} 
	      }
	      
	    } else { // isDataLineInput  
	      //--- extract data values
	      if (!isDataValueInput) {
		startID = sline.find("<");   // first tag
		if (startID!=-1) {
		  sline = sline.substr(startID,sline.length()-startID);
		  startID=0;
		  if (sline.find("<td")!=string::npos ||
		      sline.find("<TD")!=string::npos) {			
		    startID = sline.find(">",startID);
		    if (startID == (signed int)sline.length()) {
		      sline = "";
		    } else {
		      sline = sline.substr(startID+1,sline.length()-startID+1);
		      startID=0;
		    }
		    isDataValueInput = true;
		  } else if (sline.find("</td")!=string::npos ||
			     sline.find("</TD")!=string::npos) {
		    startID = sline.find(">");
		    if (startID == (signed int)sline.length()) {
		      sline = "";
		    } else {
		      sline = sline.substr(startID+1,sline.length()-startID+1);
		      startID=0;
		    }
		    isDataValueInput = false;
		    icol++;
		    if (icol==inputlen) {
		      // Add only if point valid...
		      if (t!=NO_ADJUST) {
			if (!containsObserved)
			  m = 0.0;
			mTimeArray.insert( std::pair< double const, CTimePoint>( t, CTimePoint( t, m, a, c, pw, n1, n2, n3, n4)));
			//-- clear the input
			t=m=a=NO_ADJUST, c=0;;
			pw=1.0; w=1.0;
			n1=n1d;
			n2=n2d;
			n3=n3d;
			n4=n4d;
		      }
		      isDataLineInput = false;
		    }
		  }
		}
	      } else { // isDataValueInput
		startID = sline.find("<",startID);   // first tag
		string value;
		bool isInput=false;
		if (startID!=-1) { 
		  value = sline.substr(0,startID);
		  sline = sline.substr(startID,sline.length()-startID);
		  startID=0;
		  isInput=true;
		  isDataValueInput = false;
		} else {  // </td> in next line
		  value = sline;
		  isInput=true;
		  isDataValueInput = false;
		}
		if (isInput) {
		  string entry;
		  istringstream istr(value);
		  switch(mInputFormat[icol]) {
		  case 't': // time
		    istr >> t;
		    break;
		  case 'o': // magnitude
		    istr >> m;
		    break;
		  case 'a': // adjusted
		    istr >> a;
		    break;
		  case 'c': // calculated
		    istr >> c;
		    break;
		  case 'g': // point weight
		    istr >> pw;
		    break;
		  case 'u': // point error (special treatment in constructor)
		    istr >> pw;
		    break;
		  case 'p': // Data Residuals
		    istr >> c;
		    c=m-c;
		    break;
		  case 'b': { // Adjusted Residuals
		    double r=0;
		    istr >> r;
		    if ( (a==-100) && (c!=0) ) {
		      // set Amplitude
		      a=c+r;
		    } else {
		      // set Calculated
		      c=a-r;
		    } }
		    break;
		  case '1': // name 1
		    istr >> entry;
		    n1 = mNames[0].getID( entry);
		    break;
		  case '2': // name 2
		    istr >> entry;
		    n1 = mNames[1].getID( entry);
		    break;
		  case '3': // name 3
		    istr >> entry;
		    n1 = mNames[2].getID( entry);
		    break;
		  case '4': // name 4
		    istr >> entry;
		    n1 = mNames[3].getID( entry);
		    break;
		  case '5': // weight(name 1)
		    istr >> w;
		    getIDName(0,n1).setWeight(w);
		    break;
		  case '6': // weight(name 1)
		    istr >> w;
		    getIDName(1,n1).setWeight(w);
		    break;
		  case '7': // weight(name 1)
		    istr >> w;
		    getIDName(2,n1).setWeight(w);
		    break;
		  case '8': // weight(name 1)
		    istr >> w;
		    getIDName(3,n1).setWeight(w);
		    break;
		  case 'i': // ignore next
		  case 'n':
		    break;
		  default:
		    MYERROR("entry "<<mInputFormat[icol]<<
			    " in formatstring not know...\n"
			    "ignoring column %i");
		    // writing error
		    break;
		  }
		}
	      }
	    }
	  }
	}	
	
	if (sline.find("<TABLEDATA")!=string::npos ||
	    sline.find("<tabledata")!=string::npos) {
	  isTableInput = true;
	} 

	infile.getline(line, 2047,getLineSeparator());
  }

#if PROGRESS
  file_sink.close();
#else
  infile.close();
#endif
  
  if (mFileName=="") { mFileName=fname; }  // copy name
  else { mFileName+=",\n"+fname; }         // if we are appending, add name
  garbageCollect();                        // now make a Garbage Collection
  
  
}

void CTimeString::load
  ( std::string const & fname
  )
{
  char line [ 2048];
  double t, m, a, c, pw, w;
  int n1, n2, n3, n4;

    //---
    //--- read in the entries
    //---
    int inputlen=mInputFormat.length();

    string defaulttxt = "unknown";
    int n1d = mNames[0].getID( defaulttxt);
    int n2d = mNames[1].getID( defaulttxt);
    int n3d = mNames[2].getID( defaulttxt);
    int n4d = mNames[3].getID( defaulttxt);

    bool containsObserved = (mInputFormat.find( 'o') == std::string::npos) ? false : true;

    // check if a timestring using labels "unknown" has already been read in,
    // if so then create a new item
    while (mNames[0].getIDName( n1d).getPoints( ) != 0)
    {
      defaulttxt += "+";
      n1d = mNames[0].getID( defaulttxt);
    }

#if PROGRESS
  ifstream file_sink( fname.c_str( ));

  // TODO: Is this really needed?
  if (! file_sink.is_open( ))
  {
    MYERROR( "Problems opening file:" << fname << "- does it exist?");
    return;
  }

  // Example
  // http://www.cplusplus.com/reference/iostream/istream/seekg/
  file_sink.seekg( 0, std::ios::end);
  std::streampos file_size = file_sink.tellg( );
  file_sink.seekg( 0);
  
  basic_progress_filter< char> progress_filter
    ( *(file_sink.rdbuf( ))
    , file_size
    , progress
    )
  ;

  std::istream infile( &progress_filter);
#else
  ifstream infile( fname.c_str( ));

  if (! infile.is_open( ))
  {
    MYERROR( "Problems opening file:" << fname << "- does it exist?");
    return;
  }
#endif

  while (infile.getline( line, 2047, getLineSeparator( )))
  {

	if (strcmp(line,"\n")==0) { continue; } // nothing in line, skip

	char *pnt=line;
	int linelength = strlen(line);  // save length of line
	int count=0;                    // save current line position of pnt

	// clear the input
	t=m=a=NO_ADJUST, c=0;;
	pw=1.0; w=1.0;
	n1=n1d;
	n2=n2d;
	n3=n3d;
	n4=n4d;
	for(int i=0;i<inputlen;i++) {
	    char entry[1024];
	    int len=0;
	    // skip whitspaces
	    while (isspace(*pnt)) {
		pnt++; count++;
	    }
	    if (count>=linelength) { // only empty spaces in line, skip
		  t=NO_ADJUST; break;
	    }

	    // is it a comment? - e.g does it start with "#",";",":" ?
	    switch (*pnt) {
		case '#':
		case ';':
		case '%':
		  //cout << "ignore t " << t << endl;
		    // ignoreline
		    break;
		case 0: // nothing is in line
		    break;
		default:
		{
		    while ((*pnt!=0) && (!isspace(*pnt)) ) {
			entry[len]=*pnt;
			pnt++; count++;
			len++;
		    }
		    // terminate string
		    entry[len]=0;	      	
		    // parse string
		    switch(mInputFormat[i]) {
			case 't': // time
			    sscanf(entry,"%lf",&t);
			    break;
			case 'o': // magnitude
			    sscanf(entry,"%lf",&m);
			    break;
			case 'a': // adjusted
			    sscanf(entry,"%lf",&a);
			    break;
			case 'c': // calculated
			    sscanf(entry,"%lf",&c);
			    break;
			case 'g': // point weight
			    sscanf(entry,"%lf",&pw);
			    break;
			case 'u': // point error (special treatment in CTimePoint constr.)
			    sscanf(entry,"%lf",&pw);
			    break;
			case 'p': // Data Residuals
			    // As we may not adjust the Observed data
			    // we have to set the value of calculate
			    // to the residuals
			    sscanf(entry,"%lf",&c);
			    c=m-c;
			    break;
			case 'b': // Adjusted Residuals
			{
			    double r=0;
			    sscanf(entry,"%lf",&r);
			    if ( (a==-100) && (c!=0) ) {
				// set Amplitude
				a=c+r;
			    } else {
				// set Calculated
				c=a-r;
			    }
			}
			break;
			case '1': // name 1
			    n1 = mNames[0].getID( entry);
			    break;
			case '2': // name 2
			    n2 = mNames[1].getID( entry);
			    break;
			case '3': // name 3
			    n3 = mNames[2].getID( entry);
			    break;
			case '4': // name 4
			    n4 = mNames[3].getID( entry);
			    break;
			case '5': // weight(name 1)
			    sscanf(entry,"%lf",&w);
			    getIDName(0,n1).setWeight(w);
			    break;
			case '6': // weight(name 2)
			    sscanf(entry,"%lf",&w);
			    getIDName(1,n2).setWeight(w);
			    break;
			case '7': // weight(name 3)
			    sscanf(entry,"%lf",&w);
			    getIDName(2,n3).setWeight(w);
			    break;
			case '8': // weight(name 4)
			    sscanf(entry,"%lf",&w);
			    getIDName(3,n4).setWeight(w);
			    break;
			case 'i': // ignore next
			case 'n':
			    break;
			default:
			    MYERROR("entry "<<mInputFormat[i]<<
				    " in formatstring not know...\n"
				    "ignoring column %i");
			    // writing error
			    break;
		    }
		}
	    }
	}
	// Add only if point valid...
	if (t!=NO_ADJUST) {
	  if (!containsObserved)
		m = 0.0;
	  if (!ISINF(m) && !ISNAN(m))
		mTimeArray.insert( std::pair< double const, CTimePoint>( t, CTimePoint( t, m, a, c, pw, n1, n2, n3, n4)));
	}
    }

#if PROGRESS
  file_sink.close();
#else
  infile.close();
#endif

    if (mFileName=="") { mFileName=fname; }  // copy name
    else { mFileName+=",\n"+fname; }         // if we are appending, add name
    garbageCollect();                        // now make a Garbage Collection
}

double CTimeString::baseLine() const {
    return (getSelectedPoints()==0)?0:
	mSelection[getSelectedPoints()-1]->getTime()-mSelection[0]->getTime();
}

double CTimeString::getTimeAverage() const {
    if (getSelectedPoints()==0) { return 0.0; }
    double avg = 0.0;
    for (int i=0; i<getSelectedPoints(); i++) {
	avg+=mSelection[i]->getTime();
    }
    return avg/getSelectedPoints();
}

int CTimeString::calculateExpectationValue(
    double &mi, double &ma, double t_start, double t_end) const 
{
    int const size=100;
    int i,box[size+1];
    // clean box ...
    for(i=0;i<=size;i++) {
	box[i]=0;
    }
    double diff;
    // find scale
    double scale=(ma-mi)/size;
    double bo;
    // summ up peaks
    for (i=1;i<getSelectedPoints();i++) {
	if ((mSelection[i-1]->getTime()<t_start) ||
	    (mSelection[i]->getTime()>t_end))
	    continue;
	diff=(mSelection[i]->getTime()) - (mSelection[i-1]->getTime());
	bo=floor((diff-mi)/scale);
	if ( (bo>=0) && (bo<=size) ) {
	    // increment if in box
	    box[int(bo)]++;
	}
    }
    // find highest
    int found=0, foundval=0;
    for(i=0;i<=size;i++) {
	if (box[i]>foundval) {
	    found=i;
	    foundval=box[i];
	}
    }
    mi+=found*scale;
    ma=mi+1*scale;

    // that is exact enough !!!
    if ((ma-mi)<.0001) {
	return 0;
    }
    
    return foundval;
}

double CTimeString::nyquist() const {
  if (getSelectedPoints()<2) {
    return -1;
  } else if (getSelectedPoints()==2) {
    return 0.5/(mSelection[1]->getTime() - mSelection[0]->getTime());
  }
    // min and max dt's
    double min=9999999, max=0;
    double diff;
    // find min and max
    for (int i=1;i<getSelectedPoints();i++) {
	diff=(mSelection[i]->getTime()) - (mSelection[i-1]->getTime());
	min=( min < diff ) ? min : diff ;
	max=( max > diff ) ? max : diff ;
    }
    double maxcp=max;
    const int limit = getSelectedPoints()/3;
    int v;
    while ((v=calculateExpectationValue(
		min,max, mSelection[0]->getTime(),
		mSelection[getSelectedPoints()-1]->getTime()))>limit); 
    {
	// nothing to do !!!
    }
    if (min==0) {
	min=0.00001;
	max=maxcp;
	while ((v=calculateExpectationValue(
		    min, max, mSelection[0]->getTime(),
		    mSelection[getSelectedPoints()-1]->getTime()))>limit);
	{
	    // nothing to do !!!
	}
    }

    // return nyquist - estimate...
    return 1/(min+max);
}

double CTimeString::nyquist(double t_start, double t_end) const {
    if (getSelectedPoints()==0) { return 0; }
    if (t_start>t_end) {
	char txt[256];
	sprintf(txt, "Please choose values within the range\n[ %f - %f ]",
		mSelection[0]->getTime(),
		mSelection[getSelectedPoints()-1]->getTime());
	showMessage(txt);
	return 0;
    }
    // min and max dt's
    double min=9999999, max=0;
    double diff;
    // find min and max
    for (int i=1;i<getSelectedPoints();i++) {
	if ((mSelection[i-1]->getTime()<t_start) ||
	    (mSelection[i]->getTime()>t_end))
	    continue;
	diff=(mSelection[i]->getTime()) - (mSelection[i-1]->getTime());
	min=( min < diff ) ? min : diff ;
	max=( max > diff ) ? max : diff ;
    }
    double maxcp=max;
    const int limit = getSelectedPoints()/3;
    int v;
    while ((v=calculateExpectationValue(min,max,t_start,t_end))>limit); 
    {
	// nothing to do !!!
    }
    if (min==0) {
	min=0.00001;
	max=maxcp;
	while ((v=calculateExpectationValue(min,max,t_start,t_end))>limit);
	{
	    // nothing to do !!!
	}
    }
    // return nyquist - estimate...
    return 1/(min+max);
}

//
// Here needs something to be done !!!! 
//
double CTimeString::average(int useWeight) const { 
    double sum=0;
    double t,a;
    // calculate without weights
    if (!useWeight) {
	for(int i=0; i<getSelectedPoints(); i++) {
	    point(i,&t,&a);
	    sum+=a;
	}
	return sum/getSelectedPoints();
    }
    // calculate with weights
    for(int i=0; i<getSelectedPoints(); i++) {
	point(i,&t,&a);
	sum+=a*(mSelection[i]->getWeight());
    }
    return sum/getWeightSum();
}

void CTimeString::changeNameSet
  ( int i
  , std::string const & tmp
  )
{
    checkBounds( i);
    mNameSet[i] = tmp;
}

void CTimeString::calcAverage() {
    int i,j;
    // clean all entries
    for (i=0;i<4;i++) {
	for (j=0;j<mNames[i].getNumberOfNames();j++) {
	    mNames[i].getIndexName(j).resetValues();
	}
    }
    // sum up
    CTimePoint *tmp;
    for (i=0;i<getSelectedPoints();i++) {
	// new point
	tmp=&((*this)[i]);
	// add up for each name
	for (j=0;j<4;j++) {
	    mNames[j].getIDName(tmp->getIDName(j))
		.addPoint(tmp->getDataResidual(),
			  tmp->getAdjustedResidual(),
			  tmp->getWeight());
	}
    }
}

void CTimeString::adjust
  ( int attribute
  , DataMode orig
  , int n
  , int * ids
  , bool useweights
  )
{
#if 0
  cerr << "adjust " << attribute << endl;
  
  for ( int i = 0; i < n; ++i)
  {
    cerr << "id " << ids[ i] << " " << mNames[attribute].getIDName( ids[ i]).getName( ) << endl;
  }
#endif
      
  for ( int i = 0; i < n; ++i)
  {
    int id = ids[ i];
    double ori, adj;  
    
	if (useweights) {
	    ori=(mNames[attribute].getIDName(id)).getAverageOrigWeight();
	    adj=(mNames[attribute].getIDName(id)).getAverageAdjWeight();
	} else {
	    ori=(mNames[attribute].getIDName(id)).getAverageOrig();
	    adj=(mNames[attribute].getIDName(id)).getAverageAdj();
	}
	for (int j=0; j<getSelectedPoints(); j++) { // iterate CTimePoints
	    // find out if this entry has to be changed
	    if ((*this)[j].getIDName(attribute)==id) {
		// find out previous value
		double value;
		// adjust value
		if (orig==Observed) {
		    value=(*this)[j].getObserved()-ori;
		} else {
		    value=(*this)[j].getAdjusted()-adj;
		}
		// write value back
		(*this)[j].setAdjusted(value);
	    }
	}
    }
}

void CTimeString::subtractFromObserved(double value) {
  for (int i=0; i<getSelectedPoints(); i++) {
	double newvalue = (*this)[i].getObserved()-value;
	(*this)[i].setObserved(newvalue);
	(*this)[i].setCalculated(0.0);
  }
}

void CTimeString::subtractFromAdjusted(double value) {
  for (int i=0; i<getSelectedPoints(); i++) {
	double newvalue = (*this)[i].getAdjusted()-value;
	(*this)[i].setAdjusted(newvalue);
	(*this)[i].setCalculated(0.0);
  }
}

void CTimeString::makeObservedAdjusted() {
    for (int i=0; i<getSelectedPoints(); i++) {
	double value=(*this)[i].getObserved();
	(*this)[i].setAdjusted(value);
    }
}

void CTimeString::makeDataResidualsObserved() {
    for (int i=0; i<getSelectedPoints(); i++) {
	double value=(*this)[i].getDataResidual();
	(*this)[i].setObserved(value);
	(*this)[i].setAdjusted(value);
	(*this)[i].setCalculated(0.0);
    }
}

void CTimeString::makeAdjResidualsObserved() {
    for (int i=0; i<getSelectedPoints(); i++) {
	double value=(*this)[i].getAdjustedResidual();
	(*this)[i].setObserved(value);
	(*this)[i].setAdjusted(value);
	(*this)[i].setCalculated(0.0);
    }
}

void CTimeString::makeCalculatedObserved() {
    for (int i=0; i<getSelectedPoints(); i++) {
	double value=(*this)[i].getCalculated();
	(*this)[i].setObserved(value);
	(*this)[i].setAdjusted(value);
	(*this)[i].setCalculated(0.0);
    }
}

void CTimeString::makeAdjustedObserved() {
    for (int i=0; i<getSelectedPoints(); i++) {
	double value=(*this)[i].getAdjusted();
	(*this)[i].setObserved(value);
	(*this)[i].setAdjusted(value);
	(*this)[i].setCalculated(0.0);
    }
}

void CTimeString::makeCalculatedSimulatedBase() {
    for (int i=0; i<getSelectedPoints(); i++) {
       	double value=(*this)[i].getCalculated();
	(*this)[i].setSimulatedBase(value);
    }
}

void CTimeString::makeSimulatedBaseCalculated() {
    for (int i=0; i<getSelectedPoints(); i++) {
	double value=(*this)[i].getSimulatedBase();
	(*this)[i].setCalculated(value);
    }
}

void CTimeString::storeCalculated() {
    for (int i=0; i<getSelectedPoints(); i++) {
	double value=(*this)[i].getCalculated();
	(*this)[i].setCalculatedStore(value);
    }
}

void CTimeString::restoreCalculated() {
    for (int i=0; i<getSelectedPoints(); i++) {
	double value=(*this)[i].getCalculatedStore();
	(*this)[i].setCalculated(value);
    }
}


void CTimeString::createMonteCarloData() {
    for (int i=0; i<getSelectedPoints(); i++) {
	double nval = 0;
	int k;
	for (k=0; k<mMoCaIterations; k++) {
	    //nval += (double)rand() / RAND_MAX;
	    nval += genrand_real1();  // use the MT number generator
	}
	nval = (nval / mMoCaIterations - .5) * sqrt(12. * mMoCaIterations);
	double value = (*this)[i].getSimulatedBase() + mMoCaResiduals*nval;
	(*this)[i].setSimulated(value);
    }
}

// sorting-Structure
struct IndexArray { 
  int RunningIndex;
  int SelectIndex;};

// sorting - routine
int runningIndexCompare(const void *t1,const void *t2) {
    int index1=((const IndexArray *)t1)->RunningIndex;
    int index2=((const IndexArray *)t2)->RunningIndex;
    if (index1>index2) return 1;
    if (index1<index2) return -1;
    return 0;
}

ostream &operator<<(ostream &str,CTimeString &tmp) {
    return tmp.writeOut(str,0.0);
}


ostream &CTimeString::writeOut(ostream& str, double Frequency, double zeropoint) {
  cerr << mOutputFormat << endl;
  int l=mOutputFormat.length();
  // sort data
  // now create and fill array to sort
  int selected=mSelected;
  IndexArray * sortarray = new IndexArray[selected];
  int i;
  for (i=0;i<selected;i++) {
	sortarray[i].RunningIndex=mSelection[i]->getRunningIndex();
	sortarray[i].SelectIndex=i;
  }
  // now sort array
  qsort(sortarray,selected,sizeof(IndexArray),runningIndexCompare);
  // in case we're writing a VOTable file, add some xml stuff
  if (mVOTableFormat) {
	for(int j=0;j<l;j++) {
	  switch(mOutputFormat[j]) {
	  case 't':
		if (Frequency!= 0.0) { 
		  str<<"<FIELD datatype=\"double\" name=\"Phase\"/>"<<endl; 
		} else { 
		  str<<"<FIELD datatype=\"double\" name=\"Time\"/>"<<endl;  
		}
		break;
	  case 'o':
		str<<"<FIELD datatype=\"double\" name=\"Observed\"/>"<<endl;  
		break;
	  case 'a':
		str<<"<FIELD datatype=\"double\" name=\"Adjusted\"/>"<<endl;  
		break;
	  case 'c':
		str<<"<FIELD datatype=\"double\" name=\"Calculated\"/>"<<endl;  
		break;
	  case 'p':
		str<<"<FIELD datatype=\"double\" name=\"DataResiduals\"/>"<<endl;  
		break;
	  case 'b':
		str<<"<FIELD datatype=\"double\" name=\"AdjustedResiduals\"/>"<<endl;  
		break;
	  case 'g':
		str<<"<FIELD datatype=\"double\" name=\"PointWeight\"/>"<<endl;  
		break;
	  case 'u':
		str<<"<FIELD datatype=\"double\" name=\"PointError\"/>"<<endl;  
		break;
	  case 'w':
		str<<"<FIELD datatype=\"double\" name=\"Weight\"/>"<<endl;  
		break;
	  case 'n':
		str<<"<FIELD datatype=\"double\" name=\"Ignore\"/>"<<endl;  
		break;
	  case '1':
		str<<"<FIELD arraysize=\"30\" datatype=\"char\" name=\"IDNameCol1\"/>"<<endl;  
		break;
	  case '2':
		str<<"<FIELD arraysize=\"30\" datatype=\"char\" name=\"IDNameCol2\"/>"<<endl;  
		break;
	  case '3':
		str<<"<FIELD arraysize=\"30\" datatype=\"char\" name=\"IDNameCol3\"/>"<<endl;  
		break;
	  case '4':
		str<<"<FIELD arraysize=\"30\" datatype=\"char\" name=\"IDNameCol4\"/>"<<endl;  
		break;
	  case '5':
		str<<"<FIELD datatype=\"double\" name=\"IDNameCol1Weight\"/>"<<endl;  
		break;
	  case '6':
		str<<"<FIELD datatype=\"double\" name=\"IDNameCol2Weight\"/>"<<endl;  
		break;
	  case '7':
		str<<"<FIELD datatype=\"double\" name=\"IDNameCol3Weight\"/>"<<endl;  
		break;
	  case '8':
		str<<"<FIELD datatype=\"double\" name=\"IDNameCol4Weight\"/>"<<endl;  
		break;
	  }
	}
	str<<"<DATA>\n<TABLEDATA>"<< endl;
  }

  // now write out the data...
  for(i=0;i<mSelected;i++) {
	// get the sorted selected index
	CTimePoint tm(*mSelection[sortarray[i].SelectIndex]);
	if (mVOTableFormat)
	  str<<"  <TR>"<<endl;
	for(int j=0;j<l;j++) {
	  if (mVOTableFormat)
		str<<"    <TD>";
	  switch(mOutputFormat[j]) {
	  case 't':
		if (Frequency!= 0.0) { str<<tm.getPhasedTime(Frequency, zeropoint); }
		else                 { str<<tm.getTime(); }
		break;
	  case 'o':
		str<<tm.getObserved(); break;
	  case 'a':
		str<<tm.getAdjusted(); break;
	  case 'c':
		str<<tm.getCalculated(); break;
	  case 'p':
		str<<tm.getDataResidual(); break;
	  case 'b':
		str<<tm.getAdjustedResidual(); break;
	  case 'g':
		str<<tm.getPointWeight(); break;
	  case 'u':
		str<<1/sqrt(tm.getPointWeight()); break;
	  case 'w':
		str<<tm.getWeight(); break;
	  case 'n':
		str<<i; break;
	  case '1':
		str<<getIDName(0,tm.getIDName(0)).getName(); break;
	  case '2':
		str<<getIDName(1,tm.getIDName(1)).getName(); break;
	  case '3':
		str<<getIDName(2,tm.getIDName(2)).getName(); break;
	  case '4':
		str<<getIDName(3,tm.getIDName(3)).getName(); break;
	  case '5':
		str<<getIDName(0,tm.getIDName(0)).getWeight(); break;
	  case '6':
		str<<getIDName(1,tm.getIDName(1)).getWeight(); break;
	  case '7':
		str<<getIDName(2,tm.getIDName(2)).getWeight(); break;
	  case '8':
		str<<getIDName(3,tm.getIDName(3)).getWeight(); break;
	  }
	  if (mVOTableFormat)
		str<<"</TD>"<<endl;
	  else
	    str<<"\t";
	}
	if (mVOTableFormat)
	  str<<"  </TR>";
	str<<endl;
  }
  if (mVOTableFormat)
	str<<"</TABLEDATA>\n</DATA>\n</TABLE>\n</RESOURCE>\n</VOTABLE>"<<endl;
  delete [] sortarray;
  return str;
}

std::string CTimeString::predictFileFormat
  ( std::string const & filename
  )
{
    const int linesize=4096;     // maximum line size
    char linebuffer[linesize];   // the buffer for a line
    const int form_size=256;     // maximum format string size
    std::string formstr[form_size];  // format string
    int formcnt[form_size];
    int lastform=0;
    for (int i=0;i<form_size;i++) { formcnt[i]=0; }

    //--- open file
    std::ifstream infile(filename.c_str());
    if (!infile.is_open()) { return "NOFILE"; }

    //--- check if just 'carriage return' is used as 
    //--- line separator in this file (old Mac style)
    infile.getline(linebuffer,linesize);
    setLineSeparator('\n');      // this is ok for Win + Unix
    if (strstr(linebuffer, "\r")!=NULL) {
	setLineSeparator('\r');    // for Mac OS only
    } 
    infile.close();

	//--- check whether file is in votable xml format, 
	//--- if yes then extract field information
	infile.open(filename.c_str());
    infile.clear();
	bool isVOTable=false;
	std::string sformat;
    while (!infile.eof()) {
	  infile.getline(linebuffer,linesize,getLineSeparator());
	  string sxml(linebuffer);
	  if (sxml.find("<VOTABLE")!=string::npos ||
		  sxml.find("<votable")!=string::npos)
		isVOTable=true;
	  //--- find field information tags
	  if (isVOTable) {
		bool isDone = false;
		int startID = 0;
		while (!isDone) {
		  if (sxml.find("<FIELD", startID)!=string::npos ||
			  sxml.find("<field", startID)!=string::npos) {
			startID = (sxml.find("<FIELD", startID)==string::npos ?
					   sxml.find("<field", startID) :
					   sxml.find("<FIELD", startID));
			startID = sxml.find("name", startID)+4;
			startID = sxml.find("\"", startID);
			startID++;
			int endID = sxml.find("\"", startID);
			string name = sxml.substr(startID,endID-startID);
			//--- check whether it is a typical Period04 field name
			if      (name=="Time")              { sformat.append("t"); }
			else if (name=="Observed")          { sformat.append("o"); }
			else if (name=="Adjusted")          { sformat.append("a"); }
			else if (name=="Calculated")        { sformat.append("c"); }
			else if (name=="DataResiduals")     { sformat.append("r"); }
			else if (name=="AdjustedResiduals") { sformat.append("R"); }
			else if (name=="PointWeight")       { sformat.append("p"); }
			else if (name=="PointError")        { sformat.append("u"); }
			else if (name=="Weight")            { sformat.append("w"); }
			else if (name=="IDNameCol1")        { sformat.append("1"); }
			else if (name=="IDNameCol2")        { sformat.append("2"); }
			else if (name=="IDNameCol3")        { sformat.append("3"); }
			else if (name=="IDNameCol4")        { sformat.append("4"); }
			else if (name=="IDNameCol1Weight")  { sformat.append("5"); }
			else if (name=="IDNameCol2Weight")  { sformat.append("6"); }
			else if (name=="IDNameCol3Weight")  { sformat.append("7"); }
			else if (name=="IDNameCol4Weight")  { sformat.append("8"); }
			else                                { sformat.append("n"); }
		  } else { 
			isDone=true;		  
		  }
		}
	  }
	}
	infile.close();
	if (isVOTable==true) {
	  return sformat;
	}

    //--- retrieve the file format for ASCII files
    infile.open( filename.c_str( ));
    infile.clear( );

    while (!infile.eof()) {
      infile.getline(linebuffer,linesize,getLineSeparator());
      if (strlen(linebuffer)!=0) {

        std::string form = predictLine( linebuffer);
        int j = 0;

        for ( ; j < lastform; ++j)
        {
          if (form == formstr[j])
          {
            // ok, found, so add up
            ++formcnt[j];
            break;
          }
        }

      //  not found yet, so add to list
      if (j == lastform)
      {
        formstr[lastform] = form;
        formcnt[lastform] = 1;
        ++lastform;
        if (lastform == 256) { lastform = 255; }
      }
    }
    }
    
    infile.close( );

    // now decode current
    int highest=0;
    int highestvalue=0;

    // find highest
    for (int j=0;j<lastform;j++) {
	// is this higher then previous?
	if (highestvalue<formcnt[j]) {
	    // assign new highest
	    highest=j;
	    highestvalue=formcnt[j];
	}
    }

    // and return the result
    return formstr[highest];
}

std::string CTimeString::predictLine
  ( char const * linebuffer
  )
{
    // define the current format
    std::string current;
    // new routine for linux
    char const * pnt = linebuffer;

    while(*pnt!=0) {
	// read entry from line
	char entry[1024];
	int len=0;
	while (isspace(*pnt)) { pnt++; }
	if (*pnt!=0) {
	    while ((*pnt!=0) && (!isspace(*pnt)) ) {
		entry[len]=*pnt;
		pnt++;
		len++;
	    }
	    // terminate string
	    entry[len]=0;
	    // per default integer
	    char type='i';
	    // find out if the data is float, integer or string
      for ( int i = 0; i < len; ++i)
      {
		char c=entry[i];
		if ( (!isdigit(c)) && (c!='-') && (c!='+') ) {
		    if ( (c=='E') || (c=='e') || (c=='.') ) {
			// OK, assume we are float, and continue checking
			type='f'; 
		    } else {
			// Any other character HAS to be a normal string,
			type='s';
			break; 
		    } // we do not need to continue checking
		}
	    }
	    // add type to result
	    current+=type;
	}
    }

    // now try to make some sense out of this...
    int intstart =0;
    int floatstart=0;
    int textstart=0;
    // make a copy
    int last=current.length( );

    //--- if we only have two columns let us assume that they represent
    //--- 'time' and 'observed'
    if (last==2) {
	return "to";
    }

    char* result= new char[last+1];
    strcpy(result,current.c_str());

    // parse each entry
    for (int decode=0;decode<last;decode++) {
	if (result[decode]=='i' || result[decode]=='f') {
	    if (textstart!=0) {	 // let's suppose the column is a name weight
		char resu='-';
		switch (intstart) {
		    case 0: resu='5'; break;
		    case 1: resu='6'; break;
		    case 2: resu='7'; break;
		    case 3: resu='8'; break;
		}
		result[decode]=resu;
		intstart++;
		continue;
	    }
	}
	if (result[decode]=='f') {         // start with floats now...
	    char resu;
	    switch (floatstart) {
		case 0: resu='t'; break;
		case 1: resu='o'; break;
		case 2: resu='u'; break;
		case 3: resu='a'; break;
		case 4: resu='c'; break;
		case 5: resu='r'; break;
		case 6: resu='R'; break;
		case 7: resu='w'; break;
		default:resu='n'; break;
	    }
	    result[decode]=resu;
	    // next in list...
	    floatstart++;
	} else if (result[decode]=='s')	{ // start with text now...
	    char resu;
	    switch (textstart) {
		case 0: resu='1'; break;
		case 1: resu='2'; break;
		case 2: resu='3'; break;
		case 3: resu='4'; break;
		default:resu='n'; break;
	    }
	    result[decode]=resu;
	    // next in list
	    textstart++;
	} else {
	    // unknown...
	    result[decode]='n';
	}
    }
    // clean up allocated string and save it to return
    std::string final = result;
    delete [] result;
    return final;    // return the result...
}

