
#ifndef __fourieralgorithm_h__
#define __fourieralgorithm_h__

#include "CTimeString.h"
#include "CFourier.h"


/*
 * Fourier transformation methods
 */

void fourier_transform(CTimeString const &time, CFourier &fourier, double zero);


/*
 * General methods:
 */

double noiseCalc(CTimeString const &time, double zero,
		 double from, double to, double stepping,
		 int useWeight, DataMode mode);

void calcOptimumFourierLoops(int points, int min, int max, int step);


#endif //__fourieralgorithm_h__
