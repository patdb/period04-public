/*--------------------------------------------------------------------*
 *  Period04.cpp
 *
 *  this file contains all native Methods
 *
 *--------------------------------------------------------------------*
 *
 *  General Inormation on JNI:
 *
 *  Native methods can access the following types:
 *
 *    Boolean  jboolean
 *    string   jstring
 *    byte     jbyte
 *    char     jchar
 *    short    jshort
 *    int      jint
 *    long     jlong
 *    float    jfloat
 *    double   jdouble
 *    void     void
 *
 *  All of these types can be directly accessed except for jstring, which
 *  requires a subroutine call to convert a Java Unicode string (2 bytes)
 *  to a C-style char* string (1 byte UTF-8 format).
 *
 *    Java Type				        Signature
 *    -------------------------------------------------------
 *    boolean					        Z
 *    byte						B
 *    char						C
 *    short						S
 *    int						I
 *    long						J
 *    float						F
 *    double					        D
 *    fully-qualified-class	    fully-qualified-class
 *    type[]					   [ type
 *    method type		     (arg-types) ret-type
 *    void						V
 *
 *    Example:
 *    (IJDF)Z is the signature of a method, which has four
 *    arguments ( int, long, double, float) and returns a boolean value
 *
 *--------------------------------------------------------------------*/

#include <jni.h>
#include "Period04.h"
#include <stdio.h>
#include <time.h>

#include <iostream>
using std::cerr;
using std::cout;
using std::endl;

#include <fstream>
using std::ifstream;

#include <string>
using std::string;

#include <string.h>        // only for  strcmp

#include "CProject.h"     // includes class CProject
#include "defs.h"         // includes declaration of MAX_FREQUENCIES
#include "format.h"       // includes format definitions
#include "message.h"      // for error messages
#include "Period04_java.h"
#include "mt19937ar.h"    // Mersenne Twister MT19937 random generator

#define BUILD_REV "v1.2.9.3"
#define BUILD_DATE __DATE__

#ifdef WIN32
 #define RANDOM rand
 #define SRANDOM srand
#else
 #define RANDOM random
 #define SRANDOM srandom
#endif

#define MAX_PROJECTS 5
//#define BATCH 1




//--- --------------------------------------------------------------- ---//
//---                      command interface                          ---//
//--- --------------------------------------------------------------- ---//

std::string command ( std::string const & command)
{
  if ("version" == command)
  {
    return BUILD_REV;
  }
  else if ("date" == command)
  {
    return BUILD_DATE;
  }

  return command;
}



static int batchMode = 0;

void setBatchMode(int value)
{
  batchMode = value;
}

//--- --------------------------------------------------------------- ---//
//---                      project management                         ---//
//--- --------------------------------------------------------------- ---//

static int       useProjectID   = 0;
static CProject *project        = new CProject[MAX_PROJECTS];
static CProject *currentProject;        // a pointer to the current project

void setUseProjectID(int pID) {
    useProjectID = pID;
    currentProject = &project[pID];
}

CProject* getCurrentProject() {
    return currentProject;
}

//--- --------------------------------------------------------------- ---//
//---            general methods for the use of the JNI               ---//
//--- --------------------------------------------------------------- ---//

JavaVM * jvm;
JNIEnv * jenv;
jclass jclazz, jOPane;
jmethodID sPBValue, sPString, sPVoid, sMDialog, sCDialog;


void releaseString ( jstring jstr, char const * c_str)
{
  jvm->AttachCurrentThread( (void**)&jenv, NULL);  // update JNIEnv pointer
  jenv->ReleaseStringUTFChars( jstr, c_str);
}

char const * getCString ( jstring const & text)
{
  // converts a java String into C-String
  jvm->AttachCurrentThread( (void**)&jenv, NULL);  // update JNIEnv pointer
  return jenv->GetStringUTFChars( text, 0);
}

jstring newString ( char const * c_str)
{
  jvm->AttachCurrentThread( (void**)&jenv, NULL);  // update JNIEnv pointer
  return jenv->NewStringUTF( c_str);
}

//--- --------------------------------------------------------------- ---//
//---             methods from that invoke java from c++              ---//
//--- --------------------------------------------------------------- ---//

bool progress
  ( unsigned long n
  , unsigned long total
  )
{
  static int done_old = 100; // Not 0, since most progress bars will start with this value.

  int done = 100 * n / total;

  if (done != done_old)
  {
    // the percentage number has changed, now update the
    // progress bar and set the new percentage value as old value
    done_old = done;
    return (0 != setProgressBarValue( done));
  }

  return false;
}

int setProgressBarValue ( int n)
{
  //#if BATCH
  if (batchMode>0)
  {
    if (n==0) { std::cout << std::endl; }
    std::cout << "\r" <<  n << "%";
    if (n==100) { std::cout<<std::endl; } 
    return 0;
  }
  //#else

  jvm->AttachCurrentThread( (void**)&jenv, NULL);  // update JNIEnv pointer

  sPBValue = jenv->GetStaticMethodID
    ( jclazz
    , "setProgressBarValue"
    , "(I)I"
    )
  ;

  return jenv->CallStaticIntMethod( jclazz, sPBValue, (jint) n);
  //#endif
}

int setProgressString ( char const * txt)
{
  //#if BATCH
  if (batchMode>0)
  { 
    std::cout << "\r"<< txt;
      //<< std::endl;
    return 0;
  }
  //#else
  jvm->AttachCurrentThread( (void**)&jenv, NULL);  // update JNIEnv pointer

  sPString=jenv->GetStaticMethodID
    ( jclazz
    , "setProgressString"
    , "(Ljava/lang/String;)I"
    )
  ;

  return jenv->CallStaticIntMethod( jclazz, sPString, newString( txt));
  //#endif
}

int isCalculationCanceled ( )
{
  //#if BATCH
  if (batchMode>0) {
    return 0;
  }
  //#else
  jvm->AttachCurrentThread( (void**)&jenv, NULL);  // update JNIEnv pointer
// TODO: Not used here?!
//  jOPane = jenv->FindClass( "javax/swing/JOptionPane");

  sPVoid = jenv->GetStaticMethodID
    ( jclazz
    , "isCalculationCanceled"
    , "()I"
    )
  ;

  return jenv->CallStaticIntMethod( jclazz, sPVoid);
  //#endif
}

void showMessage ( char const * text)
{
  if (batchMode>0)
  {
    std::cout << std::endl << text << std::endl;
    return;
  }
    
  jvm->AttachCurrentThread( (void**)&jenv, NULL);  // update JNIEnv pointer
  jOPane = jenv->FindClass( "javax/swing/JOptionPane");

  sMDialog = jenv->GetStaticMethodID
    ( jOPane
    , "showMessageDialog"
    , "(Ljava/awt/Component;Ljava/lang/Object;)V"
    )
  ;

  jenv->CallStaticVoidMethod( jOPane, sMDialog, NULL, newString( text));
}


int confirm ( char const * text)
{
  if (batchMode>0)
  {
    std::cout << std::endl << text << std::endl;
    std::cout << "=> confirmed" << std::endl;
    return 1;
  }

  
    jvm->AttachCurrentThread((void**)&jenv, NULL);  // update JNIEnv pointer
    jOPane=jenv->FindClass("javax/swing/JOptionPane");
    sCDialog=jenv->GetStaticMethodID(
	jOPane,"showConfirmDialog",
	"(Ljava/awt/Component;Ljava/lang/Object;Ljava/lang/String;I)I");
    int i=jenv->CallStaticIntMethod(
	jOPane,sCDialog,NULL,newString(text),newString("Confirm"),0);
    if (i==0)
	return 1;
    return 0;
}

void setJni( JNIEnv * env, jclass clazz) {
    jenv=env;
    jclazz=clazz;
    jenv->GetJavaVM(&jvm);  // update JavaVM pointer
}

//--- ---------------------------------------------------------------- ---//
//---           The implementation of the Period04 Class               ---//
//--- ---------------------------------------------------------------- ---//

/*
 * Class:     Period04
 * Method:    initializeProject
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_Period04_initializeProject
  ( JNIEnv * env, jclass clazz, jint pID, jstring userDir)
{
  setJni( env, clazz);
  char const * c_userDir = getCString( userDir);

  project[pID].newProject( );       // clean the project
  project[pID].setProjectID( pID);  // set the project ID

  //--- set project settings
  setUseProjectID( pID);
  currentProject->setUserDirectory( c_userDir);
  currentProject->readPreferences( );
  currentProject->readRecentFiles( );

  releaseString( userDir, c_userDir);
}

/*
 * Class:     Period04
 * Method:    getWorkingDirectory
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_getWorkingDirectory
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->getWorkingDirectory( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    getMaxProjects
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_getMaxProjects
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return MAX_PROJECTS;
}

/*
 * Class:     Period04
 * Method:    setUseProjectID
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_Period04_setUseProjectID
  ( JNIEnv * env, jclass clazz, jint pID)
{
  setJni( env, clazz);
  setUseProjectID( pID);
}

/*
 * Class:     Period04
 * Method:    setBatchMode
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_Period04_setBatchMode
  ( JNIEnv * env, jclass clazz, jint pID)
{
  setJni( env, clazz);
  setBatchMode( pID);
}

/*
 * Class:     Period04
 * Method:    getNumberOfRecentFiles
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_getNumberOfRecentFiles
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return currentProject->getNumberOfRecentFiles( );
}

/*
 * Class:     Period04
 * Method:    getRecentFiles
 * Signature: ()[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_Period04_getRecentFiles
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  int number = currentProject->getNumberOfRecentFiles( );

  jobjectArray ret = (jobjectArray) env->NewObjectArray
    ( number
    , env->FindClass( "java/lang/String")
    , newString("")
    )
  ;

  for ( int i = 0; i < number; ++i)
    env->SetObjectArrayElement( ret, i, newString( currentProject->getRecentFile(i).c_str( )));

  return ret;
}

/*
 * Class:     Period04
 * Method:    addRecentFile
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_Period04_addRecentFile
  ( JNIEnv * env, jclass clazz, jstring path)
{
  setJni( env, clazz);
  char const * c_path = getCString( path);
  currentProject->addRecentFile( c_path);
  releaseString( path, c_path);
}

/*
 * Class:     Period04
 * Method:    isExpertMode
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_Period04_isExpertMode
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return currentProject->isExpertMode( );
}

/*
 * Class:     Period04
 * Method:    setExpertMode
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_Period04_setExpertMode
  ( JNIEnv * env, jclass clazz, jboolean b)
{
  setJni( env, clazz);
  currentProject->setExpertMode( b);
}

/*
 * Class:     Period04
 * Method:    projectGetSelectedPoints
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetSelectedPoints
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return currentProject->getSelectedPoints( );
}

/*
 * Class:     Period04
 * Method:    projectGetTotalPoints
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetTotalPoints
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return currentProject->getTotalPoints( );
}

/*
 * Class:     Period04
 * Method:    projectGetStartTime
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetStartTime
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->getStartTime( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetEndTime
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetEndTime
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->getEndTime( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetTotalTimeLength
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectGetTotalTimeLength
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return currentProject->getTotalTimeLength( );
}

/*
 * Class:     Period04
 * Method:    projectGetReverseScale
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_Period04_projectGetReverseScale
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return (0 == currentProject->getReverseScale( )) ? false : true;
}

/*
 * Class:     Period04
 * Method:    projectSetReverseScale
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetReverseScale
  ( JNIEnv * env, jclass clazz, jint selected)
{
  setJni( env, clazz);
  currentProject->setReverseScale( selected);
}

/*
 * Class:     Period04
 * Method:    projectGetNameSet
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetNameSet
  ( JNIEnv * env, jclass clazz, jint i)
{
  setJni( env, clazz);
  return newString( currentProject->nameSet( i).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectNumberOfNames
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_Period04_projectNumberOfNames
  ( JNIEnv * env, jclass clazz, jint id)
{
  setJni( env, clazz);
  return currentProject->numberOfNames( id);
}

/*
 * Class:     Period04
 * Method:    projectGetActiveNames
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetActiveNames
  ( JNIEnv * env, jclass clazz, jint attribute)
{
  setJni( env, clazz);
  return currentProject->getTimeString( ).getActiveNames( attribute);
}

/*
 * Class:     Period04
 * Method:    projectGetIDName
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetIDName
  ( JNIEnv * env, jclass clazz, jint id, jint i)
{
  setJni( env, clazz);
  return currentProject->getIDName( i,id).getID( );
}

/*
 * Class:     Period04
 * Method:    projectGetIDNameStr
 * Signature: (II)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetIDNameStr
  ( JNIEnv * env, jclass clazz, jint id, jint i)
{
  setJni( env, clazz);
  return newString( currentProject->getIDNameStr( i,id).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetIDNameWeight
 * Signature: (II)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetIDNameWeight
  ( JNIEnv * env, jclass clazz, jint id, jint i)
{
  setJni( env, clazz);
  return newString( currentProject->getIDNameWeight( i, id).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetIDNameColor
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetIDNameColor
  ( JNIEnv * env, jclass clazz, jint id, jint attribute)
{
  setJni( env, clazz);
  return currentProject->getIDName( attribute, id).getColorID( );
}

/*
 * Class:     Period04
 * Method:    projectGetIDNameColorStr
 * Signature: (II)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetIDNameColorStr
  ( JNIEnv * env, jclass clazz, jint id, jint attribute)
{
  setJni( env, clazz);
  return newString( currentProject->getIDNameColor( attribute, id));
}

/*
 * Class:     Period04
 * Method:    projectTimeStringGetIDName
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_Period04_projectTimeStringGetIDName
  ( JNIEnv * env, jclass clazz, jint row, jint attribute)
{
  setJni( env, clazz);
  return currentProject->getTimeString( )[row].getIDName( attribute);
}

/*
 * Class:     Period04
 * Method:    projectGetIDNameAverageOrig
 * Signature: (II)D
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectGetIDNameAverageOrig
  ( JNIEnv * env, jclass clazz, jint attribute, jint selectedindex)
{
  setJni( env, clazz);
  return currentProject->getIDName( attribute, selectedindex).getAverageOrig( );
}

/*
 * Class:     Period04
 * Method:    projectGetIDNameAverageOrigWeight
 * Signature: (II)D
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectGetIDNameAverageOrigWeight
  ( JNIEnv * env, jclass clazz, jint attribute, jint selectedindex)
{
  setJni( env, clazz);
  return currentProject->getIDName( attribute, selectedindex).getAverageOrigWeight( );
}

/*
 * Class:     Period04
 * Method:    projectGetIDNameAverageAdj
 * Signature: (II)D
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectGetIDNameAverageAdj
  ( JNIEnv * env, jclass clazz, jint attribute, jint selectedindex)
{
  setJni( env, clazz);
  return currentProject->getIDName( attribute, selectedindex).getAverageAdj( );
}

/*
 * Class:     Period04
 * Method:    projectGetIDNameAverageAdjWeight
 * Signature: (II)D
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectGetIDNameAverageAdjWeight
  ( JNIEnv * env, jclass clazz, jint attribute, jint selectedindex)
{
  setJni( env, clazz);
  return currentProject->getIDName( attribute, selectedindex).getAverageAdjWeight( );
}

/*
 * Class:     Period04
 * Method:    projectGetIDNamePoints
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetIDNamePoints
  ( JNIEnv * env, jclass clazz, jint attribute, jint selectedindex)
{
  setJni( env, clazz);
  return currentProject->getIDName( attribute, selectedindex).getPoints( );
}

/*
 * Class:     Period04
 * Method:    projectGetIndexName
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetIndexName
  ( JNIEnv * env, jclass clazz, jint attribute, jint i)
{
  setJni( env, clazz);
  return currentProject->getIndexName( attribute, i).getID( );
}

/*
 * Class:     Period04
 * Method:    projectGetIndexNameStr
 * Signature: (II)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetIndexNameStr
  ( JNIEnv * env, jclass clazz, jint attribute, jint i)
{
  setJni( env, clazz);

//  cerr << attribute << " " << i << " " << currentProject->getIndexNameStr( attribute, i) << endl;

  return newString( currentProject->getIndexNameStr( attribute, i).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetIndexNameID
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetIndexNameID
  ( JNIEnv * env, jclass clazz, jint attribute, jint i)
{
  setJni( env, clazz);
  return currentProject->getIndexNameID( attribute, i);
}

/*
 * Class:     Period04
 * Method:    projectGetIndexNameSelect
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetIndexNameSelect
  ( JNIEnv * env, jclass clazz, jint attribute, jint i)
{
  setJni( env, clazz);
  return currentProject->getIndexNameSelect( attribute, i);
}

/*
 * Class:     Period04
 * Method:    projectChangeHeading
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_Period04_projectChangeHeading
  ( JNIEnv * env, jclass clazz, jint i, jstring heading)
{
  setJni( env, clazz);
  char const * c_heading = getCString( heading);
  currentProject->setAttributeName( i, c_heading);
  releaseString( heading, c_heading);
}

/*
 * Class:     Period04
 * Method:    projectChangeName
 * Signature: (IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_Period04_projectChangeName
  ( JNIEnv * env, jclass clazz, jint id, jint i, jstring name, jstring weight, jstring color)
{
  setJni( env, clazz);

  char const * c_name = getCString( name);
  char const * c_weight = getCString( weight);
  char const * c_color = getCString( color);

  currentProject->changeName( i,id, c_name, c_weight, c_color);

  releaseString( name, c_name);
  releaseString( weight, c_weight);
  releaseString( color, c_color);
}

/*
 * Class:     Period04
 * Method:    projectCalcWeights
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_Period04_projectCalcWeights
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  currentProject->calcWeights( );
}

/*
 * Class:     Period04
 * Method:    projectSelect
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_Period04_projectSelect
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  currentProject->select( );
}

/*
 * Class:     Period04
 * Method:    projectLoadTimeString
 * Signature: (Ljava/lang/String;Ljava/lang/String;IZ)V
 */
JNIEXPORT void JNICALL Java_Period04_projectLoadTimeString
  ( JNIEnv * env, jclass clazz, jstring file, jstring fileformat, jint append
  , jboolean votable
  )
{
  setJni( env, clazz);

  char const * c_str1 = getCString( file);
  char const * c_str2 = getCString( fileformat);

  currentProject->loadTimeString( c_str1, c_str2, append, votable);

  releaseString( file, c_str1);
  releaseString( fileformat, c_str2);
}

/*
 * Class:     Period04
 * Method:    predictFileFormat
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_predictFileFormat
  ( JNIEnv * env, jclass clazz, jstring file)
{
  setJni( env, clazz);

  std::string const & last_format = currentProject->getTimeString( ).getInputFormat( );
  std::string const & backup_formatoffered = currentProject->getInputFormatBackUp( );

  //--- read user-defined default input-format
  std::string defstr = currentProject->getFileFormatString( );

  //--- try to guess input-format of time string file
  char const * c_file = getCString( file);

  std::string formatoffered = currentProject->getTimeString( ).predictFileFormat( c_file);
  releaseString( file, c_file);

  if (backup_formatoffered == formatoffered)
  {
    if (last_format != "t")
      formatoffered = last_format;
  }
  else
  {
    currentProject->setInputFormatBackUp( formatoffered);
  }

  if (defstr != "AUTO")
  {
    // take the user-defined format!
    int len = formatoffered.length( );
    int deflen = defstr.length( );

    if (len < deflen)
    {
      // file has lesser columns, shorten format string to avoid problems
      defstr = defstr.substr( 0, formatoffered.length( ));
    }
    else if (len > deflen)
    {
      // file has more columns, add "ignore" flags to show them as
      // ignored columns otherwise they would not be displayed att all
      len -= deflen;

      for ( int i = 0; i < len; ++i)
      {
        defstr += "i";
      }
    }

    return newString( defstr.c_str( ));
  }

  return newString( formatoffered.c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectSetIDNameSelect
 * Signature: (IIZ)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetIDNameSelect
  ( JNIEnv * env, jclass clazz, jint listboxID, jint nameID, jboolean selected)
{
  setJni( env, clazz);
  currentProject->setIDNameSelect( listboxID, nameID, selected);
}

/*
 * Class:     Period04
 * Method:    projectHasChanged
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_projectHasChanged
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return currentProject->hasChanged( );
}

/*
 * Class:     Period04
 * Method:    projectNoChanges
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_Period04_projectNoChanges
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  currentProject->noChanges( );
}

/*
 * Class:     Period04
 * Method:    projectSetChanged
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetChanged
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  currentProject->setChanged( );
}

/*
 * Class:     Period04
 * Method:    projectNewProject
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_Period04_projectNewProject
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  currentProject->newProject( );
}

/*
 * Class:     Period04
 * Method:    projectClean
 * Signature: (ZZZ)V
 */
JNIEXPORT void JNICALL Java_Period04_projectClean
  ( JNIEnv * env, jclass clazz,
   jboolean fourier, jboolean periods, jboolean timestring)
{
  setJni( env, clazz);
  currentProject->cleanSpecial( fourier, periods, timestring);

  if (batchMode>0)
    {
      std::cout << std::endl;
    }
}

/*
 * Class:     Period04
 * Method:    projectLoadProject
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectLoadProject
  ( JNIEnv * env, jclass clazz, jstring path)
{
  setJni( env, clazz);
  char const * c_path = getCString( path);
  jstring j_str = newString( currentProject->loadProject( c_path).c_str( ));
  releaseString( path, c_path);
  return j_str;
}

/*
 * Class:     Period04
 * Method:    command
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_command
  ( JNIEnv * env, jclass clazz, jstring j_command)
{
  setJni( env, clazz);
  char const * c_command = getCString( j_command);
  jstring j_result = newString( command( c_command).c_str( ));
  releaseString( j_command, c_command);
  return j_result;
}

/*
 * Class:     Period04
 * Method:    projectSaveProject
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSaveProject
  ( JNIEnv * env, jclass clazz, jstring path)
{
  setJni( env, clazz);
  char const * c_path = getCString( path);
  currentProject->saveProject( c_path);
  releaseString( path, c_path);
}

/*
 * Class:     Period04
 * Method:    projectGetProjectFileName
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetProjectFileName
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->getProjectFileName( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetWeightString
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetWeightString
  ( JNIEnv * env, jclass clazz)
{
    setJni( env, clazz);

    int weightflags = currentProject->getUseNameWeight( );
    int usePointWeight = currentProject->getUsePointWeight( );
    int useDeviationWeight = currentProject->getUseDeviationWeight( );

    if (usePointWeight)     { usePointWeight = 16;}
    if (useDeviationWeight) { useDeviationWeight = 32;}

    int useweight = ((weightflags>>1)&15) + usePointWeight + useDeviationWeight;

    static char const * const weightstrings [ ] =
      { " none ", "1     ", " 2    ", "12    ", "  3   ", "1 3   ", " 23   "
      , "123   ", "   4  ", "1  4  ", " 2 4  ", "12 4  ", "  34  ", "1 34  "
      , " 234  ", "1234  ", "    p ", "1   p ", " 2  p ", "12  p ", "  3 p "
      , "1 3 p ", " 23 p ", "123 p ", "   4p ", "1  4p ", " 2 4p ", "12 4p "
      , "  34p ", "1 34p ", " 234p ", "1234p ", "     d", "1    d", " 2   d"
      , "12   d", "  3  d", "1 3  d", " 23  d", "123  d", "   4 d", "1  4 d"
      , " 2 4 d", "12 4 d", "  34 d", "1 34 d", " 234 d", "1234 d", "    pd"
      , "1   pd", " 2  pd", "12  pd", "  3 pd", "1 3 pd", " 23 pd", "123 pd"
      , "   4pd", "1  4pd", " 2 4pd", "12 4pd", "  34pd", "1 34pd", " 234pd"
      , "1234pd"
      }
    ;

    return newString( weightstrings[useweight]);
}

/*
 * Class:     Period04
 * Method:    tstableGetHeader
 * Signature: ()[Ljava/lang/Object;
 */
JNIEXPORT jobjectArray JNICALL Java_Period04_tstableGetHeader
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);

  int columns = 17; ////////////////////////////////////////// <<<---

  jobjectArray ret
    = (jobjectArray)env->NewObjectArray( columns, env->FindClass( "java/lang/Object"), newString(""))
  ;

  env->SetObjectArrayElement( ret, 0, newString( "Time"));
  env->SetObjectArrayElement( ret, 1, newString( "Observed"));
  env->SetObjectArrayElement( ret, 2, newString( "Adjusted"));
  env->SetObjectArrayElement( ret, 3, newString( "Calculated"));
  env->SetObjectArrayElement( ret, 4, newString( "Residuals (Observed)"));
  env->SetObjectArrayElement( ret, 5, newString( "Residuals (Adjusted)"));
  env->SetObjectArrayElement( ret, 6, newString( "Weight"));
  env->SetObjectArrayElement( ret, 7, newString( "Point Weight"));
  env->SetObjectArrayElement( ret, 8, newString( "Point Error"));

  CTimeString & ts = currentProject->getTimeString( );

  env->SetObjectArrayElement( ret,  9, newString( (             ts.nameSet( 0)      ).c_str( )));
  env->SetObjectArrayElement( ret, 10, newString( ("Weight (" + ts.nameSet( 0) + ")").c_str( )));

  env->SetObjectArrayElement( ret, 11, newString( (             ts.nameSet( 1)      ).c_str( )));
  env->SetObjectArrayElement( ret, 12, newString( ("Weight (" + ts.nameSet( 1) + ")").c_str( )));

  env->SetObjectArrayElement( ret, 13, newString( (             ts.nameSet( 2)      ).c_str( )));
  env->SetObjectArrayElement( ret, 14, newString( ("Weight (" + ts.nameSet( 2) + ")").c_str( )));

  env->SetObjectArrayElement( ret, 15, newString( (             ts.nameSet( 3)      ).c_str( )));
  env->SetObjectArrayElement( ret, 16, newString( ("Weight (" + ts.nameSet( 3) + ")").c_str( )));

  return ret;
}

/*
 * Class:     Period04
 * Method:    projectGetData
 * Signature: (I)[Ljava/lang/Object;
 */
JNIEXPORT jobjectArray JNICALL Java_Period04_projectGetData
  ( JNIEnv * env, jclass clazz, jint columnindex)
{
    setJni( env, clazz);
    int i;

    // determine size of the array
    int rows = currentProject->getSelectedPoints();
    //printf("\nJNIEXPORT jobjectArray JNICALL Java_Period04_projectGetData(index: %d): getSelectedPoints(): %d\n",columnindex,rows);

    // create array
    jobjectArray ret;
    ret=(jobjectArray)env->NewObjectArray(
	rows,env->FindClass("java/lang/Object"), newString(""));

    jclass cls=env->FindClass("java/lang/Double");
    jmethodID mid=env->GetMethodID(cls, "<init>", "(D)V");

    switch (columnindex) {                  // FIXEDFORMAT ????
	case 0:
	    for (i=0; i<rows; i++) {
		double time = currentProject->getTimeString()[i].getTime();
		env->SetObjectArrayElement(ret,i,env->NewObject(cls,mid,(double)time));
	    }
	    return ret; break;
	case 1:
	    for (i=0; i<rows; i++) {
		double time = currentProject->getTimeString()[i].getObserved();
		env->SetObjectArrayElement(ret,i,env->NewObject(cls,mid,(double)time));
	    }
	    return ret; break;
	case 2:
	    for (i=0; i<rows; i++) {
		double time = currentProject->getTimeString()[i].getAdjusted();
		env->SetObjectArrayElement(ret,i,env->NewObject(cls,mid,(double)time));
	    }
	    return ret; break;
	case 3:
	    for (i=0; i<rows; i++) {
		double time = currentProject->getTimeString()[i].getCalculated();
		env->SetObjectArrayElement(ret,i,env->NewObject(cls,mid,(double)time));
	    }
	    return ret; break;
	case 4:
	    for (i=0; i<rows; i++) {
		double time = currentProject->getTimeString()[i].getDataResidual();
		env->SetObjectArrayElement(ret,i,env->NewObject(cls,mid,(double)time));
	    }
	    return ret; break;
	case 5:
	    for (i=0; i<rows; i++) {
		double time = currentProject->getTimeString()[i].getAdjustedResidual();
		env->SetObjectArrayElement(ret,i,env->NewObject(cls,mid,(double)time));
	    }
	    return ret; break;
	case 6:
	    for (i=0; i<rows; i++) {
		double time = currentProject->getTimeString()[i].getWeight();
		env->SetObjectArrayElement(ret,i,env->NewObject(cls,mid,(double)time));
	    }
	    return ret; break;
	case 7:
	    for (i=0; i<rows; i++) {
		double time = currentProject->getTimeString()[i].getPointWeight();
		env->SetObjectArrayElement(ret,i,env->NewObject(cls,mid,(double)time));
	    }
	    return ret; break;
	case 8:
	    for (i=0; i<rows; i++) {
		double time = 1/sqrt(currentProject->getTimeString()[i].getPointWeight());
		env->SetObjectArrayElement(ret,i,env->NewObject(cls,mid,(double)time));
	    }
	    return ret; break;
	case 9:
	    for (i=0; i<rows; i++) {
		std::string name = currentProject->getTimeString().getIDName(
		    0, currentProject->getTimeString()[i].getIDName(0)).getName( );
		env->SetObjectArrayElement(ret,i,newString(name.c_str()));
	    }
	    return ret; break;
	case 10:
	    for (i=0; i<rows; i++) {
		double weight = currentProject->getTimeString().getIDName(
		    0, currentProject->getTimeString()[i].getIDName(0)).getWeight();
		env->SetObjectArrayElement(ret,i,env->NewObject(cls,mid,(double)weight));
	    }
	    return ret; break;
	case 11:
	    for (i=0; i<rows; i++) {
		std::string name = currentProject->getTimeString().getIDName(
		    1, currentProject->getTimeString()[i].getIDName(1)).getName( );
		env->SetObjectArrayElement(ret,i,newString(name.c_str()));
	    }
	    return ret; break;
	case 12:
	    for (i=0; i<rows; i++) {
		double weight = currentProject->getTimeString().getIDName(
		    1, currentProject->getTimeString()[i].getIDName(1)).getWeight();
		env->SetObjectArrayElement(ret,i,env->NewObject(cls,mid,(double)weight));
	    }
	    return ret; break;
	case 13:
	    for (i=0; i<rows; i++) {
		std::string name = currentProject->getTimeString().getIDName(
		    2, currentProject->getTimeString()[i].getIDName(2)).getName( );
		env->SetObjectArrayElement(ret,i,newString(name.c_str()));
	    }
	    return ret; break;
	case 14:
	    for (i=0; i<rows; i++) {
		double weight = currentProject->getTimeString().getIDName(
		    2, currentProject->getTimeString()[i].getIDName(2)).getWeight();
		env->SetObjectArrayElement(ret,i,env->NewObject(cls,mid,(double)weight));
	    }
	    return ret; break;
	case 15:
	    for (i=0; i<rows; i++) {
		std::string name = currentProject->getTimeString().getIDName(
		    3, currentProject->getTimeString()[i].getIDName(3)).getName( );
		env->SetObjectArrayElement(ret,i,newString(name.c_str()));
	    }
	    return ret; break;
	case 16:
	    for (i=0; i<rows; i++) {
		double weight = currentProject->getTimeString().getIDName(
		    3, currentProject->getTimeString()[i].getIDName(3)).getWeight();
		env->SetObjectArrayElement(ret,i,env->NewObject(cls,mid,(double)weight));
	    }
	    return ret; break;
    }

    // didn't find the appropriate column (which should not happen)
    for (i=0; i<rows; i++) {
	env->SetObjectArrayElement(ret,i,newString("???"));
    }
    return ret;
}


/*
 * Class:     Period04
 * Method:    timestringGetOutputFormat
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_timestringGetOutputFormat
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->getTimeString( ).getOutputFormat( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    timestringGetFileName
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_timestringGetFileName
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->getTimeString( ).getFileName( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    timestringGetWeightSum
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_Period04_timestringGetWeightSum
  ( JNIEnv * env, jclass clazz)
{
    setJni( env, clazz);
    return currentProject->getTimeString().getWeightSum();
}

/*
 * Class:     Period04
 * Method:    projectSaveTimeString
 * Signature: (Ljava/lang/String;Ljava/lang/String;Z)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSaveTimeString
  ( JNIEnv * env, jclass clazz, jstring filename, jstring outputformat, jboolean votable)
{
  setJni( env, clazz);

  char const * c_filename = getCString( filename);
  char const * c_outputformat = getCString( outputformat);

  currentProject->saveTimeString( c_filename, c_outputformat, votable);

  releaseString( filename, c_filename);
  releaseString( outputformat, c_outputformat);
}

/*
 * Class:     Period04
 * Method:    projectSaveTimeStringPhase
 * Signature: (Ljava/lang/String;Ljava/lang/String;DDZ)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSaveTimeStringPhase
  ( JNIEnv * env, jclass clazz, jstring filename, jstring outputformat,
   jdouble frequency, jdouble zeropoint, jboolean votable)
{
  setJni( env, clazz);

  char const * c_filename = getCString( filename);
  char const * c_outputformat = getCString( outputformat);

  currentProject->saveTimeStringPhase( c_filename, c_outputformat, frequency, zeropoint, votable);

  releaseString( filename, c_filename);
  releaseString( outputformat, c_outputformat);
}

/*
 * Class:     Period04
 * Method:    projectSaveTimeStringPhaseBinned
 * Signature: (Ljava/lang/String;Ljava/lang/String;DDII[D[D[D)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSaveTimeStringPhaseBinned
  ( JNIEnv * env, jclass clazz, jstring filename, jstring outputformat,
   jdouble frequency, jdouble binSpacing, jint useData, jint points,
   jdoubleArray phase, jdoubleArray amplitude, jdoubleArray sigma)
{
  setJni( env, clazz);

  //--- convert java variables to c++
  char const * c_filename = getCString( filename);
  char const * c_outputformat = getCString( outputformat);

  jdouble * phase_array     = env->GetDoubleArrayElements( phase,     0);
  jdouble * amplitude_array = env->GetDoubleArrayElements( amplitude, 0);
  jdouble * sigma_array     = env->GetDoubleArrayElements( sigma,     0);

  currentProject->saveTimeStringPhaseBinned
    ( c_filename
    , c_outputformat
    , frequency
    , binSpacing
    , (DataMode)(int)useData
    , points
    , (double *) phase_array
    , (double *) amplitude_array
    , (double *) sigma_array
    )
  ;

  //--- release the data
  releaseString( filename, c_filename);
  releaseString( outputformat, c_outputformat);
  env->ReleaseDoubleArrayElements( phase,     phase_array,     0);
  env->ReleaseDoubleArrayElements( amplitude, amplitude_array, 0);
  env->ReleaseDoubleArrayElements( sigma,     sigma_array,     0);
}

/*
 * Class:     Period04
 * Method:    projectGetDataMode
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetDataMode
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return (int) currentProject->getTimeString( ).getDataMode( );
}

/*
 * Class:     Period04
 * Method:    projectSetDataMode
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetDataMode
  ( JNIEnv * env, jclass clazz, jint mode)
{
  setJni( env, clazz);
  currentProject->getTimeString( ).setDataMode( (DataMode)((int)mode));
}

/*
 * Class:     Period04
 * Method:    timestringAverage
 * Signature: (I)D
 */
JNIEXPORT jdouble JNICALL Java_Period04_timestringAverage
  ( JNIEnv * env, jclass clazz, jint weight)
{
  setJni( env, clazz);
    /*   char txt[30];
	 sprintf(txt, FORMAT_AMPLITUDE, currentProject->getTimeString().average(weight));
	 return txt;*/
  return currentProject->getTimeString( ).average( weight);
}

/*
 * Class:     Period04
 * Method:    projectGetTimeAverage
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectGetTimeAverage
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return currentProject->getTimeAverage( );
}

/*
 * Class:     Period04
 * Method:    projectIsCalcErrors
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_Period04_projectIsCalcErrors
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return currentProject->isCalcErrors( );
}

/*
 * Class:     Period04
 * Method:    projectSetCalcErrors
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetCalcErrors
  ( JNIEnv * env, jclass clazz, jboolean value)
{
  setJni( env, clazz);
  currentProject->setCalcErrors( value);
}

/*
 * Class:     Period04
 * Method:    projectSetShiftTimes
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetShiftTimes
  ( JNIEnv * env, jclass clazz, jboolean value)
{
  setJni( env, clazz);
  currentProject->setShiftTimes( value);
}

/*
 * Class:     Period04
 * Method:    projectSetMoCaSimulations
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetMoCaSimulations
  ( JNIEnv * env, jclass clazz, jint sim)
{
  setJni( env, clazz);
  currentProject->setMoCaSimulations( sim);
}

/*
 * Class:     Period04
 * Method:    projectSetInitialCalcMode
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetInitialCalcMode
  ( JNIEnv * env, jclass clazz, jint mode)
{
    setJni( env, clazz);
    currentProject->setMCInitialCalcMode((DataMode)(int)mode);
}

/*
 * Class:     Period04
 * Method:    projectInitRand
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_Period04_projectInitRand
  ( JNIEnv * env, jclass clazz, jboolean systime)
{
    setJni( env, clazz);
    if ((bool)systime) { init_genrand(time(NULL)); } // use system time as seed
    else               { init_genrand(1); }          // use 1 as seed
}

/*
 * Class:     Period04
 * Method:    projectCalculateAverage
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_Period04_projectCalculateAverage
  ( JNIEnv * env, jclass clazz)
{
    setJni( env, clazz);
    currentProject->calcAverage();
}

/*
 * Class:     Period04
 * Method:    projectGetNameStatistics
 * Signature: (IIZ)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetNameStatistics
  ( JNIEnv * env, jclass clazz, jint attribute, jint entryID, jboolean useweights)
{
  setJni( env, clazz);
  return newString( currentProject->getNameStatistics( attribute, entryID, useweights).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetTimeStatistics
 * Signature: (IZ)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetTimeStatistics
  ( JNIEnv * env, jclass clazz, jint attribute, jboolean isDays)
{
  setJni( env, clazz);
  return newString( currentProject->getTimeStatistics( attribute, isDays).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectSubdivide
 * Signature: (DIZLjava/lang/String;I)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSubdivide__DIZLjava_lang_String_2I
  ( JNIEnv * env, jclass clazz, jdouble gapsize, jint attribute, jboolean useCounter, jstring prefix, jint digits)
{
  setJni( env, clazz);

  char const * c_prefix = getCString( prefix);
  int count = (useCounter) ? 1 : 0;

  currentProject->subdivide( gapsize, attribute, count, c_prefix, digits);

  releaseString( prefix, c_prefix);
}

/*
 * Class:     Period04
 * Method:    projectSubdivide
 * Signature: (DDIZLjava/lang/String;I)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSubdivide__DDIZLjava_lang_String_2I
  ( JNIEnv * env, jclass clazz, jdouble start, jdouble interval, jint attribute, jboolean useCounter, jstring prefix,
   jint digits)
{
  setJni( env, clazz);

  char const * c_prefix = getCString( prefix);
  int count = (useCounter) ? 1 : 0;

  currentProject->subdivide( start, interval, attribute, count, c_prefix, digits);

  releaseString( prefix, c_prefix);
}

/*
 * Class:     Period04
 * Method:    projectCombineSubstrings
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_Period04_projectCombineSubstrings
  ( JNIEnv * env, jclass clazz, jint attribute, jstring label)
{
  setJni( env, clazz);
  char const * c_label = getCString( label);
  currentProject->combine( attribute, c_label);
  releaseString( label, c_label);
}

/*
 * Class:     Period04
 * Method:    projectAdjustData
 * Signature: (III[IZ)V
 */
JNIEXPORT void JNICALL Java_Period04_projectAdjustData
  ( JNIEnv * env, jclass clazz, jint attribute, jint usePeriodWeight, jint selected, jintArray selectedindices, jboolean useWeight)
{
  setJni( env, clazz);

  //jsize len = env->GetArrayLength( selectedindices); // Laenge des Arrays bestimmen
  jint * body = env->GetIntArrayElements( selectedindices, 0); // Java-Array zu C-Array konvertieren

  currentProject->adjust( attribute, (DataMode) (int) usePeriodWeight, selected, (int *) body, useWeight);

  env->ReleaseIntArrayElements( selectedindices, body, 0);
}

/*
 * Class:     Period04
 * Method:    timestringSubtractZeroPoint
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_timestringSubtractZeroPoint
  ( JNIEnv * env, jclass clazz)
{
    setJni( env, clazz);
    return newString( currentProject->subtractZeroPoint( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectSaveStat
 * Signature: (IZLjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSaveStat
  ( JNIEnv * env, jclass clazz, jint attribute, jboolean useWeights, jstring filename)
{
  setJni( env, clazz);

  char const * c_filename = getCString( filename);
  int useweight = (useWeights) ? 1 : 0;

  currentProject->saveStat( attribute, useweight, c_filename);
  releaseString( filename, c_filename);
}

/*
 * Class:     Period04
 * Method:    projectGetDeletePointAttribute
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetDeletePointAttribute
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);

  int att;
  std::string name;

  currentProject->getDeletePointInfo( att, name);

  return att;
}

/*
 * Class:     Period04
 * Method:    projectGetDeletePointName
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetDeletePointName
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);

  int att;
  std::string name;

  currentProject->getDeletePointInfo( att, name);

  return newString( name.c_str( ));
}

 /*
 * Class:     Period04
 * Method:    projectSetDeletePointInfo
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetDeletePointInfo
  ( JNIEnv * env, jclass clazz, jint attribute, jstring name)
{
    setJni( env, clazz);

    char const * c_name = getCString( name);

    currentProject->setDeletePointInfo( attribute, c_name);

    releaseString( name, c_name);
}

/*
 * Class:     Period04
 * Method:    projectDeleteSelectedPoints
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_Period04_projectDeleteSelectedPoints
  ( JNIEnv * env, jclass clazz)
{
    setJni( env, clazz);
    currentProject->deleteSelectedPoints();
}

/*
 * Class:     Period04
 * Method:    projectMakeObservedAdjusted
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectMakeObservedAdjusted
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->makeObservedAdjusted( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectMakeDataResidualsObserved
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectMakeDataResidualsObserved
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->makeDataResidualsObserved( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectMakeAdjResidualsObserved
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectMakeAdjResidualsObserved
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->makeAdjResidualsObserved( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectMakeCalculatedObserved
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectMakeCalculatedObserved
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->makeCalculatedObserved( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectMakeAdjustedObserved
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectMakeAdjustedObserved
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->makeAdjustedObserved( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetStepRate
 * Signature: (Ljava/lang/String;D)D
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectGetStepRate
  ( JNIEnv * env, jclass clazz, jstring stepquality, jdouble step)
{
  setJni( env, clazz);
  char const * c_stepquality = getCString( stepquality);
  // set stepquality
  StepQuality stepq = HIGH; // set HIGH as default
  if (strcmp(c_stepquality,"Medium")==0)      { stepq = MEDIUM; }
  else if (strcmp(c_stepquality,"Low")==0)    { stepq = LOW;    }
  else if (strcmp(c_stepquality,"Custom")==0) { stepq = CUSTOM; }
  releaseString(stepquality,c_stepquality);

  return currentProject->getStepRate(stepq, step);
}

/*
 * Class:     Period04
 * Method:    projectGetStepRateString
 * Signature: (Ljava/lang/String;D)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetStepRateString
  ( JNIEnv * env, jclass clazz, jstring stepquality, jdouble step)
{
    setJni( env, clazz);
    char const * c_stepquality = getCString( stepquality);

    // translate stepquality
    StepQuality stepq = HIGH; // set HIGH as default
    if (strcmp(c_stepquality,"Medium")==0)      { stepq = MEDIUM; }
    else if (strcmp(c_stepquality,"Low")==0)    { stepq = LOW;    }
    else if (strcmp(c_stepquality,"Custom")==0) { stepq = CUSTOM; }
    releaseString( stepquality, c_stepquality);

    double steprate = currentProject->getStepRate(stepq, step);
    char txt[32];
    sprintf(txt,FORMAT_FREQUENCY,steprate);
    return newString(txt);
}

/*
 * Class:     Period04
 * Method:    projectTimePointGetTime
 * Signature: (I)D
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectTimePointGetTime
  ( JNIEnv * env, jclass clazz, jint i)
{
  setJni( env, clazz);
  return currentProject->getTimeString( )[i].getTime( );
}

/*
 * Class:     Period04
 * Method:    timepointGetPhasedTime
 * Signature: (IDD)D
 */
JNIEXPORT jdouble JNICALL Java_Period04_timepointGetPhasedTime
  ( JNIEnv * env, jclass clazz, jint i, jdouble freq, jdouble zero)
{
  setJni( env, clazz);
  return currentProject->getTimeString( )[i].getPhasedTime( freq, zero);
}

/*
 * Class:     Period04
 * Method:    projectTimePointGetAmplitude
 * Signature: (II)D
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectTimePointGetAmplitude
  ( JNIEnv * env, jclass clazz, jint i, jint useData)
{
    setJni( env, clazz);
    switch (useData) {
	case 0: // observed
	    return currentProject->getTimeString()[i].getObserved();
	case 1: // adjusted
	    return currentProject->getTimeString()[i].getAdjusted();
	case 2: // residuals (obs)
	    return currentProject->getTimeString()[i].getDataResidual();
	case 3: // residuals (adj)
	    return currentProject->getTimeString()[i].getAdjustedResidual();
	default:
	    return 0;
    }
}

// Lecture 5: Introduction to JNI
// http://www-stat.stanford.edu/~naras/java/course/lec5/lec5.html#NWDH

JNIEXPORT jdoubleArray JNICALL Java_Period04_projectTimePointGet
  ( JNIEnv * env, jclass clazz, jint i, jint useData)
{
  setJni( env, clazz);

  jdoubleArray result = env->NewDoubleArray( 2);
  jboolean result_isCopy;
  jdouble * result_elements = env->GetDoubleArrayElements( result, &result_isCopy);

  CTimePoint const & timepoint = currentProject->getTimeString( )[i];

  result_elements[0] = timepoint.getTime( );

  switch (useData)
  {
  case 0: // observed
    result_elements[1] = timepoint.getObserved( );
    break;
  case 1: // adjusted
    result_elements[1] = timepoint.getAdjusted( );
    break;
  case 2: // residuals (obs)
    result_elements[1] = timepoint.getDataResidual( );
    break;
  case 3: // residuals (adj)
    result_elements[1] = timepoint.getAdjustedResidual( );
    break;
  default:
    result_elements[1] =  0;
    break;
  }

  if (JNI_TRUE == result_isCopy)
  {
    env->ReleaseDoubleArrayElements( result, result_elements, 0);
  }

  return result;
}

/*
 * Class:     Period04
 * Method:    projectTimePointGetIDName
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_Period04_projectTimePointGetIDName
  ( JNIEnv * env, jclass clazz, jint attribute, jint i)
{
    setJni( env, clazz);
    return currentProject->getTimeString()[i].getIDName(attribute);
}

/*
 * Class:     Period04
 * Method:    projectDeletePoint
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectDeletePoint
  ( JNIEnv * env, jclass clazz, jint row)
{
    setJni( env, clazz);
    return newString( currentProject->deletePoint( row).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectDeletePointsWithinBox
 * Signature: (DDDDI)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectDeletePointsWithinBox
  ( JNIEnv * env, jclass clazz,
   jdouble x_start, jdouble y_start, jdouble x_end, jdouble y_end, jint mode)
{
    setJni( env, clazz);
    return newString( currentProject->deletePointsWithinBox
      ( x_start, y_start, x_end, y_end, mode).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectRelabelPoint
 * Signature: (ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectRelabelPoint
  ( JNIEnv * env, jclass clazz, jint row
  , jstring attr1, jstring attr2, jstring attr3, jstring attr4
  )
{
  setJni( env, clazz);

  return newString( currentProject->relabelPoint
    ( row
    , getCString( attr1)
    , getCString( attr2)
    , getCString( attr3)
    , getCString( attr4)
    ).c_str( )
  );
}

/*
 * Class:     Period04
 * Method:    fourierGetProperties
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_fourierGetProperties
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->getActiveFourierProperties( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    fourierGetTitle
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_fourierGetTitle
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->getFourierActive( )->getTitle( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectCheckFourierTitle
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectCheckFourierTitle
  ( JNIEnv * env, jclass clazz, jstring title)
{
    setJni( env, clazz);
    char const * c_title = getCString( title);
    std::string newtitle = currentProject->checkFourierTitle( c_title);
    releaseString( title, c_title);
    return newString( newtitle.c_str( ));
}

/*
 * Class:     Period04
 * Method:    fourierGetFrom
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_fourierGetFrom
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  char txt [256];
  sprintf( txt, FORMAT_FREQUENCY, currentProject->getFourierActive( )->getFrom( ));
  return newString(txt);
}

/*
 * Class:     Period04
 * Method:    fourierGetTo
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_fourierGetTo
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  char txt [256];
  sprintf( txt, FORMAT_FREQUENCY, currentProject->getFourierActive( )->getTo( ));
  return newString( txt);
}

/*
 * Class:     Period04
 * Method:    projectGetNyquist
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectGetNyquist
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return currentProject->getNyquist( );
}

/*
 * Class:     Period04
 * Method:    projectGetSpecialNyquist
 * Signature: (DD)D
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectGetSpecialNyquist
  ( JNIEnv * env, jclass clazz, jdouble t_start, jdouble t_end)
{
  setJni( env, clazz);
  return currentProject->getSpecialNyquist( t_start, t_end);
}

/*
 * Class:     Period04
 * Method:    projectGetNyquistString
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetNyquistString
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  char txt [256];
  sprintf( txt, "%g", currentProject->getNyquist( ));      // FORMAT FREQ??
  return newString( txt);
}

/*
 * Class:     Period04
 * Method:    fourierGetStepQuality
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_fourierGetStepQuality
  ( JNIEnv * env, jclass clazz)
{
    setJni( env, clazz);
    StepQuality stepq = currentProject->getFourierActive()->getStepQuality();
    char* stepquality;
    // get the appropriate string
    switch (stepq) {
	case HIGH:    stepquality=(char*)"High";   break;
	case MEDIUM:  stepquality=(char*)"Medium"; break;
	case LOW:     stepquality=(char*)"Low";    break;
	case CUSTOM:  stepquality=(char*)"Custom"; break;
	default:      stepquality=(char*)"High";   break;
    }
    return newString(stepquality);
}

/*
 * Class:     Period04
 * Method:    fourierGetStepping
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_Period04_fourierGetStepping
  ( JNIEnv * env, jclass clazz)
{
    setJni( env, clazz);
    return currentProject->getFourierActive()->getStepping();
}

/*
 * Class:     Period04
 * Method:    fourierGetMode
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_fourierGetMode
  ( JNIEnv * env, jclass clazz)
{
	setJni( env, clazz);
	return (int)currentProject->getFourierActive()->getMode();
}

/*
 * Class:     Period04
 * Method:    fourierSetMode
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_Period04_fourierSetMode
  ( JNIEnv * env, jclass clazz, jint mode)
{
	setJni( env, clazz);
	currentProject->getFourierActive()->setMode( (DataMode) ((int)mode) );
}

/*
 * Class:     Period04
 * Method:    fourierGetCompact
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_fourierGetCompact
  ( JNIEnv * env, jclass clazz)
{
	setJni( env, clazz);
	return (int)currentProject->getFourierActive()->getCompact();
}

/*
 * Class:     Period04
 * Method:    fourierSetCompact
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_Period04_fourierSetCompact
  ( JNIEnv * env, jclass clazz, jint compact)
{
	setJni( env, clazz);
	currentProject->getFourierActive()->setCompact( (CompactMode) ((int)compact) );
}

/*
 * Class:     Period04
 * Method:    fourierActiveGetPeak
 * Signature: ()[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_Period04_fourierActiveGetPeak
  ( JNIEnv * env, jclass clazz)
{
    setJni( env, clazz);
    jobjectArray ret;

    ret=(jobjectArray)env->NewObjectArray(2,env->FindClass("java/lang/String"),
					  newString(""));
    CPeriPoint tmp = currentProject->getFourierActive()->getPeak(); // get peak
    char txt[256];
    sprintf(txt, FORMAT_FREQUENCY, tmp.getFrequency());  // get its frequency
    env->SetObjectArrayElement(ret,0,newString(txt));
    sprintf(txt, FORMAT_AMPLITUDE, tmp.getAmplitude(-1));  // get its ampitude
    env->SetObjectArrayElement(ret,1,newString(txt));
    return ret;     // now return frequency and amplitude
}

/*
 * Class:     Period04
 * Method:    fourierGetPeak
 * Signature: (I)[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_Period04_fourierGetPeak
  ( JNIEnv * env, jclass clazz, jint i)
{
    setJni( env, clazz);
    jobjectArray ret;

    ret=(jobjectArray)env->NewObjectArray(2,env->FindClass("java/lang/String"),
					  newString(""));
    CPeriPoint tmp = currentProject->getFourierEntry(i)->getPeak(); // get peak
    char txt[256];
    sprintf(txt, FORMAT_FREQUENCY, tmp.getFrequency());  // get its frequency
    env->SetObjectArrayElement(ret,0,newString(txt));
    sprintf(txt, FORMAT_AMPLITUDE, tmp.getAmplitude(-1));  // get its ampitude
    env->SetObjectArrayElement(ret,1,newString(txt));
    return ret;     // now return frequency and amplitude
}

/*
 * Class:     Period04
 * Method:    projectIsFrequencyPeakComposition
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectIsFrequencyPeakComposition
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  CPeriPoint tmp = currentProject->getFourierActive( )->getPeak( ); // get peak
  return newString( currentProject->isPeakComposition( tmp.getFrequency( )).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetFreqResForCompoSearch
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectGetFreqResForCompoSearch
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return currentProject->getFreqResForCompoSearch();
}

/*
 * Class:     Period04
 * Method:    projectSetFreqResForCompoSearch
 * Signature: (D)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetFreqResForCompoSearch
  ( JNIEnv * env, jclass clazz, jdouble df)
{
  setJni( env, clazz);
  currentProject->setFreqResForCompoSearch(df);
}


/*
 * Class:     Period04
 * Method:    projectGetUseFourierWeight
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetUseFourierWeight
  ( JNIEnv * env, jclass clazz)
{
    setJni( env, clazz);
    return currentProject->getFourierUseWeight();
}

/*
 * Class:     Period04
 * Method:    projectCalculateFourier
 * Signature: (Ljava/lang/String;DDIDIIID)V
 */
JNIEXPORT void JNICALL Java_Period04_projectCalculateFourier
  ( JNIEnv * env, jclass clazz, jstring title, jdouble from, jdouble to, jint stepquality, jdouble customsteprate, jint datamode, jint compactmode, jint weight, jdouble zeropoint)
{
  setJni( env, clazz);

  char const * c_title = getCString( title);
  StepQuality qual;

  switch ((int) stepquality)
  {
  case 0: qual = HIGH; break;
  case 1: qual = MEDIUM; break;
  case 2: qual = LOW; break;
  case 3: qual = CUSTOM; break;
  default: qual = HIGH; break;
  }

  currentProject->calculateFourier
    ( c_title
    , from
    , to
    , qual
    , customsteprate
    , (DataMode) ((int) datamode)
    , (CompactMode) ((int) compactmode)
    , weight
    , zeropoint
    )
  ;

  releaseString( title, c_title);
}

/*
 * Class:     Period04
 * Method:    projectCalculateNoiseSpectrum
 * Signature: (DDDDIDID)V
 */
JNIEXPORT void JNICALL Java_Period04_projectCalculateNoiseSpectrum
  ( JNIEnv * env, jclass clazz, jdouble from, jdouble to, jdouble spacing, jdouble boxsize, jint stepquality, jdouble customsteprate, jint datamode, jdouble zeropoint)
{
    setJni( env, clazz);
    StepQuality qual;
    switch ((int)stepquality) {
	case 0: qual=HIGH; break;
	case 1: qual=MEDIUM; break;
	case 2: qual=LOW; break;
	case 3: qual=CUSTOM; break;
	default:qual=HIGH;
    }
    currentProject->calculateNoiseSpectrum(from, to, spacing, boxsize, qual,
				   customsteprate, (DataMode)((int)datamode),
				   zeropoint);
}

/*
 * Class:     Period04
 * Method:    projectCalculateNoiseAtFrequency
 * Signature: (DDIDID)V
 */
JNIEXPORT void JNICALL Java_Period04_projectCalculateNoiseAtFrequency
  ( JNIEnv * env, jclass clazz, jdouble freq, jdouble boxsize, jint stepquality, jdouble steprate, jint datamode, jdouble zeropoint)
{
    setJni( env, clazz);
    StepQuality qual;
    switch ((int)stepquality) {
	case 0: qual=HIGH; break;
	case 1: qual=MEDIUM; break;
	case 2: qual=LOW; break;
	case 3: qual=CUSTOM; break;
	default:qual=HIGH;
    }
    currentProject->calculateNoise(freq, boxsize, qual, steprate,
		(DataMode)((int)datamode), zeropoint);
}

/*
 * Class:     Period04
 * Method:    projectGetFourierEntries
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetFourierEntries
  ( JNIEnv * env, jclass clazz)
{
	setJni( env, clazz);
	return currentProject->getFourierEntries();
}

/*
 * Class:     Period04
 * Method:    projectGetFourierTitle
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetFourierTitle
  ( JNIEnv * env, jclass clazz, jint i)
{
  setJni( env, clazz);
  return newString( currentProject->getFourierTitle( i).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetFourierActiveTitle
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetFourierActiveTitle
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->getFourierActive( )->getTitle( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetFourierActiveIndex
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetFourierActiveIndex
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  // do not use currentProject->getFourierActive( )->getFourierID( );
  // get the index from the fourierlist instead
  return currentProject->getFourierActiveListID( );
}

/*
 * Class:     Period04
 * Method:    projectActivateFourier
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_Period04_projectActivateFourier
  ( JNIEnv * env, jclass clazz, jint fourierID)
{
  setJni( env, clazz);
  currentProject->activateFourier( fourierID);
}

/*
 * Class:     Period04
 * Method:    projectDeleteActiveFourier
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_Period04_projectDeleteActiveFourier
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  currentProject->deleteActiveFourier( );
}

/*
 * Class:     Period04
 * Method:    projectRenameActiveFourier
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_Period04_projectRenameActiveFourier
  ( JNIEnv * env, jclass clazz, jstring path)
{
  setJni( env, clazz);

  if (NULL != path)
  {
    char const * c_path = getCString( path);
    currentProject->renameActiveFourier( c_path);
    releaseString( path, c_path);
  }
}

/*
 * Class:     Period04
 * Method:    projectSaveFourier
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSaveFourier
  ( JNIEnv * env, jclass clazz, jint selectedID, jstring path)
{
  setJni( env, clazz);
  char const * c_path = getCString( path);
  currentProject->saveFourier( *(currentProject->getFourierEntry( selectedID)), c_path);
  releaseString( path, c_path);
}

/*
 * Class:     Period04
 * Method:    projectSaveAllFourier
 * Signature: (Ljava/lang/String;ZLjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSaveAllFourier
  ( JNIEnv * env, jclass clazz, jstring directory, jboolean usePrefix, jstring prefix)
{
  setJni( env, clazz);

  char const * c_directory = getCString( directory);
  char const * c_prefix = getCString( prefix);

  currentProject->saveAllFourier( c_directory, usePrefix, c_prefix);

  releaseString( directory, c_directory);
  releaseString( prefix, c_prefix);
}

/*
 * Class:     Period04
 * Method:    projectSaveSpecialFourier
 * Signature: (Ljava/lang/String;[IZLjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSaveSpecialFourier
  ( JNIEnv * env, jclass clazz, jstring directory, jintArray selectedindices, jint selected, jboolean usePrefix, jstring prefix)
{
  setJni( env, clazz);

  char const * c_directory = getCString( directory);
  char const * c_prefix = getCString( prefix);

  jint * body = env->GetIntArrayElements( selectedindices, 0); // Java-Array zu C-Array konvertieren

  currentProject->saveSpecialFourier( c_directory, (int *) body, selected, usePrefix, c_prefix);

  releaseString( directory, c_directory);
  releaseString( prefix, c_prefix);

  env->ReleaseIntArrayElements( selectedindices, body, 0);
}

/*
 * Class:     Period04
 * Method:    projectGetFourierActivePoints
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetFourierActivePoints
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return currentProject->getFourierActive( )->points( );
}

/*
 * Class:     Period04
 * Method:    projectGetFourierPointFrequency
 * Signature: (I)D
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectGetFourierPointFrequency
  ( JNIEnv * env, jclass clazz, jint i)
{
  setJni( env, clazz);
  return (*currentProject->getFourierActive( ))[i].getFrequency( );
}

/*
 * Class:     Period04
 * Method:    projectGetFourierPointAmplitude
 * Signature: (I)D
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectGetFourierPointAmplitude
  ( JNIEnv * env, jclass clazz, jint i)
{
  setJni( env, clazz);
  return (*currentProject->getFourierActive( ))[i].getAmplitude( );
}



/*
 * Class:     Period04
 * Method:    fourierTableGetData
 * Signature: (I)[Ljava/lang/Object;
 */
JNIEXPORT jobjectArray JNICALL Java_Period04_fourierTableGetData
  ( JNIEnv * env, jclass clazz, jint columnindex)
{
    setJni( env, clazz);
    int i;

    // determine size of the array
    int rows = currentProject->getFourierActive()->points();
    //printf("\nJNIEXPORT jobjectArray JNICALL Java_Period04_fourierTableGetData(index: %d): getFourierActive()->points(): %d\n",columnindex,rows);

    // create array
    jobjectArray ret;
    ret=(jobjectArray)env->NewObjectArray(
	rows,env->FindClass("java/lang/Object"), newString(""));

    jclass cls=env->FindClass("java/lang/Double");
    jmethodID mid=env->GetMethodID(cls, "<init>", "(D)V");

    switch (columnindex) {                  // FIXEDFORMAT ????
	case 0:
	    for (i=0; i<rows; i++) {
		double frequency = (*currentProject->getFourierActive())[i].getFrequency();
		env->SetObjectArrayElement(ret,i,env->NewObject(cls,mid,(double)frequency));
	    }
	    return ret; break;
	case 1:
	    for (i=0; i<rows; i++) {
		double amplitude = (*currentProject->getFourierActive())[i].getAmplitude();
		env->SetObjectArrayElement(ret,i,env->NewObject(cls,mid,(double)amplitude));
	    }
	    return ret; break;
	case 2:
	    for (i=0; i<rows; i++) {
		double amplitude = (*currentProject->getFourierActive())[i].getAmplitude();
		env->SetObjectArrayElement(ret,i,env->NewObject(cls,mid,(double)(amplitude*amplitude)));
	    }
	    return ret; break;
	default:
	    // didn't find the appropriate column (which should not happen)
	    for (i=0; i<rows; i++) {
		env->SetObjectArrayElement(ret,i,newString("???"));
	    }
	    return ret;
    }
}

/*
 * Class:     Period04
 * Method:    projectGetNoisePoints
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetNoisePoints
  ( JNIEnv * env, jclass clazz)
{
	setJni( env, clazz);
	return currentProject->getNoisePoints();
}




/*
 * Class:     Period04
 * Method:    projectGetNoiseString
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetNoiseString
  ( JNIEnv * env, jclass clazz, jint i)
{
    setJni( env, clazz);
    char txt[100];
    sprintf(txt, FIXEDFORMAT_FREQUENCY"\t"FIXEDFORMAT_AMPLITUDE,
	    currentProject->getNoiseFrequency(i), currentProject->getNoiseAmplitude(i));
    return newString(txt);
}

/*
 * Class:     Period04
 * Method:    projectGetSubPeakNoiseString
 * Signature: (DDI)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetSubPeakNoiseString
  ( JNIEnv * env, jclass clazz, jdouble fr, jdouble binscale, jint i)
{
    setJni( env, clazz);
    char txt[100];
    sprintf(txt, FIXEDFORMAT_FREQUENCY"\t"FIXEDFORMAT_AMPLITUDE,
	    currentProject->getSubPeakFrequency(fr,binscale), currentProject->getNoiseAmplitude(i));
    return newString(txt);
}


/*
 * Class:     Period04
 * Method:    projectGetNoise
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetNoise
  ( JNIEnv * env, jclass clazz)
{
    setJni( env, clazz);
    char txt[50];
    sprintf(txt, FIXEDFORMAT_AMPLITUDE, currentProject->getNoiseAmplitude(0));
    return newString(txt);
}

/*
 * Class:     Period04
 * Method:    projectGetNoiseValue
 * Signature: ()D;
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectGetNoiseValue
  ( JNIEnv * env, jclass clazz)
{
    setJni( env, clazz);
    return currentProject->getNoiseAmplitude(0);
}

/*
 * Class:     Period04
 * Method:    projectGetSubPeakFrequency
 * Signature: (D)D;
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectGetSubPeakFrequency
  ( JNIEnv * env, jclass clazz, jdouble fr, double binscale)
{
    setJni( env, clazz);
    return currentProject->getSubPeakFrequency(fr, binscale);
}

/*
 * Class:     Period04
 * Method:    projectGetSubPeakAmplitude
 * Signature: (D)D;
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectGetSubPeakAmplitude
  ( JNIEnv * env, jclass clazz, jdouble fr, double binscale)
{
    setJni( env, clazz);
    return currentProject->getSubPeakAmplitude(fr, binscale);
}

/*
 * Class:     Period04
 * Method:    projectGetFrequencyAdjustment
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetFrequencyAdjustment
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->getFrequencyAdjustment( ).c_str( ));
}


/*
 * Class:     Period04
 * Method:    projectSetFrequencyAdjustment
 * Signature: (D)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetFrequencyAdjustment
  ( JNIEnv * env, jclass clazz, jdouble adj)
{
  setJni( env, clazz);
  currentProject->setFrequencyAdjustment( adj);
}

/*
 * Class:     Period04
 * Method:    projectSetSW2Frequency
 * Signature: (D)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetSW2Frequency
  ( JNIEnv * env, jclass clazz, jdouble fre)
{
  setJni( env, clazz);
  currentProject->setSW2Frequency( fre);
}

/*
 * Class:     Period04
 * Method:    projectCleanFourier
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_Period04_projectCleanFourier
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  currentProject->cleanFourier( );
}

/*
 * Class:     Period04
 * Method:    projectAddFrequencyPeak
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_projectAddFrequencyPeak
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return currentProject->addFrequencyPeak( );
}

/*
 * Class:     Period04
 * Method:    projectAddFrequencySubPeak
 * Signature: (DD)I
 */
JNIEXPORT jint JNICALL Java_Period04_projectAddFrequencySubPeak
  ( JNIEnv * env, jclass clazz, jdouble fr, jdouble amp)
{
  setJni( env, clazz);
  return currentProject->addFrequencyPeak( fr, amp);
}

/*
 * Class:     Period04
 * Method:    projectLoadFrequencies
 * Signature: (Ljava/lang/String;I)V
 */
JNIEXPORT void JNICALL Java_Period04_projectLoadFrequencies
  ( JNIEnv * env, jclass clazz, jstring path, jint mode)
{
  setJni( env, clazz);
  char const * c_path = getCString( path);
  currentProject->loadPeriod( c_path, mode);
  releaseString( path, c_path);
}

/*
 * Class:     Period04
 * Method:    projectCopyFrequencies
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_Period04_projectCopyFrequencies
  ( JNIEnv * env, jclass clazz, jboolean toPTS)
{
  setJni( env, clazz);
  currentProject->copyPeriods( toPTS);
}

/*
 * Class:     Period04
 * Method:    projectSaveFrequencies
 * Signature: (Ljava/lang/String;IZ)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSaveFrequencies
  ( JNIEnv * env, jclass clazz, jstring path, jint mode, jboolean isUseLatex)
{
  setJni( env, clazz);
  char const * c_path = getCString( path);
  currentProject->savePeriod( c_path, mode, isUseLatex);
  releaseString( path, c_path);
}

/*
 * Class:     Period04
 * Method:    projectGetFrequenciesForPrinting
 * Signature: (Ljava/lang/String;IZ)V
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetFrequenciesForPrinting
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->getFrequenciesForPrinting( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectSaveFitError
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSaveFitError
  ( JNIEnv * env, jclass clazz, jstring path)
{
  setJni( env, clazz);
  char const * c_path = getCString( path);
  currentProject->saveFitError( c_path);
  releaseString( path, c_path);
}

/*
 * Class:     Period04
 * Method:    projectGetAnalyticalErrors
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetAnalyticalErrors
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->getAnalyticalErrors( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetFitError
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetFitError
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->getFitError( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetMaxFreq
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetMaxFreq
  ( JNIEnv * env, jclass clazz)
{
	setJni( env, clazz);
	return MAX_FREQUENCIES;
}

/*
 * Class:     Period04
 * Method:    projectGetUseData
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetUseData
  ( JNIEnv * env, jclass clazz)
{
    setJni( env, clazz);
    return currentProject->getPeriod().getUseData();
}

/*
 * Class:     Period04
 * Method:    projectGetBMUseData
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetBMUseData
  ( JNIEnv * env, jclass clazz)
{
    setJni( env, clazz);
    return currentProject->getBMPeriod().getUseData();
}

/*
 * Class:     Period04
 * Method:    projectGetActiveFrequencies
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetActiveFrequencies
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return currentProject->getActiveFrequencies( );
}

/*
 * Class:     Period04
 * Method:    projectGetActiveFrequenciesString
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetActiveFrequenciesString
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->getActiveFrequenciesString( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetBMActiveFrequencies
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetBMActiveFrequencies
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return currentProject->getBMActiveFrequencies( );
}

/*
 * Class:     Period04
 * Method:    projectGetBMActiveFrequenciesString
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetBMActiveFrequenciesString
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->getBMActiveFrequenciesString( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectWriteActiveFrequencies
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectWriteActiveFrequencies
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->getPeriod( ).writeSelected( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetZeropoint
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetZeropoint
  ( JNIEnv * env, jclass clazz)
{
	setJni( env, clazz);
	return newString( currentProject->getZeropoint().c_str());
}

/*
 * Class:     Period04
 * Method:    projectGetBMZeropoint
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetBMZeropoint
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->getBMZeropoint( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetZeropointError
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetZeropointError
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->getZeropointError( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetResiduals
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetResiduals
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->getResiduals( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetBMResiduals
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetBMResiduals
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return newString( currentProject->getBMResiduals( ).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetResidualsValue
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectGetResidualsValue
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return currentProject->getResidualsValue( );
}

/*
 * Class:     Period04
 * Method:    projectGetIterations
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetIterations
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return currentProject->getPeriod( ).getIterations( );
}

/*
 * Class:     Period04
 * Method:    projectGetTotalFrequencies
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetTotalFrequencies
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return currentProject->getTotalFrequencies( );
}

/*
 * Class:     Period04
 * Method:    projectSetTotalFrequencies
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetTotalFrequencies
  ( JNIEnv * env, jclass clazz, jint i)
{
  setJni( env, clazz);
  currentProject->setTotalFrequencies( i);
}

/*
 * Class:     Period04
 * Method:    projectGetBMTotalFrequencies
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetBMTotalFrequencies
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return currentProject->getBMTotalFrequencies( );
}

/*
 * Class:     Period04
 * Method:    projectSetBMTotalFrequencies
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetBMTotalFrequencies
  ( JNIEnv * env, jclass clazz, jint i)
{
  setJni( env, clazz);
  currentProject->setBMTotalFrequencies( i);
}

/*
 * Class:     Period04
 * Method:    projectGetFirstFrequency
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectGetFirstFrequency
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return currentProject->getFirstFrequency( );
}

/*
 * Class:     Period04
 * Method:    projectGetFirstFrequencyNumber
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetFirstFrequencyNumber
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return currentProject->getFirstFrequencyNumber( );
}

/*
 * Class:     Period04
 * Method:    projectGetNumber
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetNumber
  ( JNIEnv * env, jclass clazz, jint i)
{
  setJni( env, clazz);
  return newString( currentProject->getNumber( i).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetBMNumber
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetBMNumber
  ( JNIEnv * env, jclass clazz, jint i)
{
	setJni( env, clazz);
	return newString( currentProject->getBMNumber( i).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetActive
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_Period04_projectGetActive
  ( JNIEnv * env, jclass clazz, jint i)
{
	setJni( env, clazz);
	if (currentProject->getActive(i)) { return true; }
    else { return false; }
}

/*
 * Class:     Period04
 * Method:    projectSetActive
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetActive
  ( JNIEnv * env, jclass clazz, jint i, jboolean active)
{
	setJni( env, clazz);
	if (active) { currentProject->setActive(i, 1); }
    else        { currentProject->setActive(i, 0); }
}

/*
 * Class:     Period04
 * Method:    projectGetBMActive
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_Period04_projectGetBMActive
  ( JNIEnv * env, jclass clazz, jint i)
{
	setJni( env, clazz);
	if (currentProject->getBMActive(i)) { return true; }
    else { return false; }
}

/*
 * Class:     Period04
 * Method:    projectSetBMActive
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetBMActive
  ( JNIEnv * env, jclass clazz, jint i, jboolean active)
{
	setJni( env, clazz);
	if (active) { currentProject->setBMActive(i, 1); }
    else        { currentProject->setBMActive(i, 0); }
}

/*
 * Class:     Period04
 * Method:    periodIsSelected
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_Period04_periodIsSelected
  ( JNIEnv * env, jclass clazz, jint i, jint what)
{
    setJni( env, clazz);
    CPeriod *period=&currentProject->getPeriod();
    Parts whatpart;
    switch (what) {
	case 0: whatpart=Fre; break;
	case 1: whatpart=Amp; break;
	case 2: whatpart=Pha; break;
	default: return false;
    }
    if ((*period)[i].isSelected(whatpart)) { return true; }
    return false;
}

/*
 * Class:     Period04
 * Method:    periodIsBMSelected
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_Period04_periodIsBMSelected
  ( JNIEnv * env, jclass clazz, jint i, jint what)
{
    setJni( env, clazz);
    CPeriod *peri=&currentProject->getBMPeriod();
    Parts whatpart;
    switch (what) {
	case 0: whatpart=Fre; break;
	case 1: whatpart=Amp; break;
	case 2: whatpart=Pha; break;
	default: return false;
    }
    if ((int)i==-1) { // for periodic time shift frequency
	if (peri->getBMPeriod().isSelected(whatpart)) { return true; }
    } else { // for the other frequencies
	if ((*peri)[i].isSelected(whatpart)) { return true; }
    }
    return false;
}

/*
 * Class:     Period04
 * Method:    periodSetSelection
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_Period04_periodSetSelection
  ( JNIEnv * env, jclass clazz, jint i, jint what)
{
    setJni( env, clazz);
    CPeriod *period=&currentProject->getPeriod();
    Parts whatpart;
    switch (what) {
	case 0: whatpart=Fre; break;
	case 1: whatpart=Amp; break;
	case 2: whatpart=Pha; break;
 	default: return;
    }
    (*period)[i].setSelection(whatpart);
}

/*
 * Class:     Period04
 * Method:    periodSetBMSelection
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_Period04_periodSetBMSelection
  ( JNIEnv * env, jclass clazz, jint i, jint what)
{
    setJni( env, clazz);
    CPeriod *period=&currentProject->getBMPeriod();
    Parts whatpart;
    switch (what) {
	case 0: whatpart=Fre; break;
	case 1: whatpart=Amp; break;
	case 2: whatpart=Pha; break;
 	default: return;
    }
    if ((int)i==-1) { // for periodic time shift frequency
	(*period).getBMPeriod().setSelection(whatpart); // test
    } else { // for the other frequencies
	(*period)[i].setSelection(whatpart);
    }
}

/*
 * Class:     Period04
 * Method:    periodUnsetSelection
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_Period04_periodUnsetSelection
  ( JNIEnv * env, jclass clazz, jint i, jint what)
{
    setJni( env, clazz);
    CPeriod *period=&currentProject->getPeriod();
    Parts whatpart;
    switch (what) {
	case 0: whatpart=Fre; break;
	case 1: whatpart=Amp; break;
	case 2: whatpart=Pha; break;
	default: return;
    }
    (*period)[i].unsetSelection(whatpart);
}

/*
 * Class:     Period04
 * Method:    periodUnsetBMSelection
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_Period04_periodUnsetBMSelection
  ( JNIEnv * env, jclass clazz, jint i, jint what)
{
    setJni( env, clazz);
    CPeriod *period=&currentProject->getBMPeriod();
    Parts whatpart;
    switch (what) {
	case 0: whatpart=Fre; break;
	case 1: whatpart=Amp; break;
	case 2: whatpart=Pha; break;
	default: return;
    }
    if ((int)i==-1) { // for periodic time shift frequency
	(*period).getBMPeriod().unsetSelection(whatpart);
    } else { // for the other frequencies
	(*period)[i].unsetSelection(whatpart);
    }
}

/*
 * Class:     Period04
 * Method:    projectGetComposite
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetComposite
  ( JNIEnv * env, jclass clazz, jint i)
{
  setJni( env, clazz);
  return newString( currentProject->getComposite( i).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetBMComposite
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetBMComposite
  ( JNIEnv * env, jclass clazz, jint i)
{
  setJni( env, clazz);
  return newString( currentProject->getBMComposite( i).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectIsComposition
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_Period04_projectIsComposition
  ( JNIEnv * env, jclass clazz, jint i)
{
    setJni( env, clazz);
    CPeriod *period=&currentProject->getPeriod();
    if ((*period)[i].isComposition()) { return true; }
    return false;
}

/*
 * Class:     Period04
 * Method:    projectIsBMComposition
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_Period04_projectIsBMComposition
  ( JNIEnv * env, jclass clazz, jint i)
{
    setJni( env, clazz);
    CPeriod *period=&currentProject->getBMPeriod();
    if ((*period)[i].isComposition()) { return true; }
    return false;
}

/*
 * Class:     Period04
 * Method:    periodIsEmpty
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_Period04_periodIsEmpty
  ( JNIEnv * env, jclass clazz, jint i)
{
    setJni( env, clazz);
    if (currentProject->getPeriod()[i].isComposition()) { return true; }
    return false;
}

/*
 * Class:     Period04
 * Method:    periodIsBMEmpty
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_Period04_periodIsBMEmpty
  ( JNIEnv * env, jclass clazz, jint i)
{
    setJni( env, clazz);
    if (currentProject->getBMPeriod()[i].isComposition()) { return true; }
    return false;
}

/*
 * Class:     Period04
 * Method:    projectGetFrequencyValue
 * Signature: (I)D
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectGetFrequencyValue
  ( JNIEnv * env, jclass clazz, jint i)
{
	setJni( env, clazz);
	CPeriod *period=&currentProject->getPeriod();
    return (*period)[i].getFrequency();
}

/*
 * Class:     Period04
 * Method:    projectGetBMFrequencyValue
 * Signature: (I)D
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectGetBMFrequencyValue
  ( JNIEnv * env, jclass clazz, jint i)
{
  setJni( env, clazz);
  CPeriod * period = &currentProject->getBMPeriod( );
  return (*period)[i].getFrequency( );
}

/*
 * Class:     Period04
 * Method:    projectGetFrequency
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetFrequency
  ( JNIEnv * env, jclass clazz, jint i)
{
  setJni( env, clazz);
  return newString( currentProject->getFrequency( i).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectSetFrequency
 * Signature: (ILjava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectSetFrequency
  ( JNIEnv * env, jclass clazz, jint i, jstring freq)
{
  setJni( env, clazz);
  char const * c_freq = getCString( freq);
  std::string protocol = currentProject->setFrequency( i, c_freq);
  releaseString( freq, c_freq);
  return newString( protocol.c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetBMFrequency
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetBMFrequency
  ( JNIEnv * env, jclass clazz, jint i)
{
  setJni( env, clazz);
  return newString( currentProject->getBMFrequency( i).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectSetBMFrequency
 * Signature: (ILjava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectSetBMFrequency
  ( JNIEnv * env, jclass clazz, jint i, jstring freq)
{
  setJni( env, clazz);
  char const * c_freq = getCString( freq);
  std::string protocol = currentProject->setBMFrequency( i, c_freq);
  releaseString( freq, c_freq);
  return newString( protocol.c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetAmplitude
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetAmplitude
  ( JNIEnv * env, jclass clazz, jint i)
{
  setJni( env, clazz);
  return newString( currentProject->getAmplitude( i).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetAmplitudeValue
 * Signature: (I)D
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectGetAmplitudeValue
  ( JNIEnv * env, jclass clazz, jint i)
{
  setJni( env, clazz);
  return currentProject->getAmplitudeValue( i);
}

/*
 * Class:     Period04
 * Method:    projectGetBMAmplitude
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetBMAmplitude
  ( JNIEnv * env, jclass clazz, jint i)
{
  setJni( env, clazz);
  return newString( currentProject->getBMAmplitude( i).c_str());
}

/*
 * Class:     Period04
 * Method:    projectGetAmplitudeValue
 * Signature: (I)D
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectGetBMAmplitudeValue
  ( JNIEnv * env, jclass clazz, jint i)
{
  setJni( env, clazz);
  return currentProject->getBMAmplitudeValue( i);
}

/*
 * Class:     Period04
 * Method:    projectSetAmplitude
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetAmplitude
  ( JNIEnv * env, jclass clazz, jint i, jstring amp)
{
  setJni( env, clazz);
  char const * c_amp = getCString( amp);
  currentProject->setAmplitude( i, c_amp);
  releaseString( amp, c_amp);
}

/*
 * Class:     Period04
 * Method:    projectSetBMAmplitude
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetBMAmplitude
  ( JNIEnv * env, jclass clazz, jint i, jstring amp)
{
  setJni( env, clazz);
  char const * c_amp = getCString( amp);
  currentProject->setBMAmplitude( i, c_amp);
  releaseString( amp, c_amp);
}


/*
 * Class:     Period04
 * Method:    projectGetPhase
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetPhase
  ( JNIEnv * env, jclass clazz, jint i)
{
  setJni( env, clazz);
  return newString( currentProject->getPhase( i).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectSetPhase
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetPhase
  ( JNIEnv * env, jclass clazz, jint i, jstring pha)
{
  setJni( env, clazz);
  char const * c_pha = getCString( pha);
  currentProject->setPhase( i, c_pha);
  releaseString( pha, c_pha);
}

/*
 * Class:     Period04
 * Method:    projectGetBMPhase
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectGetBMPhase
  ( JNIEnv * env, jclass clazz, jint i)
{
  setJni( env, clazz);
  return newString( currentProject->getBMPhase( i).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectSetBMPhase
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetBMPhase
  ( JNIEnv * env, jclass clazz, jint i, jstring pha)
{
  setJni( env, clazz);
  char const * c_pha = getCString( pha);
  currentProject->setBMPhase( i, c_pha);
  releaseString( pha, c_pha);
}

/*
 * Class:     Period04
 * Method:    projectGetCompoUseFreqValue
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_Period04_projectGetCompoUseFreqValue
  ( JNIEnv * env, jclass clazz, jint i)
{
  setJni( env, clazz);
  return currentProject->getCompoUseFreqValue( i);
}

/*
 * Class:     Period04
 * Method:    projectSetCompoUseFreqValue
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetCompoUseFreqValue
  ( JNIEnv * env, jclass clazz, jint i, jboolean val)
{
  setJni( env, clazz);
  currentProject->setCompoUseFreqValue( i, val);
}

/*
 * Class:     Period04
 * Method:    projectSetCompoUseFreqValues
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetCompoUseFreqValues
  ( JNIEnv * env, jclass clazz, jboolean val)
{
  setJni( env, clazz);
  currentProject->setCompoUseFreqValues( val);
}

/*
 * Class:     Period04
 * Method:    projectGetCompoUseFreqValue
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_Period04_projectGetBMCompoUseFreqValue
  ( JNIEnv * env, jclass clazz, jint i)
{
  setJni( env, clazz);
  return currentProject->getBMCompoUseFreqValue( i);
}

/*
 * Class:     Period04
 * Method:    projectSetBMCompoUseFreqValue
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetBMCompoUseFreqValue
  ( JNIEnv * env, jclass clazz, jint i, jboolean val)
{
  setJni( env, clazz);
  currentProject->setBMCompoUseFreqValue( i, val);
}

/*
 * Class:     Period04
 * Method:    projectSetBMCompoUseFreqValues
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetBMCompoUseFreqValues
  ( JNIEnv * env, jclass clazz, jboolean val)
{
  setJni( env, clazz);
  currentProject->setBMCompoUseFreqValues( val);
}

/*
 * Class:     Period04
 * Method:    projectGetFitMode
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetFitMode
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return currentProject->getFitMode( );
}

/*
 * Class:     Period04
 * Method:    projectSetFitMode
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetFitMode
  ( JNIEnv * env, jclass clazz, jint fitmode)
{
  setJni( env, clazz);
  currentProject->setFitMode( fitmode);
}

/*
 * Class:     Period04
 * Method:    projectSetFitSelectionMode
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetFitSelectionMode
  ( JNIEnv * env, jclass clazz, jint selmode)
{
    setJni( env, clazz);
    switch (selmode) {
	case 0:
	    currentProject->getPeriod().setFittingMode(AMP_PHA); break;
	case 1:
	    currentProject->getPeriod().setFittingMode(FRE_AMP_PHA); break;
	default:
	    currentProject->getPeriod().setFittingMode(SPECIAL); break;
    }
}

/*
 * Class:     Period04
 * Method:    projectCalculatePeriod
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectCalculatePeriod
  ( JNIEnv * env, jclass clazz, jint mode)
{
  setJni( env, clazz);
  return newString( currentProject->calculatePeriod( (DataMode) ((int) mode)).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectImprovePeriod
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectImprovePeriod
  ( JNIEnv * env, jclass clazz, jint mode)
{
  setJni( env, clazz);
  return newString( currentProject->improvePeriod( (DataMode) ((int) mode)).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectImproveSpecialPeriod
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectImproveSpecialPeriod
  ( JNIEnv * env, jclass clazz, jint mode)
{
  setJni( env, clazz);
  return newString( currentProject->improveSpecialPeriod( (DataMode) ((int) mode)).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectImprovePTS
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectImprovePTS
  ( JNIEnv * env, jclass clazz, jint mode)
{
  setJni( env, clazz);
  return newString( currentProject->improvePTS( (DataMode) ((int) mode)).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectCalculateAmpVar
 * Signature: (III[II)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectCalculateAmpVar
  ( JNIEnv * env, jclass clazz, jint mode, jint varmode, jint attribute, jintArray frequencies, jint selected)
{
  setJni( env, clazz);
  //jsize len = env->GetArrayLength(frequencies); // Laenge des Arrays bestimmen

  jint * body = env->GetIntArrayElements(  frequencies, 0); // Java-Array zu C-Array konvertieren

  std::string ampVar = currentProject->calculateAmpVar
    ( (DataMode) ((int) mode)
    , (CalcMode) ((int) varmode)
    , attribute
    , (int *) body
    , selected
    )
  ;

  env->ReleaseIntArrayElements( frequencies, body, 0);
  return newString( ampVar.c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectSearchPTSStartValues
 * Signature: (II)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectSearchPTSStartValues
  ( JNIEnv * env, jclass clazz, jint mode, jint shots)
{
    setJni( env, clazz);
    return newString( currentProject->searchPTSStartValues( (DataMode) ((int) mode), shots).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectGetPeriodUseData
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetPeriodUseData
  ( JNIEnv * env, jclass clazz)
{
    setJni( env, clazz);
    return currentProject->getPeriodUseData( );
}

/*
 * Class:     Period04
 * Method:    projectResetLambda
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_Period04_projectResetLambda
  ( JNIEnv * env, jclass clazz)
{
    setJni( env, clazz);
    currentProject->getActivePeriod( ).resetLambda( );
}

/*
 * Class:     Period04
 * Method:    projectCleanPeriod
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_Period04_projectCleanPeriod__
  ( JNIEnv * env, jclass clazz)
{
    setJni( env, clazz);
    currentProject->cleanPeriod( );
}

/*
 * Class:     Period04
 * Method:    projectCleanPeriod
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_Period04_projectCleanPeriod__I
( JNIEnv * env, jclass clazz, jint id)
{
  setJni( env, clazz);
  currentProject->cleanPeriod( id);
}

/*
 * Class:     Period04
 * Method:    projectCleanSigmas
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_Period04_projectCleanSigmas
  ( JNIEnv * env, jclass clazz)
{
	setJni( env, clazz);
	currentProject->cleanSigmas( );
}

/*
 * Class:     Period04
 * Method:    projectShowEpoch
 * Signature: (DI)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectShowEpoch
  ( JNIEnv * env, jclass clazz, jdouble epoch, jint mode)
{
  setJni( env, clazz);
  return newString( currentProject->showEpoch( epoch, mode).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectRefit
 * Signature: (D)V
 */
JNIEXPORT void JNICALL Java_Period04_projectRefit
  ( JNIEnv * env, jclass clazz, jdouble zeropoint)
{
  setJni( env, clazz);
  currentProject->refit( zeropoint);
}

/*
 * Class:     Period04
 * Method:    projectPredictAmplitude
 * Signature: (D)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectPredictAmplitude__D
  ( JNIEnv * env, jclass clazz, jdouble time)
{
  setJni( env, clazz);
  return newString( currentProject->predict( time).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectPredictAmplitude
 * Signature: (DI)D
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectPredictAmplitude__DI
  ( JNIEnv * env, jclass clazz, jdouble time, jint id)
{
  setJni( env, clazz);
  return currentProject->getPeriod( ).predict( time, id);
}

/*
 * Class:     Period04
 * Method:    projectPredictBMAmplitude
 * Signature: (DI)D
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectPredictBMAmplitude
  ( JNIEnv * env, jclass clazz, jdouble time, jint id)
{
  setJni( env, clazz);
  return currentProject->getBMPeriod( ).predict( time, id);
}

/*
 * Class:     Period04
 * Method:    projectCreateArtificialData
 * Signature: (Ljava/lang/String;DDDDZ)V
 */
JNIEXPORT void JNICALL Java_Period04_projectCreateArtificialData__Ljava_lang_String_2DDDDZ
  ( JNIEnv * env, jclass clazz, jstring outfile, jdouble starttime, jdouble endtime, jdouble step, jdouble leading, jboolean append)
{
  setJni( env, clazz);
  char const * c_outfile = getCString( outfile);
  currentProject->createArtificialData( c_outfile, starttime, endtime, step, leading, append);
  releaseString( outfile, c_outfile);
}

/*
 * Class:     Period04
 * Method:    projectCreateArtificialData
 * Signature: (Ljava/lang/String;Ljava/lang/String;DDZ)V
 */
JNIEXPORT void JNICALL Java_Period04_projectCreateArtificialData__Ljava_lang_String_2Ljava_lang_String_2ZDDZ
( JNIEnv * env, jclass clazz, jstring outfile, jstring infile, jboolean isRange, jdouble step, jdouble leading, jboolean append)
{
  setJni( env, clazz);
  char const * c_outfile = getCString( outfile);
  char const * c_infile = getCString( infile);
  currentProject->createArtificialData( c_outfile, c_infile, isRange, step, leading, append);
  releaseString( outfile, c_outfile);
  releaseString( infile, c_infile);
}

/*
 * Class:     Period04
 * Method:    projectCheckActive
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_Period04_projectCheckActive
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  currentProject->checkActive( );
}

/*
 * Class:     Period04
 * Method:    projectCheckBMActive
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_Period04_projectCheckBMActive
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  currentProject->checkBMActive( );
}

/*
 * Class:     Period04
 * Method:    periodRestoreSelection
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_Period04_periodRestoreSelection
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  currentProject->getPeriod( ).restoreSelection( );
}

/*
 * Class:     Period04
 * Method:    projectGetUseNameWeight
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetUseNameWeight
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return currentProject->getUseNameWeight( );
}

/*
 * Class:     Period04
 * Method:    projectSetUseNameWeight
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetUseNameWeight
  ( JNIEnv * env, jclass clazz, jint flags)
{
  setJni( env, clazz);
  currentProject->setUseNameWeight( flags);
}

/*
 * Class:     Period04
 * Method:    projectGetUsePointWeight
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_Period04_projectGetUsePointWeight
  ( JNIEnv * env, jclass clazz)
{
	setJni( env, clazz);
	switch (currentProject->getUsePointWeight()) {
	case 1:
	    return true;
	default:
	    return false;
    }
}

/*
 * Class:     Period04
 * Method:    projectSetUsePointWeight
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetUsePointWeight
  ( JNIEnv * env, jclass clazz, jboolean value)
{
  setJni( env, clazz);

  if (value)
  { // yes, use point weight
    currentProject->setUsePointWeight( 1);
  }
  else
  { // no, don't use point weight
    currentProject->setUsePointWeight( 0);
  }
}

/*
 * Class:     Period04
 * Method:    projectSetUsePointWeight
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetUsePointErrors
  ( JNIEnv * env, jclass clazz, jboolean value)
{
  setJni( env, clazz);
  currentProject->setUsePointErrors( value);
}

/*
 * Class:     Period04
 * Method:    projectGetUseDeviationWeight
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_Period04_projectGetUseDeviationWeight
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);

  switch (currentProject->getUseDeviationWeight( ))
  {
  case 1:
    return true;
  default:
    return false;
  }
}

/*
 * Class:     Period04
 * Method:    projectSetUseDeviationWeight
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetUseDeviationWeight
  ( JNIEnv * env, jclass clazz, jboolean value)
{
    setJni( env, clazz);
    if (value) { // yes, use point weight
	currentProject->setUseDeviationWeight(1);
    } else { // no, don't use point weight
	currentProject->setUseDeviationWeight(0);
    }
}

/*
 * Class:     Period04
 * Method:    projectGetDeviationCutoff
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_Period04_projectGetDeviationCutoff
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return currentProject->getDeviationCutoff( );
}

/*
 * Class:     Period04
 * Method:    projectSetDeviationCutoff
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetDeviationCutoff
  ( JNIEnv * env, jclass clazz, jstring cutoff)
{
  setJni( env, clazz);
  char const * c_cutoff = getCString( cutoff);
  currentProject->setDeviationCutoff( c_cutoff);
  releaseString( cutoff, c_cutoff);
}

/*
 * Class:     Period04
 * Method:    projectSetFourierUseWeight
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetFourierUseWeight
  ( JNIEnv * env, jclass clazz, jboolean value)
{
    setJni( env, clazz);
    if (value) { // yes, use point weight
	currentProject->setFourierUseWeight(1);
    } else { // no, don't use point weight
	currentProject->setFourierUseWeight(0);
    }
}

/*
 * Class:     Period04
 * Method:    projectGetPeriodUseWeight
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_Period04_projectGetPeriodUseWeight
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  return currentProject->getPeriodUseWeight( );
}

/*
 * Class:     Period04
 * Method:    projectSetPeriodUseWeight
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetPeriodUseWeight
  ( JNIEnv * env, jclass clazz, jboolean value)
{
    setJni( env, clazz);
    if (value) { // yes, use point weight
	currentProject->setPeriodUseWeight(1);
    } else { // no, don't use point weight
	currentProject->setPeriodUseWeight(0);
    }
}

/*
 * Class:     Period04
 * Method:    projectSetUseWeightFlags
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetUseWeightFlags
  ( JNIEnv * env, jclass clazz)
{
  setJni( env, clazz);
  currentProject->setUseWeightFlags( );
}



/*
 * Class:     Period04
 * Method:    projectSetMCRange
 * Signature: (DDDD)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSetMCRange
  ( JNIEnv * env, jclass clazz,
   jdouble f_low, jdouble f_up, jdouble a_low, jdouble a_up)
{
  setJni( env, clazz);
  currentProject->setMCRange( f_low, f_up, a_low, a_up);
}


/*
 * Class:     Period04
 * Method:    projectWriteFrequenciesTabulated
 * Signature: (Z)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Period04_projectWriteFrequenciesTabulated
  ( JNIEnv * env, jclass clazz, jboolean markselected)
{
  setJni( env, clazz);
  return newString( currentProject->writeFrequenciesTabulated( markselected).c_str( ));
}

/*
 * Class:     Period04
 * Method:    projectSaveProtocolText
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_Period04_projectSaveProtocolText
  ( JNIEnv * env, jclass clazz, jstring text)
{
  setJni( env, clazz);
  char const * c_text = getCString( text);
  currentProject->setProtocolText( c_text);
  releaseString( text, c_text);
}


