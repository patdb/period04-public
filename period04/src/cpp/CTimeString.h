/*----------------------------------------------------------------------------
 *  CTimeString.h
 *               declares the CTimePoint Class, enumeration DataMode
 *---------------------------------------------------------------------------*/

#ifndef __timestr_h__
#define __timestr_h__

#include <string>
#include <iostream>
using std::cerr;          
using std::endl;
using std::istream;

#include <map>

#include "CTimePoint.h"
#include "CNames.h"
#include "constants.h"
#include "message.h"

typedef CTimePoint* SelectedTime;

enum DataMode { Observed,
		Adjusted,
		SpectralWindow1,
		SpectralWindow2,
		DataResiduals,
		AdjustedResiduals,
		Calculated,
		Simulated};
	       
class CTimeString
{
private:
  typedef std::multimap< double, CTimePoint> CTimeArray;

    CTimeArray    mTimeArray;    // a list of CTimePoints
    CNames        mNames[4];
    SelectedTime * mSelection;
    int           mSelected;
    
    std::string mFileName;       // the name of the time string file(s)
    std::string mInputFormat;    // input format of the time string file
    std::string mOutputFormat;   // output format of the time string file
    char     mLineSeparator;     // line separator to use for reading files
    bool     mVOTableFormat;     // use VOTable format for output
    
    DataMode mMode;              // the fourier calculation mode
    std::string mNameSet [4];    // the name of the 4 attributes (headings)
    double   mWeightSum;
    int      mDeleteWhat;
    std::string  mDeleteName;
    int      mReverseScale;      // mag or intensity?
    int      mPointWeight;
    int      mNameWeight;
    int      mDeviationWeight;
    double   mDeviationCutoff;

    bool     mCalcErrors;
    bool     mShiftTimes;
    int      mMoCaIterations;    // number of iterations for Monte Carlo Errors
    double   mMoCaResiduals;     // the rms residuals of the last fit
    double   mTimeAverage;       // "average time"

 public:

    // constructors
    CTimeString ( );

    CTimeString
      ( std::string const & name
      , std::string const & from = "ta"
      )
    ;

    // destructor
    ~CTimeString ( );

    // if we are not using magnitudes we have to reverse scale 
    int  getReverseScale()             { return mReverseScale; } 
    void setReverseScale(int scale)    { mReverseScale=scale; } 

    // do we want to decouple frequency and phase?
    void setCalcErrors(bool b) { mCalcErrors=b; }
    bool isCalcErrors() const  { return mCalcErrors; } 
    void setShiftTimes(bool b) { mShiftTimes=b; }
    bool isShiftTimes() const  { return mShiftTimes; } 
    void setMonteCarloResiduals(double res) { mMoCaResiduals=res; }
    void setMonteCarloIterations(int iter)  { mMoCaIterations=iter; }
    void writeNoisyDataSet(double residuals);

    // memory-functions
    void selectClear();
    void select();
    void selectAll();
    int  isSelected ( SelectedTime time);

    // weight setting functions
    void   setWeight(SelectedTime time);
    void   calcWeights();
    double getWeightSum() const    { return mWeightSum; }

    void setUseNameWeight(int i)   { mNameWeight=i; }
    int  getUseNameWeight() const  { return mNameWeight; }
    void setUsePointWeight(int i)  { mPointWeight=i; }
    int  getUsePointWeight() const { return mPointWeight; }

    void setUseDeviationWeight(int i)  { mDeviationWeight=i; }
    int  getUseDeviationWeight() const { return mDeviationWeight; }

    double getDeviationCutoff() const     { return mDeviationCutoff; }
    void   setDeviationCutoff(double val) { mDeviationCutoff=val; }

    void calcAverage();
    void adjust( int what, DataMode Original, int n, int * ids, bool useweight);
    void subtractFromObserved ( double value);
    void subtractFromAdjusted ( double value);
    void makeObservedAdjusted( );
    void makeDataResidualsObserved();
    void makeAdjResidualsObserved();
    void makeCalculatedObserved();
    void makeAdjustedObserved();
    void createMonteCarloData();
    void makeCalculatedSimulatedBase(); // make a copy of mCalculated for MC
    void makeSimulatedBaseCalculated(); // restore the old mCalculated value
    void storeCalculated();             // store mCalculated (wg. time shifts)
    void restoreCalculated();

    // file I/O
    void load ( std::string const & fname);
    void loadVOTable ( std::string const & fname);

    void readIn(std::istream& str,int version=0);

    void addData ( CTimePoint const & value)
    {
      mTimeArray.insert( std::pair< double const, CTimePoint>( value.getTime( ), value));
    }

    void setLineSeparator(char delim) { mLineSeparator = delim; }
    char getLineSeparator() { return mLineSeparator; }

    void save ( std::string const & Name);
    void savePhase ( std::string const & Name, double Frequency, double zeropoint);
    
    ostream & writeOut ( ostream & str, double Frequency=0.0,double zeropoint=0.0);
    void writeAll(ostream& err,char *head="");
    
    // values of interest
    int getSelectedPoints ( ) const { return mSelected; }

    int getTotalPoints ( ) const
    { 
      return mTimeArray.size( );
    }

    // file name
    inline std::string const & getFileName ( ) const { return mFileName; }
    void setFileName ( std::string const & name) { mFileName = name; }
    
    double baseLine() const;            // last time - first time
    double getTimeAverage() const;      // calculates the "average time" value
    void   setTimeAverage(double av) { mTimeAverage=av; }
    double nyquist() const;             // calculates the nyquist frequency
    double nyquist(double t_start, double t_end) const; // special mode
    double average(int useWeights=0) const; // calcs the average ampl. value
  
    // file format of the file to be read in
    void setInputFormat ( std::string const & fi);
    inline std::string const & getInputFormat ( ) const { return mInputFormat; }

    // file format of the file to be written
    void setOutputFormat ( std::string const & fo);
    inline std::string const & getOutputFormat ( ) const { return mOutputFormat; }

    // output format of the file to be written
    void setUseVOTableFormat(bool const val);
    inline bool getUseVOTableFormat() const { return mVOTableFormat; }

    void setDataMode(DataMode i) { mMode=i; }
    inline DataMode getDataMode() const { return mMode;}

    CTimePoint &operator[](int i) const;
    inline CTimePoint &point(int i, double *t, double *a) const {
	return point(i,mMode,t,a);
    }

    CTimePoint &point(int i, DataMode mode, double *t, double *a) const {
	CTimePoint &tmp=*mSelection[i];

	if (mShiftTimes) { *t=tmp.getTime()-mTimeAverage; }
	else             { *t=tmp.getTime(); }

	switch(mode) {
	    case Observed:          *a=tmp.getObserved();         break;
	    case Adjusted:          *a=tmp.getAdjusted();         break;
	    case SpectralWindow1:   *a=1;                         break;
	    case SpectralWindow2:   *a=tmp.getSW2Data();          break;
	    case DataResiduals:     *a=tmp.getDataResidual();     break;
	    case AdjustedResiduals: *a=tmp.getAdjustedResidual(); break;
	    case Calculated:        *a=tmp.getCalculated();       break;
	    case Simulated:         *a=tmp.getSimulated();        break;
	    default:      
		MYERROREXIT("Program wanted to access a data mode "
			    "that does not exist!"); break;
	}

	return *mSelection[i];
    }

    void checkBounds(int i) const {
	if ( (i<0) || (i>3) ) {
	    MYERROREXIT("Get Name out of Bounds...");
	}
    }

    void addAmpVarData ( int i, int freq)
    {
      checkBounds( i);
      mNames[i].addAmpVarData( freq);
    }

    void removeAmpVarData ( int i)
    {
      checkBounds( i);
      mNames[i].removeAmpVarData( );
    }
  
    std::string const & nameSet ( int i)
    {
      checkBounds( i);
      return mNameSet[i];
    }

    void initNameSets ( );
    void changeNameSet ( int i, std::string const & tmp);
  
    int numberOfNames ( int i)
    {
      checkBounds( i);
      return mNames[i].getNumberOfNames( );
    }

    int getID
      ( int attributeID
      , std::string const & name
      )
    {
      checkBounds( attributeID);
      return mNames[attributeID].getID( name);
    }
  
    CName &getIDName(int attributeID, int id) {  // i=column, id=entry
	checkBounds(attributeID);
	return mNames[attributeID].getIDName(id);
    }
 
    CName & getIndexName( int i, int l)
    {
      checkBounds( i);
      return mNames[i].getIndexName( l);
    }

    int getActiveNames(int i) const {
	checkBounds(i);
	return mNames[i].getActiveNames();
    }

    int getHighestActiveIDName(int i) const {
	checkBounds(i);
	return mNames[i].getHighestActiveIDName();
    }


    int createName
      ( int column
      , int & useCounter
      , double time
      , std::string const & prefix
      , int places
      )
    ;

    void relabelName
      ( int column
      , int ID
      , double time
      , std::string const & prefix
      , int places
      )
    ;

    void subdivide
      ( double timegap
      , int column
      , int useCounter
      , std::string const & prefix
      , int places
      )
    ;

    void subdivide
      ( double start
      , double interval
      , int column
      , int useCounter
      , std::string const & prefix
      , int places
      )
    ;

    void combine ( int Column, std::string const & name);
    void deleteSelection ( );

    void clean ( );
    void garbageCollect ( );


    
    void setDeletePointInfo
      ( int what
      , std::string const & name
      )     // set delete labels
    {
      mDeleteWhat = what;
      mDeleteName = name;
    }

    void getDeletePointInfo
      ( int & what
      , std::string & name
      )  // get delete labels
    {
      what = mDeleteWhat;
      name = mDeleteName;
    }
    
    std::string predictFileFormat ( std::string const & file);
    std::string predictLine ( char const * tmp);

 private:
    
    int calculateExpectationValue(double &mi, double &ma, double t_start, double t_end) const;
    
};

ostream &operator<<(ostream &str, CTimeString &tmp);

#endif
