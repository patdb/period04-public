/*---------------------------------------------------------------------------
 *  CFourier.cpp
 *---------------------------------------------------------------------------*/

#include <algorithm>
#include <sstream>

#include "CFourier.h"
#include "CProject.h"
#include "os.h"


CFourierPoint::CFourierPoint()
    : mFrequency(0),mPower(0)
{;}

CFourierPoint::CFourierPoint(double f,double a)
    : mFrequency(f),mPower(a)
{;}

int CFourierPoint::readIn(string pt) {
  //--- first, clean all values
  mFrequency=0.0;
  mPower=1.0;
  double freq=0.0;
  double amp=1.0;
  //--- now read in
  std::istringstream istr(pt);
  istr.exceptions( std::istringstream::failbit 
		   | std::istringstream::badbit);
  try 
    {
      istr >> freq >> amp;
    }
  catch(std::istringstream::failure& e) { return 1; }
  mFrequency=freq;
  mPower=amp*amp;
  return 0;
}

// comperator needet for sorted list
int operator<(CFourierPoint const &f1, CFourierPoint const &f2) {
    return (f1.getFrequency()<f2.getFrequency());
}
int operator<=(CFourierPoint const &f1, CFourierPoint const &f2) {
    return (f1.getFrequency()<=f2.getFrequency());
}

// output-operator
ostream &operator<<(ostream &str,CFourierPoint const &f1) {
    double amp=f1.getAmplitude();
    return str << f1.getFrequency() << '\t' << amp;
}

// input-operator
istream &operator>>(istream &str, CFourierPoint &f1) {
  std::string pt;
  getline(str,pt);
  if (f1.readIn(pt)) {
    std::ostringstream ostr;
    ostr << "Ignored incomprehensible Fourier data point: "
	 << pt;
    MYERROR(ostr.str());
    str.setstate(std::ios::failbit); 
  }
  return str;
}

// initialise static elements of CFourier
int CFourier::mFourierIDCounter=0;

CFourier::CFourier
  ( std::string const & title
    , FTAlgorithm algo
    , double from
    , double to
    , StepQuality typ
    , double steprate
    , DataMode Mode
    , CompactMode Compact
    , int Weight
    )
 : mData( NULL)
 , mPoints( 0)
 , mAlgorithm( KURTZ1985)
 , mFrom( 0)
 , mTo( 0)
 , mMode( Mode)
 , mCompact( Compact)
 , mUseWeight( Weight)
{
  mFourierID = mFourierIDCounter;    // set id
  ++mFourierIDCounter;             // increase counter
  setTitle( title);
  setAlgorithm( algo);
  setFrom( from);
  setTo( to);
  setStepping( typ, steprate);
  mPeak = CPeriPoint( 0, 0, 0);         // clean the peak
}

CFourier::~CFourier ( )
{ 
  delete [] mData; 
  mData = NULL; 
  mPoints = 0; 
}

void CFourier::add(CFourierPoint const &point) {
    if (mData!=NULL) {
	MYERROREXIT("You cannot add new points...");
    }
    push_back(point);
    // find out if highest peak, and set the noise for this point
    setPeak(point.getFrequency(), point.getAmplitude(), 0);
}

void CFourier::makeStatic() {
    // allocate memory
    mPoints = points();
    mData   = new CFourierPoint[mPoints];
    int i=0;
    std::sort(begin(), end());      // sort the CFourierPoints in the vector
    iterator p = begin();
    while(p != end()){
	mData[i]=*p;
	p++; i++;
    }
    clear();    // now clean the list
}

// note: readData is currently not used
istream &CFourier::readData(istream &stream) {
    deallocateData();    // clean data at first
    // now start reading
    std::string text;
    while (!stream.eof()) {
      getline(stream,text);
      if (stream.good()&&(text[0]!=0)) {
	CFourierPoint tmp;
	if (tmp.readIn(text)==0) {
	  add(tmp);   // add the point
	}
      }
    }
    makeStatic();
    return stream;
}

ostream &CFourier::writeData(ostream &stream) const {
    int prec = stream.precision();     // store default-precission
    stream.precision(15);              // set new precission
    for(int i=0; i<points(); i++) {    // write out all data of the spectrum
	stream << (*this)[i] << endl;  
    }
    stream.precision(prec);            // restore old precission
    return stream;
}

void CFourier::deallocateData ( )
{
  clear ( );   // call clean from the list-parent
}

std::string CFourier::getProperties() {
  std::ostringstream ostr;
  ostr << "Title:                     \t" << mTitle 
	   << "\nFrequency range: \t" << mFrom << " - " << mTo
	   << "\nStep rate:          \t" << mStepping 
	   << "\nNumber of points:\t" << mPoints
	   << "\nCompact Mode:    \t" << (mCompact?"Peaks only":"Not used")
	   << "\nData Mode:       \t";
  string tmp;
  switch(mMode) { 
  case 0: tmp="Observed"; break;
  case 1: tmp="Adjusted"; break;
  case 2: tmp="Spectral Window v1"; break; 
  case 3: tmp="Spectral Window v2"; break; 
  case 4: tmp="Residuals (Observed)"; break;
  case 5: tmp="Residuals (Adjusted)"; break;
  default: tmp="?";
  }
  ostr << tmp
	   << "\nUsing weights:    \t" << (mUseWeight?"Yes\n":"No\n")
	   << "\nHighest Peak (Fre,Ampl):\t" << mPeak.getFrequency()
	   << ", " << mPeak.getAmplitude(-1) << endl;

  if (4 == mMode || 5 == mMode)
  {
    ostr << "\nFrequencies already subtracted:" << std::endl;
  }

  ostr << mInfo << endl;

  return ostr.str( );
}

void CFourier::setTitle ( std::string const & title)
{
  mTitle = title;
}

void CFourier::setInfo ( std::string const & info)
{
  mInfo = info;
}

void CFourier::setAlgorithm( FTAlgorithm algo)
{
  mAlgorithm = algo;
}

void CFourier::setFrom(double from) {
    if (from<0 && mMode!=SpectralWindow1) { from=0; }
    mFrom=from;
}

void CFourier::setTo(double to) {
    if (to<mFrom) { to=mFrom+1.0; }
    mTo=to;
}

int CFourier::pointEstimate(double) {
    return (int) ((getTo()-getFrom())/getStepping())+1;
}
ostream & CFourier::writeHeader(ostream &stream) const {
    stream << "Title=\t"      << getTitle()
		   << "\nFrom=\t"     << getFrom()
		   << "\nTo=\t"       << getTo()
		   << "\nStepping=\t" << (int)getStepQuality()
		   << "\t"            << getStepping()
		   << "\nMode=\t"     << (int)getMode()
		   << "\nCompact=\t"  << (int)getCompact()
		   << "\nInfo=\t"     << getInfo() << "\nend.Info"
		   << endl;
    stream.precision(12);
    for(int i=0; i<points(); i++) {
	stream << "Point\t" << (*this)[i]<<endl;
    }
    return stream;
}

istream &CFourier::readHeader(istream &ist) {
    char c, tmp[256];
    double dtmp;
    int itmp; string stmp;

    while(ist>>c,ist.putback(c),(c!='[')) {
	if (ist.eof()) { break;}
	ist>>tmp;   // read in argument

	if (my_strcasecmp(tmp,"Title=")==0) {
	    char c;
	    ist>>c;
	    ist.putback(c);
	    ist.getline(tmp,255);
	    setTitle(tmp);
	}
	else if (my_strcasecmp(tmp,"From=")==0) {
	    ist>>dtmp;
	    setFrom(dtmp);
	}
	else if (my_strcasecmp(tmp,"To=")==0) {
	    ist>>dtmp;
	    setTo(dtmp);
	}
	else if (my_strcasecmp(tmp,"Stepping=")==0) {
	    ist>>itmp>>dtmp;
	    setStepping((StepQuality)itmp,dtmp);
	}
	else if (my_strcasecmp(tmp,"Mode=")==0) {
	    ist>>itmp;
	    switch (itmp) {
		case 0: setMode(Observed); break;
		case 1: setMode(Adjusted); break;
		case 2: setMode(SpectralWindow1); break;
		case 3: setMode(SpectralWindow2); break;
		case 4: setMode(DataResiduals); break;
		case 5: setMode(AdjustedResiduals); break;
		case 6: setMode(Calculated); break;
	    }
	}
	else if (my_strcasecmp(tmp,"Compact=")==0) {
	    ist>>itmp;
	    switch (itmp) {
		case 0: setCompact(ALL_DATA);
		case 1: setCompact(PEAKS_ONLY);
	    }
	}
   	else if (my_strcasecmp(tmp,"Info=")==0) {
	  std::ostringstream complete;     
	  while (!ist.getline(tmp,256).eof()) {
		//--- find first nonwhite name
		char *tmp1=tmp; 	    
		while ( (isspace(*tmp1)) && !(*tmp1==0) ) { tmp1++; }
		//--- check for the end of the Info field
		if (strstr(tmp,"end.Info")!=NULL) { break; }
		complete << tmp1 << endl;   
	  }
	  setInfo( complete.str( ));
	}
	else if (my_strcasecmp(tmp,"Point")==0) {
	  CFourierPoint pt;
	  ist >> pt;
	  if (ist.fail()) {
	    ist.clear();    // do not add point, revert state
	  } else {
	    add(pt);	    // everything OK, add it to the data
	  }
	} else {
	    char tmp1[256];
	    ist.getline(tmp1,256);   // and read rest of line
	}
    }

    makeStatic();    // for faster access of the data
    return ist;
}





