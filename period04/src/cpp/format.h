/*----------------------------------------------------------------------------
 *  FORMAT.h
 *        contains definitions
 *---------------------------------------------------------------------------*/

#ifndef __format__
#define __format__

#define FORMAT_NUMBER         "F%-3i"
#define FORMAT_FREQUENCY      "%.15g"
#define FORMAT_FREQUENCY_ERROR "%.4g"  // mehr digits ??
#define FORMAT_AMPLITUDE      "%.9g"
#define FORMAT_AMPLITUDE_ERROR "%.4g"
#define FORMAT_PHASE          "%.6g"
#define FORMAT_PHASE_ERROR    "%.3g"
#define FORMAT_EPOCH          "%.6g"
#define FORMAT_ZEROPOINT      "%.9g"
#define FORMAT_RESIDUALS      "%.9g"
#define FORMAT_WEIGHTS        "%.6g"
#define FORMAT_COLOR          "%s"
#define FORMAT_TIME           "%.12g"

#define FORMAT_ACTIVE_FREQUENCIES "%i"

#define FIXEDFORMAT_NUMBER    "F%-3i"
#define FIXEDFORMAT_FREQUENCY "%15.9g"
#define FIXEDFORMAT_COMPO     "%15s"
#define FIXEDFORMAT_AMPLITUDE "%12.9g"
#define FIXEDFORMAT_PHASE     "%8.6g"
#define FIXEDFORMAT_EPOCH     "%20.16g"
#define FIXEDFORMAT_ZEROPOINT "%12.9g"
#define FIXEDFORMAT_RESIDUALS "%12.9g"
#define FIXEDFORMAT_WEIGHTS   "%8.6g"
#define FIXEDFORMAT_TIME      "%15.12g"

#endif
