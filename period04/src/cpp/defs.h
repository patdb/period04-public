/*----------------------------------------------------------------------------
 *  DEFS.h
 *        contains definitions
 *---------------------------------------------------------------------------*/

#ifndef __defs__
#define __defs__

//---
//--- The Application
//---
#define APPNAME      "Period04 v1.2.1"
#define VERSION      versiontxt
#define VERSIONDATE  versiondatetxt
#define AUTHOR       "Patrick Lenz"
#define EMAIL        "ptklenz@gmail.com"
#define COPYRIGHT    "(c) 2004-2016"

//---
//--- File Extensions
//---
#define PROJECT_EXTENSION      "p04"
#define PROJECT_PATTERN        "*.p04"
#define FOURIER_EXTENSION      "fou"
#define FOURIER_PATTERN        "*.fou"
#define TIMESTRING_EXTENSION   "dat"
#define TIMESTRING_PATTERN     "*.dat"
#define PHASESTRING_EXTENSION  "pha"
#define PHASESTRING_PATTERN    "*.pha"

//---
//--- The Timestring-table settings
//---
#define TS_TABLE_NAME      "Time string table"
#define TS_TABLE_TIME      "Time"
#define TS_TABLE_PHASE     "Phase"
#define TS_TABLE_OBS       "Observed"
#define TS_TABLE_ADJ       "Adjusted"
#define TS_TABLE_PNT_WEIGHT "Pnt.weight"
#define TS_TABLE_CALC      "Calculated"
#define TS_TABLE_RESOBS    "Residuals(Obs)"
#define TS_TABLE_RESADJ    "Residuals(Adj)"
#define TS_TABLE_WEIGHT    "Weight"
#define TS_TABLE_NONE      "Ignore"

//---
//--- Constants to be read in from file .period04-pref
//---
extern int    MAX_FREQUENCIES;
extern double DEFAULT_ALIAS_STEP;
extern char*  DEFAULT_FOURIER_NAME;
extern double DEFAULT_FOURIER_LOWER_FREQUENCY;
extern double DEFAULT_FOURIER_UPPER_FREQUENCY;
extern char*  DEFAULT_SCALE_SETTING;
extern char*  DEFAULT_NAME_SET[4];

#endif
