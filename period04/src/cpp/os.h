#ifndef __OS_h__
#define __OS_h__

#include<string.h>

inline int my_strcasecmp(const char *t1, const char *t2) {
#ifdef WIN32  // compiler option: -DWIN32
  return (stricmp(t1,t2));
#else
  return (strcasecmp(t1,t2));
#endif
}

inline int my_strncasecmp(const char *t1, const char *t2, size_t n) {
#ifdef WIN32  // compiler option: -DWIN32
  return (strnicmp(t1,t2,n));
#else
  return (strncasecmp(t1,t2,n));
#endif
}

#endif // __OS_H__
