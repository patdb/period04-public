/*--------------------------------------------------------------------------
 *  CTimePoint.h
 *              declares the CTimePoint Class
 *-------------------------------------------------------------------------*/

#ifndef __timepnt_h__
#define __timepnt_h__

#include <iostream>
using std::ostream;
using std::cerr;       
using std::endl;

#include <cmath>

#define NO_ADJUST 99934.34

class CTimePoint
{
 private:

    double /*const*/ mTime;        // time <<<-- wegen vectorarray kein const
    double /*const*/ mObserved;    // observed
    double       mAdjusted;        // adjusted
    double       mCalculated;      // calculated
    double       mCalculatedStore; // store for mCalculated for timeshiftcalcs
    double       mSimulated;       // noise data for monte carlo simulation
    double       mSimulatedBase;   // stores the old mCalculated for simulations
    double       mSW2Data;         // simulated Data for SpectralWindow2
    double       mWeight;          // weight
    double       mPointWeight;     // weight for point
    int          mNames[4];        // array of names
    static int   mNextIndex;       // the index to use for the next file
    int          mRunningIndex;    // the global running index
	
 public:

    // constructors
    CTimePoint(int i=0);
    CTimePoint(double t, double o, double a=(NO_ADJUST),
	       double c=0, double pw=1.0,
	       int n1=0, int n2=0, int n3=0, int n4=0, int index=0);

    // deconstructor
    ~CTimePoint();

	static bool  mUsePointError;  // use point error instead of weight

    int getRunningIndex() const      // returns the running index of the data
	{ return mRunningIndex; }

    double getTime() const           // returns the time of the data
	{ return mTime; }
    double getPhasedTime(double Frequency, double zeropoint=0.0) const {
	double tmp, pha; 
	pha = std::modf((mTime-zeropoint)*Frequency,&tmp);
	if (pha<0) {return 1.0+pha; }
	else { return pha; }
    }
    double getObserved() const       // returns the observed value of the data
	{ return mObserved; }
    void   setObserved(double o)  // sets the observed value 
	{ mObserved=o; }          // only used by makeXXXResidualsObserved

    double getAdjusted() const       // returns the adjusted value of the data
	{ return mAdjusted; }
    void   setAdjusted(double t)     // sets the adjusted value of the data
	{ mAdjusted=t; }

    double getCalculated() const  // returns the calculated value for the data
	{ return mCalculated; }
    void   setCalculated(double t)  // sets the calculated value for the data
	{ mCalculated=t ; }

    double getCalculatedStore() const  // returns the stored calculated value for the data
	{ return mCalculatedStore; }
    void   setCalculatedStore(double t)  // sets the stored calculated value for the data
	{ mCalculatedStore=t ; }

    double getSimulated() const     // returns the value from monte carlo sim.
	{ return mSimulated; }
    void   setSimulated(double t)   // sets the simulated value for the data
	{ mSimulated=t ; }

    double getSimulatedBase() const   { return mSimulatedBase; }
    void   setSimulatedBase(double t) { mSimulatedBase=t; }
  
    double getSW2Data() const { return mSW2Data; }
    void   setSW2Data(double t) { mSW2Data=t; }

    // returns the residuals in dependance to the observed values
    double getDataResidual() const { return mObserved-mCalculated; }
    // returns the residuals in dependance to the adjusted values
    double getAdjustedResidual() const { return mAdjusted-mCalculated; }

    // sets the weight for the data
    void   setWeight(double w) { mWeight=w; }
    // returns the weight for the data
    double getWeight() const   { return mWeight; }

    // returns the point weight for this data point
    double getPointWeight() const { return mPointWeight; }
  
    // returns weight based on residuals
    double getResidualWeight(int pos, double cutoff) { 
	double val;
	switch (pos) {
	    case 0: // none
		return 1;
	    case 1: // use observed residuals
		val = mObserved;
		break;
	    case 2: // use adjusted residuals
		val = mAdjusted;
		break;
	    case 3: // use simulated residuals
		val = mSimulated;
		break;
	}
	val-=mCalculated;
	val=std::fabs(val);
	if (val<=cutoff) { return 1; }
	val=cutoff/val;
	return val*val;
    }
    
    // returns the ID of the name of the data for a column
    int  getIDName(int column) const { return mNames[column]; }
    // sets the ID of the name of the data for a column
    void setIDName(int column, int ID) { mNames[column]=ID; }
};

// compare operators
int operator<(CTimePoint const &t1, CTimePoint const &t2);
int operator<=(CTimePoint const &t1, CTimePoint const &t2);

#if 0
// unused
// output
ostream& operator<<(ostream &str, CTimePoint const &t);
#endif

#endif
