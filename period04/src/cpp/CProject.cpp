/*----------------------------------------------------------------------------
 *  CProject.cpp
 *---------------------------------------------------------------------------*/

#define PROGRESS 1

#if PROGRESS
#include "progress_filter.h"
#endif

#include <boost/algorithm/string.hpp> // iequals
#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>

#include <fstream>      
using std::ifstream;
using std::ofstream;

#include <sstream>       

#include <sys/stat.h>
#include <ctype.h>

#include <string>

#include <iostream>
using std::cout;

#include <stdio.h>     // FILE ...
#include <time.h>      // time, ctime

#include "version.h"
#include "defs.h"
#include "CProject.h"
#include "message.h"
#include "os.h"
#include "Period04_java.h" // setProgressbarValue 

CProject::CProject ( )
  : projectFile( "")
  , period( MAX_FREQUENCIES + 2)
  , periodBM( MAX_FREQUENCIES + 2)
  , fitactive( 0)
  , defaultfourier
    ( DEFAULT_FOURIER_NAME
    , KURTZ1985
    , DEFAULT_FOURIER_LOWER_FREQUENCY
    , DEFAULT_FOURIER_UPPER_FREQUENCY
    )
  , fourierActive( &defaultfourier)
  , freqstep( DEFAULT_ALIAS_STEP)
  , mErrorScale( 1.0)
  , changed( 0)
  , fileFormatVersion( 0)
  , standardFileFormat( "AUTO")
  , mInputFormatBackUp( "to1234")
  , numberOfFiles( 0)
  , expertMode( false)
  , mFreqRes4CompoSearch( 0.0)
{
  // fix decimal point!
  std::locale::global(std::locale("C"));

  period.add( );           
  period.setBMMode( false);  // deactivate periodic time shift mode
  periodBM.add( );
  periodBM.setBMMode( true); // activate for periodic time shift calculations
  setFitMode( 0);            // start with normal mode
  setFitError( );            // write the default heading into fit_protocol
}

CProject::~CProject ( )
{
  cleanAll( );
}

void CProject::cleanAll ( )
{
  cleanFourier( );         // clean fourier-space...
  cleanAllPeriods( );      // cleans all period data
  cleanTimeString( );      // remove data from Timestring
  // OK, now that we have cleaned everything,
  // there have not been any new changes yet..
  changed = 0;
  projectFile = "";
}

void CProject::cleanSpecial
  ( bool fourier
  , bool periods
  , bool timestring
  )
{
  if (fourier)
    cleanFourier( );         // clean fourier-space...

  if (periods)
    cleanAllPeriods( );      // cleans all period data

  if (timestring)
    cleanTimeString( );      // remove data from Timestring
}

bool CProject::fileExist ( std::string const & path)
{
  // return 1 for OK, 0 for bad
  struct stat stbuf;

  if (("" == path) && (stat( path.c_str( ), &stbuf)))
  {
    return false;
  }
  
  FILE * file = fopen( path.c_str( ), "r");

  if (0 == file)
  {
    return false;
  }
  
  bool result = true;
  if (EOF == fgetc( file)) { result = false; }
  if (ferror( file)) { result = false; }
  fclose( file);
  
  return result;
}

bool CProject::canWrite ( std::string const & path)
{
  std::ofstream myfile( path.c_str( ));

  if (! myfile)
  {
    return false;
  }

  return true;
}

void CProject::newProject ( )
{
  // OK, now lets clean it... 
  cleanAll( );
}

namespace
{
  std::string const cmd_Version = "[Version:";
  std::string const cmd_ProgramSettings = "[ProgramSettings]";
  std::string const cmd_TimeString = "[TimeString]";
  std::string const cmd_Period = "[Period]";
  std::string const cmd_PTSPeriod = "[PTSPeriod]";
  std::string const cmd_Fourier = "[Fourier]";
  std::string const cmd_Log = "[Log]";
}
    
    
std::string CProject::loadProject ( std::string const & path)
{
  std::string protocol;

  // clean data...
  cleanAll( );

#if PROGRESS
  ifstream file_sink( path.c_str( ));

  // check for errors...
  if (! file_sink)
  {
    std::ostringstream text;
    text << "Can not read from file" << std::endl << path;
    showMessage( text.str( ).c_str( ));
    return protocol;
  }

  // Example
  // http://www.cplusplus.com/reference/iostream/istream/seekg/
  file_sink.seekg( 0, std::ios::end);
  std::streampos file_size = file_sink.tellg( );
  file_sink.seekg( 0);
  
  basic_progress_filter< char> progress_filter
    ( *(file_sink.rdbuf( ))
    , file_size
    , progress
    )
  ;

  std::istream file( &progress_filter);
#else
  ifstream file( path.c_str( ));

  // check for errors...
  if (! file)
  {
    std::ostringstream text;
    text << "Can not read from file" << std::endl << path;
    showMessage( text.str( ).c_str( ));
    return protocol;
  }
#endif

  addRecentFile( path);   // add file to the list of recent files

  std::string tmp;

  while (! file.eof( ))
  {
    // read in command
    file >> tmp;

    // parse version
    // [Version:V2.0.0]
    static boost::regex re( "^\\" + cmd_Version + "V([[:digit:]]+)\\.([[:digit:]]+)\\.([[:digit:]]+)\\]", boost::regex::extended);
    boost::smatch match;

//    if (boost::iequals( tmp.substr( 0, cmd_Version.length( )), cmd_Version))
      if (boost::regex_match( tmp, match, re))
    {
      // calculates the version
      fileFormatVersion
        = boost::lexical_cast< int>( std::string( match[1].first, match[1].second)) * 10000 // major
        + boost::lexical_cast< int>( std::string( match[2].first, match[2].second)) *   100 // minor
        + boost::lexical_cast< int>( std::string( match[3].first, match[3].second))         // micro
      ;
    }
    else if (boost::iequals( tmp, cmd_ProgramSettings))
    {
      readProgramSettings( file);
    }
    else if (boost::iequals( tmp, cmd_TimeString))
    {
      readTimestring( file);
    }
    else if (boost::iequals( tmp, cmd_Period))
    {
      readPeriod( file);
    }
    else if (boost::iequals( tmp, cmd_PTSPeriod))
    {
      readBMPeriod( file);
    }
    else if (boost::iequals( tmp, cmd_Fourier))
    {
      readFourier( file);
    }
    else if (boost::iequals( tmp, cmd_Log))
    {
      protocol = readProtocol( file);
    }
    else
    {
      ostringstream text;
      text << "Unknown project keyword: " << tmp;
      showMessage( text.str( ).c_str( ));

      // wait for eof or [
      char c;

      while ((! file.eof( )) && ( file >> c, c != '['))
      {
        // do nothing
      }

      // if there is a new comment then put [ back
      if (c == '[')
        file.putback( c);
    }
  }

  projectFile = path;      // make a copy of the filename...
  activateFourier( 0);     // activate the first fourier loaded
  setUseWeightFlags( );    //

  noChanges( );    // file is loaded, so we can continue as unchanged...
  return protocol;
}

long CProject::saveProject( std::string const & path)
{   
  // check if we can write a file
  if (! canWrite( path))
  {
    ostringstream text;
    text << "Cannot write to file " << path;
    showMessage( text.str( ).c_str( ));
    return -1;
  }
  
  // make a copy of the filename...
  projectFile = path;

  // save file 
  ofstream file( projectFile.c_str( ));

  // check for file-error
  if (! file)
  {
    ostringstream text;
    text << "Cannot write to file " << projectFile;
    showMessage( text.str( ).c_str( ));
    return -1;
  }

  file << cmd_Version << VERSION << "]\n\n"; // write version

  file << cmd_ProgramSettings << "\n";         // write program settings
  writeProgramSettings( file);

  file << cmd_Period << "\n";                  // write period
  writePeriod( file);

  file << cmd_PTSPeriod << "\n";               // write the period time shift period
  writeBMPeriod( file);

  file << cmd_TimeString << "\n";              // write timestring
  writeTimestring( file);

                                               // write all the fourier-parts
  writeFourier( file);                         // writeFourier writes it's own headers
  file << cmd_Log << "\n";                     // write the log
  writeProtocolToFile( file);


  changed = 0;      // file is saved, so we can continue as unchanged...

  return file.tellp( );    // return size of saved data
}

void CProject::writeProgramSettings(ostream &ost) {
    ost<<"ProgramMode=\t"<<expertMode<<"\n";
    ost<<endl;
}

void CProject::readProgramSettings(istream &ist) {
    bool btmp;
    char c,tmp[256];
    while(ist>>c,ist.putback(c),c!='[') {
	ist>>tmp;	// read in argument
	if (my_strcasecmp(tmp,"ProgramMode=")==0) {
	    ist>>btmp;
	    setExpertMode(btmp);
	}
    }
}

void CProject::setUseWeightFlags() {

    // activate weights if required
    int useweights=0;
    if (getUseNameWeight())      { useweights=1;} 
    if (getUsePointWeight())     { useweights=1;} 
    if (getUseDeviationWeight()) { useweights=1;} 
    // now set flags
    setPeriodUseWeight(useweights);
    setFourierUseWeight(useweights);
}

//---
//--- protocoll related methods 
//---

std::string CProject::readProtocol ( istream & ist)
{
  static int const buffsize = 4096;
    char c, tmpin[buffsize], tmpout[buffsize];
    int putin, putout;
    std::ostringstream ostr;

    while( (!ist.eof()) && (ist.get(c)) ) {
	ist.putback(c); // put character back
	// check if new command-block
	if (c=='[') {
	    break;
	}
      // write this out...
      for (putin=0; putin<buffsize; putin++) {
	  tmpin[putin]=0; tmpout[putin]=0;
      }
      ist.getline(tmpin,buffsize-1);

      putout=0;
      // check if last character is not empty
      int len=strlen(tmpin);

      for (putin=0;putin<len;putin++) {
	  c=tmpin[putin];
	  if (c==0) {
	      tmpout[putout]=0;
	      putout++;
	      break;
	  }
	  // add character to buffer if it is valid
	  if (!iscntrl(c)) {
	      tmpout[putout]=c;
	      putout++;
	  }
      }
      ostr << tmpout << endl;
    }
    // if there is a new comment then put [ back
    if (c=='[') ist.putback(c);

  return ostr.str( );
}

void CProject::writeProtocolToFile ( ostream & ost)
{
  ost << protocoltext << endl;
}

std::string CProject::getDate ( )
{
  time_t now = time( NULL);
  char * date= ctime( &now);
  date[ 24] = 0;
  return date;
}

void CProject::writeDefaultPreferences ( )
{
  writePreferences
    ( "DEFAULT"
    , "DEFAULT"
    , "2560"
    , "0.0"
    , "50.0"
    , "ALL"
    , "DEFAULT"
    , "MAG"
    , "Date"
    , "Observatory"
    , "Observer"
    , "Other"
    , "AUTO"
    )
  ;
}

void CProject::writePreferences
  ( std::string const & prog_mode
  , std::string const & working_dir
  , std::string const & max_freq
  , std::string const & freq_low
  , std::string const & freq_high
  , std::string const & compact
  , std::string const & alias_step
  , std::string const & scale
  , std::string const & name1
  , std::string const & name2
  , std::string const & name3
  , std::string const & name4
  , std::string const & inputformat
  )
{
  // writes a new '.period04-pref' file using the default values
  std::string path = getUserDirectory( ) + ".period04-pref";
  std::ofstream new_pref_file( path.c_str( ));

    
    if ( new_pref_file.is_open() ) {
	new_pref_file << 
	    "#\n# .period04-pref\n#\n"\
	    "# This is the Period04 preferences file. You can set\n"\
	    "# your personal settings by replacing the default values.\n"\
	    "# If this file is not found, Period04 creates a new one\n"\
	    "# using its original settings.\n#\n"\
	    "#------------------------------------------\n"\
	    "PROGRAM_MODE                               "
		      << prog_mode <<"\n#\n"\
	    "# start the program in the given mode\n"\
	    "# possible values: DEFAULT\n"\
	    "#                  EXPERT\n"\
	    "#------------------------------------------\n"\
	    "WORKING_DIRECTORY                          "
		      << working_dir << "\n#\n"\
	    "# possible values: \n"\
	    "#     DEFAULT = set the user directory as\n"\
	    "#               default working directory\n"\
	    "#     a user-defined directory\n"\
	    "#------------------------------------------\n"\
	    "MAXIMUM_FREQUENCIES                        "
		      << max_freq << "\n#\n"\
	    "# maximum number of frequencies\n"\
	    "#------------------------------------------\n"\
	    "DEFAULT_FOURIER_LOWER_FREQUENCY            "
		      << freq_low << "\n#\n"\
	    "# defines the default lower boundary\n"\
	    "# of the frequency range for\n"\
	    "# Fourier calculations\n"\
	    "#------------------------------------------\n"\
	    "DEFAULT_FOURIER_UPPER_FREQUENCY            "
		      << freq_high << "\n#\n"\
	    "# defines the default upper boundary\n"\
	    "# of the frequency range for\n"\
	    "# Fourier calculations\n"\
	    "#------------------------------------------\n"\
	    "DEFAULT_FOURIER_COMPACT_MODE               "
		      << compact << "\n#\n"\
	    "# defines the default setting for\n"\
	    "# Fourier calculations:\n"\
	    "#    ALL = all data points of a Fourier\n"\
	    "#          spectrum are saved\n"\
	    "#    PEAKS_ONLY = only the maxima and\n"\
	    "#          minima of the spectrum are saved\n"\
	    "#------------------------------------------\n"\
	    "DEFAULT_ALIAS_STEP                         "
		      << alias_step << "\n#\n"\
	    "# defines the default alias step rate:\n"\
	    "#    DEFAULT = (1./365.)\n"\
	    "#    a user-defined number\n"\
	    "#------------------------------------------\n"\
	    "DEFAULT_SCALE_SETTING                      "
		      << scale << "\n#\n"\
	    "# defines the default scale setting:\n"\
	    "#    MAG = magnitudes\n"\
	    "#    INT = intensity\n"\
	    "#------------------------------------------\n"\
	    "DEFAULT_NAME_SET[0]                        "
		      << name1 << "\n"\
	    "DEFAULT_NAME_SET[1]                        "
		      << name2 << "\n"\
	    "DEFAULT_NAME_SET[2]                        "
		      << name3 << "\n"\
	    "DEFAULT_NAME_SET[3]                        "
		      << name4 << "\n#\n"\
	    "# this defines the default attribute names\n"\
	    "#------------------------------------------\n"\
	    "DEFAULT_TIMESTRING_FILE_FORMAT             "
		      << inputformat << "\n#\n"\
	    "# this defines the default file format for\n"\
	    "# reading in time strings.\n"\
	    "#    AUTO = try automatic determination\n"\
	    "# You may also enter a user-defined string,\n"\
	    "# composed of the following letters:\n"\
	    "#    t ... time\n"\
	    "#    o ... observed\n"\
	    "#    a ... adjusted\n"\
	    "#    c ... calculated\n"\
	    "#    w ... point weight\n"\
	    "#    u ... point error\n"\
	    "#    r ... residuals(observed)\n"\
	    "#    R ... residuals(adjusted)\n"\
	    "#    1 ... attribute #1\n"\
	    "#    2 ... attribute #2\n"\
	    "#    3 ... attribute #3\n"\
	    "#    4 ... attribute #4\n"\
	    "#    5 ... weight(attribute #1)\n"\
	    "#    6 ... weight(attribute #2)\n"\
	    "#    7 ... weight(attribute #3)\n"\
	    "#    8 ... weight(attribute #4)\n"\
	    "#    i ... ignore\n"\
	    "#------------------------------------------\n#\n"\
	    "# put your comments here:\n"\
	    "# (note: the first character in your comment\n"\
	    "#        lines has to be '#')\n#\n";
	new_pref_file.close();
    } else {
	MYERROR("Failed to write the \"period04-pref\" file!");
    }
}

void CProject::readPreferences ( )
{
    // reads preferences from file '.period04-pref'
    double   conf_alias_step = (1./365.);
    double   conf_fou_lower  = 0.0;
    double   conf_fou_upper  = 50.0;
    CompactMode conf_fou_compact = PEAKS_ONLY;
    int      conf_scale      = 1;
    int      conf_max_freq = 2560;
    std::string  s_prog_mode;
    std::string  s_max_freq;
    std::string  s_alias_step;
    std::string  s_fou_lower;
    std::string  s_fou_upper;
    std::string  s_fou_compact;
    std::string  s_scale;
    std::string  conf_name[4];
    std::string  conf_fileformat;
    std::string conf_working_dir;
    bool     isWorkingDirDefined = false;
    bool     corrupted       = false;
    int      countReadings   = 0;
    int      numberOfReadings= 12;  // number of parameters to read in

    std::string path = getUserDirectory( ) + ".period04-pref";
    std::ifstream pref_file( path.c_str( ));

    if ( !pref_file.is_open() ) {
	// create new default file
	writeDefaultPreferences();
	/*	showMessage(
	    "Cannot locate the Period04 preferences file\n'.period04-pref'!\n\n" \
	    "A new '.period04-pref' file using the default values\n" \
	    "has been created in your user directory.\n"\
	    "If you prefer to use other values as default\n" \
	    "you may have to edit that file.\n\n"\
	    "Please press 'OK' to resume.");*/
	setDefaultSettings();
	return; 
    }
    
    char line[512];
    while ( pref_file.getline(line,512) ) {
	switch ( line[0] ) {
	    case '#':
		break;
	    default:
	    {
		// read definition string
		string defstring = strtok(line," ");
		// check what to read in
		if (defstring.compare("PROGRAM_MODE")==0) {
		    s_prog_mode = strtok(NULL," ");
		    if (s_prog_mode=="DEFAULT")     { expertMode = false; }
		    else if (s_prog_mode=="EXPERT") { expertMode = true; }
 		    else { corrupted=true; break; }
		    countReadings++;
		}
		else if (defstring.compare("WORKING_DIRECTORY")==0) {
		    conf_working_dir = strtok(NULL," ");
		    isWorkingDirDefined = true;
		    // no countReadings here
		}
		else if (defstring.compare("MAXIMUM_FREQUENCIES")==0) {
		  s_max_freq = strtok(NULL," ");
		  conf_max_freq = atoi(s_max_freq.c_str());
		  if (conf_max_freq<1) { conf_max_freq = 2560; }
		  countReadings++;
		}
		else if (defstring.compare("DEFAULT_FOURIER_LOWER_FREQUENCY")==0) {
		    s_fou_lower = strtok(NULL," ");
		    conf_fou_lower = atof(s_fou_lower.c_str());
		    countReadings++;
		}
		else if (defstring.compare("DEFAULT_FOURIER_UPPER_FREQUENCY")==0) {
		    s_fou_upper = strtok(NULL," ");
		    conf_fou_upper = atof(s_fou_upper.c_str());
		    countReadings++;
		}
		else if (defstring.compare("DEFAULT_FOURIER_COMPACT_MODE")==0) {
		    s_fou_compact = strtok(NULL," ");
		    if (s_fou_compact=="PEAKS_ONLY") { conf_fou_compact = PEAKS_ONLY; }
		    else if   (s_fou_compact=="ALL") { conf_fou_compact = ALL_DATA; }
 		    else { corrupted=true; break; }
		    countReadings++;
		}
		else if (defstring.compare("DEFAULT_ALIAS_STEP")==0) {
		    s_alias_step = strtok(NULL," ");
		    conf_alias_step = atof(s_alias_step.c_str());
		    // if the entry is not useful, so set the default value
		    if (conf_alias_step==0.0) { conf_alias_step=1.0/365.0; }
		    countReadings++;
		}
		else if (defstring.compare("DEFAULT_SCALE_SETTING")==0) {
		    s_scale = strtok(NULL," ");
		    if      (s_scale=="MAG") { conf_scale = 1; }
		    else if (s_scale=="INT") { conf_scale = 0; }
 		    else { corrupted=true; break; }
		    countReadings++;
		}
		else if (defstring.compare("DEFAULT_NAME_SET[0]")==0) {
		    conf_name[0] = strtok(NULL," ");
		    countReadings++;
		}
		else if (defstring.compare("DEFAULT_NAME_SET[1]")==0) {
		    conf_name[1] = strtok(NULL," ");
		    countReadings++;
		}
		else if (defstring.compare("DEFAULT_NAME_SET[2]")==0) {
		    conf_name[2] = strtok(NULL," ");
		    countReadings++;
		}
		else if (defstring.compare("DEFAULT_NAME_SET[3]")==0) {
		    conf_name[3] = strtok(NULL," ");
		    countReadings++;
		}
		else if (defstring.compare("DEFAULT_TIMESTRING_FILE_FORMAT")==0) {
		    conf_fileformat = strtok(NULL," ");
		    countReadings++;
		}
		break;
	    }
	}
    }
    pref_file.close();

    //--- check if file is OK
    if (corrupted || countReadings!=numberOfReadings) {
	int val = confirm((char*)"The preferences file \".period04-pref\"\n"\
			  "seems to be corrupted!\n"\
			  "Do you want to delete the old file and create\n"\
			  "a new preferences file using default values?");
	if (val==1) {
	    writeDefaultPreferences();
	} else {
	    showMessage("The program will use default values unless you\n"\
			"delete the corrupted \".period04-pref\" file.");
	}
	setDefaultSettings();
	return;
    }
    if (!isWorkingDirDefined) {
	//--- that's an old preferences file, so add this option!
	writePreferences(
	    s_prog_mode,"DEFAULT", s_max_freq, s_fou_lower, s_fou_upper, s_fou_compact,
	    s_alias_step, s_scale, conf_name[0], conf_name[1], conf_name[2],
	    conf_name[3], conf_fileformat);
	conf_working_dir = "DEFAULT"; // set this to default and go on
    }

  
    // set values
    setWorkingDirectory(conf_working_dir);
    //MAX_FREQUENCIES = conf_max_freq;
    //getPeriod().resizeMaxFreq(MAX_FREQUENCIES);
    //getBMPeriod().resizeMaxFreq(MAX_FREQUENCIES);
    getFourierActive()->setTitle(DEFAULT_FOURIER_NAME);
    getFourierActive()->setAlgorithm(KURTZ1985);
    getFourierActive()->setFrom(conf_fou_lower);
    getFourierActive()->setTo(conf_fou_upper);
    getFourierActive()->setCompact(conf_fou_compact);
    setFrequencyAdjustment(conf_alias_step);
    setReverseScale(conf_scale); 
    noChanges(); // neccessary as setReverseScale sets project changed

    
    for ( int i = 0; i < 4; ++i)
    {
      setAttributeName( i, conf_name[i].c_str( ));
    }

    setFileFormatString( conf_fileformat.c_str( ));
    
    return;
}

void CProject::setDefaultSettings ( )
{
    getFourierActive( )->setFrom( 0.0);
    getFourierActive( )->setTo( 50.0);
    setFrequencyAdjustment( 1./365.);
    setReverseScale( 1); 
    setAttributeName( 0, "Date");
    setAttributeName( 1, "Observatory");
    setAttributeName( 2, "Observer");
    setAttributeName( 3, "Other");
    setFileFormatString( "AUTO");
}

//---
//---  management of recent files
//---

void CProject::readRecentFiles ( )
{
    // reads a set of recent files from file '.period04-recentfiles'
    std::string path = getUserDirectory( ) + ".period04-recentfiles";
    std::ifstream file( path.c_str( ));
    
    if (!file.is_open()) { return; } // file not found

    int i=0;
    char line[256];
    while (file.getline(line,256)) {
	switch (line[0]) {
	    case '#':
		break;
	    default: {
		// only add files that do exist!
		if (! fileExist( line)) { break; }
		recentFile[i] = line;
		i++;
	    }
	}
    }
    numberOfFiles = i;
    file.close();
}

void CProject::saveRecentFiles ( )
{
    
  std::string path = getUserDirectory( ) + ".period04-recentfiles";
  std::ofstream ostr( path.c_str( ));

    if (!ostr.is_open()) { return; }

    ostr << "# .period04-recentfiles\n"\
	"#------------------------------------------------------------------"
	 << endl;
    for (int i=0; i<10; i++) {
	std::string tmp = getRecentFile(i);
	if (tmp!="") { ostr << tmp << endl; }
    }
}

void CProject::addRecentFile ( std::string const & path)
{
    int empty = 0;
    for (int i=0; i<10; i++) {
	if (getRecentFile(i)=="") { empty++; }
    }

    if (empty>0) { // there are empty places
	for (int i=0; i<10; i++) {
	    if (getRecentFile(i)=="") { // first of the empty places, add file
		recentFile[i] = path;
		saveRecentFiles();
		return;
	    } else { // entry is not empty, check if file is already listed
		if (recentFile[i]==path) { // file is in list, return!
		    return;
		}
	    }
	}
    } else { // array is full, check if file is in list, if not then add it
	int i;
	for (i=0; i<10; i++) {
	    // file is already in list!
	    if (recentFile[i]==path) { return; }
	}
	// file is not in list, so delete the oldest file and add the new one
	for (i=0; i<9; i++) {
	    recentFile[i] = recentFile[i+1];
	}
	recentFile[9] = path;
    }
    saveRecentFiles(); // write the current filenames to file
}

//---
//--- if the file is a member of recentFiles it will be removed
void CProject::removeRecentFile
  ( std::string const & path)
{
    int index=-1;
    for (int i=0; i<10; i++) {
	if (getRecentFile(i)==path) { 
	    index=i; break;
	}
    }
    if (index!=-1) {
	for (int j=index; j<9; j++) { 
	    recentFile[j] = recentFile[j+1]; // shift the filenames
	}
	recentFile[9] = ""; // for the last recent filename
	saveRecentFiles(); // write the current filenames to file
    }
}

/*** conversion methods **************************************/

double CProject::doubleFromString ( std::string const & str)
{
  double val=0;
  sscanf(str.c_str(),"%lf",&val);
  return val;
}

int CProject::integerFromString ( std::string const & str)
{
  int val=0;
  sscanf(str.c_str(),"%i",&val);
  return val;
}

