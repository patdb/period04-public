/*--------------------------------------------------------------------------
 *  CName.h
 *            declares: class CName
 *-------------------------------------------------------------------------*/

#ifndef __name_h__
#define __name_h__

#include <string>

//---
//---  class CName
//---  a data type that represents a collection of datapoints
//--- 
class CName
{
private:
    
  std::string mName;          // the name by itself
  int     mID;            // the name ID
  int     mColor;         // the color for points belonging to name 
  double  mWeight;        // the weight for points belonging to name 

  bool    mSelect;        // the selection value
  int     mSelectCopy;

  // unweighted data
  double  mOrig;
  double  mAdjusted;
  double  mOrigSigma;
  double  mAdjustedSigma;
  int     mPoints;        // number of points belonging to name

  // weighted data
  double  mOrigWeight;
  double  mAdjustedWeight;
  double  mOrigSigmaWeight;
  double  mAdjustedSigmaWeight;
  double  mWeightSum;

  double  mMeanTime;    // time average of points belonging to name

  int     mFrequencies; // period-solution for amplitude variations
  double * mAmplitude;   // pointer to amplitude-array for ampl.-variations
  double * mPhase;       // pointer to phase-array for phase-variations

 public:
 
    CName(int ID, std::string const & name);         // constructor
    CName(CName const &other);                   // copy-constructor
    ~CName();                                    // destructor

    std::string const & getName ( ) const { return mName; }
    void setName ( std::string const & name);

    int     getID() const { return mID; }

    bool    getSelect() const     { return mSelect; }
    void    setSelect(bool value) { mSelect=value; }

    double  getWeight() { return mWeight; }
    void    setWeight(double w) { if (w>0) { mWeight=w; } }

    char const *   getColor();
    int     getColorID() { return mColor; }
    char const *   getTrueColor();
    void    setColor ( std::string const & color);
	
	void    setMeanTime(double t) { mMeanTime=t; }
	double  getMeanTime() const   { return mMeanTime; }

    void    resetValues();       // resets default values for CName variables
    void    addPoint(double orig, double adj, double weight);

    double  getAverageOrig() const;
    double  getAverageAdj() const;
    double  getSigmaOrig() const;
    double  getSigmaAdj() const;
    int     getPoints() const { return mPoints; }

    double  getAverageOrigWeight() const ;
    double  getAverageAdjWeight() const;
    double  getSigmaOrigWeight() const;
    double  getSigmaAdjWeight() const;
    double  getWeightedSum() const { return mWeightSum; }

    friend int operator < (CName const & one, CName const & other);
    friend int operator <= (CName const & one, CName const & other);

    void   storeSelection() { mSelectCopy=mSelect; }
    void   restoreSelection() { mSelect=mSelectCopy; }
};

#endif

