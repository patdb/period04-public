/*----------------------------------------------------------------------------
 *  CMatrix.h
 *           declares the CVector and CMatrix class
 *---------------------------------------------------------------------------*/

#include <iostream>
using std::cerr;
using std::endl;
using std::istream;
using std::ostream;

#include "message.h"

class CMatrix;

//---
//--- CVector
//---
class CVector
{
    friend class CMatrix;

 protected:

    int entries;    //number of entries
    double * data;  //data entries

 public:

    //constructors
    CVector();
    CVector(int size, double fill=0);
    CVector(CVector const & vec);

    //destructor
    ~CVector();

 protected:

    void allocate(int size);  // allocate memory
    void deallocate();        // deallocate memory

 public:

    // operators
    CVector &operator=(CVector const &tmp);
    CVector &operator=(double fil);

    CVector &operator*=(double factor);
    CVector &operator/=(double factor);

    CVector &operator+=(CVector const & vec);
    CVector &operator+=(double val);

    CVector &operator-=(CVector const & vec);
    CVector &operator-=(double val);

    friend double operator*(CVector const &v1, CVector const &v2);

    inline double &operator[](int i) const {
      return data[i];
    }

    // size of vector
    inline int size()      {return entries;}
    void changesize(int size, double fill=0);

    // filling option
    void fill(double fil=0);
    void fill(int start, double fil=0);
    void fill(int start, int end, double fil=0);

    // file i/o
    friend ostream& operator<<(ostream &ost, CVector const &vec);
    friend istream& operator>>(istream &ist, CVector const &vec);

    istream& readin(istream &ist, int size);
};


#define SIMPLEOPS(cmd) {CVector tmp(vec); cmd; return tmp;}


// other operators:

inline CVector operator*(CVector const &vec, double factor)
  SIMPLEOPS(tmp*=factor)
inline CVector operator*(double factor, CVector const &vec)
  SIMPLEOPS(tmp*=factor)

inline CVector operator/(CVector const &vec, double factor)
  SIMPLEOPS(tmp/=factor)
inline CVector operator/(double factor, CVector const &vec)
  SIMPLEOPS(tmp/=factor)

inline CVector operator+(CVector const &vec, double factor)
  SIMPLEOPS(tmp+=factor)
inline CVector operator+(double factor, CVector const &vec)
  SIMPLEOPS(tmp+=factor)
inline CVector operator+(CVector const & vec, CVector const &vec1)
  SIMPLEOPS(tmp+=vec1)

inline CVector operator-(CVector const &vec, double factor)
  SIMPLEOPS(tmp-=factor)
inline CVector operator-(double factor, CVector const &vec)
  SIMPLEOPS(tmp-=factor)
inline CVector operator-(CVector const & vec, CVector const &vec1)
  SIMPLEOPS(tmp-=vec1)


//---
//--- CMatrix
//---
class CMatrix
{
 public:

    // constructors
    CMatrix();
    CMatrix(int rows, int cols, double fill=0);
    CMatrix(CMatrix const &mat);

    //destructor
    ~CMatrix();

 protected:

    // memory allocoation functions
    void clean();        // cleans constants (for constructors only !!!)
    void allocate(int rows,int cols, double fil=0);
    void deallocate();

 public:

    // operators
    CMatrix &operator=(CMatrix const& mat);
    CMatrix &operator=(double fil);

    CMatrix &operator*=(double fac);
    CMatrix &operator/=(double fac);

    CMatrix &operator+=(double val);
    CMatrix &operator-=(double val);

    CMatrix &operator*=(CMatrix const &mat);
    friend CMatrix operator*(CMatrix const &mat1, CMatrix const &mat2);
    friend CVector operator*(CVector const &vec1, CMatrix const &mat2);
    friend CVector operator*(CMatrix const &mat1, CVector const &vec2);

    CMatrix &operator/=(CMatrix const &mat);
    friend CMatrix operator/(CMatrix const &mat1, CMatrix const &mat2);
    friend CVector operator/(CVector const &vec1, CMatrix const &mat2);
    friend CVector operator/(CMatrix const &mat1, CVector const &vec2);

    CMatrix &operator+=(CMatrix const &mat);
    CMatrix &operator-=(CMatrix const &mat);

    // transpose Matrix
    void mirror() {
	if (totrows!=totcols) {
	    MYERROREXIT("Cannot mirror this Matrix - wrong dimensions!!!");
	}
	for(int j=0;j<totrows;j++) {
	    for(int k=0;k<j;k++) {
		Rows[k].data[j]=Rows[j].data[k];
	    }
	}
    }

    CMatrix operator!() const;
    double invert();
    double cond();                // calculate the condition number

    // swap rows and columes
    inline void exchangerows(int from, int to) {
	exchangerowsint(from, to);
    }

    inline void exchangecols(int from, int to) {
	exchangecolsint(from, to);
    }

 protected:

    inline void exchangerowsint(int from, int to) {
	double tmp;
	double *fr=Rows[from].data;
	double *de=Rows[to].data;
	for(int col=0;col<totcols;col++) {
	    tmp=fr[col];
	    fr[col]=de[col];
	    de[col]=-tmp;
	}
    }

    inline void exchangecolsint(int from, int to) {
	double tmp;
	double *tmprow;
	for(int row=0;row<totrows;row++) {
	    tmprow=Rows[row].data;
	    tmp=tmprow[from];
	    tmprow[from]=tmprow[to];
	    tmprow[to]=-tmp;
	}
    }

 public:

    // size of the matrix
    inline int rows() const    { return totrows; }
    inline int cols() const    { return totcols; }
    void changesize(int row,int col);

    // adress operators
    inline CVector &operator[](int i) const {
      return Rows[i];
    }
    
    // file I/O
    friend ostream &operator<<(ostream &ost, CMatrix const &mat);
    friend istream &operator>>(istream &ist, CMatrix const &mat);

    istream &readsize(istream &ist, int rows, int cols);

 protected:

    int      totrows;
    int      totcols;
    CVector *Rows;
    double   det;

};

