/*-------------------------------------------------------------------------*
 *  CNoisePoints
 *-------------------------------------------------------------------------*/

#ifndef __cnoisepoints_h__
#define __cnoisepoints_h__

#include "message.h"

class CNoisePoints {

 private:
    double *frequency;
    double *noise;
    int size;

 public:

    CNoisePoints() {
	frequency = new double[1];
	noise = new double[1];
	size=1;
    }

    CNoisePoints(int size) {
	frequency = new double[size];
	noise     = new double[size];
	this->size = size;
    }

    ~CNoisePoints() {
	clean();
    }

    int getSize() { 
	return size; 
    }

    void setSize(int newsize) {
	delete [] frequency;
	delete [] noise;
	frequency = new double[newsize];
	noise     = new double[newsize];
	this->size = newsize;
    }
    
    double getFrequency(int index) {
	if ( (index>size) || (index<0) ) { 
	    char txt[100];
	    sprintf(txt, "index [%d] out of bounds!", index);
	    MYERROREXIT(txt);
	}
	return frequency[index];
    }
    
    void setFrequency(int index, double freq) {
	if ( (index>size) || (index<0) ) { 
	    char txt[100];
	    sprintf(txt, "index [%d] out of bounds!", index);
	    MYERROREXIT(txt);
	}
	frequency[index] = freq;
    }

    double getNoise(int index) {
	if ( (index>size) || (index<0) ) { 
	    char txt[100];
	    sprintf(txt, "index [%d] out of bounds!", index);
	    MYERROREXIT(txt);
	}
	return noise[index];
    }

    void setNoise(int index, double amplitude) {
	if ( (index>size) || (index<0) ) { 
	    char txt[100];
	    sprintf(txt, "index [%d] out of bounds!", index);
	    MYERROREXIT(txt);
	}
	noise[index] = amplitude;
    }

    void clean() {
	delete [] frequency;
	delete [] noise;
	size = 0;
    }

};

#endif
