
#include <fstream>
using std::ofstream;

#include <sstream>      
using std::ostringstream;

#include <iostream>
using std::cout;
using std::endl;

#include "CProject.h"
#include "format.h"
#include "CColors.h"
#include "message.h"
#include "os.h"


void CProject::cleanTimeString() {
    timestring.clean();
}

void CProject::select() {
    timestring.select();  // select
    sel_change++;         // selection has changed...
}

void CProject::setReverseScale(int scale) { 
    timestring.setReverseScale(scale); 
    changed++; // project has changed
} 

void CProject::setErrorScale ( std::string const & tmp)
{
  double value=doubleFromString(tmp);
  if (value>0.0) {
      mErrorScale=value;
      //updateTSDisplays();
      //
      char msg[1024];
      sprintf(msg,"The error scale has been set to: "FORMAT_AMPLITUDE,value);
//      write()<<msg<<endl;;
    }
}

void CProject::subdivide
  ( double timegap
  , int attribute
  , int useCounter
  , std::string const & prefix
  , int digits
  )
{
  timestring.subdivide( timegap, attribute, useCounter, prefix, digits);
}

void CProject::subdivide
  (double start
  , double interval
  , int attribute
  , int useCounter
  , std::string const & prefix
  , int digits
  )
{
  timestring.subdivide( start, interval, attribute, useCounter, prefix, digits);
}

void CProject::combine ( int column, std::string const & Name)
{
  timestring.combine( column, Name);
}

void CProject::changeHeading
  ( int i
  , char const * mName
  )
{
  timestring.changeNameSet( i, mName);
}

void CProject::setAttributeName
  ( int i
  , std::string const & name
  )
{
  timestring.changeNameSet( i, name);
}

void CProject::adjust
  ( int attribute
  , DataMode orig
  , int n
  , int * ids
  , bool useweights
  )
{
  timestring.adjust( attribute, orig, n, ids, useweights);
}

std::string CProject::subtractZeroPoint ( )
{
  double value = 0.0;

  switch (fitmode)
  {
  case 0: value = period.getZeropoint( ); break;
  case 1: value = periodBM.getZeropoint( ); break;
  }

  ostringstream ostr;
  ostr.precision( 9);

  if (getPeriodUseData()==0 /*=observed*/)
  {
    timestring.subtractFromObserved(value);
    ostr<< "\nSubtracted the fitted zero point from 'Observed'.\n"
    << "Zeropoint:\t" << value << endl;
  }
  else
  {
    timestring.subtractFromAdjusted(value);
    ostr<< "\nSubtracted the fitted zero point from 'Adjusted'.\n"
    << "Zeropoint:\t" << value << endl;
  }

  return ostr.str( );
}

struct myentry {
public:
    int id;
    std::string string;
    std::string content;
};

int SortMyEntry(const void *t1,const void *t2) {
    std::string const & tmp1 = ((myentry *)t1)->string;
    std::string const & tmp2 = ((myentry *)t2)->string;
    if (tmp1>tmp2) return 1;
    if (tmp1<tmp2) return -11;
    return 0;
}

void CProject::computeMeanTimes(int attribute) {
  if (getSelectedPoints()<1) { 
	return;
  }
  int     idNames      = numberOfNames(attribute);
  int    *idPoints     = new int[idNames];
  double *mean_time    = new double[idNames];
  bool    found        = false;
  int i;

  // create sorted table
  myentry* array=new myentry[idNames];
  for (i=0; i<idNames; i++) { // fill with initial values
	mean_time[i]    = 0.0;
	idPoints[i]     = 0;
	array[i].string = getIndexNameStr(attribute,i).c_str( );
	array[i].id     = getIndexNameID(attribute,i);
  }
  for (i=0; i<getSelectedPoints(); i++) {
	double time = timestring[i].getTime();
	int id   = timestring[i].getIDName(attribute);
	for (int j=0; j<idNames; j++) {
	  if (id==array[j].id) { id = j; found=true; break; }
	}
	if (!found) {
	  MYERROR("Cannot find index!");
	}
	mean_time[id]+=time; // sum up the times
	idPoints[id]++;
  }
  for (i=0; i<idNames; i++) {
	timestring.getIndexName(attribute,i).setMeanTime(mean_time[i]/idPoints[i]);
  }

  delete [] idPoints;
  delete [] mean_time;
  delete [] array;
}

std::string CProject::getTimeStatistics(int attribute, bool unitIsDays) {
  char tmp [256];
  std::ostringstream ostr;
  int i;
  //---
  //--- check if there is a time string at all
  //---


  if (getSelectedPoints()<1)
  {
	sprintf(tmp, "%-20s%13s%13s%14s%13s%10s ", "Substring", "Start time", 
		"End time", "Length [hrs]", "Mean time", "Points");
	ostr<<tmp<< endl;
	ostr <<
	    "\n                       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"\
	    "                       !!!  There are no selected points !!!\n"\
	    "                       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" 
	    << endl;
	ostr << "\nTotal observing time [hrs]: " << endl;
	ostr << "Total mean time: " << endl;
	return ostr.str( );
    }

    int     idNames      = numberOfNames(attribute);
    double *start_time   = new double[idNames];
    double *end_time     = new double[idNames];
    double *mean_time    = new double[idNames];
    int    *idPoints     = new int[idNames];
    bool    found        = false;

    // create sorted table
    myentry* array=new myentry[idNames];
 
    for (i=0; i<idNames; i++) { // fill with initial values
	start_time[i]   = 1.0E20;
	end_time[i]     = -1.0E20;
	mean_time[i]    = 0.0;
	idPoints[i]     = 0;
	array[i].string = getIndexNameStr(attribute,i).c_str( );
	array[i].id     = getIndexNameID(attribute,i);
    }


    //---
    //--- find start and end time for each IDName
    //---
    for (i=0; i<getSelectedPoints(); i++) {
	double time = timestring[i].getTime();
	int id   = timestring[i].getIDName(attribute);
	for (int j=0; j<idNames; j++) {
	    if (id==array[j].id) { id = j; found=true; break; }
	}
	if (!found) {
	    MYERROR("Cannot find index!");
	}
	if (time<start_time[id]) { start_time[id] = time; }
	if (time>end_time[id])   { end_time[id] = time; } 
	mean_time[id]+=time; // sum up the times
	idPoints[id]++;
    }

    double duration = 0.0;
    double diff = 0.0;
    if (unitIsDays) {
	sprintf(tmp, "%-20s%13s%13s%14s%13s%10s ", "Substring", "Start time", 
		"End time", "Length [hrs]", "Mean time", "Points");
    } else {
	sprintf(tmp, "%-20s%13s%13s%14s%13s%10s ", "Substring", "Start time",
		"End time", "Length", "Mean time", "Points");
    }
    ostr << tmp << endl;
    for (i=0; i<idNames; i++) {
	if (getIndexNameSelect(attribute,i)) {
	    if (idPoints[i]<1) { continue; }
	    diff = end_time[i]-start_time[i];
	    if (unitIsDays) { diff*=24; } // in hours
	    duration+=diff;
	    sprintf(tmp, "%-20s%13f%13f%14f%13f%10i ",
		    getIndexName(attribute,i).getName().c_str(), start_time[i],
		    end_time[i], diff, mean_time[i]/idPoints[i], idPoints[i]);
	    array[i].content = tmp;
	}
    }
    // sort array
    qsort(array,idNames,sizeof(myentry),SortMyEntry);
    for (i=0; i<idNames; i++) {
	if (array[i].content!="")
	    ostr << array[i].content << endl;
    }
    ostr << endl;

    //---
    //--- show some additional information if the attribute is 'Date'
    //---
    if (attribute==0) {
	if (unitIsDays) {
	    ostr << "Total observing time [hrs]: " << duration << endl;
	} else {
	    ostr << "Total observing time: " << duration << endl;
	}
	sprintf(tmp, "Total mean time: %13f", getTimeAverage());
	ostr << tmp << endl;
    }
    
    delete [] start_time;
    delete [] end_time;
    delete [] mean_time;
    delete [] idPoints;
    delete [] array;

    return ostr.str( );
}

std::string CProject::getNameStatistics
  ( int boxID
  , int i
  , bool useweighted
  )
{
    char tmp[256];
    CName *data=&(getIndexName(boxID,i));
    double sigma, average;
    sigma   = 0;
    average = 0;
    char* isadj;
    if ( data->getSelect() && (data->getPoints()!=0) ) {
	if (useweighted) {
	    if (getPeriodUseData()==Observed) {
		sigma   = data->getSigmaOrigWeight();
		average = data->getAverageOrigWeight();
	    } else {
		sigma   = data->getSigmaAdjWeight();
		average = data->getAverageAdjWeight();
	    }
	} else {
	    if (getPeriodUseData()==Observed) {
		sigma   = data->getSigmaOrig();
		average = data->getAverageOrig();
	    } else {
		sigma   = data->getSigmaAdj();
		average = data->getAverageAdj();
	    }
	}
	// check if mean values are the same for both
	isadj=(char*)"!!!";
	if ( fabs(data->getAverageAdj()-data->getAverageOrig()) > 1e-14 ) {
	    isadj=(char*)"yes";
	} else {
	    isadj=(char*)"no";
	}
    } else {
	return "";
    }
    sprintf(tmp, "%-20s%13f%13f%10i %3s",
	    data->getName().c_str(),
	    average,sigma,
	    data->getPoints(),
	    isadj);
    return tmp;
}

void CProject::saveStat
  ( int what
  , int weighted
  , std::string const & filename
  )
{
  if (filename == "") { return; }

  std::ofstream file( filename.c_str( ));
  file << statStrings( what, weighted, 0);
}

std::string CProject::statStrings
  ( int what
  , int weighted
  , int header
  )
{
  std::string tmp;

  if (header)
  { 
    tmp = "Name                      Average        Sigma    Points  Adj\n";
  }
  
  for ( int i = 0; i < numberOfNames( what); ++i)
  {
    tmp += getNameStatistics( what, i, weighted) + "\n";
  }
  
  return tmp;
}

std::string CProject::makeObservedAdjusted ( )
{
  timestring.makeObservedAdjusted( );
  char txt [256];

  sprintf
    ( txt
    , "\nRestoring original data for %i points.\n"
    , getSelectedPoints( )
    )
  ;

  return txt;
}

std::string CProject::makeDataResidualsObserved ( )
{
  timestring.makeDataResidualsObserved( );
  ++changed;
  return "\nReplaced \"Observed\" by \"Residuals (Observed)\".\n";
}

std::string CProject::makeAdjResidualsObserved ( )
{
  timestring.makeAdjResidualsObserved( );
  ++changed;
  return "\nReplaced \"Observed\" by \"Residuals (Adjusted)\".\n";
}

std::string CProject::makeCalculatedObserved ( )
{
  timestring.makeCalculatedObserved( );
  ++changed;
  return "\nReplaced \"Observed\" by \"Calculated\".\n";
}

std::string CProject::makeAdjustedObserved ( )
{
  timestring.makeAdjustedObserved( );
  ++changed;
  return "\nReplaced \"Observed\" by \"Adjusted\".\n";
}

void CProject::loadTimeString
  ( std::string const & filename
  , std::string const & fileformat
  , int append
  , bool votable
  )
{
  if (! append) // are we appending?
  {
    timestring.clean( );   	// no, so remove all datapoints
  }
  
  timestring.setInputFormat( fileformat);    // set format...

  if (votable)
  {
    timestring.loadVOTable( filename);
  }
  else
  {
    timestring.load( filename);   // load data
  }
  
  timestring.selectAll( );      // select all data
  select( );                    // now reselect all
  ++changed;
}

void CProject::saveTimeStringPhase
  ( std::string const & name
  , std::string const & format
  , double frequency
  , double zeropoint
  , bool votable
  )
{
  if (format=="") { return; } // if format is empty then return
  if (name=="")   { return; } // if no filename is given then return

  // check if we can write a file
  if (! canWrite( name))
  {
    char text [1024];
    sprintf( text, "Cannot write to file %s", name.c_str( ));
    showMessage( text);
    return;
  }

  std::string oldFormat = timestring.getOutputFormat( ); // store old format
  timestring.setOutputFormat( format);             // set output format
  timestring.setUseVOTableFormat( votable);        // ASCII or VOTable?
  timestring.savePhase( name, frequency, zeropoint); // save the data
  timestring.setOutputFormat( oldFormat);          // restore old format

  return;
}

void CProject::saveTimeStringPhaseBinned
  ( std::string const & name
  , std::string const & format
  , double frequency
  , double binSpacing
  , DataMode useData
  , int points
  , double * phase
  , double * amplitude
  , double * sigma
  )
{
  if (name=="")   { return; } // if no filename is given then return

  //--- check if we can write a file
  if (! canWrite( name))
  {
    char text [1024];
    sprintf( text, "Cannot write to file %s", name.c_str( ));
    showMessage( text);
    return;
  }

  //--- save data
  {
    std::ofstream file( name.c_str( ));
    int counter;
    char text [256];

    for ( counter = 0; counter < points; ++counter)
    {
      sprintf
        ( text
        , FORMAT_PHASE"\t"FORMAT_AMPLITUDE"\t"FORMAT_AMPLITUDE
        , phase[counter]
        , amplitude[counter]
        , sigma[counter]
        )
      ;
      
      file << text << std::endl;
    }
  }

  return;
}

void CProject::saveTimeString
  ( std::string const & filename
  , std::string const & outputformat
  , bool votable
  )
{
  // is the format empty, so let's return
  if (outputformat == "") { return; }
  // set the format
  timestring.setOutputFormat( outputformat);
  timestring.setUseVOTableFormat( votable);           // ASCII or VOTable?

  // check if we can write a file
  if (! canWrite( filename))
  {
    char text [1024];
    sprintf( text, "Cannot write to file %s", filename.c_str( ));
    showMessage( text);
    return;
  }
  // save data
  timestring.save( filename);
  // set input-format to output-format
  timestring.setInputFormat( timestring.getOutputFormat( ));
}

void CProject::changeName
  ( int i
  ,int ID
  , std::string name
  , std::string const & Sweight
  , std::string const & color
  )
{
    double weight=doubleFromString(Sweight);
    // get original settings
    CName *tmp=&(timestring.getIDName(i,ID));
    // check if new name allready exists
    int names=timestring.numberOfNames(i);
    for (int index=0;index<names;index++) {
	CName &other=getIndexName(i,index);
	if ( (ID!=other.getID()) && (other.getName().c_str()==name) ) {
	    // show message
	    char msg[2048];
	    sprintf(msg,"The attribute %s does allready exist.\n"\
		    "Please use the Combine Substrings menu\n"\
		    "to join subsets...",name.c_str());
	    showMessage(msg);
	    // set flag
	    name=tmp->getName().c_str();
	    break;
	}
    }
    // set values
    tmp->setName(name.c_str( ));
    tmp->setWeight(weight);
    tmp->setColor(color.c_str());
}

std::string CProject::deletePoint
  ( int i
  )
{
  int what;
  std::string name;
  //--- get appropriate labels for deleted points
  getDeletePointInfo( what, name);    
  std::string tmp[ 4];
  tmp[ what] = name;
  //--- check whether the point has already been deleted
  CTimePoint & pnt = timestring[i];
  int oldid = pnt.getIDName( what);
  int nameid = timestring.getID( what, name);

  if (oldid == nameid)
  {
    char text [1024];

    sprintf
      ( text
      , "This point has already been deleted!\n"
        "You still see this point because the item \"%s\"\n"
        "in column \"%s\" which contains all deleted points\n"
        "is selected and therefore displayed!"
      , name.c_str( )
      , nameSet( what).c_str( )
      )
    ;

    showMessage( text);
    return std::string( );
  }

  //--- and finally relabel
  return relabelPoint
    ( i
    , tmp[0]
    , tmp[1]
    , tmp[2]
    , tmp[3]
    )
  ;
}

std::string CProject::deletePointsWithinBox
  ( double x_start
  , double y_start
  , double x_end
  , double y_end
  , int mode
  )
{
  ostringstream ostr;

  ostr
    << "Relabel datapoints to deleted:\n"
        "Time\t\tObserved\tAdjusted:"
//    << " " << x_start
//    << " " << y_start
//    << " " << x_end
//    << " " << y_end
//    << " " << mode
    << std::endl
  ;

  bool pointFound = false;
  bool showedMessage = false;
  
  for ( int index = 0; index < getSelectedPoints( ); ++index)
  {
    // get coordinates
    
    
    
	double amplitude=0.0, time = timestring[index].getTime();
	// check if data is within range
	if (time<x_start || time>x_end) { continue;}  
	// time is within box, so now check amplitude
	switch (mode) {
	    case 0: // observed
		amplitude = timestring[index].getObserved(); break;
	    case 1: // adjusted
		amplitude = timestring[index].getAdjusted(); break;
	    case 2: // residuals (obs)
		amplitude = timestring[index].getDataResidual(); break;
	    case 3: // residuals (adj)
		amplitude = timestring[index].getAdjustedResidual(); break;
	}

	if (amplitude<y_start || amplitude>y_end) { continue; }
	// amplitude is within box, delete the point

	pointFound = true;
	int what;
  std::string name;
	getDeletePointInfo( what, name);	// get defaults
	std::string tmp [4];	                        // assign to array
	tmp[what] = name;
	//--- check whether the point has already been deleted
	CTimePoint &pnt=timestring[index];
	int oldid=pnt.getIDName(what);
	int nameid = timestring.getID( what, name);
	if (oldid==nameid) {
	    if (!showedMessage) {
		char text[1024];
		sprintf(text, 
		    "Some of the points have already been deleted!\n"\
		    "You still see deleted points because the item \"%s\"\n"\
		    "in column \"%s\" which contains all deleted points\n"\
		    "is selected and therefore displayed!", 
		    name.c_str( ), nameSet( what).c_str( ));
		showMessage(text);
		showedMessage=true;
	    }
	    continue;      // don't relabel this point again!
	}


  //--- and finally relabel
  ostr << relabelPoint
    ( index
    , tmp[0]
    , tmp[1]
    , tmp[2]
    , tmp[3]
    , true
    )
  ;


    //--- the timepoints index has been shifted by 1, therefore ...
    --index;
  }

  // ptf to fix slow deletion of points.

  // now try Garbage Collection
  timestring.garbageCollect( );

  if (! pointFound)
  {
    return std::string( );
  }

  return ostr.str( );
}

std::string CProject::relabelPoint
  ( int i
  , std::string const & name0
  , std::string const & name1
  , std::string const & name2
  , std::string const & name3
  , bool quiet
  )
{
    ostringstream tmp;
    
    CTimePoint &pnt=timestring[i];
    char tmpstr[512];
    if (quiet) {
	sprintf(tmpstr, FORMAT_TIME"\t"FORMAT_AMPLITUDE"\t"FORMAT_AMPLITUDE,
		pnt.getTime(), pnt.getObserved(), pnt.getAdjusted());
    } else {
	sprintf(tmpstr,"Relabel datapoint:\n"\
		"Time:     "FORMAT_TIME"\n"\
		"Observed: "FORMAT_AMPLITUDE"\n"\
		"Adjusted: "FORMAT_AMPLITUDE,
		pnt.getTime(), pnt.getObserved(), pnt.getAdjusted());
    }
	
    tmp<<tmpstr<<endl;
    // create log
    if (name0!="") {
	int n0=timestring.getID(0,name0);
	int oldid=pnt.getIDName(0);
	if (oldid!=n0) {
	    if (!quiet) { // write log

        
      tmp
        << "from " << timestring.getIDName( 0, oldid).getName( )
        << " to " << name0
        << std::endl
      ;
    

	    }
	    // rename
	    pnt.setIDName(0,n0);
	}
    }
    if (name1!="") {
	int n1=timestring.getID(1,name1);
	int oldid=pnt.getIDName(1);
	if (oldid!=n1) {
	    if (!quiet) { // write log
		sprintf(tmpstr,"from %s to %s\n",
			timestring.getIDName(1,oldid).getName().c_str(),
			name1.c_str());
		tmp<<tmpstr;
	    }
	    // rename
	    pnt.setIDName(1,n1);
	}
    }
    if (name2!="") {
	int n2=timestring.getID(2,name2);
	int oldid=pnt.getIDName(2);
	if (oldid!=n2) {
	    if (!quiet) { // write log
		sprintf(tmpstr,"from %s to %s\n",
			timestring.getIDName(2,oldid).getName().c_str(),
			name2.c_str());
		tmp<<tmpstr;
	    }
	    // rename
	    pnt.setIDName(2,n2);
	}
    }
    if (name3!="") {
	int n3=timestring.getID(3,name3);
	int oldid=pnt.getIDName(3);
	if (oldid!=n3) {
	    if (!quiet) { // write log
		sprintf(tmpstr,"from %s to %s\n",
			timestring.getIDName(3,oldid).getName().c_str(),
			name3.c_str());
		tmp<<tmpstr;
	    }
	    // rename
	    pnt.setIDName(3,n3);
	}
    }

    // ptf to fix slow deletion of points.
    if (! quiet)
    {
      // now try Garbage Collection
      timestring.garbageCollect( );
    }

    // change selections
    select( );

    return tmp.str( );
}

void CProject::readTimestring
  ( istream & ist
  )
{
  char c, tmp [ 2048];

  while (ist>>c, ist.putback(c), c!='[')
  {
    ist >> tmp; 	// read in argument

	if (my_strcasecmp(tmp,"Filename=")==0) {
	    if (fileFormatVersion>=20000) {
		//--- this version is up to date!
		ist.putback(ist.get());	    // skip tab
		ostringstream complete;     // save the filenames in complete
		while (true) {
		    ist.getline(tmp,2048);	    // get a line
		    //--- find first nonwhite name
		    char *tmp1=tmp; 	    
		    while ( (isspace(*tmp1)) && !(*tmp1==0) ) { tmp1++; }
		    //--- check for the end of the Filename field
		    if (strstr(tmp,"end.Filename")!=NULL) { break; }
		    complete << tmp1 << endl;   // add filename
		}
		//--- finally set filename 
		timestring.setFileName(complete.str().c_str());   
	    } else {
		//--- ok, this is an older version, so we have to take 
		//--- the old code which may cause problems if the filename
		//--- field is too long
		ist.putback(ist.get());	    // skip tab
		ist.getline(tmp,2048);	    // read arguments
		// find first nonwhite name
		char *tmp1=tmp;	
		while ( (isspace(*tmp1)) && !(*tmp1==0) ) {
		    tmp1++;
		}
		timestring.setFileName(tmp1);   // set filename
	    }
	}
	else if (my_strcasecmp(tmp,"InputFormat=")==0) {
	    ist.putback(ist.get());	    // skip tab
	    char fmt[256];
	    ist >> fmt;
	    setInputFormatBackUp(fmt);
	}
	else if (my_strcasecmp(tmp,"SelectAll")==0) {
	    timestring.selectAll();
	}
	else if (my_strcasecmp(tmp,"Select")==0) {
	    timestring.select();
	}
	else if (my_strcasecmp(tmp,"Weight")==0) {
	    int where;
	    double weight;
	    ist>> where;
	    ist>>tmp;
	    ist>>weight;
	    int ID=timestring.getID(where,tmp);
	    timestring.getIDName(where,ID).setWeight(weight);
	}
	else if (my_strcasecmp(tmp,"WeightName")==0) {
	    int what;
	    ist>>what;
	    // read in line
	    timestring.setUseNameWeight(what);
	}
	else if (my_strcasecmp(tmp,"WeightPoint")==0) {
	    int what;
	    ist>>what;
	    // read in line
	    timestring.setUsePointWeight(what);
	}
	else if (my_strcasecmp(tmp,"WeightDeviation")==0) {
	    int what;
	    ist>>what;
	    // read in line
	    timestring.setUseDeviationWeight(what);
	}
	else if (my_strcasecmp(tmp,"WeightDeviationCutoff")==0 ||
		 // due to a spelling misstake in one of the Period98 versions
		 // we have to add the following line:
		 my_strcasecmp(tmp,"WeightDeviationCuttoff")==0) {
	    double what;
	    ist>>what;
	    // read in line
	    timestring.setDeviationCutoff(what);
	}
	else if (my_strcasecmp(tmp,"SelectName")==0) {
	    int where;
	    ist>>where;
	    ist>>tmp;
	    int ID=timestring.getID(where,tmp);
	    timestring.getIDName(where,ID).setSelect(1);
	}      
	else if (my_strcasecmp(tmp,"Attributes")==0) {
	    char a1[1024],a2[1024],a3[1024],a4[1024];
	    ist>>a1>>a2>>a3>>a4;
	    timestring.changeNameSet(0,a1);
	    timestring.changeNameSet(1,a2);
	    timestring.changeNameSet(2,a3);
	    timestring.changeNameSet(3,a4);
	}      
	else if (my_strcasecmp(tmp,"Color")==0) {
	  int where;
	  ist>> where;
	  ist>>tmp;
	  char quote;
	  ist>>quote;
	  char color[256];
	  ist.get(color,256,'"');
	  ist>>quote;
	  int ID=timestring.getID(where,tmp);
	  timestring.getIDName(where,ID).setColor(color);
	}
	else if (my_strcasecmp(tmp,"DeleteLabel")==0) {
	  // set defaults
	  int what;
	  char name[256];
	  ist>>what>>name;
	  setDeletePointInfo(what,name);
	}
	else if (my_strcasecmp(tmp,"Magnitude")==0) {
	  int what;
	  ist>>what;
	  // read in line
	  setReverseScale(what);
	}
	else if (my_strcasecmp(tmp,"Data")==0) {
	  // read in line
	  timestring.readIn(ist,fileFormatVersion);
	}
	else {
	    char txt[1024]; 
	    sprintf(txt,"Unknown Project-data in Timestring-part:\n%s",tmp);
	    showMessage(txt);
	}
    }
    // now try Garbage Collection
    timestring.garbageCollect();
}

void CProject::writeTimestring(ostream &ost) {
    ost << "Filename=\t"    << timestring.getFileName() << "\nend.Filename\n";
    ost << "InputFormat=\t" << getInputFormatBackUp()   << endl;
    // writeout the selected names and their weights if not default
    for (int i=0;i<4;i++) {
	int ent=numberOfNames(i);
	for (int j=0;j<ent;j++) {
	    if (getIndexName(i,j).getSelect()) {
		ost<<"SelectName\t"<<i<<"\t"
		   <<getIndexName(i,j).getName()<<endl;
	    }
	    if (getIndexName(i,j).getWeight()!=1.0) {
		ost<<"Weight\t"<<i<<"\t"
		   <<getIndexName(i,j).getName()<<"\t"
		   <<getIndexName(i,j).getWeight()<<endl;
	    }
	    {
		ost<<"Color\t"<<i<<"\t"
		   <<getIndexName(i,j).getName()<<"\t"
		   <<'"'<<getIndexName(i,j).getColor()<<'"'<<endl;
	    }
	}
    }
    // delete default label
    {
      // set defaults
      int what;
      std::string name;
      getDeletePointInfo( what, name);
      // and write it out
      ost << "DeleteLabel\t" << what << "\t" << name << std::endl;
    }

ost<<"Attributes"
       <<"\t"<<nameSet(0)
       <<"\t"<<nameSet(1)
       <<"\t"<<nameSet(2)
       <<"\t"<<nameSet(3)<<endl;
    ost<<"Magnitude\t"<<getReverseScale()<<endl;
    ost<<"WeightName\t"<<getUseNameWeight()<<endl;
    ost<<"WeightPoint\t"<<getUsePointWeight()<<endl;
    ost<<"WeightDeviation\t"<<getUseDeviationWeight()<<endl;
    ost<<"WeightDeviationCutoff\t"<<getDeviationCutoff()<<endl;
    // write out the data
    timestring.writeAll(ost,(char*)"Data");
    // and select all needed
    ost<<"Select"<<endl;
}

void CProject::setUseNameWeight(int i) {
    timestring.setUseNameWeight(i);
    select();
}

void CProject::setUsePointWeight(int i) {
    timestring.setUsePointWeight(i);
    select();
}

void CProject::setUseDeviationWeight(int i) {
    timestring.setUseDeviationWeight(i);
    select();
}

void CProject::setDeviationCutoff ( std::string const & c)
{
    double cut = 0;
    if (sscanf( c.c_str( ), "%lf", &cut) != 1) {return;}
    timestring.setDeviationCutoff(cut);
    //Select();
}

std::string CProject::getStartTime ( )
{
  if (getSelectedPoints( ))
  {
    char txt [32];
    sprintf( txt, FORMAT_TIME, timestring[0].getTime( ));
    return txt;
  }

  return "";
}

std::string CProject::getEndTime ( )
{
  if (getSelectedPoints( ))
  {
    char txt [32];
    sprintf( txt, FORMAT_TIME, timestring[getSelectedPoints( ) - 1].getTime( ));
    return txt;
  }

  return "";
}

double CProject::getTotalTimeLength() {
    if (getSelectedPoints()) {
	return timestring[getSelectedPoints()-1].getTime() -
	    timestring[0].getTime();
    }
    return 0;
}

std::string CProject::getIDNameWeight(int i, int ID) {
  char txt [32];
  sprintf( txt, FORMAT_WEIGHTS, getIDName( i, ID).getWeight( ));
  return txt;
} 

char const * CProject::getIDNameColor ( int i, int ID)
{
  return getIDName( i, ID).getColor( ); 
}


void CProject::deleteSelectedPoints ( )
{
  timestring.deleteSelection( );
  select( );
}

double CProject::getFrequencyResolution ( )
{
  return 1.5 / (timestring[getSelectedPoints( ) - 1].getTime( ) - timestring[0].getTime( ));
}
