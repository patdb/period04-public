/*--------------------------------------------------------------------------
 *  CName.cpp
 *            contains: class CName members
 *-------------------------------------------------------------------------*/

#include <ctype.h>         // isspace
#include <cmath>          // sqrt

#include <iostream>
using std::cout;
using std::endl;

#include "message.h"
#include "CColors.h" 
#include "CName.h"
#include "os.h"


CName::CName
  ( int ID
  , std::string const & name
  )
  : mID( ID)
  , mColor( 0)
  , mWeight( 1)
  , mSelect( 0)
  , mSelectCopy( 0)
  , mOrig( 0)
  , mAdjusted( 0)
  , mOrigSigma( 0)
  , mAdjustedSigma( 0)
  , mPoints( 0)
  , mOrigWeight( 0)
  , mAdjustedWeight( 0)
  , mOrigSigmaWeight( 0)
  , mAdjustedSigmaWeight( 0)
  , mWeightSum( 0)
  , mFrequencies( 0)
  , mAmplitude( NULL)
  , mPhase( NULL)
{
  mColor = ID%MAXCOLORS;
  resetValues( );
  setName( name);
}

CName::CName
  ( CName const & other
  )
  : mID( other.mID)
  , mColor( other.mColor)
  , mWeight( other.mWeight)
  , mSelect( 0)
  , mSelectCopy( 0)
  , mOrig( other.mOrig)
  , mAdjusted( other.mAdjusted)
  , mOrigSigma( other.mOrigSigma)
  , mAdjustedSigma( other.mAdjustedSigma)
  , mPoints( other.mPoints)
  , mOrigWeight( other.mOrigWeight)
  , mAdjustedWeight( other.mAdjustedWeight)
  , mOrigSigmaWeight( other.mOrigSigmaWeight)
  , mAdjustedSigmaWeight( other.mAdjustedSigmaWeight)
  , mWeightSum( other.mWeightSum)
  , mAmplitude( other.mAmplitude)
  , mPhase( other.mPhase)
{
  setName( other.mName);
}

CName::~CName ( )
{
}

void CName::resetValues ( )
{
  // unweighted data
  mOrig = 0;
  mOrigSigma = 0;
  mAdjusted = 0;
  mAdjustedSigma = 0;
  mPoints = 0;
  // weighted data
  mOrigWeight = 0;
  mOrigSigmaWeight = 0;
  mAdjustedWeight = 0;
  mAdjustedSigmaWeight = 0;
  mWeightSum = 0;
  // pointers
  mAmplitude = NULL;
  mPhase = NULL;
}

void CName::setName ( std::string const & name)
{
  if ("" == name)
  {
    mName = "unknown";
  }
  else
  {
    mName = "";

    for ( size_t i = 0; i < name.length( ); ++i)
    {
      char c = name[ i];

      if (isspace( c))
      {   // replaces whitespaces with underscores
        mName += '_'; 
      }
      else
      {
        mName += c;
      }
    }
  }
}

int operator < ( CName const & one, CName const & other)
{
  return (one.mName < other.mName);
}

int operator <= ( CName const & one, CName const & other)
{
  return (one.mName <= other.mName);
}

char const * CName::getColor ( )
{
  return Colors[mColor];
}

char const * CName::getTrueColor ( )
{
  return TrueColors[mColor];
}

void CName::setColor ( std::string const & color)
{
  for ( int i = 0; i < MAXCOLORS; ++i)
  {
    if (my_strcasecmp( Colors[i], color.c_str( )) == 0)
    {
      mColor=i;
      return;
    }
  }
}

// now the unweighted data

double CName::getAverageOrig ( ) const
{
  return (mPoints > 0) ? (mOrig / (double) mPoints) : 0;
}

double CName::getAverageAdj ( ) const
{ 
  return (mPoints > 0) ? (mAdjusted / (double) mPoints) : 0;
}

double CName::getSigmaOrig ( ) const
{
  return (mPoints > 0) ? std::sqrt( mOrigSigma / (double) mPoints) : 0;
}

double CName::getSigmaAdj ( ) const
{
  return (mPoints > 0) ? std::sqrt( mAdjustedSigma / (double) mPoints) : 0; 
}

// now the weighted data

double CName::getAverageOrigWeight ( ) const
{ 
  return (mWeightSum > 0.0) ? (mOrigWeight / mWeightSum) : 0; 
}

double CName::getAverageAdjWeight ( ) const
{ 
  return (mWeightSum > 0.0) ? (mAdjustedWeight / mWeightSum) : 0;
}

double CName::getSigmaOrigWeight ( ) const
{
  return (mWeightSum > 0.0) ? std::sqrt( mOrigSigmaWeight / mWeightSum) : 0; 
}

double CName::getSigmaAdjWeight ( ) const
{
  return (mWeightSum > 0.0) ? std::sqrt( mAdjustedSigmaWeight / mWeightSum) : 0; 
}

// now the statistical storage-function

void CName::addPoint ( double orig, double adj, double weight)
{
  // unweighted data
  mOrig += orig;
  mOrigSigma += orig * orig;
  mAdjusted += adj;
  mAdjustedSigma += adj * adj;
  mPoints += 1;
  // weighted data
  mOrigWeight += weight * orig;
  mOrigSigmaWeight += weight * orig * orig;
  mAdjustedWeight += weight * adj;
  mAdjustedSigmaWeight += weight * adj * adj;
  mWeightSum += weight;
}


