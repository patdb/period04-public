
#include "CColors.h"

#define N_COL0  "Red"
#define N_COL1  "Green"
#define N_COL2  "Blue"
#define N_COL3  "Cyan"
#define N_COL4  "Yellow"
#define N_COL5  "Violet"
#define N_COL6  "Dark green"
#define N_COL7  "Grey"
#define N_COL8  "Orange"
#define N_COL9  "Navy"
#define N_COL10 "Pink"
#define N_COL11 "White"
#define N_COL12 "Orchid"
#define N_COL13 "Maroon"
#define N_COL14 "Plum"
#define N_COL15 "Brown"

// do not change below this point !!!

#define T_COL0  "RED"
#define T_COL1  "GREEN"
#define T_COL2  "BLUE"
#define T_COL3  "CYAN"
#define T_COL4  "YELLOW"
#define T_COL5  "VIOLET"
#define T_COL6  "DARK GREEN"
#define T_COL7  "GREY"
#define T_COL8  "ORANGE"
#define T_COL9  "NAVY"
#define T_COL10 "PINK"
#define T_COL11 "WHITE"
#define T_COL12 "ORCHID"
#define T_COL13 "MAROON"
#define T_COL14 "PLUM"
#define T_COL15 "BROWN"

char const * const Colors [] =
  { (char *) N_COL0
  , (char *) N_COL1
  , (char *) N_COL2
  , (char *) N_COL3
  , (char *) N_COL4
  , (char *) N_COL5
  , (char *) N_COL6
  , (char *) N_COL7
  , (char *) N_COL8
  , (char *) N_COL9
  , (char *) N_COL10
  , (char *) N_COL11
  , (char *) N_COL12
  , (char *) N_COL13
  , (char *) N_COL14
  , (char *) N_COL15
  }
;

char const * const TrueColors [] =
  { (char *) T_COL0
  , (char *) T_COL1
  , (char *) T_COL2
  , (char *) T_COL3
  , (char *) T_COL4
  , (char *) T_COL5
  , (char *) T_COL6
  , (char *) T_COL7
  , (char *) T_COL8
  , (char *) T_COL9
  , (char *) T_COL10
  , (char *) T_COL11
  , (char *) T_COL12
  , (char *) T_COL13
  , (char *) T_COL14
  , (char *) T_COL15
  }
;
