/*--------------------------------------------------------------------------
 *  CNames.cpp
 *            contains: class CNames members
 *-------------------------------------------------------------------------*/

#include <iostream>
using std::cout;
using std::endl;

#include "message.h"
#include "CColors.h" 
#include "CNames.h"
#include "os.h"

//
//
//

CNames::CNames ( )
{
  cleanUp( );
}


//
//
//

CNames::~CNames ( )
{
  cleanUp( );
}

//
//
//

int CNames::getID
  ( std::string name
  )
{
  if ("" == name)
  {
    name = "unknown";
  }

  // check if "history"-value
  if ((mLastObject != NULL) && (mLastObject->getName() == name))
  {
    return mLastObject->getID( );
  }

  // OK, a different value, search in list
  for
    ( iterator_type iter = m_list.begin( )
    ; iter != m_list.end( )
    ; ++iter
    )
  {
    if (iter->getName( ) == name)
    {
      mLastObject = &(*iter);       // assign new history-values
      return mLastObject->getID( );    // return the result
    }
  }

  // not found, let's add it
  ++mIndex;
  m_list.push_back( CName( mIndex, name));
  return mIndex;
}

//
//
//

CName & CNames::getIDName
  ( int id
  )
{
  // check if "history"-value
  if ((mLastObject != NULL) && (id == mLastObject->getID( )))
  {
    return (*mLastObject);
  }

  // OK, a different value, search in list
  for
    ( iterator_type iter = m_list.begin( )
    ; iter != m_list.end( )
    ; ++iter
    )
  {
    if (iter->getID( ) == id)
    {
      mLastObject = &(*iter);   // assign new history-values
      return (*iter);           // return the result
    }
  }

  // index not found
  mLastObject = NULL;
  MYERROREXIT( "index " << id << " was not found... quitting...");
  static CName result( 0, "");
  return result;
}

//
//
//

CName & CNames::getIndexName ( int i)
{
  if ((i < 0) || (i >= m_list.size( )))
  {
    MYERROREXIT( "index " << i << " is out of bounds...");
  }

  iterator_type iter = m_list.begin( );

  for
    ( int n = 0
    ; (iter != m_list.end( )) && (n < i)
    ; ++iter, ++n
    )
  {
  }

  return (*iter);
}

void CNames::cleanUp ( )
{
  m_list.clear( );      // remove entries from list
  mIndex = -1;         // set index-counter back to 0
  mLastObject = NULL;  // empty "history"-value
  (void) getID( "");   // add entry 0 - unknown
}

//
//
//

void CNames::garbageCollect ( )
{
  for
    ( iterator_type iter = m_list.begin( )
    ; iter != m_list.end( )
    ; 
    )
  {
    if (iter->getPoints( ) == 0)
    {
      iter = m_list.erase( iter);
    }
    else
    {
      ++iter;
    }
  }

  return;
}

//
//
//

void CNames::storeSelection ( )
{
  for
    ( iterator_type iter = m_list.begin( )
    ; iter != m_list.end( )
    ; ++iter
    )
  {
    iter->storeSelection( );
  }
}

//
//
//

void CNames::restoreSelection ( )
{
  for
    ( iterator_type iter = m_list.begin( )
    ; iter != m_list.end( )
    ; ++iter
    )
  {
    iter->restoreSelection( );
  }
}

//
//
//

int CNames::getActiveNames ( ) const
{
  int active=0;

  for
    ( const_iterator_type iter = m_list.begin( )
    ; iter != m_list.end( )
    ; ++iter
    )
  {
    if (iter->getPoints( ) != 0)
    {
      ++active;
    }
  }

  return active;
}

//
//
//

int CNames::getHighestActiveIDName ( ) const
{
  int activeID = 0;

  for
    ( const_iterator_type iter = m_list.begin( )
    ; iter != m_list.end( )
    ; ++iter
    )
  {
    // names have to be active
    if (iter->getPoints( ) != 0)
    {
      int ID = iter->getID( );

      //the ID must be the highest
      if (ID > activeID)
      {
        activeID = ID;
      }
    }
  }

  return activeID;
}

//
//
//
