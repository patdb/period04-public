
// look for further members in CLeastSquaresRoutine.cpp

#include <stdio.h>             // sscanf
#include <ctype.h>             // isspace

#include <fstream>
using std::ifstream;
using std::ofstream;

#include <iostream>   // nur fuer testzwecke
using std::cout;
using std::endl;

#include <sstream>
using std::ostringstream;

#include "CPeriod.h"
#include "message.h"
#include "Period04_java.h" // setProgressbarValue 

#include <string.h>

#define DOUBLEOUT "%8g"
#define INTOUT    "%5i"

CPeriod::CPeriod(int size)
  :mFitMode(SPECIAL),mBaseMatrix(0,0),mWorkMatrix(0,0),
   mVariables(0),mIterations(0),mFlamda(0.001),mFlamda_def(0.001),
   mZeropoint(0),mChisqr(0),mChisqr_unreduced(0),mTimeString(0),
   mFrequencies(0),mMaximumFrequencies(size),
   mActive(0),mActiveIndex(0),
   mFileName(""),mUseWeight(0),
   mUseData(Observed),mWhat(0),mBinaryMode(false),mCheckCondNumber(true)
{
    mData=new CPeriPoint[size];
    for (int j=0;j<size;j++) {
	  mData[j].setNumber(j);      // set frequency-values right !!!
    }
    setBMDefaultValues();   // sets default values of the binary mode 
                            // frequency and activates it
}

CPeriod::~CPeriod() {
    delete [] mData; 
    delete [] mActiveIndex;
}

int CPeriod::getActiveFrequencies() const {
    int count=0;
    for (int i=0; i<getFrequencies(); i++) {
	if (mData[i].getActive()) {
	    count++;
	}
    }
    return count;
}

void CPeriod::setActiveIndex() {
    delete [] mActiveIndex;
    mActive=getActiveFrequencies();

    mActiveIndex = new int[mActive];
    int i,j=0;
    for (i=0;i<getFrequencies();i++) {
	if ((*this)[i].getActive()) {
	    mActiveIndex[j]=i;
	    j++;
	}
    }
}

void CPeriod::selectDefault() {
    for(int fr=0;fr<getFrequencies();fr++) {
	mData[fr].unsetSelection(Fre);
	mData[fr].setSelection(Amp);
	mData[fr].setSelection(Pha);
    }
}

void CPeriod::selectBMDefault() {
    for(int fr=0;fr<getFrequencies();fr++) {
	mData[fr].unsetSelection(Fre);
	mData[fr].setSelection(Amp);
	mData[fr].setSelection(Pha);
    }
    if (mBinaryMode) { // for binary mode
	mBMPeriod.setActive(1); // activate the binary mode frequency
	mBMPeriod.setSelection(Fre);
	mBMPeriod.setSelection(Amp);
	mBMPeriod.setSelection(Pha);
    }
}

void CPeriod::selectAll() {
    for(int fr=0;fr<getFrequencies();fr++) {
	mData[fr].setSelection(Fre);
	mData[fr].setSelection(Amp);
	mData[fr].setSelection(Pha);
    }
    if (mBinaryMode) { // for binary mode
	mBMPeriod.setActive(1); // activate the binary mode frequency
	mBMPeriod.setSelection(Fre);
	mBMPeriod.setSelection(Amp);
	mBMPeriod.setSelection(Pha);
    }
}

void CPeriod::selectPTS() {
    for(int fr=0;fr<getFrequencies();fr++) {
	mData[fr].unsetSelection(Fre);
	mData[fr].unsetSelection(Amp);
	mData[fr].unsetSelection(Pha);
    }
    if (mBinaryMode) { // for binary mode
	mBMPeriod.setActive(1); // activate the binary mode frequency
	mBMPeriod.setSelection(Fre);
	mBMPeriod.setSelection(Amp);
	mBMPeriod.setSelection(Pha);
    }
}

void CPeriod::storeSelection() {
    for(int fr=0;fr<getFrequencies();fr++) {
	mData[fr].storeSelection(); 
    }
    if (mBinaryMode) { // for binary mode
	mBMPeriod.storeSelection();
    }
}

void CPeriod::restoreSelection() {
    for(int fr=0;fr<getFrequencies();fr++) { 
	mData[fr].restoreSelection(); 
    }
    if (mBinaryMode) { // for binary mode
	mBMPeriod.restoreSelection();
    }
}

void CPeriod::storeParameters(int store) {
    if (store==1) { // store parameters
	mChisqr_store    = mChisqr;
	mZeropoint_store = mZeropoint;
	mData_store = new CPeriPoint[getFrequencies()];
	for (int i=0; i<getFrequencies(); i++) {
	    mData_store[i] = mData[i];
	}
	mBMPeriod_store = mBMPeriod;
    } else { // store parameters temporarily
	mChisqr_tmp    = mChisqr;
	mZeropoint_tmp = mZeropoint;
	mData_tmp = new CPeriPoint[getFrequencies()];
	for (int i=0; i<getFrequencies(); i++) {
	    mData_tmp[i] = mData[i];
	}
	mBMPeriod_tmp = mBMPeriod;
    }
}

void CPeriod::restoreParameters(int store) {
    if (store==1) { // restore parameters
	mChisqr = mChisqr_store;
	mZeropoint = mZeropoint_store;
	for (int i=0; i<getFrequencies(); i++) {
	    mData[i] = mData_store[i];
	}
	mBMPeriod = mBMPeriod_store;
//      delete [] mData_store;
    } else { // restore parameters
	mChisqr = mChisqr_tmp;
	mZeropoint = mZeropoint_tmp;
	for (int i=0; i<getFrequencies(); i++) {
	    mData[i] = mData_tmp[i];
	}
	mBMPeriod = mBMPeriod_tmp;
    }
}

CPeriPoint &CPeriod::operator[](int i) const {
    if ((i<0) || (i>=mFrequencies)) {
	MYERROREXIT("CPeriod["<<i<<"]: out of range of "
		    "[0,"<<mFrequencies<<")...");
    }
    return mData[i];
}

int CPeriod::findEmpty() const {
    for (int i=0; i<mMaximumFrequencies; i++) {
	if (mData[i].empty()) {
	    return i;
	}
    }
    return mMaximumFrequencies-1;
}

void CPeriod::add() {
    resizeLeast(mFrequencies);
}

void CPeriod::remove() {
    mFrequencies--;
    if (mFrequencies<0) {
	mFrequencies=0;
    } else {
	mData[mFrequencies].cleanComposition();
	// this needs to be DONE !!!
	//*this[Freqs].CleanDepend();
    }
}

void CPeriod::removeOne() {
  mFrequencies--;
  if (mFrequencies<0) {
    mFrequencies=0;
  }	
}

void CPeriod::removeAll() {
  mFrequencies=0;
}

void CPeriod::resizeLeast(int i) {
  i++;
  if (i>mFrequencies) {
	if (i<mMaximumFrequencies) {
	  for (int j=mFrequencies; j<i; j++) {
		// set frequency-values right !!!
		mData[j].setNumber(j);
		mData[j].CSimplePeriPoint::setActive(0);
	  }
	  mFrequencies=i;
	}
  }
}


void CPeriod::recalc() {
    setPointers(-1);    // first update pointers
    for(int i=0;i<mFrequencies;i++) {
	  mData[i].recalcFrequencies();
    }
}

void CPeriod::adapt() {
    for (int i=0;i<mFrequencies;i++) {
	mData[i].adapt();
    }
    if (mBinaryMode) {
	mBMPeriod.adapt();
    }
}

void CPeriod::fixNumbers() {
    for (int i=0;i<mFrequencies;i++) {
	mData[i].fixNumbers();
    }
    if (mBinaryMode) {
	mBMPeriod.fixNumbers();
    }
}

void CPeriod::setPointers(int f) {
  if (f==-1) { 
	for (int i=0; i<mFrequencies; i++) { setPointers(i); } 
  } else {
	// check for f needed
	if ((f<0)||(f>=mFrequencies)) { 
	    MYERROR("Tried to update a bad pointer !!!");
	    exit(-1); 
	}

	int freq;
	for (int i=0; i<mData[f].getCompoTermNumber(); i++) {
	  mData[f].getDepending(i,&freq);
	  if (freq!=-1) {
		// check if freq is good
		if ((freq<0)||(freq>=mFrequencies)) {
		  MYERROR("Bad pointer requested !!!");
		  //exit(-1);
		  ostringstream ostr;
		  ostr << "f"<< freq+1 << " in your combination string\n'f" 
			   << (mData[f].getNumber()+1)
			   << string(mData[f].getCompositeString())
			   <<"' could not be found!" << endl;
		  showMessage(ostr.str().c_str());
		  mData[f].cleanComposition();
		  mData[f].setActive(0);
		  mData[f].setFrequency(0);
		  return;
		}
		mData[f].setDepending(i,freq,&mData[freq]);
	  }
	}
  }
}

void CPeriod::setResiduals ( CTimeString & times)
{
  for ( int i = 0; i < times.getSelectedPoints( ); ++i)
  {
    double const ti = times[i].getTime( );
    int const id = times[i].getIDName( mWhat);
    times[i].setCalculated( predict( ti, id));
  }

  mChisqr = chiSqr( times);     // set ChiSquare
  times.calcWeights( );         // update weights
}

void CPeriod::load ( std::string const & path)
{
    //--- check if just 'carriage return' is used as 
    //--- line separator in this file (old Mac style)
    char linebuffer[256];
    ifstream file(path.c_str());
    if (!file.is_open()) { return; }
    file.getline(linebuffer,255);
    char delim = '\n';
    if (strstr(linebuffer, "\r")!=NULL) {
	delim = '\r';
    } 
    file.close();
    file.open(path.c_str());
    file.clear();
    readPeriods( file, (*this) , delim);
    mFileName = path;
}

bool mUseLatex=false;

void CPeriod::save ( std::string const & path, bool isUseLatex)
{
  mUseLatex = isUseLatex;
    ofstream file(path.c_str());
    file << (*this);
}

std::string CPeriod::getFrequencyStringForPrinting() {
  mUseLatex = false;
  ostringstream buffer(ostringstream::out);
  buffer << (*this);
  return buffer.str();
}

//---
//--- at the moment there are two methods to write out periods:
//---  operator<<()     is used for exporting the periods
//---                        
ostream& operator<<(ostream& s,const CPeriod &t) {
    int oldprec = s.precision(15);
    if (t.getBMMode()) { // binary mode
	s<< "PTSF"<<"\t"
	 << t.getBMFrequency() << "\t"
	 << t.getBMAmplitude(0)<< "\t"
	 << t.getBMPhase(0)    << endl;
    }
    if (mUseLatex) {
      s<<"\\begin{tabular}[ht!]{llll}\n&Frequency&Amplitude&Phase\\\\ \n\\hline"<<endl;
      for(int i=0;i<t.getFrequencies();i++) {
	if ((t[i].getActive()!=0) || (t[i].getFrequency()!=0)) {
	  s<<t[i].writeLatexTableOut().c_str()<<endl; 
	}
      }
      s<<"\\hline \n\\end{tabular}"<<endl;
    } else {
      for(int i=0;i<t.getFrequencies();i++) {
	if ((t[i].getActive()!=0) || (t[i].getFrequency()!=0)) {
	  s<<t[i].writeSpecialOut(false,true).c_str()<<endl; 
	}
      }
    }
    s.precision(oldprec);
    return s;
}

//--- 
//--- writePeriods()   is used for saving the periods in the project file
//--- 
void CPeriod::writePeriods(ostream &ost, CPeriod &t) {
    if (t.getBMMode()) { // binary mode
	ost<< "PTSF"<<"\t"
	   << t.getBMFrequency() << "\t"
	   << t.getBMAmplitude(0)<< "\t"
	   << t.getBMPhase(0)    << endl;
    }
    for(int i=0; i<t.getFrequencies(); i++) {
	if ((t[i].getActive()!=0) || (t[i].getFrequency()!=0)) {
	    ost << t[i] << endl;
	}
    }
    ost << endl;
}

ostream &CPeriod::additionalOut(ostream &s) {
    // store default-precission and set new 
    int prec=s.precision(15);
    // output myself
    s<<*this;
    // output additional data...
    s<<"Zeropoint="<<getZeropoint()<<endl;
    s<<"Residuals="<<getResiduals()<<endl;
    s<<"Iteration="<<getIterations()<<endl;
    // restore old precission
    s.precision(prec);
    // and return
    return s;
}

#include <sstream>
using std::ostringstream;
std::string CPeriod::writeSelected() {
    ostringstream ostr;
    for (int i=0; i<getFrequencies(); i++) {
	if ((*this)[i].getActive()) {
	    ostr << (*this)[i].writeSpecialOut(true,false) << endl;
	}
    }
    return ostr.str( );
}


void CPeriod::readPeriods(istream &s, CPeriod &t, char delim) { // ersatz fuer oben
    int Freq;
    char c=0;
    char linestr[256];
    while ( (s>>c) && (!s.eof())) {        // read in character
   	// check if it is "F"
	switch (c) {
	    case 'f':
	    case 'F': // this is a frequency line, let's interprete it...
	    {
		s.getline(linestr,255,delim);	    // read the line
		int result = sscanf(linestr,"%i",&Freq);
		char *ptr = linestr;	    // find next interresting part
		while ((*ptr!=0) && (!isspace(*ptr))) {ptr++;}
		// check for sanity
		if ( (result!=1) ||(Freq<1) || (*ptr==0) ) {
		    MYERROR("This line-format is not recognized...\nLine:"
			    <<linestr<<"after the F a number is missing "
			    "or lower than zero !!!");
		} else {
		    // setup data before start reading...
		    Freq--;               // make Freq 0-based !!!
		    t.resizeLeast(Freq);  // resize the period array
		    // read rest of line into the corresponding frequency
		    t[Freq].readIn(ptr);
		}
	    } break;
	    case 'p':
	    case 'P': // this is the periodic time shift frequency line
	    {
		s.getline(linestr,255,delim);	    // read the line
		char *ptr = linestr;	    // find next interresting part
		while ((*ptr!=0) && (!isspace(*ptr))) {ptr++;}
		// read rest of line into the corresponding frequency
		mBMPeriod.readIn(ptr);
	    } break;
	    case ';':
	    case '#':
	    case '/':
	    case '%':
		// this is a comment-line, read it and ignore it...
		s.getline(linestr,255,delim);  break;
	    case '[': {
		s.putback('[');
		t.recalc(); 
		return; }
	    default:
		s.getline(linestr,255,delim);
		MYERROR("This line-format is not recognized...\nLine:"<<c<<linestr);
		t.recalc();
	}
    }
    
    t.recalc();
    t.checkActive(); 
}


void CPeriod::createArtificialData
  ( std::string const & outfile, double from, double to
  , double step, double leading, bool append
  )
{
    from -= leading;
    to   += leading;

    // create a stream and set write-flags
    ofstream str;
    if (append) { 
	str.open( outfile.c_str( ), ofstream::out | ofstream::app);
    } else {
	str.open( outfile.c_str( ), ofstream::out | ofstream::trunc);
    }


    // create stream to write the file
    str.precision(15);
    for(double i=from; i<=to; i+=step) {
	str << i << "\t" << predict(i,-1) << endl;
    }
    str.close();
}

void CPeriod::createArtificialData
  ( std::string const & outfile, std::string const & infile, bool isRange
  , double step, double leading, bool append
  )
{
    // create a stream to read the time ranges
    ifstream istr;
    istr.open( infile.c_str( ), ifstream::in);
    if(!istr.is_open()) {
	MYERROR("Opening file failed!");
	return;
    }
    
    // create an stream to write the results and set its write-flags
    ofstream ostr;
    if (append) { 
	ostr.open( outfile.c_str( ), ofstream::out | ofstream::app);
    } else {
	ostr.open( outfile.c_str( ), ofstream::out | ofstream::trunc);
    }

    double from, to;
	while(!istr.eof()) {
	  if (isRange) {
		istr >> from >> to;    // read in the first time range
		from -= leading;
		to   += leading;
	  
		// create stream to write the file
		ostr.precision(15);
		for(double i=from; i<=to; i+=step) {
		  ostr << i << "\t" << predict(i,-1) << endl;
		}
      } else {
		istr >> from;
		ostr.precision(15);
		ostr << from << "\t" << predict(from,-1) << endl;
      }
    }
    istr.close();
    ostr.close(); 
}

