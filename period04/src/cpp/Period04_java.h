#ifndef __Period04_java_h__
#define __Period04_java_h__

bool progress
  ( unsigned long n
  , unsigned long total
  )
;

int setProgressBarValue ( int n);
int setProgressString ( char const * txt);
int isCalculationCanceled ( );
void showMessage ( char const * text);
int confirm ( char const * text);

#endif //  __Period04_java_h__
