/*
 * File CFourierRoutine.cpp
 * 
 * contains
 *           struct fourier_data
 *           methods for fourier transforms
 */

#include <iostream>        

#include <stdlib.h>             // RAND_MAX
//#include <stdio.h>
//#include <string.h>
#include <time.h>               // time()

// for multithreading
#ifndef WIN32
#include <omp.h>           // omp_get_max_threads()
#endif
#include <pthread.h>  

#include "CFourierAlgorithm.h"
#include "CFourier.h"
#include "CTimeString.h"
#include "CProject.h"      // getCurrentProject()
#include "Period04_java.h" // setProgressbarValue (-> CFourierRoutine)

#ifdef WIN32
#define RANDOM rand 
#define SRANDOM srand
#else
#define RANDOM random
#define SRANDOM srandom
#endif

#define PERCENTAGE_START 0              
#define PERCENTAGE_STEP  100


 /*
 * Structure fourier_data 
 * 
 * contains information required by the fourier transform function
 * to access its input data and store its output.
 *
 */
typedef struct
{
  double *tmpt;         // time
  double *tmpa;         // observed
  int npts;             // time points

  FTAlgorithm algorithm;// Fourier algorithm       
  double from;          // lower limit of frequency range to be calculated
  double to;            // upper limit of frequency range to be calculated
  int nfreq;            // frequency points
  double dfreq;         // step size in frequency
  
  double *amp_r;        // real amplitudes
  double *amp_i;        // imaginary amplitudes
  double powersum;      // sum of power
  int powerpoints;      // points used for calculation of powersum

  int progress;         // progress of calculation
  int quit;             // quit variable

} fourier_data;

/*
 * Definition of global variables
 */

fourier_data     ft_data;

int useloops=0;

#ifndef WIN32
int const NUM_THREADS = omp_get_max_threads();
#else 
int const NUM_THREADS = 8;
#endif
pthread_mutex_t  mutex_increment;
pthread_mutex_t  mutex_progress;
pthread_mutex_t  mutex_quit;



void ft_clean_data
( )
{
  //--- delete allocated arrays
  //---
  delete [] ft_data.tmpt;
  delete [] ft_data.tmpa;
  delete [] ft_data.amp_r;
  delete [] ft_data.amp_i;
  ft_data.tmpt = NULL;
  ft_data.tmpa = NULL;
  ft_data.amp_r = NULL;
  ft_data.amp_i = NULL;
  ft_data.npts = 0;
  ft_data.from = 0.0;
  ft_data.to = 0.0;
  ft_data.nfreq = 0;
  ft_data.dfreq = 0.0;
  // don't clean powersum & powerpoints here, will be done later! 
  ft_data.progress = 0;
  ft_data.quit = 0;
}

void ft_evaluate
( CFourier &fourier )
{
  int nfreq = ft_data.nfreq;
  double dfreq = ft_data.dfreq;
  double *freq  = new double[nfreq];
  double *power = new double[nfreq];
  freq[0]=ft_data.from;
  for (int i=0; i<nfreq; ++i) {
    freq[i]=freq[0]+dfreq*double(i);
  }
  
  //--- calculate power
  {
    double *tmpp = power;
    double *tmpr = ft_data.amp_r;
    double *tmpi = ft_data.amp_i;
    
    ft_data.powersum=0.0;
    ft_data.powerpoints=0;
    for ( int j = 0; j < nfreq; ++j)
      {
        (*tmpp) = (*tmpr) * (*tmpr) + (*tmpi) * (*tmpi);
        // calculate sum of power
        ft_data.powersum += sqrt( *tmpp);
	++(ft_data.powerpoints);
        // increment pointers
        ++tmpr; ++tmpi; ++tmpp;
      }
  }
  
  double compact=fourier.getCompact();
  //--- write out data
  {
    int store;
    double po_l = power[ 0];
    double po_t = power[ 0];
    double po_n = power[ 0];
    double fr;
    
    for ( int loc = 0; loc < nfreq; ++loc)
      {
        // should I write this out?
        store = 0;
        // rotate values
        po_l = po_t;
        po_t = po_n;
        fr = freq[ loc];

        if (loc + 1 < nfreq)
        {
          po_n = power[ loc + 1];
        }

        // check if we are compacting
        if (compact)
        {
          if ((po_l <= po_t) && (po_t >= po_n))
          { 
            store = 1;
          }
          else
          {
            if ((po_l >= po_t) && (po_t <= po_n))
            {
              store = 1;
            }
          }
        }
        else 
        {
          store = 1;
        }

        if (store)
        {
          fourier.add( CFourierPoint( fr, po_t));
       }
      }
    }

  delete [] freq;
  delete [] power;
  freq = NULL;
  power = NULL;
  ft_clean_data();
}

void dft_kurtz1985
(int taskid)
{
  double const *tmpt = ft_data.tmpt;
  double const *tmpa = ft_data.tmpa;
  int const     npts = ft_data.npts;
  int const    nfreq = ft_data.nfreq;
  double const dfreq = ft_data.dfreq;
  
  // allocate memory
  double *freq  = new double [ nfreq];
  double *amp_r = new double [ nfreq];
  double *amp_i = new double [ nfreq];
  
  int done_old = 0;
  int quit = 0;
  
  freq[0]=ft_data.from;
  for (int i=0; i<nfreq; ++i) {
    amp_r[i]=0.0;
    amp_i[i]=0.0;
    freq[i]=freq[0]+dfreq*double(i);
  }
  
  //--- determine part of the data to be computed by this thread 
  int const nsect = int(npts/NUM_THREADS);
  int const npts_l = taskid*nsect;
  int const npts_u = ( taskid==(NUM_THREADS-1) ? npts : npts_l+nsect );
  int nloop = 10;
  if (nloop>nsect)
    nloop=nsect;

  int from = npts_l;
  int to = npts_l+nloop;
  clock_t before=::clock();
  do {
    //--- calculate part of the spectra

    for ( int i = from; i < to; ++i)
      {
	double const & ti = tmpt[ i];
	double const & am = tmpa[ i];   
	double const & fre0 = freq[ 0];
	
	//--- prepare trigonometric functions
	//---
	double LastSin, SDF, LastCos, CDF;
	{
	  double const XX = MY2PI * ti;
	  
	  double XF0 = XX * fre0;
	  XF0 = std::fmod( XF0, MY2PI);
	  
	  double XDF = XX * dfreq;
	  XDF = std::fmod( XDF, MY2PI);
	  
	  LastSin = am * std::sin( XF0);
	  LastCos = am * std::cos( XF0);
	  SDF = std::sin( XDF);
	  CDF = std::cos( XDF);
	}
	
	//--- main loop
	//---
	double *ar = amp_r, *ai = amp_i;
	for ( int j = 0; j < nfreq; ++j)
	  {
	    (*ar++) += LastSin;
	    (*ai++) += LastCos;
	    
	    double ThisSin = LastSin * CDF + LastCos * SDF;
	    double ThisCos = LastCos * CDF - LastSin * SDF;
	    LastSin = ThisSin;
	    LastCos = ThisCos;
	  }
      }
    
    // calculate % done
    double done1 = ( to - npts_l ) * PERCENTAGE_STEP / ( npts_u - npts_l );

    if (done1 > PERCENTAGE_STEP) 
      done1 = PERCENTAGE_STEP;

    int done=(int)(done1+PERCENTAGE_START);

    if (done > done_old) {
      // the percentage number has changed, now update the
      // progress bar and set the new percentage value as old value
      pthread_mutex_lock( &mutex_progress);
      ft_data.progress += (done - done_old);
      pthread_mutex_unlock( &mutex_progress);
      pthread_mutex_lock( &mutex_quit);
      // if this returns non zero, cancel has been pressed...
      quit = setProgressBarValue( ft_data.progress / NUM_THREADS );
      ft_data.quit = quit;
      pthread_mutex_unlock( &mutex_quit);
      done_old = done;
    }

    // check how fast the computation is on this computer and
    // adjust the callback times for updating the progress bar
    if (from==npts_l)
      {
	nloop = int(float(::clock()-before)/CLOCKS_PER_SEC*20.0);
	if (nloop<1) nloop=1;
	if (nloop>nsect) nloop=nsect;
	//std::cout << float(::clock()-before)/CLOCKS_PER_SEC  <<" "<<nloop<< std::endl;
      }

    // check if we finished calculation. if not, prepare next part
    if (to==npts_u) 
      {
	quit=1;
      } 
    else
      {
	from = to;
	to = from+nloop;
	if (to>npts_u) 
	  to = npts_u;
      }

  }
  while (quit==0);
 
  pthread_mutex_lock (&mutex_increment);
  for(int j=0; j<nfreq; ++j) {
    ft_data.amp_r[j] += amp_r[j];
    ft_data.amp_i[j] += amp_i[j];
  }
  pthread_mutex_unlock (&mutex_increment);
    
  delete [] freq;
  delete [] amp_r;
  delete [] amp_i;
  
  return;
}

void *ft_thread
( void *threadID ) 
{
  int taskid = (long) threadID;
  //long long sig1 = reinterpret_cast<long long> (threadID);
  //const int id = static_cast<int>(sig1);
  
  switch(ft_data.algorithm) {
  case KURTZ1985:
  default:
    dft_kurtz1985( taskid );
  }

  pthread_exit( (void *) 0);
  return 0;
}


void fourier_transform
( CTimeString const &time, 
  CFourier &fourier, 
  double zero ) 
{
  //--- prepare data for fourier transform
  //---
  int npts = time.getSelectedPoints();
  DataMode mode = fourier.getMode();
  int use_weight= fourier.getUseWeight();
  double from = fourier.getFrom();
  double to   = fourier.getTo();
  if (to<from) {
    double tmp=from;
    from = to;
    to = tmp;
  }
  ft_data.from = from;
  ft_data.to   = to;
  ft_data.dfreq  = fourier.getStepping();
  int nfreq      = int((to-from)/ft_data.dfreq)+2;

  ft_data.tmpt = new double[npts];
  ft_data.tmpa = new double[npts];
  ft_data.npts         = npts;
  ft_data.algorithm    = fourier.getAlgorithm();
  ft_data.nfreq        = nfreq;//wind;
  ft_data.powersum     = 0.0;
  ft_data.powerpoints  = 0;
  ft_data.progress     = 0;
  ft_data.quit         = 0;

  ft_data.amp_r = new double [ nfreq];
  ft_data.amp_i = new double [ nfreq];
  for (int j=0; j<nfreq; ++j) 
    {
      ft_data.amp_r[j] = 0.0;
      ft_data.amp_i[j] = 0.0;
    }


  // should we use weighted data?
  double p2;
  if (use_weight) {
    // use the sum of weight instead
    p2=2.0/time.getWeightSum();
  } else {
    p2=2.0/npts;
  }

  // find out mode of calculations
  //    if (mode==SpectralWindow1 || mode==SpectralWindow2) {
  if (mode==SpectralWindow1) {
    p2/=2.0;   // for scaling purposes...
  }
  double mSW2Freq=0.0;
  if (mode==SpectralWindow2) {
    mSW2Freq = fourier.getSW2Frequency();
  }

  // copy values into arrays
  for (int i=0;i<npts;i++) { 
    double tm,am;
    double weight=(time.point(i,mode,&tm,&am)).getWeight();
    am-=zero;
    if (mode==SpectralWindow1) {
      if (use_weight) {
	am=p2*weight;
      } else {
	am=p2;
      }
    } else {
      if (mode==SpectralWindow2) {
	double tmp;
	double fac = std::sin( MY2PI * std::modf( tm * mSW2Freq, &tmp));  //phase!
	if (use_weight) {
	  am=p2*weight*fac;
	} else {
	  am=p2*fac;
	}
      } else {
	if (use_weight) {
	  am*=p2*weight;
	} else {
	  am*=p2;
	}
      }
    }
    // finally assign values
    ft_data.tmpt[i]=tm;
    ft_data.tmpa[i]=am;
  }

  //---
  //--- initialize variables for threads
  //---
  pthread_t threads[NUM_THREADS];

  //--- set attributes for joinable threads
  pthread_attr_t attr;
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

  //--- Initialize mutex  objects
  pthread_mutex_init(&mutex_increment, NULL);
  pthread_mutex_init(&mutex_progress,  NULL);
  pthread_mutex_init(&mutex_quit,      NULL);

  int rc, t;
  void *status;

  //--- create threads
  //---
  for(t=0; t<NUM_THREADS; t++) {
    rc = pthread_create(&threads[t], &attr, ft_thread, (void *) t); 
    if (rc)	{
      std::ostringstream ostr;
      ostr<< "Problem creating threads! Return code from pthread_create(): "
	  << rc << std::endl;
      MYERROREXIT(ostr.str());
    }
  }
  
  //--- Join threads
  //---
  pthread_attr_destroy(&attr);
  for(t=0;t<NUM_THREADS;t++) {
    rc = pthread_join(threads[t], &status);
    if (rc) {
      std::ostringstream ostr;
      ostr<< "Problem joining threads! Return code from pthread_join(): "<< rc << std::endl;
      MYERROREXIT(ostr.str());
    }
  }

  ft_evaluate(fourier);

  //--- make access more efficient
  fourier.makeStatic();
  
  //--- Clean up and exit
  pthread_mutex_destroy(&mutex_increment);
  pthread_mutex_destroy(&mutex_progress);
  pthread_mutex_destroy(&mutex_quit);
}


double noiseCalc
( CTimeString const &time, 
  double zero,
  double from, 
  double to, 
  double step,
  int useWeight, 
  DataMode mode
)
{
  CFourier *tmp = new CFourier( "noise", KURTZ1985, from, to, HIGH,step, mode, ALL_DATA, useWeight);
  fourier_transform(time, (*tmp), zero);
  double result = ft_data.powersum/double(ft_data.powerpoints);
  ft_data.powersum=0.0;
  ft_data.powerpoints=0;
  delete tmp;
  return result;
 }

void calcOptimumFourierLoops
( int points,
  int min,
  int max,
  int step) 
{
    CTimeString time;              // create a Timestring
    SRANDOM(::time(NULL));         // put in current time as randome seed
    for(int i=0;i<points;i++) {    // fill in values
	double ti,am;
	ti=1.0*RANDOM()/RAND_MAX;
	am=1.0*RANDOM()/RAND_MAX;
	CTimePoint t(ti,am);
	time.addData(t);
    }
    time.selectAll();              // and select all points

    // now loop
    for (useloops=min;
	 useloops<=max;
	 useloops+=step) {
	clock_t before,after;
	CFourier tmp("temp");	   // create a fourier Object
	tmp.setFrom(0);
	tmp.setTo(500);
	tmp.setUseWeight(1);
	before=::clock();           // now do timed calculations
	fourier_transform(time,tmp,0);
	after=::clock();
	// calculate time differences and output stuff
	getCurrentProject()->getTimeString().addData(
	    CTimePoint(useloops,after-before));
    }
    getCurrentProject()->getTimeString().selectAll();
}
