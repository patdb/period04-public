/*--------------------------------------------------------------------------
 *  CNames.h
 *            declares: class CNames
 *-------------------------------------------------------------------------*/

#ifndef __names_h__
#define __names_h__

#include "CName.h"
#include <string>
#include <list>

//---
//---  class CNames
//---  represents a collection of CName items
//---
class CNames
{
 private:
    int    mIndex;            // the list index
    CName* mLastObject;       // for speedup-purpose on search

    typedef std::list< CName> list_type;
    typedef list_type::iterator iterator_type;
    typedef list_type::const_iterator const_iterator_type;
    list_type m_list;

 public:

    CNames ( );                      // constructor
    ~CNames ( );                     // destructor

    int getID ( std::string name);   // returns the ID for the given name,
                                   // creating a new entry if necessary
    CName & getIDName ( int id);     // return the name fitting the ID
    CName & getIndexName ( int i);   // returns entry number i in the CName-list

    int getActiveNames ( ) const; // get number of list entries with
                                    // mPoints > 0 (for amplitude variations)
    int getHighestActiveIDName ( ) const ;   // get the highest name-ID
                                               // from the list

    void computeMeanTimes ( );

    int  setColor ( int c);

    int  getNumberOfNames ( ) const
    { // returns the number of names in the list
      return m_list.size( );
    }

    void addAmpVarData ( int Freqs); // allocates data for amplitude variations
    void removeAmpVarData ( );       // removes the allocated data

    void storeSelection ( );
    void restoreSelection ( );

    void cleanUp ( );                // to remove all name-labels
    void garbageCollect ( );         // to remove unnecessary names
};

#endif

