
//#include <stdio.h>

#include <string>
using std::string;

#include <sstream>
using std::ostringstream;

#include <iostream>
using namespace std;

#include "CProject.h"
#include "format.h"
#include "message.h"
#include "os.h"

#define ALMOST_ZERO 1.0E-200

//--- message for the progress bar
#define LS_MESSAGE "%i Iterations of possible %i done..."

//--- compare calculated errors to analytical approximation
//--- and show output on console?
#define SHOW_ERRORS false


//--- cleanPeriod(ID)
//--- cleans specific period data of the active mode
//---
void CProject::cleanPeriod(int id) {
  CPeriPoint empty;   // create an empty period and ...
  empty.setActive(0); // deactivate this period
  
  CPeriod *peri = &getActivePeriod();
  (*peri)[id]=empty;   // now clean
  peri->removeOne();
}

//---
//--- cleanPeriod()
//--- cleans the period data in all modes
//---
void CProject::cleanPeriod() {
    CPeriPoint empty;   // create an empty period and ...
    empty.setActive(0); // deactivate this period

    //--- now clean
    CPeriod *peri = &getActivePeriod();
    for (int i=0; i<peri->getFrequencies(); i++) {
	(*peri)[i]=empty;
    }
    peri->setZeropoint(0);
    peri->setResiduals(0);
    peri->removeAll();
    if (fitmode==1) // clean additional data in the pts mode
	peri->cleanBMPeriod();

    //--- reset errors
    fitactive=0;       // no fit is active
    setFitError();     // set default fit_protocol text
}

//---
//--- cleanAllPeriods()
//--- cleans the period data of the active mode
//---
void CProject::cleanAllPeriods() {
    CPeriPoint empty;   // create an empty period and ...
    empty.setActive(0); // deactivate this period
    int i;
    //--- clean normal mode
    for (i=0; i<period.getFrequencies(); i++) {
	period[i]=empty;
    }
    period.setZeropoint(0);
    period.setResiduals(0);
    period.removeAll();

    //--- clean binary mode
    for (i=0; i<periodBM.getFrequencies(); i++) {
	periodBM[i]=empty;
    }
    periodBM.setZeropoint(0);
    periodBM.setResiduals(0);
    periodBM.cleanBMPeriod();
    periodBM.removeAll();

    fitactive=0;       // no fit is active
    setFitError();     // set default fit_protocol text
}

std::string CProject::getNumber(int i) const {
    char tmp[32];
    if (fitmode==0) { // normal mode
	sprintf(tmp, FORMAT_NUMBER, period[i].getNumber()+1);
    } else if (fitmode==1) { // binary mode
	sprintf(tmp, FORMAT_NUMBER, periodBM[i].getNumber()+1);
    }
    return tmp;
}


std::string CProject::getBMNumber(int i) const { 
    char tmp[32];
    sprintf(tmp, FORMAT_NUMBER, periodBM[i].getNumber()+1);
    return tmp;
}

std::string CProject::getFrequency(int i) const {
    char tmp[32];
    sprintf(tmp,FORMAT_FREQUENCY,period[i].getFrequency());
    return tmp;
}

std::string CProject::getBMFrequency(int i) const {
    char tmp[32];
    if (i==-1) { // get bm frequency 
	sprintf(tmp,FORMAT_FREQUENCY,periodBM.getBMFrequency());
    } else {
	sprintf(tmp,FORMAT_FREQUENCY,periodBM[i].getFrequency());
    }
    return tmp;
}

//--- only for phase plots therefore no pts:
//--- search for the first frequency that is not active and not empty
double
CProject::getFirstFrequency() {
    for (int i=0; i<period.getFrequencies(); i++) {
	if (!period[i].getActive()) {
	    if (!period[i].empty()) {
		return period[i].getFrequency();
	    }
	}
    }
    return 1;
}

//--- only for amp/phase var therefore no pts:
//--- search for the first frequency that is active
int
CProject::getFirstFrequencyNumber() {
    for (int i=0; i<period.getFrequencies(); i++) {
	if (period[i].getActive())
	    return (period[i].getNumber()+1);
    }
    return -1;
}

std::string CProject::setFrequency ( int fr, std::string const & f)
{
    ostringstream ostr;
    char txt[1024];

    int i=f.length();
    char *tmp=(char*)f.c_str( );
    int isCompo=0;
    for (int j=0; j<i; j++) {
	if (f[j]=='=') { isCompo=1; break;}
    }
    int count=0;
    for (i=i-1;i>=0;i--) {
	if (tmp[i]=='+')      { count++; tmp[i]=' '; }
	else if (tmp[i]=='-') { count--; tmp[i]=' '; }
	else if (tmp[i]!=' ') { break; }
    }
    
    if (isCompo) {
	period.setCompositeString(fr,f.c_str( ));
	return "";
    }
    if (count==0) {
	double fre;
	if (sscanf(f.c_str(),"%lf",&fre)) {
	    period.setFrequency(fr,fre);   // set unchanged
	}
    } else {
	double freqold;
	if (sscanf(f.c_str(),"%lf",&freqold)) {
	    double freqnew=freqold+count*freqstep;
	    sprintf(txt,"\nChanged frequency %i by %i steps of %g\n"\
		    "from "FORMAT_FREQUENCY" to "FORMAT_FREQUENCY"\n",
		    fr+1,count,freqstep,freqold,freqnew);
	    ostr << txt;
	    // now set it...
	    period.setFrequency(fr,freqnew);
	}
    }

  return ostr.str( );
}

std::string CProject::setBMFrequency ( int fr, std::string const & f)
{
    char txt [1024];
    if (fr==-1) {  // set the bm frequency
	double fre;
	if (sscanf(f.c_str(),"%lf",&fre)) {
	    // set unchanged
	    periodBM.setBMFrequency(fre);
	}
	return "";
    }

    int i=f.length();
    char *tmp=(char*)f.c_str();
    int isCompo=0;
    for (int j=0; j<i; j++) {
	if (f[j]=='=') { isCompo=1; break;}
    }
    int count=0;
    for (i=i-1;i>=0;i--) {
	if (tmp[i]=='+')      { count++; tmp[i]=' '; }
	else if (tmp[i]=='-') { count--; tmp[i]=' '; }
	else if (tmp[i]!=' ') { break; }
    }
    
    if (isCompo) {
	periodBM.setCompositeString(fr,f.c_str());
	return "";
    }
    if (count==0) {
	double fre;
	if (sscanf(f.c_str(),"%lf",&fre)) {
	    periodBM.setFrequency(fr,fre);  // set unchanged
	}
    } else {
	double freqold;
	if (sscanf(f.c_str(),"%lf",&freqold)) {
	    double freqnew=freqold+count*freqstep;
	    sprintf(txt,"\nChanged frequency %i by %i steps of %g\n"\
		    "from "FORMAT_FREQUENCY" to "FORMAT_FREQUENCY"\n",
		    fr+1,count,freqstep,freqold,freqnew);
	    // now set it...
	    periodBM.setFrequency(fr,freqnew);
	}
    }

  return txt;
}

std::string CProject::getAmplitude(int i, int what) const {
    char tmp[32];
    sprintf(tmp,FORMAT_AMPLITUDE,period[i].getAmplitude(what));
    return tmp;
}

std::string CProject::getBMAmplitude(int i, int what) const {
    char tmp[32];
    if (i==-1) {  // get amplitude of bm frequency
	sprintf(tmp,FORMAT_AMPLITUDE,periodBM.getBMAmplitude(what));
    } else {
	sprintf(tmp,FORMAT_AMPLITUDE,periodBM[i].getAmplitude(what));
    }
    return tmp;
}

double CProject::getAmplitudeValue(int i, int what) const {
    return period[i].getAmplitude(what);
}

void CProject::setAmplitude(int i, std::string const & a) {
    double tmp;
    if (sscanf(a.c_str(),"%lf",&tmp)) {
	// if scanned ok, let's set it
	period[i].setAmplitude(tmp,-1);
    }
}

double CProject::getBMAmplitudeValue(int i, int what) const {
  return periodBM[i].getAmplitude(what);
}

void CProject::setBMAmplitude(int i, std::string const & a) {
    double tmp;
    if (sscanf(a.c_str(),"%lf",&tmp)) {
	// if scanned ok, let's set it
	if (i==-1) {  // set amplitude of bm frequency
	    periodBM.setBMAmplitude(tmp,-1);
	} else {
	    periodBM[i].setAmplitude(tmp,-1);
	}
    }
}

std::string CProject::getPhase(int i,int what) const {
    char tmp[32];
    sprintf(tmp,FORMAT_PHASE,period[i].getPhase(what));
    return tmp;
}

std::string CProject::getBMPhase(int i,int what) const {
    char tmp[32];
    if (i==-1) {  // get phase of bm frequency
	sprintf(tmp,FORMAT_PHASE,periodBM.getBMPhase(what));
    } else {
	sprintf(tmp,FORMAT_PHASE,periodBM[i].getPhase(what));
    }
    return tmp;
}

void CProject::setPhase(int i, std::string const & p) {
    double tmp;
    if (sscanf(p.c_str(),"%lf",&tmp)) {
	// if scanned ok, let's set it
	period[i].setPhase(tmp,-1);
    }
}

void CProject::setBMPhase(int i, std::string const & p) {
    double tmp;
    if (sscanf(p.c_str(),"%lf",&tmp)) {
	// if scanned ok, let's set it
	if (i==-1) {  // set phase of bm frequency
	    periodBM.setBMPhase(tmp,-1);
	} else {
	    periodBM[i].setPhase(tmp,-1);
	}
    }
}

bool CProject::getCompoUseFreqValue(int i) const {
  return period[i].getCompoUseFreqValue();
}

void CProject::setCompoUseFreqValue(int i, bool val) {
  period[i].setCompoUseFreqValue(val);
}

void CProject::setCompoUseFreqValues(bool val) {
  for (int i=0; i<period.getFrequencies(); i++) {
	period[i].setCompoUseFreqValue(val);
  }
}

bool CProject::getBMCompoUseFreqValue(int i) const {
  return periodBM[i].getCompoUseFreqValue();
}

void CProject::setBMCompoUseFreqValue(int i, bool val) {
  periodBM[i].setCompoUseFreqValue(val);
}

void CProject::setBMCompoUseFreqValues(bool val) {
  for (int i=0; i<periodBM.getFrequencies(); i++) {
	periodBM[i].setCompoUseFreqValue(val);
  }
}

std::string CProject::getResiduals() const {
    char tmp[32];
    sprintf(tmp,FORMAT_RESIDUALS,period.getResiduals());
    return tmp;
}

std::string CProject::getBMResiduals() const {
    char tmp[32];
    sprintf(tmp,FORMAT_RESIDUALS,periodBM.getResiduals());
    return tmp;
}

std::string CProject::getZeropoint() const {
    char tmp[32];
    sprintf(tmp,FORMAT_ZEROPOINT,period.getZeropoint());
    return tmp;
}

std::string CProject::getBMZeropoint() const {
    char tmp[32];
    sprintf(tmp,FORMAT_ZEROPOINT,periodBM.getZeropoint());
    return tmp;
}

std::string CProject::getZeropointError ( ) const
{
  char tmp [32];
  double sigma = period.getZeropointSigma( );
  
  if (0.0 == sigma)
  {
    sprintf( tmp, "fixed"); 
  }
  else
  {
    sprintf( tmp, FORMAT_ZEROPOINT, sqrt( sigma));
  }
  
  return tmp;
}


std::string const & CProject::getFitError ( ) const
{
    return fit_protocol;
}

void CProject::setFitError() {
    ostringstream ostr;
    ostr.precision(10);
    ostr << showpoint <<
	" ---------------------------------------------------------\n" << 
	" --          Results of the Least-Squares-Fit:          --\n" << 
	" ---------------------------------------------------------\n" <<endl; 

    if (fitactive==0 ||            // there's no calculated fit at the moment
	(fitactive==1 && fitmode==1) ||     // there's a fit of another mode
	(fitactive==2 && fitmode==0)) { 
	fit_protocol = ostr.str( );
	return;
    }
    if (fitactive==-1) {
	ostr << 
	    "\n    The correlation matrix has been left\n"\
	    "    ill-conditioned by the least-squares routine.\n"\
	    "\n    Please perform a Monte Carlo Simulation\n"\
	    "    for a reliable determination of uncertainties!" << endl;
	fit_protocol = ostr.str( );
	return;
    }

    //--- define the period object we are working on
    CPeriod *peri = &getActivePeriod();
    int attribute = period.getUseID();

    //--- check whether lambda increased
    //cout<<"fit lambda ratio: "<<peri->getLambdaRatio()<<endl;
    //if (peri->getLambdaRatio()>0.1) {
    if (peri->getLambdaRatio()>10) {
	ostr << "\n    Cannot calculate uncertainties from the\n"
	     << "    matrix left by the least-squares routine.\n"
	     << "\n    Please perform a Monte Carlo Simulation\n"
	     << "    for a reliable determination of uncertainties!" << endl;
	if (fitmode==0) {
	    ostr << "\n\n    Instead, analytical uncertainties will be displayed:\n"
		 << "    (Please consult Period04 help for more information.)"
		 << "\n\n" << getAnalyticalErrors() << endl;
	}
	fit_protocol = ostr.str( );
	return;
    }

    //--- write protocol header
    ostr << " Chi Square:           " << peri->getChiSqrUnred() << "\n" 
	 << " Reduced Chi Square:   " << peri->getChiSqr()      << "\n"
	 << " Residuals (Chi):      " << peri->getResiduals()   << "\n\n"
	 << " Zeropoint:\t"<<peri->getZeropoint()<<"\n Uncertainty:\t" 
	 << peri->getZeropointSigma()*peri->getResiduals()<<"\n\n"
	 << " Nr.\tFreq. sigma\tAmpl. sigma\tPha. sigma\n" 
	 << " ---------------------------------------------------------"
	 << endl;

    //---
    //--- if the periodic time shift mode has been used write out the errors
    //--- for the PTS parameters
    //---
    if (fitactive==2) { 
	if (periodBM.getBMActive()) {
	    //--- write out pts frequency sigma
	    if (periodBM.getBMFrequencySigma()>=ALMOST_ZERO) {
		ostr<<" PTSF\t"<<periodBM.getResiduals()*
		    periodBM.getBMFrequencySigma();
	    } else { ostr<<" PTSF\tfixed\t"; }
	    //--- write out pts amplitude sigma
	    if (periodBM.getBMAmplitudeSigma()>=ALMOST_ZERO) { 
		ostr<<"\t"<<periodBM.getResiduals()*
		    periodBM.getBMAmplitudeSigma();
	    } else { ostr<<"\tfixed\t"; }
	    //--- write out pts phase sigma
	    if (periodBM.getBMPhaseSigma()>=ALMOST_ZERO) { 
		ostr<<"\t"<<periodBM.getResiduals()*
		    periodBM.getBMPhaseSigma()<<endl;
	    } else { ostr<<"\tfixed"<<endl; }
	} else {
	    ostr << 
		" PTSF  The periodic time shift parameters have not been\n"\
		"       included in this calculation.\n"\
		"       To fit a periodic time shift (PTS) please set initial\n"\
		"       values for frequency and amplitude of the PTS.\n" 
		 << endl;
	}
    }
    //---
    //--- now write out the errors for the all frequencies
    //--- 
    for (int i=0; i<peri->getFrequencies(); i++) {
	if ((*peri)[i].getActive()) {              // is the frequency active
	    switch ((*peri)[i].getAmpVariation()) {// get the type of variation
		case AmpVar: {
		    ostr<<" F"<<(*peri)[i].getNumber()+1; 
		    //--- write out frequency sigma
		    if ((*peri)[i].getFrequencySigma()>=ALMOST_ZERO) {
			ostr<<"\t"<<(*peri)[i].getFrequencySigma()*
			    peri->getResiduals();
		    } else { ostr<<"\tfixed\t"; }
		    //--- write out phase sigma
		    if ((*peri)[i].getPhaseSigma(-1)>=ALMOST_ZERO) {
			ostr<<"\t\t\t"<<(*peri)[i].getPhaseSigma(-1)*
			    peri->getResiduals()<<endl;
		    } else { ostr<<"\t\t\tfixed"<<endl; }
		    //--- write out amplitude variation sigmas
		    ostr<<"           Name:\tAmpl.sigma:"<<endl;

        
        int const names = timestring.numberOfNames( attribute);

        for ( int na = 0; na < names; ++na)
        {
          CName const & cname = timestring.getIndexName( attribute, na);

          if (0 != cname.getPoints( ))
          {
            ostr
              << "           " << cname.getName( )
              << "\t" << (*peri)[i].getAmplitudeSigma( cname.getID( )) * peri->getResiduals( )
              << std::endl
            ;
          }
        }



		    break;
		}
		case PhaseVar: {
		    ostr<<" F"<<(*peri)[i].getNumber()+1; 
		    //--- write out frequency sigma
		    if ((*peri)[i].getFrequencySigma()>=ALMOST_ZERO) {
			ostr<<"\t"<<(*peri)[i].getFrequencySigma()*
			    peri->getResiduals();
		    } else { ostr<<"\tfixed\t"; }
		    //--- write out amplitude sigma
		    if ((*peri)[i].getAmplitudeSigma(-1)>=ALMOST_ZERO) {
			ostr<<"\t"<<(*peri)[i].getAmplitudeSigma(-1)*
			    peri->getResiduals()<<endl;
		    } else { ostr<<"\tfixed"<<endl; }
		    //--- write phase variation sigmas 
		    ostr<<"           Name:\t\t\tPha. sigma:"<<endl;

        
        int const names = timestring.numberOfNames( attribute);

        for ( int na = 0; na < names; ++na)
        {
          CName const & cname = timestring.getIndexName( attribute, na);

          if (0 != cname.getPoints( ))
          {
            ostr
              << "           " << cname.getName( )
              << "\t\t\t" << (*peri)[i].getPhaseSigma( cname.getID( )) * peri->getResiduals( )
              << std::endl
            ;
          }
        }


		    break;
		}
		case AllVar: {
		    ostr<<" F"<<(*peri)[i].getNumber()+1; 
		    //--- write out frequency sigma
		    if ((*peri)[i].getFrequencySigma()>=ALMOST_ZERO) {
			ostr<<"\t"<<(*peri)[i].getFrequencySigma()*
			    peri->getResiduals()<<endl;
		    } else { ostr<<"\tfixed"<<endl; }
		    //--- write ampl./phase variation data
		    ostr<<"           Name:\tAmpl. sigma:\tPha. sigma:"<<endl;

        
        int const names = timestring.numberOfNames( attribute);

        for ( int na = 0; na < names; ++na)
        {
          CName const & cname = timestring.getIndexName( attribute, na);

          if (0 != cname.getPoints( ))
          {
            int const id = cname.getID( );

            ostr
              << "           " << cname.getName( )
              << "\t" << (*peri)[i].getAmplitudeSigma( id) * peri->getResiduals( )
              << "\t" << (*peri)[i].getPhaseSigma( id) * peri->getResiduals( )
              << std::endl
            ;
          }
        }



		    break;
		}
		case NoVar: 
		default: {
		    ostr<<" F"<<(*peri)[i].getNumber()+1; 
		    //--- write out frequency sigma
		    if ((*peri)[i].getFrequencySigma()>=ALMOST_ZERO) {
			ostr<<"\t"<<(*peri)[i].getFrequencySigma()*
			    peri->getResiduals();
		    } else { ostr<<"\tfixed\t"; }
		    //--- write out amplitude sigma
		    if ((*peri)[i].getAmplitudeSigma(-1)>=ALMOST_ZERO) {
			ostr<<"\t"<<(*peri)[i].getAmplitudeSigma(-1)*
			    peri->getResiduals();
		    } else { ostr<<"\tfixed\t"; }
		    //--- write out phase sigma
		    if ((*peri)[i].getPhaseSigma(-1)>=ALMOST_ZERO) {
			ostr<<"\t"<<(*peri)[i].getPhaseSigma(-1)*
			    peri->getResiduals()<<endl;
		    } else { ostr<<"\tfixed"<<endl; }
		}
	    }
	    //---
	    //--- compare the results to the analytic formula and show 
	    //--- it on the console
	    //---
	    if (SHOW_ERRORS) {
		double frsig = 
		    sqrt(6.0/getSelectedPoints())*peri->getResiduals()/
		    (MYPI*(timestring[getSelectedPoints()-1].getTime()-
			   timestring[0].getTime())*(*peri)[i].getAmplitude(-1));
		double amsig = sqrt(2.0/getSelectedPoints())*peri->getResiduals();
		double phsig = 
		    sqrt(2.0/getSelectedPoints())*peri->getResiduals()/
		    (*peri)[i].getAmplitude(-1)/MY2PI;
		double p04_frsig = 
		    peri->getResiduals()*(*peri)[i].getFrequencySigma();
		double p04_amsig = 
		    peri->getResiduals()*(*peri)[i].getAmplitudeSigma(-1);
		double p04_phsig = 
		    peri->getResiduals()*(*peri)[i].getPhaseSigma(-1);
		cout << "Formel: FreqSigma = " << frsig << "\tProg: "
		     << p04_frsig << "\tVerh: " << frsig/p04_frsig << endl;
		cout << "        AmpSigma  = " << amsig << "\t      " 
		     << p04_amsig << "\t      " << amsig/p04_amsig << endl;
		cout << "        PhaSigma  = " << phsig << "\t      " 
		     << p04_phsig << "\t      " << phsig/p04_phsig << endl;
	    }
	}
    }
    fit_protocol = ostr.str( );
}

std::string CProject::getAnalyticalErrors ( ) const
{
    if (fitmode!=0) { return ""; }
    ostringstream ostr;
    ostr.precision(10);
    //--- write protocol header
    ostr << showpoint <<
	" ---------------------------------------------------------\n" << 
	" --       Analytical results for an ideal case:         --\n" << 
	" ---------------------------------------------------------\n" <<endl;

    //--- check whether the residuals are zero
    if (period.getResiduals()<=1.0e-200) {
	ostr << "\n Please make a fit first!" << endl;
	return ostr.str();
    }
 
    //--- write header
    ostr << " Nr.\tFreq. sigma\tAmpl. sigma\tPha. sigma\n" 
	 << " ---------------------------------------------------------"
	 << endl;

    for (int i=0; i<period.getFrequencies(); i++) {
	if (period[i].getActive()) {         // is the frequency active
	    //--- calculate the values
	    double frsig = 
		sqrt(6.0/getSelectedPoints())*period.getResiduals()/
		(MYPI*(timestring[getSelectedPoints()-1].getTime()-
		       timestring[0].getTime())*period[i].getAmplitude(-1));
	    double amsig = 
		sqrt(2.0/getSelectedPoints())*period.getResiduals();
	    double phsig = 
		sqrt(2.0/getSelectedPoints())*period.getResiduals()/
		period[i].getAmplitude(-1)/MY2PI;
	    //--- write it out
	    ostr<<" F"<<period[i].getNumber()+1
		<<"\t"<<frsig<<"\t"<<amsig<<"\t"<<phsig<<endl;
	}
    }		
    return ostr.str();
}

void CProject::saveFitError ( std::string const & path)
{
  if (path=="") { return; }    // no filename given?

  if (! canWrite( path))
  {     // check if we can write a file
    std::ostringstream text;
    text << "Cannot write to file" << path;
    showMessage( text.str( ).c_str( ));
    return;
  }

  ofstream ostr( path.c_str( ));
  ostr << fit_protocol << endl;
  ostr.close( );
}

void CProject::setFrequencyAdjustment(double step) {
    freqstep=step;
}

std::string CProject::getFrequencyAdjustment() const {
  char txt[32];
  sprintf(txt,FORMAT_FREQUENCY,freqstep);
  return txt;
}

std::string CProject::getActiveFrequenciesString() const {
  char txt[32];
  sprintf(txt,FORMAT_ACTIVE_FREQUENCIES,period.getActiveFrequencies());
  return txt;
}

std::string CProject::getBMActiveFrequenciesString() const {
  char txt[32];
  sprintf(txt,FORMAT_ACTIVE_FREQUENCIES,periodBM.getActiveFrequencies());
  return txt;
}

void CProject::loadPeriod ( std::string const & path, int fitmode)
{
  if (path=="") { return; } // no filename given?

  //--- test if file exists
  if (! fileExist( path))
  {
    std::ostringstream text;
    text << "File " << path << " does not exist";
    showMessage( text.str( ).c_str( ));
    return;
  }  

//--- now load the data
  if (fitmode==1) { periodBM.load( path); } // periodic time shift mode
  else            { period.load( path); }   // normal mode
}

void CProject::savePeriod( std::string const & path, int fitmode, bool isUseLatex) {
  if (path=="") { return; }    // no filename given?

  if (! canWrite( path))
  {     // check if we can write a file
    std::ostringstream text;
    text << "Cannot write to file " << path;
    showMessage( text.str( ).c_str( ));
    return;
  }
  
  if (fitmode==1) { periodBM.save(path, isUseLatex); } // periodic time shift mode
  else            { period.save(path, isUseLatex); }   // normal mode
}

std::string CProject::getFrequenciesForPrinting ( )
{
  if (1 == fitmode)
  { // periodic time shift mode
    return periodBM.getFrequencyStringForPrinting( ); 
  }
  else
  {          // normal mode
    return period.getFrequencyStringForPrinting( );
  }
}

void CProject::copyPeriods(bool toPTS) {
    if (toPTS) { // standard -> pts
	if (period.getFrequencies()==0) { return; } // there is nothing to copy
	//---
	//--- first clean the periodic time shift frequencies
	//---
	int i;
	CPeriPoint empty;
	empty.setActive(0);
	for (i=0; i<periodBM.getFrequencies(); i++) {
	    periodBM[i]=empty;
	}
	periodBM.setZeropoint(0);
	periodBM.setResiduals(0);
	periodBM.cleanBMPeriod();
	periodBM.removeAll();
	//---
	//--- now copy the frequencies
	//---
	int freqs = period.getFrequencies();
	periodBM.resizeLeast(freqs);
	for (i=0; i<freqs; i++) {
	    if (!period[i].empty()) {
		periodBM[i].setFrequency(period[i].getFrequency());
		periodBM[i].setAmplitude(period[i].getAmplitude(-1),-1);
		periodBM[i].setPhase    (period[i].getPhase(-1)    ,-1);
		periodBM[i].setActive   (period[i].getActive());
	    }
	}
    } else { // pts -> standard
	if (periodBM.getFrequencies()==0) { return; } // there is nothing to copy
	//---
	//--- first clean the standard mode frequencies
	//---
	int i;
	CPeriPoint empty;
	empty.setActive(0);
	for (i=0; i<period.getFrequencies(); i++) {
	    period[i]=empty;
	}
	period.setZeropoint(0);
	period.setResiduals(0);
	period.removeAll();
	//---
	//--- now copy the frequencies
	//---
	int freqs = periodBM.getFrequencies();
	period.resizeLeast(freqs);
	for (i=0; i<freqs; i++) {
	    if (!periodBM[i].empty()) {
		period[i].setFrequency(periodBM[i].getFrequency());
		period[i].setAmplitude(periodBM[i].getAmplitude(-1),-1);
		period[i].setPhase    (periodBM[i].getPhase(-1)    ,-1);
		period[i].setActive   (periodBM[i].getActive());
	    }
	}
     }
}

std::string CProject::predict(double time) {
    char tmp[32];
    double pre;
    if (fitmode==0) { // normal mode
	pre = period.predict(time,-1);
    } else { // binary mode
	pre = periodBM.predict(time,-1);
    }
    sprintf(tmp,FORMAT_AMPLITUDE,pre);
    return tmp;
}

std::string CProject::showEpoch(double time, int mode) {
    // empty result
    std::string result;
    int intensity = getReverseScale();
    // fill result
    char tmp[256];
    if (fitmode==0) { // normal mode
	for (int i=0; i<getTotalFrequencies(); i++) {
	    if (period[i].getActive()) {
		// write out line
		sprintf(tmp,"%5s %17s "FIXEDFORMAT_EPOCH"\n",
			getNumber(i).c_str(),
			getFrequency(i).c_str(),
			period[i].calcEpoch(time, mode, intensity)
		    );
		result+=tmp;
	    }
	}
    } else { // binary mode
	for (int i=0; i<getBMTotalFrequencies(); i++) {
	    if (periodBM[i].getActive()) {
		// write out line
		sprintf(tmp,"%5s %17s "FIXEDFORMAT_EPOCH"\n",
			getBMNumber(i).c_str(),
			getBMFrequency(i).c_str(),
			periodBM[i].calcBMEpoch(
			    time, mode, intensity, periodBM.getBMPeriod())
		    );
		result+=tmp;
	    }
	}
    }
    return result;
}

void CProject::refit(double zeropoint) {
    if (fitmode==0) { // NORMAL MODE
	// set the zeropoint
	period.setZeropoint(zeropoint);
	// recalculate points
	period.setResiduals(timestring);
    } else if (fitmode==1) {
	// set the zeropoint
	periodBM.setZeropoint(zeropoint);
	// recalculate points
	periodBM.setResiduals(timestring);
    }
}

void CProject::createArtificialData
  ( std::string const & outfile, double from, double to
  , double step, double leading, bool append
  )
{
    if (fitmode==0) {
	period.createArtificialData(outfile, from, to, step, leading, append);
    } else if (fitmode==1) {
	periodBM.createArtificialData(outfile, from, to, step, leading,append);
    }
}

void CProject::createArtificialData
  ( std::string const & outfile, std::string const & infile, bool isRange
  , double step, double leading, bool append
  )
{
    if (fitmode==0) {
      period.createArtificialData(outfile, infile, isRange, step, leading, append);
    } else if (fitmode==1) {
      periodBM.createArtificialData(outfile, infile, isRange, step, leading, append);
    }
}

std::string CProject::writeFrequenciesTabulated
  ( bool selected
  , bool heading
  )
{
  char text [256];
  std::ostringstream ostr;

	if (heading) {
	  ostr << "Nr.         Frequency        Amplitude      Phase" << endl;
	}
    CPeriod *peri;
    if (fitmode==1) { // periodic time shift mode
	peri=&getBMPeriod(); // use the appropriate period instance
	// write data for the PTSM frequency
	double fre = (*peri).getBMFrequency();
	double amp = (*peri).getBMAmplitude(0);
	double pha = (*peri).getBMPhase(0);
	char fresel=' ',ampsel=' ',phasel=' ';
	// get labels
	if (selected && (*peri).getBMPeriod().isSelected(Fre)) { fresel='*'; }
	if (selected && (*peri).getBMPeriod().isSelected(Amp)) { ampsel='*'; }
	if (selected && (*peri).getBMPeriod().isSelected(Pha)) { phasel='*'; }
	sprintf(text, "PTSF %c"FIXEDFORMAT_FREQUENCY"%c " \
		" %c"FIXEDFORMAT_AMPLITUDE"%c " \
		" %c"FIXEDFORMAT_PHASE    "%c ", 
		fresel,fre,fresel, ampsel,amp,ampsel, phasel,pha,phasel);
	ostr << text << endl;
    } else { // normal mode
	peri=&getPeriod();   // use the appropriate period instance
    }
    //
    // now write the frequency list
    //
    for (int i=0; i<(*peri).getFrequencies(); i++) {
	if ((*peri)[i].getActive()) {
	    double fre, amp, pha;
	    char fresel=' ',ampsel=' ',phasel=' ';
	    int num;
	    num = (*peri)[i].getNumber()+1; 
	    // get values
	    fre = (*peri)[i].getFrequency();
	    amp = (*peri)[i].getAmplitude(0);
	    pha = (*peri)[i].getPhase(0);
	   
	    // get labels
	    if (selected && (*peri)[i].isSelected(Fre)) { fresel='*'; }
	    if (selected && (*peri)[i].isSelected(Amp)) { ampsel='*'; }
	    if (selected && (*peri)[i].isSelected(Pha)) { phasel='*'; }
	    // is composition
	    if ((*peri)[i].isComposition()) { 
		std::string compo=(*peri)[i].getCompositeString(); 
		sprintf(text, FIXEDFORMAT_NUMBER\
			" %c"FIXEDFORMAT_COMPO    "%c " \
			" %c"FIXEDFORMAT_AMPLITUDE"%c " \
			" %c"FIXEDFORMAT_PHASE    "%c " \
			FIXEDFORMAT_FREQUENCY, num, 
			fresel, compo.c_str(), fresel,
			ampsel, amp,           ampsel,
			phasel, pha,           phasel,
			fre);
	    } else {
		sprintf(text, FIXEDFORMAT_NUMBER\
			" %c"FIXEDFORMAT_FREQUENCY"%c " \
			" %c"FIXEDFORMAT_AMPLITUDE"%c " \
			" %c"FIXEDFORMAT_PHASE    "%c ", num,
			fresel, fre, fresel,
			ampsel, amp, ampsel,
			phasel, pha, phasel);
	    }
	    ostr << text << endl;
	}
    }

  return ostr.str( );
}

std::string CProject::writeErrorsTabulated ( bool selected)
{
    char text[256];
    ostringstream ostr;
    CPeriod *peri = &getActivePeriod();
    ostr << "Nr.       Freq. sigma      Ampl. sigma     Pha. sigma" << endl;
    
    for (int i=0; i<(*peri).getFrequencies(); i++) {
	if ((*peri)[i].getActive()) {
	    double fre, amp, pha;
	    char fresel=' ',ampsel=' ',phasel=' ';
	    int num;
	    num = (*peri)[i].getNumber()+1; 
	    // get values
	    fre = (*peri)[i].getFrequencySigma() * (*peri).getResiduals();
	    amp = (*peri)[i].getAmplitudeSigma(0)* (*peri).getResiduals();
	    pha = (*peri)[i].getPhaseSigma(0)    * (*peri).getResiduals();
	    
	    // get labels
	    if (selected && (*peri)[i].isSelected(Fre)) { fresel='*'; }
	    if (selected && (*peri)[i].isSelected(Amp)) { ampsel='*'; }
	    if (selected && (*peri)[i].isSelected(Pha)) { phasel='*'; }
	    // is composition
	    if ((*peri)[i].isComposition()) { 
		std::string compo=(*peri)[i].getCompositeString(); 
		sprintf(text, FIXEDFORMAT_NUMBER\
			" %c"FIXEDFORMAT_COMPO    "%c " \
			" %c"FIXEDFORMAT_AMPLITUDE"%c " \
			" %c"FIXEDFORMAT_PHASE    "%c " \
			FIXEDFORMAT_FREQUENCY, num,
			fresel, compo.c_str(), fresel,
			ampsel, amp,           ampsel,
			phasel, pha,           phasel,
			fre);
	    } else {
		sprintf(text, FIXEDFORMAT_NUMBER\
			" %c"FIXEDFORMAT_FREQUENCY"%c " \
			" %c"FIXEDFORMAT_AMPLITUDE"%c " \
			" %c"FIXEDFORMAT_PHASE    "%c ", num,
			fresel, fre, fresel,
			ampsel, amp, ampsel,
			phasel, pha, phasel);
	    }
	    ostr << text << endl;
	}
    }

  return ostr.str( );
}

void CProject::writePeriod(ostream &ost) {
    int oldprec=ost.precision(15);
    ost<<"Zeropoint=\t"<< period.getZeropoint()   <<"\n";
    ost<<"Residuals=\t"<< period.getResiduals()   <<"\n";
    ost<<"Aliasgap=\t" << getFrequencyAdjustment()<<"\n";
    ost<<"DataMode=\t" << period.getUseData()     <<"\n";
    period.writePeriods(ost, period);
    ost.precision(oldprec);
}

void CProject::writeBMPeriod(ostream &ost) {
    int oldprec=ost.precision(15);
    ost<<"ActivePanel=\t"<< getFitMode() << "\n";
    ost<<"Zeropoint=\t"<< periodBM.getZeropoint() <<"\n";
    ost<<"Residuals=\t"<< periodBM.getResiduals() <<"\n";
    ost<<"Aliasgap=\t" << getFrequencyAdjustment()<<"\n";
    ost<<"DataMode=\t" << periodBM.getUseData()   <<"\n";
    periodBM.writePeriods(ost, periodBM);
    ost.precision(oldprec);
}

void CProject::readPeriod ( istream & ist)
{
  int itmp;
  double dtmp;
  char c, tmp [ 256];

  period.setZeropoint( 0);
  period.setResiduals( 0);

  while (ist>>c, ist.putback( c), c != '[')
  {

    
	if ((c=='F')||(c=='f')) {
	    // load periods
	    period.readPeriods(ist, period);
	} else {
	  // read in argument
	  ist>>tmp;
	  if (my_strcasecmp(tmp,"Zeropoint=")==0) {
	      ist>>dtmp;
	      period.setZeropoint(dtmp);
	  }
	  else if (my_strcasecmp(tmp,"Residuals=")==0) {
	      ist>>dtmp;
	      period.setResiduals(dtmp);
	  }
	  else if (my_strcasecmp(tmp,"Aliasgap=")==0) {
	      char adjust[256];
	      ist>>adjust;
	      setFrequencyAdjustment(atof(adjust));
	  }
	  else if (my_strcasecmp(tmp,"DataMode=")==0) {
	      ist>>itmp;
	      period.setUseData((DataMode)itmp);
	  }
	  else {
	      char txt[1024]; 
	      sprintf(txt,"Unknown Project-data in Period-part:\n%s",tmp);
	      showMessage(txt);
	  }
      }  
  }
}

void CProject::readBMPeriod ( istream & ist)
{
  double dtmp;
  int itmp;
  char c, tmp [ 256];
  
  periodBM.setZeropoint( 0);
  periodBM.setResiduals( 0);
  
  while (ist>>c, ist.putback(c),c!='[')
  {



	if ((c=='F')||(c=='f')||(c=='P')||(c=='p')) {
	    // load periods
	    periodBM.readPeriods(ist, periodBM);
	} else {
	  // read in argument
	  ist>>tmp;
	  if (my_strcasecmp(tmp,"ActivePanel=")==0) {
	      ist>>itmp;
	      setFitMode(itmp);
	  } else if (my_strcasecmp(tmp,"Zeropoint=")==0) {
	      ist>>dtmp;
	      periodBM.setZeropoint(dtmp);
	  }
	  else if (my_strcasecmp(tmp,"Residuals=")==0) {
	      ist>>dtmp;
	      periodBM.setResiduals(dtmp);
	  }
	  else if (my_strcasecmp(tmp,"Aliasgap=")==0) {
	      char adjust[256];
	      ist>>adjust;
	      setFrequencyAdjustment(atof(adjust));
	  }
	  else if (my_strcasecmp(tmp,"DataMode=")==0) {
	      ist>>itmp;
	      periodBM.setUseData((DataMode)itmp);
	  }
	  else {
	      char txt[1024]; 
	      sprintf(txt,"Unknown Project-data in PTSPeriod-part:\n%s",tmp);
	      showMessage(txt);
	  }
      }  
  }
}

std::string CProject::generalCalcPeriod ( DataMode mode)
{
    if (!((mode==Observed)||(mode==Adjusted))) {
	MYERROR("Wrong calculation-mode..." << (int)mode);
	return "";
    }
    char txt[256];
    ostringstream pro;
    bool noErrors=false;
    bool illCond =false;

    //--- define the period object we are working on
    CPeriod *peri = &getActivePeriod();

    //--- define settings
    peri->setProgressMessage((char*)LS_MESSAGE);
    DataMode old = timestring.getDataMode();
    timestring.setDataMode(mode);

    //--- calculate the "average time" (needed for calculation of errors)
    timestring.setTimeAverage(timestring.getTimeAverage());

    //--- check if weights should be used
    peri->setUseData(mode);
    if (!peri->getUseWeight()) {
	if (timestring.getSelectedPoints()!=timestring.getWeightSum()) {
	    if (confirm((char*)"Shall I use the given weights\n"\
			"for the calculation?")==1) {
		peri->setUseWeight(1);
	    }
	}
    }
 
    //--- write calculation-header
    if (isCalcErrors())
	pro << "Calculation of uncertainties:\nCalculation based on:     ";
    else
	pro << "Period calculation:\nCalculation based on:     ";
    if (peri->getUseWeight()) { pro << "weighted "; }
    if (mode==Observed)       { pro << "original data" << endl; }
    else                      { pro << "adjusted data" << endl; }
    if (isShiftTimes())
	pro << "Time string has been shifted in time" << endl;
    pro << "Calculation started on:   " << getDate() << endl;
	
    //--- calculate
    switch(peri->calc(timestring,false)) {
	case 0: // normal calculations done
	{
	    pro << "Calculation finished on:  " << getDate() << endl;
	    if (isCalcErrors()) { pro << writeErrorsTabulated(true); }
	    else                { pro << writeFrequenciesTabulated(true); }
	    sprintf(txt,"Zeropoint:  "FORMAT_AMPLITUDE"\nResiduals:  " \
		    FORMAT_AMPLITUDE"\nIterations: %i",
		    peri->getZeropoint(),
		    peri->getResiduals(),
		    peri->getIterations());
	    pro << txt << endl;
	    if (peri->getIterations()>=peri->maxIterations()) {
		showMessage("The maximum number of iterations\n"\
			    "has been reached during calculations.\n"\
			    "The result may not be reliable.\n"\
			    "Please retry the calculations.");
	    }
	    break;
	}
	case 1: // matrix cannot be inverted
	{
	    pro<<"Stopped Calculation:   Matrix cannot be inverted"<<endl;
	    showMessage("Problem calculating\n"\
			"matrix cannot be inverted.");
	    noErrors=true;
	    break;
	}
	case 2: // cancel pressed, may not be a stable solution
	{
	    showMessage("Calculation has been interrupted\n"\
			"The calculated values may not be reliable.");
	    pro << "Calculation canceled!" << endl;
	    break;
	}
	case 3: // mFlamda reached zero
	{
	    pro << "Calculation stopped on:   " << getDate() << endl;
	    if (isCalcErrors()) { pro << writeErrorsTabulated(true); }
	    else                { pro << writeFrequenciesTabulated(true); }
	    sprintf(txt,"Zeropoint:  "FORMAT_AMPLITUDE"\nResiduals:  " \
		    FORMAT_AMPLITUDE"\nIterations: %i",
		    peri->getZeropoint(),
		    peri->getResiduals(),
		    peri->getIterations());
	    pro << txt << endl;
	    showMessage("Calculation has been interrupted\n"\
			"The value mFlamda got too small.\n"\
			"The calculated values may not be reliable.");
	    break;
	}
	case 4: // matrix is ill-conditioned, don't trust errors
	{
	    pro << "Calculation finished on:  " << getDate() << endl;
	    if (isCalcErrors()) { pro << writeErrorsTabulated(true); }
	    else                { pro << writeFrequenciesTabulated(true); }
	    sprintf(txt,"Zeropoint:  "FORMAT_AMPLITUDE"\nResiduals:  " \
		    FORMAT_AMPLITUDE"\nIterations: %i",
		    peri->getZeropoint(),
		    peri->getResiduals(),
		    peri->getIterations());
	    pro << txt << endl;
	    if (peri->getIterations()>=peri->maxIterations()) {
		showMessage("The maximum number of iterations\n"\
			    "has been reached during calculations.\n"\
			    "The result may not be reliable.\n"\
			    "Please retry the calculations.");
	    }
	    noErrors=true; 
	    illCond =true;
	    break;
	}
    }
    timestring.setDataMode(old);
    //--- define some settings for the results
    if (noErrors) {
	if (illCond) { fitactive=-1; } // show a message
	else         { fitactive= 0; } // don't show errors
    } else {
	if (fitmode==0)      { fitactive=1; } // NORMAL MODE
	else if (fitmode==1) { fitactive=2; } // BINARY MODE
    }
    //--- calculate the errors and write them to a string
    setFitError(); 

  return pro.str( );
}


std::string CProject::calculatePeriod(DataMode mode) {
    //--- get the period object we are working on
    CPeriod *peri = &getActivePeriod();
    //--- set zeropoint
    peri->setZeropoint(timestring.average(peri->getUseWeight()));
    //--- store the selection
    peri->storeSelection(); 
    //--- select all amplitudes and phases
    if (fitmode==0) { peri->selectDefault(); }
    else            { peri->selectBMDefault(); }
    std::string protocol;
    //--- start calculation (monte carlo or standard calculation)
    if (mode==Simulated) { protocol = generalMonteCarloSimulation(); }
    else                 { protocol = generalCalcPeriod(mode); }
    //--- and restore selection 
    peri->restoreSelection(); 
    return protocol;
}

std::string CProject::improvePeriod(DataMode mode, bool isMonteCarlo) {
    //--- get the period object we are working on
    CPeriod *peri = &getActivePeriod();
    bool restore=false;
    if (mode!=Simulated) {     // ok, this is NOT based on simulated data
	if (isShiftTimes()) {  // save old parameters (phases will be changed!)
	    timestring.storeCalculated();
	    peri->storeParameters(1);
	    if (!isMonteCarlo) // we don't need the calculated parameters for
		restore=true;  // the montecarlo sim. so restore the old
	}                      // "unshifted" parameters
	calculatePeriod(mode); // make a calculate first
    }
    peri->storeSelection();    // store the selection
    peri->selectAll();         // select all frequencies, amplitudes and phases
    std::string protocol;
    //--- start calculation (monte carlo or standard calculation)
    if (mode==Simulated) { protocol = generalMonteCarloSimulation(); }
    else                 { protocol = generalCalcPeriod(mode); }
    //--- and restore selection 
    peri->restoreSelection(); 
    //--- in case of an error- (least-squares-) calculation with shifted times
    //--- we have to restore the old parameters
    if (restore) {
	peri->restoreParameters(1);// restore old parameters (old phases)
	timestring.restoreCalculated();
    }
    return protocol;
}

std::string CProject::improvePTS(DataMode mode) {
    periodBM.storeSelection();// store the selection
    periodBM.selectPTS();     // select pts frequencies, amplitudes and phases
    std::string protocol;
    if (mode==Simulated) { protocol = generalMonteCarloSimulation(); }
    else                 { protocol = generalCalcPeriod(mode); }
    periodBM.restoreSelection(); // and restore selection
    return protocol;
}

std::string CProject::improveSpecialPeriod(DataMode mode) {
    std::string protocol;
    //--- get the period object we are working on
    CPeriod *peri = &getActivePeriod();
    if (isShiftTimes()) {
	//--- calculate the "average time"
	timestring.setTimeAverage(timestring.getTimeAverage());
	timestring.storeCalculated(); // store the fit
	peri->storeParameters(1);    // store the old parameters
	peri->resetLambda();         // set lambda to initial value
	improvePeriod(mode, true);   // make a fit
	cleanSigmas();               // clean the errors
	peri->resetLambda();         // set lambda to initial value
    }
    if (mode==Simulated) { protocol = generalMonteCarloSimulation(); }
    else                 { protocol = generalCalcPeriod(mode); }
    if (isShiftTimes()) {                        // restore old parameters ...
	peri->restoreParameters(1);             // ... after MC simulation
	timestring.restoreCalculated();
    }
    return protocol;
}

std::string CProject::calculateAmpVar(
    DataMode datamode, CalcMode calcmode,
    int attribute, int* freqs, int selected, bool isMonteCarlo) 
{
    //--- if we work on a shifted data set first do an improve using the
    //--- shifted time values to get the phases right.
    if (isShiftTimes()) {
	//--- calculate the "average time"
	timestring.setTimeAverage(timestring.getTimeAverage());
	timestring.storeCalculated(); // store the fit
	period.storeParameters(1);    // store the old parameters
	period.resetLambda();         // set lambda to initial value
	if (datamode==Simulated) {
	    improvePeriod(getMCInitialCalcMode(), true); 
	} else {
	    improvePeriod(datamode, true); 
	}
	cleanSigmas();                // clean the errors
    }   

    std::string protocol;
    DataMode old = timestring.getDataMode();    // set new datamode mode
    timestring.setDataMode(datamode);
    DataMode old_pmode = period.getUseData();
    period.setUseData(datamode);
    period.storeSelection();

    //--- calculate amplitude variation
    if (datamode==Simulated) //--- make a monte carlo simulation
	protocol=calculateMoCaAmpVarPeriod(selected,freqs,attribute,calcmode);
    else
	protocol=calculateAmpVarPeriod(selected,freqs,attribute,calcmode);
    period.restoreSelection();
    period.setUseData(old_pmode);
    timestring.setDataMode(old);                 // restore old datamode
    if (isShiftTimes()) {                        // restore old parameters ...
	period.restoreParameters(1);             // ... after MC simulation
	timestring.restoreCalculated();
    }
    return protocol;
}

//---
//--- calculateAmpVarPeriod is not available in the periodic time shift mode!
//---
std::string CProject::calculateAmpVarPeriod(int freqpnt, int *freqdat,
					int attribute, CalcMode mode) {
    int i;
    period.setProgressMessage((char*)LS_MESSAGE);

    //--- check if weights should be used
    if (!period.getUseWeight()) {
	if (timestring.getSelectedPoints()!=timestring.getWeightSum()) {
	    if (confirm((char*)
		    "Shall I use the given weights for the calculation?")==1) { 
		period.setUseWeight(1);
	    }
	}
    }

    //--- set frequencies for Amplitude Variations
    for (i=0; i<freqpnt; i++)
	period[freqdat[i]].setAmpVar(mode);
   
    //--- which attribute should be used ?
    period.setUseID(attribute);
	computeMeanTimes(attribute);

    //--- write to protocol
    char txt[1024];
    std::string tmp;
    bool noErrors=false;
    bool illCond =false;
    ostringstream pro;
    pro << "Period amplitude/phase variation calculation:" << endl;
    if (period.getUseWeight())  {
	pro << "Calculation based on:     weighted data" << endl;
    }
    pro << "Names are selected from: " << nameSet(attribute) << endl;
    pro << "Calculation startet on:  " << getDate() << endl;

    // calculate
    switch (period.calc(timestring)) {
	case 3: {
	    showMessage("Calculation has been interrupted\n"\
			"The value mFlamda got too small.\n"\
			"The calculated values may not be reliable.");
	    // NO "break" here! Proceed as in case 0
	}
	case 4: { // matrix is ill-conditioned, parameters are ok, 
	    noErrors=true; // but not the errors
	    illCond=true;
	    // NO "break" here! Proceed as in case 0
	}
	case 0: {
	    // general data..
	    CTimeString &timestring=getTimeString();
	    CPeriod const &period=getPeriod();
	    
	    int freqs=period.getFrequencies();
	       
	    // now fill in the data...
	    for (int fre=0; fre<freqs; fre++) {
		int fr=fre;
		if (period[fr].getActive()) {
		    switch (period[fr].getAmpVariation()) {
			case NoVar: {
			    sprintf(txt,
				    "%7s Fre = %12s Amp = %12.6g Pha = %12.6g ",
				    getNumber(fr).c_str(),
				    getFrequency(fr).c_str(),
				    period[fr].getAmplitude(-1),
				    period[fr].getPhase(-1)
				);
			    // write out
			    pro << txt << endl;
			    break;
			}
			case AmpVar: {
			    // first write out the header
			    sprintf(txt,
				    "%7s Fre = %12s                    Pha = %12.6g ",
				    getNumber(fr).c_str(),
				    getFrequency(fr).c_str(),
				    period[fr].getPhase(-1));
			    // write out
			    pro << txt << endl;
				if (timestring.numberOfNames(attribute)!=0) {
				    sprintf(txt,"            Name      Mean time     Amplitude");
				    pro << txt << endl;
				}


      // now write out the relevant data
      for ( int na = 0; na < timestring.numberOfNames( attribute); ++na)
      {
        CName const & cname = timestring.getIndexName( attribute, na);

        if (0 != cname.getPoints( ))
        {
          sprintf
            ( txt
            , "    %12s   %12.9g  %12.6g"
            , cname.getName( ).c_str( )
            , cname.getMeanTime( )
            , period[fr].getAmplitude( cname.getID( ))
            )
          ;

          // write out
          pro << txt << std::endl;
        }
      }



			    break;
			}
			case PhaseVar: {
			    // first write out the header
			    sprintf(txt,"%7s Fre = %12s Amp = %12.6g",
				    getNumber(fr).c_str(),
				    getFrequency(fr).c_str(),
				    period[fr].getAmplitude(-1));
			    // write out
			    pro << txt << endl;
				if (timestring.numberOfNames(attribute)!=0) {
				    sprintf(txt,"            Name      Mean time                      Phase");
				    pro << txt << endl;
				}



        // now write out the relevant data
        for ( int na = 0; na < timestring.numberOfNames( attribute); ++na)
        {
          CName const & cname = timestring.getIndexName( attribute, na);

          if (0 != cname.getPoints( ))
          {
            sprintf
              ( txt
              , "    %12s   %12.9g               %12.6g"
              , cname.getName( ).c_str( )
              , cname.getMeanTime( )
              , period[fr].getPhase( cname.getID( ))
              )
            ;

            // write out
            pro << txt << endl;
          }
        }



			    break;  
			}
			case AllVar: {
			    // first write out the header
			    sprintf(txt,"%7s Fre = %12s",
				    getNumber(fr).c_str(),
				    getFrequency(fr).c_str());
			    // write out
			    pro << txt <<endl;
				if (timestring.numberOfNames(attribute)!=0) {
				    sprintf(txt,"            Name      Mean time     Amplitude        Phase");
				    pro << txt << endl;
				}




      // now write out the relevant data
      for ( int na = 0; na < timestring.numberOfNames( attribute); ++na)
      {
        CName const & cname = timestring.getIndexName( attribute, na);
        
        if (0 != cname.getPoints( ))
        {
          int const id = cname.getID( );
          
          sprintf
            ( txt
            , "    %12s   %12.9g  %12.6g %12.6g"
            , cname.getName( ).c_str( )
            , cname.getMeanTime( )
            , period[fr].getAmplitude( id)
            , period[fr].getPhase( id)
            )
          ;

          // write out
          pro << txt << endl;
        }
      }





			    break;
			}
		    }
		}
	    }
	
	    // now write the rest..
	    sprintf(txt,"Zeropoint:  "FORMAT_AMPLITUDE"\nResiduals:  " \
		    FORMAT_AMPLITUDE"\nIterations: %i",
		    period.getZeropoint(),
		    period.getResiduals(),
		    period.getIterations());
	    pro<<txt<<flush;
	    // have we reached Maximum Number Iterations?
	    if (period.getIterations()>=period.maxIterations()) {
		showMessage("The maximum number of iterations\n"\
			     "has been reached during calculations.\n"\
			     "The result may not be reliable.\n"\
			     "Please retry the calculations.");
	    }
	    // now display the data...
	    pro << endl;
	    tmp = pro.str().c_str();
	    break;
	}
	case 1: // matrix cannot be inverted
	{
	    showMessage("Problem calculating\nmatrix cannot be inverted.");
	    tmp = "Matrix inversion failed";
	    noErrors=true; // don't write errors
	    break;
	}
	case 2: // cancel pressed, may not be a stable solution
	{
	    showMessage("Calculation has been interrupted\n"\
			"The calculated values may not be reliable.");
	    tmp = "Calculation has been interrupted";
	    break;
	}
    }

    //--- create output for the fit errors
    if (noErrors) { 
	if (illCond) { fitactive=-1; } // write a message
	else         { fitactive= 0; } // don't write any errors
    } else           { fitactive= 1; } // normal mode
    setFitError(); // create output

    //--- cleans frequencies of amplitude variations
    for (i=0;i<freqpnt;i++) {
	period[freqdat[i]].setAmpVar(NoVar);
    }
    return tmp;
}

//---
//--- 
//---
std::string CProject::calculateMoCaAmpVarPeriod(int freqpnt, int *freqdat,
					    int attribute, CalcMode mode) {
    int i;
    //--- check if weights should be used
    if (!period.getUseWeight()) {
	if (timestring.getSelectedPoints()!=timestring.getWeightSum()) {
	    if (confirm((char*)"Shall I use the given weights for the calculation?")==1) { 
		period.setUseWeight(1);
	    }
	}
    }
    //--- set timestring settings
    timestring.setMonteCarloResiduals(getResidualsValue());
    timestring.setMonteCarloIterations(mMoCaSimulations);
    //--- don't perform condition checks during monte carlo simulations
    period.setCheckCondNumber(false); 
    //--- set attribute to use
    period.setUseID(attribute); 

    //--- write to protocol
    //---
    char txt[1024]; std::string tmp;
    ostringstream pro;
    pro << "Monte Carlo Simulation:\n";
    if (period.getUseWeight()) 
	pro << "Calculation based on:     weighted data" << endl;
    pro << "Names are selected from: " << nameSet(attribute) << endl;
    if (isShiftTimes())
	pro << "Time string has been shifted in time" << endl;
    pro << "Calculation started on:   " << getDate() << endl;


    //--- prepare monte carlo calculation
    //---
    //--- store the starting values
    period.storeParameters();
    //--- make a copy of the calculated values
    makeCalculatedSimulatedBase();    

    int result=0, mVariables=0;
    mFailedSimulations=0;
    double res_sum=0.0;
    mMoCaResiduals = new double[mMoCaSimulations];

    for (int s=0; s<mMoCaSimulations; s++) { 
	//--- set progress bar message
	sprintf(txt,"Process %i of %i:  ",s+1,mMoCaSimulations);
	period.setProgressMessage(strcat(txt,"( Iteration %i/%i )"));
	//--- set frequencies for amplitude variations
	for (i=0; i<freqpnt; i++) {
	    period[freqdat[i]].setAmpVar(mode);
	}
	//--- create a new noise data set
	timestring.createMonteCarloData(); 
	//--- calculate a fit
	period.resetLambda();
	result = period.calc(timestring, false);
	if (s==0) {
	    //--- initialize an array to save monte carlo data 
	    mVariables = period.getNumberOfParameters();
	    mMonteCarloData.changesize(mMoCaSimulations,mVariables);
	    //--- store a signature that refers to the fitted parameters
	    setFitSignature();
	}
	//--- check if everything is ok
	if (result==2) {                       // cancel calculation
	    mFailedSimulations += mMoCaSimulations-s;  break; 
	}
	if (result==1 ||                       // cannot invert matrix
	    result==3) {                       // mFlamda reached zero
	    mFailedSimulations++; //count the number of failed calculations
	} else {                               // save the results
	    //--- check if the residuals are within the correct bounds!
	    //--- curfit might have converged into an other local minimum
	    int pos = s-mFailedSimulations;
	    if (pos>5) {
		double mean_res=res_sum/pos;
		double new_res =period.getResiduals(); 
		double var=0.0;
		//--- calculate variance
		for (int v=0; v<pos; v++) {
		    var+=pow(mMoCaResiduals[v]-mean_res,2);
		}
		if (fabs(new_res-mean_res)>(3.0*sqrt(var/(pos-1)))) {
		    // residuals violate the 3-sigma criteria
		    mFailedSimulations++;
		    continue;
		}
		mMoCaResiduals[pos]=new_res;
		res_sum+=new_res;
	    } else { // collect data first
		mMoCaResiduals[pos]=period.getResiduals();
		res_sum+=period.getResiduals();
	    }
	    //--- everything is ok, save the data
	    saveMonteCarloParameters(s-mFailedSimulations);
	    //--- save the phases of the first run for reference later
	    if (s==0) { saveMonteCarloInitialPhases(); }
	    //--- restore the old period parameters
	    period.restoreParameters();
	    //--- clean frequencies of amplitude variations
	    for (i=0;i<freqpnt;i++) {
		period[freqdat[i]].setAmpVar(NoVar);
	    }
 	}    
    }
    //---
    //--- restore the old period parameters and timestring values
    //---
    period.restoreParameters();
    makeSimulatedBaseCalculated();
    period.setCheckCondNumber(true); 
    //---
    //--- write the protocol
    //---
    pro << "Calculation finished on:  " << getDate() << endl;
    pro << "Total number of processes: " << mMoCaSimulations << endl;
    pro << "... whereof successfully completed: " 
	<< mMoCaSimulations-mFailedSimulations << endl;
    //---
    //--- calculate the results and terminate
    //---
    calculateErrorMatrix(mVariables);
    if (fitmode==0)      { fitactive=1; } // NORMAL MODE
    else if (fitmode==1) { fitactive=2; } // BINARY MODE
    pro << setMonteCarloFitError(mVariables);
    mMonteCarloData.changesize(0,0);      // clean
    mMonteCarloPhaseData.changesize(0,0);      // clean
    mMoCaResiduals=0;
    mMonteCarloData.changesize(0,0);
    return pro.str( );
}


//---                                          
//--- searchPTSStartValues()
//--- determines good start values for the pts-frequency parameters
//--- by a monte-carlo search
//---
std::string CProject::searchPTSStartValues(DataMode mode, int shots) {
    char txt[256];
    ostringstream pro;

    DataMode old = timestring.getDataMode();
    timestring.setDataMode(mode);

    periodBM.setUseData(mode);
    // check if weights should be used
    if (!periodBM.getUseWeight()) {
	if (timestring.getSelectedPoints()!=timestring.getWeightSum()) {
	    if (confirm((char*)"Shall I use the given weights\n"\
			"for the calculation?")==1) {
		periodBM.setUseWeight(1);
	    }
	}
    }

    // write calculation-header
    pro << "Periodic time shift estimation:\n"<<"Calculation based on:     ";
    if (periodBM.getUseWeight())  { pro << "weighted "; }
    if (mode==Observed)           { pro << "original data" << endl; }
    else                          { pro << "adjusted data" << endl; }
    pro << "Frequency range:          " << periodBM.getMCFreqLow() 
	<< " - " << periodBM.getMCFreqUp() << "\n"; 
    pro << "Amplitude range:          " << periodBM.getMCAmpLow() 
	<< " - " << periodBM.getMCAmpUp() << "\n"; 
    pro << "Calculation started on:   " << getDate() << endl;

    // calculate
    switch(periodBM.searchStart(timestring, shots)) {
	case 0: // normal termination
	{
	    pro << "Calculation finished on:  " << getDate() << endl;
	    pro << writeFrequenciesTabulated(true);
	    sprintf(txt,"Zeropoint:  "FORMAT_AMPLITUDE"\nResiduals:  " \
		    FORMAT_AMPLITUDE, 
		    periodBM.getZeropoint(), periodBM.getResiduals());
	    pro << txt << endl;
	    break;
	}
	default: // user canceled calculation
	{
	    pro << "Calculation canceled on:  " << getDate() << endl;
	    pro << writeFrequenciesTabulated(true);
	    sprintf(txt,"Zeropoint:  "FORMAT_AMPLITUDE"\nResiduals:  " \
		    FORMAT_AMPLITUDE,
		    periodBM.getZeropoint(), periodBM.getResiduals());
	    pro << txt << endl;
	    break;

	}
    }
    timestring.setDataMode(old);
    return pro.str( );
}

//---
//--- setFitSignature()
//--- this method creates a string that contains information
//--- on which parameters have been variable in the calculation.
//--- the signature is needed for the correct output of the
//--- monte carlo uncertainties.
//---
void CProject::setFitSignature() {

    //--- define the period object we are working on
    CPeriod *peri = &getActivePeriod();

    mFitSignature="";
    if (fitmode==1 && (*peri).getBMActive()) { // BINARY MODE
	mFitSignature.append("s");
	if ((*peri).getBMPeriod().isSelected(Fre))
	    mFitSignature.append("f");
	if ((*peri).getBMPeriod().isSelected(Amp)) 
	    mFitSignature.append("a");
	if ((*peri).getBMPeriod().isSelected(Pha)) 
	    mFitSignature.append("p");
    }
    for (int i=0; i<peri->getFrequencies(); i++) {
	//--- skip frequencies that are not active
	if (!(*peri)[i].getActive()) { continue; } 
	ostringstream txt; 
	txt<<'|'<<i;
	mFitSignature.append(txt.str());
	switch ((*peri)[i].getAmpVariation()) { 
	    case AmpVar: 
	    case PhaseVar:
	    case AllVar: { 
		mFitSignature.append("v");
		ostringstream txt; 
		txt<<(int)(*peri)[i].getAmpVariation();
		mFitSignature.append(txt.str());  
		if ((*peri)[i].isSelected(Fre)) 
		    mFitSignature.append("f");
		if ((*peri)[i].isSelected(Amp)) 
		    mFitSignature.append("a");
		if ((*peri)[i].isSelected(Pha)) 
		    mFitSignature.append("p");
		break;
	    }
	    default: { // no variation
		if ((*peri)[i].isSelected(Fre)) 
		    mFitSignature.append("f");
		if ((*peri)[i].isSelected(Amp)) 
		    mFitSignature.append("a");
		if ((*peri)[i].isSelected(Pha)) 
		    mFitSignature.append("p");
		break;
	    }
	}
    }
}

//--- --------------------------------------------------------------- ---//
//---
//--- methods for the 
//--- monte carlo simulation
//---
//---

std::string CProject::generalMonteCarloSimulation() {
    //--- get the period object we are working on
    CPeriod *peri = &getActivePeriod();
    char txt[256];
    ostringstream pro;

    if (isShiftTimes()) {
	// first an improve all with shifted times to get the right phases
	peri->resetLambda();                // set lambda to initial value
	improvePeriod(getMCInitialCalcMode(), true);          // calculate
	cleanSigmas();                                 // clean the errors
    }

    //--- define the settings
    DataMode old = timestring.getDataMode();
    timestring.setDataMode(Simulated);
    timestring.setMonteCarloResiduals(getResidualsValue());
    timestring.setMonteCarloIterations(mMoCaSimulations);
    DataMode old_pmode = peri->getUseData();
    peri->setUseData(Simulated);
    // check if weights should be used
    if (!peri->getUseWeight()) {
	if (timestring.getSelectedPoints()!=timestring.getWeightSum()) {
	    if (confirm((char*)"Shall I use the given weights\n"\
			"for the calculation?")==1) {
		peri->setUseWeight(1);
	    }
	}
    }
    //--- don't perform condition checks during monte carlo simulations
    peri->setCheckCondNumber(false); 

    //--- write calculation-header
    pro << "Monte Carlo Simulation:\n";
    if (peri->getUseWeight()) 
	pro << "Calculation based on:     weighted data" << endl;
    if (isShiftTimes())
	pro << "Time string has been shifted in time" << endl;
    pro << "Calculation started on:   " << getDate() << endl;
    
    //--- store the old period parameters
    peri->storeParameters();
    //--- update the time average (we may use it or not)
    timestring.setTimeAverage(timestring.getTimeAverage());
    //--- make a copy of the calculated values
    makeCalculatedSimulatedBase();
    
    int result=0, mVariables=0;
    mFailedSimulations=0;
    double res_sum=0.0;
    mMoCaResiduals = new double[mMoCaSimulations];

    //--- start the simulations
    for (int i=0; i<mMoCaSimulations; i++) { 
      	//--- set progress bar message
	sprintf(txt,"Process %i of %i:  ",i+1,mMoCaSimulations);
	peri->setProgressMessage(strcat(txt,"( Iteration %i/%i )"));
	//--- create a new noise data set
	timestring.createMonteCarloData(); 
	//--- calculate a fit
	peri->resetLambda();
	result = peri->calc(timestring, false);
	if (i==0) {
	    //--- initialize an array to save monte carlo data 
	    mVariables = peri->getNumberOfParameters();
	    mMonteCarloData.changesize(mMoCaSimulations,mVariables);
	    //--- store a signature that refers to the fitted parameters
	    setFitSignature();
	}
	//--- check if everything is ok
	if (result==2) {                       // cancel calculation
	    mFailedSimulations += mMoCaSimulations-i; break; 
	}
	if (result==1 ||                       // cannot invert matrix
	    result==3) {                       // mFlamda reached zero
	    mFailedSimulations++; //count the number of failed calculations
	} else {                               // save the results
	    //--- check if the residuals are within the correct bounds!
	    //--- curfit might have converged into an other local minimum
	    int pos = i-mFailedSimulations;
	    if (pos>5) {
		double mean_res=res_sum/pos;
		double new_res =peri->getResiduals(); 
		double var=0.0;
		//--- calculate variance
		for (int v=0; v<pos; v++) {
		    var+=pow(mMoCaResiduals[v]-mean_res,2);
		}
		if (fabs(new_res-mean_res)>(3.0*sqrt(var/(pos-1)))) {
		    // residuals violate the 3-sigma criteria
		    mFailedSimulations++;
		    continue;
		}
		mMoCaResiduals[pos]=new_res;
		res_sum+=new_res;
	    } else { // collect data first for residual checks
		mMoCaResiduals[pos]=peri->getResiduals();
		res_sum+=peri->getResiduals();
	    }
	    //--- everything is ok, save the data
	    saveMonteCarloParameters(i-mFailedSimulations);
	    //--- save the phases of the first run for reference later
	    if (i==0) { saveMonteCarloInitialPhases(); }
	    //--- restore the old period parameters
	    peri->restoreParameters();
	}    
    }
    //---
    //--- restore the old period parameters and timestring values
    //---
    peri->restoreParameters();
    makeSimulatedBaseCalculated();
    timestring.setDataMode(old);
    peri->setUseData(old_pmode);
    peri->setCheckCondNumber(true); 
    //---
    //--- write the protocol
    //---
    pro << "Calculation finished on:  " << getDate() << endl;
    pro << "Total number of processes: " << mMoCaSimulations << endl;
    pro << "... whereof successfully completed: " 
	<< mMoCaSimulations-mFailedSimulations << endl;
    //---
    //--- calculate the results and terminate
    //---
    calculateErrorMatrix(mVariables);
    if (fitmode==0)      { fitactive=1; } // NORMAL MODE
    else if (fitmode==1) { fitactive=2; } // BINARY MODE
    pro << setMonteCarloFitError(mVariables);
    mMonteCarloData.changesize(0,0);      // clean
    mMonteCarloPhaseData.changesize(0,0); // clean
    mMoCaResiduals=0;
    //--- restore the original parameters
    if (isShiftTimes()) { 
	peri->restoreParameters(1); 
	timestring.restoreCalculated();
    }
    return pro.str( );
}

void CProject::saveMonteCarloInitialPhases() {
    int count=0;
    //--- get the period object we are working on
    CPeriod *peri = &getActivePeriod();
    int attribute = peri->getUseID();
    //---
    //--- first the pts phase parameter
    //---    
    if (fitmode==1 && (*peri).getBMActive()) {
	if ((*peri).getBMPeriod().isSelected(Pha)) {
	    mMonteCarloPhaseData.changesize(count+1);
	    mMonteCarloPhaseData[count]=(*peri).getBMPhase(-1); count++;
	}
    }
    //---
    //--- now for the common frequencies
    //---    
    for (int i=0; i<peri->getFrequencies(); i++) {
	//--- skip frequencies that are not active
	if (!(*peri)[i].getActive()) { continue; }
	switch ((*peri)[i].getAmpVariation()) { 
	    case PhaseVar: {
		// now write out the phase variation data
		for (int na=0; na<timestring.numberOfNames(attribute); na++) {
		    int id=timestring.getIndexName(attribute,na).getID();
		    int points=timestring.getIndexName(attribute,na).getPoints();		    
		    if (points!=0) {
			mMonteCarloPhaseData.changesize(count+1);
			mMonteCarloPhaseData[count] = (*peri)[i].getPhase(id);
			count++;
		    }
		}
		break;
	    }
	    case AllVar: {
		// now write out the phase variation data
		for (int na=0; na<timestring.numberOfNames(attribute); na++) {
		    int id=timestring.getIndexName(attribute,na).getID();
		    int points=timestring.getIndexName(attribute,na).getPoints();		    
		    if (points!=0) {
			mMonteCarloPhaseData.changesize(count+1);
			mMonteCarloPhaseData[count] = (*peri)[i].getPhase(id);
			count++;
		    }
		}
		break;
	    }
	    default: { // NoVar & AmpVar
		if ((*peri)[i].isSelected(Pha)) {
		    mMonteCarloPhaseData.changesize(count+1);
		    mMonteCarloPhaseData[count] = (*peri)[i].getPhase(-1); 
		    count++;
		}
		break;
	    }
	}
     }
}

//---
//--- this method brings the phases into the right range
//--- (with a difference < 1.0), this is necessary for a correct 
//--- error determination!
//---
double CProject::adaptMonteCarloPhase(double phase, int i) {
    if (mMonteCarloPhaseData.size()==0) { return phase; }
    double diff = phase-mMonteCarloPhaseData[i];
    if (diff>0.5) { 
	//cout<<"+from "<<phase<<" ("<<diff<<")";
	double tmp=0.0; modf(diff,&tmp);
	if (tmp<0.5) { tmp=1.0; }
	//cout<<" ["<<(tmp)<<"]";
	phase-=(tmp);
	//cout<<" to "<<phase<<endl;
    } else if (diff<-0.5) {
	//cout<<"-from "<<phase<<" ("<<diff<<")";
	double tmp=0.0; modf(diff,&tmp);
	if (-tmp<0.5) { tmp=-1.0; }
	//cout<<" ["<<(-tmp)<<"]";
	phase+=(-tmp);
	//cout<<" to "<<phase<<endl;
    }
    return phase;
}

void CProject::saveMonteCarloParameters(int sim) {
    int count=0, pcount=0;
    //--- get the period object we are working on
    CPeriod *peri = &getActivePeriod();
    int attribute = peri->getUseID();
    //---
    //--- store zero point
    //---
    mMonteCarloData[sim][count] = peri->getZeropoint();
    count++;
    //---
    //--- store pts parameters
    //---
    if (fitmode==1 && (*peri).getBMActive()) {
	if ((*peri).getBMPeriod().isSelected(Fre)) {
	    mMonteCarloData[sim][count] = (*peri).getBMFrequency(); count++;
	}
	if ((*peri).getBMPeriod().isSelected(Amp)) {
	    mMonteCarloData[sim][count] = (*peri).getBMAmplitude(-1); count++;
	}
	if ((*peri).getBMPeriod().isSelected(Pha)) {
	    mMonteCarloData[sim][count] = 
		adaptMonteCarloPhase((*peri).getBMPhase(-1), pcount); 
	    count++; pcount++;	
	}
    }
    //---
    //--- now the common frequencies
    //---
    for (int i=0; i<peri->getFrequencies(); i++) {
	//--- skip frequencies that are not active
	if (!(*peri)[i].getActive()) { continue; }
	switch ((*peri)[i].getAmpVariation()) { 
	    case AmpVar: {
		if ((*peri)[i].isSelected(Fre)) {
		    mMonteCarloData[sim][count] = (*peri)[i].getFrequency();
		    count++;
		}
		// now write out the amplitude variation data
		for (int na=0; na<timestring.numberOfNames(attribute); na++) {
		    int id=timestring.getIndexName(attribute,na).getID();
		    int points=timestring.getIndexName(attribute,na).getPoints();		    
		    if (points!=0) {
			mMonteCarloData[sim][count] = (*peri)[i].getAmplitude(id);
			count++;
		    }
		}
		if ((*peri)[i].isSelected(Pha)) {
		    mMonteCarloData[sim][count] = 
			adaptMonteCarloPhase((*peri)[i].getPhase(-1), pcount); 
		    count++; pcount++;
		}
		break;
	    }
	    case PhaseVar: {
		if ((*peri)[i].isSelected(Fre)) {
		    mMonteCarloData[sim][count] = (*peri)[i].getFrequency();
		    count++;
		}
		if ((*peri)[i].isSelected(Amp)) {
		    mMonteCarloData[sim][count] = (*peri)[i].getAmplitude(-1); 
		    count++;
		}
		// now write out the phase variation data
		for (int na=0; na<timestring.numberOfNames(attribute); na++) {
		    int id=timestring.getIndexName(attribute,na).getID();
		    int points=timestring.getIndexName(attribute,na).getPoints();		    
		    if (points!=0) {
			mMonteCarloData[sim][count] = 
			    adaptMonteCarloPhase((*peri)[i].getPhase(id), pcount); 
			count++; pcount++;
		    }
		}
		break;
	    }
	    case AllVar: {
		if ((*peri)[i].isSelected(Fre)) {
		    mMonteCarloData[sim][count] = (*peri)[i].getFrequency();
		    count++;
		}
		// now write out the amplitude variation data
		int na;
		for (na=0; na<timestring.numberOfNames(attribute); na++) {
		    int id=timestring.getIndexName(attribute,na).getID();
		    int points=timestring.getIndexName(attribute,na).getPoints();		    
		    if (points!=0) {
			mMonteCarloData[sim][count] = (*peri)[i].getAmplitude(id);
			count++;
		    }
		}
		// now write out the phase variation data
		for (na=0; na<timestring.numberOfNames(attribute); na++) {
		    int id=timestring.getIndexName(attribute,na).getID();
		    int points=timestring.getIndexName(attribute,na).getPoints();		    
		    if (points!=0) {
			mMonteCarloData[sim][count] = 
			    adaptMonteCarloPhase((*peri)[i].getPhase(id), pcount); 
			count++; pcount++;
		    }
		}
		break;
	    }
	    default: { // NoVar
		if ((*peri)[i].isSelected(Fre)) {
		    mMonteCarloData[sim][count] = (*peri)[i].getFrequency();
		    count++;
		}
		if ((*peri)[i].isSelected(Amp)) {
		    mMonteCarloData[sim][count] = (*peri)[i].getAmplitude(-1); 
		    count++;
		}
		if ((*peri)[i].isSelected(Pha)) {
		    mMonteCarloData[sim][count] = 
			adaptMonteCarloPhase(
			    (*peri)[i].getPhase(-1), pcount); 
		    count++; pcount++;
		}
		break;
	    }
	}
    }
}

void CProject::calculateErrorMatrix(int mVariables) {
    mErrorMatrix.changesize(0,0);  // clear error matrix
    mErrorMatrix.changesize(mVariables,mVariables);
    int sims = mMoCaSimulations-mFailedSimulations;

    //--- first calculate the mean parameter values
    mMeanParam = new double[mVariables];
    for (int p=0; p<mVariables; p++) {
	mMeanParam[p]=0.0;
	for (int m=0; m<sims; m++) {
	    mMeanParam[p]+=mMonteCarloData[m][p];
	    //cerr<<"------ "<<mMonteCarloData[m][p]<<endl;
	}
	mMeanParam[p]/=sims;
	//cerr<<"param: "<<mMeanParam[p]<<endl;
    }

	
	if (sims>mVariables) {
	double factor = 1.0/(sims-mVariables);//(sims-1);
	for (int i=0; i<mVariables; i++) {
	  for (int j=0; j<mVariables; j++) {
		for (int k=0; k<sims; k++) { // sum over simulations
		  mErrorMatrix[i][j]+=((mMonteCarloData[k][i]-mMeanParam[i])*
							   (mMonteCarloData[k][j]-mMeanParam[j]));
		}
		mErrorMatrix[i][j]*=factor;
	  }
	}
	} else { 
	  //--- number of usable simulations is too low to use the upper formula
	  //--- so use this approximation
	  double *variance = new double[mVariables];
	  for (int p=0; p<mVariables; p++) { variance[p] = 0.0; }
	  for (int m=0; m<sims; m++) {
	    for (int p=0; p<mVariables; p++) {
	      variance[p] += pow(mMonteCarloData[m][p]-mMeanParam[p],2);
	      mErrorMatrix[p][p]=variance[p]/m;
	    }
	  }
	  delete [] variance;
    }
}

std::string CProject::setMonteCarloFitError(int mVariables) {
    ostringstream ostr, pro, heading;
    ostr.precision(10); pro.precision(10);
    ostr << showpoint <<
	" ---------------------------------------------------------\n" << 
	" --        Results of the Monte Carlo Simulation        --\n" << 
	" ---------------------------------------------------------\n" <<endl; 
  
    if (fitactive==0 ||            // there's no calculated fit at the moment
	(fitactive==1 && fitmode==1) ||     // there's a fit of another mode
	(fitactive==2 && fitmode==0)) { 
	fit_protocol = ostr.str( );
	return "";
    }

    //--- get the period object we are working on
    CPeriod *peri = &getActivePeriod();
    int attribute = peri->getUseID();

    //--- write header
    ostr << " Monte Carlo Processes (total):  "<<mMoCaSimulations<<"\n"
	 << "      (successfully completed):  "
	 << mMoCaSimulations-mFailedSimulations << "\n\n"
	 << " Zeropoint:\t"<< peri->getZeropoint() <<"\n Uncertainty:\t" 
	 << showpoint <<  sqrt(mErrorMatrix[0][0])<<"\n\n"
	 << " Nr.\tFreq. sigma\tAmpl. sigma\tPha. sigma\n" 
	 << " ---------------------------------------------------------"
	 << endl;

    int pos=1;
    int freq=0;
    int fnr=0;   // F number of the frequency with index freq

    //---
    //--- mSignature contains information about which parameters have been
    //--- fitted and a reference to their position in the error matrix
    //---
    const int names  = timestring.numberOfNames(attribute);
    const int length = mFitSignature.size();
    for (int s=0; s<length; s++) {
	if (mFitSignature.at(s)=='|') // skip delimiter
	    continue;
	if (atoi(&mFitSignature.at(s))!=0 || mFitSignature.at(s)=='0') {
	    //--- are there more numbers?
	    string s_freq="";
	    int t=s;
	    while (atoi(&mFitSignature.at(t))!=0 || mFitSignature.at(t)=='0') {
		s_freq.append(sizeof(char),mFitSignature.at(t));
		t++;
		if (t>=length) {
		    //--- there is no further information, finish
		    ostr << pro.str();
		    //--- fit protocol for the goodness of fit display
		    fit_protocol = ostr.str( ); 
		    //--- return results for the log
		    return pro.str( );
		}
		else if (mFitSignature.at(t)=='|')
		    break;
	    }
	    istringstream iss(s_freq);
	    iss >> freq;
	    if (mFitSignature.at(t)=='|') {
		//--- there are no selected parameters of this frequency,
		//--- go to next frequency
		s=t;
	    } else {
		s=t-1; // set the appropriate position
		if (s<0) { s=0; }
		fnr=(*peri)[freq].getNumber()+1;
		pro<<" F"<<fnr;
	    }
	} else {
	    switch (mFitSignature.at(s)) {//(sig) {
		case 'f': {
		    //--- write out frequency sigma
		    pro<<"\t"<<sqrt(mErrorMatrix[pos][pos]);
		    pos++; 
		    heading <<"F"<<fnr<<": Freq\t";  
		    //--- write out amplitude sigma
		    if ((s+1)<length && mFitSignature.at(s+1)=='a') {
  			pro<<"\t"<<sqrt(mErrorMatrix[pos][pos]);
			pos++; s++;
			heading <<"F"<<fnr<<": Amplitude\t";
		    } else { 
			if ((s+1)<length && mFitSignature.at(s+1)=='p') {
			    pro<<"\tfixed\t\t"<<sqrt(mErrorMatrix[pos][pos]);
			    pos++; s++;
			    heading <<"F"<<fnr<<": Phase\t";
			} else {
			    pro<<"\tfixed\t\tfixed"<<endl;  
			}
			break;
		    }
		    //--- write out phase sigma
		    if ((s+1)<length && mFitSignature.at(s+1)=='p') {
			pro<<"\t"<<sqrt(mErrorMatrix[pos][pos]);
		        pos++; s++;
			heading <<"F"<<fnr<<": Phase\t";
		    } else {
			pro<<"\tfixed"; 
		    }
		    pro<<endl;
		    break;
		}
		case 'a': { //--- an amplitude with no preceeding frequency
		    //--- write out amplitude
		    pro<<"\tfixed\t\t"<<sqrt(mErrorMatrix[pos][pos]);
		    pos++; 
		    heading <<"F"<<fnr<<": Amplitude\t";
		    //--- write out phase
		    if ((s+1)<length && mFitSignature.at(s+1)=='p') {
			pro<<"\t"<<sqrt(mErrorMatrix[pos][pos]);
			pos++; s++;
			heading <<"F"<<fnr<<": Phase\t";
		    } else {  
			pro<<"\tfixed"; 
		    }
		    pro<<endl;
		    break;  
		}
		case 'p': { //--- a phase with no preceeding freq. and ampl. 
		    pro<<"\tfixed\t\tfixed\t\t"<<sqrt(mErrorMatrix[pos][pos])<<endl;
		    pos++; 
		    heading <<"F"<<fnr<<": Phase\t";
		    break;
		}
		case 's': { //--- write out periodic time shift parameters
		    pro<<" PTSF";
		    //--- write out pts frequency sigma
		    if (mFitSignature.at(s+1)=='f') { 
			pro<<"\t"<<sqrt(mErrorMatrix[pos][pos]);
			pos++; s++;
			heading <<"PTSF: Freq\t";
		    } else { pro<<"\tfixed\t"; }
		    //--- write out pts amplitude sigma
		    if (mFitSignature.at(s+1)=='a') { 
			pro<<"\t"<<sqrt(mErrorMatrix[pos][pos]);
			pos++; s++;
			heading <<"PTSF: Amplitude\t";
		    } else { pro<<"\tfixed\t"; }
		    //--- write out pts phase sigma
		    if (mFitSignature.at(s+1)=='p') { 
			pro<<"\t"<<sqrt(mErrorMatrix[pos][pos]);
			pos++; s++;
			heading <<"PTSF: Phase\t";
		    } else { pro<<"\tfixed"; }
		    pro << endl;
		    break;
		}
		case 'v': { //--- write out amplitude/phase variation
		    // read in type of variation
		    char ch = mFitSignature.at(s+1); s++; 
		    int ampvarmode = atoi(&ch);
		    switch (ampvarmode) {
			case AmpVar: {
			    int names = timestring.numberOfNames(attribute);
			    //--- write the line with frequency and phase sigmas first 
			    if (mFitSignature.at(s+1)=='f') {
				pro<<"\t"<<sqrt(mErrorMatrix[pos][pos]);
				pos++; s++;
				heading <<"F"<<fnr<<": Freq\t";
			    } else { pro<<"\tfixed\t"; }
			    //--- write amplitude sigmas
			    ostringstream protmp;
			    protmp<<"           Name:\tAmpl.sigma:"<<endl;

          
          
          
          for ( int na = 0; na < names; ++na)
          {
            CName const & cname = timestring.getIndexName( attribute, na);

            if (0 != cname.getPoints( ))
            {
              protmp
                << "           "<< cname.getName( )
                << "\t" << std::sqrt( mErrorMatrix[pos][pos])
                << std::endl
              ;

              ++pos; 
              heading << "F" << fnr << ": Amp[" << na << "]\t";
            }
          }


			    s++; // skip the 'a'
			    //--- write phase sigma
			    if ((s+1)<length && mFitSignature.at(s+1)=='p') {
				pro<<"\t\t\t"<<sqrt(mErrorMatrix[pos][pos])<<endl;
				pos++; s++;
				heading <<"F"<<fnr<<": Phase\t";
			    } else { pro<<"\t\t\tfixed"<<endl; }
			    pro<<protmp.str();
			    break;
			} 
			case PhaseVar: {
			    //--- write the line with frequency and amplitude sigmas first 
			    if (mFitSignature.at(s+1)=='f') {
				pro<<"\t"<<sqrt(mErrorMatrix[pos][pos]);
				pos++; s++;
				heading <<"F"<<fnr<<": Freq\t";
			    } else { pro<<"\tfixed\t"; }
			    //--- write amplitude sigma
			    if (mFitSignature.at(s+1)=='a') {
				pro<<"\t"<<sqrt(mErrorMatrix[pos][pos])<<endl;
				pos++; s++;
				heading <<"F"<<fnr<<": Amplitude\t";	
			    } else { pro<<"\tfixed"<<endl; }
			    //--- write phase sigma
			    pro<<"           Name:\t\t\tPha. sigma:"<<endl;
          
          
          
          
          
          for ( int na = 0; na < names; ++na)
          {
            CName const & cname = timestring.getIndexName( attribute, na);

            if (0 != cname.getPoints( ))
            {
              pro
                << "           " << cname.getName( )
                << "\t\t\t" << std::sqrt( mErrorMatrix[pos][pos])
                << std::endl
              ;

              ++pos;
              heading << "F" << fnr << ": Pha[" << na << "]\t";
            }
          }



			    s++; // skip the 'p'
			    break;
			}
			case AllVar: {
			    //--- write the line with frequency and amplitude sigmas first 
			    if (mFitSignature.at(s+1)=='f') {
				pro<<"\t"<<sqrt(mErrorMatrix[pos][pos])<<endl;
				pos++; s++;
				heading <<"F"<<fnr<<": Freq\t";////////
			    } else { pro<<"\tfixed\t"<<endl; }
			    //--- write amplitude and phase sigmas
			    pro<<"           Name:\tAmpl. sigma:\tPha. sigma:"<<endl;

          
          
          for ( int na = 0; na < names; ++na)
          {
            CName const & cname = timestring.getIndexName( attribute, na);

            if (0 != cname.getPoints( ))
            {
              pro
                << "           " << cname.getName( )
                << "\t" << std::sqrt( mErrorMatrix[pos][pos])
              ;

              ++pos; 

              pro
                << "\t" << std::sqrt( mErrorMatrix[pos][pos])
                << std::endl
              ;
              
              ++pos;

              heading <<"F"<<fnr<<": Amp["<<na<<"]\t";
              heading <<"F"<<fnr<<": Pha["<<na<<"]\t";
            }
          }



			    s+=2; // skip 'ap'
			    break;
			}
		    }
		    break;
		}
	    }
	}
    }
    ostr << pro.str();
    //---
    //--- write out the results of each run
    //---
    int sims = mMoCaSimulations-mFailedSimulations;
    ostr.precision(15);
    ostr << "\n\n\n Results for each process:\n" 
	 << " -------------------------\n" << endl;
    ostr << " Proc#\tZeropoint\t" << heading.str() << endl;
    for (int m=0; m<sims; m++) {
	ostr<<" "<<m;
	for (int p=0; p<mVariables; p++) {
	    ostr<<"\t"<<mMonteCarloData[m][p];
	}
	ostr<<endl;
    }

    if (isShiftTimes()) {
	ostr << "\nPlease note:\n"\
	    "The 'Uncouple frequency and phase uncertainties' option\n"\
	    "has been used for this calculation. To provide uncorrelated\n"\
	    "uncertainties the time string has been shifted in time,\n"\
	    "so that the 'average time' is zero. For this reason, \n"\
	    "the phases displayed here do have a different value as opposed\n"\
	    "to the unshifted data set." << endl;
    }

    fit_protocol = ostr.str( ); // fit protocol for the display
    return pro.str( );          // return results for the log
}
