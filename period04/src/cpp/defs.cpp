
#include <iostream>
using std::cout;
using std::endl;

int    MAX_FREQUENCIES = 2560;
double DEFAULT_ALIAS_STEP;
char*  DEFAULT_FOURIER_NAME = (char*)"My Fourier calculation";
double DEFAULT_FOURIER_LOWER_FREQUENCY = 0.0;
double DEFAULT_FOURIER_UPPER_FREQUENCY = 50.0;
char*  DEFAULT_SCALE_SETTING;
char*  DEFAULT_NAME_SET[4];

