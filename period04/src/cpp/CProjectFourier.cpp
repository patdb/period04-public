
#include <stdio.h>

#include <fstream>
using std::ofstream;

#include <sstream>              
using std::ostringstream;

#include <iostream>       
using std::cout;
using std::endl;

#include <algorithm>
using std::sort;

#include "CProject.h"
#include "CFourierAlgorithm.h"
#include "defs.h"
#include "format.h"
#include "message.h"


void CProject::cleanFourier ( )
{
  for
    ( std::list< PFourier>::iterator iter = fourierList.begin( )
    ; iter != fourierList.end( )
    ; ++iter
    )
  {
    delete (*iter);
  }

  fourierList.clear( );
  fourierActive = &defaultfourier;
  useFourierWeight = 0;
}

int CProject::getFourierActiveListID ( )
{
  int n = 0;

  if (fourierActive == &defaultfourier)
  { 
    return n; 
  }

  for
    ( std::list< PFourier>::iterator iter = fourierList.begin( )
    ; iter != fourierList.end( )
    ; ++iter,  ++n
    )
  {
    if ((*iter) == fourierActive)
    {
      return n;
    }
  }

  return n;
} 

void CProject::renameActiveFourier ( std::string const & newName)
{
  fourierActive->setTitle( newName);
  ++changed;
}

void CProject::deleteActiveFourier ( )
{
  if (fourierActive == &defaultfourier)
  { 
    return; 
  }

  for
    ( std::list< PFourier>::iterator iter = fourierList.begin( )
    ; iter != fourierList.end( )
    ; ++iter
    )
  {
    if ((*iter) == fourierActive)
    {
      //--- we have found the entry, let's remove it
      delete fourierActive;
      //--- and also from the list:
      //    Since the call to erase invalidates the iterator, we have to 
      //    increment iter before calling erase. Note that in the following
      //    line, while the iterator is incremented before calling erase,
      //    the old value of iter is passed to erase.
      iter = fourierList.erase( iter++ );
      
      if( iter != fourierList.end( ))
      {
        fourierActive = (*iter);
      }
      else // == fourierList.end()
      {	
	if ( iter == fourierList.begin() )
	  {
	    fourierActive = &defaultfourier;
	  }
	else 
	  {
	    --iter;
	    fourierActive = (*iter);
	  }
      }

      ++changed;

      return;
    }
  }

  return;
}

std::string CProject::getActiveFourierProperties ( )
{
  return fourierActive->getProperties( );
}

void CProject::saveFourier( CFourier const & fourier, std::string const & path)
{
    if (path.empty( ))
    {
      // no filename given, let's return... , should not happen
      return;
    }
    // check if we can write a file
    if (! canWrite( path.c_str( )))
    {
      std::ostringstream text;
      text << "Cannot write to file " << path;
      showMessage( text.str( ).c_str( ));
      return;
    }

  // save file...
  ofstream datafile( path.c_str( ));
  fourier.writeData( datafile);
}

void CProject::saveAllFourier
  ( std::string const & directory
  , bool usePrefix
  , std::string const & prefix
  )
{
  std::list< PFourier>::const_iterator iter = fourierList.begin( );

  for
    ( int n = 0
    ; (iter != fourierList.end( ))
    ; ++iter, ++n
    )
  {
    if (usePrefix)
    {
      char txt[256];

      sprintf( txt, "%s%i.fou", (directory+prefix).c_str( ), n);
      saveFourier( *(*iter), txt);
    }
    else
    {
      saveFourier( *(*iter), directory + (*iter)->getTitle( ) + ".fou");
    }
  }

  return;
}

void CProject::saveSpecialFourier
  ( std::string const & directory
  , int * selectedindices
  , int selected
  , bool usePrefix
  , std::string const & prefix
  )
{
  std::list< PFourier>::const_iterator iter = fourierList.begin( );

  for
    ( int n = 0
    ; (iter != fourierList.end( ))
    ; ++iter, ++n
    )
  {
    for ( int k = 0; k < selected; ++k)
    {
      if (n == selectedindices[k])
      {
        if (usePrefix)
        {
          char txt[256];

          sprintf( txt, "%s%i.fou", (directory+prefix).c_str( ), n);
          saveFourier( *(*iter), txt);
        }
        else
        {
          saveFourier( *(*iter), directory + (*iter)->getTitle( ) + ".fou");
        }
      }
    }
  }

  return;
}


void CProject::loadFourier
  ( std::string const & path
  )
{
  if (! fileExist( path))
  {
    std::ostringstream text;
    text << "File " << path << " does not exist";
    showMessage( text.str( ).c_str( ));
    return;
  }
}

void CProject::readFourier
  ( istream & ist
  )
{
  char c;
  int eof;

  while ( ist>>c, eof=(ist.eof()), ist.putback(c), (c!='[') )
  {
    if (eof == 1) {break;}

    CFourier * tmp = new CFourier( "noname");  // allocate memory
    fourierList.push_back( tmp);               // add to list
    tmp->readHeader(ist);                // reading info...
  }
}

void CProject::writeFourier ( ostream & ost)
{
  for
    ( std::list< PFourier>::iterator iter = fourierList.begin( )
    ; iter != fourierList.end( )
    ; ++iter
    )
  {
    // OK lets save the data
    ost << "[Fourier]\n";
    (*iter)->writeHeader( ost);
    ost << endl;
  }

  return;
}

double CProject::getStepRate(StepQuality qstep, double step) const {
    double tmp = timestring.baseLine(); 
    if (tmp==0) {
	// no baseline, no steps
	return 0;
    }
    // now calculate reciprocal
    tmp=1/tmp;
    // and multiply with weight
    switch (qstep) {
	case LOW:
	    tmp/=10; break;
	case MEDIUM: 
	    tmp/=15; break;
	case HIGH:
	    tmp/=20; break;
	default:
	    tmp=step; break;
    }
    return tmp;
}

char* CProject::getStepQualityLabel(StepQuality mode) {
    switch (mode) {
	case LOW:    return (char*)"Low";
	case MEDIUM: return (char*)"Medium";
	case HIGH:   return (char*)"High";
	case CUSTOM: return (char*)"Custom";
    }
    return (char*)"Custom";
}

std::string CProject::checkFourierTitle ( std::string title)
{
  for
    ( std::list< PFourier>::iterator iter = fourierList.begin( )
    ; iter != fourierList.end( )
    ; ++iter
    )
  {
    if (title == (*iter)->getTitle( ))
    {
      title += "+";
    }
  }

  return title;
}

int CProject::addFrequencyPeak() {
    int i=-1;
    if (fitmode==0) {
	//--- for normal mode
	i = period.findEmpty();                 // find an empty position
	period.add();                               // allocate 
	period[i]=getFourierActive()->getPeak();    // add peak to frequency-list
	period[i].setActive(0);                     // and DEACTIVATE it
    } else if (fitmode==1) {
	//--- for binary mode
	i = periodBM.findEmpty();               // find an empty position
	periodBM.add();                             // allocate 
	periodBM[i]=getFourierActive()->getPeak();  // add peak to frequency-list
	periodBM[i].setActive(0);                   // and DEACTIVATE it
    }
    // return the index of the frequency (for normal mode)
    return i;
}

int CProject::addFrequencyPeak(double fr, double amp) {
    int i=-1;
    if (fitmode==0) {
	//--- for normal mode
	i = period.findEmpty();                 // find an empty position
	period.add();                               // allocate 
	period[i]=CPeriPoint(fr,amp,0);    // add peak to frequency-list
	period[i].setActive(0);                     // and DEACTIVATE it
    } else if (fitmode==1) {
	//--- for binary mode
	i = periodBM.findEmpty();               // find an empty position
	periodBM.add();                             // allocate 
	periodBM[i]=CPeriPoint(fr,amp,0);   // add peak to frequency-list
	periodBM[i].setActive(0);                   // and DEACTIVATE it
    }
    // return the index of the frequency (for normal mode)
    return i;
}

string CProject::isPeakComposition ( double freq)
{
  // select the simplest composition
  string scompo = "";
  double weight = -1.0;

/*
  double offset=-1.0; // was diff.
  double mufactor=-1.0; // not calculated or used at all.
    //if (iw>weight && mu>0.0) { // this would be very strict!

im Prinzip wird in isPeakComposition der Nenner in der mu-Wert-Formel  
berechnet. Der mu-Wert würde zusätzlich noch die Amplitude der combination  
frequency berücksichtigen.
*/

  int maxdepth = 4;
  double accuracy = getFreqResForCompoSearch( );
  int depth = maxdepth;

  // check for harmonics
  int size = period.getFrequencies( );

  for ( int i = 0; i < size; ++i)
  {
    if (! period[i].getActive( ) || period[i].empty( ) || period[i].isComposition( )) 
      continue;

    double f1 = period[i].getFrequency( );

    for ( int k = 1; k <= depth; ++k)
    {
      double diff = fabs( k * f1 - freq);

      if (diff < accuracy)
      {
        double iw = 0.01 * pow( period[i].getAmplitude( -1), k);

        if (iw > weight)
        {
          ostringstream ostr;

          ostr
            << k * f1
            << "=" << k << "f" << period[i].getNumber( ) + 1 
          ;

          scompo = ostr.str( );
          weight = iw;
        }
      }
    }
  }

  // check two-component compositions
  --depth; // reduce depth

  for ( int i = 0; i < size; ++i)
  {
    //	for (int j=i+1; j<size; j++) {
    // we need 0 for - combinations, so we have duplicates for + combis
    for (int j = 0; j < size; ++j)
    { 
      if (i == j)
        continue;
      
      if ( ! period[i].getActive() || period[i].empty() || period[i].isComposition( )
        || ! period[j].getActive() || period[j].empty() || period[j].isComposition( )
         )
        continue;

      // i and j define the frequency indices
      // k and l define the depth of the combination
      for ( int k = 1; k <= depth; ++k)
      {
        for (int l=1; l<=depth; ++l)
        {
          double diff = fabs( k * period[i].getFrequency( ) + l * period[j].getFrequency( ) - freq);

          if (diff < accuracy)
          {
            double iw = pow( period[i].getAmplitude( -1), k) * pow( period[j].getAmplitude( -1), l) * pow( 0.01, k + l);

            if (iw > weight)
            {
              ostringstream ostr;

              ostr
                << k * period[i].getFrequency( ) + l * period[j].getFrequency( )
                << "=" << k << "f" << period[i].getNumber( ) + 1
                << "+" << l << "f" << period[j].getNumber( ) + 1
              ;

              scompo = ostr.str( );
              weight = iw;
            }
          }

          diff = fabs( k * period[i].getFrequency( ) - l * period[j].getFrequency( ) - freq);

          if (diff < accuracy)
          {
            double iw = pow( period[i].getAmplitude( -1), k) * pow( period[j].getAmplitude( -1), l) * pow( 0.01, k + l);

            if (iw > weight)
            {
              ostringstream ostr;

              ostr
                << k * period[i].getFrequency( ) - l * period[j].getFrequency( ) << "=" << k << "f" << period[i].getNumber( ) + 1
                << "-" << l << "f" << period[j].getNumber( ) + 1
              ;

              scompo = ostr.str( );
              weight = iw;
            }
          }
        }
      }
    }
  }

  // check three-component compositions
  --depth; // reduce depth

  for ( int i = 0; i < size; ++i)
  {
    //for ( int j = i + 1; j < size; ++j)
    // we need 0 for - combinations, so we have duplicates for + combis
    for ( int j = 0; j < size; ++j)
    { 
      if (i == j)
        continue;

        //for ( int h = j + 1; h < size; ++h)
      for ( int h = 0; h < size; ++h)
      {
        if (i == h || j == h)
          continue;

        if ( ! period[i].getActive( ) || period[i].empty( ) || period[i].isComposition( )
          || ! period[j].getActive( ) || period[j].empty( ) || period[j].isComposition( )
          || ! period[h].getActive( ) || period[h].empty( ) || period[h].isComposition( )
           )
          continue;

  // i,j and h define the frequency indices
	// k,l and m define the depth of the combination
	for (int k=1; k<=depth; k++) {
	  for (int l=1; l<=depth; l++) {
	    for (int m=1; m<=depth; m++) {

        
  double diff = fabs(k*period[i].getFrequency() + l*period[j].getFrequency() + m*period[h].getFrequency() - freq);

  if (diff < accuracy)
  {
    double iw = pow(period[i].getAmplitude(-1),k)*pow(period[j].getAmplitude(-1),l)*pow(period[h].getAmplitude(-1),m)*pow(0.01,k+l+m);

    if (iw > weight)
    {
      ostringstream ostr;

      ostr
        << k*period[i].getFrequency() + l*period[j].getFrequency() + m*period[h].getFrequency() << "=" << k << "f" << period[i].getNumber()+1
        << "+" << l << "f" << period[j].getNumber()+1
        << "+" << m << "f" << period[h].getNumber()+1
      ;
      
      scompo = ostr.str( );
      weight = iw;
    }
  }

  diff = fabs(k*period[i].getFrequency() + l*period[j].getFrequency() - m*period[h].getFrequency() - freq);
  
  if (diff < accuracy)
  {
    double iw = pow(period[i].getAmplitude(-1),k)*pow(period[j].getAmplitude(-1),l)*pow(period[h].getAmplitude(-1),m)*pow(0.01,k+l+m);

    if (iw > weight)
    {
      ostringstream ostr;

      ostr
        << k*period[i].getFrequency() + l*period[j].getFrequency() - m*period[h].getFrequency() << "=" << k << "f" << period[i].getNumber()+1
        << "+" << l << "f" << period[j].getNumber()+1
        << "-" << m << "f" << period[h].getNumber()+1
      ;
      
      scompo = ostr.str( );
      weight = iw;
    }
  }

  diff = fabs(k*period[i].getFrequency() - l*period[j].getFrequency() + m*period[h].getFrequency() - freq);

  if (diff < accuracy)
  {
    double iw = pow(period[i].getAmplitude(-1),k)*pow(period[j].getAmplitude(-1),l)*pow(period[h].getAmplitude(-1),m)*pow(0.01,k+l+m);

    if (iw > weight)
    {
      ostringstream ostr;

      ostr
        << k*period[i].getFrequency() - l*period[j].getFrequency() + m*period[h].getFrequency() << "=" << k << "f" << period[i].getNumber()+1
        << "-" << l << "f" << period[j].getNumber()+1
        << "+" << m << "f" << period[h].getNumber()+1
      ;
      
      scompo = ostr.str( );
      weight = iw;
    }
  }

  diff = fabs(k*period[i].getFrequency() - l*period[j].getFrequency() - m*period[h].getFrequency() - freq);

  if (diff < accuracy)
  {
    double iw = pow(period[i].getAmplitude(-1),k)*pow(period[j].getAmplitude(-1),l)*pow(period[h].getAmplitude(-1),m)*pow(0.01,k+l+m);

    if (iw > weight)
    {
      ostringstream ostr;

      ostr
        << k*period[i].getFrequency() - l*period[j].getFrequency() - m*period[h].getFrequency() << "=" << k << "f" << period[i].getNumber()+1
        << "-" << l << "f" << period[j].getNumber()+1
        << "-" << m << "f" << period[h].getNumber()+1
      ;
      
      scompo = ostr.str( );
      weight = iw;
    }
  }
        
        
        
      }
    }
  }
      }
    }
  }


  return scompo;
}

std::string CProject::writeSelectedData ( )
{
  std::ostringstream ostr;
  ostr << "Selected data:" << endl;
  int most = 0;
  int names [4];

  for ( int m = 0; m < 4; ++m)
  { 
    ostr << nameSet( m) << "\t";
    int n = numberOfNames( m);
    names[m] = n;

    if (n > most)
    {
      most = n;
    }
  }

  ostr << endl;

  for ( int i = 0; i < most; ++i)
  {
    for ( int m = 0; m < 4; ++m)
    {
      if (i == most)
      {
        break;
      }
      
      ostr
        << ((i < names[m] && getIndexNameSelect( m, i))
          ? getIndexNameStr( m, i) + "\t"
          : "        \t"
          )
      ;
    }

    ostr << std::endl;
  }

  return ostr.str( );
}


void CProject::calculateFourier
  ( std::string const & title
  , double from
  , double to
  , StepQuality stepq
  , double step
  , DataMode mode
  , CompactMode compact
  , int weight
  , double zero
  )
{
    double steprate = getStepRate( stepq, step);

    // create the fourier-data
    CFourier * tmp = new CFourier
      ( title, KURTZ1985, from, to
      , stepq, steprate
      , mode, compact, weight
      )
    ;

    if (mode==3) { // SpecWin2
      tmp->setSW2Frequency(mSW2Frequency);
    }

    tmp->setInfo
      ( writeFrequenciesTabulated( true, false)
      + "\n"
      + writeSelectedData( )
      )
    ;

    // make the calculations
    fourier_transform( timestring, *tmp, zero);

    if (!isCalculationCanceled()) {
	// add calculations to the Fourier-list
	fourierList.push_back( tmp);
	fourierActive=tmp;
    } else {
	delete tmp;
    }
}

int CProject::calculateNoiseSpectrum(double from, double to, 
				     double calcDist, double box, 
				     StepQuality stepq, double step, 
				     DataMode mode, double zero)
{
    int weight      = useFourierWeight;
    double steprate = getStepRate(stepq,step);

    // make the calculations
    int pos=0;

    // find out how many steps
    int steps=0;
    double p;
    for (p=from+calcDist/2; p<to; p+=calcDist) {
	steps++;
    }
    
    double percentageStart=0;
    double percentageStep=100.0/(steps+1);

    noisepoint.setSize(steps);
 
    for (p=from+calcDist/2; p<to; p+=calcDist) {
	double noise = noiseCalc(timestring,zero,p-box/2,p+box/2,
				 steprate,weight, mode);
	// check if chancel has been pressed
	if (noise==-1) {
	    return -1;	  // if so then quit function
	}
	noisepoint.setFrequency(pos, p);
	noisepoint.setNoise(pos, noise);
	pos++;
	percentageStart+=percentageStep;
    }
    
    return pos; // returns number of points // not necessary
}

double CProject::calculateNoise(double frequency, double box, 
				StepQuality stepq, double step,
				DataMode mode, double zero)
{
    int weight      = useFourierWeight;
    double steprate = getStepRate(stepq,step);

    // make the calculations
    double noise = noiseCalc(timestring,zero,frequency-box/2,frequency+box/2,
			     steprate, weight, mode);
    // check if chancel has been pressed
    if (noise==-1) {
	return -1;	// if so then quit function
    }

    // save noise point
    noisepoint.setSize(1);   // 1 noise point 
    noisepoint.setFrequency(0, frequency);
    noisepoint.setNoise(0, noise);
    
    return noise; 
}




