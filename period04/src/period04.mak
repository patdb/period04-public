#
# makefile for Visual C++ compiler
# (successfully tested with the free Visual C++ 2010 Express Suite)
#
# things you may have to change:
# - path of jdk, pthreads
#

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP="C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\bin\amd64\cl.exe"
RSC="C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\bin\amd64\rc.exe"

OUTDIR=.\cpp
INTDIR=.\cpp

ALL : "$(OUTDIR)/Period04.dll"

CLEAN :

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

LIB32="C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\bin\amd64\link.exe"
LIB32_FLAGS=jvm.lib pthreadVC2.lib libboost_regex-vc100-mt-s-x64-1_67.lib /nologo /DLL /out:"Period04.dll" /libpath:"C:\Program Files\Java\jdk-9.0.4\lib" /libpath:"..\pthreads_win\Pre-built.2\lib\x64" /libpath:"C:\local\boost_1_67_0\lib64-msvc-10.0"
LIB32_OBJS= \
	"$(INTDIR)\CColors.obj" \
	"$(INTDIR)\CFourier.obj" \
	"$(INTDIR)\CFourierAlgorithm.obj" \
	"$(INTDIR)\CLeastSquaresRoutine.obj" \
	"$(INTDIR)\CMatrix.obj" \
	"$(INTDIR)\CName.obj" \
	"$(INTDIR)\CNames.obj" \
	"$(INTDIR)\CPeriod.obj" \
	"$(INTDIR)\CPeriPoint.obj" \
	"$(INTDIR)\CProject.obj" \
	"$(INTDIR)\CProjectFourier.obj" \
	"$(INTDIR)\CProjectPeriod.obj" \
	"$(INTDIR)\CProjectTimeString.obj" \
	"$(INTDIR)\CTimePoint.obj" \
	"$(INTDIR)\CTimeString.obj" \
	"$(INTDIR)\DEFS.obj" \
	"$(INTDIR)\VERSION.obj" \
	"$(INTDIR)\mt19937ar.obj" \
	"$(INTDIR)\Period04.obj" \


"$(OUTDIR)\Period04.dll" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<
