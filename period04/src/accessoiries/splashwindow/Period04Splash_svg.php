<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
  version='1.2'
  baseProfile='full'
  xmlns='http://www.w3.org/2000/svg'
  xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
  width='300'
  height='150'
>
<?php
  $blue = '#542dd0';
  $grey = '#cccccc';
  $white = '#ffffff';
  $revision = shell_exec( 'svnversion -n');
?>
  <rect x="1" y="1" width='298' height='148' style='fill:<?php echo $grey ?>; stroke:<?php echo $blue ?>' stroke-width='2' />

  <text
    x="151"
    y="74"
    font-family="Engebrechtre Expanded"
    font-size="58"
    font-weight="bold"
    text-anchor="middle"
    fill="<?php echo $blue ?>"
    stroke="<?php echo $blue ?>"
    stroke-width="0"
  >Period04</text>

  <text
    x="20"
    y="99"
    font-family="Dejavu Sans"
    font-size="17"
    font-weight="bold"
    font-stretch="extra-condensed"
    text-anchor="start"
    fill="<?php echo $white ?>"
    stroke="<?php echo $white ?>"
    stroke-width="0"
  >Revision <?php echo $revision ?></text>

  <text
    x="280"
    y="126"
    font-family="Serpentine-Light"
    font-size="29"
    font-stretch="extra-condensed"
    text-anchor="end"
    fill="<?php echo $white ?>"
    stroke="<?php echo $white ?>"
    stroke-width="0"
  >loading...</text>

</svg>
