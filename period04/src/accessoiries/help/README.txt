
Requirements to generate the help system:
- JavaHelp 2.0:
  Download JavaHelp 2.0 from 
  java.sun.com/products/javahelp/download_binary.html 
  and unpack the archive. Put the directory that contains the JavaHelp
  binaries and libraries into the same directory where this README file is
  located. The directory should be called 'javahelp'.

Register new webpages in files:
- Master.jhm
- MasterTOC.xml
- MasterIndex.xml

makeitso.sh:
  a shell script that calls make, move_help.sh and creates a new period04.jar
  file including the current help files.
  This files executes 
  - Makefile:        produces a jar file in this directory
  - move_help.sh:    a shell script to move the help files into the classes
                     directory, where they will be added to period04.jar



