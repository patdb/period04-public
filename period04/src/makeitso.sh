echo "prepare SGT"
cd ../../sgt/
ant -f build_sgt.xml 
cd ../period04/src/
rsync -av --update ../../sgt/classes/gov/noaa/pmel/ classes/gov/noaa/pmel/
echo "compiling java ..."
ant compile
# options: -Xlint:deprecation -Xlint:unchecked 
# => javah command is now part of build.xml compile
echo "compiling c++ ..."
###############################
# choose appropriate makefile
# GNUMakefile for GCC (linux)
# ICCMakefile for INTEL C++ Compiler (linux), outdated
# MACMakefile for GCC (Mac)
# XLCMakefile.amk for IBM XL C++ Compiler (Mac), outdated
##############################
make -f GNUMakefile
#make -f ICCMakefile
#make -f MACMakefile
#amake -f XLCMakefile.amk 
###############################
echo "jar up ..."
rm -f release/period04.jar
ant
mv libperiod04* release/
# for macOS:
#install_name_tool -change @rpath/libomp.dylib @executable_path/../Java/libomp.dylib release/libperiod04.jnilib 

