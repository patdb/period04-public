
-----------------------------
Compiling Period04 for Linux:
-----------------------------

Requirements:
  Sun Java compiler, gcc (g++), make, ant (http://ant.apache.org/), boost (boost_regex)

Steps:
.) Change the program version and release date of the 'About' box in file 
   XMainFrame.java
.) To avoid compatibility problems it is better to statically link libstdc++ 
   so make a symlink here: 
   ln -s `g++ --print-file-name=libstdc++.a` .
.) Open file makeitso.sh and select the appropriate Makefile
.) Run the shell script by typing 'bash makeitso.sh'.
.) Change to directory release. You will find two new files in this directory:
   period04.jar and libperiod04.so. 
.) To run Period04, type 'java -jar period04.jar'. Note that you have to
   specify the library path otherwise libperiod04.so cannot be found.
   This can be done by setting the environment variable LD_LIBRARY_PATH to the
   appropriate directory or by using the java option -Djava.library.path to define it.


-------------------------------
Compiling Period04 for Windows:
-------------------------------

Requirements:
  Sun Java Compiler
  Visual C++ Compiler (e.g. Visual C++ 2010 Express)
  pthreads-w32 library (http://sourceware.org/pthreads-win32/)
  ant (http://ant.apache.org)
  boost (boost_regex)

Steps:
.) Change the program version and release date of the 'About' box in file 
   XMainFrame.java
.) Open the files '_mak.bat' and 'period04.mak' and change all paths to point
   to the correct directories and the appropriate compiler.
.) run '_mak.bat'
.) Change to directory release. You will find two new files in this directory:
   period04.jar and Period04.dll. 
.) To run Period04, type 'java -jar period04.jar'. 
.) To make a Period04.exe use Launch4j (http://launch4j.sourceforge.net)

------------------------------
Compiling Period04 for Mac OS:
------------------------------

Requirements:
  XCode (Free Download from MacDeveloperCenter)
  Iceberg 
  FreeDMG
  boost (boost_regex)

Steps:
.) Change the program version and release date of the 'About' box in file 
   XProjectFrame.java
.) Activate the ApplicationListener interface for Mac in the file
   Period04.java (for more detailed instructions see the header of the file)
.) Activate the appropriate compiler (MACMakefile) in file makeitso.sh
.) Type 'bash makeitso.sh' to compile the program
.) To test the application:
   cd release
   java -jar period04.jar
.) Option 1: 
   Launch JarBundler (in directory /Developer/Applications/Java/Tools/)
   Set the appropriate settings 
   ("Create Mac Menu Bar", set Max Heap to "256", Bundle Name: "Period04")
   This will create a Mac Application (in a directory called Period04.app)
   Option 2: 
   Just replace libperiod.jnlib, period04.jar in the Period04.app directory 
   of a previous Period04 Installation
.) Create the Period04.pkg package using Iceberg
   Iceberg > Open: period04.packproj in the Period04Iceberg directory
   In "Settings" update version numbers, in "Scripts" provide the path to 
   file 'postflight' in the Period04Iceberg directory
   Iceberg > Build > Build (to build the Package). You'll find the Period04.pkg
   in the Period04Iceberg directory)
.) Create Period04.dmg using FreeDMG

-------------------------
Updating the Help System:
-------------------------

For instructions on updating the help system please see file README.txt in 
directory 'accessoiries/help/'
